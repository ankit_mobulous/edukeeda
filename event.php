<div class="event">


<?php
/**
/*
 Template Name: Home Page event
 */

get_header(); ?>

    <div class="col-md-12">
    <div class="alt">
     <h1>Latest Event</h1>
       <div class="col-md-4"> 
             <a href="http://edukeeda.com/subcategory/">  <div class="even11">
              <img src="http://edukeeda.com/wp-content/uploads/2018/11/student-1.jpg ">
              <div class="eventdst">            
                    <p class="mainhdev">Education Event 2018</p>
                      <span><strong>Location: </strong> <i class="fa fa-map-marker"></i>Noida</span>
                       <span><strong>Author:</strong> Admin</span>
					   <span><strong>Date:</strong> 20/12/2018</span>
    </div>
    </div><p><strong>Hosted By: </strong> Organiser Name</p></a> 
        </div>
          <div class="col-md-4"> 
             <a href="http://edukeeda.com/subcategory/">  <div class="even11">
              <img src="http://edukeeda.com/wp-content/uploads/2018/11/event.jpg ">
              <div class="eventdst">            
                    <p class="mainhdev"> Expert Event 2018</p>
                      <span><strong>Location: </strong><i class="fa fa-map-marker"></i> Noida</span>
                       <span><strong>Author:</strong> Admin</span>
					     <span><strong>Date:</strong> 20/12/2018</span>
    </div>
    </div><p><strong>Hosted By: </strong> Organiser Name</p></a> 
        </div>
          <div class="col-md-4"> 
             <a href="http://edukeeda.com/subcategory/">  <div class="even11">
              <img src="http://edukeeda.com/wp-content/uploads/2018/11/touriem-event.jpg ">
              <div class="eventdst">            
                    <p class="mainhdev">Tourism Event 2018</p>
                      <span><strong>Location: </strong><i class="fa fa-map-marker"></i> Noida</span>
                       <span><strong>Author:</strong> Admin</span>
					     <span><strong>Date:</strong> 20/12/2018</span>
    </div>
    </div><p><strong>Hosted By: </strong> Organiser Name</p></a> 
        </div>
        </div>
         
 </div>
        <div class="col-md-12">
        <div class="odd">
             <h2>Explore by Category</h2>
        <div class="col-md-3"> 
             <a href="#">  <div class="even1">
              <img src="http://edukeeda.com/wp-content/uploads/2018/11/student.jpg ">
                  <p>Competition</p>
              </div></a> 
        </div>
         <div class="col-md-3">
               <a href="#"><div class="even1">
			    <img src="http://edukeeda.com/wp-content/uploads/2018/11/eventimage.jpg  ">
               
                 <p>Seminar</p>
              </div></a>
        </div>
        <div class="col-md-3">
              <a href="#"> <div class="even1">
               <img src="http://edukeeda.com/wp-content/uploads/2018/11/corporate-img-2.jpg  ">
                  <p>Conference</p>
              </div></a>
        </div>
         <div class="col-md-3">
              <a href="#"> <div class="even1">
               <img src="http://edukeeda.com/wp-content/uploads/2018/11/Interesting-facts.jpg  ">
             
                <p>Spiritual Event</p>
              </div></a>
        </div>
 


            
        <div class="col-md-3"> 
              <a href="#"> <div class="even1">
                <img src="http://edukeeda.com/wp-content/uploads/2018/11/tourism.jpg  ">
                <p>Intellectual Discussion</p>
              </div></a>
        </div>
         <div class="col-md-3">
              <a href=""> <div class="even1">
                 <img src="http://edukeeda.com/wp-content/uploads/2018/11/tourism.jpg  ">
                 <p>Social </br>Work/Volunteering</p>
              </div></a>
        </div>
        <div class="col-md-3">
               <a href="#"><div class="even1">
               <img src="http://edukeeda.com/wp-content/uploads/2018/11/inteviwes.jpg  ">
                 <p>Expo/Exibition</p>
              </div></a>
        </div>
		
		<div class="col-md-3">
               <a href="#"><div class="even1">
               <img src="http://edukeeda.com/wp-content/uploads/2018/11/inteviwes.jpg  ">
                 <p>Concert/Live </br>Performance</p>
              </div></a>
        </div>
         
 </div>


			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	

<?php get_footer(); ?>
</div>
<style>
.even {
    box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
       margin-bottom: 35px;
}
.even p{
     margin-bottom: 0px;
     
   }
  
    
   .even1 p{
     position: absolute;
    top: 35%;
    right: 0;
    left: 0;
    font-size: 24px;
	line-height: 24px;

   }
   
   
   .alt h1 {
    font-size: 20px;
}
.odd h2 {
    font-size: 20px;
}
.even1{
  
  box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
       margin-bottom: 35px;
       background-size: cover;
    text-align: center;
    color: white;
}
.even11{
  
  box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
      
       background-size: cover;
    text-align: center;
    color: white;
}
.even1 img {
    width: 100%;
        }
.even11 img {
    width: 100%;
        height: 263px;
}

.eventdst {
    position: absolute;
    top: 29%;
    right: 0;
    left: 0;
}
.eventdst i{
  color: #f68e2f;
  margin-right: 0px!important;
}
p.mainhdev {
    font-size: 24px;
    margin: 0;
}
.eventdst span {
    float: left;
    width: 100%;
}
.eventdst strong {
    font-weight: 600;
}
p.mainhdev{
      position: initial
}





@media only screen and (max-width: 780px)
{
.alt {
    margin-top: 180px;
}

}

</style>
