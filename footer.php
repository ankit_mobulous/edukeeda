<?php
/**
* The template for displaying the footer.
*
* Contains the closing of the #content div and all content after
*
* @package Sydney
*/

print_r($_SESSION['message']);
?>

<script type="text/javascript">
    $(document).ready(function(){
      $("#feedsubmits").on("click", function(){
            var name = $("#exampleInputname").val();
            var email = $("#exampleInputEmail1").val();
            var mobile = $("#exampleInputPassword1").val();
            var message = $("#exampleInputMessage").val();
            
            if(name == '') {
               $("#msg1").html("Please fill this field");
            
            } else if(email == '') {
               $("#msg2").html("Please fill this field");
            
            } else if(mobile == '') {
               $("#msg3").html("Please fill this field");
            
            } else if(message == '') {
               $("#msg4").html("Please fill this field");
            
            } else {
               $.ajax({
                   type: "POST",
                   dataType: "json",
                   url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/save_feedback.php",
                   data:{name:name,email:email,mobile:mobile,message:message},
                   success : function(data) {
                       $("#feedbackform").css("display","none");
                       $("#feedbackmsg").css("display","block");
                   },
                   error: function(jqXHR, textStatus, errorThrown){
                       console.log(textStatus, errorThrown);
                   }
               });
            }
      });
  });
</script>

</div>
</div>
</div><!-- #content -->
<?php do_action('sydney_before_footer'); ?>
<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
<?php get_sidebar('footer'); ?>
<?php endif; ?>
<a class="go-top"><i class="fa fa-angle-up"></i></a>
<footer id="colophon" class="site-footer" role="contentinfo">
   <hr>
   <div class="site-info container">
      <a href="<?php echo esc_url( __( '/', 'sydney' ) ); ?>"><?php printf( __( '&#9400; EDUKEEDA, 2019 . ALL RIGHTS RESERVED', 'sydney' )); ?></a>
      <div class="soci">
         <a href="https://www.facebook.com/TheEdukeeda/" target="_blank"><i class="fa fa-facebook-f"></i></a>
         <a href="https://twitter.com/TheEdukeeda" target="_blank"><i class="fa fa-twitter"></i></a>
         <a href="https://www.instagram.com/edukeedaofficial/" target="_blank"><i class="fa fa-instagram"></i></a>
         <a href="https://www.linkedin.com/company/edukeeda/" target="_blank"><i class="fa fa-linkedin"></i></a>
      </div>
   </div>
   <!-- .site-info -->
</footer>
<!-- #colophon -->
<?php do_action('sydney_after_footer'); ?>
</div><!-- #page -->
<?php                            
   //session_destroy();
wp_footer(); ?>
<script type="text/javascript">
   jQuery(document).ready(function($){
   });
</script>
<?php if(!empty($_SESSION['message'])){ ?>
<script type="text/javascript">
   jQuery(document).ready(function($){
   $('#myModal_message').modal('show'); 
   });
</script>
<?php
   session_destroy();
    } ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<!-- Trigger the modal with a button -->
<!-- Modal -->
<div class="modal fade" id="myModal_message" role="dialog">
   <div class="modal-dialog modal-dialog-centered">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
         </div>
         <div class="modal-body">
            <p><?php echo $_SESSION['message']; ?></p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal_pass" role="dialog">
   <div class="modal-dialog modal-dialog-centered">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Change Password</h4>
         </div>
         <div class="modal-body">
            <p>
            <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profilecode9.php" method="post" id="changepassss">
               <div class="form-group">
                  <label>New Password</label>
                  <input type="text" class="form-control" id="exampleFormControlTextarea1" name="newPass">
                  <center><input type="submit" value="Save" name="submitsteppass" style="margin-top: 20px;"></center>
               </div>
            </form>
            </p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="myModalsend" role="dialog">
   <div class="modal-dialog modal-dialog-centered">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">We would like to hear from you</h4>
         </div>
         <div class="modal-body">
            <p id="feedbackmsg" style="display: none;"> Feedback Submitted Successfully</p>
            <form method="post" action="" id="feedbackform">
               <?php
                  $results3 = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $_SESSION["login"]['id']) );
               ?>
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="exampleIname">Name</label>
                     <input type="text" class="form-control" name="name" id="exampleInputname" placeholder="Enter Name"  autocomplete="false" value="<?php if($results3[0]->name){ echo $results3[0]->name;}?>" <?php if($results3[0]->name){ echo "readonly";}?> required>
                     <span id="msg1"></span>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="exampleInputEmail1">Email address</label>
                     <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" autocomplete="false" value="<?php if($_SESSION["login"]['email']){echo $_SESSION["login"]['email'];}?>" <?php if($_SESSION["login"]['email']){echo "readonly";}?> required>
                     <span id="msg2"></span>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <label for="exampleInputPassword1">Mobile Number</label>
                     <input type="text" class="form-control" name="mobile" id="exampleInputPassword1" placeholder="Enter Mobile" autocomplete="false" value="<?php if($results3[0]->mobile){ echo $results3[0]->mobile;}?>" required>
                     <span id="msg3"></span>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="form-check">
                     <label for="exampleInputMessage">Message</label>
                     <input type="text" class="form-control" name="message" id="exampleInputMessage" placeholder="Message" required>
                     <span id="msg4"></span>
                  </div>
               </div>
               <div class="subbtn">
                  <button type="button" name="feedbacksubmit" id="feedsubmits" class="btn btn-primary">Submit</button>
               </div>
            </form>
         </div>
         <div class="modal-footer">
         </div>
      </div>
   </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
   jQuery(document).ready(function($){
         // validate signup form on keyup and submit
         $("#changepassss").validate({
            rules: {
               newPass: {
                  required: true,
                  pwcheck: true,
                  minlength: 6
               }
            },
              messages: { 
                 newPass: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                 }               
              }
         });
         jQuery.validator.addMethod("lettersonly", function(value, element) {
         return this.optional(element) || /^[a-z\s]+$/i.test(value);
         }, "Only alphabetical characters");
         $.validator.addMethod("pwcheck", function(value) {
            return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
                && /[a-z]/.test(value) // has a lowercase letter
                && /[A-Z]/.test(value) // has a lowercase letter
                && /\d/.test(value) // has a digit
         }, "password must contain a digit, lowercase letter, uppercase letter");
     });
</script>
</body>
</html>


