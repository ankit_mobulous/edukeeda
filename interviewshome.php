<div class="List Of Events">
<?php
/**
/*
 Template Name: Home Page interviews
*/
get_header(); ?>
<?php
    if(isset($_POST["follow"])) {
        $cid = $_POST["catId"];
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $data = ["user_id"=>$userid, "cat_id"=>$cid, "cat_type"=>$catType];
        $wpdb->insert("wp_follow_cat", $data);
    }
    if(isset($_POST["unfollow"])) {
        $cid = $_POST["catId"];
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $wpdb->query($wpdb->prepare("DELETE FROM wp_follow_cat WHERE user_id = %d AND cat_id = %d", $userid, $cid));
    }
?>
<style type="text/css">
   .leftoption input[type="Text"] {
      width: 80%!important;
   }
</style>
    <div class="col-md-8 col-sm-8">
     <div class="colh singlepost">
          <?php
              global $wpdb;
              $catId = 114;
              $getCName = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $catId) 
                  );
              $articles = $wpdb->get_results( 
               $wpdb->prepare( "SELECT COUNT(wp_term_relationships.object_id) as article FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE term_taxonomy_id = %d AND wp_posts.post_status='publish'", $catId) 
           );
          ?>
        <h4 class="colh"><?php echo $getCName[0]->name; ?> <span>(<?php echo $articles[0]->article; ?> Interviews)</span> </h4>
            <?php
              if(isset($_SESSION["login"])) {
                $userId = $_SESSION["login"]["id"];
                $type = "interview";
                $term_id = $catId;
                $checkFollow = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_follow_cat WHERE user_id=%d AND cat_id=%d", $userId, $term_id) 
                );
                if($checkFollow){
              ?>
                    <form method="post" action="">
                      <input type="hidden" name="catId" value="<?php echo $term_id;?>">
                      <input type="hidden" name="catType" value="<?php echo $type;?>">
                      <input type="submit" name="unfollow" value="Unfollow" class="btn btn-warning followbtn" style="float:right;">
                    </form>
              <?php
                } else{
              ?>
                    <form method="post" action="">
                      <input type="hidden" name="catId" value="<?php echo $term_id;?>">
                      <input type="hidden" name="catType" value="<?php echo $type;?>">
                      <input type="submit" name="follow" value="Follow" class="btn btn-warning followbtn" style="float:right;">
                    </form>
              <?php 
                }
              } else {
            ?>
                <button type="button" class="btn btn-warning followbtn" data-toggle="modal" data-target="#myModalCampus" style="float:right;">Follow</button>
            <?php   
              }
            ?>
     </div>
  </div>
   <div class="col-md-8 col-sm-12">
      <div class="">
         <div class="filterkeeda">
            <div class="leftoption">
               <form class="searchform" action=" " method="post" class="search-form">
                 <input type="text" placeholder="Search.." name="search" class="searchtext">
                 <button type="submit"><i class="fa fa-search"></i></button>
               </form>
            </div>
            <div class="rightoption">
              <form action="" method="post" name="myFormName">
               <p>Sort By:</p>
                 <select name="top_filter" id="topdropfilter" onchange='this.form.submit()'>
                    <option value=""> Select </option>
                    <option value="recent">Recent Posts</option>
                    <option value="view">Most Viewed</option>
                 </select>
              </form>
            </div>
         </div>
      </div>


   <div class="col-md-4 col-sm-12 customsideedus">
      <div id="secondary" class="widget-area col-md-3" role="complementary">
         
         <aside id="recent-posts-3" class="widget widget_recent_entries" style="padding-top: 12px;">
            <button class="accordion">  <h3 class="widget-title">Filter</h3></button>
                <div class="panel">
  
                     <?php
                        $parent_cat_ID = $_GET["id"];
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => 114,
                               'taxonomy' => 'big_interview_category'
                           );

                        $subcats = get_categories($args);
                        
                        echo "<ul>";
                        if($subcats) {
                        foreach($subcats as $subcat) {

                            $term_id = $subcat->term_id;
                            $getListCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id));

                      ?>
                                <li>
                                    <form method="post" action="" id="form-id"> 
                                      <a href="<?php echo site_url(); ?>/interviews?action=<?php echo $term_id;?>" onclick="document.getElementById('form-id').submit();"> <?php echo $subcat->name; ?>  ( <?php echo $getListCount[0]->countPost; ?> ) </a>
                                      <input type="hidden" name="fitercat" value="<?php echo $subcat->term_id;?>">
                                    </form>
                                </li>
                            <?php
                        }
                        } else{
                    ?>
                        <li> General </li>
                    <?php
                        }
                        echo "</ul>";
                     ?></div>
                  </aside>

         <aside id="recent-posts-3" class="widget widget_recent_entries">
            <button class="accordion">  <h3 class="widget-title">Name Start With</h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">
              <form action=" " method="post">
               
                 <ul>
                    <li><input type="checkbox" name="vehicle1[]" value="a"> A</li>
                    <li><input type="checkbox" name="vehicle1[]" value="b"> B</li>
                    <li><input type="checkbox" name="vehicle1[]" value="c"> C</li>
                    <li><input type="checkbox" name="vehicle1[]" value="d"> D</li>
                    <li><input type="checkbox" name="vehicle1[]" value="e"> E</li>
                    <li><input type="checkbox" name="vehicle1[]" value="f"> F</li>
                    <li><input type="checkbox" name="vehicle1[]" value="g"> G</li>
                    <li><input type="checkbox" name="vehicle1[]" value="h"> H</li>
                    <li><input type="checkbox" name="vehicle1[]" value="i"> I</li>
                    <li><input type="checkbox" name="vehicle1[]" value="j"> J</li>
                    <li><input type="checkbox" name="vehicle1[]" value="k"> K</li>
                    <li><input type="checkbox" name="vehicle1[]" value="l"> L</li>
                    <li><input type="checkbox" name="vehicle1[]" value="m"> M</li>
                    <li><input type="checkbox" name="vehicle1[]" value="n"> N</li>
                    <li><input type="checkbox" name="vehicle1[]" value="o"> O</li>
                    <li><input type="checkbox" name="vehicle1[]" value="p"> P</li>
                    <li><input type="checkbox" name="vehicle1[]" value="q"> Q</li>
                    <li><input type="checkbox" name="vehicle1[]" value="r"> R</li>
                    <li><input type="checkbox" name="vehicle1[]" value="s"> S</li>
                    <li><input type="checkbox" name="vehicle1[]" value="t"> T</li>
                    <li><input type="checkbox" name="vehicle1[]" value="u"> U</li>
                    <li><input type="checkbox" name="vehicle1[]" value="v"> V</li>
                    <li><input type="checkbox" name="vehicle1[]" value="w"> W</li>
                    <li><input type="checkbox" name="vehicle1[]" value="x"> X</li>
                    <li><input type="checkbox" name="vehicle1[]" value="y"> Y</li>
                    <li><input type="checkbox" name="vehicle1[]" value="z"> Z</li>
                 </ul>
                 <input type="submit" class="applyfilt" value="apply" name="apply_filter">
              </form></div>
            </div>
         </aside>

         <aside id="recent-posts-3" class="widget widget_recent_entries">
            <!--<button class="accordion">  
               <h3 class="widget-title">See Also - Success Story</h3>
            </button>--> 
            <div class="panel1">   
                  See Also - <a href="http://edukeeda.com/success-story/">  Success Story</a>
            </div>
         </aside>
      </div>
   </div>
      <?php
        
         if(isset($_POST["top_filter"])) {
            $topFilter = $_POST["top_filter"];

            if($topFilter == "recent") {
                echo "<div style='clear:both;'></div>";
                echo "<div><p>Sort by: Recent Post</p></div>";
                 $args = array(
                  'numberposts' => 15,
                  'offset' => 0,
                  'category' => 0,
                  'orderby' => 'post_date',
                  'order' => 'DESC',
                  'include' => '',
                  'exclude' => '',
                  'meta_key' => '',
                  'meta_value' =>'',
                  'post_type' => 'interview',
                  'post_status' => 'publish',
                  'suppress_filters' => true
                );

              $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
              //print_r($recent_posts);
            if(!empty($recent_posts))
            {
              foreach ($recent_posts as $value) {
               $titl = strtolower(get_field( "full_name", $value['ID']));

               //var_dump($value["ID"]);
                $cats = get_the_category($value["ID"]);
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
                $author_name = get_the_author_meta( 'display_name', $value['post_author'] );
                $checkViews = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_post_views WHERE post_id = %d", $value['ID']) 
                );
                $countPostTitle = str_word_count($value['post_title']);
                    if($countPostTitle >= 20) {
                      $postTitle = substr($value['post_title'],0, 130);
                      $postTitle .= "...";
                    } else {
                      $postTitle = $value['post_title'];
                    }
            ?>

                   <div class="">


                        <div class="subc">
                           <div class="col-md-4 col-sm-5">
                              <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">
                                <img src="<?php echo $image[0]; ?>" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">
                              </a>
                           </div>
                           <div class="col-md-8 col-sm-7">
                            <p class="categtp" style="margin-bottom: 2px;"><strong></strong><a href="<?php echo site_url();?>/interviews/"> INTERVIEWS </a></p>
                              <div class="subcate">
                                 <p class="subb eventarticle"><a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>"><?php echo !empty($postTitle)?$postTitle:""; ?></a></p>
                              </div>
                           </div>
                           <div class="col-md-4 col-sm-7">
                              <div class="subcate">            
                                 <span><strong><?php echo get_field( "full_name", $value['ID']); ?></strong>
                                 </span>
                                 <span><?php echo get_field( "main_identity", $value['ID']); ?> </span>
                              </div>
                              
                           </div>
                           <div class="col-md-4 col-sm-7">
                              <!--<div class="datespn">
                                 <p>Interviews date<span> <?php //echo get_field( "interviews_date", $value['ID']); ?></span></p>
                              </div>-->
                              <div class="detail">
                                 <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($value['ID']) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                              </div>
                              <div class="subcate">            
                              </div>
                           </div>
                        </div>
                     </div>

            <?php

               }
            }

            } else if($topFilter == "name"){
                echo "<div style='clear:both;'></div>";
                echo "<div><p>Sort by: Name</p></div>";
                 $args = array(
                  'numberposts' => -1,
                  'offset' => 0,
                  'category' => 0,
                  'orderby' => 'post_title',
                  'order' => 'DESC',
                  'include' => '',
                  'exclude' => '',
                  'meta_key' => '',
                  'meta_value' =>'',
                  'post_type' => 'interview',
                  'post_status' => 'publish',
                  'suppress_filters' => true
                );

                $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
              //print_r($recent_posts);
            if(!empty($recent_posts))
            {
              foreach ($recent_posts as $value) {
               $titl = strtolower(get_field( "full_name", $value['ID']));

               //var_dump($value["ID"]);
                $cats = get_the_category($value["ID"]);
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
                $author_name = get_the_author_meta( 'display_name', $value['post_author'] );
                $checkViews = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_post_views WHERE post_id = %d", $value['ID']) 
                );
                $countPostTitle = str_word_count($value['post_title']);
                    if($countPostTitle >= 20) {
                      $postTitle = substr($value['post_title'],0, 130);
                      $postTitle .= "...";
                    } else {
                      $postTitle = $value['post_title'];
                    }
            ?>

                   <div class="">
                        <div class="subc">
                           <div class="col-md-4 col-sm-5">
                              <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">
                                <img src="<?php echo $image[0]; ?>" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">
                              </a>
                           </div>
                           <div class="col-md-8 col-sm-7">
                            <p class="categtp" style="margin-bottom: 2px;"><strong></strong><a href="<?php echo site_url();?>/interviews/"> INTERVIEWS </a></p>
                              <div class="subcate">
                                 <p class="subb eventarticle"><a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>"><?php echo !empty($postTitle)?$postTitle:""; ?></a></p>
                              </div>
                           </div>
                           <div class="col-md-4 col-sm-7">
                              <div class="subcate">            
                                 <span><strong><?php echo get_field( "full_name", $value['ID']); ?></strong>
                                 </span>
                                 <span><?php echo get_field( "main_identity", $value['ID']); ?> </span>
                              </div>
                              
                           </div>
                           <div class="col-md-4 col-sm-7">
                              <!--<div class="datespn">
                                 <p>Interviews date<span> <?php //echo get_field( "interviews_date", $value['ID']); ?></span></p>
                              </div>-->
                              <div class="detail">
                                 <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($value['ID']) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                              </div>
                              <div class="subcate">            
                              </div>
                           </div>
                        </div>
                     </div>

            <?php

               }
            }
             } else if($topFilter == "date") {
                echo "<div style='clear:both;'></div>";
                echo "<div><p>Sort by: Date</p></div>";
                 $args = array(
                  'numberposts' => -1,
                  'offset' => 0,
                  'category' => 0,
                  'orderby' => 'post_title',
                  'order' => 'DESC',
                  'include' => '',
                  'exclude' => '',
                  'meta_key' => '',
                  'meta_value' =>'',
                  'post_type' => 'interview',
                  'post_status' => 'publish',
                  'suppress_filters' => true
                );

                $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
              //print_r($recent_posts);
            if(!empty($recent_posts))
            {
              foreach ($recent_posts as $value) {
               $titl = strtolower(get_field( "full_name", $value['ID']));

               //var_dump($value["ID"]);
                $cats = get_the_category($value["ID"]);
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
                $author_name = get_the_author_meta( 'display_name', $value['post_author'] );
                $checkViews = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_post_views WHERE post_id = %d", $value['ID']) 
                );
                $countPostTitle = str_word_count($value['post_title']);
                    if($countPostTitle >= 20) {
                      $postTitle = substr($value['post_title'],0, 130);
                      $postTitle .= "...";
                    } else {
                      $postTitle = $value['post_title'];
                    }
            ?>

                   <div class="">
                        <div class="subc">
                           <div class="col-md-4 col-sm-5">
                              <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">
                                <img src="<?php echo $image[0]; ?>" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">
                              </a>
                           </div>
                           <div class="col-md-8 col-sm-7">
                            <p class="categtp" style="margin-bottom: 2px;"><strong></strong><a href="<?php echo site_url();?>/interviews/"> INTERVIEWS </a></p>
                              <div class="subcate">
                                 <p class="subb eventarticle"><a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>"><?php echo !empty($postTitle)?$postTitle:""; ?></a></p>
                              </div>
                           </div>
                           <div class="col-md-4 col-sm-7">
                              <div class="subcate">            
                                 <span><strong><?php echo get_field( "full_name", $value['ID']); ?></strong>
                                 </span>
                                 <span><?php echo get_field( "main_identity", $value['ID']); ?> </span>
                              </div>
                              
                           </div>
                           <div class="col-md-4 col-sm-7">
                              <!--<div class="datespn">
                                 <p>Interviews date<span> <?php //echo get_field( "interviews_date", $value['ID']); ?></span></p>
                              </div>-->
                              <div class="detail">
                                  <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($value['ID']) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                              </div>
                              <div class="subcate">            
                              </div>
                           </div>
                        </div>
                     </div>

            <?php

               }
            }
             }  else if($topFilter == "view") {
                echo "<div style='clear:both;'></div>";
                echo "<div><p>Sort by: Most Views</p></div>";
                  $type= 'interview';
                 $results = $wpdb->get_results(
                    $wpdb->prepare( "SELECT * FROM wp_posts INNER JOIN wp_post_views ON wp_posts.ID = wp_post_views.post_id WHERE wp_posts.post_type = %s ORDER BY wp_post_views.view_count DESC", $type) 
                 );
             }
              //print_r($recent_posts);
            if(!empty($results))
            {
              foreach ($results as $result) {
               $titl = strtolower(get_field( "full_name", $result->ID));

               //var_dump($value["ID"]);
                $cats = get_the_category($result->ID);
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result->ID ), 'single-post-thumbnail' );
                $author_name = get_the_author_meta( 'display_name', $result->post_author );
                $checkViews = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_post_views WHERE post_id = %d", $result->ID) 
                );
                $countPostTitle = str_word_count($result->post_title);
                    if($countPostTitle >= 20) {
                      $postTitle = substr($result->post_title ,0, 130);
                      $postTitle .= "...";
                    } else {
                      $postTitle = $result->post_title;
                    }
            ?>

                   <div class="">
                        <div class="subc">
                           <div class="col-md-4 col-sm-5">
                              <a href="<?php echo esc_url( get_permalink($result->ID) ); ?>">
                                <img src="<?php echo $image[0]; ?>" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">
                              </a>
                           </div>
                           <div class="col-md-8 col-sm-7">
                            <p class="categtp" style="margin-bottom: 2px;"><strong></strong><a href="<?php echo site_url();?>/interviews/"> INTERVIEWS </a></p>
                              <div class="subcate">
                                 <p class="subb eventarticle"><a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>"><?php echo !empty($postTitle)?$postTitle:""; ?></a></p>
                              </div>
                           </div>
                           <div class="col-md-4 col-sm-7">
                              <div class="subcate">            
                                 <span><strong><?php echo get_field( "full_name", $result->ID); ?></strong>
                                 </span>
                                 <span><?php echo get_field( "main_identity", $result->ID); ?> </span>
                              </div>
                              
                           </div>
                           <div class="col-md-4 col-sm-7">
                              <!--<div class="datespn">
                                 <p>Interviews date<span> <?php //echo get_field( "interviews_date", $value['ID']); ?></span></p>
                              </div>-->
                              <div class="detail">
                                <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                              </div>
                              <div class="subcate">            
                              </div>
                           </div>
                        </div>
                     </div>

            <?php

               }
            }

         } else if(isset($_POST['apply_filter'])) {
            $vehicle1 = $_POST["vehicle1"];
          
            $args = array(
              'numberposts' => -1,
              'offset' => 0,
              'category' => 0,
              'orderby' => 'post_date',
              'order' => 'DESC',
              'include' => '',
              'exclude' => '',
              'meta_key' => '',
              'meta_value' =>'',
              'post_type' => 'interview',
              'post_status' => 'publish',
              'suppress_filters' => true
            );
         
            $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
            //print_r($recent_posts);
            if(!empty($recent_posts))
            {
              foreach ($recent_posts as $value) {
               $titl = strtolower(get_field( "full_name", $value['ID']));
               
               $getFirstTitle = substr("$titl", 0, 1);

               if (in_array($getFirstTitle, $vehicle1)) {
                 //var_dump($value["ID"]);
                $cats = get_the_category($value["ID"]);

                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );

                $author_name = get_the_author_meta( 'display_name', $value['post_author'] );

                 global $wpdb;
                 $table_name = "wp_post_views";
                 $checkViews = $wpdb->get_results( 
                       $wpdb->prepare( "SELECT * FROM wp_post_views WHERE post_id = %d", $value['ID']) 
                 );
                 $countPostTitle = str_word_count($value['post_title']);
                    if($countPostTitle >= 20) {
                      $postTitle = substr($value['post_title'],0, 130);
                      $postTitle .= "...";
                    } else {
                      $postTitle = $value['post_title'];
                    }
         ?>
                  <div class="">
                     <div class="subc">
                        <div class="col-md-4 col-sm-5">
                          <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">
                           <img src="<?php echo $image[0]; ?>" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">
                         </a>
                        </div>
                        <div class="col-md-8 col-sm-7">
                          <p class="categtp" style="margin-bottom: 2px;"><strong></strong><a href="<?php echo site_url();?>/interviews/"> INTERVIEWS </a></p>
                           <div class="subcate">
                              <p class="subb eventarticle"><a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>"><?php echo !empty($postTitle)?$postTitle:""; ?></a></p>
                           </div>
                        </div>
                        <div class="col-md-4 col-sm-7">
                           <div class="subcate">            
                              <span><strong><?php echo get_field( "full_name", $value['ID']); ?></strong>
                              </span>
                              <span><?php echo get_field( "main_identity", $value['ID']); ?> </span>
                           </div>
                           
                        </div>
                        <div class="col-md-4 col-sm-7">
                           <!--<div class="datespn">
                              <p>Interviews date<span> <?php //echo get_field( "interviews_date", $value['ID']); ?></span></p>
                           </div>-->
                           <div class="rightwd views">
                              <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($value['ID']) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                           </div>
                           <div class="subcate">      
                           </div>
                        </div>
                     </div>
                  </div>
      <?php
                 }
               }
             }
         } else if(isset($_POST['search'])) {
               global $wpdb;
               $action = "interview";
               $searchFilter = $_POST["search"];
              
               $results1 = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_posts WHERE post_status='publish' AND post_type=%s", $action) 
               );
                 
               foreach($results1 as $result1) {
 
                  if(stripos($result1->post_title, $searchFilter) !== false) {
              
                   $getCatName = $wpdb->get_results( 
                       $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $catId) 
                   );
                   $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                  
                  $checkViews = $wpdb->get_results( 
                     $wpdb->prepare( "SELECT * FROM wp_post_views WHERE post_id = %d", $result1->ID) 
                  );
                  $countPostTitle = str_word_count($result1->post_title);
                    if($countPostTitle >= 20) {
                      $postTitle = substr($result1->post_title,0, 130);
                      $postTitle .= "...";
                    } else {
                      $postTitle = $result1->post_title;
                    }
      ?>
               <div class="">
                  <div class="subc">
                     <div class="col-md-4 col-sm-5">
                        <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">
                          <img src="<?php echo $image[0]; ?>" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">
                        </a>
                     </div>
                     <div class="col-md-8 col-sm-7">
                        <p class="categtp" style="margin-bottom: 2px;"><strong></strong><a href="<?php echo site_url();?>/interviews/"> INTERVIEWS </a></p>
                        <div class="subcate">
                           <p class="subb eventarticle"><a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><?php echo !empty($postTitle)?$postTitle:""; ?></a></p>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-7">
                        <div class="subcate">            
                           <span><strong><?php echo get_field( "full_name", $result1->ID); ?></strong>
                           </span>
                           <span><?php echo get_field( "main_identity", $result1->ID); ?> </span>
                        </div>
                        
                     </div>
                     <div class="col-md-4 col-sm-7">
                        <!--<div class="datespn">
                           <p>Interviews date<span> <?php //echo get_field( "interviews_date", $value['ID']); ?></span></p>
                        </div>-->
                        <div class="rightwd views">
                           <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                        </div>
                        <div class="subcate">            
                        </div>
                     </div>
                  </div>
               </div>
      <?php
               }
            }
         }  else if(isset($_GET['action'])) {
               
               global $wpdb;
               $action = "interview";
               $catId = $_GET['action'];
               $results = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id = %d", $catId));

               foreach($results as $result) {

                  $results1 = $wpdb->get_results( 
                     $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d AND post_status='publish' AND post_type=%s", $result->object_id, $action));
                  
                  foreach($results1 as $result1) {

                   $getCatName = $wpdb->get_results( 
                       $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $catId) 
                   );
                   $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                  
                  $checkViews = $wpdb->get_results( 
                     $wpdb->prepare( "SELECT * FROM wp_post_views WHERE post_id = %d", $result1->ID) 
                  );
                  $countPostTitle = str_word_count($result1->post_title);
                    if($countPostTitle >= 20) {
                      $postTitle = substr($result1->post_title,0, 130);
                      $postTitle .= "...";
                    } else {
                      $postTitle = $result1->post_title;
                    }
      ?>
               <div class="">
                  <div class="subc">
                     <div class="col-md-4 col-sm-5">
                        <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">
                          <img src="<?php echo $image[0]; ?>" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">
                        </a>
                     </div>
                     <div class="col-md-8 col-sm-7">
                        <p class="categtp" style="margin-bottom: 2px;"><strong></strong><a href="<?php echo site_url();?>/interviews/"> INTERVIEWS </a></p>
                        <div class="subcate">
                           <p class="subb eventarticle"><a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><?php echo !empty($postTitle)?$postTitle:""; ?></a></p>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-7">
                        <div class="subcate">            
                           <span><strong><?php echo get_field( "full_name", $result1->ID); ?></strong>
                           </span>
                           <span><?php echo get_field( "main_identity", $result1->ID); ?> </span>
                        </div>
                        
                     </div>
                     <div class="col-md-4 col-sm-7">
                        <div class="rightwd views">
                           <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                        </div>
                        <div class="subcate">            
                        </div>
                     </div>
                  </div>
               </div>
      <?php
               }
            }
         } else {
      ?>
      <?php
               $args = array(
                 'numberposts' => 20,
                 'offset' => 0,
                 'category' => 0,
                 'orderby' => 'post_date',
                 'order' => 'DESC',
                 'include' => '',
                 'exclude' => '',
                 'meta_key' => '',
                 'meta_value' =>'',
                 'post_type' => 'interview',
                 'post_status' => 'publish',
                 'suppress_filters' => true
               );
         
               $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
               //print_r($recent_posts);
               if(!empty($recent_posts))
               {
                 foreach ($recent_posts as $value) {
                    //var_dump($value["ID"]);
                   $cats = get_the_category($value["ID"]);
                   $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
                   $author_name = get_the_author_meta( 'display_name', $value['post_author'] );

                   $checkViews = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_post_views WHERE post_id = %d", $value['ID']) 
                    );

                    $countPostTitle = str_word_count($value['post_title']);
                    if($countPostTitle >= 20) {
                      $postTitle = substr($value['post_title'],0, 130);
                      $postTitle .= "...";
                    } else {
                      $postTitle = $value['post_title'];
                    }
         ?>
               <div class="">
                  <div class="subc">
                     <div class="col-md-4 col-sm-5">
                        <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">
                          <img src="<?php echo $image[0]; ?>" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">
                        </a>
                     </div>
                     <div class="col-md-8 col-sm-7">
                        <p class="categtp" style="margin-bottom: 2px;"><strong></strong><a href="<?php echo site_url();?>/interviews/"> INTERVIEWS </a></p>
                        <div class="subcate">
                           <p class="subb eventarticle"><a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>"><?php echo !empty($postTitle)?$postTitle:""; ?></a></p>
                        </div>
                     </div>
                     <div class="col-md-4 col-sm-7">
                        <div class="subcate">            
                           <span><strong><?php echo get_field( "full_name", $value['ID']); ?></strong>
                           </span>
                           <span><?php echo get_field( "main_identity", $value['ID']); ?> </span>
                        </div>
                        
                     </div>
                     <div class="col-md-4 col-sm-7">
                        <!--<div class="datespn">
                           <p>Interviews date<span> <?php //echo get_field( "interviews_date", $value['ID']); ?></span></p>
                        </div>-->
                        <div class="rightwd views">
                          <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($value['ID']) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                        
                        </div>
                        <div class="subcate">            
                        </div>
                     </div>
                  </div>
               </div>
      <?php
            }
          }
        }
      ?>
   </div>


   <div class="col-md-4 col-sm-12 customsideedu">
      <div id="secondary" class="widget-area col-md-3" role="complementary">
         
         <aside id="recent-posts-3" class="widget widget_recent_entries" style="padding-top: 12px;">
            <button class="accordion">  <h3 class="widget-title">Filter</h3></button>
                <div class="panel">
  
                     <?php
                        $parent_cat_ID = $_GET["id"];
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => 114,
                               'taxonomy' => 'big_interview_category'
                           );

                        $subcats = get_categories($args);
                        
                        echo "<ul>";
                        if($subcats) {
                        foreach($subcats as $subcat) {

                            $term_id = $subcat->term_id;
                            $getListCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id));

                      ?>
                                <li>
                                    <form method="post" action="" id="form-id"> 
                                      <a href="<?php echo site_url(); ?>/interviews?action=<?php echo $term_id;?>" onclick="document.getElementById('form-id').submit();"> <?php echo $subcat->name; ?>  ( <?php echo $getListCount[0]->countPost; ?> ) </a>
                                      <input type="hidden" name="fitercat" value="<?php echo $subcat->term_id;?>">
                                    </form>
                                </li>
                            <?php
                        }
                        } else{
                    ?>
                        <li> General </li>
                    <?php
                        }
                        echo "</ul>";
                     ?></div>
                  </aside>

         <aside id="recent-posts-3" class="widget widget_recent_entries">
            <button class="accordion">  <h3 class="widget-title">Name Start With</h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">
              <form action=" " method="post">
               
                 <ul>
                    <li><input type="checkbox" name="vehicle1[]" value="a"> A</li>
                    <li><input type="checkbox" name="vehicle1[]" value="b"> B</li>
                    <li><input type="checkbox" name="vehicle1[]" value="c"> C</li>
                    <li><input type="checkbox" name="vehicle1[]" value="d"> D</li>
                    <li><input type="checkbox" name="vehicle1[]" value="e"> E</li>
                    <li><input type="checkbox" name="vehicle1[]" value="f"> F</li>
                    <li><input type="checkbox" name="vehicle1[]" value="g"> G</li>
                    <li><input type="checkbox" name="vehicle1[]" value="h"> H</li>
                    <li><input type="checkbox" name="vehicle1[]" value="i"> I</li>
                    <li><input type="checkbox" name="vehicle1[]" value="j"> J</li>
                    <li><input type="checkbox" name="vehicle1[]" value="k"> K</li>
                    <li><input type="checkbox" name="vehicle1[]" value="l"> L</li>
                    <li><input type="checkbox" name="vehicle1[]" value="m"> M</li>
                    <li><input type="checkbox" name="vehicle1[]" value="n"> N</li>
                    <li><input type="checkbox" name="vehicle1[]" value="o"> O</li>
                    <li><input type="checkbox" name="vehicle1[]" value="p"> P</li>
                    <li><input type="checkbox" name="vehicle1[]" value="q"> Q</li>
                    <li><input type="checkbox" name="vehicle1[]" value="r"> R</li>
                    <li><input type="checkbox" name="vehicle1[]" value="s"> S</li>
                    <li><input type="checkbox" name="vehicle1[]" value="t"> T</li>
                    <li><input type="checkbox" name="vehicle1[]" value="u"> U</li>
                    <li><input type="checkbox" name="vehicle1[]" value="v"> V</li>
                    <li><input type="checkbox" name="vehicle1[]" value="w"> W</li>
                    <li><input type="checkbox" name="vehicle1[]" value="x"> X</li>
                    <li><input type="checkbox" name="vehicle1[]" value="y"> Y</li>
                    <li><input type="checkbox" name="vehicle1[]" value="z"> Z</li>
                 </ul>
                 <input type="submit" class="applyfilt" value="apply" name="apply_filter">
              </form></div>
            </div>
         </aside>

         <aside id="recent-posts-3" class="widget widget_recent_entries">
            <!--<button class="accordion">  
               <h3 class="widget-title">See Also - Success Story</h3>
            </button>--> 
            <div class="panel1">   
                  See Also - <a href="http://edukeeda.com/success-story/">  Success Story</a>
            </div>
         </aside>
      </div>
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>
<style>
 #secondary button.accordion {
    background-color: transparent;
    border: none;
    width: 100%;
    text-align: left;
   
    padding:0px;
}
#secondary button.accordion:hover {
    outline: none;
}
.panel ul li form#form-id {
    padding: 0px 10px;
}
.panel ul li {
    display: contents;
}
.panel {
  display: none;
  background-color: #f4f4f4;
}
.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    margin-top: -37px;
    font-size: 43px;
}
.colh.singlepost{
margin-top:10px; 
}
a.rightwd.views {
    color: #000;
}
.panel ul li {
    width: 19%;
    float: left;
}
#secondary button.accordion {
    background-color: transparent;
    border: none;
    width: 100%;
    text-align: left;
    border-bottom: 1px solid #f68e2f;
    padding: 0px;
}
#secondary button.accordion:hover {
    outline: none;
}
.panel {
  display: none;
  background-color: #f4f4f4;
      float: left;
    width: 100%;
}
.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    margin-top: -37px;
    font-size: 43px;
}

.active:after {
    content: "\2212";
}

.colh .btn-warning {
    color: #fff;
    background-color: #f68e2f;
    border-color: #f68e2f;
}
.colh {
    float: left;
    margin: 0px;
    padding: 10px 0px 4px;
}
   .datespn span {
   color: #474280;
   font-weight: 600;
   }
   span.dateDisplay-day {
   color: #f68e2f;
   font-size: 20px;
   font-weight: 800;
   line-height: 21px;
   }
   .subc i.fa.fa-share-alt {
   
   }
   .subc
   {
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   float: left;
   width: 100%;
   margin-bottom: 40px;
   padding:10px;
   border:1px solid #ddd;
   }
   .intdate span {
   font-size: 13px;
   font-weight: 600;
   display: block;
   padding: 0px;
   background: #f4f4f4;
   border-radius:10px;
   margin: 0px 53px 0px 53px;
   text-align: center;
   line-height: 25px;
   }
   .detail a {
   background: #439a37;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   display: block;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .detail a:hover {
   background-color: #f68e2f;
   }
   .subc i{
   margin-right: 0px!important;
   }
   p.subb {
   font-size: 20px;
   font-weight: 600;
   margin-bottom: 0px;
   float: left;
   }
   .subcate a {
   float: right;
   color:#e96125;
   }
    .subcate a:hover{
        color:#5a5a5a;
    }
   .subcate strong {
   font-size: 15px;
   }
   .subcate span {
   display: block;
   }
   .leftoption {
   float: left;
   }
   .leftoption input[type="Text"] {
   width: 103%;
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   padding: 0 16px;
   }
   .rightoption {
   float: right;
   }
   
   .rightoption p{
   float:left;
   }
   #secondary{width:100%;}
   input.applyfilt {
   padding: 4px 17px;
   float: right;
   }
   .sortbyalphabet ul li {
   display: inline-block;
   margin: 0 19px 0 0;
   }
   p.subb.eventarticle {
   line-height: 23px;
   min-height: 57px;
   overflow: hidden;
   }
   img.img-thumbnail {
   height: 131px;
   }
   div.rightwd.views {
    color: #000;
    font-size: 13px;
    float: right;
}
.subcate {
    float: left;
    width: 100%;
}
.col-md-4.col-sm-12.customsideedus {
    display: none;
}
 @media only screen and (max-width: 767px)
   {
.rightoption {
    float: right;
    width: 100%;
}
.col-md-4.col-sm-12.customsideedu {
    display: none;
}
.col-md-4.col-sm-12.customsideedus #secondary {
    width: 100%;
    float: left;
    margin-bottom: 25px;
}
.col-md-4.col-sm-12.customsideedus {
    display: block;

    padding: 0px;
    margin: 20px 0px;

}
.leftoption {
    float: left;
    width: 100%;
}
.rightoption select {
    width: 70%;
    margin: 0px;
}
.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
       margin-top: 0px;
}
.colh {
    padding: 0px;
}
.colh.singlepost{
margin-top:100px; 
    margin-bottom: 15px;
}
.subc img.img-thumbnail {
    width: 100%;
    height: auto;
}
.page-wrap {
    padding: 0px;
}
.searchform button{
  padding: 4px 21px;
}
   }
</style>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
   <!-- Modal -->
<div id="myModalCampus" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Follow</h4>
      </div>
      <div class="modal-body">
        <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
      </div>
    </div>
  </div>
</div>
