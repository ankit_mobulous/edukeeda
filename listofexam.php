<div class="join">
<?php
  /**
  /*
    Template Name:competitive examinations details 
  */
        
get_header(); ?>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;
?>
<style>

   .want a {
   border-radius: 10px;
   background: #f68e2f;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .bt ul.nav.nav-tabs {
    border-bottom: none;
}
   .join .page-wrap .container {
    width: 100%;
    padding: 0;
    margin: 0 auto;
}

.join .page-wrap .container .row {
    margin: 0;
}
 .want {
    float: left;
    width: 100%;
    padding: 9px 0px;
text-align: center;
}
  
   .catchybga {
       padding: 6% 0;
    background-position: 100%;
    position: relative;
    margin-bottom: 45px;
    background: #4054b2;
   }
   
   .want h3 {
   color: white;
   font-size: 28px;
   font-weight: 600;
   margin-bottom: 0px;
   }
  
 .dynamicprofile {
      float: left;
      margin: 0 auto;
      width: 100%;
      padding: 10px 27px;
      background: #ffffff;
      color: #5c6b77;
      box-shadow: 0px 0px 4px 0px;
      margin-bottom: 60px;
      }
      .dynamicprofile img{
      width: 35px;
      height: 34px;
      float: left;
      margin-right: 10px;
      }
      .dynamicprofile p.username.info {
      font-size: 18px;
      margin: 0;
      color: #1553de;
      }
      .header-clone {
      display: none;
      }
      .settingsopn {
      float: right;
      }
      .settingsopn ul {list-style-type:none;    padding: 0; margin: 0;}
      .settingsopn ul li{display:inline-block;    margin-right: 30px;}
      .settingsopn ul li:last-child{ margin-right: 0px;}
      .usernamesopn{float:left;     width: 50%;}
      .studentprofilesmt {
      float: left;
      width: 100%;
      }
      .studentprofilesmt .elementor-widget-tabs .elementor-tab-title.elementor-active {
      color: #ffffff;
      background: #555ba2;
      border-radius: 1px 33px 1px 0px;
      box-shadow: none;
      border: 0px solid #fff;
      }
      .studentprofilesmt .elementor-widget-tabs .elementor-tab-title{
      color: #a3a9ab;
      }
      .studentprofilesmt .elementor-tabs {
      box-shadow: #bdbdbd 0px 0px 20px 0px;
      }
      .studentprofilesmt .elementor-column-gap-default>.elementor-row>.elementor-column>.elementor-element-populated {
      padding: 0 12px;
      }
      .advanceftr {
      float: left;
      }
      .advanceftr h5 {
      font-size: 20px;
      padding: 0 23px;
      }
      .mainhead {
      margin: 74px 0 40px 0;
      text-align: center;
      background: #4054b2;
      padding: 20px 0;
      color: #fff;
      }
      .mainhead h6 {
      color: #ffffff;
      font-size: 30px;
      font-family: "Roboto", Sans-serif;
      font-weight: 600;
      }
      .sidebar.sidebarBox {
      box-shadow: 0px 0px 4px 0px;
      width: 100%;
      float: right;
      padding: 20px;
      }
      .sortFilter {
      border-bottom: 1px solid #e5e5e5;
      margin: -15px -15px 0;
      padding: 10px 14px;
      font-size: 16px;
      color: #333;
      font-weight: 600;
      }
      a.clAll.clearAll {
      font-size: 14px;
      font-weight: 400;
      color: #3a4d95;
      float: right;
      }
      input[type="checkbox"], input[type="radio"] {
      margin: 0 10px 0 0px;
      }
      .filterBox h3 {
      font-size: 14px;
      color: #333;
      margin: 12px 0 11px;
      font-weight: 600;
      text-transform: uppercase;
      }
      .filterBox label {
      text-transform: uppercase;
      font-weight: 100;
      color:#000;
      }
      a.elementor-button-link.elementor-button.elementor-size-sm{
      font-weight: 400;
      font-style: normal;
      color: #000;
      background-color: #ffffff;
      display: block;
      width: 100%;
      box-shadow: 0px 0px 2px 0px;
text-align: center;
      }
      .elementor-button-wrapper {
      width: 100%;
      }
      a.elementor-button-link.elementor-button.elementor-size-sm:hover{
      color: #ffffff;
      background-color: #23282d;
      }
      .previouspapas a {
      font-size: 21px;
      color: #484848;
      }
      .previouspapas a:hover{
      color: #2f2fd8;
      }
      .examlistcolm{
      margin: 0 0 30px 0;
      }
      .row.filterbtndrow {
      margin: 0 0 30px 0;
      }  
.bt a {
    background-color: #818a91;
    color: #fff;
    text-align: center;
}
.bt {
    float: left;
    width: 100%;
    margin-bottom: 20px;
}
.bt li {
        margin: 0px 0px 0px 56px;
}
.bt .nav>li>a:focus, .nav>li>a:hover {
    text-decoration: none;
       background-color: #eee;
    color: black;
}
   @media only screen and (max-width: 767px)
   {
   .want h3 {
   color: white;
   font-size: 23px;
   font-weight: 100;
   }

   .want p {
   font-size: 17px;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 20px;
   }
  
   }
</style>

   <div class="catchybga">
      <div class="want">
         <h3>List of Competitive Examinations & details</h3> 
      </div>
         </div>
      
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    
           
                <div class="bt">
                     <ul class="nav nav-tabs">
                     <li class="active"><a href="#tab1" data-toggle="tab" aria-expanded="true"> Entrance Exam for higher studies </a></li><a href="#tab1" data-toggle="tab">
                     </a><li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false"> Competitive Exams for Scholarship </a></li>
                      <li class=""><a href="#tab3" data-toggle="tab" aria-expanded="false"> Competitive Exams for Job </a></li>
                </ul>
            
      </div>
            
      </div>
      <div class="tab-content">
           <div class="tab-pane active" id="tab1">
		        <div class="contents">
                    <div class="col-md-9 tabdatabga">
                         <section class="examlistcolm ">
               <div class="row">
                  <div class="col-md-2">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-image">
                           <img width="45" height="45" src="http://edukeeda.com/wp-content/uploads/2018/10/JEE-Advanced_logo.png" class="attachment-full size-full" alt="">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-text-editor elementor-clearfix previouspapas">
                           <p><a href="#">Joint Entrance Exam Advanced (JEE Advanced)</a>
                              <br><strong>Purpose: </strong> Class 12 passed, graduation, post graduation &amp; coaching
                              <br><strong>Eligibility:</strong> 12th and Graduate.
                              <br><strong>Exam Date: </strong>21/04/2019.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-button-wrapper">
                           <a href="http://edukeeda.com/examdetails/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                           <span class="elementor-button-content-wrapper">
                           <span class="elementor-button-text">Details</span>
                           </span>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="examlistcolm ">
               <div class="row">
                  <div class="col-md-2">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-image">
                           <img width="45" height="45" src="http://edukeeda.com/wp-content/uploads/2018/10/JEE-Advanced_logo.png" class="attachment-full size-full" alt="">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-text-editor elementor-clearfix previouspapas">
                           <p><a href="#">Joint Entrance Exam Advanced (JEE Advanced)</a>
                              <br><strong>Purpose: </strong> Class 12 passed, graduation, post graduation &amp; coaching
                              <br><strong>Eligibility:</strong> 12th and Graduate.
                              <br><strong>Exam Date: </strong>21/04/2019.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-button-wrapper">
                           <a href="http://edukeeda.com/examdetails/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                           <span class="elementor-button-content-wrapper">
                           <span class="elementor-button-text">Details</span>
                           </span>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </section> 
                               
                                    </div>
									<div class="col-md-3">
                                     <div class="sidebar sidebarBox">
               <div class="sortFilter">Sort &amp; Filter <a class="clAll clearAll" href="/exams/">Clear All</a></div>
               <form class="sortFilterForm" method="post">
                  <div class="filterBox examSort">
                     <h3>Sort By</h3>
                     <div class="box">
                        <input checked="" id="popularity" name="sort_filter" type="radio" value="pop">
                        <label for="popularity">Popularity</label>
                     </div>
                     <div class="box">
                        <input id="alphabetically" name="sort_filter" type="radio" value="alpha">
                        <label for="alphabetically">Alphabetically</label>
                     </div>
                  </div>
                <div class="row form-group">
                                                
                                                <div class="slec">
                                                    <label for="select" class="form-control-label">  Select Purpose </label>
                                                </div>
                                               <div class="slecopt">
                                                    <select name="select" id="select" class="form-control">
                                                        <option value="0">Please select Categories</option> 
                                                        <option value="1">Class 12 passed</option>
                                                        <option value="2">graduation</option>
                                                        <option value="3">post graduation</option>
                                                        <option value="3">post graduation & coaching </option>
                                                    </select>
                                                </div>
                                                </div>  
                  <input name="csrfmiddlewaretoken" type="hidden" value="JLIYsrNZXI6q8JJOPkDyJBEpEPJ4k7ZaPks7QcXl1I27uviYpcgOb5RcJLpcijT9">
                 
               </form>
            </div>
                                    </div>
                              </div>
							  
                                    </div>
                                    <div class="tab-pane " id="tab2">
                                       <div class="conten2">
                                               <div class="col-md-9 tabdatabga">
                                    <section class="examlistcolm ">
               <div class="row">
                  <div class="col-md-2">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-image">
                           <img width="45" height="45" src="http://edukeeda.com/wp-content/uploads/2018/10/JEE-Advanced_logo.png" class="attachment-full size-full" alt="">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-text-editor elementor-clearfix previouspapas">
                           <p><a href="#">Joint Entrance Exam Advanced (JEE Advanced)</a>
                              <br><strong>Purpose: </strong> Class 12 passed, graduation, post graduation &amp; coaching
                              <br><strong>Eligibility:</strong> 12th and Graduate.
                              <br><strong>Exam Date: </strong>21/04/2019.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-button-wrapper">
                           <a href="http://edukeeda.com/examdetails/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                           <span class="elementor-button-content-wrapper">
                           <span class="elementor-button-text">Details</span>
                           </span>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </section> 
                                    </div>
									<div class="side">
										  <div class="advanceftr col-md-3">
            <div class="sidebar sidebarBox">
               <div class="sortFilter">Sort &amp; Filter <a class="clAll clearAll" href="/exams/">Clear All</a></div>
               <form class="sortFilterForm" method="post">
                  <div class="filterBox examSort">
                     <h3>Sort By</h3>
                     <div class="box">
                        <input checked="" id="popularity" name="sort_filter" type="radio" value="pop">
                        <label for="popularity">Popularity</label>
                     </div>
                     <div class="box">
                        <input id="alphabetically" name="sort_filter" type="radio" value="alpha">
                        <label for="alphabetically">Alphabetically</label>
                     </div>
                  </div>
                 
                  <input name="csrfmiddlewaretoken" type="hidden" value="JLIYsrNZXI6q8JJOPkDyJBEpEPJ4k7ZaPks7QcXl1I27uviYpcgOb5RcJLpcijT9">
               </form>
  <div class="filterBox examLabel">
                     <h3>Exam Level</h3>
                     <div class="box">
                        <label class="control controlCheckbox">
                           <input name="level" type="checkbox" value="1">UG
                           <div class="controlIndicator"></div>
                        </label>
                     </div>
                     <div class="box">
                        <label class="control controlCheckbox">
                           <input name="level" type="checkbox" value="2">PG
                           <div class="controlIndicator"></div>
                        </label>
                     </div>
                     <div class="box">
                        <label class="control controlCheckbox">
                           <input name="level" type="checkbox" value="4">Diploma
                           <div class="controlIndicator"></div>
                        </label>
                     </div>
                     <div class="box">
                        <label class="control controlCheckbox">
                           <input name="level" type="checkbox" value="5">School
                           <div class="controlIndicator"></div>
                        </label>
                     </div>
                  </div>
            </div>
         </div>
									</div>
                              
                                    </div>

</div>
     <div class="tab-pane " id="tab3">
    
    
                               <div class="">
                                    <div class="col-md-9 tabdatabga">
                                             <section class="examlistcolm ">
               <div class="row">
                  <div class="col-md-2">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-image">
                           <img width="45" height="45" src="http://edukeeda.com/wp-content/uploads/2018/10/JEE-Advanced_logo.png" class="attachment-full size-full" alt="">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-8">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-text-editor elementor-clearfix previouspapas">
                           <p><a href="#">Joint Entrance Exam Advanced (JEE Advanced)</a>
                              <br><strong>Purpose: </strong> Class 12 passed, graduation, post graduation &amp; coaching
                              <br><strong>Eligibility:</strong> 12th and Graduate.
                              <br><strong>Exam Date: </strong>21/04/2019.
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-button-wrapper">
                           <a href="http://edukeeda.com/examdetails/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                           <span class="elementor-button-content-wrapper">
                           <span class="elementor-button-text">Details</span>
                           </span>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
                               
                                    </div>
                                          <div class="side">
										  <div class="advanceftr col-md-3">
            <div class="sidebar sidebarBox">
               <div class="sortFilter">Sort &amp; Filter <a class="clAll clearAll" href="/exams/">Clear All</a></div>
               <form class="sortFilterForm" method="post">
                  <div class="filterBox examSort">
                     <h3>Sort By</h3>
                     <div class="box">
                        <input checked="" id="popularity" name="sort_filter" type="radio" value="pop">
                        <label for="popularity">Popularity</label>
                     </div>
                     <div class="box">
                        <input id="alphabetically" name="sort_filter" type="radio" value="alpha">
                        <label for="alphabetically">Alphabetically</label>
                     </div>
                  </div>
                  <div class="row form-group">
                                                
                                                <div class="slec">
                                                    <label for="select" class="form-control-label">  Select Purpose </label>
                                                </div>
                                               <div class="slecopt">
                                                    <select name="select" id="select" class="form-control">
                                                        <option value="0">Please select Categories</option> 
                                                        <option value="1">Class 12 passed</option>
                                                        <option value="2">graduation</option>
                                                        <option value="3">post graduation</option>
                                                        <option value="3">post graduation & coaching </option>
                                                    </select>
                                                </div>
                                                </div> 
                  <input name="csrfmiddlewaretoken" type="hidden" value="JLIYsrNZXI6q8JJOPkDyJBEpEPJ4k7ZaPks7QcXl1I27uviYpcgOb5RcJLpcijT9">
               </form>
            </div>
         </div>
									</div>
                                    </div>

</div>
</div>
</div>

</div>
</div>

 
   
 
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
</div>   
<?php get_footer(); ?>
</div>


