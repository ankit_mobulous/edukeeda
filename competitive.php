<div class="join">
<?php
  /**
  /*
    Template Name:Competitive Exams
  */
        
  get_header(); ?>
   <?php
      $path = $_SERVER['DOCUMENT_ROOT'];
      include_once $path . '/wp-config.php';
      include_once $path . '/wp-load.php';
      include_once $path . '/wp-includes/wp-db.php';
      include_once $path . '/wp-includes/pluggable.php';
      
      global $wpdb;
      ?>
   <style>
   .post_article{
       padding: 10px;
    font-size: 8px;
    line-height: 15px;
   }
   .join a:hover {
    color: none;
    border-color: #f68e2f;
    border: 0px solid #f68e2f;
    background: transparent;
}
.join a {
    font-size: 16px;
    margin: 0px;
    padding: 0px;
    background: none;
    color: none;
    border-radius: 0px;
}
      .want a {
      border-radius: 10px;
      background: #f68e2f;
      color: #fff;
      padding: 6px 18px;
      font-size: 13px;
      text-align: center;
      margin: 13px 0 0 0;
      text-transform: capitalize;
      }
      .bt ul.nav.nav-tabs {
      border-bottom: none;
      }
      .join .page-wrap .container {
      
      padding: 0;
      margin: 0 auto;
      }
     
      .want {
      float: left;
      width: 100%;
         padding: 0px 0px;
      text-align: center;
      }
      .catchybga {
        padding: 2% 0;
    background-position: 100%;
    position: relative;
    margin-bottom: 46px;
   
        margin: 7% 0%;
      }
	  .container-fluid {
          padding: 0px 47px;
      }
      .want h3 {
      color: #060658;
      font-size: 28px;
      font-weight: 600;
      margin-bottom: 0px;
          
      }
      .dynamicprofile {
      float: left;
      margin: 0 auto;
      width: 100%;
      padding: 10px 27px;
      background: #ffffff;
      color: #5c6b77;
      box-shadow: 0px 0px 4px 0px;
      margin-bottom: 60px;
      }
      .dynamicprofile img{
      width: 35px;
      height: 34px;
      float: left;
      margin-right: 10px;
      }
      .dynamicprofile p.username.info {
      font-size: 18px;
      margin: 0;
      color: #1553de;
      }
      .header-clone {
      display: none;
      }
      .settingsopn {
      float: right;
      }
      .settingsopn ul {list-style-type:none;    padding: 0; margin: 0;}
      .settingsopn ul li{display:inline-block;    margin-right: 30px;}
      .settingsopn ul li:last-child{ margin-right: 0px;}
      .usernamesopn{float:left;     width: 50%;}
      .studentprofilesmt {
      float: left;
      width: 100%;
      }
      .studentprofilesmt .elementor-widget-tabs .elementor-tab-title.elementor-active {
      color: #ffffff;
      background: #555ba2;
      border-radius: 1px 33px 1px 0px;
      box-shadow: none;
      border: 0px solid #fff;
      }
      .studentprofilesmt .elementor-widget-tabs .elementor-tab-title{
      color: #a3a9ab;
      }
      .studentprofilesmt .elementor-tabs {
      box-shadow: #bdbdbd 0px 0px 20px 0px;
      }
      .studentprofilesmt .elementor-column-gap-default>.elementor-row>.elementor-column>.elementor-element-populated {
      padding: 0 12px;
      }
      .advanceftr {
      float: left;
      }
      .advanceftr h5 {
      font-size: 20px;
      padding: 0 23px;
      }
      .mainhead {
      margin: 74px 0 40px 0;
      text-align: center;
      background: #4054b2;
      padding: 20px 0;
      color: #fff;
      }
      .mainhead h6 {
      color: #ffffff;
      font-size: 30px;
      font-family: "Roboto", Sans-serif;
      font-weight: 600;
      }
      .sidebar.sidebarBox {
      box-shadow: 0px 0px 4px 0px;
      width: 100%;
      float: right;
      padding: 20px;
      }
      .sortFilter {
      border-bottom: 1px solid #e5e5e5;
      margin: -15px -15px 0;
      padding: 10px 14px;
      font-size: 16px;
      color: #333;
      font-weight: 600;
      }
      a.clAll.clearAll {
      font-size: 14px;
      font-weight: 400;
      color: #3a4d95;
      float: right;
      }
      input[type="checkbox"], input[type="radio"] {
      margin: 0 10px 0 0px;
      }
      .filterBox h3 {
      font-size: 14px;
      color: #333;
      margin: 12px 0 11px;
      font-weight: 600;
      text-transform: uppercase;
      }
      .filterBox label {
      text-transform: uppercase;
      font-weight: 100;
      color: #484848;
      }
      a.elementor-button-link.elementor-button.elementor-size-sm{
      font-weight: 400;
      font-style: normal;
      color: #484848;
      background-color: #ffffff;
      display: block;
      width: 100%;
      box-shadow: 0px 0px 2px 0px;
      text-align: center;
      }
      .elementor-button-wrapper {
      width: 100%;
      }
      a.elementor-button-link.elementor-button.elementor-size-sm:hover{
      color: #ffffff;
      background-color: #23282d;
      }
      .previouspapas a {
      font-size: 21px;
      color: #060658;
      }
      .previouspapas a:hover{
      color: #f68e2f;
      }
      .examlistcolm{
      margin: 0 0 30px 0;
	  padding: 32px 10px;
      }
      .row.filterbtndrow {
      margin: 0 0 30px 0;
      }  
      .bt a {
      background-color: #818a91;
      color: #fff;
      text-align: center;
      }
      .bt {
      float: left;
      width: 100%;
      margin-bottom: 20px;
      }
      .bt li {
    margin: 0px 0px 0px 0px;
      }
      .bt .nav>li>a:focus, .nav>li>a:hover {
      text-decoration: none;
      background-color: #eee;
      color: black;
      }
      #mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-megamenu.mega-menu-item
      {
          margin: 11px 0px 11px 0px;
      }
	  
	  
	  
  @media only screen and (max-width: 767px)
      {
		  
		 .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper{
           padding: 15px;
		 }			 
		  .contents {
    float: left;
    width: 100%;
    margin: 0px 0px;
}
      .want h3 {
      color: white;
      font-size: 23px;
      font-weight: 100;
    width: 100%;
    float: left;
    padding: 0px 0px;
    margin: -19px 0px;
      }
      .want p {
      font-size: 17px;
      }
      .eventhead {
      width: 100%;
      float: left;
      margin-top: 20px;
      }
      .elementor-image img {
        width: 100%;
          height: auto;
         }
    .catchybga {
    padding: 12% 0;
    background-position: 100%;
    position: relative;
    margin-bottom: 46px;
    background: #4054b2;
    margin: 0% ;
	margin-top: 60px;
}
.tab-content {
    margin-top: 215px;
}
.bt li {
   margin: 0px 0px 0px 0px;
}
.nav-tabs>li>a {
  margin: 10px 2px 16px 1px;
}
.elementor-text-editor.elementor-clearfix.previouspapas {
    margin: 5px;
    padding: 0px;
    float: left;
    width: 100%;
}
.elementor-button-wrapper {
    width: 100%;
    padding: 5px;
}
      }
   </style>
<script type="text/javascript">
  function mySubmit(theForm) {
      $.ajax({ 
          data: $(theForm).serialize(), // get the form data
          type: $(theForm).attr('method'), // GET or POST
          url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_exam_list.php",
          success: function (response) { // on success..
               var data_array = $.parseJSON(response);
              $('.tabstep1').html(data_array); // update the DIV
          },
          error: function(jqXHR, textStatus, errorThrown){
              console.log(textStatus, errorThrown);
          }
    
      });
  }

  function mySubmit1(theForm) {
      $.ajax({ 
          data: $(theForm).serialize(), // get the form data
          type: $(theForm).attr('method'), // GET or POST
          url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_exam_list.php",
          success: function (response) { // on success..
               var data_array = $.parseJSON(response);
              $('.tabstep1').html(data_array); // update the DIV
          },
          error: function(jqXHR, textStatus, errorThrown){
              console.log(textStatus, errorThrown);
          }
    
      });
  }

  function mySubmit2(theForm2) {
      $.ajax({ 
          data: $(theForm2).serialize(), // get the form data
          type: $(theForm2).attr('method'), // GET or POST
          url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_exam_list.php",
          success: function (response) { // on success..
               var data_array = $.parseJSON(response);
              $('.tabstep2').html(data_array); // update the DIV
          },
          error: function(jqXHR, textStatus, errorThrown){
              console.log(textStatus, errorThrown);
          }
    
      });
  }

  function mySubmit3(theForm3) {

      $.ajax({ 
          data: $(theForm3).serialize(), // get the form data
          type: $(theForm3).attr('method'), // GET or POST
          url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_exam_list2.php",
          success: function (response2) { // on success..
               var data_array2 = $.parseJSON(response2);
              $('.tabstep3').html(data_array2); // update the DIV
          },
          error: function(jqXHR, textStatus, errorThrown){
              console.log(textStatus, errorThrown);
          }
    
      });
  }
  $(document).ready(function(){
      $(".examlevel").on('click', function(){
            var favorite = [];
            $.each($("input[name='level']:checked"), function(){
                favorite.push($(this).val());
            });
            //alert(favorite);
            var eid = $("#geteid").val();
            $.ajax({ 
                data: {eid:eid,favorite:favorite}, // get the form data
                type: "post", // GET or POST
                url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_exam_list3.php",
                success: function (response3) { // on success..
                  //console.log(response3);
                  var data_array2 = $.parseJSON(response3);
                  $('.tabstep2').html(data_array2); // update the DIV
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(textStatus, errorThrown);
                }
          
            });
      });
  });
</script>

   <div class="catchybga">
      <div class="want">
         <h3>List of Competitive Examinations & details</h3>
      </div>
   </div>
   <div class="container-fluid">
      <div class="row">
         <div class="col-md-12">
            <div class="bt">
               <ul class="nav nav-tabs">
                  <li class="<?php if(isset($_POST["selectc"])) {echo "";}else{echo "active";}?>"><a href="#tab1" data-toggle="tab" aria-expanded="<?php if(isset($_POST["selectc"])) {echo "false";}else{echo "true";}?>"> Entrance Exam for higher studies </a></li>

                  <li class=""><a href="#tab2" data-toggle="tab" aria-expanded="false"> Competitive Exams for Scholarship </a></li>

                  <li class="<?php if(isset($_POST["selectc"])) {echo "active";}?>"><a href="#tab3" data-toggle="tab" aria-expanded="<?php if(isset($_POST["selectc"])){echo "true";}else{echo "false";}?>"> Competitive Exams for Job </a></li>
               </ul>
            </div>
         </div>
         <div class="tab-content">
            <div class="tab-pane active" id="tab1">
               <div class="contents">
                  <div class="col-md-8 tabdatabga tabstep1">
                    <?php
                        if($_GET["action"]) {
                          $action = $_GET["action"];
                          $getExamDetails = $wpdb->get_results( 
                              $wpdb->prepare( "SELECT id,exam_title,purpose,eligibility,exam_date,exam_image FROM wp_exam_details WHERE exam_id = %d AND exam_of=1", $action)
                          );
                        } else {
                          $examOf = 1;
                          $getExamDetails = $wpdb->get_results("SELECT id,exam_title,purpose,eligibility,exam_date,exam_image FROM wp_exam_details WHERE exam_of=1");
                        }
                        foreach($getExamDetails as $getExamDetail) {
                    ?>
                     <section class="examlistcolm ">
                        <div class="row">
                           <div class="col-md-2">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-image">
                                    <img width="45" height="45" src="<?php echo $getExamDetail->exam_image; ?>" class="attachment-full size-full" alt="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-8">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-text-editor elementor-clearfix previouspapas">
                                    <p><a href="#"><?php echo $getExamDetail->exam_title; ?></a>
                                      <br><strong>Purpose: </strong> <?php echo $getExamDetail->purpose; ?>
                                      <br><strong>Eligibility:</strong> <?php echo $getExamDetail->eligibility; ?>
                                      <br><strong>Exam Date: </strong> <?php echo date("d/m/Y", strtotime($getExamDetail->exam_date)); ?>
                                   </p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-2">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-button-wrapper">
                                    <a href="http://edukeeda.com/examdetails?action=<?php echo $getExamDetail->id; ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                    <span class="elementor-button-content-wrapper">
                                    <span class="elementor-button-text">Details</span>
                                    </span>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                     <?php
                          }
                    ?>
                  </div>
                  <div class="col-md-4">
                     <div class="sidebar sidebarBox">
                        <div class="sortFilter">Sort &amp; Filter <a class="clAll clearAll" href="<?php echo site_url();?>/list-of-competitive-examinations-details/">Clear All</a></div>
                        <form class="sortFilterForm" method="post">
                           <div class="filterBox examSort">
                              <h3>Sort By</h3>
                              <div class="box">
                                 <input checked="" id="popularity" name="sort_filter" type="radio" value="pop">
                                 <label for="popularity">Popularity</label>
                              </div>
                              <div class="box">
                                 <input id="alphabetically" name="sort_filter" type="radio" value="alpha">
                                 <label for="alphabetically">Alphabetically</label>
                              </div>
                           </div>
                           <div class="row form-group">
                              <div class="slec">
                                 <label for="select" class="form-control-label">  Course Type </label>
                              </div>
                              <div class="slecopt">
                                <form action="" method="post" name="theForm">
                                 <?php $action = $_GET["action"]; ?>
                                 <select name="selecta" id="select" class="form-control" onchange="mySubmit(this.form)">
                                    <option value=""> Select </option>
                                     <option value="school-level"> School Level </option>
                                     <option value="under-graduate"> Under Graduate </option>
                                     <option value="post-graduate"> Post Graduate </option>
                                 </select>
                                    <input type="hidden" name="eid" value="<?php echo $action;?>" id="geteid">
                              </div>
                              <div class="slec">
                                 <label for="select" class="form-control-label">  Field Type </label>
                              </div>
                              <div class="slecopt">
                                 <select name="fieldtype" id="selectfirst" class="form-control" onchange="mySubmit1(this.form)">
                                    <option value=""> Select </option>
                                    <option value="Engineering"> Engineering </option>
                                    <option value="Medical"> Medical </option>
                                    <option value="Law"> Law </option>
                                    <option value="Management"> Management </option>
                                    <option value="Others"> Others </option>
                                 </select>
                              </div>
                            </form>
                           </div>
                           <input name="csrfmiddlewaretoken" type="hidden" value="JLIYsrNZXI6q8JJOPkDyJBEpEPJ4k7ZaPks7QcXl1I27uviYpcgOb5RcJLpcijT9">
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane " id="tab2">
               <div class="conten2">
                  <div class="col-md-8 tabdatabga tabstep2">
                    <?php
                        if($_GET["action"]) {
                          $action = $_GET["action"];
                          $getExamDetails1 = $wpdb->get_results( 
                              $wpdb->prepare( "SELECT id,exam_title,purpose,eligibility,exam_date,exam_image FROM wp_exam_details WHERE exam_id = %d AND exam_of=2", $action)
                          );
                        } else {
                          $getExamDetails1 = $wpdb->get_results( "SELECT id,exam_title,purpose,eligibility,exam_date,exam_image FROM wp_exam_details WHERE exam_of=2");
                        }
                        foreach($getExamDetails1 as $getExamDetail1) {
                    ?>
                     <section class="examlistcolm ">
                        <div class="row">
                           <div class="col-md-2">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-image">
                                    <img width="45" height="45" src="<?php echo $getExamDetail1->exam_image; ?>" class="attachment-full size-full" alt="">
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-8">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-text-editor elementor-clearfix previouspapas">
                                    <p><a href="#"><?php echo $getExamDetail1->exam_title; ?></a>
                                      <br><strong>Purpose: </strong> <?php echo $getExamDetail1->purpose; ?>
                                      <br><strong>Eligibility:</strong> <?php echo $getExamDetail1->eligibility; ?>
                                      <br><strong>Exam Date: </strong> <?php echo date("d/m/Y", strtotime($getExamDetail1->exam_date)); ?>
                                   </p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-2">
                              <div class="elementor-column-wrap elementor-element-populated">
                                 <div class="elementor-button-wrapper">
                                    <a href="http://edukeeda.com/examdetails?action=<?php echo $getExamDetail1->id; ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                    <span class="elementor-button-content-wrapper">
                                    <span class="elementor-button-text">Details</span>
                                    </span>
                                    </a>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </section>
                     <?php
                          }
                      ?>
                  </div>
                  <div class="side">
                     <div class="advanceftr col-md-4">
                        <div class="sidebar sidebarBox">
                           <div class="sortFilter">Sort &amp; Filter <a class="clAll clearAll" href="<?php echo site_url();?>/list-of-competitive-examinations-details/">Clear All</a></div>
                           <form class="sortFilterForm" method="post">
                              <div class="filterBox examSort">
                                 <h3>Sort By</h3>
                                 <div class="box">
                                    <input checked="" id="popularity" name="sort_filter" type="radio" value="pop">
                                    <label for="popularity">Popularity</label>
                                 </div>
                                 <div class="box">
                                    <input id="alphabetically" name="sort_filter" type="radio" value="alpha">
                                    <label for="alphabetically">Alphabetically</label>
                                 </div>
                              </div>
                              <input name="csrfmiddlewaretoken" type="hidden" value="JLIYsrNZXI6q8JJOPkDyJBEpEPJ4k7ZaPks7QcXl1I27uviYpcgOb5RcJLpcijT9">
                           </form>
                           <div class="filterBox examLabel">
                              <h3>Exam Level</h3>
                              <form action="" method="post" name="theForm2">
                                <?php $action = $_GET["action"]; ?>
                                <input type="hidden" name="eid" value="<?php echo $action;?>" id="geteidd">
                                <select name="selecta" class="form-control" onchange="mySubmit2(this.form)">
                                   <option value=""> Select </option>
                                   <option value="post-graduate"> Post Graduate </option>
                                   <option value="under-graduate"> Under Graduate </option>
                                   <option value="1"> 1st </option>
                                   <option value="2"> 2nd </option>
                                   <option value="3"> 3rd </option>
                                   <option value="4"> 4th </option>
                                   <option value="5"> 5th </option>
                                   <option value="6"> 6th </option>
                                   <option value="7"> 7th </option>
                                   <option value="8"> 8th </option>
                                   <option value="9"> 9th </option>
                                   <option value="10"> 10th </option>
                                   <option value="11"> 11th </option>
                                   <option value="12"> 12th </option>
                                </select>
                              </form>
                              <!--<div class="box">
                                 <label class="control controlCheckbox">
                                    <input name="level" class="examlevel" type="checkbox" value="Senior Secondary">Senior Secondary
                                    <div class="controlIndicator"></div>
                                 </label>
                              </div>
                              <div class="box">
                                 <label class="control controlCheckbox">
                                    <input name="level" class="examlevel" type="checkbox" value="Higher Secondary">Higher Secondary
                                    <div class="controlIndicator"></div>
                                 </label>
                              </div>
                              <div class="box">
                                 <label class="control controlCheckbox">
                                    <input name="level" class="examlevel" type="checkbox" value="Diploma">Diploma
                                    <div class="controlIndicator"></div>
                                 </label>
                              </div>
                              <div class="box">
                                 <label class="control controlCheckbox">
                                    <input name="level" class="examlevel" type="checkbox" value="School">School
                                    <div class="controlIndicator"></div>
                                 </label>
                              </div>-->
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane " id="tab3">
               <div class="">
                  <div class="col-md-8 tabdatabga tabstep3">
                      <?php
                        if($_GET["action"]) {
                          $action = $_GET["action"];
                          $getExamDetails2 = $wpdb->get_results( 
                              $wpdb->prepare( "SELECT id,exam_title,purpose,eligibility,exam_date,exam_image FROM wp_exam_details WHERE exam_id = %d AND exam_of=3", $action)
                          );
                        } else{
                            $getExamDetails2 = $wpdb->get_results( "SELECT id,exam_title,purpose,eligibility,exam_date,exam_image FROM wp_exam_details WHERE exam_of=3");
                        }
                          
                          foreach($getExamDetails2 as $getExamDetail2) {
                      ?>
                       <section class="examlistcolm ">
                          <div class="row">
                             <div class="col-md-2">
                                <div class="elementor-column-wrap elementor-element-populated">
                                   <div class="elementor-image">
                                      <img width="45" height="45" src="<?php echo $getExamDetail2->exam_image; ?>" class="attachment-full size-full" alt="">
                                   </div>
                                </div>
                             </div>
                             <div class="col-md-8">
                                <div class="elementor-column-wrap elementor-element-populated">
                                   <div class="elementor-text-editor elementor-clearfix previouspapas">
                                      <p><a href="#"><?php echo $getExamDetail2->exam_title; ?></a>
                                        <br><strong>Purpose: </strong> <?php echo $getExamDetail2->purpose; ?>
                                        <br><strong>Eligibility:</strong> <?php echo $getExamDetail2->eligibility; ?>
                                        <br><strong>Exam Date: </strong> <?php echo date("d/m/Y", strtotime($getExamDetail2->exam_date));?>
                                     </p>
                                   </div>
                                </div>
                             </div>
                             <div class="col-md-2">
                                <div class="elementor-column-wrap elementor-element-populated">
                                   <div class="elementor-button-wrapper">
                                      <a href="http://edukeeda.com/examdetails?action=<?php echo $getExamDetail2->id; ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                      <span class="elementor-button-content-wrapper">
                                      <span class="elementor-button-text">Details</span>
                                      </span>
                                      </a>
                                   </div>
                                </div>
                             </div>
                          </div>
                       </section>
                     <?php
                          }
                     ?>
                  </div>
                  <div class="side">
                     <div class="advanceftr col-md-4">
                        <div class="sidebar sidebarBox">
                           <div class="sortFilter">Sort &amp; Filter <a class="clAll clearAll" href="/exams/">Clear All</a></div>
                           <form class="sortFilterForm" method="post">
                              <div class="filterBox examSort">
                                 <h3>Sort By</h3>
                                 <div class="box">
                                    <input checked="" id="popularity" name="sort_filter" type="radio" value="pop">
                                    <label for="popularity">Popularity</label>
                                 </div>
                                 <div class="box">
                                    <input id="alphabetically" name="sort_filter" type="radio" value="alpha">
                                    <label for="alphabetically">Alphabetically</label>
                                 </div>
                              </div>
                              <div class="row form-group">
                                 <div class="slec">
                                    <label for="select" class="form-control-label">  Education Eligibility </label>
                                 </div>
                                 <div class="slecopt">
                                    <form action="" method="post" name="theForm3">
                                       <?php $action = $_GET["action"]; ?>
                                     <select name="selectc" id="select2" class="form-control" onchange="mySubmit3(this.form)">
                                          <option value=" "> Select </option>
                                          <option value="school-level"> School Level </option>
                                          <option value="under-graduate"> Under Graduate </option>
                                          <option value="post-graduate"> Post Graduate </option>
                                     </select>
                                    <input type="hidden" name="eid" value="<?php echo $action;?>">
                                    <input type="hidden" name="step" value="step3">
                                 </form>
                                 </div>
                              </div>
                              <input name="csrfmiddlewaretoken" type="hidden" value="JLIYsrNZXI6q8JJOPkDyJBEpEPJ4k7ZaPks7QcXl1I27uviYpcgOb5RcJLpcijT9">
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'content', 'page' ); ?>
<?php
   // If comments are open or we have at least one comment, load up the comment template
   if ( comments_open() || get_comments_number() ) :
     comments_template();
   endif;
   ?>
<?php endwhile; // end of the loop. ?>
</main><!-- #main -->
</div>
<!-- #primary -->
</div>   
<?php get_footer(); ?>
</div>
