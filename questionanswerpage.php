<div class="homepagemenedu discussionback">
<?php
   /*
    Template Name: Question Anwers
    */
if($_GET['action']){
}else{
header('Location: https://edukeeda.com/discussion-forum/');
exit;
}
   get_header(); ?>
<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   include_once $path . '/wp-config.php';
   include_once $path . '/wp-load.php';
   include_once $path . '/wp-includes/wp-db.php';
   include_once $path . '/wp-includes/pluggable.php';
   
   global $wpdb;
   global $current_user;

  $bid = (int) $_GET["action"];
  $table_name = "wp_discussion_views";
  $checkViews = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM $table_name WHERE ques_id = %d", $bid) 
  );
  if($checkViews){
      $valView = $checkViews[0]->view_count + 1;
      $wpdb->update($table_name, array('view_count'=>$valView), array('ques_id'=>$bid));
?>
    <script type="text/javascript">
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
    </script>
<?php
  } else {
    $data = array("ques_id" =>$bid, "view_count"=>1);
    $wpdb->insert($table_name, $data);
?>
    <script type="text/javascript">
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
    </script>
<?php
  }
  
  if(isset($_POST["follow"])) {
       
       $quesId = $_POST["quesId"];
       $userid = $_SESSION["login"]["id"];
       $data = ["ques_id"=>$quesId, "user_id"=>$userid];
       $wpdb->insert("wp_discussion_ques_follow", $data);
?>
        <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
        </script>
<?php
   }
   if(isset($_POST["unfollow"])) {
       
       $quesId = $_POST["quesId"];
       $userid = $_SESSION["login"]["id"];
       $wpdb->query($wpdb->prepare("DELETE FROM wp_discussion_ques_follow WHERE ques_id = %d AND user_id=%d", $quesId, $userid));
?>
        <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
        </script>
<?php
   }

   if(isset($_POST["likesubmit"])) {
       $ansId = $_POST["ansId"];
       $userid = $_SESSION["login"]["id"];
       $data = ["user_id"=>$userid, "ans_id"=>$ansId];
       $wpdb->insert("wp_answer_like", $data);
?>
        <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
        </script>
<?php
   }
    if(isset($_POST['anspostsubmit'])) {
        $qid = $_POST['qid'];
        $userid = $_SESSION["login"]["id"];
        $ansPost = $_POST['ansPost111'];
$ansPost = stripslashes($ansPost);
        $data = ["ques_id"=>$qid, "user_id"=>$userid, "ans_content"=>$ansPost];
        $wpdb->insert("wp_discussion_ans", $data);
        $msgAns = "Answer posted successfully";
    }
    if(isset($msgAns)) {
?>
        <script type="text/javascript">
            alert("Answer posted successfully");
        </script>
<?php
        unset($msgAns);
?>
        <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
        </script>
<?php
    }

    if(isset($_POST['replypostsubmit'])) {
        $rid = $_POST['rid'];
        $userid = $_SESSION["login"]["id"];
        $replyPost = $_POST['replyPost'];
        $data = ["user_id"=>$userid, "ans_id"=>$rid, "reply"=>$replyPost];
        $wpdb->insert("wp_discussion_reply", $data);
        $msgReply = "Successfully Posted";
    }

    if(isset($msgReply)) { 
?>
        <script type="text/javascript">
            alert("Successfully Posted");
        </script>
<?php
        unset($msgReply);
?>
        <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
        </script>
<?php
    }

    if(isset($_POST['hidesubmit'])) {
    $qid = $_POST['qid'];
    $userid = $_SESSION["login"]["id"];
    $data = ["user_id"=>$userid, "quesid"=>$qid, "status"=>"hide"];
    $wpdb->insert("wp_question_hide", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['booksubmit'])) {
    $qid = $_POST['quesid'];
    $userid = $_SESSION["login"]["id"];
    $data = ["ques_id"=>$qid, "user_id"=>$userid];
    $wpdb->insert("wp_question_book", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['unbookmarksubmit'])) {
    $qid = $_POST['quesid'];
    $userid = $_SESSION["login"]["id"];
    $wpdb->query($wpdb->prepare("DELETE FROM wp_question_book WHERE ques_id = %d AND user_id=%d", $qid, $userid));
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['submitReport'])) {
    $qid = $_POST['qid'];
    $reason = $_POST['reason'];
    $userid = $_SESSION["login"]["id"];

    $data = ["user_id"=>$userid, "quesid"=>$qid, "status"=>"report", "reason"=>$reason];
    $wpdb->insert("wp_question_hide", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['anshidesubmit'])) {
    $aid = $_POST['aid'];
    $userid = $_SESSION["login"]["id"];
    $data = ["user_id"=>$userid, "ansid"=>$aid, "status"=>"hide"];
    $wpdb->insert("wp_answer_hide", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['anssubmitReport'])) {
    $aid = $_POST['aid'];
    $reason = $_POST['reason'];
    $userid = $_SESSION["login"]["id"];

    $data = ["user_id"=>$userid, "ansid"=>$aid, "status"=>"report", "reason"=>$reason];
    $wpdb->insert("wp_answer_hide", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['repostsubmit'])) {
      $quesid = $_POST['quesid'];
      $ques = $_POST['ques'];
      $reposttext = $_POST['reposttext'];
      $userid = $_SESSION["login"]["id"];
      $data = ["user_id"=>$userid, "content"=>$reposttext, "shareType"=>"ques", "shareid"=>$quesid, "sharetext"=>$ques]; 
      $wpdb->insert("wp_wallpost", $data);
?>
    <script type="text/javascript">
      alert("Successfully posted on your wall");
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }
  if(isset($_POST['repostanssubmit'])) {
      $ans = $_POST['ans'];
      $ansid = $_POST['ansid'];
      $reposttext = $_POST['reposttext'];
      $userid = $_SESSION["login"]["id"];
      $data1 = ["user_id"=>$userid, "content"=>$reposttext, "shareType"=>"ans", "shareid"=>$ansid, "sharetext"=>$ans];
      $wpdb->insert("wp_wallpost", $data1);
?>
    <script type="text/javascript">
      alert("Successfully posted on your wall");
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST["unlikesubmit11"])) {
        $ansid = $_POST['ansId'];
        $userid = $_SESSION["login"]["id"];
        $wpdb->query($wpdb->prepare("DELETE FROM wp_answer_like WHERE ans_id = %d AND user_id=%d", $ansid, $userid));
?>
      <script type="text/javascript">
        if(window.history.replaceState) {
            window.history.replaceState( null, null, window.location.href );
        }
     </script>
<?php
    }

    if(isset($_POST['ansdeletesubmit'])) {
        $aid = $_POST['aid'];
        $wpdb->query($wpdb->prepare("DELETE FROM wp_discussion_ans WHERE id = %d", $aid));
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }
?>
<style>
.direct a {
    color: #9a9a9a;
}
.col-md-12.blo {
    margin-bottom: 0px;
}
.col-md-12.blo.postpanels {
    margin-bottom: 0px;
  background: #e2e2e8;
}
   .imgforr {
   text-align: center;
   }
   .imgforr img.img-thumbnail {
   width: 60px;
   height: 60!important;;
   border-radius: 100%;
   }
   .post span {
   margin-right: 6px;
   }
   .post .dotsseprate {
   margin: 0 6px;
   }
   .plugin { 
   float: right;
   }
   .pp {
   float: left;
   width: 100%;
   display: -webkit-box;
   margin: 10px 0px;
   }
   .pp span {
   margin-right: 6px;
   }
   .pp strong {
 
   color: #373779;
   }
   .catp strong {
   color: #060658;
   font-weight: 600;
   }
   .catp span {
   margin: 5px;
   color: #4c4242;
   }
   .text {
   float: left;
   width: 100%;
   margin-bottom: 15px;
   margin-top: 15px;
   }
   .text p {
   font-weight: 800;
   color: #4c4242;
   line-height: 30px;
   font-size: 29px!important;
   }
   .share .fa {
   color: #060658;
   }
   .share1 .fa {
   color: #060658;
   }
   .share2 .fa {
   color: #060658;
   }
   #articleblous .share .fa {
   color: #9a9a9a;
   }
   .share2 {
   color: #9a9a9a;
   }
   .share2 .fa {
   color: #9a9a9a;
   }
   .reply {
   background: none;
   margin: 0px;
   padding: 0px;
   color: #060658;
   }
   .repost {
   background: none;
   margin: 0px;
   padding: 0px;
   color: #060658;
   }
   .repot p {
   color: #9a9a9a;
   }
   .dot {
   float: right;
   }
   .questio {
   float: left;
   width: 100%;
   margin-bottom: 20px;
   }
   .dot span.countno {
   margin: 0px;
   float: left;
   }
   .dot ul.dropdown-menu {
   min-width: 217px;
   padding: 3px;
   }
   .modal-body p {
   margin-bottom: 0px;
   padding: 3px;
   }
   .post{
   float: left;
   width: 100%;
   display: -webkit-box;
   margin-bottom: 20px;
   color:#9a9a9a;
   }
   .text1 {
   float: left;
   width: 100%;
   margin-bottom: 0px;
   }
   .pluging {
   float: right;
   }
   .plugin a {
   color: #9a9a9a!important;
   }
   .like {
   float: left;
   width: 93%;
   display: flex;
   }
   .like p {
   margin: 0 10px 0 0;
   }
   .join a {
   font-size: 16px;
   margin: 14px;
   padding: 5px;
   background: #f68e2f;
   color: #FFFFFF;
   border-radius: 5px;
   }
   #articleblous .share1 button {
   color: #9a9a9a;
   }
   .share1 button {
   color: #102358;
   font-size: 14px;
   text-transform: capitalize;
   background: transparent;
   border: none;
   padding: 0 6px;
   }
   .share1 img {
   width: 18px;
   height: auto;
   }  
   .text1 p {
   font-weight: 800;
   line-height: 30px;
   font-size: 20px!important;
   }
   .text1 h2 {
   font-size: 29px;
   color: #4c4242;
   }
   .house button {
   font-size: 12px;
   margin: 14px;
   padding: 5px;
   }
   .house {
   float: left;
   width: 100%;
   margin: 35px;
   margin-top: 50px;
   }
   .plug img {
   width: 20px;
   }
   .plug {
   text-align:right;}
   .cat a {
   color: #000000bd;
   font-weight: 600;
   }.catp a {
   color: #060658;
   font-weight: 600;
   }
   .cat span {
   color: #060658;
   margin: 7px;
   }
   .cat {
   margin: 6px 0px;
   }
   .headdiscus h1 {
   font-size: 25px;
   text-align: center;
   }
   .accordion {
   background-color: #eee;
   color: #444;
   cursor: pointer;
   padding: 18px;
   width: 100%;
   border: none;
   text-align: left;
   outline: none;
   font-size: 15px;
   transition: 0.4s;
   }
   .active, .accordion:hover {
   background-color: #ccc; 
   }
   .headarea .active, .accordion:hover {
   background-color: transparent;
   }
   .accordion:after { 
   content: ' \002B';
   font-size: 21px;
   margin: 1px;
   padding: 0px;
   }
   .panel {
   padding: 0 18px;
   display: none;
   background-color: white;
   overflow: hidden;
   }
   .headarea button.accordion {
   background: none;
   border: none;
   color: #616161;
   margin: 0px;
   padding: 0px;
   font-weight: 500;
   text-transform: capitalize;
   }
   .join a:hover {
   font-size: 16px;
   margin: 14px;
   padding: 5px;
   border-radius: 5px;
   color: #f68e2f;
   border-color: #f68e2f;
   border: 1px solid #f68e2f;
   background: transparent;
   }
   .moreartc {
   text-transform: capitalize;
   font-weight: 600;
   cursor: pointer;
   float: left;
   position: absolute;
   right: 0;
   bottom: -5px;
   background: #fff;
   padding: 0 51% 0 10px;
   }
   .para {
   float: left;
   width: 100%;
   margin-bottom: 20px;
   position: relative;
   }
   .para p {
   overflow: hidden;
   text-overflow: ellipsis;
   display: -webkit-box;
   max-height: 80px;
   -webkit-line-clamp: 3;
   -webkit-box-orient: vertical;
   margin: 0;
   }
   .postpanels ul li:first-child {
   border-bottom: 1px solid #ddd;
   }
   .reportingsets ul li {
   float: left;
   width: 100%;
   text-align: left;
   }
   .reportingsets ul {
   list-style-type: circle;
   }
   .reportingsets ul li span {
   float: left;
   }
   .reportingsets {
   width: 100%;
   border-bottom: 1px solid #ddd;
   padding: 20px 0;
   margin: 0 0 15px 0;
   }
   .reportingsets .modal-footer{
   border-top:none;
   }
   .bottom-btns {
   padding: 0 21px;
   text-align: right;
   }
   .repolinks label {
   color: #797977;
   cursor: pointer;
   }
   .repolinks label:hover {
   color: #de7514;
   }
   .reportingsets input[type="radio"] {
   margin: 0 8px 0 0;
   }
   .groupname p{margin:0;}
   .text2 {
   float: left;
   width: 100%;
   }
   .discussionback .headarea {
   background: #fff;
   padding: 10px;
   }
   .active, .accordion:hover {
   background-color: #ccc; 
   }
   .headarea .active, .accordion:hover {
   background-color: transparent;
   }
   .active:after {
   content: "\2212";
   }
   .accordion:after { 
   content: ' \002B';
   font-size: 21px;
   margin: 1px;
   padding: 0px;
   float: right;
   }
   .p-3 .btn-primary {
   color: #fff;
   background-color: #f68e2f;
   border-color: #f68e2f;
   }
   .direct p {
   color:#9a9a9a;
   } 
   #articleblous .pp
   {
   color:#9a9a9a;
   }
   #articleblous .pp strong
   {
   color:#9a9a9a;
   }
   #articleblous .pp span
   {
   color:#9a9a9a;
   }
   #articleblous .share
   {
   color:#9a9a9a;
   }
   #articleblous .repot{
   color:#9a9a9a;
   }
   #articleblous .reply{
   color:#9a9a9a;
   }
   #articleblous .repot .fa{
   color:#9a9a9a;
   }
   #articleblous .reply .fa{
   color:#9a9a9a;
   }
   #articleblous .share .fa{
   color:#9a9a9a;
   }
   #articleblous .share button{
   color:#9a9a9a;
   }
   @media only screen and (max-width: 767px)
   {
   .memberpage {
   margin-top: 0px; 
   }
   .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
}
.page-wrap {
    padding: 0px;
}
   .text1 p {
   font-size: 15px!important;
   line-height: 20px;
   }
   .direct p {
   font-size: 14px!important;
   }
   .post p {
   font-size: 12px!important;
   }
   .share1 {
   text-align: center;
   }
   .share2 {
   text-align: center;
   line-height: 14px;
   }
   .post .dotsseprate {
   margin: 0 2px;
   }
   .post span {
   margin-right: 2px;
   }
   .plugin {
   float: right;
   margin-top: -50px;
   }
   .dot ul.dropdown-menu {
   min-width: 160px;
   padding: 3px;
   width: 100%;
   margin-left: 0px;
   }
   p.rigwd {
   float: left;
   width: 100%;
   font-size:12px!important;
   margin-left: -25px;
   }
   p.catp {
   float: left;
   width: 100%;
   font-size:12px!important;
   margin-left: -25px;
   }
   }
   .repostmodal {
        border: 1px solid #C0C0C0;
        padding: 10px;
    }
    .bookmarkques button{padding: 0px;background: none;border:0px;color: #9a9a9a;}
</style>
<div class="memberpage">
   <div class="row">
      <div class="col-md-9">
         <div class="col-xs-12 col-md-4">
            <div class="headarea">
               <div class="other">
                  <h4>Discussion Group</h4>
               </div>
               <div class="headarea">
                  <div class="addposituon bottomspc">
                    <p class="tablinks"><a href="<?php echo site_url();?>/opinion-poll/">Opinion Poll</a><span class="countno"></span></p>
                        <?php
                           $getSidebarCats = $wpdb->get_results("SELECT * FROM wp_discussion_main");
                           
                           foreach($getSidebarCats as $getSidebarCat) {
                              if($getSidebarCat->have_sub == 0) {
                            ?>
                                <p class=""><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>"><?php echo $getSidebarCat->name;?> <span class="countno"></span></a></p>
                        <?php
                              } else {
                        ?>
                        <div class="spacetp">
                           <button class="accordion"><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>"><?php echo $getSidebarCat->name;?></a></button>
                           <div class="panel">
                              <?php
                                 $getSubCats = $wpdb->get_results( 
                                     $wpdb->prepare( "SELECT * FROM wp_discussion_sub WHERE main_id = %d",$getSidebarCat->id) 
                                 );
                                 foreach($getSubCats as $getSubCat) {
                                 ?>
                                    <p class="tablinks"><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>actions=<?php echo $getSubCat->id; ?>"><?php echo $getSubCat->sub_name;?><span class="countno"></span></a></p>
                              <?php
                                 }
                              ?>
                           </div>
                        </div>
                        <?php
                           }
                           }
                        ?>
                        <?php
                            if(isset($_SESSION["login"]["id"]) || $current_user->ID == 1) {
                        ?>
                             <p class="tablinks"><a href="<?php echo site_url();?>/peoples-wallposts"> People's Wallposts </a><span class="countno"></span></p>
                             <p class="tablinks"><a href="<?php echo site_url();?>/my-discussion-house">My Discussion Houses</a><span class="countno"></span></p>
                              <p class="tablinks"><a href="<?php echo site_url();?>/my-discussion-group">My Groups</a><span class="countno"></span></p>
                        <?php
                          }
                        ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-8">
            <div class="arteduro">
               <div class="row">
                  <div class="col-md-12 col-sm-12 basictabsed">
                     <div id="articleblous" class="tabcontent" style="display: block;">
                        <div class="blogaresnews">
                           <?php
                              if($_GET["action"]) {
                                  $quesId = $_GET["action"];
                              }
                              
                              $quesPosts = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ques WHERE id=%d", $quesId));
                              
                              foreach($quesPosts as $quesPost) {
                                
                                if($quesPost->sub_cat_id == 0) {
                                    $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost->main_id));    
                                    $nameCat = $catName[0]->name;
                                } else {
                                    $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost->main_id));
                                    $catName1 = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $quesPost->sub_cat_id));

                                    $nameCat = $catName1[0]->sub_name;
                                    $nameCat1 = $catName[0]->name;
                                }
                                
                                $houseName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE id = %d", $quesPost->house_id));
                                
                                $viewsCount = $wpdb->get_results( 
                                    $wpdb->prepare("SELECT * FROM wp_discussion_views WHERE ques_id = %d", $quesPost->id));
                              
                                $ansCount = $wpdb->get_results( 
                                    $wpdb->prepare("SELECT COUNT(id) as countAns FROM wp_discussion_ans WHERE ques_id = %d", $quesPost->id));

                                $ansTime = $wpdb->get_results( 
                                    $wpdb->prepare("SELECT created_at FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $quesPost->id));
                            
                                $quesFollow = $wpdb->get_results( 
                                    $wpdb->prepare("SELECT COUNT(id) as countFollowed FROM wp_discussion_ques_follow WHERE ques_id = %d", $quesPost->id));
                                
                                if($ansTime) {
                                  $timeAnsGet = "<strong>Answered </strong> " . time_elapsed_string($ansTime[0]->created_at);
                                } else {
                                  $timeAnsGet = "<strong>Posted </strong> " . time_elapsed_string($quesPost->created_at);
                                }

                                $countqRepost = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as cqRepost FROM wp_wallpost WHERE shareType='ques' AND shareid=%d", $quesPost->id));
                                
                           ?>
                           <div class="col-md-12 blo postpanels">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">

                                    <p> <?php if($quesPost->sub_cat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$quesPost->main_id."'>".$nameCat1 .'</a> . ';}?> <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $quesPost->main_id;?><?php if($quesPost->sub_cat_id == 0){}else { echo "&actions=". $quesPost->sub_cat_id;}?>"><?php echo $nameCat; ?></a> . <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $quesPost->house_id;?>"> <?php if(strlen($houseName[0]->title) > 35){echo substr($houseName[0]->title,0,35). '...';}else{echo $houseName[0]->title;} ?> </a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                 <div class="dot">
                                    <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...
                                       </p>
                                        <ul class="dropdown-menu">
                                        <?php
                                          if($_SESSION['login']['id']) {
                                        ?>
                                           <li data-toggle="modal" data-target="#myModalreport<?php echo $z; ?>">Report This Post</li>
                                        <?php
                                           } else{
                                        ?>
                                             <li data-toggle="modal" data-target="#myModalreportnotq">Report This Post</li>
                                        <?php
                                           }
                                        ?>
                                        </ul>
                                             <div id="myModalreportnotq" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                       <!-- Modal content-->
                                                       <div class="modal-content">
                                                          <div class="modal-header">
                                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                             <h1>Report this post</h1>
                                                          </div>
                                                          <div class="modal-body">
                                                             <p> Please Login and Register first! </p>
                                                          </div>
                                                       </div>
                                                    </div>
                                                </div> 
                                              <div class="modal fade" id="myModalreport<?php echo $z; ?>" role="dialog">
                                                 <div class="modal-dialog vertical-align-center">
                                                    <div class="modal-content">
                                                       <div class="repocontent modal-body reportingsets" id="gridone1" style="display:block;">
                                                          <ul>
                                                             <li>
                                                                <span  class="countno repolinks" onclick="openReport(event, 'gridtwo1')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it's spam, Promotional</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridthree1')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think  it's objectionable</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridfour1')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it false or misinformation</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                          </ul>
                                                          <div class="modal-footer">
                                                          </div>
                                                       </div>

                                                       <div class="repocontent modal-body reportingsets" id="gridtwo1" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                             <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                          </div>
                                                          <div class="modal-footer">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone1')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       
                                                       <div class="repocontent modal-body reportingsets" id="gridthree1" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent" checked="checked">It's pornographic or extremely violent.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                             </div>
                                                          </div>
                                                          <div class="bottom-btns">
                                                            <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone1')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       <div class="repocontent modal-body reportingsets" id="gridfour1" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                          </div>
                                                          <div class="bottom-btns">
                                                             <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone1')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="text1">
                                    <p><?php echo $quesPost->ques; ?></p>
                                 </div>
                              </div>
                              <div class="questio">
                                 <div class="col-md-12 col-sm-12">
                                    <p class="rigwds"> <?php echo $timeAnsGet; ?></p>
                                 </div>
                              </div>
                              <div class="col-xs-9 col-md-9">
                                 <div class="post">
                                    <p><span><?php echo $ansCount[0]->countAns; ?></span><strong>Answers</strong>
                                    <div class="dotsseprate">.</div>
                                    </p>

                                    <!--<p data-toggle="modal" data-target="#myModalrepostlistaaa"><span><?php //if($countqRepost[0]->cqRepost == 0){ echo "0";} else {echo $countqRepost[0]->cqRepost;}?></span><strong>Repost</strong>
                                    <div class="dotsseprate">.</div>
                                    </p>-->
                                    
                                    <p><span><?php if($viewsCount){ echo $viewsCount[0]->view_count;}else{ echo "0";} ?></span><strong>Views</strong>
                                    <div class="dotsseprate">.</div>
                                    </p>
                                    <p data-toggle="modal" data-target="#myModalfollowlistaaa"><span> <?php if($quesFollow){ echo $quesFollow[0]->countFollowed; }else{ echo "0";} ?> </span><strong>Followers</strong></p>
                                 </div>

                                             <!---- Modal  --->
                                              <div id="myModalrepostlistaaa" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       <h3>People who have shared in their wall</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                          <?php
                                                              $reLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ques' AND shareid=%d", $quesPost->id));
                                                              if($reLists) {
                                                                  foreach($reLists as $reList) {
                                                                      $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $reList->user_id));
                                                              
                                                                      $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $reList->user_id));
                                                              
                                                                      $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $reList->user_id));

                                                                      $userAdd = $wpdb->get_results(
                                                                          $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $result->ID));
                                                                        $userCountry = $wpdb->get_results(
                                                                          $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
                                                                        $userState = $wpdb->get_results(
                                                                            $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
                                                          ?>   
                                                             <div class="col-md-6">
                                                              <div class="poll">
                                                                 <div class="col-md-3 col-sm-2 col-xs-3">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="col-md-9 col-sm-10 col-xs-9">
                                                                    <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                    <p class="rigwd"> <strong> <?php if($userAdd[0]->city){echo $userAdd[0]->city; echo ",";} ?> <?php if($userState[0]->name){echo $userState[0]->name; echo ",";} ?> <?php echo $userCountry[0]->name; ?></strong> </p>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                          <?php
                                                                  }
                                                              }
                                                          ?>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>


                                              <!---- Modal  --->
                                              <div id="myModalfollowlistaaa" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                          <?php
                                                              $foLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ques_follow WHERE ques_id=%d", $quesPost->id));
                                                              if($foLists) {
                                                                  foreach($foLists as $foList) {
                                                                    $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $foList->user_id));
                                                              
                                                                    $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $foList->user_id));
                                                              
                                                                    $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $foList->user_id));
                                                                    
                                                                    $userAdd = $wpdb->get_results(
                                                                          $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $foList->user_id));
                                                                        $userCountry = $wpdb->get_results(
                                                                          $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
                                                                        $userState = $wpdb->get_results(
                                                                            $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
                                                          ?>   
                                                             <div class="col-md-6">
                                                              <div class="poll">
                                                                 <div class="col-md-3 col-sm-2 col-xs-3">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="col-md-9 col-sm-10 col-xs-9">
                                                                    <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                    <p class="rigwd"> <strong> <?php if($userAdd[0]->city){echo $userAdd[0]->city; echo ",";} ?> <?php if($userState[0]->name){echo $userState[0]->name; echo ",";} ?> <?php echo $userCountry[0]->name; ?></strong> </p>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                          <?php
                                                                  }
                                                              }
                                                          ?>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                              </div>
                              <div class="col-xs-3 col-md-3">
                                 <div class="plugin bookmarkques">
                                    <div class="dropdown customs">      
                                      <button class="dropbtn">
                                        <i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>
                                      </button>
                                      <div class="dropdown-content">
                                        <?php $urlshare = "http://edukeeda.com/questionanswers/?action=".$quesPost->id;
                                          $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                        ?>
                                          <?php echo do_shortcode("$urlsharing"); ?>
                                      </div>
                                    </div>
                                    <?php
                                       if(isset($_SESSION["login"])) {
                                          $userId = $_SESSION["login"]["id"];
                             
                                          $checkBook1 = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_question_book WHERE user_id=%d AND ques_id=%d", $userId, $quesPost->id));
                                          if($checkBook1){
                                    ?> 
                                             <form method="post" action="">
                                                 <input type="hidden" name="quesid" value="<?php echo $quesId;?>">
                                                 <button type="submit" name="unbookmarksubmit"><img src="http://edukeeda.com/wp-content/uploads/2019/02/bookmarked1.png" style="width:10px;"></button>
                                             </form>
                                    <?php
                                          } else{
                                    ?>
                                          <form method="post" action="">
                                              <input type="hidden" name="quesid" value="<?php echo $quesId;?>">
                                              <button type="submit" name="booksubmit"><img src="/img/bookmark.png" style="width: 17px;margin:4px 10px;"></button>
                                          </form>    
                                    
                                    <?php
                                          }
                                       } else{
                                    ?>
                                        <a href="#" data-toggle="modal" data-target="#myModalbookLogin"><img src="/img/bookmark.png" style="width: 17px;margin:4px 10px;"></a>
                                    <?php
                                      }
                                    ?>
                                 </div>
                              </div>
                              <div class="col-xs-4 col-md-4">
                                 <div class="share">
                                    <?php
                                       if(isset($_SESSION["login"])) {
                                    ?>
                                            <p><button data-toggle="modal" data-target="#myModalAnsLogin" style="background-color: #e2e2e8;border: 0px;padding: 0px;color: #9a9a9a!important;"><i class="fa fa-reply" aria-hidden="true"></i>Answer</button></p>
                                    <?php
                                       } else{
                                    ?>
                                            <p><button type="button" data-toggle="modal" data-target="#myModalAns" style="background-color: #e2e2e8;border: 0px;padding: 0px;color: #9a9a9a;"><i class="fa fa-reply" aria-hidden="true"></i>Answer</button></p>
                                    <?php
                                       }
                                    ?>

                                    <!-- Modal -->
<div id="myModalAnsLogin" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Answer</h4>
         </div>
         <div class="modal-body">
            <form method="post" action="">
               <div>
                  <?php
                    $my_content3 =" ";
                    $editor_id3 = 'ansPost111';
                    $option_name3 ='ansPost111';
                    wp_editor($my_content3, $editor_id3, array('wpautop'=> false,'textarea_name' => $option_name3, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> false));
                    ?>
                  <input type="hidden" name="qid" value="<?php echo $quesId;?>">
               </div>
               <div>
                  <input type="submit" name="anspostsubmit" value="Submit" class="btn btn-warning">
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
                                 </div>
                              </div>

                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                    <?php
                                       if(isset($_SESSION["login"])) {
                                         $userId = $_SESSION["login"]["id"];
                                         
                                         $checkFollow = $wpdb->get_results( 
                                           $wpdb->prepare( "SELECT * FROM wp_discussion_ques_follow WHERE user_id=%d AND ques_id=%d", $userId, $quesId));
                                         if($checkFollow){
                                       ?>
                                            <form method="post" action="">
                                               <input type="hidden" name="quesId" value="<?php echo $quesId;?>">
                                               <img src="/img/follow-icon-selected.png"><button type="submit" name="unfollow">follow </button>
                                            </form>
                                    <?php
                                         } else{
                                    ?>
                                            <form method="post" action="">
                                               <input type="hidden" name="quesId" value="<?php echo $quesId;?>">
                                               <img src="/img/Follow.png"><button type="submit" name="follow"> Follow </button>
                                            </form>
                                    <?php 
                                         }
                                        } else {
                                    ?>
                                            <img src="/img/Follow.png"><button types="button" data-toggle="modal" data-target="#myModalques">Follow</button>
                                    <?php   
                                       }
                                    ?>
                                 </div>
                              </div>
                              <div class="col-xs-4 col-md-4">
                                 <div class="share2">
                                    <?php
                                        if(isset($_SESSION["login"])) {
                                          $userId = $_SESSION["login"]["id"];
                                    ?>
                                          <!--<p data-toggle="modal" data-target="#myModalrepost1"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                    <?php
                                        } else{
                                    ?>
                                          <!--<p data-toggle="modal" data-target="#myModalrepost"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                    <?php
                                        }
                                    ?>
                                 </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>

                                          <!-- Modal -->
                                          <div id="myModalrepost1" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title"> Post this on your wall </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                    <form method="post" action="">
                                                      <p> <textarea name="reposttext"> </textarea></p>
                                                      <div class="repostmodal">
                                                         <?php
                                                            $userPic11 = $wpdb->get_results( 
                                                              $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$quesPost->user_id));
                                                            $userName11 = $wpdb->get_results( 
                                                              $wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$quesPost->user_id));
                                                            $userHeadline11 = $wpdb->get_results( 
                                                             $wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$quesPost->user_id));

                                                            $userAdd11 = $wpdb->get_results(
                                                              $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $quesPost->user_id));
                                                            $userCountry11 = $wpdb->get_results(
                                                              $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd11[0]->country));
                                                            $userState11 = $wpdb->get_results(
                                                                $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd11[0]->state));
                                                          ?>
                                                          <div class="col-md-2 col-sm-2 col-xs-4">
                                                            <div class="imgforr">
                                                               <a href="#"><img src="<?php echo $userPic11[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                            </div>
                                                         </div>
                                                             <div class="col-md-10 col-sm-10 col-xs-8">
                                                               <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $quesPost->user_id;?>"><?php echo $userName11[0]->name;?></a></strong>, <span><?php if($userHeadline11[0]->headline) {echo ', '. substr($userHeadline11[0]->headline,0,15).'..';}?></span></p>
                                                               <p class="rigwd"> <strong> <?php if($userAdd11[0]->city){echo $userAdd11[0]->city; echo ",";} ?> <?php if($userState11[0]->name){echo $userState11[0]->name; echo ",";} ?> <?php echo $userCountry11[0]->name; ?></strong> </p>
                                                            </div>
                                                          <p> Repost this question </p>
                                                          <p> Question :  <?php echo $quesPost->ques; ?> </p>
                                                      </div>
                                                      <input type="hidden" name="quesid" value="<?php echo $quesId;?>">
                                                      <input type="hidden" name="ques" value="<?php echo $quesPost->ques;?>">
                                                      <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                    </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                              </div>
                           </div>
                           <?php
                              }
                           ?>


                           <?php
                              $ansPosts = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ans WHERE ques_id=%d ORDER BY id DESC", $quesId));
                              
                              if($ansPosts) {
                                $z=1;
                               foreach($ansPosts as $ansPost) {
                                 
                                 $ansDate = $ansPost->created_at;
                                 $ansDate = date("F jS, Y g:i a", strtotime($ansDate));
                              
                                 $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $ansPost->user_id));
                                 $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $ansPost->user_id));
                                 $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $ansPost->user_id));

                                 $countLike = $wpdb->get_results( 
                                     $wpdb->prepare("SELECT COUNT(id) as cLike FROM wp_answer_like WHERE ans_id=%d", $ansPost->id));

                                 $countReply = $wpdb->get_results( 
                                     $wpdb->prepare("SELECT COUNT(id) as cReply FROM wp_discussion_reply WHERE ans_id=%d",$ansPost->id));

                                 $typeans = "ans";
                                 $countRepost = $wpdb->get_results( 
                                     $wpdb->prepare("SELECT COUNT(id) as cRepost FROM wp_wallpost WHERE shareType='ans' AND shareid=%d", $ansPost->id, $typeans));
                              ?>

                            <div class="col-md-12 blo">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="dot">
                                   <div class="dropdown">
                                      <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...
                                      </p>
                                      <ul class="dropdown-menu">
                                       <?php
                                          if($_SESSION['login']['id']) {
                                        ?>
                                             <li data-toggle="modal" data-target="#myModalreport1<?php echo $z; ?>">Report This Post</li>
                                       <?php
                                          } else{
                                       ?>
                                             <li data-toggle="modal" data-target="#myModalreportnot<?php echo $z; ?>">Report This Post</li>
                                       <?php
                                          }
                                       ?>

                                       <?php
                                          $userid11 = $_SESSION["login"]["id"];
                                          if($userid11 == $ansPost->user_id || $current_user->ID == 1) {
                                       ?>
                                          <li data-toggle="modal" data-target="#myModaldelete1<?php echo $z; ?>">Delete</li>
                                       <?php
                                          }
                                       ?>
                                      </ul>
                                   </div>

                                       <div id="myModaldelete1<?php echo $z; ?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h1>Delete this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to Delete this post? </p>
                                                     <form method="post" action="">
                                                       <input type="hidden" name="aid" value="<?php echo $ansPost->id;?>">
                                                       <input type="submit" name="ansdeletesubmit" value="Delete">
                                                     </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>

                                       <div id="myModalreportnot<?php echo $z; ?>" class="modal fade" role="dialog">
                                           <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                 <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h1>Report this post</h1>
                                                 </div>
                                                 <div class="modal-body">
                                                    <p> Please Login and Register first! </p>
                                                 </div>
                                              </div>
                                           </div>
                                       </div>

                                        <div class="modal fade" id="myModalreport1<?php echo $z; ?>" role="dialog">
                                         <div class="modal-dialog vertical-align-center">
                                            <div class="modal-content">
                                               <div class="repocontent modal-body reportingsets" id="gridone<?php echo $z.$z; ?>" style="display:block;">
                                                  <ul>
                                                     <li>
                                                        <span  class="countno repolinks" onclick="openReport(event, 'gridtwo<?php echo $z.$z; ?>')">
                                                           <div class="funkyradio-default">
                                                              <label for="checkbox1">I think it's spam, Promotional</label>
                                                           </div>
                                                        </span>
                                                     </li>
                                                     <li>
                                                        <span class="countno repolinks" onclick="openReport(event, 'gridthree<?php echo $z.$z; ?>')">
                                                           <div class="funkyradio-default">
                                                              <label for="checkbox1">I think  it's objectionable</label>
                                                           </div>
                                                        </span>
                                                     </li>
                                                     <li>
                                                        <span class="countno repolinks" onclick="openReport(event, 'gridfour<?php echo $z.$z; ?>')">
                                                           <div class="funkyradio-default">
                                                              <label for="checkbox1">I think it false or misinformation</label>
                                                           </div>
                                                        </span>
                                                     </li>
                                                  </ul>
                                                  <div class="modal-footer">
                                                  </div>
                                               </div>

                                               <div class="repocontent modal-body reportingsets" id="gridtwo<?php echo $z.$z; ?>" style="display:none;">
                                                  <form method="post" action="">
                                                  <div class="modal-body">
                                                     <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                     <input type="hidden" name="aid" value="<?php echo $ansPost->id;?>">
                                                  </div>
                                                  <div class="modal-footer">
                                                     <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z; ?>')">Back</button>
                                                     <button type="submit" name="anssubmitReport" class="btn btn-default">Submit</button>
                                                  </div>
                                                  </form>
                                               </div>
                                               
                                               <div class="repocontent modal-body reportingsets" id="gridthree<?php echo $z.$z; ?>" style="display:none;">
                                                  <form method="post" action="">
                                                  <div class="modal-body">
                                                     <div class="funkyradio-default">
                                                        <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent" checked="checked">It's pornographic or extremely violent.</label>
                                                     </div>
                                                     <div class="funkyradio-default">
                                                        <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                     </div>
                                                     <div class="funkyradio-default">
                                                        <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                     </div>
                                                  </div>
                                                  <div class="bottom-btns">
                                                    <input type="hidden" name="aid" value="<?php echo $ansPost->id;?>">
                                                     <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z; ?>')">Back</button>
                                                     <button type="submit" name="anssubmitReport" class="btn btn-default">Submit</button>
                                                  </div>
                                                  </form>
                                               </div>
                                               <div class="repocontent modal-body reportingsets" id="gridfour<?php echo $z.$z; ?>" style="display:none;">
                                                  <form method="post" action="">
                                                  <div class="modal-body">
                                                     <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                  </div>
                                                  <div class="bottom-btns">
                                                     <input type="hidden" name="aid" value="<?php echo $ansPost->id;?>">
                                                     <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z; ?>')">Back</button>
                                                     <button type="submit" name="anssubmitReport" class="btn btn-default">Submit</button>
                                                  </div>
                                                  </form>
                                               </div>
                                            </div>
                                         </div>
                                      </div>

                                </div>
                             </div>
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2 col-xs-4">
                                    <a href="#">
                                    <img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10 col-xs-8">
                                 <p class="catp"><strong><a href="#"> <?php echo $ansAuthor[0]->name; ?> </a></strong> <span> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,25).'..';}?></span>
                                 </p>
                                 <p class="rigwd"><strong> <?php echo $ansDate; ?> </strong></p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p>

                                       <span><?php if(strlen($ansPost->ans_content) > 180){echo substr($ansPost->ans_content, 0,180). '';} else{echo $ansPost->ans_content;} ?></span>

                                                 <?php if(strlen($ansPost->ans_content) > 180){ ?>
                                                    <span class="anscomplete<?php echo $z;?>" style="display:none;"><?php echo substr($ansPost->ans_content, 180,300); ?></span>
                                                    
                                                    <span class="ansmore<?php echo $z;?>" id="<?php echo $z;?>" onClick="moreFunction(this.id)" style="color: #f68e2f;">more...</span>
                                                 <?php } ?>
                                               
                                    </p>
                                 </div>
                              </div>
                              <div class="col-xs-12 col-md-12">
                                 <div class="pp">
                                    <p><span><?php if($countLike){echo $countLike[0]->cLike;}else{echo "0";} ?></span><strong>Likes</strong><div class="dotsseprate">.</div>
                                    </p>
                                    <p data-toggle="modal" data-target="#myModalreply<?php echo $z;?>"><span><?php if($countReply){echo $countReply[0]->cReply;}else{echo "0";} ?></span><strong>Reply</strong><div class="dotsseprate">.</div>
                                    </p>
                                    
                                    <!--<p data-toggle="modal" data-target="#myModalrepostlist<?php //echo $z;?>"><span> <?php //if($countRepost[0]->cRepost != 0){ echo $countRepost[0]->cRepost;} else{echo 0;}?> </span><strong>Repost</strong>
                                    </p>-->
                                 </div>
                              </div>
                                            <!---- Modal  --->
                                            <div id="myModalreply<?php echo $z;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                               <!-- Modal content-->
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                     <h3> Reply </h3>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row">
                                                        <div clss="col-md-12">
                                                        <?php
                                                          $ansReplys = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_reply WHERE ans_id=%d", $ansPost->id));
                                                          if($ansReplys) {

                                                              foreach($ansReplys as $ansReply) {
                                                                $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $ansReply->user_id));
                                                            
                                                                $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $ansReply->user_id));
                                                            
                                                                $ansAuthorPic1 = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $ansReply->user_id));
                                                        ?>   
                                                           <div class="col-md-12">
                                                              <div class="poll">
                                                                 <div class="left">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $ansAuthorPic1[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="right">
                                                                    <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallReply->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong></p>
                                                                 </div>
                                                                 <div class="custext">
                                                                    <div class="text2">
                                                                       <p> <?php echo $ansReply->reply; ?></p>
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        <?php
                                                                }
                                                            }
                                                        ?>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                            </div>

                                            <!---- Modal  --->
                                            <div id="myModalrepostlist<?php echo $z;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog" style="width: 700px;">
                                               <!-- Modal content-->
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                     <h3>People who have shared in their wall</h3>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row">
                                                        <div clss="col-md-12">
                                                        <?php
                                                            $wallRes = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ans' AND shareid=%d", $ansPost->id));
                                                            if($wallRes) {
                                                                foreach($wallRes as $wallRe) {
                                                                if($wallRe->shareid != 0) {
                                                                    $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $wallRe->user_id));
                                                            
                                                                    $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $wallRe->user_id));
                                                            
                                                                    $ansAuthorPic2 = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $wallRe->user_id));
                                                                    $userAdd11 = $wpdb->get_results(
                                                                       $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $quesPost->user_id));
                                                                     $userCountry11 = $wpdb->get_results(
                                                                       $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd11[0]->country));
                                                                     $userState11 = $wpdb->get_results(
                                                                         $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd11[0]->state));
                                                        ?>   
                                                           <div class="col-md-6">
                                                            <div class="poll">
                                                               <div class="col-md-3 col-sm-2 col-xs-3">
                                                                  <div class="imgforr">
                                                                     <a href="#"><img src="<?php echo $ansAuthorPic2[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-9 col-sm-10 col-xs-9">
                                                                  <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                  <p class="rigwd"> <strong> <?php if($userAdd11[0]->city){echo $userAdd11[0]->city; echo ",";} ?> <?php if($userState11[0]->name){echo $userState11[0]->name; echo ",";} ?> <?php echo $userCountry11[0]->name; ?></strong> </p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                        <?php
                                                                }
                                                                }
                                                            }
                                                        ?>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                            </div>

                              <div class="col-xs-3 col-md-4">
                                 <div class="share">
                                    <p>
                                    <?php
                                      if(isset($_SESSION["login"])) {
                                        $userId = $_SESSION["login"]["id"];
                                      
                                        $checkLiked = $wpdb->get_results( 
                                        $wpdb->prepare( "SELECT * FROM wp_answer_like WHERE user_id=%d AND ans_id=%d", $userId, $ansPost->id));

                                        if($checkLiked) {
                                    ?>
               
                                            <div class="shares">
                                               <form method="post" action="">
                                                  <input type="hidden" name="ansId" value="<?php echo $ansPost->id;?>">
                                                  <button type="submit" name="unlikesubmit11" class="unlikecolor" style="background-color: #fff;border: 0px;padding: 5px;"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Like</button>
                                               </form>
                                            </div>
                                    <?php
                                      } else{
                                    ?>
                                            <div class="shares">
                                               <form method="post" action="">
                                                  <input type="hidden" name="ansId" value="<?php echo $ansPost->id;?>">
                                                  <button type="submit" name="likesubmit" style="background-color: #fff;border: 0px;padding: 5px;"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                               </form>
                                            </div>
                                    <?php
                                         }
                                       } else {
                                    ?>
                                            <button type="button" style="background-color: #fff;border: 0px;padding: 0px;color: #060658;" data-toggle="modal" data-target="#myModalLike"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                    <?php
                                       }
                                    ?>
                                    </p> 
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="reply">
                                    <?php
                                      if(isset($_SESSION["login"])) {
                                    ?>
                                            <p data-toggle="modal" data-target="#myModalrep<?php echo $z.$z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                    <?php
                                      } else {
                                    ?>
                                            <p data-toggle="modal" data-target="#myModalrep<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                    <?php
                                      }
                                    ?>
                                 </div>
                                        <!-- Modal -->
                                        <div id="myModalrep<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Reply</h4>
                                              </div>
                                              <div class="modal-body">
                                                <p> For reply on answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <!-- Modal -->
                                        <div id="myModalrep<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Reply</h4>
                                              </div>
                                              <div class="modal-body">
                                                <form method="post" action="">
                                                  <input type="hidden" name="rid" value="<?php echo $ansPost->id;?>">
                                                  <textarea name="replyPost"> </textarea>
                                                  <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                </form>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                              </div>
                              <div class="col-xs-4 col-md-4">
                                 <div class="repot">
                                    <?php
                                        if(isset($_SESSION["login"])) {
                                          $userId = $_SESSION["login"]["id"];
                                    ?>
                                          <!--<p data-toggle="modal" data-target="#myModalrepost1<?php //echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                    <?php
                                        } else{
                                    ?>
                                          <!--<p data-toggle="modal" data-target="#myModalrepost1<?php echo $z.$z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                    <?php
                                        }
                                    ?>
                                 </div>
                                      <!-- Modal -->
                                      <div id="myModalrepost1<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                         <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                               <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title"> Post this on your wall </h4>
                                               </div>
                                               <div class="modal-body">
                                                  <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                               </div>
                                            </div>
                                         </div>
                                      </div>
                                      <!-- Modal -->
                                      <div id="myModalrepost1<?php echo $z;?>" class="modal fade" role="dialog">
                                         <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                               <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title"> Post this on your wall </h4>
                                               </div>
                                               <div class="modal-body">
                                                <form method="post" action="">
                                                  <p> <textarea name="reposttext"> </textarea></p>
                                                  <div class="row repostmodal">
                                                   <?php
                                                      $userPic111 = $wpdb->get_results( 
                                                        $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$ansPost->user_id));
                                                      $userName111 = $wpdb->get_results( 
                                                        $wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$ansPost->user_id));
                                                      $userHeadline111 = $wpdb->get_results( 
                                                       $wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$ansPost->user_id));

                                                      $userAdd111 = $wpdb->get_results(
                                                        $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $ansPost->user_id));
                                                      $userCountry111 = $wpdb->get_results(
                                                        $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd11[0]->country));
                                                      $userState111 = $wpdb->get_results(
                                                          $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd11[0]->state));
                                                    ?>
                                                       <div class="col-md-2 col-sm-2 col-xs-4">
                                                          <div class="imgforr">
                                                             <a href="#"><img src="<?php echo $userPic111[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                          </div>
                                                       </div>
                                                       <div class="col-md-10 col-sm-10 col-xs-8">
                                                          <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $ansPost->user_id;?>"><?php echo $userName111[0]->name;?></a></strong>,<span><?php echo $userHeadline111[0]->headline;?></span></p>
                                                          <p class="rigwd"> <strong> <?php if($userAdd111[0]->city){echo $userAdd111[0]->city; echo ",";} ?> <?php if($userState111[0]->name){echo $userState111[0]->name; echo ",";} ?> <?php echo $userCountry111[0]->name; ?></strong> </p>
                                                       </div>
                                                       <div class="col-md-12">
                                                          <div class="text2">
                                                             <p><?php echo $ansPost->ans_content; ?></p>
                                                          </div>
                                                       </div>
                                                  </div>
                                                  
                                                  <input type="hidden" name="ansid" value="<?php echo $ansPost->id;?>">
                                                  <input type="hidden" name="ans" value="<?php echo $ansPost->ans_content;?>">
                                                  <p> <input type="submit" name="repostanssubmit" value="Repost"> </p>
                                                </form>
                                               </div>
                                            </div>
                                         </div>
                                      </div>

                              </div>
                           </div>
                           <?php
                                    $z++;
                                }
                              }
                           ?>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- #main -->
            </div>
         </div>
      </div>
      <div class="col-md-3">
      </div>
   </div>
   <!-- #primary -->
</div>
<?php get_footer(); ?>
<script>
   var acc = document.getElementsByClassName("accordion");
   var i;
   
   for (i = 0; i < acc.length; i++) {
     acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
         panel.style.display = "none";
       } else {
         panel.style.display = "block";
       }
     });
   }
</script>
<script> 
   function openCity(evt, cityName) {
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("tabcontent");
     for (i = 0; i < tabcontent.length; i++) {
       tabcontent[i].style.display = "none";
     }
     tablinks = document.getElementsByClassName("tablinks");
     for (i = 0; i < tablinks.length; i++) {
       tablinks[i].className = tablinks[i].className.replace(" active", "");
     }
     document.getElementById(cityName).style.display = "block";
     evt.currentTarget.className += " active";
   }
   
   function openReport(evt, reportName) {
     var i, repocontent, repolinks;
     repocontent = document.getElementsByClassName("repocontent");
     for (i = 0; i < repocontent.length; i++) {
       repocontent[i].style.display = "none";
     }
     repolinks = document.getElementsByClassName("repolinks");
     for (i = 0; i < repolinks.length; i++) {
       repolinks[i].className = repolinks[i].className.replace(" active", "");
     }
     document.getElementById(reportName).style.display = "block";
     evt.currentTarget.className += " active";
   }
</script>
<div class="modal fade" id="myModal" role="dialog">
   <div class="modal-dialog vertical-align-center">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-body">
            <p>Some text in the modal.</p>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="myModal1" role="dialog">
   <div class="modal-dialog vertical-align-center">
      <div class="modal-content">
         <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
            <ul>
               <li>
                  <span  class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                     <div class="funkyradio-default">
                        <label for="checkbox1">I think it's spam, Promotional</label>
                     </div>
                  </span>
               </li>
               <li>
                  <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                     <div class="funkyradio-default">
                        <label for="checkbox1">I think  it's objectionable</label>
                     </div>
                  </span>
               </li>
               <li>
                  <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                     <div class="funkyradio-default">
                        <label for="checkbox1">I think it false or misinformation</label>
                     </div>
                  </span>
               </li>
            </ul>
            <div class="modal-footer">
            </div>
         </div>
         <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
            <div class="modal-body">
               <p>You Are Reorting that this is spam, Promotional</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
               <button type="button" class="btn btn-default">Submit</button>
            </div>
         </div>
         <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
            <div class="modal-body">
               <div class="funkyradio-default">
                  <label class="checkbox-inline"><input type="radio" value="">It's pornographic or extremely violent.</label>
               </div>
               <div class="funkyradio-default">
                  <label class="checkbox-inline"><input type="radio" value="Education Level">It's hate speech.</label>
               </div>
               <div class="funkyradio-default">
                  <label class="checkbox-inline"><input type="radio" value="Education Level">Topic or language is offensive/Abusive.</label>
               </div>
            </div>
            <div class="bottom-btns">
               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
               <button type="button" class="btn btn-default">Submit</button>
            </div>
         </div>
         <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
            <div class="modal-body">
               <p>You are reporting that it is a salse information or misinformation.</p>
            </div>
            <div class="bottom-btns">
               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
               <button type="button" class="btn btn-default">Submit</button>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
   function time_elapsed_string($datetime, $full = false) {
       $now = new DateTime;
       $ago = new DateTime($datetime);
       $diff = $now->diff($ago);
   
       $diff->w = floor($diff->d / 7);
       $diff->d -= $diff->w * 1;
   
       $string = array(
           'y' => 'year',
           'm' => 'month',
           'w' => 'week',
           'd' => 'day',
           'h' => 'hour',
           'i' => 'minute',
           's' => 'second',
       );
       foreach ($string as $k => &$v) {
           if ($diff->$k) {
               $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
           } else {
               unset($string[$k]);
           }
       }
   
       if (!$full) $string = array_slice($string, 0, 1);
       return $string ? implode(', ', $string) . ' ago' : 'just now';
   }
   ?>
<!-- Modal -->
<div id="myModalques" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Follow</h4>
         </div>
         <div class="modal-body">
            <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div id="myModalLike" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Like</h4>
         </div>
         <div class="modal-body">
            <p> For Like please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div id="myModalAns" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Answer</h4>
         </div>
         <div class="modal-body">
            <p> For Answer please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>

<!-- Modal -->
<div id="myModalbookLogin" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Bookmark Question</h4>
         </div>
         <div class="modal-body">
            <p> For Bookmark please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>



<script>
   $(document).ready(function(){
     $(".share").click(function(){
       $(".p-3").hide();
     });
     $(".share").click(function(){
       $(".p-3").show();
     });
   });

   function moreFunction(id) {
      var cusId = ".anscomplete" + id;
      var moreId = ".ansmore" + id;

      $(cusId).css("display", "inline");
      $(moreId).css("display", "none");
   }
</script>

