<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;

    if(isset($_POST['submitstepone'])) {
        $headline = trim($_POST["headline"]);
        $other = trim($_POST["summaryother"]);
        $userId = $_SESSION["login"]["id"];
        $page = trim($_POST["lmember"]);
        $tablename = "wp_profileStepOne";
        $getStepOne = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_profileStepOne WHERE user_id = %d",$userId) 
        );

        if($getStepOne) {
            
            if($page == "homemember") {
              $wpdb->update($tablename, array('headline'=>$headline), array('user_id'=>$userId));
              header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
            } else if($page == "homememberabout") {
              $wpdb->update($tablename, array('other'=>$other), array('user_id'=>$userId));
              header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
            } else {
              $wpdb->update($tablename, array('headline'=>$headline), array('user_id'=>$userId));
              $wpdb->update($tablename, array('other'=>$other), array('user_id'=>$userId));
              header('Location: https://edukeeda.com/edit-profile/');
            }
        } else {

            if($page == "homemember") {
                $data = array(
                      "user_id" => $userId,
                      "headline" => $headline
                    );
                $result = $wpdb->insert($tablename, $data);
              header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
            
            } else if($page == "homememberabout") {
                $data = array(
                      "user_id" => $userId,
                      "other" => $other
                    );
                $result = $wpdb->insert($tablename, $data);
                header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
            
            } else {
                $data = array(
                      "user_id" => $userId,
                      "headline" => $headline,
                      "other" => $other
                    );
                $result = $wpdb->insert($tablename, $data);
              header('Location: https://edukeeda.com/edit-profile/');
            } 
        }
    } else{
          $wrongCred = "Profile updation failed";
          $_SESSION["failed"] = $wrongCred;
          if($_POST["lmember"] == "homemember") {
            header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
          } else if($page == "homememberabout") {
              header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
          } else {
            header('Location: https://edukeeda.com/edit-profile/');
          }
    }

?>
