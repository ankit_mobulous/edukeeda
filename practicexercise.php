<?php
session_start();
  /**
  /*
   Template Name: Practice Exercise Template
   */   
get_header(); ?>
 <?php
    $path = $_SERVER['DOCUMENT_ROOT'];
    include_once $path . '/wp-config.php';
    include_once $path . '/wp-load.php';
    include_once $path . '/wp-includes/wp-db.php';
    include_once $path . '/wp-includes/pluggable.php';
    global $wpdb;
?>
<style>
   .enter textarea.form-control {
   height: 50px;
   padding: 10px;
   }
   .int textarea.form-control {
   height: 50px;
   padding: 10px;
   }
   .sub {
   float: left;
   width: 100%;
   text-align: center;
   margin-top: 5px;
   }
   .flot {
   float: left;
   width: 100%;
   margin: 5px 0px;
   }
   ol.breadcrumb li {
   display: inline-block;
   margin: 16px 13px;
   }
   .container .jumbotron, .container-fluid .jumbotron{
   margin-left: -16px;
   margin-right: -35px;
   background-color: #fff;
   }
   .jumbotron .h1, .jumbotron h1{
   color: white!important;
   }
   h3 {
   font-size: 30px;
   text-align: center;}
   form{
   margin-top: 22px;
   }
   h3 a{
   border-radius: 1px 1px 1px 4px;
   box-shadow: 1px 1px 1px 2px;
   padding: 7px;
   }
   hr {
   margin-top: 0px!important;
   margin-bottom: 0px!important;
   border: 0;
   border-top: 1px solid #eee;
   width: 280px!important;
   }
   input[type=checkbox], input[type=radio]{
   margin: 10px 0 0!important;
   }
   @media screen and (min-width: 768px){
   .jumbotron .h1, .jumbotron h1 {
   font-size: 30px !important;
   color: white!important;
   }
   }
   .container .jumbotron, .container-fluid .jumbotron{
   border-radius: 0
   }
   .jumbotron.text-center {
   margin: 0;
   width: 100%;
   border-radius: 0px !important;
   }
   .content-area .post-wrap, .contact-form-wrap {
   padding-right: 0 !important;
   }
   .container1 {
   margin: 0 auto;
   max-width: 1200px;
   }
   .quesitionset {
   float: left;
   width: 100%;
   border-bottom: 1px solid #ddd;
   }
   p.questionset {
   font-size: 23px;
   margin: 0;
   }
   .showopn ul{list=style-type:none; padding:0;}
   .showopn ul li{display:inline-block; margin-right:10px;}
   .showopn {
   margin: 10px 0;
   }
   .answersection{
   -webkit-transition: 1s;
   transition: .2s;
   }
   .answersection, .Workspace {
   opacity: 0;
   visibility: hidden;
   -webkit-transition: 1s;
   transition: .2s;
   height: 0px;
   }
   .answersection, .Workspaces {
   opacity: 0;
   visibility: hidden;
   -webkit-transition: 1s;
   transition: .2s;
   height: 0px;
   }
   .answersection.openansedu, .Workspace.openworksps{    opacity: 1;
   visibility: visible;
   -webkit-transition: 1s;
   transition: .2s;
   width: 45%;
   }
   .answersection.openansedu, .Workspaces.openworksps{    opacity: 1;
   visibility: visible;
   -webkit-transition: 1s;
   transition: .2s;
   width: 45%;
   }
   .answersection.openansedu{
   height: auto;
   }
   .Workspace.openworksps{
   height:100px;
   }
   .Workspaces.openworksps{
   height:146px;
   width: 100%;
   }
   .showans1{cursor:pointer;}
   .worksps{cursor:pointer;}
   .difcuttype1{margin-top: 30px;}
  .comment-section{
       list-style: none;
    max-width: 100%;
    width: 100%;
    margin: 0px auto;
    padding: 0px 0px;
    border: 1px solid#ddd;
    float: left;
   }
   .comment{
   display: flex;
   border-radius: 3px;
   margin-bottom: 0px;
   flex-wrap: wrap;
   }
   .comment.user-comment{
   color:  #808080;
   }
   .comment.author-comment{
   color:  #60686d;
   justify-content: flex-end;
   }
   /* User and time info */
   .comment .info{
   width: 100%;
   }
   .comment.user-comment .info{
   text-align: left;
   }
   .comment.author-comment .info{
   order: 3;
   }
   .comment .info a{ /* User name */
   display: block;
   text-decoration: none;
   color: #656c71;
   font-weight: bold;
   text-overflow: ellipsis;
   overflow: hidden;
   white-space: nowrap;
   padding: 0px 0 3px 0;
   margin-bottom: -10px;
   }
   .comment .info span{  /* Time */
   font-size: 14px;
   color:  #9ca7af;
   }
   /* The user avatar */
   .comment .avatar{
   width: 8%;
   }
   .comment.user-comment .avatar{
   padding: 10px 18px 0 3px;
   }
   .comment.author-comment .avatar{
   order: 2;
   padding: 10px 3px 0 18px;
   }
   .comment .avatar img{
   display: block;
   border-radius: 50%;
   }
   .comment.user-comment .avatar img{
   float: right;
   }
   /* The comment text */
   .comment p{
   line-height: 1.5;
   padding: 15px 22px;
   width: 100%;
   position: relative;
   word-wrap: break-word;
   }
   .comment.user-comment p{
   background-color:  #f3f3f3;
   width: 100%;
   }
   .comment.author-comment p{
   background-color:  #e2f8ff;
   order: 1;
   width: 100%;
   }
   .user-comment p:after{
   content: '';
   position: absolute;
   width: 15px;
   height: 15px;
   border-radius: 50%;
   background-color: #ffffff;
   border: 2px solid #f3f3f3;
   left: -8px;
   top: 18px;
   }
   .author-comment p:after{
   content: '';
   position: absolute;
   width: 15px;
   height: 15px;
   border-radius: 50%;
   background-color: #ffffff;
   border: 2px solid #e2f8ff;
   right: -8px;
   top: 30px;
   }
   /* Comment form */
   .write-new{
   margin: 0px;
   width: 100%;
   }
   .write-new textarea{
   color: #444;
   font: inherit;
   outline: 0;
   border-radius: 3px;
   border: 1px solid #cecece;
   background-color: #fefefe;
   box-shadow: 1px 2px 1px 0 rgba(0, 0, 0, 0.06);
   overflow: auto;
   width: 100%;
   min-height: 28px;
   padding: 5px 20px;
   height: 42px;
   }
   .write-new img{
   border-radius: 50%;
   margin-top: 15px;
   }
   .write-new button{
   float:right;
   background-color:  #f68e2f;
   box-shadow: 1px 2px 1px 0 rgba(0, 0, 0, 0.12);
   border-radius: 2px;
   border: 0;
   color: #ffffff;
   font-weight: bold;
   cursor: pointer;
   padding: 10px 25px;
   margin-top: 18px;
   }
   li.write-new {
   list-style: none;
   }
   
   .customcomment{
	max-height: 200px;
    height: 200px;
    overflow-y: scroll;
   }
   
   
   /* Responsive styles */
   
   
   @media (max-width: 800px){
   /* Make the paragraph in the comments take up the whole width,
   forcing the avatar and user info to wrap to the next line*/
   
    button.tablinks.active {
        margin: 10px;
        width: 100%;
     }
   button.tablinks {
        margin: 10px;
       width: 100%;
    }
  .ajeetbaba h3 {
      font-size: 16px;
      text-align: center;
    }
    .jumbotron h1 {
          color: white!important;
         font-size: 16px;
      }
    .jumbotron.text-center{
      margin-top:50px;
    }
   .comment p{
   width: 100%;
   }
   /* Reverse the order of elements in the user comments,
   so that the avatar and info appear after the text. */
   .comment.user-comment .info{
   order: 3;
   text-align: left;
   }
   .comment.user-comment .avatar{
   order: 2;
   }
   .comment.user-comment p{
   order: 1;
   }
   /* Align toward the beginning of the container (to the left)
   all the elements inside the author comments. */
   .comment.author-comment{
   justify-content: flex-start;
   }
   .comment-section{
   margin-top: 10px;
   }
   .comment .info{
   width: auto;
   }
   .comment .info a{
   padding-top: 15px;
   }
   .comment.user-comment .avatar,
   .comment.author-comment .avatar{
   padding: 15px 10px 0 18px;
   width: auto;
   }
   .comment.user-comment p:after,
   .comment.author-comment p:after{
   width: 12px;
   height: 12px;
   top: initial;
   left: 28px;
   bottom: -6px;
   }
   .write-new{
   width: 100%;
   }
   }
   .col-sm-12.quesitionset span{ margin-left: 8px!important;}
   .tablenav-pages span{ background-color: forestgreen;
    padding: 10px;
    margin-right: 5px;
    color: #fff; }

    .tablenav-pages a{ background-color: forestgreen;
    padding: 10px;
    margin-right: 5px;
    color: #fff; }
.want h3 {
    color: #060658;
    font-size: 28px;
    font-weight: 600;
    margin-bottom: 0px;
}
.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper{
  padding:15px;
}
.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper ol.breadcrumb {
    width: 100%;
    margin-top: -16px;
}
   /* ------- Demo footer. Please ignore and remove ------- */
</style>
<div class="ajeetbaba">
   <div id="primary" class="content-area">
      <main id="main" class="post-wrap" role="main">
  
        
         <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="<?php echo site_url();?>/student-corner-india/">Student Corner</a></li>
               <?php
                  $sn = $_GET["action"];
                  $getSampleName = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT * FROM wp_exam_type WHERE id = %d", $sn)
                    );
                  ?>
               <li class="breadcrumb-item"><a href="<?php echo site_url();?>/exams-subject-listing?action=<?php echo $sn;?>"><?php echo $getSampleName[0]->examName;?></a></li>
               <li class="breadcrumb-item active" aria-current="page"><strong>Practice Exercise</strong></li>
            </ol>
         </nav>
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <h3>Practice Exercise</h3>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-sm-12">
                  <?php
                     $tid = $_GET["action2"];
                     $getSampletop = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_topic_subject WHERE exam_id = %d AND subject_id", $sn, $tid)
                       );
                     ?>
                  <h3><?php echo $getSampletop[0]->topic_name;?></h3>
               </div>
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="container1">
                  <div class="col-sm-8">
                     <?php                    
                        $getLeveltexts = $wpdb->get_results("SELECT id, level_text FROM wp_exam_level");
                        
                        foreach($getLeveltexts as $getLeveltext) {
                        ?> 
                     <button class="tablinks <?php if($getLeveltext->id == 1){echo "active";}?>" onclick="openCity(event, '<?php if($getLeveltext->id == 1){echo "London";}else{echo "Paris";}?>')"> <?php echo $getLeveltext->level_text;?> </button>
                     <?php
                        }
                        ?>
                     <button class="tablinks" onclick="openCity(event, '#')">Concept sheet</button>
                  </div>
                  <div class="col-sm-4">
                  </div>
               </div>
            </div>
         </div>
         <?php
            $getLeveltexts1 = $wpdb->get_results("SELECT id, level_text FROM wp_exam_level");
            
             foreach($getLeveltexts1 as $getLeveltext1) {
            ?>
         <div class="difcuttype1 tabcontent" id="<?php if($getLeveltext1->id == 1){echo "London";}else{echo "Paris";} ?>" style="<?php if($getLeveltext1->id == 1){}else{echo "display: none;";} ?>">
            <div class="container1">
               <div class="row">
             <?php
                 $action = $_GET["action"];
                 $action1 = $_GET["action1"];
                 $action2 = $_GET["action2"];
                 
                 $countResults = $wpdb->get_results( 
                     $wpdb->prepare( "SELECT * FROM wp_sample_paper WHERE exam_id = %d AND subject_id = %d AND topic_id = %d AND level_id = %d", $action,$action1,$action2,$getLeveltext1->id)
                 );
                  $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
                  $limit = 10; // number of rows in page
                  $offset = ( $pagenum - 1 ) * $limit;
                  $total = count($countResults);
                  $num_of_pages = ceil( $total / $limit );

                  $getSamples = $wpdb->get_results( 
                     $wpdb->prepare( "SELECT * FROM wp_sample_paper WHERE exam_id = %d AND subject_id = %d AND topic_id = %d AND level_id = %d LIMIT $offset, $limit", $action,$action1,$action2,$getLeveltext1->id)
                 );

                 $i=1;
               foreach($getSamples as $getSample) {
              ?> 
                  <div class="col-sm-12 quesitionset" >
                     <p class="questionset"><?php echo $i;?>. <?php echo $getSample->question ;?>?<br></p>
                     <input type="radio" name="selectAnswer" class="a<?php echo $i;?>" value="<?php echo $getSample->answer; ?>" > A.  <span class="optiona<?php echo $i;?>"><?php echo $getSample->optiona; ?></span> <BR>
                     <input type="radio" name="selectAnswer" class="b<?php echo $i;?>" value="<?php echo $getSample->answer; ?>" > B.  <span class="optionb<?php echo $i;?>"><?php echo $getSample->optionb; ?></span> <BR>
                     <input type="radio" name="selectAnswer" class="c<?php echo $i;?>" value="<?php echo $getSample->answer; ?>" > C.  <span class="optionc<?php echo $i;?>"><?php echo $getSample->optionc; ?></span> <BR>
                     <input type="radio" name="selectAnswer" class="d<?php echo $i;?>" value="<?php echo $getSample->answer; ?>" > D.  <span class="optiond<?php echo $i;?>"><?php echo $getSample->optiond; ?></span> <BR>
                     <div class="showopn">
                        <ul>
                           <li>
                              <a class="showans<?php echo $i;?>" id="<?php echo $i;?>" onclick="myFunctionshow(this.id);"><img src="/wp-content/themes/sydney/images/Bookedu.png"><strong style="cursor: grab;"> Show Answer</strong></a>
                           </li>
                           <li>
                              <p class="worksps"><strong>Work Space</strong></p>
                           </li>
                          <?php
                              if($getSample->discussion_forum == 1) {
                          ?>
                           <li>
                              <p class="worksp"><strong>Discussion Forum</strong></p>
                           </li>
                          <?php 
                            }
                          ?>
                        </ul>
                     </div>

                     <div class="answersection<?php echo $i;?> hideanswer" style="display:none;">Answer is: <?php echo $getSample->answer; ?> <BR> <?php echo $getSample->answerExplain; ?> </div>

                     <div class="Workspace"><textarea></textarea></div>
                     <?php
                        if($getSample->discussion_forum == 1) {
                     ?>
                     <div class="Workspaces">
                        <ul class="comment-section">
                           <div class="col-md-6 customcomment">
                            <?php
                                $getSampleComments = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_sample_comments WHERE sample_ques_id = %d",$getSample->id)
                                );

                                if($getSampleComments) {
                                  $gsc = 1;
                                  foreach($getSampleComments as $getSampleComment) {
                            ?>
                                    <li class="comment <?php if($gsc%2 == 0){ echo 'user-comment';}else{ echo 'author-comment';} ?>">
                                       <div class="info">
                                          <a href="#"> <?php echo $getSampleComment->name; ?></a>
                                          <span> <?php echo $getSampleComment->email; ?> </span>
                                          <p> <?php echo $getSampleComment->sample_comment; ?> </p>
                                       </div>
                                    </li>
                            <?php
                                    $gsc++;
                                  }
                              } else{
                            ?>
                                <h4> No Comments </h4>
                            <?php
                              }
                            ?>
                           </div>
                           <div class="col-md-6">
                              <li class="write-new">
                                 <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/samplePaperCommentCode.php" method="post">
                                    <div class="col-md-12 customsend">
                                       <div class="flot">
                                          <textarea placeholder="Write your comment here" name="comment"></textarea>
                                       </div>
                                       <div class="sub">
                                          <input type="hidden" name="sp_id" value="<?php echo $getSample->id; ?>">
                                          <input type="hidden" name="action" value="<?php echo $action; ?>">
                                          <input type="hidden" name="action1" value="<?php echo $action1; ?>">
                                          <input type="hidden" name="action2" value="<?php echo $action2; ?>">
                                        <?php
                                          if(isset($_SESSION["login"])) {
                                            $userID = $_SESSION["login"]["id"];
                                            $getName = $wpdb->get_results( 
                                                $wpdb->prepare( "SELECT name FROM wp_profileStepTwo WHERE user_id = %d", $userID)
                                            );
                                            $getEmail = $wpdb->get_results( 
                                                $wpdb->prepare( "SELECT user_email FROM wp_users WHERE ID = %d", $userID)
                                            );
                                        ?>
                                            <input type="hidden" name="name" value="<?php echo $getName[0]->name; ?>">
                                            <input type="hidden" name="email" value="<?php echo $getEmail[0]->user_email; ?>">
                                            <input type="submit" value="Submit" name="postComment">
                                        <?php
                                          } else {
                                        ?>
                                            <button type="button" name="postComment" data-toggle="modal" data-target="#myModalpaper" > Submit </button>
                                        <?php
                                          }
                                        ?>
                                       </div>
                                 </form>
                              </li>
                              </div>
                        </ul>
                        </div>
                      <?php
                        }
                      ?>
                        <br>
                     </div>
                     <?php
                        $i++;
                      }
                     ?>
                  </div>
               </div>
               <div style="clear: both;"></div>
                <?php
                  $page_links = paginate_links( array(
                      'base' => add_query_arg( 'pagenum', '%#%' ),
                      'format' => '',
                      'prev_text' => __( '&laquo;', 'text-domain' ),
                      'next_text' => __( '&raquo;', 'text-domain' ),
                      'total' => $num_of_pages,
                      'current' => $pagenum
                  ));

                  if ( $page_links ) {
                      echo '<div class="tablenav" style="margin-top:50px;"><div class="tablenav-pages" style="margin: 1em 112px">' . $page_links . '</div></div>';
                  }
                ?>
            </div>
            <?php
               }
            ?>
         </div>

         <?php while ( have_posts() ) : the_post(); ?>
         <?php get_template_part( 'content', 'page' ); ?>
         <?php
            // If comments are open or we have at least one comment, load up the comment template
            if ( comments_open() || get_comments_number() ) :
              comments_template();
            endif;
            ?>
         <?php endwhile; // end of the loop. ?>
      </main>
      <!-- #main -->
   </div>
   <!-- #primary -->
   <script>
      function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
      }
      
      
      jQuery(".showans1").click(function (e) {
        e.stopPropagation();
        jQuery(".answersection").toggleClass('openansedu');
        jQuery(".Workspace").removeClass('openworksps');
        jQuery(".Workspaces").removeClass('openworksps');

        var showid = $(this).attr('id');
        var optionone = ".a" + showid;
        var optiontwo = ".b" + showid;
        var optionthree = ".c" + showid;
        var optionfour = ".d" + showid;
        var answer = $(optionone).val();

        var answerColor = ".option" + answer + showid;
        $(answerColor).css('color','green');

        if($(optionone).is(':checked')) { 
           if(answer == "a"){              
           } else {
              var sel = ".optiona" + showid;
              //$(sel).css('color','red');
           }
        }
        if($(optiontwo).is(':checked')) { 
           if(answer == "b"){              
           } else {
              var sel = ".optionb" + showid;
              //$(sel).css('color','red');
           }
        }
        if($(optionthree).is(':checked')) { 
           if(answer == "c"){              
           } else {
              var sel = ".optionc" + showid;
              //$(sel).css('color','red');
           }
        }
        if($(optionfour).is(':checked')) { 
           if(answer == "d"){              
           } else {
              var sel = ".optiond" + showid;
              //$(sel).css('color','red');
           }
        }
      });
      
      jQuery(".worksps").click(function (e) {
        e.stopPropagation();
        jQuery(".Workspace").toggleClass('openworksps');
        jQuery(".answersection").removeClass('openansedu');
        jQuery(".Workspaces").removeClass('openworksps');

      });
      
       jQuery(".worksp").click(function (e) {
        e.stopPropagation();
        jQuery(".Workspaces").toggleClass('openworksps');
        jQuery(".Workspace").removeClass('openworksps');
        jQuery(".answersection").removeClass('openansedu');
      });
   </script>
   <script type="text/javascript">
      $(document).ready(function(){
         $(".tablinks").on('click',function(){
               $(".col-sm-12.quesitionset span").css('color','black');
         });
      });
   </script>
   <?php get_footer(); ?>
</div>

<!-- Modal -->
<div id="myModalpaper" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Comment</h4>
      </div>
      <div class="modal-body">
        <p>For Comment on Practice Question, Please <a href="<?php echo site_url();?>/post-article-form"> Login / Register </a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
  function myFunctionshow(id) {
    var checkclass = ".answersection" + id;
    $(".hideanswer").css("display","none");
    $(checkclass).css("display","block");
  }
</script>
