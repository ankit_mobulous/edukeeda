<style type="text/css">
  .insidecontainer{margin-top: 8%!important;}
  .headlink h4 {
    text-transform: uppercase;
}
.headlink a {
    color: #060658;
}
.cont1 a{
    color: #020253;
font-weight: 600;
font-size: 21px;
text-transform: uppercase;
}
</style>
<div class="eventnew">
<?php
session_start();
/**
/*
  Template Name:Join Test 
*/
if($_SESSION["login"]) {

} else {
  header('Location: https://edukeeda.com/signin/');
}
get_header(); ?>
<?php
  $path = $_SERVER['DOCUMENT_ROOT'];
  include_once $path . '/wp-config.php';
  include_once $path . '/wp-load.php';
  include_once $path . '/wp-includes/wp-db.php';
  include_once $path . '/wp-includes/pluggable.php';
  
  global $wpdb;
?>
    
   <div class="insidecontainer">
   <div class="col-md-12">
      <div class="stu">
         <div class="ardo">
            <div class="col-md-6">
               <div class="cont">
                  <div class="halfar">
                  <?php
                    $examid = $_GET["action"];
                    $getExamType = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT * FROM wp_premium_exam_type WHERE id = %d", $examid));

                    $getExamID = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT * FROM wp_exam_type WHERE examName = %s", $getExamType[0]->exam_title));
                    if($getExamID) {
                  ?>
                    <a href="<?php echo site_url();?>/list-of-competitive-examinations-details/?action=<?php echo $getExamID[0]->id;?>">
                  <?php
                    } else {
                  ?>
                      <a href="#">
                  <?php
                    }
                  ?>
                       <p> About <?php echo $getExamType[0]->exam_title;?> Examination </p>
                    </a>
                  </div>

               </div>
            </div>
            <div class="col-md-6">
               <div class="cont1">
                   <div class="headlink">
                  	<a href="<?php echo site_url();?>/studentmore/">
                     <h4>Practice Exercise</h4>
                   	</a>
                   </div>   

                <?php
                  $eid = $_GET["action"];
                  $getsubs = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_premium_exam_subject WHERE exam_id = %d", $eid) 
                  );

                  foreach($getsubs as $getsub) {
                ?>
                    <a href="<?php site_url();?>/test-list?action=<?php echo $eid;?>&action1=<?php echo $getsub->id; ?>"> <p> <?php echo $getsub->subject; ?> </p> </a>
                <?php
                  }
                ?>
                
                <?php
                  $getsubs1 = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_premium_quiz WHERE exam_id = %d AND subject_id=0", $eid) 
                  );
                  if($getsubs1){
                    foreach($getsubs1 as $getsub1) {
                ?>
                      <a class="ctests" href="<?php site_url();?>/test-list?action=<?php echo $eid;?>&action1=<?php echo "0"; ?>"> <p> <?php echo "Complete Test Series"; ?> </p> </a>
                <?php
                    }
                  }
                ?>

               </div>
               <div class="cont2">
                  <a href="<?php site_url();?>/performance?action=<?php echo $eid;?>"> Test Performance </a>
               </div>
            </div>
         </div>
      </div>
   </div>
   
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
</div>   
<?php get_footer(); ?>
</div>
<style>

.cont2 {
    padding: 0px 21px;
}

.centerbts {
    padding: 0 12px;
      border: none;
}

.centerbts a {
    margin: 15% 0 0;
    display: inline-block;
    background: #f68e2f;
    padding: 0 10px;
    color: #fff;
}

.eventnew .content-wrapper.container {
    width: 100%;
    margin: 0 auto;
    padding: 0 !important;
}
.cont1 {
    border-left: 1px solid#DDD;
    padding: 0px 21px;
}
.cont2 a {
    color: #060658;
    font-size: 19px;
}
.eventnew .content-wrapper.container > .row {
    margin: 0;
}
.cont:hover {
    box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0.07);
}
   .want a {
     border-radius: 10px;
     background: #f68e2f;
     color: #fff;
     padding: 6px 18px;
     font-size: 13px;
     text-align: center;
     margin: 13px 0 0 0;
     text-transform: capitalize;
   }
   .cont1 h4 {
    font-size: 21px;
    text-transform: uppercase;
}
   .cont p {
     font-size: 25px;
     font-weight: 600;
     margin: 5px 0px;
     color: #060658;
   text-transform: uppercase;
       float: none;
    margin-top: 20%;
    margin-left: 19%;
    text-decoration: underline;
   }
   .cont {
     float: left;
     width: 100%;
     
   }
   .join .page-wrap .container {
     width: 100%;
     padding: 0;
     margin: 0 auto;
   }
   .join .page-wrap .container .row {
      margin: 0;
   }
   .catchybga {
   padding: 2% 0;
   background-position: 100%;
   position: relative;
   margin-bottom: 45px;
   background: #4054b2;
   }
   .want {
   text-align: center;
   color: white;
   }
   .want h3 {
   color: white;
   font-size: 36px;
   font-weight: 600;
   margin-bottom: 0px;
   }
   .want a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .stu {
   float: left;
   width: 100%;
       margin-bottom: 50px;
   }
   .headlink {
    float: left;
    width: 100%;
    margin-bottom: 7%;
}
   @media only screen and (max-width: 767px)
   {
       .cont2 {
    padding: 0px;
    }
    .cont2 a {
    font-size: 18px;
    }
       .cont p {
    font-size: 20px;
    font-weight: 600;
    color: #060658;
    text-transform: uppercase;
    float: none;
    text-decoration: underline;
    margin: 0px;
    float: left;
    width: 100%;
    }
    .headlink h4 {

    font-size: 19px!important;
    margin: 22px 0px;
    }
    .cont1 {
    border-left: 0px solid#DDD;
    padding: 0px;
    }
    .cont1 h4 {
    font-size: 17px;
    text-transform: uppercase;
    }
   .want h3 {
   color: white;
   font-size: 23px;
   font-weight: 100;
   }
   .want p {
   font-size: 17px;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 20px;
   }
   }
   .cont h4{margin-top: 10%;}
   .cont1 .ctests p{margin-top: 9%;}
</style>

