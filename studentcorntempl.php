<div class="ajeetbaba bluheadpack studentcoredu">

<?php
/**
/*
 Template Name: Student Corner Template
 */

get_header(); ?>


 <style>
 .elementor-854 .elementor-element.elementor-element-7df58ea > .elementor-widget-container{
	 background-color: #484543;
 }
 
 .elementor-element.elementor-button-warning .elementor-button {
    background-color: #33302b;
}

.elementor a {
    font-size: 14px;
    color: #000;
}

.elementor a:hover{
	    color: #4a4aca;
}

.student-corns-pg div#primary {
    padding: 0;
}

.content-area .hentry{
	    padding-top: 21px;
}

.elementor-element.elementor-button-warning.elementor-widget-button {
    padding: 23px 40px 0 40px;
    text-align: right;
}

.elementor-widget-button a.elementor-button, .elementor-widget-button .elementor-button {
    font-family: "Roboto", Sans-serif;
    font-weight: 500;
    background-color: #393c39;
}

.elementor-854 .elementor-element.elementor-element-7df58ea > .elementor-widget-container {
    background-color: #484543;
    margin: 34px 0;
}

.bluheadpack .site-header{
    background:#ffffff;
    box-shadow:none;
}

.bluheadpack .site-header #mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link{
    color:#464646;
}

.bluheadpack .site-header .site-logo {
    max-height: 100px;
    width: 84%;
    padding: 10px;
    background: #fff;
    border-radius: 10px;
    box-shadow: #fff 0px 0px 9px 0px;
}

.site-header.float-header{
    background:#fff;
        box-shadow: 0px 0px 4px 0px;
}

.site-header.float-header #mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link{
    color:#464646;
}
.bluheadpack .site-header .tophdsa {
padding: 14px 0;
}

 .elementor-element.elementor-button-info .elementor-button {
    background-color: #f68e2f!important;
    margin-right: 83px;
}

 .elementor-widget-button a.elementor-button, .elementor-widget-button .elementor-button {
    font-family: "Roboto", Sans-serif;
    font-weight: 500;
    background-color: #52c641;
}

.elementor-854 .elementor-element.elementor-element-136ce67{
	    background: url(../img/scr4.jpg);
    background-size: cover;
    background-position: 100% 76%;
}
 </style>

 
 
	<div id="primary" class="content-area col-md-12">
		<main id="main" class="post-wrap" role="main">
		
		<div class="elementor-element  elementor-button-warning  elementor-widget-button">
							<?php if(!empty($_SESSION['user_data'])){?>
							<a href="<?php echo site_url();?>/student-home-profile/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
								<span class="elementor-button-content-wrapper">
								<span class="elementor-button-icon elementor-align-icon-left">
								<i class="fa fa-address-book-o" aria-hidden="true"></i>
								</span>
								<span class="elementor-button-text">Go to Profile</span>
								</span>
							</a>
							<?php }
							else{?>
							<a href="<?php echo site_url();?>/student-sign-up/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
								<span class="elementor-button-content-wrapper">
								<span class="elementor-button-icon elementor-align-icon-left">
								<i class="fa fa-address-book-o" aria-hidden="true"></i>
								</span>
								<span class="elementor-button-text">Student Login</span>
								</span>
							</a>

								<?php } ?>
		</div>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
</div>