<div class="ajeetbaba bluheadpack studentcoredu">

<?php
/**
/*
 Template Name: Student Corner Template New Ht
 */

get_header(); ?>


 <style>
 .elementor-854 .elementor-element.elementor-element-7df58ea > .elementor-widget-container{
	 background-color: #484543;
 }
 
 .elementor-element.elementor-button-warning .elementor-button {
    background-color: #33302b;
}

.elementor a {
    font-size: 14px;
    color: #000;
}

.elementor a:hover{
	    color: #4a4aca;
}

.student-corns-pg div#primary {
    padding: 0;
}

.content-area .hentry{
	    padding-top: 21px;
}

.elementor-element.elementor-button-warning.elementor-widget-button {
    padding: 23px 40px 0 40px;
    text-align: right;
}

.elementor-widget-button a.elementor-button, .elementor-widget-button .elementor-button {
    font-family: "Roboto", Sans-serif;
    font-weight: 500;
    background-color: #393c39;
}

.elementor-854 .elementor-element.elementor-element-7df58ea > .elementor-widget-container {
    background-color: #484543;
    margin: 34px 0;
}

.bluheadpack .site-header{
    background:#4054b2;
    box-shadow:none;
}

.bluheadpack .site-header #mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link{
    color:#fff;
}

.bluheadpack .site-header .site-logo {
    max-height: 100px;
    width: 84%;
    padding: 10px;
    background: #fff;
    border-radius: 10px;
    box-shadow: #fff 0px 0px 9px 0px;
}

.site-header.float-header{
    background:#fff;
        box-shadow: 0px 0px 4px 0px;
}

.site-header.float-header #mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link{
    color:#464646;
}
.bluheadpack .site-header .tophdsa {
padding: 14px 0;
}

 .elementor-element.elementor-button-info .elementor-button {
    background-color: #f68e2f!important;
    margin-right: 83px;
}

 .elementor-widget-button a.elementor-button, .elementor-widget-button .elementor-button {
    font-family: "Roboto", Sans-serif;
    font-weight: 500;
    background-color: #52c641;
}

.elementor-854 .elementor-element.elementor-element-136ce67{
	    background: url(../img/scr4.jpg);
    background-size: cover;
    background-position: 100% 76%;
}

.studentcoredu h2.elementor-heading-title.elementor-size-default {
    font-size: 38px;
    padding: 5% 0;
    text-align:center;
    color:#fff;
    margin:0;
}

 .container2 {
    margin: 0 auto;
    max-width: 1170px;
}

.studentlogs {
    float: left;
    width: 100%;
        margin: 24px 0;
}
article{
    float: left;
    width: 100%;
}

.studentlogs a {
    padding: 8px 20px;
    margin: 0 !important;
    border-radius: 5px;
}

.studentlogs .elementor-button .elementor-align-icon-left {
    float: left;
    margin: 3px;
    padding:0
}

.overlybg a {
    font-size: 14px;
    color: #000;
}

.overlybg a:hover{
	    color: #3131a7;
}

.practicetest {
    float: left;
    width: 100%;
    margin: 40px 0;
}

.practicetest h3 {
    text-align: center;
    color: #5c5cd0;
    font-weight: 800;
}

.papertypesection {
    float: left;
    width: 100%;
    border-bottom: 1px solid #ddd;
    padding-bottom: 30px;
    margin-bottom: 30px;
}

.premiumbtn {
    text-align: center;
}

.topicholder .premiumbtn a {
    padding: 12px 25px;
    border-radius: 3px;
    font-weight: 600;
    background-color: #52c641;
    font-size: 16px;
    color: #fff;
}

.topicholder .premiumbtn a:hover{        background: #4a4aca;}

.papertypesection h5 a {
    color: #5c5cd0;
}

.topicholder a {
    font-size: 14px;
    color: #000;
}

.topicholder button.btn.btn-info {
    color: #484848;
    font-weight: 800;
    text-transform: capitalize;
    background: transparent;
    border: none;
}
.topicholder .btn-info.focus, .topicholder .btn-info:focus{
	    outline: none;
}
 </style>

 <div class="elementor elementor-854">
 <div data-id="136ce67" class="elementor-element elementor-element-136ce67 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="b544494" class="elementor-element elementor-element-b544494 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="b605c4b" class="elementor-element elementor-element-b605c4b elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Gateway to Success - A true guide</h2>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</div>
</div>		
 
	<div id="primary" class="content-area col-md-12">
		<main id="main" class="post-wrap" role="main">
		<div class="container2">
		<div class="studentlogs">
			<div class="elementor-element  elementor-button-info  elementor-widget-button">
				<a href="http://edukeeda.com/student-sign-up/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
				<span class="elementor-button-content-wrapper">
				<span class="elementor-button-icon  elementor-align-icon-left">
				<i class="fa fa-address-book-o" aria-hidden="true"></i>
				</span>
				<span class="elementor-button-text">Student Login</span>
				</span>
				</a>

			</div>		
		</div>
			
		<div class="col-md-4">
			<div class="overlybg">
			<a href="http://edukeeda.com/list-of-competitive-examinations-details/">
				<h5>Competitive Exams</h5>
				</a>
				<ul><a href="http://edukeeda.com/list-of-competitive-examinations-details/">
				<li>Exam Details</li>
				 <li>Qualification</li>
				  <li>Syllabus</li>
				   <li>Pattern</li>
				</a><p><a href="http://edukeeda.com/list-of-competitive-examinations-details/"></a><a href="http://edukeeda.com/list-of-competitive-examinations-details/">Detail</a></p>
					</ul>
			</div>
		</div>
		
		<div class="col-md-4">
			<a href="http://edukeeda.com/previous-year-papers/">
			<div class="overlybg" style="    border: 1px solid #e96126;">
					<h5>Download Previous Year Paper</h5>
			</div>
			</a>
		</div>
		
		<div class="col-md-4">
		<a href="#">
		<div class="overlybg" style="border: 1px solid #7c7cce;">
			<h5>Success Story</h5>
		<ul>
			<li>IIT JEE/Medical</li>
			 <li>NEET/Medical</li>
			  <li>IAS</li>
				<li>PCS</li>
			<p>More</p>
		</ul>   
		</div>
		</a>
		</div>
		
		<div class="practicetest">
		<h3 class="elementor-heading-title elementor-size-default">Practice Exercise &amp; Tests by experts </h3>
		
		</div>
		
		
		
		
		<div class="papertypesection">
			<h5 class="elementor-heading-title elementor-size-default">
			<a href="http://edukeeda.com/jee-mains-info/http://edukeeda.com/jee-mains-info/">JEE Mains / Engineering</a>
			</h5>
			
			<div class="topicholder">
				<div class="col-md-4">
					
					 
						  <div class="subjectonecont">
					<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">Physics</button>
					  <div id="demo" class="collapse in">
						  <div class="subjectopic">
							<ol>
							<li><a href="#">Physical World and Measurement</a></li>
							<li><a href="#">Kinematics</a></li><li><a href="#">Law of Motion</a></li>
							</ol>
							<p><a href="http://edukeeda.com/jee-mains-information/">More..</a></p>
						  </div>
					  </div>
					 </div>
					  <div class="subjectonecont">
					  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo2">Chemistry</button>
					  <div id="demo2" class="collapse">
						<div class="subjectopic">
							<ol>
							<li><a href="#">Physical World and Measurement</a></li>
							<li><a href="#">Kinematics</a></li><li><a href="#">Law of Motion</a></li>
							</ol>
							<p><a href="http://edukeeda.com/jee-mains-information/">More..</a></p>
						  </div>
					  </div>
					  </div>
					  
					  <div class="subjectonecont">
					  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo3">Maths</button>
					  <div id="demo3" class="collapse">
						<div class="subjectopic">
							<ol>
							<li><a href="#">Physical World and Measurement</a></li>
							<li><a href="#">Kinematics</a></li><li><a href="#">Law of Motion</a></li>
							</ol>
							<p><a href="http://edukeeda.com/jee-mains-information/">More..</a></p>
						  </div>
					  </div>
					  </div>
					 
				</div>
				
				<div class="col-md-4">
					<div class="elementor-text-editor elementor-clearfix"><p><a href="http://edukeeda.com/list-of-competitive-examinations-details/">Exam detail &amp; Syllabus</a></p><p><a href="#">Previous Year Papers</a></p></div>
				</div>
				
				<div class="col-md-4">
					<div class="premiumbtn">
					<a href="http://edukeeda.com/premium-test-series/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Premium Test Series</span>
						</span>
					</a>
					</div>
				</div>
			</div>
			
		</div>
		
		<div class="papertypesection">
			<h5 class="elementor-heading-title elementor-size-default">
			<a href="http://edukeeda.com/jee-mains-info/http://edukeeda.com/jee-mains-info/">JEE Mains Advance</a>
			</h5>
			
			<div class="topicholder">
				<div class="col-md-4">
					
					  
						  <div class="subjectonecont">
					<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo7">Physics</button>
					  <div id="demo7" class="collapse in">
						  <div class="subjectopic">
							<ol>
							<li><a href="#">Physical World and Measurement</a></li>
							<li><a href="#">Kinematics</a></li><li><a href="#">Law of Motion</a></li>
							</ol>
							<p><a href="http://edukeeda.com/jee-mains-information/">More..</a></p>
						  </div>
					  </div>
					 </div>
					  <div class="subjectonecont">
					  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo8">Chemistry</button>
					  <div id="demo8" class="collapse">
						<div class="subjectopic">
							<ol>
							<li><a href="#">Physical World and Measurement</a></li>
							<li><a href="#">Kinematics</a></li><li><a href="#">Law of Motion</a></li>
							</ol>
							<p><a href="http://edukeeda.com/jee-mains-information/">More..</a></p>
						  </div>
					  </div>
					  </div>
					  
					  <div class="subjectonecont">
					  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo9">Maths</button>
					  <div id="demo9" class="collapse">
						<div class="subjectopic">
							<ol>
							<li><a href="#">Physical World and Measurement</a></li>
							<li><a href="#">Kinematics</a></li><li><a href="#">Law of Motion</a></li>
							</ol>
							<p><a href="http://edukeeda.com/jee-mains-information/">More..</a></p>
						  </div>
					  </div>
					  </div>
					 
				</div>
				
				<div class="col-md-4">
					<div class="elementor-text-editor elementor-clearfix"><p><a href="http://edukeeda.com/list-of-competitive-examinations-details/">Exam detail &amp; Syllabus</a></p><p><a href="#">Previous Year Papers</a></p></div>
				</div>
				
				<div class="col-md-4">
					<div class="premiumbtn">
					<a href="http://edukeeda.com/premium-test-series/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Premium Test Series</span>
						</span>
					</a>
					</div>
				</div>
			</div>
			
		</div>
		
		
		<div class="papertypesection">
			<h5 class="elementor-heading-title elementor-size-default">
			<a href="http://edukeeda.com/jee-mains-info/http://edukeeda.com/jee-mains-info/">NEET/ Medical</a>
			</h5>
			
			<div class="topicholder">
				<div class="col-md-4">
					<div class="subjectonecont">
					<button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo4">Physics</button>
					  <div id="demo4" class="collapse in">
						  <div class="subjectopic">
							<ol>
							<li><a href="#">Physical World and Measurement</a></li>
							<li><a href="#">Kinematics</a></li><li><a href="#">Law of Motion</a></li>
							</ol>
							<p><a href="http://edukeeda.com/jee-mains-information/">More..</a></p>
						  </div>
					  </div>
					 </div>
					  <div class="subjectonecont">
					  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo5">Chemistry</button>
					  <div id="demo5" class="collapse">
						<div class="subjectopic">
							<ol>
							<li><a href="#">Physical World and Measurement</a></li>
							<li><a href="#">Kinematics</a></li><li><a href="#">Law of Motion</a></li>
							</ol>
							<p><a href="http://edukeeda.com/jee-mains-information/">More..</a></p>
						  </div>
					  </div>
					  </div>
					  
					  <div class="subjectonecont">
					  <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo6">Maths</button>
					  <div id="demo6" class="collapse">
						<div class="subjectopic">
							<ol>
							<li><a href="#">Physical World and Measurement</a></li>
							<li><a href="#">Kinematics</a></li><li><a href="#">Law of Motion</a></li>
							</ol>
							<p><a href="http://edukeeda.com/jee-mains-information/">More..</a></p>
						  </div>
					  </div>
					  </div>
				</div>
				
				<div class="col-md-4">
					<div class="elementor-text-editor elementor-clearfix"><p><a href="http://edukeeda.com/list-of-competitive-examinations-details/">Exam detail &amp; Syllabus</a></p><p><a href="#">Previous Year Papers</a></p></div>
				</div>
				
				<div class="col-md-4">
					<div class="premiumbtn">
					<a href="http://edukeeda.com/premium-test-series/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Premium Test Series</span>
						</span>
					</a>
					</div>
				</div>
			</div>
			
		</div>
		
		
		
		
		</div>
		
		
		
		
		
		<div class="elementor-element  elementor-button-warning  elementor-widget-button">
							<?php if(!empty($_SESSION['user_data'])){?>
							<a href="<?php echo site_url();?>/student-home-profile/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
								<span class="elementor-button-content-wrapper">
								<span class="elementor-button-icon elementor-align-icon-left">
								<i class="fa fa-address-book-o" aria-hidden="true"></i>
								</span>
								<span class="elementor-button-text">Go to Profile</span>
								</span>
							</a>
							<?php }
							else{?>
							<a href="<?php echo site_url();?>/student-sign-up/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
								<span class="elementor-button-content-wrapper">
								<span class="elementor-button-icon elementor-align-icon-left">
								<i class="fa fa-address-book-o" aria-hidden="true"></i>
								</span>
								<span class="elementor-button-text">Student Login</span>
								</span>
							</a>

								<?php } ?>
		</div>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?> 
</div>
