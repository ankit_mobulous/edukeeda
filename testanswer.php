<style>

    ol.breadcrumb li {
    display: inline-block;
    margin: 16px 13px;
    }
    .container .jumbotron, .container-fluid .jumbotron{
    margin-left: -16px;
    margin-right: -35px;
    background-color: #4054b2;
    }
    .jumbotron .h1, .jumbotron h1{
    color: white!important;
    }
    h3 {
    font-size: 30px;
    text-align: center;}
    form{
    margin-top: 23px;
    }
    h3 a{
    border-radius: 1px 1px 1px 4px;
    box-shadow: 1px 1px 1px 2px;
    padding: 7px;
    }
    hr {
    margin-top: 0px!important;
    margin-bottom: 0px!important;
    border: 0;
    border-top: 1px solid #eee;
    width: 280px!important;
    }
    input[type=checkbox], input[type=radio]{
    margin: 10px 0 0!important;
    }
	.panel-body p {
    font-weight: 600!important;
}
.panel-heading center {
    color: #fff!important;
    font-weight: 600!important;
    font-size: 25px!important;
}
.panel-warning {
    border-color: #52c641!important;
}
.panel-warning>.panel-heading {
    color: #000!important;
    background-color: #52c641!important;
    border-color: #f68e2f!important;
}

	@media screen and (min-width: 768px){

	.jumbotron .h1, .jumbotron h1 {
    font-size: 30px !important;
    color: white!important;
    }
    }
    .container .jumbotron, .container-fluid .jumbotron{
    border-radius: 0
    }
    .jumbotron.text-center {
    margin: 0;
    width: 100%;
    border-radius: 0px !important;
    }
    .content-area .post-wrap, .contact-form-wrap {
    padding-right: 0 !important;
    }
    .container1 {
    margin: 0 auto;
    max-width: 1200px;
    }
    .quesitionset {
    float: left;
    width: 100%;
    border-bottom: 1px solid #ddd;
    }
    p.questionset {
   font-size: 20px;
    margin: 0;
    font-weight: 600;
    }
    .showopn ul{list=style-type:none; padding:0;}
    .showopn ul li{display:inline-block; margin-right:10px;}
    .showopn {
    margin: 10px 0;
    }
    .answersection{
    -webkit-transition: 1s;
    transition: .2s;
    }
    .answersection, .Workspace {
    opacity: 0;
    visibility: hidden;
    -webkit-transition: 1s;
    transition: .2s;
    height: 0px;
    }
    .answersection.openansedu, .Workspace.openworksps{    opacity: 1;
    visibility: visible;
    -webkit-transition: 1s;
    transition: .2s;
    width: 45%;
    }
    .answersection.openansedu{
    overflow-y: scroll;
    height: auto;
    }
    .Workspace.openworksps{
    height:100px;
    }
    .quesitionset .correctans{
        border: 1px solid #008000;
        padding-left: 20px;
        padding-right: 20px;}
    .quesitionset .realans{
        border: 1px solid #008000;
        padding-left: 20px;
        padding-right: 20px;}
    .quesitionset .wrongans{
        border: 1px solid red;
        padding-left: 20px;
        padding-right: 20px;}
        #remain{float:right;color:red;font-size:18px;margin-right: 10%;}
        .redWrong{margin-left: 40%;color: #f00;text-transform: uppercase;}
        .greenCorrect{margin-left: 40%;color: darkgreen;text-transform: uppercase;}
        .jumbotron.text-center h3{color:#fff;font-size:28px;}
 </style>
<div class="ajeetbaba">
<?php
session_start();
/**
/*
 Template Name: Test Answer
 */
if($_SESSION["login"]) {

} else {
  header('Location: https://edukeeda.com/signin/');
}
$userId = $_SESSION["login"]["id"];
get_header(); ?>

<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

  $eid = $_GET["action"];
  $sid = $_GET["action1"];
  $qid = $_GET["action2"];
  $uid = $_SESSION["login"]["id"];
?>

   <div id="primary" class="content-area">
      <main id="main" class="post-wrap" role="main">
         <!--<div class="jumbotron text-center">
            <h3>Premium Test</h3>
         </div>-->
         
         <div class="container" style="margin-top:30px;">
           
            <div class="row">
               <div class="col-sm-12">
                <?php
                  $getexam = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_premium_exam_type WHERE id = %d", $eid) 
                  );
                  $getSub = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_premium_exam_subject WHERE exam_id = %d AND id=%d", $eid, $sid) 
                  );
                  $getTime = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_premium_quiz WHERE exam_id = %d AND subject_id=%d", $eid, $sid) 
                  );

                ?>
                 <h3> <?php echo $getexam[0]->exam_title; ?> - <?php echo $getSub[0]->subject;  ?></h3>
               </div>
            </div>

            <div class="row">
              <?php
                $getResult = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_user_performance_premium WHERE user_id =%d AND test_id = %d AND quiz_id=%d", $uid, $eid, $qid) 
                );
              ?>
              <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-warning">
                    <?php
                        $incorr = $getResult[0]->attemptQues - $getResult[0]->correctAnswer;
                        $incorrPoint = $incorr * $getTime[0]->minus_point;
                        $corrPoint = $getResult[0]->correctAnswer * $getTime[0]->points;
                        $finalPoint =  $corrPoint - $incorrPoint;
                      ?>
                  <div class="panel-heading"><center>Total Marks: <?php echo $finalPoint;?> / <?php echo $getResult[0]->totalQues * $getTime[0]->points;?></center></div>
                  <div class="panel-body">
                    <p>Marks for Correct Answers : <?php echo $getTime[0]->points;?></p>
                    <p>Negative Marks for Wrong Answers : <?php echo $getTime[0]->minus_point;?></p>
                    <p>Total number of questions : <?php echo $getResult[0]->totalQues;?></p>
                    <p>Number of answered questions : <?php echo $getResult[0]->attemptQues;?></p>
                    <p>Number of Incorrect Questions : <?php echo $incorr; ?></p>
                    <p>Number of unanswered questions : <?php echo $unanswer = $getResult[0]->totalQues-$getResult[0]->attemptQues; ?></p>
                  </div>
                </div>
              </div>
            </div>
            
         </div>
        
          <div class="difcuttype1 tabcontent" id="">
            <div  class="container1">
               
               <div class="row">
                <?php
                  $getAnswer = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_user_premium_answer WHERE user_id = %d AND test_id = %d AND quiz_id = %d", $uid, $eid, $qid));
                  $saveAnswer = $getAnswer[0]->saveAnswer;
                  $ansSave = explode("=", $saveAnswer);

                    // $sAnswer = $_POST["selectAnswer"];
                    // $ans = array();

                    // for($x=1; $x<=$cid-1; $x++) {
                    //   $op = "selectAnswer$x";
                    //   $ans[] = $_POST[$op];
                    // }
                    
                    $getPremiums = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT * FROM wp_premium_paper WHERE exam_id = %d AND quiz_id = %d", $eid,$qid)
                    );
                    $i=0;
                    
                    foreach($getPremiums as $getPremium) {
                      $getAns =  $ansSave[$i];
                      $refineAns = explode("-",$getAns);
                ?>
                       <div class="col-sm-12 quesitionset" style="margin-bottom: 20px; padding: 20px;">
                        <p class="questionset"><?php echo $i;?>. <?php echo $getPremium->question_title ;?>?<br></p>

                        A. <span class="optiona<?php echo $i;?> <?php if($refineAns[0] == "A"){if($refineAns[0] == $getPremium->answer){echo "correctans";}else if($refineAns[0] == NULL){}else{ echo "wrongans";}} ?> <?php if($refineAns[0] != "A"){if($getPremium->answer == "A"){ echo "realans";}} ?>">
                            <?php echo $getPremium->optiona;?>

                            <?php if($refineAns[0] == "A"){} ?>
                          </span> <?php if($refineAns[0] == "A"){if($refineAns[0] == $getPremium->answer){echo "<span class='greenCorrect'>Correct</span>";}else if($refineAns[0] == NULL){}else{ echo "<span class='redWrong'>wrong</span>";}} ?><BR>

                        B. <span class="optionb<?php echo $i;?> <?php if($refineAns[0] == "B"){if($refineAns[0] == $getPremium->answer){echo "correctans";}else if($refineAns[0] == NULL){}else{ echo "wrongans";}} ?> <?php if($refineAns[0] != "B"){if($getPremium->answer == "B"){ echo "realans";}} ?>">
                            <?php echo $getPremium->optionb;?>
                           </span> <?php if($refineAns[0] == "B"){if($refineAns[0] == $getPremium->answer){echo "<span class='greenCorrect'>Correct</span>";}else if($refineAns[0] == NULL){}else{ echo "<span class='redWrong'>wrong</span>";}} ?><BR>
                <?php
                        if($getPremium->choice != 2) {
                ?>
                        C. <span class="optionc<?php echo $i;?> <?php if($refineAns[0] == "C"){if($refineAns[0] == $getPremium->answer){echo "correctans";}else if($refineAns[0] == NULL){}else{ echo "wrongans";}} ?> <?php if($refineAns[0] != "C"){if($getPremium->answer == "C"){ echo "realans";}} ?>">

                            <?php echo $getPremium->optionc;?>
                          </span> <?php if($refineAns[0] == "C"){if($refineAns[0] == $getPremium->answer){echo "<span class='greenCorrect'>Correct</span>";}else if($refineAns[0] == NULL){}else{ echo "<span class='redWrong'>wrong</span>";}} ?><BR>

                        D. <span class="optiond<?php echo $i;?> <?php if($refineAns[0] == "D"){if($refineAns[0] == $getPremium->answer){echo "correctans";}else if($refineAns[0] == NULL){}else{ echo "wrongans";}} ?> <?php if($refineAns[0] != "D"){if($getPremium->answer == "D"){ echo "realans";}} ?>">
                            <?php echo $getPremium->optiond;?>
                          </span> <?php if($refineAns[0] == "D"){if($refineAns[0] == $getPremium->answer){echo "<span class='greenCorrect'>Correct</span>";}else if($refineAns[0] == NULL){}else{ echo "<span class='redWrong'>wrong</span>";}} ?><BR>
              <?php
                        }
              ?>
                        </div>
                  <?php
                        $i++;
                    }
                  ?>
                        <br>
                        <hr>
                        <br><br>
                </div>
            </div>
         </div>
  </div>
  </main><!-- #main  -->

</div>
<!-- #primary -->
<script>
   function openCity(evt, cityName) {
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("tabcontent");
     for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
     }
     tablinks = document.getElementsByClassName("tablinks");
     for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace(" active", "");
     }
     document.getElementById(cityName).style.display = "block";
     evt.currentTarget.className += " active";
   }
   
   
   jQuery(".showans1").click(function (e) {
     e.stopPropagation();
     jQuery(".answersection").toggleClass('openansedu');
   });
   
   jQuery(".worksps").click(function (e) {
     e.stopPropagation();
     jQuery(".Workspace").toggleClass('openworksps');
   });
   
</script>
<script type="text/javascript">
   function myFunction(obj,id){
      var option = id;
      var clickOption = $(obj).attr('class');
      var checkOption = ".option"+clickOption;

      var ans = $(checkOption).html();
      console.log(id);
      console.log(ans);

      if(id == ans){
         $(checkOption).css('color','green');
      } else{
         $(checkOption).css('color','red');
      }
   }

   $(document).ready(function(){
      $(".tablinks").on('click',function(){
            $(".col-sm-12.quesitionset span").css('color','black');
      });
   });
</script>
<?php get_footer(); ?>
</div>

