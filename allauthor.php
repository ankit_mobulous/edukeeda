<div class="allauthors">
<?php
session_start();
  /**
  /*
   Template Name:  Home Page all author
   */
  if($_SESSION["login"]) {
   } else {
     header('Location: http://edukeeda.com/members-page/');
   }
get_header();
?>
<?php
  if(isset($_POST["followuser"])) {
      $memberid = $_POST["memberid"];
      $userid = $_SESSION["login"]["id"];
      global $wpdb;
      $data = ["follow_id"=>$memberid, "user_id"=>$userid];
      $wpdb->insert("wp_member_follow", $data);
  }
  if(isset($_POST["unfollowuser"])) {
      $memberid = $_POST["memberid"];
      $userid = $_SESSION["login"]["id"];
      global $wpdb;
      $wpdb->query($wpdb->prepare("DELETE FROM wp_member_follow WHERE user_id = %d AND follow_id = %d", $userid, $memberid));
  }
?>
   <div class="col-md-9 col-sm-9 custo">
      <div class="col-md-12">
         <?php
            global $wpdb;
            $table_name = "wp_users";
            $status = 1;
            $countUser = $wpdb->get_results($wpdb->prepare("SELECT Count(ID) as uCount FROM $table_name WHERE user_status = %d", $status));
         ?>
         <h4>Meet Edukeeda Members <span style="font-size: 19px;color: #828282;font-family: serif;">( <?php echo $countUser[0]->uCount; ?> Members )</span></h4>
      </div>
      <div class="col-md-8 col-sm-8">
         <div class="filterkeeda">
            <div class="leftoption">
               <form class="searchform" action=" " method="post" class="search-form">
                  <input type="text" placeholder="Search by name" name="search" class="searchtext">
                  <button type="submit"><i class="fa fa-search"></i></button>
               </form>
            </div>
         </div>
      </div>
      <div class="col-md-4 col-sm-4">
         <div class="rightoptions1">
            <p>Sort By:</p>
            <form action="" method="post" name="myFormName">
             <select name="top_filter" id="topsuccessfilter" onchange='this.form.submit()'>
                <option value=""> Select </option>
                <option value="popular">Most Viewed</option>
                <option value="active">Most Active</option>
                <option value="top">Top Writers</option>
                <option value="follow">Most Followed</option>
             </select>
            </form>
         </div>
      </div>
   </div>
   <div class="col-md-9 customs">
    <?php
      global $wpdb;
      $table_name = "wp_users";
      $status = 1;

      if(isset($_POST["search"])) {
        $searchFilter = $_POST["search"];

        $results = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM $table_name WHERE user_status = %d", $status)
        );
        foreach($results as $result) {
          $userImg = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $result->ID)
          );
          $userHeadline = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM wp_profileStepOne WHERE user_id = %d", $result->ID));
          $userAdd = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $result->ID));
          $userCountry = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
          $userState = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
          $userFollowed = $wpdb->get_results(
              $wpdb->prepare("SELECT COUNT(id) as followed FROM wp_member_follow WHERE follow_id = %d", $result->ID));
          if(stripos($result->user_login, $searchFilter) !== false) {
    ?>

      <div class="col-md-6">
         <div class="allprofiles">
            <div class="rap">
               <div class="col-xs-4 col-md-4">
                  <div class="imgg">
                     <a href="<?php echo site_url();?>/profile?action=<?php echo $result->ID;?>"><img src="<?php if($userImg){echo $userImg[0]->profilePic;}else{echo "http://edukeeda.com//wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                  </div>
               </div>
               <div class="col-xs-8 col-md-8">
                  <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $result->ID;?>"><?php echo $result->user_login;?></a></strong></p>
                  <p class="catp"> <span> <?php if(strlen($userHeadline[0]->headline) > 30){echo substr($userHeadline[0]->headline,0,30) .'...';}else{ echo $userHeadline[0]->headline;} ?> </span></p>
                  <p class="rigwd"><strong> <?php if($userAdd[0]->city){echo $userAdd[0]->city; echo ",";} ?> <?php if($userState[0]->name){echo $userState[0]->name; echo ",";} ?> <?php echo $userCountry[0]->name; ?></strong></p>
               </div>
               <div class="share1">
                <?php
                  if(isset($_SESSION["login"])) {
                    $userId = $_SESSION["login"]["id"];
                    $checkFollow = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM wp_member_follow WHERE user_id = %d AND follow_id=%d", $userId, $result->ID)
                    );

                    if($checkFollow){
                ?>
                      <form method="post" action=" ">
                        <input type="hidden" value="<?php echo $result->ID;?>" name="memberid">
                        <button type="submit" name="unfollowuser" value="Unfollow" class="followbutton"><img src="/img/follow-icon-selected.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button></p>
                      </form>
                <?php
                    } else {
                ?>
                        <form method="post" action=" ">
                          <input type="hidden" value="<?php echo $result->ID;?>" name="memberid">
                          <button type="submit" name="followuser" value="Follow" class="followbutton"><img src="/img/Follow.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button></p>
                        </form>
                <?php
                    }
                  } else {
                ?>
                    <button type="button" class="followbutton" data-toggle="modal" data-target="#myModal"><img src="/img/Follow.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button>
                <?php
                  }
                ?>
               </div>
            </div>
         </div>
      </div>
    <?php
          }
        }
      } else if(isset($_POST['apply_filter'])) {
          $vehicle1 = $_POST["vehicle1"];

        $results = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM $table_name WHERE user_status = %d", $status)
        );
        foreach($results as $result) {

          $titl = strtolower($result->user_login);

          $getFirstTitle = substr("$titl", 0, 1);

         if (in_array($getFirstTitle, $vehicle1)) {
            $userImg = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $result->ID)
            );
            $userHeadline = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_profileStepOne WHERE user_id = %d", $result->ID));

            $userAdd = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $result->ID));
            $userCountry = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
            $userState = $wpdb->get_results(
                $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
            $userFollowed = $wpdb->get_results(
                $wpdb->prepare("SELECT COUNT(id) as followed FROM wp_member_follow WHERE follow_id = %d", $result->ID));
      ?>
          <div class="col-md-6">
             <div class="allprofiles">
                <div class="rap1">
                   <div class="col-xs-4 col-md-4">
                      <div class="imgg">
                         <a href="<?php echo site_url();?>/profile?action=<?php echo $result->ID;?>"><img src="<?php if($userImg){echo $userImg[0]->profilePic;}else{echo "http://edukeeda.com//wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                      </div>
                   </div>
                   <div class="col-xs-8 col-md-8">
                      <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $result->ID;?>"><?php echo $result->user_login;?></a></strong></p>
                      <p class="catp"> <span> <?php if(strlen($userHeadline[0]->headline) > 30){echo substr($userHeadline[0]->headline,0,30) .'...';}else{ echo $userHeadline[0]->headline;} ?> </span></p>
                      <p class="rigwd"><strong> <?php if($userAdd[0]->city){echo $userAdd[0]->city; echo ",";} ?> <?php if($userState[0]->name){echo $userState[0]->name; echo ",";} ?> <?php echo $userCountry[0]->name; ?></strong></p>
                   </div>
                   <div class="share1">
                    <?php
                      if(isset($_SESSION["login"])) {
                        $userId = $_SESSION["login"]["id"];
                        $checkFollow = $wpdb->get_results(
                          $wpdb->prepare("SELECT * FROM wp_member_follow WHERE user_id = %d AND follow_id=%d", $userId, $result->ID)
                        );

                        if($checkFollow){
                    ?>
                          <form method="post" action=" ">
                            <input type="hidden" value="<?php echo $result->ID;?>" name="memberid">
                            <button type="submit" name="unfollowuser" value="Unfollow" class="followbutton"><img src="/img/follow-icon-selected.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button></p>
                          </form>
                    <?php
                        } else {
                    ?>
                            <form method="post" action=" ">
                              <input type="hidden" value="<?php echo $result->ID;?>" name="memberid">
                              <button type="submit" name="followuser" value="Follow" class="followbutton"><img src="/img/Follow.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button></p>
                            </form>
                    <?php
                        }
                      } else {
                    ?>
                        <button type="button" class="followbutton" data-toggle="modal" data-target="#myModal"><img src="/img/Follow.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button>
                    <?php
                      }
                    ?>
                   </div>
                </div>
             </div>
          </div>

      <?php
          }
        }
      }  else if(isset($_POST["top_filter"])) {
          $topFilter = $_POST["top_filter"];

          if($topFilter == "popular") {
            echo "<div><p>Sort by: Most Viewed </p></div>";
             $results = $wpdb->get_results(
                $wpdb->prepare("SELECT * FROM $table_name INNER JOIN wp_member_views ON wp_users.ID = wp_member_views.member_id WHERE wp_users.user_status = %d ORDER BY wp_member_views.view_count DESC", $status));

          } else if($topFilter == "active") {
            echo "<div><p>Sort by: Most Active </p></div>";

              $results = $wpdb->get_results(
                $wpdb->prepare("SELECT wp_users.ID, wp_users.user_login FROM $table_name INNER JOIN wp_discussion_ques ON wp_users.ID = wp_discussion_ques.user_id WHERE wp_users.user_status = %d AND wp_discussion_ques.status='1' GROUP BY wp_discussion_ques.user_id ORDER BY COUNT(*) DESC", $status));

          } else if($topFilter == "top") {
              echo "<div><p>Sort by: Top Writters </p></div>";

              $results = $wpdb->get_results(
                $wpdb->prepare("SELECT wp_users.ID, wp_users.user_login FROM $table_name INNER JOIN wp_posts ON wp_users.ID = wp_posts.post_author WHERE wp_users.user_status = %d AND wp_posts.post_status='publish' GROUP BY wp_posts.post_author ORDER BY COUNT(*) DESC", $status));

          } else if($topFilter == "follow") {
              echo "<div><p>Sort by: Most Followed </p></div>";
              // $results = $wpdb->get_results(
              //   $wpdb->prepare("SELECT * FROM $table_name WHERE user_status = %d ORDER BY user_login ASC", $status));

              $results = $wpdb->get_results(
                $wpdb->prepare("SELECT wp_users.ID, wp_users.user_login FROM $table_name INNER JOIN wp_member_follow ON wp_users.ID = wp_member_follow.follow_id WHERE wp_users.user_status = %d GROUP BY wp_member_follow.follow_id ORDER BY COUNT(wp_member_follow.follow_id) DESC", $status));
          }


        foreach($results as $result) {

            $userImg = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $result->ID)
            );
            $userHeadline = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_profileStepOne WHERE user_id = %d", $result->ID));

            $userAdd = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $result->ID));
            $userCountry = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
            $userState = $wpdb->get_results(
                $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
            $userFollowed = $wpdb->get_results(
                $wpdb->prepare("SELECT COUNT(id) as followed FROM wp_member_follow WHERE follow_id = %d", $result->ID));
      ?>
          <div class="col-md-6">
             <div class="allprofiles">
                <div class="rap1">
                   <div class="col-xs-4 col-md-4">
                      <div class="imgg">
                         <a href="<?php echo site_url();?>/profile?action=<?php echo $result->ID;?>"><img src="<?php if($userImg){echo $userImg[0]->profilePic;}else{echo "http://edukeeda.com/wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                      </div>
                   </div>
                   <div class="col-xs-8 col-md-8">
                      <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $result->ID;?>"><?php echo $result->user_login;?></a></strong></p>
                      <p class="catp"> <span> <?php if(strlen($userHeadline[0]->headline) > 30){echo substr($userHeadline[0]->headline,0,30) .'...';}else{ echo $userHeadline[0]->headline;} ?> </span></p>
                      <p class="rigwd"><strong> <?php if($userAdd[0]->city){echo $userAdd[0]->city; echo ",";} ?> <?php if($userState[0]->name){echo $userState[0]->name; echo ",";} ?> <?php echo $userCountry[0]->name; ?></strong></p>
                   </div>
                   <div class="share1">
                    <?php
                      if(isset($_SESSION["login"])) {
                        $userId = $_SESSION["login"]["id"];
                        $checkFollow = $wpdb->get_results(
                          $wpdb->prepare("SELECT * FROM wp_member_follow WHERE user_id = %d AND follow_id=%d", $userId, $result->ID)
                        );

                        if($checkFollow){
                    ?>
                         <form method="post" action=" ">
                            <input type="hidden" value="<?php echo $result->ID;?>" name="memberid">
                            <button type="submit" name="unfollowuser" value="Unfollow" class="followbutton"><img src="/img/follow-icon-selected.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button></p>
                          </form>
                    <?php
                        } else {
                    ?>
                            <form method="post" action=" ">
                              <input type="hidden" value="<?php echo $result->ID;?>" name="memberid">
                              <button type="submit" name="followuser" value="Follow" class="followbutton"><img src="/img/Follow.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button></p>
                            </form>
                    <?php
                        }
                      } else {
                    ?>
                        <button type="button" class="followbutton" data-toggle="modal" data-target="#myModal"><img src="/img/Follow.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button>
                    <?php
                      }
                    ?>
                   </div>
                </div>
             </div>
          </div>

      <?php
        }
      } else {

        $results = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM $table_name WHERE user_status = %d ORDER BY user_login ASC", $status)
        );
        foreach($results as $result) {
          $userImg = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $result->ID)
          );
          $userHeadline = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM wp_profileStepOne WHERE user_id = %d", $result->ID));

          $userAdd = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $result->ID));
          $userCountry = $wpdb->get_results(
            $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
          $userState = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
          $userFollowed = $wpdb->get_results(
              $wpdb->prepare("SELECT COUNT(id) as followed FROM wp_member_follow WHERE follow_id = %d", $result->ID));
    ?>
   
      <div class="col-md-6">
         <div class="allprofiles">
            <div class="rap1">
               <div class="col-xs-4 col-md-4">
                  <div class="imgg">
                     <a href="<?php echo site_url();?>/profile?action=<?php echo $result->ID;?>">
                      <img src="<?php if($userImg){echo $userImg[0]->profilePic;}else{echo "http://edukeeda.com//wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                    </a>
                  </div>
               </div>
               <div class="col-xs-8 col-md-8">
                  <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $result->ID;?>"><?php echo $result->user_login;?></a></strong></p>
                  <p class="catp"> <span> <?php if(strlen($userHeadline[0]->headline) > 30){echo substr($userHeadline[0]->headline,0,30) .'...';}else{ echo $userHeadline[0]->headline;} ?> </span></p>
                  <p class="rigwd"><strong> <?php if($userAdd[0]->city){echo $userAdd[0]->city; echo ",";} ?> <?php if($userState[0]->name){echo $userState[0]->name; echo ",";} ?> <?php echo $userCountry[0]->name; ?> 
                  </strong></p>
               </div>
               <div class="share1">
                <?php
                  if(isset($_SESSION["login"])) {
                    $userId = $_SESSION["login"]["id"];
                    $checkFollow = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM wp_member_follow WHERE user_id = %d AND follow_id=%d", $userId, $result->ID));

                    if($checkFollow){
                ?>
                  <form method="post" action=" ">
                    <input type="hidden" value="<?php echo $result->ID;?>" name="memberid">
                    <button type="submit" name="unfollowuser" value="Unfollow" class="followbutton"><img src="/img/follow-icon-selected.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button></p>
                  </form>
                <?php
                    } else {
                ?>
                        <form method="post" action=" ">
                          <input type="hidden" value="<?php echo $result->ID;?>" name="memberid">
                          <button type="submit" name="followuser" value="Follow" class="followbutton"><img src="/img/Follow.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button></p>
                        </form>
                <?php
                    }
                  } else {
                ?>
                    <button type="button" class="followbutton" data-toggle="modal" data-target="#myModal"><img src="/img/Follow.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button>
                <?php
                  }
                ?>
               </div>
            </div>
         </div>
      </div>
    <?php
      }
    }
    ?>
   </div>

   <div class="col-md-3 customsideedu">
      <div id="secondary" class="widget-area col-md-3" role="complementary">
         <aside id="recent-posts-3" class="widget widget_recent_entries">

      <div class="spacetop sortbyalphabet"><button class="accordion"><h3 class="widget-title">Name Start With</h3></button>
        <div class="panel">
          <form action=" " method="post">
           <ul>
                <li><input type="checkbox" name="vehicle1[]" value="a"> A</li>
                <li><input type="checkbox" name="vehicle1[]" value="b"> B</li>
                <li><input type="checkbox" name="vehicle1[]" value="c"> C</li>
                <li><input type="checkbox" name="vehicle1[]" value="d"> D</li><br>
                <li><input type="checkbox" name="vehicle1[]" value="e"> E</li>
                <li><input type="checkbox" name="vehicle1[]" value="f"> F</li>
                <li><input type="checkbox" name="vehicle1[]" value="g"> G</li>
                <li><input type="checkbox" name="vehicle1[]" value="h"> H</li><br>
                <li><input type="checkbox" name="vehicle1[]" value="i"> I</li>
                <li><input type="checkbox" name="vehicle1[]" value="j"> J</li>
                <li><input type="checkbox" name="vehicle1[]" value="k"> K</li>
                <li><input type="checkbox" name="vehicle1[]" value="l"> L</li><br>
                <li><input type="checkbox" name="vehicle1[]" value="m"> M</li>
                <li><input type="checkbox" name="vehicle1[]" value="n"> N</li>
                <li><input type="checkbox" name="vehicle1[]" value="o"> O</li>
                <li><input type="checkbox" name="vehicle1[]" value="p"> P</li><br>
                <li><input type="checkbox" name="vehicle1[]" value="q"> Q</li>
                <li><input type="checkbox" name="vehicle1[]" value="r"> R</li>
                <li><input type="checkbox" name="vehicle1[]" value="s"> S</li>
                <li><input type="checkbox" name="vehicle1[]" value="t"> T</li><br>
                <li><input type="checkbox" name="vehicle1[]" value="u"> U</li>
                <li><input type="checkbox" name="vehicle1[]" value="v"> V</li>
                <li><input type="checkbox" name="vehicle1[]" value="w"> W</li>
                <li><input type="checkbox" name="vehicle1[]" value="x"> X</li><br>
                <li><input type="checkbox" name="vehicle1[]" value="y"> Y</li>
                <li><input type="checkbox" name="vehicle1[]" value="z"> Z</li>
           </ul>
            <input type="submit" class="applyfilt" value="apply" name="apply_filter">
          </form>
        </div>

            </div>

         </aside>
      </div>
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
          comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Follow</h4>
         </div>
         <div class="modal-body">
            <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>
<style>
.col-md-9.col-sm-9.custo {
    margin-top: 35px;
}
.custo h4{
  margin-bottom:26px;
}
.imgg .img-thumbnail {
    border-radius: 100%;
    width:100%;
  max-height: 100px;
  height:85px;
}
.accordion {
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 0px!important;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
  transition: 0.4s;
  border-bottom: 1px solid #f68e2f;
}

.active, .accordion:hover {
  background-color: #f4f4f4;
}

.accordion:after {
 content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    margin-top: -29px;
    font-size: 43px;
}

.active:after {
  content: "\2212";
}

.panel {
  padding: 0 2px;
  background-color: #f4f4f4;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
}
   .share1 button {
   padding: 0px 20px;
   padding: 0px 20px;
   background: transparent;
   color: #060658;
   font-weight: 500;
   border: none;
   }
   .imgg {
   float: left;
   margin: 0px 7px;
   width: 100%;
   }
   .filterkeeda {
   float: left;
   width: 100%;
   }
   .allprofiles {
   float: initial;
   box-shadow: -1px 4px 4px 0 rgba(0, 0, 0, 0.07);
   padding: 8px;
   margin-bottom: 20px;
   border: 1px solid #ddd;
   width: 100%;
   min-height: 120px;
   
   }
   p.rigwd {
   margin-bottom: 0px;
   font-size: 14px;
    color: #29296f!important;
   }
   p.catp {
   margin-bottom: 0px;
   line-height: 19px;
   }
   
    p.catp {
    margin-bottom: 0px;
    
}
.catp a {
    color: #f68e2f!important;
}
.rap1{
   float:left;
   width:100%;
}
   
   
   .imgforr {
   text-align: left;
   }
   .share1 {
   margin: 0px 58px;
   }
   .rap {
   float: left;
   width: 100%;
   }
   .imgforr img.img-thumbnail {
   width: 60px;
   height: 60!important;
   border-radius: 100%;
   }
   .catp strong {
   color: #060658;
   font-weight: 600;
   }
   .col-md-9.customs {
    margin-top: 30px;
}
   .share1 img {
   width: 15px;
   height: auto;
   }
   .rightoptions p {
   float: left;
   }
   .rightoptions1 p {
   float: left;
   }
   .leftoption input[type="Text"] {
   width: 50%;
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   padding: 0 16px;
   }
   .rightoptions select {
   background: #fff;
   border: 1px solid #ddd;
   height: 25px;
   line-height: 3px;
   padding: 1px 18px;
   margin: 0 0px 0 27px;
   }
   button, input[type="button"], input[type="reset"], input[type="submit"] {
   position: relative;
   display: inline-block;
   font-family: "Raleway", sans-serif;
   font-size: 15px;
   line-height: 27px;
   font-weight: 700;
   padding: 6px 36px;
   color: #fff;
   text-transform: uppercase;
   -webkit-border-radius: 3px;
   -moz-border-radius: 3px;
   -o-border-radius: 3px;
   border-radius: 3px;
   -webkit-transition: all 0.3s;
   -moz-transition: all 0.3s;
   -ms-transition: all 0.3s;
   -o-transition: all 0.3s;
   transition: all 0.3s;
   }
   .rightoptions1 select {
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   line-height: 3px;
   padding: 1px 18px;
   margin: 0 0px 0 0px;
   }
   .accordion h3.widget-title {
    font-size: 15px;
}
   .alau img.img-thumbnail {
   max-width: 243px;
   max-height: 156px;
   border-radius: 10%;
   }
   
   .customsideedu  .spacetop h4{    margin-bottom: 9px;}
   .alau:hover{
   background: #f9f9f9;
   }
   .alau:hover p{
   color:#f68e2f;
   }
   .alau:hover p.hedlineauedu{ color: #484848;}
   .customsideedu h4.widget-title {
   padding: 0;
   font-size: 13px;
   font-weight: 900;
   }
   div#secondary {
   width: 100%;
   float: left;
   margin-top: 55px;
   }
   .all p{
   margin-bottom: 0px;
   }
   .alla p{
   margin-bottom: 0px;
   font-size: 19px;
   font-weight: 600;
   }
   .alau {
   float: left;
   box-shadow: -1px 4px 4px 0 rgba(0, 0, 0, 0.07);
   padding: 8px;
   margin-bottom: 20px;
   border: 1px solid #ddd;
   text-align: center;
   width: 100%;
   }
   .more a{
   background: #439a37;
   color: #fff;
   padding: 6px 18px;
   font-size: 14px;
   display: inline;
   text-align: center;
   margin: 29px 0;
   }
   .more {
   text-align: center;
   }
   .alla span {
   color: #92797d;
   font-size: 13px;
   }
   .alau .col-md-3{
   padding:0;
   }
   p.hedlineauedu {
   font-size: 14px;
   color: #484848;
   margin-bottom: 5px;
   }
   .follow .followbutton {
   background: #439a37;
   color: #fff;
   padding: 5px 43px;
   font-size: 14px;
   display: inline-block;
   text-align: center;
   }
   .alla p{ color: #020253;}

   .allauthors .container2{
   max-width:1170px;
   margin:0 auto;
   }
   .blo h2{
   font-size: 28px;
   }
   .rightoptions1 {
   text-align: right;
   width: 100%;
   float: right;
   }
   .panel ul li {
       margin: 4px 9px;
    float: left;
    width: 23%;
}
   .sortbyalphabet ul li {
   display: inline-block;
   margin-left: 10px;
    margin-right: 10px;
   }
   input.applyfilt {
   padding: 3px 15px;
   float: right;
   }
   .widget-area .widget-title{    font-size: 22px;}
   .customsideedu h4.widget-title{    font-size: 16px;}
   div#secondary a{    color: #6f605b;}
   p.hedlineauedu strong{color: #777171;

   }
.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
      padding: 15px;
   }

   @media only screen and (max-width: 767px)
   {
    .leftoption input[type="Text"]{
           width:100%;
       }
       .share1 button {
    padding: 0px 0px;
       }
   .div#secondary {
      width: 100%;
    }
   .alau img.img-thumbnail {
       max-width: 100%;
   }
   .share1 {
      margin: 0px 105px;
   }
   .imgg .img-thumbnail {
      border-radius: 100%;
      width: 96px;
      max-height: 100px;
          height: 96px;
    }
  .rap1 .col-xs-4{
    padding:0px;
  }
    .imgg{
     text-align: center;
    }
   div#secondary {
      margin-top: 0px;
   }
   .rightoptions1 p {
   margin-bottom: 5px;
   margin-top: 5px;
    float: left;
    font-size: 18px;
    font-weight: 700;
   }
   .rightoptions1 select {
       background: #fff;
       border: 1px solid #ddd;
       height: 35px;
       line-height: 3px;
        padding: 1px 38px;
     margin: 0px; 
}
   p.catp a {
    font-size: 12px;
}
   .rightoptions1 {
   text-align: right;
   width: 100%;
   float: right;
   }
   .rightoption p {
    margin: 0px 0px;
    font-size: 20px;
    font-weight: 700;
    }
    .searchform input {
      height: 36px!important;
      padding: 4px 13px!important;
 }
  .searchform button {
     margin: 0px 3px;
}

   .allprofiles {
    float: left;
    box-shadow: -1px 4px 4px 0 rgba(0, 0, 0, 0.07);
    padding: 8px;
    margin-bottom: 20px;
    border: 1px solid #ddd;
    width: 100%;
    min-height: 120px;
   }
    .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
      padding: 15px;
        margin-top: 55px;
   }
   h4 {
    font-size: 18px;
    margin-top: 0px;
}
.col-md-9.col-sm-9.custo {
    margin-top: 0px;
}
.page-wrap {
    padding: 0px;
}
.widget-area .widget{
    margin-bottom: 0px;
}
p.catp {
    margin-bottom: 3px;
    line-height: 19px;
    font-size: 14px;
}
   }
</style>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
</script>
