
<div class="eventholder">

<?php
/**
/*
 Template Name: Home Page event description
 */

get_header(); ?>
<div class="container1">
<ul class="breadcrumb">
  <li class="breadcrumb-item"><a href="#">Event&amp;Seminar</a></li>
    <li class="breadcrumb-item"><a href="#">List of Events</a></li>
     <li class="breadcrumb-item"><a href="#"></a></li>
</ul>
<div class="topholder"> 
  <div class="container2">  
  <div class="col-md-8">
    <div class="col-md-2">
     <div class="headdesc">
          <div class="caption"> 
           <div class="datespn">
               <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                     <span class="dateDisplay-day">
                       <span>29</span><br>
                         </span>
                        <span class="dateDisplay-month text--tiny">
                             <span>Nov</span>
                        </span>
                </time>
             </div>
            </div>
        </div>
     </div>
 <div class="col-md-9 spndat">
  <a href="#" class="eventname">Edukeeda Summit 2019</a>
                               <img src="/img/corporate-event-planning.jpg" class="img-thumbnail" alt="Cinque Terre" width="304" height="236">
                      
                       <div class="spn">
             <p> 
             <span><strong>Hosted by</strong> Author</span>   
                 <span><strong>Posted by</strong>  Author</span> 
                 </p> 
                      </div>
</div>
</div>

 <div class="col-md-4">
  <div class="headdescip">
        <h2>Are you going?</h2>
        <button class="button button--fullWidth">Yes</button>
        <button class="button button--fullWidth">May be</button>
          <button class="button button--fullWidth">No</button>
          <div class="shareevents">
          <p><strong>Share</strong><span><a href=""><i class="fa fa-facebook-square"></i></a></span>
            <span><a href=""><i class="fa fa-twitter"></i></a></span>
            <span><a href=""><i class="fa fa-instagram"></i></a></span>
             <span><a href=""><i class="fa fa-whatsapp"></i></a></span>
          </div>



          </p>
     </div>
 </div>
       </div>

 </div>

 
       </div>
</div>

<div class="container2">  
    <div class="col-md-8">
   
     
          <div class="summ">
          <h1>About Event</h1>
          <p>This is SummaryLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem </p>
      </div>
 </div>
 

     <div class="col-md-4">
         
     
                              <div class="dd">
                      
                              <div class="spndate">
                      
                                    <i class="fa fa-clock-o"></i> 10:00 AM - 6:00 PM
                       </div>
                                     <div class="spndate">
                      
                                    <i class="fa fa-calendar"></i> Wednesday, November 28, 2018
                       </div>
                       <div class="spnb">
                     <p> 
                     
                             <span><i class="fa fa-map-marker"></i><strong>Location:</strong>Noida sector144</span>   
                            
                             </p> 
                     
  
            </div>
            
            <div class="spnb">
                     <p> 
                     
                             <span><i class="fa fa-globe"></i><strong>official/Authentic Link:</strong></span><a hrf="#">www.xyz.com</a>   
                            
                             </p> 
                     
  
   </div>
 
   </div>
   
   </div>
<div class="col-md-8">
      
        <section class="comment-list">
          <!-- First Comment -->
          <article class="rowrep">
            <div class="col-md-2 col-sm-2 hidden-xs">
              <figure class="thum">
               <img alt="" src="https://secure.gravatar.com/avatar/68c40a13826bd26e82530065128464d9?s=60&amp;d=mm&amp;r=g" srcset="https://secure.gravatar.com/avatar/68c40a13826bd26e82530065128464d9?s=120&amp;d=mm&amp;r=g 2x" class="avatar avatar-60 photo" height="60" width="60">
                
              </figure>
            </div>
            <div class="col-md-10 col-sm-10">
              <div class="pane">
                <div class="REP">
                  <header class="text-left">
                      <div class="com">
                      <sapn><strong>admin</strong> Says:</span
                      </div>
                    <time class="comment-date" datetime="16-12-2014 01:05"> DECEMBER 3, 2018 at 6:39 AM  EDIT</time>
                  </header>
                  <div class="comment-post">
                    <p>
                      Lorem ipsum dolor sit amet, 
                    </p>
                  </div>
                  <p class="text-right"><a href="#" class="btn btn-default btn-sm"> reply</a></p>
                </div>
              </div>
            </div>
          </article> 
         
        <article class="rowrep">
            <div class="col-md-2 col-sm-2 hidden-xs">
              <figure class="thum">
               <img alt="" src="https://secure.gravatar.com/avatar/68c40a13826bd26e82530065128464d9?s=60&amp;d=mm&amp;r=g" srcset="https://secure.gravatar.com/avatar/68c40a13826bd26e82530065128464d9?s=120&amp;d=mm&amp;r=g 2x" class="avatar avatar-60 photo" height="60" width="60">
                
              </figure>
            </div>
            <div class="col-md-10 col-sm-10">
              <div class="pane">
                <div class="REP">
                  <header class="text-left">
                     <div class="com">
                     <sapn><strong>admin</strong> Says:</span</div>
                    <time class="comment-date" datetime="16-12-2014 01:05">DECEMBER 3, 2018 at 6:39 AM  EDIT</time>
                  </header>
                  <div class="comment-post">
                    <p>
                      Lorem ipsum dolor sit amet,
                    </p>
                  </div>
                  <p class="text-right"><a href="#" class="btn btn-default btn-sm"> reply</a></p>
                </div>
              </div>
            </div>
          </article> 
        
         
          
         
        </section>
    </div>
   <div class="col-md-8">
   <div class="widget-area no-padding blank">
                <div class="status-upload">
                   <p>Leave a Reply </p>
                   <span><a href="" aria-label="Logged in as admin. Edit your profile.">Logged in as admin,Log Out?</a></span>
                    <span>Comment</span>
                  <form>
                    <textarea placeholder="Post Comment" ></textarea>
                    <button type="submit">Post Comment</button>
                  </form>
                </div><!-- Status Upload  -->
              </div>
   </div>
<div class="col-md-4">

  </div>



      <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'content', 'page' ); ?>

        <?php
          // If comments are open or we have at least one comment, load up the comment template
          if ( comments_open() || get_comments_number() ) :
            comments_template();
          endif;
        ?>

      <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

  

<?php get_footer(); ?>
</div>
<style>
.dateDisplay-month.text--tiny span {
    color: #060658;
}
article.rowrep {
    float: left;
    width: 100%;
    margin-bottom: 50px;
    border-bottom: 1px solid #c0cb4a;
}
.pane {
    margin-bottom: 20px;
}
.status-upload button {
    line-height: 15px;
    margin-top: 20px;
}
.pane time.comment-date {
    display: block;
    color: #c5c5c5;
    margin-bottom: 0px;
    line-height: 17px;
    margin-top: 3px;
        FONT-SIZE: 14PX;

}

.pane p {
    margin-top: 20px;
    margin-bottom: 0px;
}
.pane a.btn.btn-default.btn-sm {
      padding: 8px 22px;
    font-size: 13px;
    text-transform: uppercase;
    color: #fff;
    line-height: 1;
    font-weight: 600;
    display: table;
    background: #f68e2f;
        border-radius: inherit;
}
.thum{
  float:right;
}
.pane .text-right {
    text-align: left;
}
.status-upload a {
    color: #f68e2f;
    FLOAT: LEFT;
    FONT-SIZE: 20PX;
    MARGIN-BOTTOM: 11PX;
}
.status-upload p {
    text-align: left;
    color: #020253;
    font-size: 25px;
    margin-bottom: 0px;
}
.comment-list .panel .panel-heading {
  padding: 4px 15px;
  position: absolute;
  border:none;
  /*Panel-heading border radius*/
  border-top-right-radius:0px;
  top: 1px;
}
.comment-list .panel .panel-heading.right {
  border-right-width: 0px;
  /*Panel-heading border radius*/
  border-top-left-radius:0px;
  right: 16px;
}
.comment-list .panel .panel-heading .panel-body {
  padding-top: 6px;
}
.comment-list figcaption {
  /*For wrapping text in thumbnail*/
  word-wrap: break-word;
}
.status-upload span {
    display: block;
    float: left;
    width: 100%;
    text-align: left;
    color: black;
}
/* Portrait tablets and medium desktops */

a.eventname {
    display: block;
    font-size: 29px;
    color: #040404;
    font-weight: 600;
}



.dd{
      background: #f9f9f9;
    padding: 20px;
    border-radius: 10px 10px 10px 10px;
   box-shadow: #cac3c3 0px 0px 0px 0px;
    border: 1px solid#cac3c3;

}
.spndat img.img-thumbnail {
    height: 89px;
    width: 90px;
    border-radius: 100%;
}
.shareevents {
    width: 100%;
    margin: 20px 0;
    border-top: 1px solid #ddd;
}
.spnb span {
    display: block;
}
.headdescip strong {
    font-weight: 600;
    margin: 5px;
    font-size: 18px;
}
.eventholder .container{
    width: 100%;
    padding: 0 !important;
}

.eventholder .row{
    margin:0;
}
.headdescip {
    background: #fff;
    float: left;
    width: 100%;
    padding: 10px;
    box-shadow: #ddd 0px 0px 18px 0px;
    border-radius: 11px 11px 11px 11px;
}
.topholder {
    background: #dddddd30;
    float: left;
    width: 100%;
    box-shadow: -1px 2px 0px 0px #e4dbdb;
    padding: 30px 0;
    margin: 30px 0;
   
}
.spndate i.fa.fa-clock-o {
    margin: 8px;
}
.spndate i.fa.fa-calendar {
    margin: 7px;
}
.spnb strong {
    margin: 13px;
}
.spnb strong {
    margin: 9px;
}
.spnb i.fa.fa-map-marker {
    margin: 10px;
}
.container1 {
}

.container2 {
    max-width: 1170px;
    margin: 0 auto;
}


.breadcrumb {
        background-color: #dddddd30;
    margin-bottom: 0px;
}
.headdescip h2{
      font-size: 22px;
}
.summ h1 {
    font-size: 32px;

}
  
.summ {
    box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
     padding: 20px;
         margin-bottom: 40px;
    
}
.sum{
    box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
    margin-bottom: 0px;
      padding: 20px;
}


.other h4 {
    float: left;
}
.sum h3{
  font-size: 28px;
      margin-bottom: 4px
}
.sum h2{
  font-size: 28px;
      margin-bottom: 4px
}
.sum h4{
  font-size: 28px;
      margin-bottom: 4px
}
.other{
    background-color: #80808024;
    padding: 22px;
    margin-bottom: 20px;
    
}
.other p{
  font-size: 25px;
}
.otherla{
  text-align: center;
}
.sum p {
    margin-bottom: 0px;
}
.summ p {
    margin-bottom: 0px;
	font-size: 20px;
    font-weight: 400;
    font-family: Roboto, sans-serif;
    line-height: 39px;
}
.datespn { 
   position: absolute;
    top: 5px;
    left: 20px;
    background: #f68e2f;
    padding: 7px;
    color: #ffffff;
    font-size: 16px;
    text-align: center;
    border: 1px solid lavenderblush;
    border-radius: 15px;
    font-weight: 600;
}

.row1{
  background: #fff;
    border-bottom: 1px solid rgba(46,62,72,.12);
}
.spn strong {
    font-weight: 600;
    display: block;
    float: left;
}
.spn span {
    display: block;
}
.spndat p {
    font-size: 14px;
    color: black;
    font-weight: 600;
        margin-bottom: 0px;
}

.spndat {
    margin-left: 0px;
}

.spn {
    margin-left: 0px;
}
.headdescip button {
    padding: 10px 25px;
    font-size: 12px;
    line-height: 15px;
}
.headdescip i{
      font-size: 30px;
}

.detailBox {
    width:100%;
    border:1px solid #bbb;
   
        margin-top: 50px;
}
.titleBox {
    background-color:#fdfdfd;
    padding:10px;
}
.titleBox label{
  color:#444;
  margin:0;
  display:inline-block;
}

.commentBox {
    padding:10px;
    border-top:1px dotted #bbb;
}
.commentBox .form-group:first-child, .actionBox .form-group:first-child {
    width:80%;
}
.commentBox .form-group:nth-child(2), .actionBox .form-group:nth-child(2) {
    width:18%;
}
.actionBox .form-group * {
    width:100%;
}
.taskDescription {
    margin-top:10px 0;
}
.commentList {
    padding:0;
    list-style:none;
    max-height:200px;
    overflow:auto;
}
.commentList li {
    margin:0;
    margin-top:10px;
}
.commentList li > div {
    display:table-cell;
}
.commenterImage {
    width:30px;
    margin-right:5px;
    height:100%;
    float:left;
}
.commenterImage img {
    width:100%;
    border-radius:50%;
}
.commentText p {
    margin:0;
}
.sub-text {
    color:#aaa;
    font-family:verdana;
    font-size:11px;
}
.actionBox {
    border-top:1px dotted #bbb;
    padding:10px;
}

.actionBox .form-group * {
    width: 100%;
    padding: 0 8px;
    height: 41px;
}

.spndat img.img-thumbnail {
    float: left;
}

.spn {
    float: left;
  margin: 6px 8px;
}
.spn p{font-weight:600;}
.spn p strong{
  font-weight:100;
      margin: 0 5px 0 0;
}

.spndat img.img-thumbnail {
    height: 60px;
    width: 61px;
    border-radius: 100%;
}

.datespn {
    position: absolute;
    top: 5px;
    left: 20px;
    background: transparent;
        padding: 14px;
    color: #040404;
    font-size: 17px;
    text-align: center;
    border: 1px solid lavenderblush;
    border-radius: 15px;
    font-weight: 800;
    border: 1px solid #f68e2f;
    width: 72px;
}

.datespn span {
    line-height: 16px;
	
}
@media only screen and (max-width: 767px) {
	
    .headdescip
	{margin-top: 40px!important;
	
}

}
</style>
 <script>

 </script>
