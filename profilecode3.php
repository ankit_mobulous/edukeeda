<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;

    if(isset($_POST['submitstepfour'])) {

      $title = trim($_POST["title"]);
      $company_name = trim($_POST["company_name"]);
      $country = trim($_POST["country"]);
      $location = trim($_POST["location"]);
      $from = trim($_POST["from"]);
      $to = trim($_POST["to"]);
      $tablename = "wp_profileStepFour";

      if($_POST["current_working"]) {
          $current_working = $_POST["current_working"];
	  $to = " ";
      } else {
          $current_working = 0;
          $to = $to;
      }
      
      $desc = trim($_POST["desc"]);
      $userId = $_SESSION["login"]["id"];
      
      if(!empty($_POST["pid"])) {
        $checkComp = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_companies WHERE companyName = %s", $company_name));

        if($checkComp) {
          $wpdb->update("wp_profileStepFour", array('company_name' => $checkComp[0]->id), array('id'=>$pid));
        } else {
          $compData = ["companyName"=>$company_name];
          $wpdb->insert("wp_companies", $compData);
          $lastid = $wpdb->insert_id;

          $wpdb->update("wp_profileStepFour", array('company_name' => $lastid), array('id'=>$pid));
        }

        $wpdb->update("wp_profileStepFour", array('title' => $title), array('id'=>$pid));
        $wpdb->update("wp_profileStepFour", array('country' => $country), array('id'=>$pid));
        $wpdb->update("wp_profileStepFour", array('location' => $location), array('id'=>$pid));
        $wpdb->update("wp_profileStepFour", array('from' => $from), array('id'=>$pid));
        $wpdb->update("wp_profileStepFour", array('to' => $to), array('id'=>$pid));
        $wpdb->update("wp_profileStepFour", array('current_working' => $current_working), array('id'=>$pid));
        $wpdb->update("wp_profileStepFour", array('desc' => $desc), array('id'=>$pid));
        
        if(!empty($_POST["emember"])) {
            header("Location: https://edukeeda.com/all-members/homepage?action=$userId");
        } else{
            $session["sessaddedu"] = "done";
            header('Location: https://edukeeda.com/edit-profile/');
        }
      } else {
          $checkComp = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_companies WHERE companyName = %s", $company_name));

          if($checkComp) {
          
            $data = array(
                  "user_id" => $userId,
                  "title" => $title,
                  "company_name" => $checkComp[0]->id,
                  "country" => $country,
                  "location" => $location,
                  "from" => $from,
                  "to" => $to,
                  "current_working" => $current_working,
                  "desc" => $desc
                );
            $result = $wpdb->insert($tablename, $data);
          } else {
              $compData = ["companyName"=>$company_name];
              $wpdb->insert("wp_companies", $compData);
              $lastid = $wpdb->insert_id;

              $data = array(
                  "user_id" => $userId,
                  "title" => $title,
                  "company_name" => $lastid,
                  "country" => $country,
                  "location" => $location,
                  "from" => $from,
                  "to" => $to,
                  "current_working" => $current_working,
                  "desc" => $desc
                );
            $result = $wpdb->insert($tablename, $data);
          }
        
          if(!empty($_POST["emember"])){
            header("Location: https://edukeeda.com/all-members/homepage?action=$userId");
          } else{
              header('Location: https://edukeeda.com/edit-profile?addexperience=Experience added successfully');
          }
      }

    }
    
  if(isset($_POST["stepFourDelete"])) {
      $id = $_POST["stepFourId"];
      //var_dump($id);die;
      $wpdb->query($wpdb->prepare("DELETE FROM wp_profileStepFour WHERE id = %d", $id));
      header('Location: https://edukeeda.com/edit-profile/');
  }
    
?>
