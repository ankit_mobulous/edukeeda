<div class="eventholder">
   <?php
      session_start();
      get_header();
          $postid = get_the_ID();
      
          if(isset($_POST["yes"])) {
              $userId = $_SESSION["login"]["id"];
      
              $checkEntry = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_event_status WHERE event_id = %d AND user_id = %d", $postid, $userId) 
              );
              if($checkEntry) {
      
              } else {
                $dataGoing = array("event_id"=>$postid, "user_id"=>$userId, "status"=> 1);
                
                $wpdb->insert("wp_event_status", $dataGoing);
              }
          }
          if(isset($_POST["may"])) {
              $userId = $_SESSION["login"]["id"];
              $checkEntry = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_event_status WHERE event_id = %d AND user_id = %d", $postid, $userId) 
              );
              if($checkEntry) {} else {
                $dataGoing = array("event_id"=>$postid, "user_id"=>$userId, "status"=> 2);
                $wpdb->insert("wp_event_status", $dataGoing);
              }
          }
      
          if(isset($_POST["cyes"])) {
              $userId = $_SESSION["login"]["id"];
      
              $wpdb->query($wpdb->prepare("DELETE FROM wp_event_status WHERE event_id = %d AND user_id = %d AND status ='1'", $postid, $userId));
          }
          if(isset($_POST["cmay"])) {
              $userId = $_SESSION["login"]["id"];
              $wpdb->query($wpdb->prepare("DELETE FROM wp_event_status WHERE event_id = %d AND user_id = %d AND status ='2'", $postid, $userId));
          }
      
           $table_name = "wp_post_views";
            $checkViews = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $postid) 
            );
            if($checkViews){
                $valView = $checkViews[0]->view_count + 1;
                $wpdb->update($table_name, array('view_count'=>$valView), array('post_id'=>$postid));
            } else {
                $data = array("post_id" =>$postid,"view_count"=>1);
                $wpdb->insert($table_name, $data);
            }
           
           $post_data = get_post($postid,ARRAY_A); 
           $author_image = $post_data['post_author'];
          
           $featured_img_url = get_the_post_thumbnail_url($post_data['ID'], 'full');
           if($featured_img_url){
                $featured_img_url = $featured_img_url;
            } else {
                $mkey = "_thumbnail_id";
              $checkimg = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $post_data['ID'],$mkey) 
              );
              $getimg = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
              );
              $featured_img_url = $getimg[0]->guid;  
            }
            $eventDate = get_field('event_date', $result1->ID);
            
            $results11 = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE object_id = %d", $postid) 
            );
            $results111 = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_terms WHERE term_id = %d", $results11[0]->term_taxonomy_id) 
            );

          ?>
   <div class="container1">
      <div class="topholder">
         <div class="container2">
            <div class="col-md-12">
                <div class="meta-post" style="margin-left: 2%;margin-bottom: 2%;">
                    <span> <a href="<?php site_url();?>/events-list?id=<?php echo $results111[0]->term_id;?>"><?php echo $results111[0]->name;?> </a> </span>
                </div>
            </div>
            <div class="col-md-8">
               <div class="col-xs-3 col-md-2">
                  <div class="headdesc">
                     <div class="caption">
                        <div class="datespn">
                           <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                           <span class="dateDisplay-day">
                           <span><?php echo date('d',strtotime($eventDate)); ?></span><br>
                           </span>
                           <span class="dateDisplay-month text--tiny">
                           <span><?php echo date('M',strtotime($eventDate)); ?></span>
                           </span>
                           </time>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-xs-9 col-md-9 spndat">
                  <a href="#" class="eventname"><?php echo $post_data['post_title'] ;?></a>
                  <div class="evntimgs">
                     <?php echo get_avatar( get_the_author_meta('ID'), 60); ?>
                  </div>
                  <div class="spn">
                     <p>  
                        <span><strong>Posted by</strong> <a href="<?php echo site_url();?>/profile/?action=<?php echo $post_data['post_author'];?>"><?php the_author_meta( 'display_name', $post_data['post_author'] ); ?> </a></span> 
                        <span><strong>Posted on</strong> <?php the_time('d-M-y'); ?> </span> 
                     </p>
                  </div>
               </div>
            </div>
            <div class="col-md-4">
               <div class="headdescip">
                  <h2>Are you going?</h2>
                  <?php
                     if($_SESSION["login"]) {
                       $userId = $_SESSION["login"]["id"];
                       $checkStatus = $wpdb->get_results( 
                         $wpdb->prepare( "SELECT * FROM wp_event_status WHERE event_id = %d AND user_id=%d", $postid, $userId)
                       );
                       if($checkStatus) {
                     ?>
                  <form action=" " method="post">
                     <?php if($checkStatus[0]->status == "2"){
                        ?>
                     <button type="button" name="cyes" class="button button--fullWidth">Yes</button>
                     <?php
                        } else {
                        ?>    <button type="submit" name="cyes" class="button button--fullWidth">Yes
                     <?php
                        if($checkStatus[0]->status == "1") { echo "<img src='/img/green-light-tick-mark-md.png' style='width: 16px;float:right;margin-left: 10px;'>";}
                        ?>
                     </button>
                     <?php
                        }
                            if($checkStatus[0]->status == "1") {
                        ?>
                     <button  type="button" name="cmay" class="button button--fullWidth">May be</button>
                     <?php
                        } else {
                        ?>
                     <button  type="submit" name="cmay" class="button button--fullWidth">May be
                     <?php if($checkStatus[0]->status == "2"){ echo "<img src='/img/green-light-tick-mark-md.png' style='width: 16px;float:right;margin-left: 10px;'>"; }?>
                     </button>
                     <?php 
                        }
                        ?>
                  </form>
                  <?php
                     } else {
                     ?>
                  <form action=" " method="post">
                     <button type="submit" name="yes" class="button button--fullWidth">Yes</button>
                     <button  type="submit" name="may" class="button button--fullWidth">May be</button>
                  </form>
                  <?php
                     }
                     } else {
                     ?>
                  <button class="button button--fullWidth" data-toggle="modal" data-target="#myModalEvent">Yes</button>
                  <button class="button button--fullWidth" data-toggle="modal" data-target="#myModalEvent">May be</button>
                  <?php
                     }
                     ?>
                  <div class="shareevents">
                     <p><?php echo do_shortcode('[Sassy_Social_Share type="standard"]') ?></p>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div class="container2">
   <div class="col-md-8">
      <div class="summ">
         <p><img src="<?php echo $featured_img_url; ?>"><br></p>
         <BR><BR>
         <p><?php echo $post_data['post_content'] ;?></p>
      </div>
   </div>
   <div class="col-md-4">
      <div class="dd">
         <div class="spndate">
            <p> 
               <span>
               <i class="fa fa-user-circle"></i>Hosted by: <?php echo get_field( "hosted_by", $post_data['ID']); ?>
               </span>
            </p>
         </div>
         <div class="spndate">
            <p> 
               <span>
               <i class="fa fa-clock-o"></i><?php if(get_field('event_time')) { ?>From: <?php echo get_field('event_time'); }?> <?php if(get_field('event_time_to')) { ?>To: <?php echo get_field('event_time_to'); }?>
               </span>
            </p>
         </div>
         <div class="spndate">
            <p> 
               <span>
               <i class="fa fa-calendar"></i><?php echo get_field('event_date'); ?> - <?php echo get_field('event_date_end'); ?>
               </span>
            </p>
         </div>
         <div class="spnb">
            <p> 
               <span><i class="fa fa-map-marker"></i><strong>Location:</strong><BR><span class="subtexts"><?php echo get_field('event_location'); ?></span></span>   
            </p>
         </div>
         <div class="spnb">
            <p> 
               <span><i class="fa fa-globe"></i><strong>official/Authentic Link:</strong><BR><span class="subtexts"><?php echo get_field('officialauthentic_link'); ?></span></span>   
            </p>
         </div>
      </div>
   </div>
   <div class="col-md-8">
      <main id="main" class="post-wrap" role="main">
         <?php while ( have_posts() ) : the_post(); ?>
         <hr class="hrtagstyleborder">

         <div class="row">
            <div class="col-md-12 statusgoingcheck">
               <div class="headdescip">
                <?php
                  $checkCountYes = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_event_status WHERE event_id = %d AND status='1' LIMIT 4", $postid));
                  $CountYes = count($checkCountYes);
                ?>
                  <h2>People going: <spane> ( <?php echo $CountYes; ?> )</span></h2>
                  <a data-toggle="modal" data-target="#myModalsee">See all</a> 
               </div>
              
              <?php
                $checks = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_event_status WHERE event_id = %d AND status='1' LIMIT 4", $postid));

                if($checks) {
                  foreach($checks as $check) {

                    $userName = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM wp_users WHERE ID = %d", $check->user_id));
                    
                    $userImg = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $check->user_id)
                    );
                    
                    $userHeadline = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM wp_profileStepOne WHERE user_id = %d", $check->user_id));

                    $userAdd = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $check->user_id));
                    
                     $userCountry = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
                    $userState = $wpdb->get_results(
                        $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
              ?>               

               <div class="col-md-6 col-xs-12">
                  <div class="allprofiles">
                     <div class="col-xs-4 col-md-4">
                        <div class="imgg">
                           <a href="<?php echo site_url();?>/profile?action=<?php echo $check->user_id;?>">
                           <img src="<?php if($userImg){echo $userImg[0]->profilePic;}else{echo "https://edukeeda.com/wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                           </a>
                        </div>
                     </div>
                     <div class="col-xs-8 col-md-8">
                        <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $check->user_id;?>"><?php echo $userName[0]->user_login;?></a></strong>,
                         <p class="catp"> <?php if(strlen($userHeadline[0]->headline) > 30){echo substr($userHeadline[0]->headline,0,30).'...';} ?> </p>
                        <p class="rigwd"><strong> <?php if($userAdd[0]->city){echo $userAdd[0]->city; echo ",";} ?> <?php if($userState[0]->name){echo $userState[0]->name; echo ",";} ?> <?php echo $userCountry[0]->name; ?> 
                           </strong>
                        </p>
                     </div>
                  </div>
               </div>
              <?php
                    }
                  }
              ?>

               <div class="headdescips">
              <?php
                  $checkCountmay = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_event_status WHERE event_id = %d AND status='2' LIMIT 4", $postid));
                  $CountMay = count($checkCountmay);
              ?>
                  <h2>People May Be going: <spane> ( <?php echo $CountMay; ?> )</h2>
                  <a data-toggle="modal" data-target="#myModalseeall">See all</a> 
               </div>
               <?php
                $checks = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_event_status WHERE event_id = %d AND status='2' LIMIT 4", $postid));

                if($checks) {
                  foreach($checks as $check) {

                    $userName = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM wp_users WHERE ID = %d", $check->user_id));
                    
                    $userImg = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $check->user_id)
                    );
                    
                    $userHeadline = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM wp_profileStepOne WHERE user_id = %d", $check->user_id));

                    $userAdd = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $check->user_id));
                    
                     $userCountry = $wpdb->get_results(
                      $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
                    $userState = $wpdb->get_results(
                        $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
              ?>
               <div class="col-md-6 col-xs-12">
                  <div class="allprofiles">
                     <div class="col-xs-4 col-md-4">
                        <div class="imgg">
                           <a href="<?php echo site_url();?>/profile?action=<?php echo $check->user_id;?>">
                           <img src="<?php if($userImg){echo $userImg[0]->profilePic;}else{echo "http://edukeeda.com/wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                           </a>
                        </div>
                     </div>
                     <div class="col-xs-8 col-md-8">
                        <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $check->user_id;?>"><?php echo $userName[0]->user_login;?></a></strong>,
                         <p class="catp"> <?php if(strlen($userHeadline[0]->headline) > 30){echo substr($userHeadline[0]->headline,0,30).'...';} ?> </p>
                        <p class="rigwd"><strong> <?php if($userAdd[0]->city){echo $userAdd[0]->city; echo ",";} ?> <?php if($userState[0]->name){echo $userState[0]->name; echo ",";} ?> <?php echo $userCountry[0]->name; ?> 
                           </strong>
                        </p>
                     </div>
                  </div>
               </div>
            <?php
                    }
                  }
              ?>
            </div>
         </div>

         <hr class="hrtagstyleborder">
         <?php
            // If comments are open or we have at least one comment, load up the comment template
            if ( comments_open() || get_comments_number() ) :
               comments_template();
            endif;
            ?>
         <?php endwhile; // end of the loop. ?>
      </main>
      <!-- #main -->   
   </div>
   <div class="col-md-4">
   </div>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>
<style>
   .allprofiles{
   float:left;
   width:100;
   background:#fff!important;
   }
   .statusgoingcheck .allprofiless {
   float: left;
   box-shadow: -1px 4px 4px 0 rgba(0, 0, 0, 0.07);
   padding: 8px;
   margin-bottom: 20px;
   border: 1px solid #ddd;
   width: 100%;
   min-height: 120px;
   margin-top: 40px;
   }
   .statusgoingcheck .headdescips {
   float: left;
   width: 100%;
   margin-bottom: 20px;
   }
   .statusgoingcheck .allprofiles
   {
   margin-top: 0px; 
   min-height: 0px;
   ba    
   }
   .statusgoingcheck .share1 {
   margin-bottom: -22px;
   }
   .statusgoingcheck .share1 button {
   background: transparent;
   color: #060658;
   font-weight: 500;
   border: none;
   }
   .statusgoingcheck .headdescip h2, .headdescips h2 {
   font-size: 22px;
   float: left;
   margin: 0px 20px;
   }
   .share1 form {
   float: left;
   padding: 0px;
   margin: 0px -20px;
   }
   .statusgoingcheck .headdescip {
   padding: 0px;
   margin-top: 20px;
   margin-bottom:20px;
   background: #f6f6f6;
   box-shadow: #f7f7f7 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   }
   .statusgoingcheck .imgg .img-thumbnail
   {
   max-height: 100px;
   width: 60px;
   height: 60px;
   border-radius: 100%;
   }
   .statusgoingcheck p.rigwd {
   margin-bottom: 0px;
   font-size: 13px;
   }
   .statusgoingcheck p.catp {
   margin-bottom: 0px;
   line-height: 19px;
   font-size: 13px;
   }
   .allprofiles {
   float: left;
   box-shadow: -1px 4px 4px 0 rgba(0, 0, 0, 0.07);
   padding: 8px;
   margin-bottom: 20px;
   border: 1px solid #ddd;
   width: 100%;
   min-height: 120px;
   margin-top: 40px;
   }
   .imgg .img-thumbnail {
   border-radius: 100%;
   width: 100%;
   max-height: 100px;
   height: 100px;
   }
   p.catp {
   margin-bottom: 0px;
   line-height: 19px;
   }
   p.rigwd {
   margin-bottom: 0px;
   font-size: 14px;
   }
   .catp strong {
   color: #060658;
   font-weight: 600;
   font-size:12px;
   }
   .share1 button {
   padding: 0px 20px;
   padding: 0px 20px;
   background: transparent;
   color: #060658;
   font-weight: 500;
   border: none;
   }
   .share1 img {
   width: 15px;
   height: auto;
   }
   .col-md-6.goingmember p {
   margin: 10px 0px;
   }
   .col-md-6.goingmember span {
   margin: 0px 5px;
   }.col-md-6.goingstatus p {
   margin: 15px 0px;
   }
   .statusgoingcheck{
   width: 100%;
   }
   .goingmember p img{
   width: 50px;
   height: 50px;
   border-radius: 100%;
   }
   .spnb i.fa.fa-globe {
   margin: 9px;
   }
   }
   .statusgoingcheck .goinglist{
   border-bottom: 1px solid #C0C0C0;
   margin-top: 10px;
   }
   .statusgoingcheck{
   background-color: #f7f7f7;
   }
   article.rowrep {
   float: left;
   width: 100%;
   margin-bottom: 50px;
   border-bottom: 1px solid #ffc0cb4a;
   }
   .pane {
   margin-bottom: 20px;
   }
   .status-upload button {
   line-height: 15px;
   margin-top: 20px;
   }
   .pane time.comment-date {
   display: block;
   color: #c5c5c5;
   margin-bottom: 0px;
   line-height: 17px;
   margin-top: 3px;
   FONT-SIZE: 14PX;
   }
   .pane p {
   margin-top: 20px;
   margin-bottom: 0px;
   }
   .pane a.btn.btn-default.btn-sm {
   padding: 8px 22px;
   font-size: 13px;
   text-transform: uppercase;
   color: #fff;
   line-height: 1;
   font-weight: 600;
   display: table;
   background: #f68e2f;
   border-radius: inherit;
   }
   .thum{
   float:right;
   }
   .pane .text-right {
   text-align: left;
   }
   .status-upload a {
   color: #f68e2f;
   FLOAT: LEFT;
   FONT-SIZE: 20PX;
   MARGIN-BOTTOM: 11PX;
   }
   .status-upload p {
   text-align: left;
   color: #020253;
   font-size: 25px;
   margin-bottom: 0px;
   }
   .comment-list .panel .panel-heading {
   padding: 4px 15px;
   position: absolute;
   border:none;
   /*Panel-heading border radius*/
   border-top-right-radius:0px;
   top: 1px;
   }
   .comment-list .panel .panel-heading.right {
   border-right-width: 0px;
   /*Panel-heading border radius*/
   border-top-left-radius:0px;
   right: 16px;
   }
   .comment-list .panel .panel-heading .panel-body {
   padding-top: 6px;
   }
   .comment-list figcaption {
   /*For wrapping text in thumbnail*/
   word-wrap: break-word;
   }
   .status-upload span {
   display: block;
   float: left;
   width: 100%;
   text-align: left;
   color: black;
   }
   /* Portrait tablets and medium desktops */
   a.eventname {
   display: block;
   font-size: 25px;
   color: #040404;
   font-weight: 600;
   }
   .dd{
   background: #f9f9f9;
   padding: 20px;
   border-radius: 10px 10px 10px 10px;
    box-shadow: #cac3c3 0px 0px 0px 0px;
    border: 1px solid#cac3c3;
   
   }
   .spndat img.img-thumbnail {
   height: 89px;
   width: 90px;
   border-radius: 100%;
   }
   .shareevents {
   width: 100%;
   margin: 20px 0;
   border-top: 1px solid #ddd;
   }
   .spnb span {
   display: block;
   }
   .headdescip strong {
   font-weight: 600;
   margin: 5px;
   font-size: 18px;
   }
   .eventholder .container{
   padding: 0 !important;
   }
   .eventholder .row{
   margin:0;
   }
   .headdescip {
   background: #fff;
   float: left;
   width: 100%;
   padding: 10px;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   }
   .topholder {
   background: #dddddd30;
   float: left;
   width: 100%;
   box-shadow: -1px 2px 0px 0px #e4dbdb;
   padding: 30px 0;
   margin: 0px 0;
   margin-bottom: 30px;
   }
   .spndate i.fa.fa-user-circle {
   margin: 8px;
   }
   .spndate i.fa.fa-clock-o {
   margin: 8px;
   }
   .spndate i.fa.fa-calendar {
   margin: 7px;
   }
   .spnb strong {
   margin: 1px;
   }
   .spnb i.fa.fa-map-marker {
   margin: 10px;
   }
   .container1 {
   }
   .container2 {
   max-width: 1170px;
   margin: 0 auto;
   }
   .spndate p {
   margin-bottom: 0px;
   }
   .breadcrumb {
   background-color: #dddddd30;
   margin-bottom: 0px;
   }
   .headdescip h2{
   font-size: 22px;
   }
   .summ h1 {
   font-size: 32px;
   }
   .summ {
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   padding: 20px;
   margin-bottom: 40px;
   }
   .sum{
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   margin-bottom: 0px;
   padding: 20px;
   }
   .other h4 {
   float: left;
   }
   .sum h3{
   font-size: 28px;
   margin-bottom: 4px
   }
   .sum h2{
   font-size: 28px;
   margin-bottom: 4px
   }
   .sum h4{
   font-size: 28px;
   margin-bottom: 4px
   }
   .other{
   background-color: #80808024;
   padding: 22px;
   margin-bottom: 20px;
   }
   .other p{
   font-size: 25px;
   }
   .otherla{
   text-align: center;
   }
   .sum p {
   margin-bottom: 0px;
   }
   .summ p {
   margin-bottom: 0px;
   font-size: 20px;
   font-weight: 400;
   font-family: serif !important;
   line-height: 37px;
   text-align: justify;
   }
   .row1{
   background: #fff;
   border-bottom: 1px solid rgba(46,62,72,.12);
   }
   .spn strong {
   font-weight: 600;
   display: block;
   float: left;
   }
   .spn span {
   display: block;
   }
   .spndat p {
   font-size: 14px;
   color: black;
   font-weight: 600;
   margin-bottom: 0px;
   }
   .spndat {
   margin-left: 0px;
   margin-top: -27px;
   }
   .spn {
   margin-left: 0px;
   }
   .headdescip button {
   padding: 10px 25px;
   font-size: 12px;
   line-height: 15px;
   }
   .headdescip i{
   font-size: 30px;
   }
   .detailBox {
   width:100%;
   border:1px solid #bbb;
   margin-top: 50px;
   }
   .titleBox {
   background-color:#fdfdfd;
   padding:10px;
   }
   .titleBox label{
   color:#444;
   margin:0;
   display:inline-block;
   }
   .commentBox {
   padding:10px;
   border-top:1px dotted #bbb;
   }
   .commentBox .form-group:first-child, .actionBox .form-group:first-child {
   width:80%;
   }
   .commentBox .form-group:nth-child(2), .actionBox .form-group:nth-child(2) {
   width:18%;
   }
   .actionBox .form-group * {
   width:100%;
   }
   .taskDescription {
   margin-top:10px 0;
   }
   .commentList {
   padding:0;
   list-style:none;
   max-height:200px;
   overflow:auto;
   }
   .commentList li {
   margin:0;
   margin-top:10px;
   }
   .commentList li > div {
   display:table-cell;
   }
   .commenterImage {
   width:30px;
   margin-right:5px;
   height:100%;
   float:left;
   }
   .commenterImage img {
   width:100%;
   border-radius:50%;
   }
   .commentText p {
   margin:0;
   }
   .sub-text {
   color:#aaa;
   font-family:verdana;
   font-size:11px;
   }
   .actionBox {
   border-top:1px dotted #bbb;
   padding:10px;
   }
   .actionBox .form-group * {
   width: 100%;
   padding: 0 8px;
   height: 41px;
   }
   .spndat img.img-thumbnail {
   float: left;
   }
   .spn {
   float: left;
   margin: 6px 8px;
   }
   .spn p{font-weight:600;}
   .spn p strong{
   font-weight:100;
   margin: 0 5px 0 0;
   }
   .spndat img.img-thumbnail {
   height: 60px;
   width: 61px;
   border-radius: 100%;
   }
   .datespn {
   position: absolute;
   top: 5px;
   left: 20px;
   background: transparent;
   padding: 14px;
   color: #040404;
   font-size: 17px;
   text-align: center;
   border: 1px solid lavenderblush;
   border-radius: 15px;
   font-weight: 800;
   border: 1px solid #f68e2f;
   width: 72px;
   }
   .datespn span {
   line-height: 16px;
   color: #e96125; 
   }
   .dateDisplay-month.text--tiny span {
   color: #060658;
   }
   img.avatar.avatar-60.photo.avatar-default{float: left; border-radius: 50%;}
   .spnb p{margin: 0px !important;}
   .spnb p .subtexts {
   margin: 0px !important;
   padding: 0px !important;
   margin-left: 44px !important;
       font-size: 17px;
   }
   .statusgoingcheck {
   background-color: #f7f7f7;
   }
   .commenttoggle .btn-lg{
   left:0%;
   }
   .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper{
   width: 100%;
   }
   .modal-content .allprofiles{
   margin:5px 0px;
   min-height: 100px;
    margin-top: 20px;
    margin-bottom: 0px;
   }
   #myModalsee .modal-content {
   height: 500px;
   overflow-y: scroll;
   
   }
   #myModalseeall .modal-content {
   height: 500px;
   overflow-y: scroll;
  
   }
   
   
   
   @media only screen and (max-width: 767px)


   {
     .statusgoingcheck{
           background-color: #f7f7f7;
    float: left;
     }
     .statusgoingcheck .headdescip h2, .headdescips h2 {
    font-size: 14px;
    float: left;
    margin: 0px 0px;
}
   #myModalsee .modal-content p.catp {
   margin-bottom: 0px;
   line-height: 19px;
   font-size: 13px;
   }
   .modal-content .allprofiles{
   margin:5px 0px;
   min-height: 100px;
   }
   .container2 .col-md-4 {
   padding: 2px;
   }
   .eventholder .container {
   padding: 8px 15px !important;
   }
   .container2 .col-md-8 {
   padding: 2px;
   }
   .col-md-6.goingstatus {
   text-align: center;
   }
   .evntimgs {
   float: left;
       width: 28%;
    margin-top: 10px;
   }
   .spn {
   float: right;
      margin: 9px 1px;
    width: 68%;
   }
   .summ img {
   width: 100%;
   height: auto;
   }
   a.eventname {
   display: block;
   font-size: 20px;
   color: #040404;
   font-weight: 600;
   line-height: 23px;
   margin-top: 11px;
   }
  .spn span {
    display: block;
    font-size: 13px;
}
   .datespn {
   float: none;
   left: 7%;
   }
   .foot {
   float: left;
   width: 100%;
   
   }
   div#myModalEvent button.close {
    opacity: 1;
}
   .footer-widgets .widget-title {
   text-transform: uppercase;
   color: #c5c5c5;
   line-height: normal;
   margin: 7px 10px 0;
   }
   .menu-footer-menu-container {
   float: left;
   width: 100%;
   margin: 0px 10px;
   }

   .container1 {
   float: left;
   width: 100%;
  padding:15px 0px;
   }
   .statusgoingcheck {
   width: 100%;
   margin-top: 20px;
   }
   .summ strong {
    font-size: 18px;
      line-height: 20px;
}
   }
</style>
<script></script>

<!-- Modal -->
<div id="myModalsee" class="modal fade" role="dialog">
   <div class="modal-dialog">
       <div class="modal-header" style="background: #fff;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Members</h4>
        </div>
      <!-- Modal content-->
      <div class="modal-content">
      <?php
        $checks = $wpdb->get_results( 
          $wpdb->prepare( "SELECT * FROM wp_event_status WHERE event_id = %d AND status='1'", $postid));

        if($checks) {
          foreach($checks as $check) {

            $userName = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_users WHERE ID = %d", $check->user_id));
            
            $userImg = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $check->user_id)
            );
            
            $userHeadline = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_profileStepOne WHERE user_id = %d", $check->user_id));

            $userAdd = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $check->user_id));
            
             $userCountry = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
            $userState = $wpdb->get_results(
                $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
      ?>   
         <div class="col-md-6 col-xs-12">
            <div class="allprofiles">
               <div class="col-xs-4 col-md-4">
                  <div class="imgg">
                     <a href="<?php echo site_url();?>/profile?action=<?php echo $check->user_id;?>">
                     <img src="<?php if($userImg){echo $userImg[0]->profilePic;}else{echo "http://edukeeda.com/wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                     </a>
                  </div>
               </div>
               <div class="col-xs-8 col-md-8">
                  <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $check->user_id;?>"><?php echo $userName[0]->user_login;?></a></strong>,
                   <p class="catp"> <?php if(strlen($userHeadline[0]->headline) > 30){echo substr($userHeadline[0]->headline,0,30).'...';} ?> </p>
                  <p class="rigwd"><strong> <?php if($userAdd[0]->city){echo $userAdd[0]->city; echo ",";} ?> <?php if($userState[0]->name){echo $userState[0]->name; echo ",";} ?> <?php echo $userCountry[0]->name; ?> 
                     </strong>
                  </p>
               </div>
            </div>
         </div>
      <?php
            }
          }
      ?>
      </div>
   </div>
</div>
<div id="myModalseeall" class="modal fade" role="dialog">
   <div class="modal-dialog">
       <div class="modal-header" style="background: #fff;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Members</h4>
        </div>
      <!-- Modal content-->
      <div class="modal-content">
      <?php
        $checks = $wpdb->get_results( 
          $wpdb->prepare( "SELECT * FROM wp_event_status WHERE event_id = %d AND status='2'", $postid));

        if($checks) {
          foreach($checks as $check) {

            $userName = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_users WHERE ID = %d", $check->user_id));
            
            $userImg = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $check->user_id)
            );
            
            $userHeadline = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_profileStepOne WHERE user_id = %d", $check->user_id));

            $userAdd = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $check->user_id));
            
             $userCountry = $wpdb->get_results(
              $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
            $userState = $wpdb->get_results(
                $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
      ?>   
         <div class="col-md-6 col-xs-12">
            <div class="allprofiles">
               <div class="col-xs-4 col-md-4">
                  <div class="imgg">
                     <a href="<?php echo site_url();?>/profile?action=<?php echo $check->user_id;?>">
                     <img src="<?php if($userImg){echo $userImg[0]->profilePic;}else{echo "https://edukeeda.com/wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                     </a>
                  </div>
               </div>
               <div class="col-xs-8 col-md-8">
                  <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $check->user_id;?>"><?php echo $userName[0]->user_login;?></a></strong>,
                   <p class="catp"> <?php if(strlen($userHeadline[0]->headline) > 30){echo substr($userHeadline[0]->headline,0,30).'...';} ?> </p>
                  <p class="rigwd"><strong> <?php if($userAdd[0]->city){echo $userAdd[0]->city; echo ",";} ?> <?php if($userState[0]->name){echo $userState[0]->name; echo ",";} ?> <?php echo $userCountry[0]->name; ?> 
                     </strong>
                  </p>
               </div>
            </div>
         </div>
      <?php
            }
          }
      ?>
      </div>
   </div>
</div>
<div id="myModalEvent" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Are you going?</h4>
         </div>
         <div class="modal-body">
            <p> Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> first!</p>
         </div>
      </div>
   </div>
</div>

