<?php
    $path = $_SERVER['DOCUMENT_ROOT'];
    include_once $path . '/wp-config.php';
    include_once $path . '/wp-load.php';
    include_once $path . '/wp-includes/wp-db.php';
    include_once $path . '/wp-includes/pluggable.php';
    global $wpdb;
    
    if(isset($_POST["postComment"])){
        $name = $_POST["name"];
        $email = $_POST["email"];
        $comment = $_POST["comment"];
        $sp_id = $_POST["sp_id"];

        $action = $_POST["action"];
        $action1 = $_POST["action1"];
        $action2 = $_POST["action2"];
    
        $data = ["parent_id"=>0,"name"=>$name,"email"=>$email,"sample_comment"=>$comment, "sample_ques_id"=>$sp_id];
        $inserted = $wpdb->insert("wp_sample_comments",$data);

        header("Location: http://edukeeda.com/practice-exercise/?action=$action&&action1=$action1&&action2=$action2");
    }
?>
