<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;
	unset($_SESSION["login"]);
	session_destroy();
	wp_logout();
	header('Location: https://edukeeda.com/signin/');
?>
