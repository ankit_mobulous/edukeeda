<div class="join">
<?php
session_start();
/**
/*
Template Name:Test List
*/
if($_SESSION["login"]) {

} else {
  header('Location: http://edukeeda.com/post-article-form/');
}
get_header(); ?>
   <?php
      $path = $_SERVER['DOCUMENT_ROOT'];
      include_once $path . '/wp-config.php';
      include_once $path . '/wp-load.php';
      include_once $path . '/wp-includes/wp-db.php';
      include_once $path . '/wp-includes/pluggable.php';

      global $wpdb;
      ?>
   <div class="catchybga">
      <div class="want">
        <?php
          global $wpdb;
          $eid = $_GET["action"];
          $getexam = $wpdb->get_results(
              $wpdb->prepare( "SELECT * FROM wp_premium_exam_type WHERE id = %d", $eid)
          );
        ?>
         <h3> <?php echo $getexam[0]->exam_title; ?> </h3>
      </div>
   </div>
   <div class="col-md-12">
      <div class="stu">
         <div class="ardo">
            <div class="col-md-10 col-md-offset-1">
              <div class="cont">
              <table class="table">
                  <thead>
                     <tr>
                        <th>S.No.</th>
                        <th> Test Name </th>
                        <th> Duration (Hour:min) </th>
                        <th>  </th>
                     </tr>
                  </thead>
                  <tbody>
                  <?php
                    $user_id = $_SESSION["login"]["id"];
                    $sid = $_GET["action1"];
                    $getQuizDetails = $wpdb->get_results(
                        $wpdb->prepare( "SELECT * FROM wp_premium_quiz WHERE exam_id = %d AND subject_id=%d", $eid, $sid)
                    );
                    $i=1;
                    foreach($getQuizDetails as $getQuizDetail) {
                      $newTime = getexamtimeFormat($getQuizDetail->time_limit);
                      $given = $wpdb->get_results(
                          $wpdb->prepare( "SELECT * FROM wp_user_premium_start WHERE exam_id = %d AND subject_id = %d AND quiz_id=%d AND user_id=%d",$eid, $sid, $getQuizDetail->id, $user_id));
                  ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td><?php echo $getQuizDetail->quiz_title; ?></td>
                          <td><?php echo $newTime; ?></td>
                  <?php if($given) { ?>
                          <td><a href="<?php site_url();?>/test-answer?action=<?php echo $eid;?>&action1=<?php echo $sid; ?>&action2=<?php echo $getQuizDetails[0]->id; ?>"><span class="btn btn-warning"><b>Check you performance</b></span></a></td>
                  <?php
                      } else  {
                  ?>
                        <td><a href="<?php site_url();?>/online-test-start?action=<?php echo $eid;?>&action1=<?php echo $sid; ?>&action2=<?php echo $getQuizDetail->id; ?>"><span class="btn btn-warning"><b>Start Test</b></span></a></td>
                  <?php } ?>
                        </tr>
                  <?php $i++; } ?>
                  </tbody>
               </table>
             </div>
            </div>
         </div>
      </div>
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
</div>
<?php get_footer(); ?>
</div>
<?php
    function getexamtimeFormat($timeExam){
        $tValue = (int) $timeExam;
        $dateFormat = "d F Y -- g:i a";
        $targetDate = time() + ($tValue*60);//Change the 25 to however many minutes you want to countdown
        $actualDate = time();
        $secondsDiff = $targetDate - $actualDate;
        $remainingDay  = floor($secondsDiff/60/60/24);
        $remainingHour  = floor(($secondsDiff-($remainingDay*60*60*24))/60/60);
        $remainingMinutes = floor(($secondsDiff-($remainingDay*60*60*24)-($remainingHour*60*60))/60);
        $remainingSeconds = floor(($secondsDiff-($remainingDay*60*60*24)-($remainingHour*60*60))-($remainingMinutes*60));
        $actualDateDisplay = date($dateFormat,$actualDate);
        $targetDateDisplay = date($dateFormat,$targetDate);

        $newtime = $remainingHour .":". $remainingMinutes;
        return $newtime;
    }
?>
<style>
.cont thead {
    background: #4054b2;
    color: #fff;
}
.join a {
    background: none;
    border: none;
	padding:0px;
}
.join a:hover {
    background: none;
    border: none;
}
   .want a {
     border-radius: 10px;
     background: #f68e2f;
     color: #fff;
     padding: 6px 18px;
     font-size: 13px;
     text-align: center;
     margin: 13px 0 0 0;
     text-transform: capitalize;
   }
   .cont p {
     font-size: 18px;
     font-weight: 600;
     margin: 5px 0px;
     color: #060658;
     height: 77px;
   }
   .cont {
     float: left;
     width: 100%;
     padding: 10px;
   }
   .join .page-wrap .container {
     width: 100%;
     padding: 0;
     margin: 0 auto;
   }
   .join .page-wrap .container .row {
      margin: 0;
   }
   span.btn.btn-warning {
    float: right;
    background: #f68e2f;
	font-size: 15px;
   }
      span.btn.btn-warning:hover{
		   background: #fff;
		   color:#f68e2f;
		   border:1px solid#f68e2f;
	  }
   .catchybga {
   padding: 2% 0;
   background-position: 100%;
   position: relative;
   margin-bottom: 45px;
   background: #4054b2;
       margin-top: -60;
   }
   .want {
   text-align: center;
   color: white;
   }
   .want h3 {
   color: white;
   font-size: 36px;
   font-weight: 600;
   margin-bottom: 0px;
   }
   .want a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .stu {
   float: left;
   width: 100%;
       margin-bottom: 50px;
   }
   @media only screen and (max-width: 767px)
   {
	.cont{
		text-align:center;
	   }
   .stu{
	   margin-top:30px;
	   }
   .want h3 {
   color: white;
   font-size: 23px;
   font-weight: 100;
   }
   .want p {
   font-size: 17px;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 20px;
   }
   .catchybga{
	   margin-top:0px;
   }
</style>
