<div class="eventnew">
<?php
/**
/*
Template Name:home1
*/
get_header(); ?>
<?php
  if(isset($_POST["followuser"])) {
      $memberid = $_POST["memberid"];
      $userid = $_SESSION["login"]["id"];
      global $wpdb;
      $data = ["follow_id"=>$memberid, "user_id"=>$userid];
      $wpdb->insert("wp_member_follow", $data);
  }
  if(isset($_POST["unfollow"])) {
        $memberid = $_POST["memberid"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $wpdb->query($wpdb->prepare("DELETE FROM wp_member_follow WHERE follow_id = %d AND user_id = %d", $memberid, $userid));
    }
?>
   <div class="catchybga">
      <div class="want">
        <h2><p>Common platform for eminent professionals / learners / achievers with a vision to create one stop destination for all kinds of experienced knowledge</p></h2>
         <h3>Grow Your Professional Network, Share Expertise</h3>
         <p>"The best way to have a good idea is to have a lot of ideas"<span>- Linus Pauling</span></p>
         
          
         <div class="belowlinksedus">
            <ul>
                <li><a href="<?php if($_SESSION["login"]["id"]){echo "https://edukeeda.com/postyourquerypage/";}else{echo "#";}?>" <?php if($_SESSION["login"]["id"]){}else{ echo "data-toggle='modal' data-target='#myModalpost'"; }?> >Post </a></li>.
                <li><a href="#section1000" id="2000" class="click"">Explore </a></li>.
                <li><a href="https://edukeeda.com/discussion-forum/">Discuss </a></li>
				
            </ul>
         </div>
      </div>
   </div>
   <div class="categorybga">
      <div class="insidecontainer">
         <div class="control">
            <div class="catch">
               <!--<div class="col-xs-6 col-md-3">
                  <div class="cont">
                     <a href="<?php echo site_url();?>/student-corner/">
                        <img width="124" height="124" src="/img/student-corner-2.png" class="attachment-full size-full" alt="">
                        <p>Student Corner</p>
                     </a>
                  </div>
               </div>-->
               <div class="col-xs-6 col-md-4">
                  <div class="cont">
                     <a href="<?php echo site_url();?>/latest-articles/" data-elementor-open-lightbox="">
                        <img width="124" height="124" src="/img/expert-gayan.png">
                        <p>Expert Gyan</p>
                     </a>
                  </div>
               </div>
               <div class="col-xs-6 col-md-4">
                  <div class="cont">
                     <a href="<?php echo site_url();?>/latest-event/" data-elementor-open-lightbox="">
                        <img width="124" height="124" src="/img/events-icon.png" class="attachment-large size-large" alt="">
                        <p>Events</p>
                     </a>
                  </div>
               </div>
               <div class="col-xs-6 col-md-4">
                  <div class="cont">
                     <a href="<?php echo site_url();?>/interviews/" data-elementor-open-lightbox="">
                        <img width="124" height="124" src="/img/interview-icon.png" class="attachment-large size-large" alt="">
                        <p> Interviews</p>
                     </a>
                  </div>
               </div>
            </div>
            <div class="stu">
               <div class="col-md-6 col-xs-6">
                  <div class="cont">
                     <a href="https://edukeeda.com/members-page/" data-elementor-open-lightbox="">
                        <img width="124" height="124" src="/img/members-icon.png" class="attachment-large size-large" alt="">
                        <p>Members</p>
                     </a>
                  </div>
               </div>
               <div class="col-md-6 col-xs-6">
                  <div class="cont">
                     <a href="https://edukeeda.com/discussion-forum/" data-elementor-open-lightbox="">
                        <img width="124" height="124" src="/img/Discussion-icon.png" class="attachment-large size-large" alt="">
                        <p>Discussion Forum</p>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="simplegrid">
      <div class="insidecontainer"  id="section1000" style="height: 375px;">

         <div class="head">
            <h2>Explore By Industry / Business</h2>
            <p><a href="<?php echo site_url();?>/expertzone/">See more</a></p>
         </div>
         <div class="pare">
            <div id="owl-demo1" class="owl-carousel owl-theme">
               <?php
                  $categories = get_categories( array(
                       'child_of'            => 0,
                       'current_category'    => 0,
                       'parent'              => 0,
                       'depth'               => 0,
                       'echo'                => 1,
                       'exclude'             => '',
                       'exclude_tree'        => '',
                       'feed'                => '',
                       'feed_image'          => '',
                       'feed_type'           => '',
                       'hide_empty'          => 0,
                       'hide_title_if_empty' => false,
                       'hierarchical'        => true,
                       'order'               => 'ASC',
                       'orderby'             => 'name',
                       'separator'           => '<br />',
                       'show_count'          => 0,
                       'show_option_all'     => '',
                       'show_option_none'    => __( 'No categories' ),
                       'style'               => 'list',
                       'taxonomy'            => 'expert_gyan_category',
                       'title_li'            => __( 'Categories' ),
                       'use_desc_for_title'  => 1,
                   ));
                    $x =1;
                    foreach ($categories as $terms) {
                      if($x <= 6) {
                      $term_id = $terms->term_id;
                      $image   = category_image_src( array('term_id'=>$term_id) , false );
                      $category_link = get_category_link( $term_id );
                  ?>
               <div class="item">
                  <div class="its">
                     <a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id; ?>&action=industry">
                        <img src="<?php echo !empty($image)?$image:"/wp-content/themes/sydney/images/person-interviewing-200n-012110.jpg"; ?>">
                        <p><?php echo $terms->name; ?></p>
                     </a>
                  </div>
               </div>
               <?php
                  }
                  $x++;
                  }
                  ?>
            </div>
         </div>
      </div>
   </div>
   <div class="simplegrid categorybga interviewsas">
      <div class="insidecontainer">
         <div class="head">
            <h2>College Stuff & Career</h2>
            <p><a href="<?php echo site_url();?>/expertzone/">See more</a></p>
         </div>
         <div class="pare">
            <div id="owl-demo2" class="owl-carousel owl-theme">
            <?php
              $categories1 = get_categories( array(
                 'child_of'            => 0,
                 'current_category'    => 0,
                 'parent'    => 0,
                 'depth'               => 0,
                 'echo'                => 1,
                 'exclude'             => '',
                 'exclude_tree'        => '',
                 'feed'                => '',
                 'feed_image'          => '',
                 'feed_type'           => '',
                 'hide_empty'          => 0,
                 'hide_title_if_empty' => false,
                 'hierarchical'        => true,
                 'order'               => 'ASC',
                 'orderby'             => 'name',
                 'separator'           => '<br />',
                 'show_count'          => 0,
                 'show_option_all'     => '',
                 'show_option_none'    => __( 'No categories' ),
                 'style'               => 'list',
                 'taxonomy'            => 'social_stuff_category',
                 'title_li'            => __( 'Categories' ),
                 'use_desc_for_title'  => 1,
              ));
             
              if(!empty($categories1))
              {
                  foreach ($categories1 as $terms1) {
                  
                  $term_id1 = $terms1->term_id;
                  $image1   = category_image_src( array('term_id'=>$term_id1) , false );
                  $category_link1 = get_category_link( $term_id1 );
            ?>   
               <div class="item">
                  <div class="its">
                     <a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id1; ?>&action=home_appliances">
                        <img src="<?php echo !empty($image1)?$image1:"/wp-content/themes/sydney/images/person-interviewing-200n-012110.jpg"; ?>">
                        <p><?php echo $terms1->name; ?></p>
                     </a>
                  </div>
               </div>
            <?php
                }
              }
            ?>
            
            </div>
         </div>
      </div>
   </div>
   <div class="simplegrid">
      <div class="insidecontainer">
         <div class="head">
            <h2>Blogs & Opinion</h2>
            <p><a href="<?php echo site_url();?>/expertzone/">See more</a></p>
         </div>
         <div class="pare">
            <div id="owl-demo3" class="owl-carousel owl-theme">
            <?php
             //  $categories2 = get_categories( array(
             //     'child_of'            => 0,
             //     'current_category'    => 0,
             //     'parent'    => 0,
             //     'depth'               => 0,
             //     'echo'                => 1,
             //     'exclude'             => '',
             //     'exclude_tree'        => '',
             //     'feed'                => '',
             //     'feed_image'          => '',
             //     'feed_type'           => '',
             //     'hide_empty'          => 0,
             //     'hide_title_if_empty' => false,
             //     'hierarchical'        => true,
             //     'order'               => 'ASC',
             //     'orderby'             => 'name',
             //     'separator'           => '<br />',
             //     'show_count'          => 0,
             //     'show_option_all'     => '',
             //     'show_option_none'    => __( 'No categories' ),
             //     'style'               => 'list',
             //     'taxonomy'            => 'social_stuff_category',
             //     'title_li'            => __( 'Categories' ),
             //     'use_desc_for_title'  => 1,
             // ) );
             
             //  if(!empty($categories2))
             //  {
             //      foreach ($categories2 as $terms2) {
                  
             //      $term_id2 = $terms2->term_id;
             //      $image2  = category_image_src( array('term_id'=>$term_id2) , false );
             //      $category_link2 = get_category_link( $term_id2 );
            ?>   
               <!--<div class="item">
                  <div class="its">
                     <a href="<?php //echo site_url(); ?>/expertise?id=<?php //echo $term_id2; ?>&action=student_stuff">
                        <img src="<?php //echo !empty($image2)?$image2:"/wp-content/themes/sydney/images/person-interviewing-200n-012110.jpg"; ?>">
                        <p><?php //echo $terms2->name; ?></p>
                     </a>
                  </div>
               </div>-->
            <?php
              // }
              //  }
            ?>

            <?php
              $categories2 = get_categories( array(
                 'child_of'            => 0,
                 'current_category'    => 0,
                 'parent'    => 0,
                 'depth'               => 0,
                 'echo'                => 1,
                 'exclude'             => '',
                 'exclude_tree'        => '',
                 'feed'                => '',
                 'feed_image'          => '',
                 'feed_type'           => '',
                 'hide_empty'          => 0,
                 'hide_title_if_empty' => false,
                 'hierarchical'        => true,
                 'order'               => 'ASC',
                 'orderby'             => 'name',
                 'separator'           => '<br />',
                 'show_count'          => 0,
                 'show_option_all'     => '',
                 'show_option_none'    => __( 'No categories' ),
                 'style'               => 'list',
                 'taxonomy'            => 'blog_category',
                 'title_li'            => __( 'Categories' ),
                 'use_desc_for_title'  => 1,
             ) );
             
              if(!empty($categories2))
              {
                  foreach ($categories2 as $terms2) {
                  
                  $term_id2 = $terms2->term_id;
                  $image2  = category_image_src( array('term_id'=>$term_id2) , false );
                  $category_link2 = get_category_link( $term_id2 );
            ?>   
               <div class="item">
                  <div class="its">
                     <a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id2; ?>&action=blogs">
                        <img src="<?php echo !empty($image2)?$image2:"/wp-content/themes/sydney/images/person-interviewing-200n-012110.jpg"; ?>">
                        <p><?php echo $terms2->name; ?></p>
                     </a>
                  </div>
               </div>
            <?php
              }
               }
            ?>
            </div>
         </div>
      </div>
   </div>
   <div class="simplegrid categorybga interviewsas">
      <div class="insidecontainer">
         <div class="head">
            <h2> Interviews</h2>
            <p><a href="<?php echo site_url();?>/interviews/">See more</a></p>
         </div>
         <div class="pare">
            <div id="owl-demo7" class="owl-carousel owl-theme">
               <?php
                  $args = array(
                     'numberposts' => 6,
                     'offset' => 0,
                     'category' => 0,
                     'orderby' => 'post_date',
                     'order' => 'DESC',
                     'include' => '',
                     'exclude' => '',
                     'meta_key' => '',
                     'meta_value' =>'',
                     'post_type' => 'interview',
                     'post_status' => 'publish',
                     'suppress_filters' => true
                   );
                  
                   $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
                   //print_r($recent_posts);
                   if(!empty($recent_posts))
                   {
                     foreach ($recent_posts as $value) {
                        //var_dump($value["ID"]);
                       $cats = get_the_category($value["ID"]);
                       $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
                       $author_name = get_the_author_meta( 'display_name', $value['post_author'] );
                  
                       $checkViews = $wpdb->get_results( 
                              $wpdb->prepare( "SELECT * FROM wp_post_views WHERE post_id = %d", $value['ID']) 
                        );

                      $countPostTitle1 = str_word_count($value['post_title']);
                      if($countPostTitle1 >= 20) {
                        $postTitle1 = substr($value['post_title'],0, 40);
                        $postTitle1 .= "...";
                      } else {
                        $postTitle1 = $value['post_title'];
                      }
                  ?>  
               <div class="item">
                  <div class="its">
                     <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">
                        <img src="<?php echo $image[0]; ?>">
                        <div class="insidepart">
                           <div class="title">
                     <a href="<?php echo site_url();?>/interviews/"> INTERVIEWS </a>
                     </div>
                     <div class="cls">
                     <p><a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>"><?php echo $postTitle1; ?></a></p>
                     </div>
                     <div class="useridentitysect">
                     <div class="iidentconholders">
                     <div class="clss">
                     <p><?php echo get_field( "full_name", $value['ID']); ?></p>
                     </div>
                     <div class="csl">
                     <p><?php echo get_field( "main_identity", $value['ID']); ?></p>
                     </div>
                     </div>
                     </div>
                     </div>
                     </a>
                  </div>
               </div>
               <?php
                  }
                  }
                  ?>
            </div>
         </div>
      </div>
   </div>
   <div class="simplegrid">
      <div class="insidecontainer">
         <div class="head">
            <h2>Events</h2>
            <p><a href="<?php echo site_url();?>/events/">See more</a></p>
         </div>
         <div class="pare">
         <div id="owl-demo4" class="owl-carousel owl-theme">
            <?php
               $action = "event";
               $results1 = $wpdb->get_results( 
                   $wpdb->prepare( "SELECT * FROM wp_posts WHERE post_status='publish' AND post_type=%s ORDER BY id DESC LIMIT 30", $action));

               if($results1) {
                $limit = 1;
               foreach($results1 as $result1) {
                 
                 $cat = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE object_id = %d", $result1->ID));
                 
                 $getCatName = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT term_id, name FROM wp_terms WHERE term_id = %d", $cat[0]->term_taxonomy_id));
               
                  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                
                  if($image){
                      $image = $image[0];
                  } else {
                      $mkey = "_thumbnail_id";
                    $checkimg = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                    );
                    $getimg = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                    );
                    $image = $getimg[0]->guid;  
                  }

                  $getAuthor = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                               $author_name = $getAuthor[0]->display_name;
                
                  $countPostTitle = strlen($result1->post_title);
                
                  if($countPostTitle >= 80) {
                    $postTitle = substr($result1->post_title,0, 80);
                    $postTitle .= "...";
                  } else {
                    $postTitle = $result1->post_title;
                  }
                  
                  $eventDate = get_field('event_date', $result1->ID);
                  $eventDateEnd = get_field('event_date_end', $result1->ID);
                  $currentDate = date("Y-m-d");
                  $eventDateEnd = date("Y-m-d", strtotime($eventDateEnd));
                  
                  $eDate = (int) strtotime($eventDateEnd);
                  $cDate = (int) strtotime($currentDate);
                
                  if($eDate >= $cDate) {
                    if($limit<=5) {
          ?>  
            <div class="item">
               <div class="its">
                  <div class="alte">
                     <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">
                        <div class="altimg0">
                           <img src="<?php echo $image; ?>"> 
                           <div class="datespn1">
                              <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                              <span class="dateDisplay-day">
                              <span><?php echo date('d',strtotime($eventDate)); ?></span><br>
                              </span>
                              <span class="dateDisplay-month text--tiny">
                              <span><?php echo date('M',strtotime($eventDate)); ?></span>
                              </span>
                              </time>
                           </div>
                        </div>
                     </a>
                     <div class="spn">
                        <div class="col-md-12">
                           <div class="linkevent">
                              <a href="<?php echo site_url();?>/events-list/?id=<?php echo $getCatName[0]->term_id;?>">
                              <?php if(strlen($getCatName[0]->name)>32) { echo substr($getCatName[0]->name,0,32)."..."; } else { echo $getCatName[0]->name;} ?>
                              </a>
                           </div>
                           <div class="titleevent">
                              <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">
                                 <?php echo $postTitle; ?>
                              </a>
                           </div>
                           <div class="evp">
                              <span>Post By :<?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <?php
                    $limit++;
                    }
                  }
                }
               }
            ?>  
         </div>
       </div>
      </div>
   </div>
   <div class="simplegrid categorybga interviewsas">
      <div class="insidecontainer">
        <div class="head">
            <h2>Members</h2>
            <p><a href="https://edukeeda.com/members-page/">See more</a></p>
        </div>
        <div class="pare">
        <div id="owl-demo8" class="owl-carousel owl-theme">
        <?php
          $table_name = "wp_users";
          $status = 1;
          $results = $wpdb->get_results( 
            $wpdb->prepare("SELECT * FROM $table_name WHERE user_status = %d ORDER BY id LIMIT 6", $status) 
          );
          $memCount = count($results);

          foreach($results as $result) {
            $userImg = $wpdb->get_results( 
              $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $result->ID) 
            );
            $userHeadline = $wpdb->get_results( 
              $wpdb->prepare("SELECT * FROM wp_profileStepOne WHERE user_id = %d", $result->ID));

            $userAdd = $wpdb->get_results( 
              $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $result->ID));
            $userCountry = $wpdb->get_results( 
              $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $userAdd[0]->country));
            $userState = $wpdb->get_results( 
                $wpdb->prepare("SELECT * FROM states WHERE id = %d", $userAdd[0]->state));
            $userFollowed = $wpdb->get_results( 
                $wpdb->prepare("SELECT COUNT(id) as followed FROM wp_member_follow WHERE follow_id = %d", $result->ID));
        ?>    
            <div class="item">
               <div class="its">
                  <!--<a href=""><img src="/img/IWC_2_HIGHRES-6.jpg"></a>-->
                  <div class="allapro">
                     <div class="col-xs-4 col-md-4">
                        <a href="<?php echo site_url();?>/profile?action=<?php echo $result->ID;?>"><img src="<?php if($userImg){echo $userImg[0]->profilePic;}else{echo "https://edukeeda.com/wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>"  alt="Cinque Terre" width="300" height="300"></a>
                     </div>
                     <div class="col-xs-8 col-md-8">
                        <p><a href="<?php echo site_url();?>/profile?action=<?php echo $result->ID;?>">
                            <?php //if(strlen($getCatName[0]->name)>32) { echo substr($getCatName[0]->name,0,32)."..."; } else { echo $getCatName[0]->name;} ?>
                            <?php echo $result->user_login;?>
                          </a>
                        </p>
                      <?php 
                        if($userHeadline[0]->headline) { 
                      ?>
                        <p class="hedlineauedu">
                          <strong>
                            <?php if(strlen($userHeadline[0]->headline)>25) { echo substr($userHeadline[0]->headline,0,25)."..."; } else { echo $userHeadline[0]->headline;} ?>
                          </strong>
                        </p>
                      <?php
                        }
                      ?>
                        <p class="rigwds">
                          <strong>
                            <?php
                              if($userAdd[0]->city) {
                                $locCity = $userAdd[0]->city .', ';
                              }
                              if($userState[0]->name) {
                                $locState = $userState[0]->name .', ';
                              }
                              $locationName = $locCity . $locState. $userCountry[0]->name;
                            ?>
                            <?php if(strlen($locationName)>20) { echo substr($locationName,0,20)."..."; } else { echo $locationName;} ?>
                          </strong>
                        </p>
                       <div class="share1">
                        <?php      
                          if(isset($_SESSION["login"])) {
                            $userId = $_SESSION["login"]["id"];
                            $checkFollow = $wpdb->get_results( 
                              $wpdb->prepare("SELECT * FROM wp_member_follow WHERE user_id = %d AND follow_id=%d", $userId, $result->ID));
                            
                            if($checkFollow){
                        ?>
                              <form method="post" action=" ">
                                  <input type="hidden" value="<?php echo $result->ID;?>" name="memberid">
                                  <button type="submit" name="unfollow" value="Unfollow" class="followbutton"><img src="/img/follow-icon-selected.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button></p>
                                </form>
                        <?php
                            } else {
                        ?>
                                <form method="post" action=" ">
                                  <input type="hidden" value="<?php echo $result->ID;?>" name="memberid">
                                  <button type="submit" name="followuser" class="followbutton"><img src="/img/Follow-icon.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button></p>
                                </form>
                        <?php
                            }
                          } else {
                        ?>
                            <button type="button" class="followbutton" data-toggle="modal" data-target="#myModal"><img src="/img/Follow-icon.png"> <span> <?php echo $userFollowed[0]->followed; ?> </span></button>
                        <?php
                          }
                        ?>
                       </div>
                     </div>
                     
                  </div>
                  </a>
               </div>
            </div>
        <?php
          }
        ?> 
        </div>
        </div>
      </div>
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php //get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
</div>  
<?php get_footer(); ?>
</div>
<style>
   .spn a {
     color: #9e9e9e;
   }
  .allapro a{
    font-size: 13px;
   color:#f68e2f;
   }
   .page-wrap {
    padding-top: 68px;
}
   .allapro {
    float: left;
    width: 100%;
    min-height: 130px;
}
  .cont:hover{
       box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0.07);
   }
   .titleevent p {
   float: left;
   line-height: 17px;
   text-align: left;
   color: #5a5a5a!important;
   }
   .titleevent {
   float: left;
   width: 100%;
   text-align: left;
   min-height: 50px;
   overflow: hidden;
   }
   .share1 {
   width: 100%!important;
   float: left!important;
   text-align: left;
   }
   .share1 button.followbutton {
   background: none!important;
   border: none!important;
   }
   .share1 button img {
   border: none!important;
    width: 70px!important;
    height: 21px!important;
    margin: 0px 0px 0px -47px;
    padding: 0px;
   }
   .share1 button {
    background: none;
    border: none;
}
   .share1 img {
   width: 30px!important;
   height: 30px!important;
   }
   .share1 span {
   color: #000;
   }
   .allapro p {
   text-align: left!important;
   margin-bottom: 0px!important;
   line-height: 21px!important;
   font-size:13px!important;
   color: #f68e2f!important;
   font-size: 16px!important;
   }
   .allapro img {
   width: 60px!important;
   height: 60px!important;
   border-radius: 100%!important;
   border: 1px solid #ddd;
   padding: 2px;
   margin-top: 5px;
   }
   .allpro {
   float: left!important;
   width: 100%!important;
   text-align: left!important;
   }
   .owl-controls.clickable {
   display: block !important;
   }
   div#owl-demo4 .owl-buttons {
   position: absolute;
   top: 50%;
   width: 100%;
   }
   .linkevent a {
   color:#9a9a9a!important;
   font-weight: 600;
   font-size: 14px;
   line-height: 20px;
   }
   .insidepart{
   padding: 1px 9px;
   float:left;
   }
   .linkevent {
   float: left;
   width: 100%;
   max-height: 100px;
   }
   .owl-next:after {
   box-sizing: border-box;
   content: "\003E";
   width: 38px;
   height: 38px;
   right: -54px;
   position: absolute;
   display: inline-block;
   color: #000;
   font-size: 22px;
   border-radius: 100%;
  border: 1px solid#ddd;
    
    box-shadow: 1px 1px 1px 1px #ddd;
   }
   .owl-prev:after {
   box-sizing: border-box;
   content: "\003C";
   width: 38px;
   height: 38px;
   position: absolute;
   display: inline-block;
   color: #000;
   font-size: 22px;
   border-radius: 100%;
  border: 1px solid#ddd;
    left: -54px;
    box-shadow: 1px 1px 1px 1px #ddd;
   }
   .alte .spn {
   padding: 15px 0px!important;
   }
   div#owl-demo1 {
   position: relative;
   }
   .control {
   float: left;
   width: 100%;
   margin-top: 0px;
   }
   .cls a{
       color: #000;
   }
   .alte {
   float: left;
   width: 100%;
   border: 1px solid #ddd;
       min-height: 204px;
    height: 393px;
    overflow: hidden;
   }
   .alla p {
   margin-bottom: 0px;
   line-height: 10px;
   text-align: left;
   }
   .ent {
   float: left;
   width: 100%;
   }
   .ent a {
   margin-bottom: 5px;
   float: left;
   width: 100%;
   text-align: left;
   font-size: 12px;
   }
   .alla {
   float: left;
   width: 100%;
   }
   p.rigwds {
   color: #070758!important;
   margin: 0px!important;
   padding: 0px!important;
   font-size: 12px!important;
   }
   .hedlineauedu strong {
   margin-bottom: 0px;
   line-height: 10px;
   font-size: 14px;
   color: #0e0e5d;
   font-weight: 100;
   }
   .its .clss p {
   margin-bottom: 0px;
   float: left;
   width: 100%;
   text-align: left;
   padding: 0px;
   font-size: 13px;
   }
   .csl p {
   float: left;
   padding: 0px!important;
   color: #c2b2bd;
   margin: 0px;
   }
   .clss {
   float: left;
   width: 100%;
   }
   .cls {
   float: left;
   width: 100%;
   text-align: left; 
   max-height: 100px;
   min-height: 50px;
   overflow: hidden;
   }
   .cls p{
   font-weight:600;
   }
   .stu {
   width: 50%;
   text-align: center;
   padding: 0px;
   float: none;
   margin: 0 auto;
   }
   .it img {
   max-height: 218px;
   border-radius: 10%;
   width: 100%;
   height: 218px;
   }
   .iidentconholders {
   float: left;
   margin: 0 0 0 0px;
   }
   .it {
   float: left;
   width: 100%;
   }
   .its .cls p{
   font-size: 16px;
   margin: 0px;
   color: #5a5a5a;
   padding: 0px 0px;
   text-align: left;
   line-height: 20px;
   }
   .title{
   float: left;
   width: 100%;
   text-align: left;
   }
   .title a {
   color: #9a9a9a;
   }
   .csl {
   float: left;
   width: 100%;
   }
   .entrry span {
   color: #9a9b96;
   float: left;
   margin: 0px ;
   font-size: 12px;
   }
   .entrry strong {
   color: #060658;
   float: right;
   margin: 0px 32px;
   }
   .entrry p {
   color: black;
   line-height: 7px;
   margin: 0px2
   }
   .fitt {
   text-align: left;
   float: left;
   width: 100%;
   }
   .ent p {
   padding: 0px!important;
   }
   .entrry {
   text-align: left;
   width: 100%;
   }
   .itss img {
   max-height: 218px;
   border-radius: 10%;
   width: 100%;
   height: 218px;
   }
   .evp{
   float:left;
   width:100%;
  
   }
   .want h2 {
    text-align: center;
    position: absolute;
    top: 5%;
    left: 2.5%; 
}
.want h2 p {
    font-size: 18px;
    margin: 8px;
    color: #fff;
}
   .titleevent a {
    color: #000;
    font-size: 18px;
    text-align: center;
    font-weight: 600;
    line-height: 20px;
}
   .evp p{
   font-size: 14px;
   margin: 0px;
   line-height: 17px;
   }
   .evp i.fa.fa-share-alt {
   color: #060658;
   }
   .evimg img {
   border-radius: 60%;
   width: 56px;
   height: 56px;
   }
   .altimg1 img {
   border-radius: 7px 7px 0px 0px;
   }
   .datespn1 {
   position: absolute;
   top:49px;
   left:20px;
   background: #fff;
   padding: 7px;
   color: #060658;
   font-size: 24px;
   text-align: center;
   border: 1px solid lavenderblush;
   border-radius: 7px;
   line-height: 17px;
   }
   .more p {
   text-align: center;
   margin-bottom: 0px;
   margin-top: 5px;
   }
   .itss .spn {
   margin: 11px 0px;
   }
   .pare {
   float: left;
   width: 100%;
   }
   .more a {
   background: #ff822e;
   color: #fff;
   padding: 7px 25px;
   font-size: 17px;
   text-align: center; 
   text-transform: capitalize;
   border-radius: 50px;
   }
   .its p {
   padding: 5px 0px;
   font-size: 18px;
   color: #060658;
   text-align: center;
   font-weight: 600;
   margin: 0;
   line-height: 20px;
   }
   .its .fitt p {
   text-align: left;
   font-size: 20px;
   margin: 0 0 7px 0;
   padding: 0;
   line-height: 10px;
   }
   .owl-buttons {
   position: absolute;
   top: 38%;
   width: 100%;
   }
   div#owl-demo8 .owl-buttons {
   position: absolute;
   top: 40%;
   width: 100%;
   }
   .owl-prev {
   float: left;
   margin-left: -36px;
   font-size: 0px;
   }
   .owl-next {
   float: right;
   margin-right: -36px;
   font-size: 0px;
   }
   .its img {
   max-height: 218px;
   width: 100%;
   height: 218px;
   }
   .its {
   float: left;
   width: 100%;
   background: #fff;
   border-radius: 6px;
   }
   #owl-demo  .item,
   #owl-demo2 .item,
   #owl-demo3 .item,
   #owl-demo4 .item,
   #owl-demo5 .item,
   #owl-demo6 .item,
   #owl-demo7 .item,
   #owl-demo8 .item,
   #owl-demo1 .item{
   padding: 30px 10px 0 0;
   margin: 0;
   color: #FFF;
   -webkit-border-radius: 3px;
   -moz-border-radius: 3px;
   border-radius: 3px;
   text-align: center;
   float: left;
   width: 100%;
   }
   .cont img {
   margin: 0px;
   }
   .catch {
   float: left;
   width: 100%;
   padding: 0px;
   text-align: center;
   margin-bottom: 50px;
   }
   .want a {
   border-radius: 10px;
   background: #f68e2f;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .wantpost a {
   border-radius: 10px;
   background: #f68e2f;
   color:#ffffff; 
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .wantpost p {
   color: white;
   font-size: 20px;
   position: ABSOLUTE;
   left: 0;
   right: 0;
   margin: 10 AUTO;
   FLOAT: NONE;
   WIDTH: AUTO;
   BOTTOM: 0px;
   }
   .cont p {
   margin: 0;
      font-size: 20px;
    font-weight: 600;
   color: #000;
   }
   .even1 img {
   width: 100%;
   border-radius: 10px 10px 10px 10px;
   max-height: 200px;
   height: 180px;
   }
   .altimg img {
   border-radius: 7px 7px 0px 0px;
   }
   .EVEN {
   float: left;
   width: 100%;
   margin-bottom: 35px;
   }
   span.dateDisplay-day {
   color: #f68e2f;
   font-size: 20px;
   font-weight: 800;
   }
   span.dateDisplay-month.text--tiny {
   color: #020253;
   font-size: 20px;
   font-weight: 800;
   }
   .spn img {
   border-radius: 60%;
   width: 100%!important;
   height: 56px;
   }
   .evp span {
   display: block;
   color: #c2b2bd;
   font-size: 14px;
   }
   
   .spn a:hover {
   color: orange;
   }
   .alt{
   box-shadow: 0 0 2px 0 rgba(46,62,72,.12), 0 2px 4px 0 rgba(46,62,72,.12);
   border-radius: 0px 0px 10px 10px;
   float: left;
   }
   .odd {
   float: left;
   width: 100%;
   margin-bottom: 60px;
   margin-top: 30px;
   }
   div#sidebar-footer {
   float: left;
   width: 100%;
   }
   .eventhead a {
   float: right;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 79px;
   }
   .eventnew .container2 {
   margin: 0 auto;
   max-width: 1170px;
   }
   .catchybga {
   padding: 13% 0;
   background: url(/img/image-education.png);
   background-size: cover;
   background-position: 100%;
   position: relative;
   }
   div#sidebar-footer {
   float: left;
   width: 100%;
   }
   .alt thumbnail{
   float: left;
   position: relative;
   }
   .alt h1 {
   font-size: 31px;
   }
   .eventhead h1 {
   font-size: 31px;
   float: left;
   margin: 0 0 60px 15px;
   }
   .datespn {
   position: absolute;
   top: 16px;
   left: 33px;
   background: #fff;
   padding: 7px;
   color: #060658;
   font-size: 24px;
   text-align: center;
   border: 1px solid lavenderblush;
   border-radius: 7px;
   line-height: 17px;
   }
   .thumbnail strong {
   width: 100px;
   }
   .thumbnail .caption {
   padding: 9px;
   color: #333;
   float: left;
   width: 100%;
   }
   .spndat span {
   font-size: 17px;
   color: #b2b2bd;;
   float: left;
   width: 100%;
   }
   .spndat {
   padding: 16px;
   }
   .spndat p {
   color: black;
   font-size: 25px;
   float: left;
   width: 100%;
   line-height: 26px;
   margin-bottom: 13px;
   margin-top: 5px;
   font-weight: 600;
   }
   .spndat p:hover{
   color: #f68e2f;
   }
   .spn span {
   display: block;
   color: #c2b2bd;
   }
   .alt .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img{
   display: block;
   max-width: 100%;
   height: 150px;
   width: 333px;
   }
   .alt:hover{
   box-shadow: 3px 6px 2px 0 rgba(46,62,72,.12), 0 2px 4px 0 rgba(46,62,72,.12)
   }
   .spn {
   padding: 5px 0px;
   margin: 0px;
   float: left;
   width: 100%;
   text-align: left;
   }
   .evimg {
   float: left;
   width: 100%;
   }
   .odd h2 {
   font-size: 20px;
   margin: 0 0 50px 15px;
   }
   .even1{
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   margin-bottom: 35px;
   background-size: cover;
   text-align: center;
   color: white;
   }
   .even1 p {
   color: black;
   font-size: 20px;
   font-weight: 600;
   }
   .thumbnail{
   box-shadow:0 0 2px 0 rgba(46,62,72,.12), 0 2px 4px 0 rgba(46,62,72,.12);
   border-radius: 0px 0px 10px 10px;
   }
   .thumbnail:hover{
   box-shadow: 2px 4px 0px 0 rgba(0, 0, 0, 0.07);
   }
   .want {
   text-align: center;
   color: white;
   }
   .want h3 {
   color: white;
   font-size: 36px;
   font-weight: 600;
   margin-bottom: 0px;
   }
   .want a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .wantpost {
   TEXT-ALIGN: CENTER;
   }
   .wantpost a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .want p {
   font-size: 20px;
   margin: 8px;
   }
   .want span {
    margin: 0px 5px;
    color: #f68e2f;
}
   .expview strong {
   color: black;
   margin: 0px 11px;
   }
   .hentry {
   float: left;
   width: 100%;
   }
   .head h2 {
   font-size: 22px;
   float: left;
   }
   .head p {
   float: right;
   }
   .head a {
   background: #060658;
   color: #fff;
   padding: 7px 25px;
   font-size: 17px;
   text-align: center;
   text-transform: capitalize;
   border-radius: 50px;
   }
   .eventnew .content-wrapper.container {
   max-width: 100%;
   width: 100%;
   padding: 0 !important;
   }
   .eventnew .content-wrapper.container > .row{
   margin:0;
   }
   .insidecontainer {
   max-width: 1170px;
   margin: 0 auto;
   }
 .mystyle {
  height: 100px;
}


   @media only screen and (max-width: 767px)
   {
       .cont {
    float: left;
    width: 100%;
}
.want span {
    margin: 0px 5px;
    color: #f68e2f;
    display: -webkit-inline-box;
}
.want h2 {
    text-align: center;
    position: absolute;
    top: 5%;
    left: 39.5%;
	display:none;
}
.want h2 p {
    font-size: 12px;
    margin: 8px;
        color: #060658;
		display:none;
}
   .want h3 {
 
   font-size: 13px;
   font-weight: 600;
  
   }
   .wantpost p {
   color: white;
   font-size: 15px;
   position: ABSOLUTE;
   left: 0;
   right: 0;
   margin: 10 AUTO;
   FLOAT: NONE;
   WIDTH: AUTO;
   BOTTOM: 0px;
   
   }
   .page-wrap{
      padding:0px;
   }
   .belowlinksedus ul li a {
    padding: 0;
    background: transparent;

}
   .want p {
       font-size: 11px;
    margin-bottom: 0px;
      color: #060658;
	   display:none;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 20px;
   }
   .eventhead h1 {
   font-size: 31px;
   float: left;
   margin: 0 0 20px 10px;
   }
   .eventhead a {
   float: right;
   MARGIN: 0px 19px;
   }
   .alt{
   margin-bottom: 12px;
   }
   .owl-buttons{
       display:none;
   }
   .head h2 {
    font-size: 15px;
    float: left;
}
.share1 button img {
    border: none!important;
    width: 50px!important;
    height: 22px!important;
    margin: 0px 0px 0px -36px;
    padding: 0px;
    float: left;
}
.share1 {
    width: 100%!important;
    float: left!important;
    text-align: left;
}
.catchybga {
    padding: 13% 0;
    background: none;

    position: relative;
    background: url(/img/image-education.png);
    background-size: cover;
    background-position: 100%;
    margin-top: -5px;
      
}
.cont p {
    margin: 0;
    font-size: 15px;
    font-weight: 600;
    color: #000;
}
.stu {
    width: 100%;
    
}
div#owl-demo4 {
    width: 100%!important;
    float: left!important;
}

.catch{
  margin-bottom:0px;
   }
   }
</style>
<script>
function myFunction() {
   var element = document.getElementById("2000");
   element.classList.toggle("mystyle");
}
</script>
<script>
   jQuery(document).ready(function() {
     jQuery("#owl-demo").owlCarousel({
       navigation : true
    });
   });
</script>
<script>
   jQuery("#owl-demo1").owlCarousel({
    navigation : true,
    loop:true,
     margin:10,
   autoPlay: 3000, //Set AutoPlay to 3 seconds
   
      items : 4,
   });
</script>
<script>
   jQuery("#owl-demo2").owlCarousel({
    navigation : true,
    loop:true,
     margin:10,
   autoPlay: 3000, //Set AutoPlay to 3 seconds
   
      items : 4,
   });
</script>
<script>
   jQuery("#owl-demo3").owlCarousel({
    navigation : true,
    loop:true,
     margin:10,
   autoPlay: 3000, //Set AutoPlay to 3 seconds
   
      items : 4,
   });
</script>
<script>
   jQuery("#owl-demo4").owlCarousel({
    navigation : true,
    loop:true,
     margin:10,
   autoPlay: 3000, //Set AutoPlay to 3 seconds
   
      items : 4,
   });
</script>
<script>
   jQuery("#owl-demo5").owlCarousel({
    navigation : true,
    loop:true,
     margin:10,
   autoPlay: 3000, //Set AutoPlay to 3 seconds
   
      items : 4,
   });
</script>
<script>
   jQuery("#owl-demo6").owlCarousel({
    navigation : true,
    loop:true,
     margin:10,
   autoPlay: 3000, //Set AutoPlay to 3 seconds
   
      items : 4,
   });
</script>
<script>
   jQuery("#owl-demo7").owlCarousel({
    navigation : true,
    loop:true,
     margin:10,
   autoPlay: 3000, //Set AutoPlay to 3 seconds
   
      items : 4,
   });
</script>
<script>
   jQuery("#owl-demo8").owlCarousel({
    navigation : true,
    loop:true,
     margin:10,
   autoPlay: 3000, //Set AutoPlay to 3 seconds
   
      items : 4,
   });
</script>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Follow</h4>
         </div>
         <div class="modal-body">
            <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>

<!-- Modal -->
<div id="myModalpost" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Posting</h4>
         </div>
         <div class="modal-body">
            <p> For Posting please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>
