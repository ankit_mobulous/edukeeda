<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;

    if(isset($_POST['submitsteplang'])) {
      $userId = $_SESSION["login"]["id"];
      $lang = $_POST["lang"];

      if(!empty($_POST["lid"])){
          $tablename = "wp_profileStepEighth";
          $getStepEighth = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM $tablename WHERE user_id = %d and user_lang=%d",$userId, $lang) 
          );
          
          if($getStepEighth) {
             header("Location: https://edukeeda.com/all-members/homepage?action=$userId");
          } else {
              $lid = $_POST["lid"];
              $wpdb->update($tablename, array('user_lang' => $lang), array('id'=>$lid));
             header("Location: https://edukeeda.com/all-members/homepage?action=$userId");
          }

      } else {
          $tablename = "wp_profileStepEighth";
          $getStepEighth = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM $tablename WHERE user_id = %d and user_lang=%d",$userId, $lang) 
          );
          
          if($getStepEighth) {
              if(!empty($_POST["lmember"])){
                header("Location: https://edukeeda.com/all-members/homepage?action=$userId");
              } else{
               header('Location: https://edukeeda.com/edit-profile/');
              }
          } else {
              $data = array(
                        "user_id" => $userId,
                        "user_lang" => $lang,
                      );
              var_dump($data);
              $result = $wpdb->insert($tablename, $data);
              if(!empty($_POST["lmember"])){
                header("Location: https://edukeeda.com/all-members/homepage?action=$userId");
              } else{
                header('Location: https://edukeeda.com/edit-profile/');
              }
          }
      }
    }

    if(isset($_POST["stepEightDelete"])) {
      $id = $_POST["stepEightId"];
      //var_dump($id);die;
      $wpdb->query($wpdb->prepare("DELETE FROM wp_profileStepEighth WHERE id = %d", $id));
      header('Location: https://edukeeda.com/edit-profile/');
  }
?>
