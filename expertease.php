<div class="eventnew">
<?php
session_start();
/**
/*
 Template Name:expertease
 */  
get_header(); ?>
<style type="text/css">
  .followbtn {
    padding: 3px 23px!important;
    margin-bottom: 13px!important;
    float: right;
  margin-right: 17px;
  }
  .spndat {
    padding: 16px;
    min-height: 100px;
}
  .spn a {
    color: #a2a2a2!important;
}

</style>
 <?php
    if(isset($_POST["follow"])) {
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $categories = get_categories( array(
                   'child_of'            => 0,
                   'parent'              => 0,
                   'current_category'    => 0,
                   'depth'               => 0,
                   'echo'                => 1,
                   'exclude'             => '',
                   'exclude_tree'        => '',
                   'feed'                => '',
                   'feed_image'          => '',
                   'feed_type'           => '',
                   'hide_empty'          => 0,
                   'hide_title_if_empty' => false,
                   'hierarchical'        => true,
                   'order'               => 'ASC',
                   'orderby'             => 'name',
                   'separator'           => '<br />',
                   'show_count'          => 0,
                   'show_option_all'     => '',
                   'show_option_none'    => __( 'No categories' ),
                   'style'               => 'list',
                   'taxonomy'            => 'expert_gyan_category',
                   'title_li'            => __( 'Categories' ),
                   'use_desc_for_title'  => 1,
               ) );
        foreach ($categories as $terms) {
          $term_id = $terms->term_id;
          $data = ["user_id"=>$userid, "cat_id"=>$term_id, "cat_type"=>$catType];
          $wpdb->insert("wp_follow_cat", $data);
        }
    }
    if(isset($_POST["unfollow"])) {
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $wpdb->query($wpdb->prepare("DELETE FROM wp_follow_cat WHERE user_id = %d AND cat_type = %s", $userid, $catType));
    }

    if(isset($_POST["follow1"])) {
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $categories = get_categories( array(
                   'child_of'            => 0,
                   'parent'              => 0,
                   'current_category'    => 0,
                   'depth'               => 0,
                   'echo'                => 1,
                   'exclude'             => '',
                   'exclude_tree'        => '',
                   'feed'                => '',
                   'feed_image'          => '',
                   'feed_type'           => '',
                   'hide_empty'          => 0,
                   'hide_title_if_empty' => false,
                   'hierarchical'        => true,
                   'order'               => 'ASC',
                   'orderby'             => 'name',
                   'separator'           => '<br />',
                   'show_count'          => 0,
                   'show_option_all'     => '',
                   'show_option_none'    => __( 'No categories' ),
                   'style'               => 'list',
                   'taxonomy'            => 'explore_gyan_category',
                   'title_li'            => __( 'Categories' ),
                   'use_desc_for_title'  => 1,
               ) );
        foreach ($categories as $terms) {
          $term_id = $terms->term_id;
          $data = ["user_id"=>$userid, "cat_id"=>$term_id, "cat_type"=>$catType];
          $wpdb->insert("wp_follow_cat", $data);
        }
    }
    if(isset($_POST["unfollow1"])) {
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $wpdb->query($wpdb->prepare("DELETE FROM wp_follow_cat WHERE user_id = %d AND cat_type = %s", $userid, $catType));
    }

    if(isset($_POST["follow2"])) {
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $categories = get_categories( array(
                       'child_of'            => 0,
                       'current_category'    => 0,
                       'parent' => 0,
                       'depth'               => 0,
                       'echo'                => 1,
                       'exclude'             => '',
                       'exclude_tree'        => '',
                       'feed'                => '',
                       'feed_image'          => '',
                       'feed_type'           => '',
                       'hide_empty'          => 0,
                       'hide_title_if_empty' => false,
                       'hierarchical'        => true,
                       'order'               => 'ASC',
                       'orderby'             => 'name',
                       'separator'           => '<br />',
                       'show_count'          => 0,
                       'show_option_all'     => '',
                       'show_option_none'    => __( 'No categories' ),
                       'style'               => 'list',
                       'taxonomy'            => 'social_stuff_category',
                       'title_li'            => __( 'Categories' ),
                       'use_desc_for_title'  => 1,
                   ) );
        foreach ($categories as $terms) {
          $term_id = $terms->term_id;
          $data = ["user_id"=>$userid, "cat_id"=>$term_id, "cat_type"=>$catType];
          $wpdb->insert("wp_follow_cat", $data);
        }
    }
    if(isset($_POST["unfollow2"])) {
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $wpdb->query($wpdb->prepare("DELETE FROM wp_follow_cat WHERE user_id = %d AND cat_type = %s", $userid, $catType));
    }

    if(isset($_POST["follow3"])) {
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
       $categories = get_categories( array(
                       'child_of'            => 0,
                       'current_category'    => 0,
                       'parent' => 0,
                       'depth'               => 0,
                       'echo'                => 1,
                       'exclude'             => '',
                       'exclude_tree'        => '',
                       'feed'                => '',
                       'feed_image'          => '',
                       'feed_type'           => '',
                       'hide_empty'          => 0,
                       'hide_title_if_empty' => false,
                       'hierarchical'        => true,
                       'order'               => 'ASC',
                       'orderby'             => 'name',
                       'separator'           => '<br />',
                       'show_count'          => 0,
                       'show_option_all'     => '',
                       'show_option_none'    => __( 'No categories' ),
                       'style'               => 'list',
                       'taxonomy'            => 'blog_category',
                       'title_li'            => __( 'Categories' ),
                       'use_desc_for_title'  => 1,
                   ));
        foreach ($categories as $terms) {
          $term_id = $terms->term_id;
          $data = ["user_id"=>$userid, "cat_id"=>$term_id, "cat_type"=>$catType];
          $wpdb->insert("wp_follow_cat", $data);
        }
    }
    if(isset($_POST["unfollow3"])) {
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $wpdb->query($wpdb->prepare("DELETE FROM wp_follow_cat WHERE user_id = %d AND cat_type = %s", $userid, $catType));
    }
  ?>
   <div class="catchybga">
      <div class="want">
       <h2><p>Write & Earn</p></h2>
        
         <p>"If you have knowledge, let others light their candles in it"<span>- Margaret Fuller</span></p>
        
     
     <div class="belowlinksedus">
      <ul>
        <li><a href="<?php if($_SESSION["login"]["id"]){echo "https://edukeeda.com/expertise-query-form/";}else{echo "#";}?>" <?php if($_SESSION["login"]["id"]){}else{ echo "data-toggle='modal' data-target='#myModalpost'"; }?>>Post Expertise</a></li>.
        <li><a href="#section1000" id="2000" class="click">Explore</a></li>.
        <li><a href="https://edukeeda.com/discussion-forum/">Discuss</a></li>
      </ul>
     </div>
      </div>
      
   </div>
   <div  class="insidecontainer">
      <div class="eventhead">
         <h1>Latest Posts</h1>
         <a href="<?php echo site_url();?>/latest-articles/">See all</a>
      </div>
      <div class="EVEN">
      <?php
          $args = array(
               'numberposts' => 4,
               'offset' => 0,
               'category' => 0,
               'orderby' => 'post_date',
               'order' => 'DESC',
               'include' => '',
               'exclude' => '',
               'meta_key' => '',
               'meta_value' =>'',
               'post_type' => array('industry','home_appliances','student_stuff','blogs'),
               'post_status' => 'publish',
               'suppress_filters' => true
             );
                     
         $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
         
         if(!empty($recent_posts))
         {
           
           foreach ($recent_posts as $value) {
             
            $cid = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = %d", $value['ID']));

            $getCatName = $wpdb->get_results( 
                $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $cid[0]->term_taxonomy_id));

            $image1 = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
             if($image1){
                  $image1 = $image1[0];
              } else {
                  $mkey = "_thumbnail_id";
                $checkimg = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $value['ID'],$mkey) 
                );
                $getimg = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                );
                $image1 = $getimg[0]->guid;  
              }
              
              $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $value['post_author']));
                           $author_name = $getAuthor[0]->display_name;

              $countPostTitle = strlen($value['post_title']);
              if($countPostTitle >= 70) {
                $postTitle = substr($value['post_title'],0, 70);
                $postTitle .= "...";
              } else {
                $postTitle = $value['post_title'];
              }
          ?>

         <div class="col-md-3">
            <div class="alt">
               <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">
                  <div class="altimg"> 
                     <img src="<?php echo $image1; ?>" style="height: 152px;width: 260px;">
                  </div>
               </a>
               <div class="caption"> 
               </div>
               <div class="spndat">
                  <span>
                  <?php
                    if($value['post_type'] == "industry") {
                  ?>
                    <a href="<?php echo site_url();?>/expertise/?id=<?php echo $cid[0]->term_taxonomy_id;?>&action=industry"> 
                  <?php
                    } else if($value['post_type'] == "home_appliances") {
                  ?>
                      <a href="<?php echo site_url();?>/expertise/?id=<?php echo $cid[0]->term_taxonomy_id;?>&action=home_appliances"> 
                  <?php
                    }  else if($value['post_type'] == "student_stuff") {
                  ?>
                      <a href="<?php echo site_url();?>/expertise/?id=<?php echo $cid[0]->term_taxonomy_id;?>&action=student_stuff"> 
                  <?php
                    }  else if($value['post_type'] == "blogs") {
                  ?>
                      <a href="<?php echo site_url();?>/expertise?id=<?php echo $cid[0]->term_taxonomy_id;?>&action=blogs">
                  <?php
                    }
                  ?>

                    <?php echo strtoupper($getCatName[0]->name); ?> </a></span>
                  <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">
                     <p><?php echo !empty($postTitle)?$postTitle:""; ?></p>
                  </a>
               </div>
               <div class="spn">
                  <div class="expview">
                     <p> 
                        <span> By :<?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1) { ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $value['post_author'];?>"> <?php echo $author_name; ?> </a>
                                          <?php }
                                              else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                          
                          <div class="dropdown customs1">      
                            <button class="dropbtn">
                               <i class="fa fa-share-alt" aria-hidden="true"></i>
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($value['ID']) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                          

                        </span>
                     </p>
                  </div>
               </div>
            </div>
         </div>
         <?php
            }
          }
         ?>
         
      </div>
      <div class="EVEN" id="section1000">

         <div class="odd">
            <h2>Industry/Business</h2>
              <?php
                if(isset($_SESSION["login"])) {
                  $userId = $_SESSION["login"]["id"];
                  $categories = get_categories( array(
                   'child_of'            => 0,
                   'parent'              => 0,
                   'current_category'    => 0,
                   'depth'               => 0,
                   'echo'                => 1,
                   'exclude'             => '',
                   'exclude_tree'        => '',
                   'feed'                => '',
                   'feed_image'          => '',
                   'feed_type'           => '',
                   'hide_empty'          => 0,
                   'hide_title_if_empty' => false,
                   'hierarchical'        => true,
                   'order'               => 'ASC',
                   'orderby'             => 'name',
                   'separator'           => '<br />',
                   'show_count'          => 0,
                   'show_option_all'     => '',
                   'show_option_none'    => __( 'No categories' ),
                   'style'               => 'list',
                   'taxonomy'            => 'expert_gyan_category',
                   'title_li'            => __( 'Categories' ),
                   'use_desc_for_title'  => 1,
               ) );
               
                if(!empty($categories))
                {
                  $x=1;
                  foreach ($categories as $terms) {
                    $term_id = $terms->term_id;
                    $checkFollow = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_follow_cat WHERE user_id=%d AND cat_id=%d", $userId, $term_id) 
                    );
                    
                    if($checkFollow) {
                      $x++;
                    }
                  }
                    if($x > 1) {
                ?>
                      <form method="post" action="">
                        <input type="hidden" name="catType" value="industry">
                        <center><input type="submit" name="unfollow" value="Unfollow All" class="btn btn-warning followbtn"></center>
                      </form>
                <?php
                    } else {
                ?>
                      <form method="post" action="">
                        <input type="hidden" name="catType" value="industry">
                        <center><input type="submit" name="follow" value="Follow All" class="btn btn-warning followbtn"></center>
                      </form>
            <?php
                    }
                }
              } else {
             ?>
                  <button type="button" class="btn btn-warning followbtn" data-toggle="modal" data-target="#myModal">Follow All</button>
            <?php
              }
            ?>
            </h2>
            <?php
               $categories = get_categories( array(
                   'child_of'            => 0,
                   'current_category'    => 0,
                   'parent'              => 0,
                   'depth'               => 0,
                   'echo'                => 1,
                   'exclude'             => '',
                   'exclude_tree'        => '',
                   'feed'                => '',
                   'feed_image'          => '',
                   'feed_type'           => '',
                   'hide_empty'          => 0,
                   'hide_title_if_empty' => false,
                   'hierarchical'        => true,
                   'order'               => 'ASC',
                   'orderby'             => 'name',
                   'separator'           => '<br />',
                   'show_count'          => 0,
                   'show_option_all'     => '',
                   'show_option_none'    => __( 'No categories' ),
                   'style'               => 'list',
                   'taxonomy'            => 'expert_gyan_category',
                   'title_li'            => __( 'Categories' ),
                   'use_desc_for_title'  => 1,
               ) );
               
                if(!empty($categories))
                {
                    foreach ($categories as $terms) {
                    
                    $term_id = $terms->term_id;
                    $image   = category_image_src( array('term_id'=>$term_id) , false );
                    $category_link = get_category_link( $term_id );
                ?>
                     <div class="col-md-3">
                        <a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id; ?>&action=industry">
                           <div class="even1">
                              <img src="<?php echo !empty($image)?$image:"/wp-content/themes/sydney/images/person-interviewing-200n-012110.jpg"; ?>">
                              <p><?php echo $terms->name; ?></p>
                            </div>
                        </a>
                     </div>
               <?php 
                   }
                }
            ?>
         </div>
      </div>
      
      <div class="EVEN">
       <div class="odd">
          <h2>College Stuff & Career
            <?php
                    if(isset($_SESSION["login"])) {
                      $userId = $_SESSION["login"]["id"];
                      $categories22 = get_categories( array(
                       'child_of'            => 0,
                       'parent'              => 0,
                       'current_category'    => 0,
                       'depth'               => 0,
                       'echo'                => 1,
                       'exclude'             => '',
                       'exclude_tree'        => '',
                       'feed'                => '',
                       'feed_image'          => '',
                       'feed_type'           => '',
                       'hide_empty'          => 0,
                       'hide_title_if_empty' => false,
                       'hierarchical'        => true,
                       'order'               => 'ASC',
                       'orderby'             => 'name',
                       'separator'           => '<br />',
                       'show_count'          => 0,
                       'show_option_all'     => '',
                       'show_option_none'    => __( 'No categories' ),
                       'style'               => 'list',
                       'taxonomy'            => 'social_stuff_category',
                       'title_li'            => __( 'Categories' ),
                       'use_desc_for_title'  => 1,
                   ) );
                   
                    if(!empty($categories22))
                    {
                      $x2 = 1;
                      foreach ($categories22 as $terms22) {
                        $term_id22 = $terms22->term_id;
                        $checkFollow22 = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_follow_cat WHERE user_id=%d AND cat_id=%d", $userId, $term_id22) 
                        );

                        if($checkFollow22) {
                          $x2++;
                        }
                      }
                        if($x2 > 1) {
                    ?>
                          <form method="post" action="">
                            <input type="hidden" name="catType" value="student_stuff">
                            <center><input type="submit" name="unfollow2" value="Unfollow All" class="btn btn-warning followbtn"></center>
                          </form>
                    <?php
                        } else {
                    ?>
                          <form method="post" action="">
                            <input type="hidden" name="catType" value="student_stuff">
                            <center><input type="submit" name="follow2" value="Follow All" class="btn btn-warning followbtn"></center>
                          </form>
                <?php
                        }
                    }
                  } else {
                 ?>
                      <button type="button" class="btn btn-warning followbtn" data-toggle="modal" data-target="#myModal">Follow All</button>
                <?php
                  }
                ?>
          </h2>
          <?php
             $categories2 = get_categories( array(
                 'child_of'            => 0,
                 'parent'              => 0,
                 'current_category'    => 0,
                 'depth'               => 0,
                 'echo'                => 1,
                 'exclude'             => '',
                 'exclude_tree'        => '',
                 'feed'                => '',
                 'feed_image'          => '',
                 'feed_type'           => '',
                 'hide_empty'          => 0,
                 'hide_title_if_empty' => false,
                 'hierarchical'        => true,
                 'order'               => 'ASC',
                 'orderby'             => 'name',
                 'separator'           => '<br />',
                 'show_count'          => 0,
                 'show_option_all'     => '',
                 'show_option_none'    => __( 'No categories' ),
                 'style'               => 'list',
                 'taxonomy'            => 'social_stuff_category',
                 'title_li'            => __( 'Categories' ),
                 'use_desc_for_title'  => 1,
             ) );
             
              if(!empty($categories2))
              {
                  foreach ($categories2 as $terms2) {
                  
                  $term_id2 = $terms2->term_id;
                  $image2  = category_image_src( array('term_id'=>$term_id2) , false );
                  $category_link2 = get_category_link( $term_id2 );
          ?>
                <div class="col-md-3">
                   <a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id2; ?>&action=student_stuff">
                      <div class="even1">
                         <img src="<?php echo !empty($image2)?$image2:"/wp-content/themes/sydney/images/person-interviewing-200n-012110.jpg"; ?>">
                         <p><?php echo $terms2->name; ?></p>
                      </div>
                    </a>
                </div>
          <?php 
             }
               }
          ?>
          </div>
        </div>
      
        <div class="EVEN">
          <div class="odd">
          <h2>Blogs & Opinion
            <?php
                    if(isset($_SESSION["login"])) {
                      $userId = $_SESSION["login"]["id"];
                      $categories33 = get_categories( array(
                       'child_of'            => 0,
                       'parent'              => 0,
                       'current_category'    => 0,
                       'depth'               => 0,
                       'echo'                => 1,
                       'exclude'             => '',
                       'exclude_tree'        => '',
                       'feed'                => '',
                       'feed_image'          => '',
                       'feed_type'           => '',
                       'hide_empty'          => 0,
                       'hide_title_if_empty' => false,
                       'hierarchical'        => true,
                       'order'               => 'ASC',
                       'orderby'             => 'name',
                       'separator'           => '<br />',
                       'show_count'          => 0,
                       'show_option_all'     => '',
                       'show_option_none'    => __( 'No categories' ),
                       'style'               => 'list',
                       'taxonomy'            => 'blog_category',
                       'title_li'            => __( 'Categories' ),
                       'use_desc_for_title'  => 1,
                   ));
                   
                    if(!empty($categories33))
                    {
                      $x3 = 1;
                      foreach ($categories33 as $terms33) {
                        $term_id33 = $terms33->term_id;
                        $checkFollow33 = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_follow_cat WHERE user_id=%d AND cat_id=%d", $userId, $term_id33) 
                        );
                        
                        if($checkFollow33) {
                          $x3++;
                        }
                      }
                        if($x3 > 1) {
                    ?>
                          <form method="post" action="">
                            <input type="hidden" name="catType" value="blog-posting">
                            <center><input type="submit" name="unfollow3" value="Unfollow All" class="btn btn-warning followbtn"></center>
                          </form>
                    <?php
                        } else {
                    ?>
                          <form method="post" action="">
                            <input type="hidden" name="catType" value="blog-posting">
                            <center><input type="submit" name="follow3" value="Follow All" class="btn btn-warning followbtn"></center>
                          </form>
                <?php
                        }
                    }
                  } else {
                 ?>
                      <button type="button" class="btn btn-warning followbtn" data-toggle="modal" data-target="#myModal">Follow All</button>
                <?php
                  }
                ?>
          </h2>
          <?php
             $categories3 = get_categories( array(
                 'child_of'            => 0,
                 'parent'              => 0,
                 'current_category'    => 0,
                 'depth'               => 0,
                 'echo'                => 1,
                 'exclude'             => '',
                 'exclude_tree'        => '',
                 'feed'                => '',
                 'feed_image'          => '',
                 'feed_type'           => '',
                 'hide_empty'          => 0,
                 'hide_title_if_empty' => false,
                 'hierarchical'        => true,
                 'order'               => 'ASC',
                 'orderby'             => 'name',
                 'separator'           => '<br />',
                 'show_count'          => 0,
                 'show_option_all'     => '',
                 'show_option_none'    => __( 'No categories' ),
                 'style'               => 'list',
                 'taxonomy'            => 'blog_category',
                 'title_li'            => __( 'Categories' ),
                 'use_desc_for_title'  => 1,
             ) );
             
              if(!empty($categories3))
              {
                  foreach ($categories3 as $terms3) {
                  
                  $term_id3 = $terms3->term_id;
                  $image3  = category_image_src( array('term_id'=>$term_id3) , false );
                  $category_link3 = get_category_link( $term_id3 );
          ?>
                <div class="col-md-3">
                   <a href="<?php echo site_url(); ?>/expertise/?id=<?php echo $term_id3; ?>&action=blogs">
                      <div class="even1">
                         <img src="<?php echo !empty($image3)?$image3:"/wp-content/themes/sydney/images/person-interviewing-200n-012110.jpg"; ?>">
                         <p><?php echo $terms3->name; ?></p>
                      </div>
                    </a>
                </div>
          <?php 
             }
               }
          ?>
       </div>

       <div class="EVEN">
        <div class="odd">
          <h2>Home Appliances
            <?php
                    if(isset($_SESSION["login"])) {
                      $userId = $_SESSION["login"]["id"];
                      $categories11 = get_categories( array(
                       'child_of'            => 0,
                       'parent'              => 0,
                       'current_category'    => 0,
                       'depth'               => 0,
                       'echo'                => 1,
                       'exclude'             => '',
                       'exclude_tree'        => '',
                       'feed'                => '',
                       'feed_image'          => '',
                       'feed_type'           => '',
                       'hide_empty'          => 0,
                       'hide_title_if_empty' => false,
                       'hierarchical'        => true,
                       'order'               => 'ASC',
                       'orderby'             => 'name',
                       'separator'           => '<br />',
                       'show_count'          => 0,
                       'show_option_all'     => '',
                       'show_option_none'    => __( 'No categories' ),
                       'style'               => 'list',
                       'taxonomy'            => 'explore_gyan_category',
                       'title_li'            => __( 'Categories' ),
                       'use_desc_for_title'  => 1,
                   ) );
                   
                    if(!empty($categories11))
                    {
                      $x1 =1;
                      foreach ($categories11 as $terms11) {
                        $term_id11 = $terms11->term_id;
                        $checkFollow11 = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_follow_cat WHERE user_id=%d AND cat_id=%d", $userId, $term_id11) 
                        );
                        
                        if($checkFollow11) {
                          $x1++;
                        }
                      }
                        if($x1 > 1) {
                    ?>
                         <form method="post" action="">
                            <input type="hidden" name="catType" value="home_appliances">
                            <center><input type="submit" name="unfollow1" value="UnFollow All" class="btn btn-warning followbtn"></center>
                          </form>
                    <?php
                        } else {
                    ?>
                          <form method="post" action="">
                            <input type="hidden" name="catType" value="home_appliances">
                            <center><input type="submit" name="follow1" value="Follow All" class="btn btn-warning followbtn"></center>
                          </form>
                <?php
                        }
                    }
                  } else {
                 ?>
                      <button type="button" class="btn btn-warning followbtn" data-toggle="modal" data-target="#myModal">Follow All</button>
                <?php
                  }
                ?>
          </h2>
          <?php
             $categories1 = get_categories( array(
                 'child_of'            => 0,
                 'parent'              => 0,
                 'current_category'    => 0,
                 'depth'               => 0,
                 'echo'                => 1,
                 'exclude'             => '',
                 'exclude_tree'        => '',
                 'feed'                => '',
                 'feed_image'          => '',
                 'feed_type'           => '',
                 'hide_empty'          => 0,
                 'hide_title_if_empty' => false,
                 'hierarchical'        => true,
                 'order'               => 'ASC',
                 'orderby'             => 'name',
                 'separator'           => '<br />',
                 'show_count'          => 0,
                 'show_option_all'     => '',
                 'show_option_none'    => __( 'No categories' ),
                 'style'               => 'list',
                 'taxonomy'            => 'explore_gyan_category',
                 'title_li'            => __( 'Categories' ),
                 'use_desc_for_title'  => 1,
             ) );
             
              if(!empty($categories1))
              {
                  foreach ($categories1 as $terms1) {
                  
                  $term_id1 = $terms1->term_id;
                  $image1   = category_image_src( array('term_id'=>$term_id1) , false );
                  $category_link1 = get_category_link( $term_id1 );
          ?>
                <div class="col-md-3">
                   <a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id1; ?>&action=home_appliances">
                      <div class="even1">
                         <img src="<?php echo !empty($image1)?$image1:"/wp-content/themes/sydney/images/person-interviewing-200n-012110.jpg"; ?>">
                         <p><?php echo $terms1->name; ?></p>
                      </div>
                    </a>
                </div>
          <?php 
             }
               }
          ?>
        </div>
      </div>

     </div>
  </div>
</div>
<?php while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'content', 'page' ); ?>
<?php
   // If comments are open or we have at least one comment, load up the comment template
   if ( comments_open() || get_comments_number() ) :
     comments_template();
   endif;
   ?>
<?php endwhile; // end of the loop. ?>
</main><!-- #main -->
</div><!-- #primary -->
</div>  
<?php get_footer(); ?>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Follow</h4>
      </div>
      <div class="modal-body">
        <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
      </div>
    </div>
  </div>
</div>

<style>
.dropdown.customs1 .dropbtn{    
  background-color: #fff;
    color: #000;
}

   .page-wrap{
         padding-top: 68px;
   }
   .wantpost a {
   border-radius: 10px;
   background: #f68e2f;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .want a {
   border-radius: 10px;
   background: #f68e2f;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
.want h2 p {
    font-size: 20px;
    color: #fff;
    position: absolute;
    top: 108%;
    left: 46%;
}
.want p{
    position: absolute;
    top: 325px;
    left: 22%;
    font-size: 20px;
}
.want span {
    color: #f68e2f;
}
   .even1 img {
   width: 100%;
   border-radius: 10px 10px 10px 10px;
   max-height: 200px;
   height: 180px;
   }
   .altimg img {
   border-radius: 7px 7px 0px 0px;
   }
   .EVEN {
   float: left;
   width: 100%;
   margin-bottom: 35px;
   }
   span.dateDisplay-day {
   color: #f68e2f;
   font-size: 20px;
   font-weight: 800;
   }
   span.dateDisplay-month.text--tiny {
   color: #020253;
   font-size: 20px;
   font-weight: 800;
   }
   .spn img {
   border-radius: 60%;
   width: 56px;
   height: 56px;
   }
   .spn a {
   color: black;
   }
   .spn a:hover {
   color: orange;
   }
  
   .odd {
   float: left;
   width: 100%;
  
   }
   div#sidebar-footer {
   float: left;
   width: 100%;
   }
   .eventhead a {
   float: right;
   color: #f68e2f;
    padding: 0px 16px;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 79px;
   }
   .eventnew .container2 {
   margin: 0 auto;
   max-width: 1170px;
   }
   .eventnew .content-wrapper.container {
   width: 100%;
   margin: 0 auto;
   padding: 0;
   }
   .eventnew .content-wrapper.container > .row{
   margin:0;
   }
   .catchybga {
   padding: 14% 0;
   background: url(/img/expertise-image.png);
   background-size: cover;
   background-position: 100%;
   position: relative;
   }
   div#sidebar-footer {
   float: left;
   width: 100%;
   }
   .alt thumbnail{
   float: left;
   position: relative;
   }
   .alt h1 {
   font-size: 31px;
   }
   .eventhead h1 {
   font-size: 31px;
   float: left;
   margin: 0 0 60px 15px;
   }
   .datespn {
   position: absolute;
   top: 16px;
   left: 33px;
   background: #fff;
   padding: 7px;
   color: #060658;
   font-size: 24px;
   text-align: center;
   border: 1px solid lavenderblush;
   border-radius: 7px;
   line-height: 17px;
   }
   .thumbnail strong {
   width: 100px;
   }
   .thumbnail .caption {
   padding: 9px;
   color: #333;
   float: left;
   width: 100%;
   }
   .spndat span {
   font-size: 17px;
   color: #b2b2bd;;
   float: left;
   width: 100%;
   }
   .spndat {
   padding: 16px;
   }
   .spndat p {
   color: black;
   font-size: 17px;
   float: left;
   width: 100%;
   line-height: 20px;
   margin-bottom: 13px;
   margin-top: 0px;
   font-weight: 600;
   }
   .spndat p:hover{
   color: #f68e2f;
   }
   .spn span {
   display: block;
   color: #c2b2bd;
   }
   .alt .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img{
   display: block;
   max-width: 100%;
   height: 150px;
   width: 333px;
   }
   .alt:hover{
   box-shadow: 3px 6px 2px 0 rgba(46,62,72,.12), 0 2px 4px 0 rgba(46,62,72,.12)
   }
   .spn {
   padding: 0px;
   margin: 15px;
   }
   .odd h2 {
   font-size: 20px;
   margin: 0 0 50px 15px;
   }
   .even1{
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   margin-bottom: 35px;
   background-size: cover;
   text-align: center;
   color: white;
   }
   .even1 p {
   color: black;
   font-size: 16px;
   font-weight: 600;
   padding-bottom: 16px;
    padding-top: 10px;
   }
   .thumbnail{
   box-shadow:0 0 2px 0 rgba(46,62,72,.12), 0 2px 4px 0 rgba(46,62,72,.12);
   border-radius: 0px 0px 10px 10px;
   }
   .thumbnail:hover{
   box-shadow: 2px 4px 0px 0 rgba(0, 0, 0, 0.07);
   }
   .want {
   text-align: left;
    color: white;
    position: absolute;
    top: 0%;
    width: 100%;
   }
   .belowlinksedus ul li a {
    font-size: 30px;
}
   .want h3 {
   color: white;
    font-size: 22px;
    font-weight: 100;
    margin-bottom: 13px;
   }
   .want a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .wantpost {
   TEXT-ALIGN: CENTER;
   }
   .wantpost p {
   color: white;
   font-size: 20px;
   position: ABSOLUTE;
   left: 0;
   right: 0;
   margin: 13 AUTO;
   FLOAT: NONE;
   WIDTH: AUTO;
   BOTTOM: 0px;
   }
   .wantpost a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .want p {
   position: absolute;
    top: 325px;
    left: 22%;
    font-size: 22px;
   }
    .belowlinksedus {
    position: absolute!important;
    bottom: 0;
    right: 0;
    left:5%!important;
    top: 150px!important;
    text-align: center;
}
   .expview strong {
   color: black;
   margin: 0px 11px;
   }
   .hentry {
   float: left;
   width: 100%;
   }
   .expview i.fa.fa-share-alt {
   float: right;
   }
   div#section1000 .odd h2 {
    font-size: 20px;
    margin: 0 0 50px 20px;
    float: left;
    width: 80%;
}
   
   @media only screen and (max-width: 767px)
   {
       .EVEN img {
    width: 100%!important;
}
div#section1000 .odd h2 {
    font-size: 20px;
    margin: 0 0 20px 0px;
    float: left;
    width: 100%;
        padding: 0px 15px;
}
.odd h2 {
    font-size: 17px;
    margin: 0 0 50px 15px;
}
.belowlinksedus {
    position: absolute!important;
    bottom: 0;
    right: 0;
    left: 4%!important;
    top: 290%!important;
}
.catchybga{
  padding: 23% 0;
      margin-top: 65px;
}

   .want h3 {
   color: white;
    font-size: 13px;
    font-weight: 100;
    padding: 5px;
   }
   .wantpost p {
   color: white;
   font-size: 17px;
   position: ABSOLUTE; 
   margin: 0 AUTO;
   BOTTOM: 0px;
   font-weight: unset;
   }
   .want h2 p {
    font-size: 14px;
    color: #fff;
    position: absolute;
    top: 100%;
    left: 38%;
       font-weight: 100;
margin-bottom:0px;
}
   .want p {
    font-size: 11px;
    position: absolute;
    top: 141px;
    left: 5%;

   }
   .want{
     top: 0%;
   }
   
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 20px;
   }
   .eventhead h1 {
   font-size: 29px;
   float: left;
   margin: 0 0 20px 10px;
   }
   .eventhead a {
   float: right;
   MARGIN: 0px 19px;
   }
   .alt{
   margin-bottom: 12px;
   }
   .insidecontainer {
    max-width: 1170px;
    margin: 0 auto;
    padding: 0 0px;
}
.page-wrap {
    padding-top: 0px;
}
.belowlinksedus ul li a {
    font-size: 14px!important;
font-weight: 600;
}
   }
</style>
<!-- Modal -->
<div id="myModalpost" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Posting</h4>
         </div>
         <div class="modal-body">
            <p> For Posting please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>
