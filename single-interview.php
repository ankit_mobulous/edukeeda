<?php
/**
 * The template for displaying all single posts.
 *
 * @package Sydney
 */

get_header(); ?>
<style type="text/css">
.post-navigation .nav-next span {
    margin-left: 0px;
}
    .customsideedu h3.widget-title {
        border: 1px solid #ffaf8c;
        padding: 6px 0px 4px 0;
        margin: 0 0 10px 0;
        border-left: none;
        border-right: none;
        font-size: 16px;
        line-height: normal;
        font-weight: 600;
        text-transform: uppercase;
    }
    #secondary {
    background-color: #f4f4f4;
    margin-top: 50px;
}
    .simg {
    float: left;
    width: 100%;
}

.simg a {
    float: left;
    width: 100%;
}
.pimg {
    float: left;
    width: 100%;
}
.pimg p {
    margin-bottom: 0px;
  line-height: 15px;
}

.hrtagstyleborder{border-bottom: 1px solid #ddd;}

.pimg a {
    line-height: 15px;
    float: left;
    width: 100%;
    margin: 4px 0px;
}
@media only screen and (max-width: 767px)
   {
.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
}

.single .hentry .title-post {
    font-size: 25px;
}
   }
   
</style>

    <?php if (get_theme_mod('fullwidth_single')) { //Check if the post needs to be full width
        $fullwidth = 'fullwidth';
    } else {
        $fullwidth = '';
    } ?>

    <?php do_action('sydney_before_content'); ?>

    <div id="primary" class="content-area col-md-8 <?php echo $fullwidth; ?>">

        <?php sydney_yoast_seo_breadcrumbs(); ?>

        <main id="main" class="post-wrap" role="main">

        <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'content', 'interview' ); ?>
<hr class="hrtagstyle">
<hr class="hrtagstyle">
            <?php sydney_post_navigation(); ?>
        <hr class="hrtagstyleborder">
            <?php
                // If comments are open or we have at least one comment, load up the comment template
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;
            ?>

        <?php endwhile; // end of the loop. ?>

        </main><!-- #main -->
    </div><!-- #primary -->

    <?php do_action('sydney_after_content'); ?>

<?php if ( get_theme_mod('fullwidth_single', 0) != 1 ) {
    ?>
    <div class="col-md-4 customsideedu">
  <div id="secondary" class="widget-area" role="complementary">
        <aside id="recent-posts-3" class="widget widget_recent_entries">
            <h3 class="widget-title">Top Trending Interviews</h3>
            <?php
              $args1 = array(
                'numberposts' => 3,
                'offset' => 0,
                'category' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' =>'',
                'post_type' => 'interview',
                'post_status' => 'publish',
                'suppress_filters' => true
              );
              
              $recent_posts1 = wp_get_recent_posts( $args1, ARRAY_A );
              //print_r($recent_posts);
              if(!empty($recent_posts1))
              {
                
                foreach ($recent_posts1 as $value1) {
                  $i1=0;
                 $cats1 = get_the_category($value1["ID"]);
                  $image1 = wp_get_attachment_image_src( get_post_thumbnail_id( $value1['ID'] ), 'single-post-thumbnail' );
                  $countPostTitle = strlen($value1['post_title']);
                    if($countPostTitle >= 30) {
                      $postTitle = substr($value1['post_title'],0, 30);
                      $postTitle .= "...";
                    } else {
                      $postTitle = $value1['post_title'];
                    }
               ?>
             <div class="othermemd">
                <div class="col-md-5">
                   <div class="simg">
                      <a href="<?php echo esc_url( get_permalink($value1['ID']) ); ?>">   <img src="<?php echo $image1[0]; ?>" class="img-thumbnail"></a>
                   </div>
                </div>
                <div class="col-md-7">
                   <div class="pimg">
                      <p> <strong><a href="<?php site_url();?>/interviews/">INTERVIEWS</a></strong></p>
                      <a href="<?php echo esc_url( get_permalink($value1['ID']) ); ?>">
                         <span class="authorname">
                         <?php echo !empty($postTitle)?$postTitle:""; ?></span>
                      </a>
                      <span>By Admin</span>
                   </div>
                </div>
             </div>
             <?php
                }
                }
             ?>
        </aside>
    </div>
    </div>
<?php
} ?>
<?php get_footer(); ?>
