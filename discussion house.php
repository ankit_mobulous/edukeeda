<div class="homepagemenedu discussionback">
<?php
   /*
   Template Name: DiscussionHouse
   */
if($_GET['action']){
}else{
header('Location: http://edukeeda.com/discussion-forum/');
exit;
}
   get_header(); ?>
<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   include_once $path . '/wp-config.php';
   include_once $path . '/wp-load.php';
   include_once $path . '/wp-includes/wp-db.php';
   include_once $path . '/wp-includes/pluggable.php';
   
   global $wpdb;
   
   $bid = (int) $_GET["action"];
   $table_name = "wp_discuss_house_views";
   $checkViews = $wpdb->get_results( 
         $wpdb->prepare( "SELECT * FROM $table_name WHERE house_id = %d", $bid) 
   );
   if($checkViews){
       $valView = $checkViews[0]->view_count + 1;
       $wpdb->update($table_name, array('view_count'=>$valView), array('house_id'=>$bid));
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   } else {
       $data = array("house_id" =>$bid,"view_count"=>1);
       $wpdb->insert($table_name, $data);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   
   if(isset($_POST["follow"])) {
     $quesId = $_POST["quesId"];
     $userid = $_SESSION["login"]["id"];
     $data = ["ques_id"=>$quesId, "user_id"=>$userid];
     $wpdb->insert("wp_discussion_ques_follow", $data);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   if(isset($_POST["unfollow"])) {
     $quesId = $_POST["quesId"];
     $userid = $_SESSION["login"]["id"];
     $wpdb->query($wpdb->prepare("DELETE FROM wp_discussion_ques_follow WHERE user_id = %d AND ques_id=%d", $userid, $quesId));
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   
   if(isset($_POST['joinhousesubmit'])) {
       $houseId = $_POST["hid"];
       $userid = $_SESSION["login"]["id"];
       $data = ["joinId"=>$houseId, "user_id"=>$userid];
       $wpdb->insert("wp_discussion_join", $data);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   if(isset($_POST['unjoinhousesubmit'])) {
?>
<?php
       $houseId = $_POST["hid"];
       $userid = $_SESSION["login"]["id"];
       $wpdb->query($wpdb->prepare("DELETE FROM wp_discussion_join WHERE user_id = %d AND joinId=%d", $userid, $houseId));
?> 
    <script type="text/javascript">
       if ( window.history.replaceState ) {
           window.history.replaceState( null, null, window.location.href );
       }
    </script>
<?php
   }
   
   if(isset($_POST['postquessubmit'])) {
     $cid = $_POST['cid'];
     $scid = $_POST['scid'];
     $hid = $_POST['hid'];
     $userid = $_SESSION["login"]["id"];
     $quesPost = $_POST['quesPost'];
     $data = ["main_id"=>$cid, "sub_cat_id"=>$scid, "house_id"=>$hid, "user_id"=>$userid, "ques"=>$quesPost, "status"=>0];
     $wpdb->insert("wp_discussion_ques", $data);
     $msgQues = "Question posted successfully. Waiting for admin approval";
   }
   ?>
<?php if(isset($msgQues)) { ?>
<script type="text/javascript">
   alert("Question posted successfully. Waiting for admin approval");
</script>
<?php  
   unset($msgQues); 
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   } 
   ?>
<?php
   if(isset($_POST['anspostsubmit'])) {
     $qid = $_POST['qid'];
     $userid = $_SESSION["login"]["id"];
     $ansPost = $_POST['ansPost'];
$ansPost = stripslashes($ansPost);
     $data = ["ques_id"=>$qid, "user_id"=>$userid, "ans_content"=>$ansPost];
     $wpdb->insert("wp_discussion_ans", $data);
     $msgAns = "Answer posted successfully. Waiting for admin approval";
   }
   ?>
<?php if(isset($msgAns)) { ?>
<script type="text/javascript">
   alert("Answer posted successfully. Waiting for admin approval");
</script>
<?php  
   unset($msgAns); 
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   
   if(isset($_POST['repostsubmit'])) {
      $type = $_POST['type'];
      $quesid = $_POST['quesid'];
      $ques = $_POST['ques'];
      $reposttext = $_POST['reposttext'];
      $userid = $_SESSION["login"]["id"];
      $data = ["user_id"=>$userid, "content"=>$ques, "shareType"=>$type, "shareid"=>$quesid, "sharetext"=>$reposttext]; 
      $wpdb->insert("wp_wallpost", $data);
   ?>
        <script type="text/javascript">
           if ( window.history.replaceState ) {
               window.history.replaceState( null, null, window.location.href );
           }
        </script>
<?php
   }


  if(isset($_POST['submitReport'])) {
    $qid = $_POST['qid'];
    $reason = $_POST['reason'];
    $userid = $_SESSION["login"]["id"];

    $data = ["user_id"=>$userid, "quesid"=>$qid, "status"=>"report", "reason"=>$reason];
    $wpdb->insert("wp_question_hide", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['repostanssubmit'])) {
      $type = $_POST['type'];
      $ans = $_POST['ans'];
      $ansid = $_POST['ansid'];
      $reposttext = $_POST['reposttext'];
      $userid = $_SESSION["login"]["id"];

      $data = ["user_id"=>$userid, "content"=>$reposttext, "shareType"=>$type, "shareid"=>$ansid, "sharetext"=>$ans];
      $wpdb->insert("wp_wallpost", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['ansdeletesubmit'])) {
        $aid = $_POST['aid'];
        $wpdb->query($wpdb->prepare("DELETE FROM wp_discussion_ans WHERE id = %d", $aid));
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['likeSubmit'])) {
    $ansid = $_POST['ansid'];
    $userid = $_SESSION["login"]["id"];
    $data = ["user_id"=>$userid, "ans_id"=>$ansid];
    $wpdb->insert("wp_answer_like", $data);
?>
      <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
     </script>
<?php
  }

  if(isset($_POST['unlikeSubmit'])) {
    $ansid = $_POST['ansid'];
    $userid = $_SESSION["login"]["id"];
    $wpdb->query($wpdb->prepare("DELETE FROM wp_answer_like WHERE ans_id = %d AND user_id=%d", $ansid, $userid));
?>
      <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
     </script>
<?php
  }
?>
<style>
 
   .imgforr {
   text-align: center;
   }
   .imgforr img.img-thumbnail {
   width: 60px;
   height: 60!important;;
   border-radius: 100%;
   }
   .plug img {
   width: 20px;
   float:right;
   }
   .plug {
   text-align: right;
   float: left;
   margin-top: 7px;
   }
   .discussionback .firstpostdefults
   {
      background: #9a9a9a17;
   }
   .dropfilter {
    margin-bottom:0px;

}
   .post span {
   margin-right: 6px;
   }
   .post strong {
   color: #373779;
   }
   .post .dotsseprate {
   margin: 0 6px;
   }
   .plugin { 
   float: right;
   color:#9a9a9a;
   }
   .join {
   float: left;
   width: 100%;
   }
   .join form {
   padding: 8px 7px;
   }
   .join .like {
   padding: 0px 10px;
   }
   .pp {
   float: left;
   width: 100%;
   display: -webkit-box;
   margin: 10px 0px;
   }
   .join .hjh {
   float: left!important;
   width: 100%!important;
   display: inline-table!important;
   }
   .pp span {
   margin-right: 10px;
   }
   .pp strong {
   margin-right: 10px;
   color: #373779;
   }
   .catp strong {
   color: #060658;
   font-weight: 600;
   }
   .catp span {
   margin: 5px;
   color: #4c4242;
   }
   .text {
   float: left;
   width: 100%;
   margin-bottom: 15px;

   }
   .text p {
   font-weight: 800;
   color: #4c4242;
   line-height: 30px;
   font-size: 29px!important;
   }
   .share .fa {
   color: #060658;
   }
   .share1 .fa {
   color: #060658;
   }
   .share2 .fa {
   color: #060658;
   }
   .dot {
   float: right;
   }
   .questio {
   float: left;
   width: 100%;
   margin-bottom: 20px;
   }
   .dot span.countno {
   margin: 0px;
   float: left;
   }
   .dot ul.dropdown-menu {
   min-width: 217px;
   padding: 3px;
   }
   .modal-body p {
   margin-bottom: 0px;
   padding: 3px;
   }
   .post{
   float: left;
   width: 100%;
   display: -webkit-box;
   margin-bottom: 20px;
   }
   #articleblous .post
   {
   color:#9a9a9a;
   }
   #articleblous .post strong
   {
   color:#9a9a9a;
   }
   #articleblous .share
   {
   color:#9a9a9a;
   }
   #articleblous .share1{
   color:#9a9a9a;
   }
   #articleblous .share2{
   color:#9a9a9a;
   }
   #articleblous .share .fa{
   color:#9a9a9a;
   }
   #articleblous .share1 .fa{
   color:#9a9a9a;
   }
   #articleblous .share2 .fa{
   color:#9a9a9a;
   }
   #articleblous .share1 button{
   color:#9a9a9a;
   }
   .direct p {
   color: #9a9a9a;
   }
   .direct a {
   color: #9a9a9a;
   }
   .questio p.rigwd
   {
   color:#9a9a9a;
   }
   .like button.disjoinhouse {
   color: #000;
   font-weight: 600;
   font-size: 15px;
   padding: 4px 8px;
   } 
   .text1 {
   float: left;
   width: 100%;
   margin-bottom: 0px;
   }
   .pluging {
   float: right;
   color:#9a9a9a;
   }
   .like {
   float: left;
   width: 100%;
   display: flex;
   color:#9a9a9a;
   }
   .join a {
   font-size: 12px;
   margin: 0px;
   padding: 0px;
   background: none;
   color: #000;
   border-radius: 5px;
   font-weight:600;
   }
   .join a:hover {
   font-size: 12px!important;
   margin: 0px;
   padding: 5px;
   background: #fff;
   color:#000;
   border-radius: 5px;
   }
   .share1 button {
   color: #102358;
   font-size: 14px;
   text-transform: capitalize;
   background: transparent;
   border: none;
   padding: 0 6px;
   }
   .share1 img {
   width: 15px;
   height: auto;
   }  
   .text1 p {
   font-weight: 800;
   color:#f68e2f!important;
   line-height: 30px;
   font-size: 25px!important;
   }
   .text1 p a{
   font-weight: 800;
   color: #4c4242;
   line-height: 30px;
   font-size: 25px!important;
   }
   .text1 h2 {
   font-size: 28px;
   color: #4c4242;
   }
   .house button {
   font-size: 12px;
   margin: 14px;
   padding: 5px;
   }
   .house {
   float: left;
   width: 100%;
   margin: 35px;
   margin-top: 50px;
   }
   .plug img {
   width: 20px;
   }
   .plug {
   text-align:right;}
   .cat a {
   color: #000000bd;
   font-weight: 600;
   }.catp a {
   color: #060658;
   font-weight: 600;
   }
   .cat span {
   color: #060658;
   margin: 7px;
   }
   .cat {
   margin: 6px 0px;
   }
   .headdiscus h1 {
   font-size: 25px;
   text-align: center;
   }
   .accordion {
   background-color: #eee;
   color: #444;
   cursor: pointer;
   padding: 18px;
   width: 100%;
   border: none;
   text-align: left;
   outline: none;
   font-size: 15px;
   transition: 0.4s;
   }
   .active, .accordion:hover {
   background-color: #ccc; 
   }
   .headarea .active, .accordion:hover {
   background-color: transparent;
   }
   .accordion:after { 
   content: ' \002B';
   font-size: 21px;
   margin: 1px;
   padding: 0px;
   }
   .panel {
   padding: 0 18px;
   display: none;
   background-color: white;
   overflow: hidden;
   }
   .headarea button.accordion {
   background: none;
   border: none;
   color: #616161;
   margin: 0px;
   padding: 0px;
   font-weight: 500;
   text-transform: capitalize;
   }
   .moreartc {
   text-transform: capitalize;
   font-weight: 600;
   cursor: pointer;
   float: left;
   position: absolute;
   right: 0;
   bottom: -5px;
   background: #fff;
   padding: 0 51% 0 10px;
   }
   .para {
   float: left;
   width: 100%;
   margin-bottom: 20px;
   position: relative;
   }
   .para p {
   overflow: hidden;
   text-overflow: ellipsis;
   display: -webkit-box;
   max-height: 80px;
   -webkit-line-clamp: 3;
   -webkit-box-orient: vertical;
   margin: 0;
   }
   .postpanels ul li:first-child {
   border-bottom: 1px solid #ddd;
   }
   .reportingsets ul li {
   float: left;
   width: 100%;
   text-align: left;
   }
   .reportingsets ul {
   list-style-type: circle;
   }
   .reportingsets ul li span {
   float: left;
   }
   .reportingsets {
   width: 100%;
   border-bottom: 1px solid #ddd;
   padding: 20px 0;
   margin: 0 0 15px 0;
   }
   .reportingsets .modal-footer{
   border-top:none;
   }
   .bottom-btns {
   padding: 0 21px;
   text-align: right;
   }
   .repolinks label {
   color: #797977;
   cursor: pointer;
   }
   .repolinks label:hover {
   color: #de7514;
   }
   .reportingsets input[type="radio"] {
   margin: 0 8px 0 0;
   }
   .pluging i.fa.fa-share-alt {
   float: right;
   }
   .like button.joinhouse {
   padding: 0px 15px;
   }
   .direct p a {
   color: #9a9a9a!important;
   .questio p.rigwd 
   {
   color:#9a9a9a; 
   }

@media only screen and (max-width: 767px)
   {
   .join .like p {
   margin: 0 4px 0 0;
   } 
   .groupname p
   {
   margin:0; 
   }
  .dropfilter {
    margin-bottom: 0px;
    display: none;
}
 .discussionback .headarea {
    background: #fff;
    padding: 10px;
    display: none;
}
   .text1 p {
   font-weight: 800;
   color: #f68e2f;
   line-height: 30px;
   font-size: 20px!important;
   }
   .memberpage {
   margin-top: 80px;
   }
   .like {
   float: left;
   width: 100%;
   display: flex;
   color:9a9a9a;
   text-align:center;
   }
   .like span {
   font-size: 13px;
   }
   .post strong {
   color: #373779;
   font-size: 10px;
   }
   .pluging {
   float: right;
   width: 100%;
   margin-right: 143px;
   color:9a9a9a;
   }
   .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
   padding: 30px;
   }
   .share, .share2 {
   float: left;
   width: 100%;
   text-align: center;
   }
   .text1 p a {
   font-weight: 800;
   color: #4c4242;
   line-height: 25px;
   font-size: 18px!important;
   }
   .plugin {
   float: right;
   margin-top: -44px;
   }
   .post .dotsseprate {
   margin: 0 2px;
   }
.headarea.mobile {
    display: none;
}
   }
   .like .joinhouse{padding: 8px 4px;}
   .like .disjoinhouse{padding: 8px 4px;}
   .headdiscus{margin-bottom: -1px!important;}
   .catp1 strong a{background: none;padding:0px;}
</style>
<div class="memberpage">
   <div class="row">
      <div class="col-md-9">
         <div class="col-md-4">
            <div class="headarea mobile">
               <div class="other">
                  <h4>Discussion Group</h4>
               </div>
               <div class="headarea">
                  <div class="addposituon bottomspc">
                     <p class="tablinks"><a href="<?php echo site_url();?>/peoples-choice-opininon-poll/">Opinion Poll</a><span class="countno"></span></p>
                     <?php
                        $getSidebarCats = $wpdb->get_results("SELECT * FROM wp_discussion_main");
                        
                        foreach($getSidebarCats as $getSidebarCat) {
                           if($getSidebarCat->have_sub == 0) {
                        ?>
                     <p class=""><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>"><?php echo $getSidebarCat->name;?> <span class="countno"></span></a></p>
                     <?php
                        } else {
                        ?>
                     <div class="spacetp">
                        <button class="accordion"><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>"><?php echo $getSidebarCat->name;?></a></button>
                        <div class="panel">
                           <?php
                              $getSubCats = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_discussion_sub WHERE main_id = %d",$getSidebarCat->id) 
                              );
                              foreach($getSubCats as $getSubCat) {
                              ?>
                           <p class="tablinks"><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>&actions=<?php echo $getSubCat->id; ?>"><?php echo $getSubCat->sub_name;?><span class="countno"></span></a></p>
                           <?php
                              }
                              ?>
                        </div>
                     </div>
                     <?php
                        }
                        }
                        ?>
                     <?php
                        if(isset($_SESSION["login"]["id"]) || $current_user->ID == 1) {
                        ?>
                           <p class="tablinks"><a href="<?php echo site_url();?>/peoples-wallposts"> People's Wallposts </a><span class="countno"></span></p>
                           <p class="tablinks"><a href="<?php echo site_url();?>/my-discussion-house">My Discussion Houses</a><span class="countno"></span></p>
                           <p class="tablinks"><a href="<?php echo site_url();?>/my-discussion-group">My Groups</a><span class="countno"></span></p>
                     <?php
                        }
                        ?>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-8">
            <?php
               $idHouse = (int) $_GET["action"];
               $house = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE id = %d", $idHouse));
               
               $quesPostCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as quesCount FROM wp_discussion_ques WHERE house_id=%d AND status=1 ORDER BY id DESC", $idHouse));
               
               $jCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as jCount FROM wp_discussion_join WHERE joinId=%d", $idHouse));
               
               $ansTime = $wpdb->get_results($wpdb->prepare("SELECT created_at FROM wp_discussion_ques WHERE house_id = %d ORDER BY id DESC LIMIT 1", $idHouse));
                 
                 if($house[0]->subcat_id == 0) {
                     $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $house[0]->cat_id));
                     $cName1 = $catName[0]->name;
                 } else{
                     $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $house[0]->cat_id));
               
                     $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $house[0]->subcat_id));
                     $cName1 = $catName[0]->sub_name;
                 }
               
                   $timeAnsGet = time_elapsed_string($house[0]->updated_at);
               
                 $getViews = $wpdb->get_results( 
                     $wpdb->prepare( "SELECT * FROM $table_name WHERE house_id = %d", $bid) 
                 );
               ?>
            <div class="rap firstpostdefults">
               <div class="direct">
                  <div class="col-md-12 col-sm-12 groupname">
                     <p> 
                        <?php if($house[0]->subcat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$house[0]->cat_id."'>".$catMainName[0]->name .'</a> . ';}?> 

                        <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $house[0]->cat_id;?><?php if($house[0]->subcat_id == 0){}else { echo "&actions=". $house[0]->subcat_id;}?>"> <?php if($house[0]->subcat_id != 0){if(strlen($cName1) > 10){echo substr($cName1,0,10);}else{echo $cName1;}} else{ echo $cName1;} ?> </a> </p>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="text1">
                     <p><?php echo $house[0]->title; ?></p>
                  </div>
                  <div class="para">
                     <span><?php if(strlen($house[0]->house_desc) > 180){echo substr($house[0]->house_desc, 0,180). '';} else{echo $house[0]->house_desc;} ?></span>
                     <span class="anscomplete<?php echo $z;?>" style="display:none;"><?php echo substr($house[0]->house_desc, 180,12000); ?></span>
                     <?php
                        if(strlen($house[0]->house_desc) > 180) {
                        ?>
                     <span class="ansmore<?php echo $z;?>" id="<?php echo $z;?>" onClick="moreFunction(this.id)" style="color:#f68e2f;">... (more)</span> 
                     <?php
                        }
                        ?>
                     </p>
                  </div>
               </div>
               <div class="join">
                  
                  <div class="col-md-12">
                     <div class="pluging">
                        <div class="dropdown customs">      
                          <button class="dropbtn">
                            <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                          </button>
                          <div class="dropdown-content">
                            <?php //$urlshare = "http://edukeeda.com/discussion-house/?action=".$house[0]->id;
                              //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                            ?>
                              <?php //echo do_shortcode("$urlsharing"); ?>
                          </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="like">
                        <?php
                           if(isset($_SESSION["login"])) {
                             $userId = $_SESSION["login"]["id"];
                             $checkJoin = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_discussion_join WHERE user_id=%d AND joinid=%d", $userId, $idHouse));
                           
                             if($checkJoin) {
                           ?>
                        <form method="post" action="">
                           <input type="hidden" name="hid" value="<?php echo $_GET["action"]; ?>">
                           <button types="button" name="unjoinhousesubmit" class="disjoinhouse" onclick="return confirm('Do you want to leave this house?')">Joined</button>
                        </form>
                        <?php
                           } else {
                           ?>
                        <form method="post" action="">
                           <input type="hidden" name="hid" value="<?php echo $_GET["action"]; ?>">
                           <button types="submit" name="joinhousesubmit" class="joinhouse">Join House</button>
                        </form>
                        <?php
                           }
                           } else {
                           ?>
                        <button types="button" class="joinhouse" data-toggle="modal" data-target="#myModaljoin">Join House</button>
                        <?php
                           }
                           ?>
                     </div>
                  </div>
                  <div class="col-md-9 col-xs-12">
                   <div class="like" style="margin-top: 10px;">
                     <p><span> <?php echo $quesPostCount[0]->quesCount; ?> </span>&nbsp;&nbsp;<strong> Posts </strong>
                     <div class="dotsseprate">.</div>
                     </p>
                     <p data-toggle="modal" data-target="#myModalrepostlist"><span> <?php echo $jCount[0]->jCount;?> </span>&nbsp;&nbsp;<strong> People </strong>
                     <div class="dotsseprate">.</div>
                     </p>
                     <p><span> <?php if($getViews){ echo $getViews[0]->view_count; }else{ echo "0";} ?> </span>&nbsp;&nbsp;<strong> Views </strong>
                     <div class="dotsseprate">.</div>
                     </p>
                     <div class="hjh">
                        <p><span><?php echo $timeAnsGet;?></span></p>
                     </div>

                          <!---- Modal  --->
                          <div id="myModalrepostlist" class="modal fade" role="dialog">
                          <div class="modal-dialog" style="width: 700px;">
                             <!-- Modal content-->
                             <div class="modal-content">
                                <div class="modal-header">
                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                   <h3></h3>
                                </div>
                                <div class="modal-body">
                                   <div class="row">
                                      <div clss="col-md-12">
                                      <?php
                                          $joinHouses = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_join WHERE joinId=%d", $idHouse));
                                          
                                          if($joinHouses) {
                                              
                                              foreach($joinHouses as $joinHouse) {
                                              
                                                $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $joinHouse->user_id));
                                          
                                                $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $joinHouse->user_id));
                                          
                                                $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $joinHouse->user_id));

                                      ?>   
                                         <div class="col-md-6">
                                          <div class="poll">
                                             <div class="col-md-3 col-sm-2 col-xs-3">
                                                <div class="imgforr">
                                                   <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                </div>
                                             </div>
                                             <div class="col-md-9 col-sm-10 col-xs-9">
                                                <p class="catp1"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                <p class="rigwd"> <?php echo $userDate;?> </p>
                                             </div>
                                          </div>
                                       </div>
                                      <?php
                                              }
                                          }
                                      ?>
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                          </div>

                  </div>
                  </div>
               </div>
            </div>
            <div class="arteduro">
               <div class="row">
                  <div class="col-md-12 col-sm-12 basictabsed">
                     
                     <a href="#" style="color:#9a9a9a">
                        <?php
                           if(isset($_SESSION["login"])) {
                             $userId = $_SESSION["login"]["id"];
                             $checkJoin2 = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_discussion_join WHERE user_id=%d", $userId));
                           
                             if($checkJoin2) {
                        ?>
                                <div class="memb1">
                                   <p data-toggle="modal" data-target="#myModalques2">Want to post any Question ?</p>
                                </div>
                        <?php
                           } else {
                        ?>
                              <div class="memb1">
                                 <p data-toggle="modal" data-target="#myModalques1">Want to post any Question ?</p>
                              </div>
                        <?php
                           }
                          } else {
                        ?>
                              <div class="memb1">
                                 <p data-toggle="modal" data-target="#myModalques">Want to post any Question ?</p>
                              </div>
                        <?php
                           }
                        ?>
                     </a>

                     <div class="dropfilter mobile">
                        <select name="filterdrop" id="filterdrops" onchange="changeFunction()" style="float: right;">
                           <option value="Recent"> Recent </option>
                           <option value="alltop"> All Time Top </option>
                           <option value="monthtop"> One Month Top </option>
                        </select>
                     </div>
                     

                     <div id="articleblous" class="tabcontent recent1" style="display: block;">
                        <div class="blogaresnews">
                           <?php
                              if($_GET["action"]) {
                                  $houseId = $_GET["action"];
                              }
                              
                              $quesPosts = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ques WHERE house_id=%d AND status=1 ORDER BY id DESC", $houseId));
                              $z=1;
                              foreach($quesPosts as $quesPost) {
                                
                                if($quesPost->sub_cat_id == 0) {

                                       $catName = $wpdb->get_results( 
                                         $wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost->main_id));
                                       $nameCat = $catName[0]->name;
                                   
                                   } else {
                                        $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost->main_id));

                                        $catName = $wpdb->get_results( 
                                         $wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $quesPost->sub_cat_id));
                                        $nameCat = $catName[0]->sub_name;
                                   }
                                   $houseName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d AND subcat_id=%d", $quesPost->main_id, $quesPost->sub_cat_id));

                                   $viewsCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT * FROM wp_discussion_views WHERE ques_id = %d", $quesPost->id));
                                 
                                   $ansCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT COUNT(id) as countAns FROM wp_discussion_ans WHERE ques_id = %d", $quesPost->id));

                                   $ansTime = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT created_at FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $quesPost->id));
                                   
                                   $quesFollow = $wpdb->get_results( 
                                   $wpdb->prepare("SELECT COUNT(id) as countFollowed FROM wp_discussion_ques_follow WHERE ques_id = %d", $quesPost->id));

                                   $repostCount = $wpdb->get_results(
                                   $wpdb->prepare("SELECT COUNT(id) as countRepost FROM wp_wallpost WHERE shareType='ques' AND shareid = %d", $quesPost->id));
                                   
                                   if($ansTime) {
                                     $timeAnsGet = "<strong>Answered </strong> " . time_elapsed_string($ansTime[0]->created_at);
                                   } else {
                                     $timeAnsGet = "<strong>Posted </strong> " . time_elapsed_string($quesPost->created_at);
                                   }
                              
                              ?>  
                           <div class="col-md-12 blo postpanels">
                                   <div class="direct">
                                      <div class="col-md-10 col-sm-10 col-xs-10">
                                        <p> <?php if($quesPost->sub_cat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$quesPost->main_id."'>".$catMainName[0]->name .'</a> . ';}?>

                                          <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $quesPost->main_id;?><?php if($quesPost->sub_cat_id == 0){}else { echo "&actions=". $quesPost->sub_cat_id;}?>"><?php echo $nameCat; ?></a> . 
                                          <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $quesPost->house_id;?>"><?php if(strlen($houseName[0]->title) > 40){echo substr($houseName[0]->title, 0,40). ' ...';} else{echo $houseName[0]->title;} ?></a> </p>
                                      </div>
                                   </div>
                                   <div class="col-md-2 col-sm-2 col-xs-2">
                                      <div class="dot">
                                         <div class="dropdown">
                                            <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                            </p>
                                            <ul class="dropdown-menu">
                                            <?php
                                                if($_SESSION['login']['id']) {
                                            ?>
                                               <li data-toggle="modal" data-target="#myModalreporta<?php echo $z; ?>">Report This Post</li>
                                            <?php
                                                } else{
                                            ?>
                                                <li data-toggle="modal" data-target="#myModalreportnota<?php echo $z; ?>">Report This Post</li>
                                            <?php
                                                }
                                            ?>
                                            </ul>
                                         </div>
                                             <div id="myModalreportnota<?php echo $z; ?>" class="modal fade" role="dialog">
                                                  <div class="modal-dialog">
                                                     <!-- Modal content-->
                                                     <div class="modal-content">
                                                        <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                           <h1>Report this post</h1>
                                                        </div>
                                                        <div class="modal-body">
                                                           <p> Please Login and Register first! </p>
                                                        </div>
                                                     </div>
                                                  </div>
                                              </div>

                                              <div class="modal fade" id="myModalreporta<?php echo $z; ?>" role="dialog">
                                                 <div class="modal-dialog vertical-align-center">
                                                    <div class="modal-content">
                                                       <div class="repocontent modal-body reportingsets" id="gridonea<?php echo $z; ?>" style="display:block;">
                                                          <ul>
                                                             <li>
                                                                <span  class="countno repolinks" onclick="openReport(event, 'gridtwoa<?php echo $z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it's spam, Promotional</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridthreea<?php echo $z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think  it's objectionable</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridfoura<?php echo $z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it false or misinformation</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                          </ul>
                                                          <div class="modal-footer">
                                                          </div>
                                                       </div>

                                                       <div class="repocontent modal-body reportingsets" id="gridtwoa<?php echo $z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                             <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                          </div>
                                                          <div class="modal-footer">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridonea<?php echo $z; ?>')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       
                                                       <div class="repocontent modal-body reportingsets" id="gridthreea<?php echo $z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent" checked="checked">It's pornographic or extremely violent.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                             </div>
                                                          </div>
                                                          <div class="bottom-btns">
                                                            <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridonea<?php echo $z; ?>')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       <div class="repocontent modal-body reportingsets" id="gridfoura<?php echo $z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                          </div>
                                                          <div class="bottom-btns">
                                                             <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridonea<?php echo $z; ?>')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                      </div>
                                   </div>
                                   <div class="col-md-12">
                                      <div class="text1">
                                         <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $quesPost->id;?>"><?php echo $quesPost->ques; ?></a></p>
                                      </div>
                                   </div>
                                   <div class="questio">
                                      <div class="col-md-12 col-sm-12">
                                         <p class="rigwd"> <?php echo $timeAnsGet; ?></p>
                                      </div>
                                   </div>
                                   <div class="col-xs-9 col-md-9">
                                      <div class="post">
                                         <p><span><?php if($ansCount){echo $ansCount[0]->countAns;}else {echo "0";} ?></span><strong>Answers</strong>
                                         <div class="dotsseprate">.</div>
                                         </p>
                                         <!--<p data-toggle="modal" data-target="#myModalrepostlista<?php //echo $z;?>"><span> <?php //if($repostCount){echo $repostCount[0]->countRepost; }else{echo "0";} ?> </span><strong>Repost</strong>
                                         <div class="dotsseprate">.</div>
                                         </p>-->
                                         <p><span><?php if($viewsCount){echo $viewsCount[0]->view_count;}else{echo "0";} ?></span><strong>Views</strong>
                                         <div class="dotsseprate">.</div>
                                         </p>
                                         <p data-toggle="modal" data-target="#myModalfollowlista<?php echo $z;?>"><span><?php if($quesFollow){ echo $quesFollow[0]->countFollowed; }else{ echo "0";} ?></span><strong>Followers</strong></p>
                                      </div>

                                            <!---- Modal  --->
                                              <div id="myModalrepostlista<?php echo $z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                          <?php
                                                              $reLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ques' AND shareid=%d", $quesPost->id));
                                                              if($reLists) {
                                                                  foreach($reLists as $reList) {
                                                                      $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $reList->user_id));
                                                              
                                                                      $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $reList->user_id));
                                                              
                                                                      $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $reList->user_id));

                                                          ?>   
                                                             <div class="col-md-6">
                                                              <div class="poll">
                                                                 <div class="col-md-3 col-sm-2 col-xs-3">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="col-md-9 col-sm-10 col-xs-9">
                                                                    <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                    <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                          <?php
                                                                  }
                                                              }
                                                          ?>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                                            <!---- Modal  --->
                                              <div id="myModalfollowlista<?php echo $z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                          <?php
                                                              $foLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ques_follow WHERE ques_id=%d", $quesPost->id));
                                                              if($foLists) {
                                                                  foreach($foLists as $foList) {
                                                                    $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $foList->user_id));
                                                              
                                                                    $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $foList->user_id));
                                                              
                                                                    $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $foList->user_id));

                                                          ?>   
                                                             <div class="col-md-6">
                                                              <div class="poll">
                                                                 <div class="col-md-3 col-sm-2 col-xs-3">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="col-md-9 col-sm-10 col-xs-9">
                                                                    <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                    <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                          <?php
                                                                  }
                                                              }
                                                          ?>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>


                                   </div>
                                   <div class="col-xs-3 col-md-3">
                                      <div class="plugin">
                                         <div class="dropdown customs">      
                                              <button class="dropbtn">
                                                <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                                              </button>
                                              <div class="dropdown-content">
                                                <?php //$urlshare = "http://edukeeda.com/questionanswers?action=".$quesPost->id;
                                                  //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                                ?>
                                                  <?php //echo do_shortcode("$urlsharing"); ?>
                                              </div>
                                            </div>
                                      </div>
                                   </div>
                                   <div class="col-xs-4 col-md-4">
                                      <div class="share">
                                         <?php
                                            if(isset($_SESSION["login"])) {
                                          ?>
                                            <p data-toggle="modal" data-target="#myModalanszz<?php echo $z.$z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>  
                                          <?php
                                            } else {
                                          ?>
                                              <p data-toggle="modal" data-target="#myModalanszz<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                          <?php
                                            }
                                          ?>
                                      </div>

                                          <!-- Modal -->
                                          <div id="myModalanszz<?php echo $z;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                  <p> For post an answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Modal -->
                                          <div id="myModalanszz<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                  <form method="post" action="">
                                                    <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                    <?php
                                                        $my_content1 =" ";
                                                        $editor_id1 = 'ansPosta'.$z;
                                                        $option_name1 ='ansPost';
                                                        wp_editor($my_content1, $editor_id1, array('wpautop'=> true,'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                                                     ?>
                                                    <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                  </form>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                   </div>
                                   <div class="col-xs-4 col-md-4">
                                      <div class="share1">
                                         <?php
                                            if(isset($_SESSION["login"])) {
                                              $userId = $_SESSION["login"]["id"];
                                              $checkFollow = $wpdb->get_results( 
                                                $wpdb->prepare( "SELECT * FROM wp_discussion_ques_follow WHERE user_id=%d AND ques_id=%d", $userId, $quesPost->id));
                                              if($checkFollow){
                                         ?>
                                                <form method="post" action="">
                                                  <input type="hidden" name="quesId" value="<?php echo $quesPost->id;?>">
                                                  <img src="/img/follow-icon-selected.png"><button type="submit" name="unfollow"> Follow </button>
                                               </form>
                                         <?php
                                              } else{
                                         ?>
                                               <form method="post" action="">
                                                  <input type="hidden" name="quesId" value="<?php echo $quesPost->id;?>">
                                                  <img src="/img/Follow.png"><button type="submit" name="follow"> Follow </button>
                                               </form>
                                         <?php 
                                              }
                                            } else {
                                          ?>
                                                <img src="/img/Follow.png"><button types="button" data-toggle="modal" data-target="#myModalques">Follow</button>
                                         <?php   
                                            }
                                         ?>
                                      </div>
                                   </div>
                                   <div class="col-xs-4 col-md-4">
                                      <div class="share2">
                                      <?php
                                          if(isset($_SESSION["login"])) {
                                            $userId = $_SESSION["login"]["id"];
                                      ?>
                                            <!--<p data-toggle="modal" data-target="#myModalrepostee<?php //echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                      <?php
                                          } else{
                                      ?>
                                            <!--<p data-toggle="modal" data-target="#myModalrepostee<?php //echo $z.$z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                      <?php
                                          }
                                      ?>
                                      </div>
                                            <!-- Modal -->
                                            <div id="myModalrepostee<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                               <div class="modal-dialog">
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                     <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"> Repost </h4>
                                                     </div>
                                                     <div class="modal-body">
                                                        <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>

                                            <!-- Modal -->
                                            <div id="myModalrepostee<?php echo $z;?>" class="modal fade" role="dialog">
                                               <div class="modal-dialog">
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                     <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"> Repost </h4>
                                                     </div>
                                                     <div class="modal-body">
                                                      <form method="post" action="">
                                                        <p> <textarea name="reposttext"> </textarea></p>
                                                        <?php
                                                                  $userPic = $wpdb->get_results( 
                                                                    $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$quesPost->user_id));
                                                                  $userName = $wpdb->get_results( 
                                                                    $wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$quesPost->user_id));
                                                                  $userHeadline = $wpdb->get_results( 
                                                                   $wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$quesPost->user_id));
                                                                ?>
                                                                <div class="col-md-2 col-sm-2 col-xs-4">
                                                                        <div class="imgforr">
                                                                           <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                        </div>
                                                                     </div>
                                                                <div class="col-md-10 col-sm-10 col-xs-8">
                                                                  <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $quesPost->user_id;?>"><?php echo $userName[0]->name;?></a></strong>,<span><?php echo $userHeadline[0]->headline;?></span></p>
                                                                  <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($lastAns[0]->created_at )); ?> </p>
                                                               </div>
                                                        <div class="repostmodal">
                                                            <p> Repost this question </p>
                                                            <p> Question :  <?php echo $quesPost->ques; ?> </p>
                                                        </div>
                                                        <input type="hidden" name="type" value="ques">
                                                        <input type="hidden" name="quesid" value="<?php echo $quesPost->id;?>">
                                                        <input type="hidden" name="ques" value="<?php echo $quesPost->ques;?>">
                                                        <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                      </form>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>

                                   </div>
                                </div>
                           <?php
                              $z++;
                              }
                           ?>   
                        </div>
                     </div>

                     <div id="articleblous" class="tabcontent alltime" style="display: none;">
                        <div class="blogaresnews">
                           <?php
                              if($_GET["action"]) {
                                  $houseId = $_GET["action"];
                              }
                              
                              // $quesPosts = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ques WHERE house_id=%d AND status=1 ORDER BY id DESC", $houseId));

                              $quesPosts = $wpdb->get_results($wpdb->prepare("SELECT wp_discussion_ques.id, wp_discussion_ques.main_id, wp_discussion_ques.sub_cat_id, wp_discussion_ques.house_id, wp_discussion_ques.user_id, wp_discussion_ques.ques, wp_discussion_ques.status, wp_discussion_ques.created_at FROM wp_discussion_ques INNER JOIN wp_discussion_views ON wp_discussion_views.ques_id = wp_discussion_ques.id WHERE wp_discussion_ques.house_id=%d  AND wp_discussion_ques.status=1 ORDER BY wp_discussion_views.view_count DESC LIMIT 70", $houseId));

                              $z=11;
                              foreach($quesPosts as $quesPost) {
                                
                                if($quesPost->sub_cat_id == 0) {
                                       $catName = $wpdb->get_results( 
                                         $wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost->main_id));
                                       $nameCat = $catName[0]->name;
                                   
                                   } else {
                                        $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost->main_id));

                                        $catName = $wpdb->get_results( 
                                         $wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $quesPost->sub_cat_id));
                                       $nameCat = $catName[0]->sub_name;
                                   }

                                   $houseName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d AND subcat_id=%d", $quesPost->main_id, $quesPost->sub_cat_id));

                                   $viewsCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT * FROM wp_discussion_views WHERE ques_id = %d", $quesPost->id));
                                 
                                   $ansCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT COUNT(id) as countAns FROM wp_discussion_ans WHERE ques_id = %d", $quesPost->id));

                                   $repostCount = $wpdb->get_results(
                                   $wpdb->prepare("SELECT COUNT(id) as countRepost FROM wp_wallpost WHERE shareType='ques' AND shareid = %d", $quesPost->id));

                                   $ansTime = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT created_at FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $quesPost->id));
                                   
                                   $quesFollow = $wpdb->get_results( 
                                   $wpdb->prepare("SELECT COUNT(id) as countFollowed FROM wp_discussion_ques_follow WHERE ques_id = %d", $quesPost->id));
                                   
                                   if($ansTime) {
                                     $timeAnsGet = "<strong>Answered </strong> " . time_elapsed_string($ansTime[0]->created_at);
                                   } else {
                                     $timeAnsGet = "<strong>Posted </strong> " . time_elapsed_string($quesPost->created_at);
                                   }
                              
                              ?>  
                           <div class="col-md-12 blo postpanels">
                                   <div class="direct">
                                      <div class="col-md-10 col-sm-10 col-xs-10">
                                        <p> <?php if($quesPost->sub_cat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$quesPost->main_id."'>".$catMainName[0]->name .'</a> . ';}?>
                                              <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $quesPost->main_id;?><?php if($quesPost->sub_cat_id == 0){}else { echo "&actions=". $quesPost->sub_cat_id;}?>"><?php echo $nameCat; ?></a> . 
                                              <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $quesPost->house_id;?>"><?php if(strlen($houseName[0]->title) > 40){echo substr($houseName[0]->title, 0,40). ' ...';} else{echo $houseName[0]->title;} ?></a> </p>
                                      </div>
                                   </div>
                                   <div class="col-md-2 col-sm-2 col-xs-2">
                                      <div class="dot">
                                         <div class="dropdown">
                                            <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                            </p>
                                            <ul class="dropdown-menu">
                                            <?php
                                                if($_SESSION['login']['id']) {
                                            ?>
                                               <li data-toggle="modal" data-target="#myModalreportb<?php echo $z; ?>">Report This Post</li>
                                            <?php
                                                 } else{
                                              ?>
                                                    <li data-toggle="modal" data-target="#myModalreportnotb<?php echo $z; ?>">Report This Post</li>
                                              <?php
                                                 }
                                              ?>
                                            </ul>
                                         </div>
                                             
                                             <div id="myModalreportnotb<?php echo $z; ?>" class="modal fade" role="dialog">
                                                  <div class="modal-dialog">
                                                     <!-- Modal content-->
                                                     <div class="modal-content">
                                                        <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                           <h1>Report this post</h1>
                                                        </div>
                                                        <div class="modal-body">
                                                           <p> Please Login and Register first! </p>
                                                        </div>
                                                     </div>
                                                  </div>
                                              </div>

                                              <div class="modal fade" id="myModalreportb<?php echo $z; ?>" role="dialog">
                                                 <div class="modal-dialog vertical-align-center">
                                                    <div class="modal-content">
                                                       <div class="repocontent modal-body reportingsets" id="gridoneb<?php echo $z; ?>" style="display:block;">
                                                          <ul>
                                                             <li>
                                                                <span  class="countno repolinks" onclick="openReport(event, 'gridtwob<?php echo $z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it's spam, Promotional</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridthreeb<?php echo $z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think  it's objectionable</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridfourb<?php echo $z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it false or misinformation</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                          </ul>
                                                          <div class="modal-footer">
                                                          </div>
                                                       </div>

                                                       <div class="repocontent modal-body reportingsets" id="gridtwob<?php echo $z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                             <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                          </div>
                                                          <div class="modal-footer">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridoneb<?php echo $z; ?>')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       
                                                       <div class="repocontent modal-body reportingsets" id="gridthreeb<?php echo $z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent" checked="checked">It's pornographic or extremely violent.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                             </div>
                                                          </div>
                                                          <div class="bottom-btns">
                                                            <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridoneb<?php echo $z; ?>')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       <div class="repocontent modal-body reportingsets" id="gridfourb<?php echo $z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                          </div>
                                                          <div class="bottom-btns">
                                                             <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridoneb<?php echo $z; ?>')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                      </div>
                                   </div>
                                   <div class="col-md-12">
                                      <div class="text1">

                                         <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $quesPost->id;?>"><?php echo $quesPost->ques; ?></a></p>
                                      </div>
                                   </div>
                                   <div class="questio">
                                      <div class="col-md-12 col-sm-12">
                                         <p class="rigwd"> <?php echo $timeAnsGet; ?></p>
                                      </div>
                                   </div>
                                   <div class="col-xs-9 col-md-9">
                                      <div class="post">
                                         <p><span><?php if($ansCount){echo $ansCount[0]->countAns;}else {echo "0";} ?></span><strong>Answers</strong>
                                         <div class="dotsseprate">.</div>
                                         </p>
                                         <!--<p data-toggle="modal" data-target="#myModalrepostlistc<?php //echo $z;?>"><span><?php //if($repostCount){echo $repostCount[0]->countRepost; }else{echo "0";} ?> </span><strong>Repost</strong>
                                         <div class="dotsseprate">.</div>
                                         </p>-->

                                         <p><span><?php if($viewsCount[0]->view_count){echo $viewsCount[0]->view_count;}else{echo "0";} ?></span><strong>Views</strong>
                                         <div class="dotsseprate">.</div>
                                         </p>

                                         <p data-toggle="modal" data-target="#myModalfollowlistc<?php echo $z;?>"><span><?php if($quesFollow){ echo $quesFollow[0]->countFollowed; }else{ echo "0";} ?></span><strong>Followers</strong></p>
                                      </div>

                                            <!---- Modal  --->
                                              <div id="myModalrepostlistc<?php echo $z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                          <?php
                                                              $reLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ques' AND shareid=%d", $quesPost->id));
                                                              if($reLists) {
                                                                  foreach($reLists as $reList) {
                                                                      $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $reList->user_id));
                                                              
                                                                      $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $reList->user_id));
                                                              
                                                                      $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $reList->user_id));

                                                          ?>   
                                                             <div class="col-md-6">
                                                              <div class="poll">
                                                                 <div class="col-md-3 col-sm-2 col-xs-3">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="col-md-9 col-sm-10 col-xs-9">
                                                                    <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                    <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                          <?php
                                                                  }
                                                              }
                                                          ?>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                                            <!---- Modal  --->
                                              <div id="myModalfollowlistc<?php echo $z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                          <?php
                                                              $foLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ques_follow WHERE ques_id=%d", $quesPost->id));
                                                              if($foLists) {
                                                                  foreach($foLists as $foList) {
                                                                    $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $foList->user_id));
                                                              
                                                                    $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $foList->user_id));
                                                              
                                                                    $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $foList->user_id));

                                                          ?>   
                                                             <div class="col-md-6">
                                                              <div class="poll">
                                                                 <div class="col-md-3 col-sm-2 col-xs-3">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="col-md-9 col-sm-10 col-xs-9">
                                                                    <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                    <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                          <?php
                                                                  }
                                                              }
                                                          ?>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                                   </div>
                                   <div class="col-xs-3 col-md-3">
                                      <div class="plugin">
                                         <div class="dropdown customs">      
                                              <button class="dropbtn">
                                                <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                                              </button>
                                              <div class="dropdown-content">
                                                <?php //$urlshare = "http://edukeeda.com/questionanswers?action=".$quesPost->id;
                                                  //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                                ?>
                                                  <?php //echo do_shortcode("$urlsharing"); ?>
                                              </div>
                                            </div>
                                      </div>
                                   </div>
                                   <div class="col-xs-4 col-md-4">
                                      <div class="share">
                                         <?php
                                            if(isset($_SESSION["login"])) {
                                          ?>
                                            <p data-toggle="modal" data-target="#myModalansss<?php echo $z.$z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>  
                                          <?php
                                            } else {
                                          ?>
                                              <p data-toggle="modal" data-target="#myModalansss<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                          <?php
                                            }
                                          ?>
                                      </div>

                                          <!-- Modal -->
                                          <div id="myModalansss<?php echo $z;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                  <p> For post an answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Modal -->
                                          <div id="myModalansss<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                  <form method="post" action="">
                                                    <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                    
                                                    <?php
                                                        $my_content1 =" ";
                                                        $editor_id1 = 'ansPostc'.$z;
                                                        $option_name1 ='ansPost';
                                                        wp_editor($my_content1, $editor_id1, array('wpautop'=> true,'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                                                     ?>
                                                    <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                  </form>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                   </div>
                                   <div class="col-xs-4 col-md-4">
                                      <div class="share1">
                                         <?php
                                            if(isset($_SESSION["login"])) {
                                              $userId = $_SESSION["login"]["id"];
                                              
                                              $checkFollow = $wpdb->get_results( 
                                                $wpdb->prepare( "SELECT * FROM wp_discussion_ques_follow WHERE user_id=%d AND ques_id=%d", $userId, $quesPost->id));
                                              if($checkFollow){
                                            ?>
                                                <form method="post" action="">
                                                  <input type="hidden" name="quesId" value="<?php echo $quesPost->id;?>">
                                                  <img src="/img/follow-icon-selected.png"><button type="submit" name="unfollow"> Follow </button>
                                               </form>
                                         <?php
                                            } else{
                                            ?>
                                               <form method="post" action="">
                                                  <input type="hidden" name="quesId" value="<?php echo $quesPost->id;?>">
                                                  <img src="/img/Follow.png"><button type="submit" name="follow"> Follow </button>
                                               </form>
                                         <?php 
                                            }
                                            } else {
                                            ?>
                                                <img src="/img/Follow.png"><button types="button" data-toggle="modal" data-target="#myModalques">Follow</button>
                                         <?php   
                                            }
                                            ?>
                                      </div>
                                   </div>
                                   <div class="col-xs-4 col-md-4">
                                      <div class="share2">
                                      <?php
                                          if(isset($_SESSION["login"])) {
                                            $userId = $_SESSION["login"]["id"];
                                      ?>
                                            <!--<p data-toggle="modal" data-target="#myModalreposttt<?php //echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                      <?php
                                          } else{
                                      ?>
                                            <!--<p data-toggle="modal" data-target="#myModalreposttt<?php //echo $z.$z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                      <?php
                                          }
                                      ?>
                                      </div>
                                            <!-- Modal -->
                                            <div id="myModalreposttt<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                               <div class="modal-dialog">
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                     <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"> Repost </h4>
                                                     </div>
                                                     <div class="modal-body">
                                                        <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>

                                            <!-- Modal -->
                                            <div id="myModalreposttt<?php echo $z;?>" class="modal fade" role="dialog">
                                               <div class="modal-dialog">
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                     <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"> Repost </h4>
                                                     </div>
                                                     <div class="modal-body">
                                                      <form method="post" action="">
                                                        <p> <textarea name="reposttext"> </textarea></p>
                                                        <?php
                                                                  $userPic = $wpdb->get_results( 
                                                                    $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$quesPost->user_id));
                                                                  $userName = $wpdb->get_results( 
                                                                    $wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$quesPost->user_id));
                                                                  $userHeadline = $wpdb->get_results( 
                                                                   $wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$quesPost->user_id));
                                                                ?>
                                                                <div class="col-md-2 col-sm-2 col-xs-4">
                                                                        <div class="imgforr">
                                                                           <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                        </div>
                                                                     </div>
                                                                <div class="col-md-10 col-sm-10 col-xs-8">
                                                                  <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $quesPost->user_id;?>"><?php echo $userName[0]->name;?></a></strong>,<span><?php echo $userHeadline[0]->headline;?></span></p>
                                                                  <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($lastAns[0]->created_at )); ?> </p>
                                                               </div>
                                                        <div class="col-md-12 repostmodal">
                                                            <p> Repost this question </p>
                                                            <p> Question :  <?php echo $quesPost->ques; ?> </p>
                                                        </div>
                                                        <input type="hidden" name="type" value="ques">
                                                        <input type="hidden" name="quesid" value="<?php echo $quesPost->id;?>">
                                                        <input type="hidden" name="ques" value="<?php echo $quesPost->ques;?>">
                                                        <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                      </form>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>

                                   </div>
                                </div>
                           <?php
                              $z++;
                              }
                           ?>   
                        </div>
                     </div>


                     <div id="articleblous" class="tabcontent monthtop" style="display: none;">
                        <div class="blogaresnews">
                           <?php
                              if($_GET["action"]) {
                                  $houseId = $_GET["action"];
                              }
                              $top = date("Y-m-d", strtotime("-1 month"));
                              
                              $quesPosts = $wpdb->get_results($wpdb->prepare("SELECT wp_discussion_ques.id, wp_discussion_ques.main_id, wp_discussion_ques.sub_cat_id, wp_discussion_ques.house_id, wp_discussion_ques.user_id, wp_discussion_ques.ques, wp_discussion_ques.status, wp_discussion_ques.created_at FROM wp_discussion_ques INNER JOIN wp_discussion_views ON wp_discussion_views.ques_id = wp_discussion_ques.id WHERE wp_discussion_ques.house_id=%d AND wp_discussion_ques.status=1 AND wp_discussion_ques.created_at >= %s ORDER BY wp_discussion_views.view_count DESC LIMIT 40",$houseId, $top));

                              $z=111;
                              foreach($quesPosts as $quesPost) {
                                
                                if($quesPost->sub_cat_id == 0) {
                                       $catName = $wpdb->get_results( 
                                         $wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost->main_id));
                                       $nameCat = $catName[0]->name;
                                   
                                   } else {
                                        $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost->main_id));

                                        $catName = $wpdb->get_results( 
                                         $wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $quesPost->sub_cat_id));
                                       $nameCat = $catName[0]->sub_name;
                                   }
                                   
                                   $houseName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d AND subcat_id=%d", $quesPost->main_id, $quesPost->sub_cat_id));

                                   $viewsCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT * FROM wp_discussion_views WHERE ques_id = %d", $quesPost->id));
                                 
                                   $ansCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT COUNT(id) as countAns FROM wp_discussion_ans WHERE ques_id = %d", $quesPost->id));

                                   $repostCount = $wpdb->get_results(
                                   $wpdb->prepare("SELECT COUNT(id) as countRepost FROM wp_wallpost WHERE shareType='ques' AND shareid = %d", $quesPost->id));

                                   $ansTime = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT created_at FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $quesPost->id));
                                   
                                   $quesFollow = $wpdb->get_results( 
                                   $wpdb->prepare("SELECT COUNT(id) as countFollowed FROM wp_discussion_ques_follow WHERE ques_id = %d", $quesPost->id));
                                   
                                   if($ansTime) {
                                     $timeAnsGet = "<strong>Answered </strong> " . time_elapsed_string($ansTime[0]->created_at);
                                   } else {
                                     $timeAnsGet = "<strong>Posted </strong> " . time_elapsed_string($quesPost->created_at);
                                   }
                              
                              ?>  
                           <div class="col-md-12 blo postpanels">
                                   <div class="direct">
                                      <div class="col-md-10 col-sm-10 col-xs-10">
                                        <p> <?php if($quesPost->sub_cat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$quesPost->main_id."'>".$catMainName[0]->name .'</a> . ';}?>
                                              <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $quesPost->main_id;?><?php if($quesPost->sub_cat_id == 0){}else { echo "&actions=". $quesPost->sub_cat_id;}?>"><?php echo $nameCat; ?></a> . 
                                              <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $quesPost->house_id;?>"><?php if(strlen($houseName[0]->title) > 40){echo substr($houseName[0]->title, 0,40). ' ...';} else{echo $houseName[0]->title;} ?></a> </p>
                                      </div>
                                   </div>
                                   <div class="col-md-2 col-sm-2 col-xs-2">
                                      <div class="dot">
                                         <div class="dropdown">
                                            <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                            </p>
                                            <ul class="dropdown-menu">
                                            <?php
                                                if($_SESSION['login']['id']) {
                                            ?>
                                                  <li data-toggle="modal" data-target="#myModalreportc<?php echo $z; ?>">Report This Post</li>
                                            <?php
                                               } else{
                                            ?>
                                                  <li data-toggle="modal" data-target="#myModalreportnotc<?php echo $z; ?>">Report This Post</li>
                                            <?php
                                               }
                                            ?>
                                            </ul>
                                         </div>
                                             
                                             <div id="myModalreportnotc<?php echo $z; ?>" class="modal fade" role="dialog">
                                                  <div class="modal-dialog">
                                                     <!-- Modal content-->
                                                     <div class="modal-content">
                                                        <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                           <h1>Report this post</h1>
                                                        </div>
                                                        <div class="modal-body">
                                                           <p> Please Login and Register first! </p>
                                                        </div>
                                                     </div>
                                                  </div>
                                              </div>

                                              <div class="modal fade" id="myModalreportc<?php echo $z; ?>" role="dialog">
                                                 <div class="modal-dialog vertical-align-center">
                                                    <div class="modal-content">
                                                       <div class="repocontent modal-body reportingsets" id="gridonec<?php echo $z; ?>" style="display:block;">
                                                          <ul>
                                                             <li>
                                                                <span  class="countno repolinks" onclick="openReport(event, 'gridtwoc<?php echo $z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it's spam, Promotional</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridthreec<?php echo $z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think  it's objectionable</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridfourc<?php echo $z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it false or misinformation</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                          </ul>
                                                          <div class="modal-footer">
                                                          </div>
                                                       </div>

                                                       <div class="repocontent modal-body reportingsets" id="gridtwoc<?php echo $z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                             <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                          </div>
                                                          <div class="modal-footer">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridonec<?php echo $z; ?>')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       
                                                       <div class="repocontent modal-body reportingsets" id="gridthreec<?php echo $z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent" checked="checked">It's pornographic or extremely violent.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                             </div>
                                                          </div>
                                                          <div class="bottom-btns">
                                                            <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridonec<?php echo $z; ?>')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       <div class="repocontent modal-body reportingsets" id="gridfourc<?php echo $z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                          </div>
                                                          <div class="bottom-btns">
                                                             <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridonec<?php echo $z; ?>')">Back</button>
                                                             <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                      </div>
                                   </div>
                                   <div class="col-md-12">
                                      <div class="text1">
                                         <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $quesPost->id;?>"><?php echo $quesPost->ques; ?></a></p>
                                      </div>
                                   </div>
                                   <div class="questio">
                                      <div class="col-md-12 col-sm-12">
                                         <p class="rigwd"> <?php echo $timeAnsGet; ?></p>
                                      </div>
                                   </div>
                                   <div class="col-xs-9 col-md-9">
                                      <div class="post">
                                         <p><span><?php if($ansCount){echo $ansCount[0]->countAns;}else {echo "0";} ?></span><strong>Answers</strong>
                                         <div class="dotsseprate">.</div>
                                         </p>
                                         
                                         <!--<p  data-toggle="modal" data-target="#myModalrepostlistb<?php //echo $z;?>"><span><?php //if($repostCount){echo $repostCount[0]->countRepost; }else{echo "0";} ?></span><strong>Repost</strong>
                                         <div class="dotsseprate">.</div>
                                         </p>-->
                                         
                                         <p><span><?php if($viewsCount[0]->view_count){echo $viewsCount[0]->view_count;}else{echo "0";} ?></span><strong>Views</strong>
                                         <div class="dotsseprate">.</div>
                                         </p>
                                         <p data-toggle="modal" data-target="#myModalfollowlistb<?php echo $z;?>"><span><?php if($quesFollow){ echo $quesFollow[0]->countFollowed; }else{ echo "0";} ?></span><strong>Followers</strong></p>
                                      </div>

                                            <!---- Modal  --->
                                              <div id="myModalrepostlistb<?php echo $z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                          <?php
                                                              $reLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ques' AND shareid=%d", $quesPost->id));
                                                              if($reLists) {
                                                                  foreach($reLists as $reList) {
                                                                      $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $reList->user_id));
                                                              
                                                                      $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $reList->user_id));
                                                              
                                                                      $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $reList->user_id));

                                                          ?>   
                                                             <div class="col-md-6">
                                                              <div class="poll">
                                                                 <div class="col-md-3 col-sm-2 col-xs-3">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="col-md-9 col-sm-10 col-xs-9">
                                                                    <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                    <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                          <?php
                                                                  }
                                                              }
                                                          ?>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                                            <!---- Modal  --->
                                              <div id="myModalfollowlistb<?php echo $z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                          <?php
                                                              $foLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ques_follow WHERE ques_id=%d", $quesPost->id));
                                                              if($foLists) {
                                                                  foreach($foLists as $foList) {
                                                                    $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $foList->user_id));
                                                              
                                                                    $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $foList->user_id));
                                                              
                                                                    $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $foList->user_id));

                                                          ?>   
                                                             <div class="col-md-6">
                                                              <div class="poll">
                                                                 <div class="col-md-3 col-sm-2 col-xs-3">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="col-md-9 col-sm-10 col-xs-9">
                                                                    <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                    <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                          <?php
                                                                  }
                                                              }
                                                          ?>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                                   </div>
                                   <div class="col-xs-3 col-md-3">
                                      <div class="plugin">
                                         <div class="dropdown customs">      
                                              <button class="dropbtn">
                                                <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                                              </button>
                                              <div class="dropdown-content">
                                                <?php //$urlshare = "http://edukeeda.com/questionanswers?action=".$quesPost->id;
                                                  //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                                ?>
                                                  <?php //echo do_shortcode("$urlsharing"); ?>
                                              </div>
                                            </div>
                                      </div>
                                   </div>
                                   <div class="col-xs-4 col-md-4">
                                      <div class="share">
                                         <?php
                                            if(isset($_SESSION["login"])) {
                                          ?>
                                            <p data-toggle="modal" data-target="#myModalansaa<?php echo $z.$z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>  
                                          <?php
                                            } else {
                                          ?>
                                              <p data-toggle="modal" data-target="#myModalansaa<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                          <?php
                                            }
                                          ?>
                                      </div>

                                          <!-- Modal -->
                                          <div id="myModalansaa<?php echo $z;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                  <p> For post an answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                          <!-- Modal -->
                                          <div id="myModalansaa<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                  <form method="post" action="">
                                                    <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                    <?php
                                                        $my_content1 =" ";
                                                        $editor_id1 = 'ansPostb'.$z;
                                                        $option_name1 ='ansPost';
                                                        wp_editor($my_content1, $editor_id1, array('wpautop'=> true,'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                                                     ?>
                                                    <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                  </form>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                   </div>
                                   <div class="col-xs-4 col-md-4">
                                      <div class="share1">
                                         <?php
                                            if(isset($_SESSION["login"])) {
                                              $userId = $_SESSION["login"]["id"];
                                              
                                              $checkFollow = $wpdb->get_results( 
                                                $wpdb->prepare( "SELECT * FROM wp_discussion_ques_follow WHERE user_id=%d AND ques_id=%d", $userId, $quesPost->id));
                                              if($checkFollow){
                                            ?>
                                                <form method="post" action="">
                                                  <input type="hidden" name="quesId" value="<?php echo $quesPost->id;?>">
                                                  <img src="/img/follow-icon-selected.png"><button type="submit" name="unfollow"> follow </button>
                                               </form>
                                         <?php
                                            } else{
                                            ?>
                                               <form method="post" action="">
                                                  <input type="hidden" name="quesId" value="<?php echo $quesPost->id;?>">
                                                  <img src="/img/Follow.png"><button type="submit" name="follow"> Follow </button>
                                               </form>
                                         <?php 
                                            }
                                            } else {
                                            ?>
                                                <img src="/img/Follow.png"><button types="button" data-toggle="modal" data-target="#myModalques">Follow</button>
                                         <?php   
                                            }
                                            ?>
                                      </div>
                                   </div>
                                   <div class="col-xs-4 col-md-4">
                                      <div class="share2">
                                      <?php
                                          if(isset($_SESSION["login"])) {
                                            $userId = $_SESSION["login"]["id"];
                                      ?>
                                            <!--<p data-toggle="modal" data-target="#myModalrepostrr<?php //echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                      <?php
                                          } else{
                                      ?>
                                            <!--<p data-toggle="modal" data-target="#myModalrepostrr<?php //echo $z.$z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                      <?php
                                          }
                                      ?>
                                      </div>
                                            <!-- Modal -->
                                            <div id="myModalrepostrr<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                               <div class="modal-dialog">
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                     <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"> Repost </h4>
                                                     </div>
                                                     <div class="modal-body">
                                                        <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>

                                            <!-- Modal -->
                                            <div id="myModalrepostrr<?php echo $z;?>" class="modal fade" role="dialog">
                                               <div class="modal-dialog">
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                     <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title"> Repost </h4>
                                                     </div>
                                                     <div class="modal-body">
                                                      <form method="post" action="">
                                                        <p> <textarea name="reposttext"> </textarea></p>
                                                        <?php
                                                                  $userPic = $wpdb->get_results( 
                                                                    $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$quesPost->user_id));
                                                                  $userName = $wpdb->get_results( 
                                                                    $wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$quesPost->user_id));
                                                                  $userHeadline = $wpdb->get_results( 
                                                                   $wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$quesPost->user_id));
                                                                ?>
                                                                <div class="col-md-2 col-sm-2 col-xs-4">
                                                                        <div class="imgforr">
                                                                           <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                        </div>
                                                                     </div>
                                                                <div class="col-md-10 col-sm-10 col-xs-8">
                                                                  <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $ansPost->user_id;?>"><?php echo $userName[0]->name;?></a></strong>,<span><?php echo $userHeadline[0]->headline;?></span></p>
                                                                  <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($lastAns[0]->created_at )); ?> </p>
                                                               </div>
                                                        <div class="col-md-12 repostmodal">
                                                            <p> Repost this question </p>
                                                            <p> Question :  <?php echo $quesPost->ques; ?> </p>
                                                        </div>
                                                        <input type="hidden" name="type" value="ques">
                                                        <input type="hidden" name="quesid" value="<?php echo $quesPost->id;?>">
                                                        <input type="hidden" name="ques" value="<?php echo $quesPost->ques;?>">
                                                        <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                      </form>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>

                                   </div>
                                </div>
                           <?php
                              $z++;
                              }
                           ?>   
                        </div>
                     </div>


                  </div>
               </div>
               <!-- #main -->
            </div>
         </div>
      </div>
      <div class="col-md-3">
      </div>
   </div>
   <!-- #primary -->
</div>
<?php get_footer(); ?>
<?php
   function time_elapsed_string($datetime, $full = false) {
       $now = new DateTime;
       $ago = new DateTime($datetime);
       $diff = $now->diff($ago);
   
       $diff->w = floor($diff->d / 7);
       $diff->d -= $diff->w * 7;
   
       $string = array(
           'y' => 'year',
           'm' => 'month',
           'w' => 'week',
           'd' => 'day',
           'h' => 'hour',
           'i' => 'minute',
           's' => 'second',
       );
       foreach ($string as $k => &$v) {
           if ($diff->$k) {
               $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
           } else {
               unset($string[$k]);
           }
       }
   
       if (!$full) $string = array_slice($string, 0, 1);
       return $string ? implode(', ', $string) . ' ago' : 'just now';
   }
   ?>
<script>
   var acc = document.getElementsByClassName("accordion");
   var i;
   
   for (i = 0; i < acc.length; i++) {
     acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
         panel.style.display = "none";
       } else {
         panel.style.display = "block";
       }
     });
   }
</script>
<script> 
   function openCity(evt, cityName) {
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("tabcontent");
     for (i = 0; i < tabcontent.length; i++) {
       tabcontent[i].style.display = "none";
     }
     tablinks = document.getElementsByClassName("tablinks");
     for (i = 0; i < tablinks.length; i++) {
       tablinks[i].className = tablinks[i].className.replace(" active", "");
     }
     document.getElementById(cityName).style.display = "block";
     evt.currentTarget.className += " active";
   }
   
   function openReport(evt, reportName) {
     var i, repocontent, repolinks;
     repocontent = document.getElementsByClassName("repocontent");
     for (i = 0; i < repocontent.length; i++) {
       repocontent[i].style.display = "none";
     }
     repolinks = document.getElementsByClassName("repolinks");
     for (i = 0; i < repolinks.length; i++) {
       repolinks[i].className = repolinks[i].className.replace(" active", "");
     }
     document.getElementById(reportName).style.display = "block";
     evt.currentTarget.className += " active";
   }
   function moreFunction(id) {
      var cusId = ".anscomplete" + id;
      var moreId = ".ansmore" + id;
   
      $(cusId).css("display", "inline");
      $(moreId).css("display", "none");
   }
   
   function changeFunction() {
      
      if(document.getElementById("filterdrops").value == "Recent") {
          $(".recent1").css("display", "block");
          $(".alltime").css("display", "none");
          $(".monthtop").css("display", "none");
      
      } else if(document.getElementById("filterdrops").value == "alltop") {
          $(".recent1").css("display", "none");
          $(".alltime").css("display", "block");
          $(".monthtop").css("display", "none");
   
      } else if(document.getElementById("filterdrops").value == "monthtop") {
          $(".recent1").css("display", "none");
          $(".alltime").css("display", "none");
          $(".monthtop").css("display", "block");
      }
   }
</script>
<div class="modal fade" id="myModal1" role="dialog">
   <div class="modal-dialog vertical-align-center">
      <div class="modal-content">
         <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
            <ul>
               <li>
                  <span  class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                     <div class="funkyradio-default">
                        <label for="checkbox1">I think it's spam, Promotional</label>
                     </div>
                  </span>
               </li>
               <li>
                  <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                     <div class="funkyradio-default">
                        <label for="checkbox1">I think  it's objectionable</label>
                     </div>
                  </span>
               </li>
               <li>
                  <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                     <div class="funkyradio-default">
                        <label for="checkbox1">I think it false or misinformation</label>
                     </div>
                  </span>
               </li>
            </ul>
            <div class="modal-footer">
            </div>
         </div>
         <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
            <div class="modal-body">
               <p>You Are Reorting that this is spam, Promotional</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
               <button type="button" class="btn btn-default">Submit</button>
            </div>
         </div>
         <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
            <div class="modal-body">
               <div class="funkyradio-default">
                  <label class="checkbox-inline"><input type="radio" value="">It's pornographic or extremely violent.</label>
               </div>
               <div class="funkyradio-default">
                  <label class="checkbox-inline"><input type="radio" value="Education Level">It's hate speech.</label>
               </div>
               <div class="funkyradio-default">
                  <label class="checkbox-inline"><input type="radio" value="Education Level">Topic or language is offensive/Abusive.</label>
               </div>
            </div>
            <div class="bottom-btns">
               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
               <button type="button" class="btn btn-default">Submit</button>
            </div>
         </div>
         <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
            <div class="modal-body">
               <p>You are reporting that it is a salse information or misinformation.</p>
            </div>
            <div class="bottom-btns">
               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
               <button type="button" class="btn btn-default">Submit</button>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Join Modal -->
<div id="myModaljoin" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Join Discussion House</h4>
         </div>
         <div class="modal-body">
            <p> For join please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>
<!-- Question Modal -->
<div id="myModalques" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Post any Question?</h4>
         </div>
         <div class="modal-body">
            <p> For post any question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>
<div id="myModalques1" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Post any Question?</h4>
         </div>
         <div class="modal-body">
            <p> For post any question. Please join the discussion house. </p>
         </div>
      </div>
   </div>
</div>
<div id="myModalques2" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Post any Question?</h4>
         </div>
         <div class="modal-body">
            <form method="post" action="">
               <input type="hidden" name="cid" value="<?php echo $house[0]->cat_id;?>">
               <input type="hidden" name="scid" value="<?php echo $house[0]->subcat_id;?>">
               <input type="hidden" name="hid" value="<?php echo $_GET['action'];?>">
               <div class="form-group">
                  <?php
                     $my_content1 =" ";
                     $editor_id1 = 'quesPost';
                     $option_name1 ='quesPost';
                     wp_editor($my_content1, $editor_id1, array('wpautop'=> true,'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                     ?>
               </div>
               <input type="submit" name="postquessubmit" value="Submit">
            </form>
         </div>
      </div>
   </div>
</div>
<!-- Modal -->
<div id="myModalfollow" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Follow</h4>
         </div>
         <div class="modal-body">
            <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>
  
