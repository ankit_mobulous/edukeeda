<div class="Postyourpage">
<?php
session_start();
/**
/*
 Template Name: Post Your Query Page 
 */
if($_SESSION["login"]) {

} else {
  header('Location: https://edukeeda.com/signin/');
}  
$userId = $_SESSION["login"]["id"];
get_header(); ?>
   <div class="col-md-12 col-sm-12">
      <div class="filteropt">
         <p>What do you want to post?</p>
         <p class="wantpost"> Any Updates For Your Wall? Post it here - <b><a href="<?php echo site_url();?>/wallpost-query-form/">Wallpost</a></b> </p>
         <!--<p class="wantpost"> Any InsideCampus Story/Training & Placement Experiences? Post it here - <b><a href="<?php echo site_url();?>/student-query-form/"> Student Corner </a></b></p>-->
         <p class="wantpost"> Any Post / Article/Blog on <b>Expert Gyan</b> Section? Post it here - <b><a href="<?php echo site_url();?>/expertise-query-form/">Post / Article/Blog</a></b> </p>
         <p class="wantpost"> Any Event? Post it here - <b><a href="<?php echo site_url();?>/eventdetails/">Post Event</a></b> </p>
         <!--<p class="wantpost"> Any Question Or Discussion In Mind? Post it here - <b><a href="<?php //echo site_url();?>/discussion-forum-2/">Discussion Forum</a></b> </p>-->
      </div>
   </div>

   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>
<style>
   .stu {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   margin-bottom: 50px;
   }
   .stu h1 {
   font-size: 27px;
   margin: 15px 30px;
   }
   .optstu {
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   float: left;
   width: 100%;
   margin: 14px 3px;
   }   
   .optstu p {
   margin-bottom: 0px;
   font-size: 13px;
   color: #060658;
   padding: 20px;
   }
   .optstu input[type="checkbox"] {
   margin: 0px 11px;
   }
   .filteropt {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   margin-bottom: 30px;
   margin-top: 50px;
   padding-bottom: 30px;
   }
   .Query {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   }
   .Query p {
   font-size: 13px;
   margin: 36px 28px;
   float: left;
   }
   .Queryw{
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   }
   .Queryw p {
   font-size: 24px;
   margin: 36px 28px;
   float: left;
   font-weight: 600;
   color: #060658;
   }
   .sub {
   float: left;
   width: 100%;
   margin: 15px 0px;
   text-align: center;
   }
   .sub button.btn.btn-primary {
   background: #f68e2f;
   }
   .filteropt select {
   background: #fff;
   border: 1px solid #ddd;
   height: 42px;
   line-height: 8px;
   padding: 1px 16px;
   margin: 39px 0px 0 22px;
   }
   .filteropt p{
    padding: 28px 10px 10px 30px;
    color: #0a0a5b;
    font-weight: 600;
    font-size: 24px;
    margin-bottom: 0px;
   }
   p.wantpost {
    margin-bottom: 0px!important;
    padding-bottom: 0px;
    font-weight: 300;
    margin-left: 5%;
  }

@media only screen and (max-width: 767px) {


.filteropt p {
    padding: 28px 10px 10px 30px;
    color: #0a0a5b;
    font-weight: 600;
    font-size: 15px;
    margin-bottom: 0px;
}
}
</style>

