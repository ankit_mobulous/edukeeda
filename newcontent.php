<div class="List Of Events">
<?php
/**
/*
Template Name: Home Page newcontent
*/
if($_GET["id"]) {
   } else {
     header('Location: http://edukeeda.com/events/');
   }
get_header(); ?>
<?php
  if(isset($_POST["follow"])) {
      global $wpdb;
      $catType = $_POST["catType"];
      $catId = $_POST["catId"];
      $userid = $_SESSION["login"]["id"];
      $data = ["user_id"=>$userid, "cat_id"=>$catId, "cat_type"=>$catType];
      $wpdb->insert("wp_follow_cat", $data);
  }
  if(isset($_POST["unfollow"])) {
      global $wpdb;
      $catType = $_POST["catType"];
      $catId = $_POST["catId"];
      $userid = $_SESSION["login"]["id"];
      $wpdb->query($wpdb->prepare("DELETE FROM wp_follow_cat WHERE user_id = %d AND cat_id = %d", $userid, $catId));
  }
?>
<style>
div.rightwd.views {
    color: #000;
    font-size: 13px;
    float: right;
}
    .subc i.fa.fa-share-alt {
    float: right;
    color: #000;
}
  .datespn {
  position: absolute;
  top: 11px;
  left: 24px;
  background: #fff;
  padding: 7px;
  color: #060658;
  font-size: 24px;
  text-align: center;
  border: 1px solid lavenderblush;
  border-radius: 7px;
  line-height: 17px;
  }
  .datespn span {
  
  font-weight: 600;
  }
  .subcate span a {
  color: #9a9a9a;
    font-size: 13px;
    text-transform: uppercase;
}
div#secondary p a{text-transform: uppercase;}
.subcate a {
   color: #e96125;
    font-size: 20px;
}
.subcate a:hover {
    color: #5a5a5a;
}
  span.dateDisplay-day {
  color: #f68e2f;
  font-size: 20px;
  font-weight: 800;
  line-height: 21px;
  }
  .subc i.fa.fa-share-alt {
  float: right;
  }
  .subcate p {
    margin-bottom: 0px;
}
  .subc
  {
  box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
  float: left;
  width: 100%;
  margin-bottom: 40px;
  padding:10px;
  border:1px solid #ddd;
  }
  .intdate span {
  font-size: 13px;
  font-weight: 600;
  display: block;
  padding: 0px;
  background: #f4f4f4;
  border-radius:10px;
  margin: 0px 53px 0px 53px;
  text-align: center;
  line-height: 25px;
  }
  .detail a {
  background: #439a37;
  color: #fff;
  padding: 6px 18px;
  font-size: 13px;
  display: block;
  text-align: center;
  margin: 13px 0 0 0;
  text-transform: capitalize;
  }
  .detail a:hover {
  background-color: #f68e2f;
  }
  .subc i{
  margin-right: 0px!important;
  }
  p.subb {
  font-size: 20px;
  font-weight: 600;
  margin-bottom: 0px;
  float: left;
  }

  .subcate strong {
  font-size: 15px;
  }
  .subcate span {
  display: block;
  }
  .leftoption {
  float: left;
  }
  .leftoption input[type="Text"] {
  width: 69%;
  background: #fff;
  border: 1px solid #ddd;
  height: 31px;
  padding: 0px 16px;
  float: left;
  }
  .filterkeeda i.fa.fa-search {
  padding: 0px;
  margin-top: -3px;
  }
  .filterkeeda button {
  height: 35px;
  margin: 0px 4px;
  }
  .rightoption {
  float: right;
      margin-bottom: 0px;
    margin-top: 0px;
  }
  .rightoption select {
  background: #fff;
  border: 1px solid #ddd;
  height: 33px;
  line-height: 3px;
  padding: 1px 18px;
  margin: 0 0px 0 27px;
  }
  .rightoption p{
  float:left;
  }
  #secondary{width:100%;}
  input.applyfilt {
  padding: 4px 17px;
  float: right;
  }
  .sortbyalphabet ul li {
  display: inline-block;
  margin: 0 19px 0 0;
  }
  p.subb.eventarticle {
  line-height: 23px;
  min-height: 57px;
  }
  img.img-thumbnail {
  height: 131px;
  }
  .simg {
    float: left;
    width: 100%;
}
.othermemd {
    float: left;
    width: 100%;
  margin-bottom: 15px;
}
.simg a {
    float: left;
    width: 100%;
}
.pimg {
    float: left;
    width: 100%;
}
.pimg p {
    margin-bottom: 4px!important;
}
.pimg span {
    font-size: 15px!important;
    line-height: 0px!important;
}
.postshare span {
    margin: 0px 76px 0px 6px;
  display: -webkit-box;
}
.postshare {
    float: left;
    width: 100%;
    display: -webkit-box;
      margin: 0px 16px;
}
.postshare p{
  margin-bottom: 0px!important;
}
.pimg a {
    line-height: 15px;
    float: left;
    width: 100%;
    margin: 4px 0px;
}
div#secondary a:hover {
    color: #e86125;
}
div#secondary a {
    font-weight: 600;
    font-size: 15px;
    color: #2b221f;
}

.pimg span {
    font-size: 15px!important;
    line-height: 15px!important;
}
.pimg {
    float: left;
    width: 100%;
    margin-bottom: 11px;
}
#secondary {
    width: 100%;
    margin-top: 104px;
}
p.leftwd strong
{
   float: left;
color: #9e9292;
margin-bottom: 7px;
font-size: 15px;
width: 100%;
}
p.leftwd strong a
{
color: #9e9292;
}
.colh.singlepost {
    float: left;
    width: 100%;
    margin-bottom: 26px;
    min-height: 80px;
    overflow: hidden;
  margin-top:10px;
}
.customsideedus {
    display: none;
}
 @media only screen and (max-width: 767px) {
.rightoption select {
    background: #fff;
    border: 1px solid #ddd;
    height: 33px;
    line-height: 3px;
    margin: 0 0px 0 0px;
    padding: 0px;
    float: right;
}
 .leftoption input[type="Text"] {
  width: 100%;
 }
.col-md-4.col-sm-12.customsideedu .widget-area .widget:first-child {
    display: none;
}
.customsideedus .widget-area .widget:first-child {
    display: none;
}
 .filterkeeda button {
    height: 35px;
    margin: 0px 4px;
    padding: 0px 20px;
}
.searchform input {
    height: 36px!important;
    padding: 4px 26px!important;
}
.colh.singlepost{
margin-top: 125px;}
.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper{
  padding: 15px!important;
}
.customsideedus {
    display: contents;
}
.leftoption {
    float: left;
    width: 100%;
}
.colh.singlepost h4 {
    float: left;
    margin-bottom: 0px;
}
.colh.singlepost {
    margin-top: 10px;
}
 }
 div#secondary h3.widget-title{border-bottom: 1px solid #f68e2f;}

 #secondary button.accordion {
    background-color: transparent;
    border: none;
    width: 100%;
    text-align: left;
   
    padding:0px;
}
#secondary button.accordion:hover {
    outline: none;
}
.panel {
  display: none;
  background-color: #f4f4f4;
}
.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    margin-top: -37px;
    font-size: 43px;
}
</style>
   <div class="col-md-8 col-sm-12">
      <div class="">
       <div class="colh singlepost">
            <?php
                global $wpdb;
                $catId = $_GET["id"];
                $getCName = $wpdb->get_results($wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $catId));
                
                $articles = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $catId));
                $c = 0;
                foreach ($articles as $article) {
                    $eventDateEnd1 = get_field('event_date_end', $article->object_id);
                    $currentDate1 = date("Y-m-d");
                    $eventDateEnd1 = date("Y-m-d", strtotime($eventDateEnd1));
                    
                    $eDate1 = (int) strtotime($eventDateEnd1);
                    $cDate1 = (int) strtotime($currentDate1);
                    if($eDate1 >= $cDate1) {
                        $c++;
                    }
                }
            ?>
          <h4 class="colh" style="width: 76%;margin-top: 0px;"><?php echo $getCName[0]->name; ?> <span>( <?php echo $c; ?> Events )</span> </h4>
              <?php
                if(isset($_SESSION["login"])) {
                  $userId = $_SESSION["login"]["id"];
                  $type = "event";
                  $term_id = $catId;
                  $checkFollow = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_follow_cat WHERE user_id=%d AND cat_id=%d", $userId, $term_id) 
                  );
                  if($checkFollow){
                ?>
                     <form method="post" action="">
                        <input type="hidden" name="catId" value="<?php echo $term_id;?>">
                        <input type="hidden" name="catType" value="<?php echo $type;?>">
                        <input type="submit" name="unfollow" value="Unfollow" class="btn btn-warning followbtn" style="float:right;">
                      </form>
                <?php
                  } else{
                ?>
                      <form method="post" action="">
                        <input type="hidden" name="catId" value="<?php echo $term_id;?>">
                        <input type="hidden" name="catType" value="<?php echo $type;?>">
                        <input type="submit" name="follow" value="Follow" class="btn btn-warning followbtn" style="float:right;">
                      </form>
                <?php 
                  }
                } else {
              ?>
                  <button type="button" class="btn btn-warning followbtn" data-toggle="modal" data-target="#myModalEvent" style="float:right;">Follow</button>
              <?php   
                }
              ?>
       </div>
    </div>
      <div class="">
         <div class="filterkeeda">
            <div class="leftoption">
               <form class="searchform" action=" " method="post">
                  <input type="text" placeholder="Search.." name="search" class="searchtext">
                  <button type="submit"><i class="fa fa-search"></i></button>
               </form>
            </div>
            <div class="rightoption">
               <form action="" method="post" name="myFormName">
                  <p>Sort By:</p>
                  <select name="top_filter" id="topdropfilter" onchange="this.form.submit()">
                    <option value=""> Select </option>
                     <option value="date">Latest Events</option>
                     <option value="recent">New Posts</option>
                     <option value="views">Most Viewed </option>
                  </select>
               </form>
            </div>
         </div>
      </div>
      <div id="secondary" class="widget-area col-md-3 customsideedus" role="complementary">
        <aside id="recent-posts-3" class="widget widget_recent_entries" style="padding-top: 20px;padding-bottom: 40px;">
            <button class="accordion">  <h3 class="widget-title">Filter</h3></button>
                <div class="panel">
  
                     <?php
                        $parent_cat_ID = $_GET["id"];
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                               'taxonomy' => 'events_categories'
                           );

                        $subcats = get_categories($args);
                        //var_dump($subcats);
                        echo "<ul>";
                        if($subcats) {
                        foreach($subcats as $subcat) {

                            $term_id = $subcat->term_id;

                            $current = date('Y-m-d');
                            
                            $getListCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish' AND wp_postmeta.meta_key='_event_date_end' AND wp_postmeta.meta_value>='$current'", $term_id));

                      ?>
                                <li>
                                    <form method="post" action="" id="form-id"> 
                                      <a href="<?php echo site_url(); ?>/events-list?id=<?php echo $_GET['id']; ?>&action1=<?php echo $term_id; ?>" onclick="document.getElementById('form-id').submit();"> <?php echo $subcat->name; ?>  ( <?php echo $getListCount[0]->countPost; ?> ) </a>
                                      <input type="hidden" name="fitercat" value="<?php echo $subcat->term_id;?>">
                                    </form>
                                </li>
                            <?php
                        }
                        } else{
                    ?>
                        <li> General </li>
                    <?php
                        }
                        echo "</ul>";
                     ?></div>
                  </aside>
                </div>
      <?php
      if(isset($_POST["search"])) {

         global $wpdb;
         $searchFilter = $_POST["search"];
         if(isset($_GET['action1'])){
            $catId = $_GET['action1'];
         } else{
            $catId = $_GET["id"];
         }
         $action = "event";

         $results = $wpdb->get_results( 
             $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id = %d", $catId) 
         );

         foreach($results as $result) {
           $results1 = $wpdb->get_results( 
               $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d AND post_status='publish' AND post_type=%s", $result->object_id,$action) 
           );
           
           foreach($results1 as $result1) {
             
             if(stripos($result1->post_title, $searchFilter) !== false) {
             
                $getCatName = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]) 
                );
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                
                if($image){
                  $image = $image[0];
                } else {
                    $mkey = "_thumbnail_id";
                    $checkimg = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                    );
                    $getimg = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                    );
                    $image = $getimg[0]->guid;  
                }
                
                $table_name = "wp_post_views";
                $checkViews = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
                );
                
                $countPostTitle = strlen($result1->post_title);
                if($countPostTitle >= 80) {
                  $postTitle = substr($result1->post_title,0, 80);
                  $postTitle .= "...";
                } else {
                  $postTitle = $result1->post_title;
                }
                $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                           $author_name = $getAuthor[0]->display_name;
                 $eventDate = get_field('event_date', $result1->ID);
                  $eventDateEnd = get_field('event_date_end', $result1->ID);
                  $currentDate = date("Y-m-d");
                  $eventDateEnd = date("Y-m-d", strtotime($eventDateEnd));
                  
                  $eDate = (int) strtotime($eventDateEnd);
                  $cDate = (int) strtotime($currentDate);
                  
                  if($eDate >= $cDate) {
    ?>
            <div class="subc">
                 <div class="col-md-4 col-sm-5">
                    <div class="altimg">
                       <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><img class="img-thumbnail" src="<?php echo $image; ?>"></a>
                       <div class="datespn">
                          <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                          <span class="dateDisplay-day">
                          <span><?php echo date('d',strtotime($eventDate)); ?></span><br>
                          </span>
                          <span class="dateDisplay-month text--tiny">
                          <span><?php echo date('M',strtotime($eventDate)); ?></span>
                          </span>
                          </time>
                       </div>
                    </div>
                 </div>
                 <div class="col-md-8 col-sm-7">
                    <div class="subcate">
                      <strong>
                       <span><a href="<?php echo site_url();?>/events/"> Event</a> <a href="<?php echo site_url();?>/events-list/?id=<?php echo $catId;?>"> . <?php echo $getCatName[0]->name; ?></a> </span>
                      </strong>
                      <p><strong><a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><?php echo $postTitle; ?></a></strong></p>
                    </div>
                    <p class="leftwd">
                    <strong>
                    Post By : <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                    </strong></p>
                    <div class="dropdown customs">      
                            <button class="dropbtn">
                              <a href="#" class="rightwd views"><i class="fa fa-share-alt" aria-hidden="true"></i></a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                    <p class="rightwd"><strong> </strong> <?php //echo date('F jS, Y . g:i a',strtotime($result1->post_date)); ?></p>
                 </div>
              </div>
  <?php
            }
          }
        }
      }
    } else if(isset($_POST["top_filter"])) {
         if(isset($_GET['action1'])){
            $catId = $_GET['action1'];
         } else{
            $catId = $_GET["id"];
         }
         $action = "event";
         $filter = $_POST["top_filter"];
         echo "<div style='clear:both;'></div>";
         global $wpdb;

         if($filter == "views") {
          echo "<div><p>Sort by: Most Views</p></div>";
           $results = $wpdb->get_results( 
               $wpdb->prepare( "SELECT * FROM wp_term_relationships LEFT JOIN wp_post_views ON wp_term_relationships.object_id = wp_post_views.post_id WHERE wp_term_relationships.term_taxonomy_id = %d ORDER BY wp_post_views.view_count DESC", $catId) 
           );
         } else if($filter == "recent") {
            echo "<div><p>Sort by: Recent Post</p></div>";
            $results = $wpdb->get_results( 
              $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id = %d ORDER BY object_id DESC", $catId)
            );
         } else if($filter == "date") {
            echo "<div><p>Sort by: Event date</p></div>";
            $results = $wpdb->get_results( 
               $wpdb->prepare( "SELECT * FROM wp_term_relationships INNER JOIN wp_postmeta ON wp_term_relationships.object_id = wp_postmeta.post_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_postmeta.meta_key ='event_date' ORDER BY wp_postmeta.meta_value ASC", $catId)
           );
         }

         foreach($results as $result) {
           $results1 = $wpdb->get_results( 
               $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d AND post_status='publish' AND post_type=%s", $result->object_id, $action) 
           );
           
            foreach($results1 as $result1) {
        
             $getCatName = $wpdb->get_results( 
                 $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]) 
             );
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
              if($image){
                  $image = $image[0];
              } else {
                  $mkey = "_thumbnail_id";
                $checkimg = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                );
                $getimg = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                );
                $image = $getimg[0]->guid;  
              }

             $table_name = "wp_post_views";
              $checkViews = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
              );
              $countPostTitle = strlen($result1->post_title);
              if($countPostTitle >= 80) {
                $postTitle = substr($result1->post_title,0, 80);
                $postTitle .= "...";
              } else {
                $postTitle = $result1->post_title;
              }
              $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                           $author_name = $getAuthor[0]->display_name;
              $eventDate = get_field('event_date', $result1->ID);   
               $eventDateEnd = get_field('event_date_end', $result1->ID);
              $currentDate = date("Y-m-d");
              $eventDateEnd = date("Y-m-d", strtotime($eventDateEnd));
              
              $eDate = (int) strtotime($eventDateEnd);
              $cDate = (int) strtotime($currentDate);
              
              if($eDate >= $cDate) {        
    ?>
           <div class="subc">
                 <div class="col-md-4 col-sm-5">
                    <div class="altimg">
                       <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><img class="img-thumbnail" src="<?php echo $image; ?>"></a>
                       <div class="datespn">
                          <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                          <span class="dateDisplay-day">
                          <span><?php echo date('d',strtotime($eventDate)); ?></span><br>
                          </span>
                          <span class="dateDisplay-month text--tiny">
                          <span><?php echo date('M',strtotime($eventDate)); ?></span>
                          </span>
                          </time>
                       </div>
                    </div>
                 </div>
                 <div class="col-md-8 col-sm-7">
                    <div class="subcate">
                      <strong>
                       <span><a href="<?php echo site_url();?>/events/"> Event</a> <a href="<?php echo site_url();?>/events-list/?id=<?php echo $catId;?>"> . <?php echo $getCatName[0]->name; ?></a> </span>
                      </strong>
                      <p><strong><a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><?php echo $postTitle; ?></a></strong></p>
                    </div>
                    <p class="leftwd"><strong>
                    Post By : <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                    </strong></p>
                    <div class="dropdown customs">      
                            <button class="dropbtn">
                              <a href="#" class="rightwd views"><i class="fa fa-share-alt" aria-hidden="true"></i></a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                    <p class="rightwd"><strong> </strong> <?php //echo date('F jS, Y . g:i a',strtotime($result1->post_date)); ?></p>
                 </div>
              </div>
  <?php
          }
        }
      }
    } else {
        if(isset($_GET['action1'])) {
            $catId = $_GET['action1'];
         } else{
            $catId = $_GET["id"];
         }
        $action = "event";

       global $wpdb;
       $results = $wpdb->get_results( 
           $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id = %d", $catId) 
       );

       foreach($results as $result) {
         $results1 = $wpdb->get_results( 
             $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d AND post_status='publish' AND post_type=%s", $result->object_id,$action) 
         );
         
          foreach($results1 as $result1) {
      
           $getCatName = $wpdb->get_results( 
               $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]));
           
           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
            if($image){
                $image = $image[0];
            } else {
                $mkey = "_thumbnail_id";
              $checkimg = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
              );
              $getimg = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
              );
              $image = $getimg[0]->guid;  
            }

            $table_name = "wp_post_views";
            $checkViews = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
            );

            $countPostTitle = strlen($result1->post_title);
            if($countPostTitle >= 80) {
              $postTitle = substr($result1->post_title,0, 80);
              $postTitle .= "...";
            } else {
              $postTitle = $result1->post_title;
            }
            $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                           $author_name = $getAuthor[0]->display_name;
            $eventDate = get_field('event_date', $result1->ID);
            $eventDateEnd = get_field('event_date_end', $result1->ID);
            $currentDate = date("Y-m-d");
            $eventDateEnd = date("Y-m-d", strtotime($eventDateEnd));
            
            $eDate = (int) strtotime($eventDateEnd);
            $cDate = (int) strtotime($currentDate);
            
            if($eDate >= $cDate) {
      ?>
              <div class="subc">
                 <div class="col-md-4 col-sm-5">
                    <div class="altimg">
                       <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><img class="img-thumbnail" src="<?php echo $image; ?>"></a>
                       <div class="datespn">
                          <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                          <span class="dateDisplay-day">
                          <span><?php echo date('d',strtotime($eventDate)); ?></span><br>
                          </span>
                          <span class="dateDisplay-month text--tiny">
                          <span><?php echo date('M',strtotime($eventDate)); ?></span>
                          </span>
                          </time>
                       </div>
                    </div>
                 </div>
                 <div class="col-md-8 col-sm-7">
                    <div class="subcate">
                      <strong>
                       <span><a href="<?php echo site_url();?>/events/"> Event</a> <a href="<?php echo site_url();?>/events-list/?id=<?php echo $catId;?>"> . <?php echo $getCatName[0]->name; ?> </a></span>
                      </strong>
                      <p><strong><a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><?php echo $postTitle; ?></a></strong></p>
                    </div>
                    <p class="leftwd"><strong>
                    Post By : <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                    </strong></p>


                    <div class="dropdown customs">      
                            <button class="dropbtn">
                              <a href="#" class="rightwd views"><i class="fa fa-share-alt" aria-hidden="true"></i></a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>

                    <p class="rightwd"><strong> </strong> <?php //echo date('F jS, Y . g:i a',strtotime($result1->post_date)); ?></p>
                 </div>
              </div>
      <?php
            }
        }
      }
    }
  ?>
   </div>
   <div class="col-md-4 col-sm-12 customsideedu">
      <div id="secondary" class="widget-area col-md-3" role="complementary">

        <aside id="recent-posts-3" class="widget widget_recent_entries" style="padding-top: 12px;">
            <button class="accordion">  <h3 class="widget-title">Filter</h3></button>
                <div class="panel">
  
                     <?php
                        $parent_cat_ID = $_GET["id"];
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                               'taxonomy' => 'events_categories'
                           );

                        $subcats = get_categories($args);
                        //var_dump($subcats);
                        echo "<ul>";
                        if($subcats) {
                        foreach($subcats as $subcat) {
                            $term_id = $subcat->term_id;
                            
                            $getListP = $wpdb->get_results($wpdb->prepare("SELECT wp_term_relationships.object_id FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish' AND wp_posts.post_type='event' AND wp_postmeta.meta_key='_event_date_end'", $term_id));
                            //var_dump($getListP);
                            $xx=0;
                            foreach($getListP as $getList) {
                              $eventDateEnd = get_field('event_date_end', $getList->object_id);
                              $currentDate = date("Y-m-d");
                              $eventDateEnd = date("Y-m-d", strtotime($eventDateEnd));
                              $eDate = (int) strtotime($eventDateEnd);
                              $cDate = (int) strtotime($currentDate);
                              
                              if($eDate >= $cDate) {
                                $xx++;
                              }
                            }
                      ?>
                                <li>
                                    <form method="post" action="" id="form-id"> 
                                      <a href="<?php echo site_url(); ?>/events-list?id=<?php echo $_GET['id']; ?>&action1=<?php echo $term_id; ?>" onclick="document.getElementById('form-id').submit();"> <?php echo $subcat->name; ?>  ( <?php echo $xx; ?> ) </a>
                                      <input type="hidden" name="fitercat" value="<?php echo $subcat->term_id;?>">
                                    </form>
                                </li>
                            <?php
                        }
                        } else{
                    ?>
                        <li> General </li>
                    <?php
                        }
                        echo "</ul>";
                     ?></div>
                  </aside>

        <aside id="recent-posts-3" class="widget widget_recent_entries">
          <h3 class="widget-title">TOP TRENDING EVENTS</h3>
          <?php
            $typePost = "event";
            $topPosts = $wpdb->get_results( 
            $wpdb->prepare("SELECT * FROM wp_posts INNER JOIN wp_post_views ON wp_posts.ID = wp_post_views.post_id WHERE wp_posts.post_type = %s AND wp_posts.post_status='publish' ORDER BY wp_post_views.view_count DESC LIMIT 3", $typePost)
            );
                if(!empty($topPosts))
                {
                  foreach ($topPosts as $topPost) {
                    $getCatID = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = %d", $topPost->ID) 
                    );
                    $getCatName = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $getCatID[0]->term_taxonomy_id) 
                    );
                    
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $topPost->ID ), 'single-post-thumbnail' );
                    if($image){
                        $image = $image[0];
                    } else {
                        $mkey = "_thumbnail_id";
                      $checkimg = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $topPost->ID,$mkey) 
                      );
                      $getimg = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                      );
                      $image = $getimg[0]->guid;
                    }
                    $countPostTitle1 = strlen($topPost->post_title);
                    if($countPostTitle1 >= 30) {
                      $postTitle1 = substr($topPost->post_title,0, 30);
                      $postTitle1 .= "...";
                    } else {
                      $postTitle1 = $topPost->post_title;
                    }
                    $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $topPost->post_author));
                           $author_name = $getAuthor[0]->display_name;
                  $eventDateEnd2 = get_field('event_date_end', $topPost->ID);
                  $currentDate2 = date("Y-m-d");
                  $eventDateEnd2 = date("Y-m-d", strtotime($eventDateEnd2));
                  
                  $eDate2 = (int) strtotime($eventDateEnd2);
                  $cDate2 = (int) strtotime($currentDate2);
                  if($eDate2 >= $cDate2) {
           ?>
                  <div class="othermemd">
                      <div class="col-md-5">
                         <div class="simg">
                            <a href="<?php echo esc_url( get_permalink($topPost->ID) ); ?>"> <img src="<?php echo $image; ?>" class="img-thumbnail"></a>
                         </div>
                      </div>
                      <div class="col-md-7">
                         <div class="pimg">
                            <p> <strong><a href="<?php echo site_url();?>/events-list/?id=<?php echo $getCatID[0]->term_taxonomy_id;?>"><?php if(strlen($getCatName[0]->name) > 20){echo substr($getCatName[0]->name, 0,20) .'...';}else{echo $getCatName[0]->name;} ?></a></strong></p>
                            <a href="<?php echo esc_url( get_permalink($topPost->ID) ); ?>">
                               <?php echo !empty($postTitle1)?$postTitle1:""; ?>
                            </a>
                            <span>By <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $topPost->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?></span>
                         </div>
                      </div>
                   </div>
              <?php
                    }
                 }
               }
              ?>
        </aside>
      </div>
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>

<!-- Modal -->
<div id="myModalEvent" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Follow</h4>
      </div>
      <div class="modal-body">
        <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
      </div>
    </div>
  </div>
</div>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
