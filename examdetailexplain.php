<div class="examdetal">
<?php
/**
/*
Template Name: Exam Detail Page
*/
get_header(); ?>
<style type="text/css">
  .page-wrap .content-wrapper{
  background-color:transparent;
  }
  .boardExamHeader {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;
  padding-bottom: 100px;
  padding-top: 72px;
  }
  .boardExamHeader_left {
  width: 674px;
  padding-top: 35px;
  padding-left: 361px;
  }
  .boardExamHeader_title {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  line-height: 1;
  color: #60b91f;;}
  .boardExamHeader_titleImage {
  width: 90px;
  height: 90px;
  background: hsla(0,0%,100%,.2);
  border-radius: 50%!important;
  margin-right: 20px;
  }
  img {
  max-width: 100%;
  }
  .ajeetbaba .container{
  width: 100%;
  }
  .headings_h1 {
  font-size: 34px;
  letter-spacing: -1.67px;
  }
  .boardExamHeader_description {
  margin: 32px 0 50px;
  font-size: 14px;
  line-height: 21px;
  font-weight: 600;
  color: white;
  }
  .button-smallHeight {
  height: 40px;
  font-size: 12px;}
  .button {
  font-size: 14px;
  font-weight: 700;
  text-align: center;
  border-radius: 100px;
  text-transform: uppercase;
  padding: 0 20px;
  letter-spacing: 1px;
  color: #fff;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  height: 50px;
  -webkit-transition: background .2s ease;
  transition: background .2s ease;
  background: #60b91f;
  }
  .boardExamHeader_right {
  font-size: 15px;
  margin: 0 auto;
  line-height: 20px;
  font-weight: 600;
  width: 441px;
  padding: 20px 35px 35px;
  background-color: white;
  border-radius: 100px 4px 100px 4px;
  }
  .boardExamHeader_detailsHeading {
  color: #5c5cd3;
  text-align:center;
  margin-bottom: 25px;
  font-size: 23;}
  .boardExamHeader_details {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;
  }
  .boardExamHeader_details_list {
  margin-bottom: 20px;
  width: calc(50% - 15px);
  }
  li {
  list-style-type: none;
  }
  .boardExamHeader_details_label {
  font-size: 12px;
  line-height: 17px;
  letter-spacing: 1px;
  color: #6666d3;
  text-transform: uppercase;}
  .boardExamHeader_details_value {
  color: #000;
  }
  .knowAboutSection {
  padding-top: 50px;
  padding-bottom: 100px;
  background: #fff;
  }
  .knowAboutSection_mainHeading {
  text-align: center;
  font-size: 32;
  color: #5c5cd0;
  }
  .knowAboutSection_lhsRhsWrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;
  margin-top: 50px;
  }
  .knowAboutSection_lhs {
  width: 30.22%;
  padding-top: 43px;
  }
  .flex_centered {
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  }
  .knowAboutSection_lhsHeading {
  background: #f9f9f9;
  font-weight: 700;
  margin-bottom: 4px;
  text-align:left;
  padding-left: 35px;
  }
  .knowAboutSection_lhsHeading, .knowAboutSection_lhsItem, .knowAboutSection_lhsSubListItem {
  font-size: 12px;
  color: #000;
  letter-spacing: 2px;
  text-transform: uppercase;
  line-height: 18px;
  }
  .knowAboutSection_lhsItem {
  border-bottom: 1px solid #efefef;
  background: #fafafa;
  cursor: pointer;
  }
  .knowAboutSection_lhsItem p {
  height: 50px;
  padding-left: 36px;
  padding-right: 15px;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-transition: color .1s ease-in;
  transition: color .1s ease-in;
  font-size: 16px;
  }
  .knowAboutSection_lhsSubList {
  display: none;
  }
  .knowAboutSection_lhsSubListItem:first-child {
  margin-top: 0;
  }
  .knowAboutSection_lhsSubListItem {
  color: #969696;
  cursor: pointer;
  padding: 12px 15px 12px 56px;
  -webkit-transition: color .1s ease-in;
  transition: color .1s ease-in;
  }
  .knowAboutSection_rhsHeading {
  color: #5c5cd0;
  margin-bottom: 16px;
  font-weight: 700;
  font-size: 32px;
  }
  .knowAboutSection_rhsHeading svg {
  display: none;
  }
  .knowAboutSection_rhsContent {
  color: #7f7f7f;
  line-height: 26px;
  }
  .knowAboutSection_rhsSubHeading {
  font-weight: 600;
  font-size: 20px;
  color: #060658;
  margin-bottom: 10px;
  }
  .knowAboutSection_rhsSubContent {
  margin-bottom: 22px;
  color: #000;
  }
  .knowAboutSection_list>li:first-child {
  margin-top: 10px;
  }
  .knowAboutSection_list li {
  margin-top: 15px;
  line-height: 26px;
  }
  .knowAboutSection_list>li, .knowAboutSection_list li>ul li {
  padding-left: 25px;
  text-indent: -25px;
  }
  li {
  list-style-type: none;
  }
  .knowAboutSection_rhsSubHeading {
  font-weight: 600;
  font-size: 20px;
  color: #000;
  margin-bottom: 10px;
  }
  .knowAboutSection_rhsSubContent {
  margin-bottom: 22px;
  }
  .knowAboutSection_rhs li, .knowAboutSection_rhs p, .knowAboutSection_rhs table {
  line-height: 26px;
  }
  .knowAboutSection_subject {
  width: 100%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  font-size: 16px;
  line-height: 26px;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  font-weight: 600;
  margin-top: 20px;
  }
  .knowAboutSection_subject svg {
  margin-right: 10px;
  width: 40px;
  height: 40px;
  }
  .knowAboutSection_subject {
  width: 100%;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  font-size: 16px;
  line-height: 26px;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  font-weight: 600;
  margin-top: 20px;
  }
  .knowAboutSection_blockWrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  line-height: 26px;
  }
  .knowAboutSection_blockWrap .knowAboutSection_orderedList {
  -webkit-box-flex: 1;
  -ms-flex: 1;
  flex: 1;
  }
  .knowAboutSection_orderedList {
  padding-left: 30px;
  }
  .knowAboutSection_orderedList>li:first-child {
  margin-top: 10px;
  }
  .knowAboutSection_orderedList>li {
  margin-top: 15px;
  list-style-type: decimal-leading-zero;
  }
  .knowAboutSection_rhs li, .knowAboutSection_rhs p, .knowAboutSection_rhs table {
  line-height: 26px;
  }
  li {
  list-style-type: none;
  }
  .knowAboutSection_blockWrap .knowAboutSection_orderedList:nth-child(2n) {
  margin-left: 15px;
  }
  .knowAboutSection_blockWrap .knowAboutSection_orderedList {
  -webkit-box-flex: 1;
  -ms-flex: 1;
  flex: 1;
  }
  .knowAboutSection_orderedList {
  padding-left: 30px;
  }
  .knowAboutSection_subject svg {
  margin-right: 10px;
  width: 40px;
  height: 40px;
  }
  .knowAboutSection_blockWrap {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  line-height: 26px;}
  .knowAboutSection_blockWrap .knowAboutSection_orderedList {
  -webkit-box-flex: 1;
  -ms-flex: 1;
  flex: 1;
  }
  .knowAboutSection_orderedList {
  padding-left: 30px;
  }
  .knowAboutSection_rhsSubContent:last-child {
  margin-bottom: 0;
  }
  .knowAboutSection_rhs li, .knowAboutSection_rhs p, .knowAboutSection_rhs table {
  line-height: 26px;
  }
  .knowAboutSection_table {
  margin-top: 20px;
  table-layout: fixed;
  border: 1px solid #e7e7e7;
  border-radius: 10px;
  border-collapse: separate;
  width: 100%;
  word-break: break-word;
  text-indent: 0;
  }
  table {
  border-collapse: collapse;
  border-spacing: 0;
  }
  .knowAboutSection_table thead {
  background: #ededed;
  }
  .knowAboutSection_table th:last-child {
  border-top-right-radius: 10px;
  border-right: 0;
  }
  .knowAboutSection_table th:first-child {
  border-top-left-radius: 10px;
  }
  .knowAboutSection_table th {
  text-align: center;
  padding: 15px 5px;
  font-size: 14px;
  font-weight: 400;
  letter-spacing: 1px;
  border-right: 1px solid #e7e7e7;
  }
  .knowAboutSection_table td {
  width: 25%;
  padding: 15px 20px;
  border-right: 1px solid #e7e7e7;
  border-bottom: 1px solid #e7e7e7;
  text-align: left;
  }
  .knowAboutSection_table td:last-child:not(:first-child):not(.keepBorder) {
  border-right: 0;
  }
  .knowAboutSection_table td {
  width: 25%;
  padding: 15px 20px;
  border-right: 1px solid #e7e7e7;
  border-bottom: 1px solid #e7e7e7;
  }
  abbr, address, article, aside, audio, b, blockquote, body, canvas, caption, cite, code, dd, del, details, dfn, div, dl, dt, em, fieldset, figcaption, figure, footer, form, h1, h2, h3, h4, h5, h6, header, hgroup, html, i, iframe, img, ins, kbd, label, legend, li, mark, menu, nav, object, ol, p, pre, q, samp, section, small, span, strong, sub, summary, sup, table, tbody, td, tfoot, th, thead, time, tr, ul, var, video {
  margin: 0;
  padding: 0;
  border: 0;
  outline: 0;
  font-size: 100%;
  background: transparent;
  }
  *, :after, :before {
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
  }
  user agent stylesheet
  td, th {
  display: table-cell;
  vertical-align: inherit;
  }
  .knowAboutSection_rhs li, .knowAboutSection_rhs p, .knowAboutSection_rhs table {
  line-height: 18px;
  }
  .knowAboutSection_table {
  margin-top: 20px;
  table-layout: fixed;
  border: 1px solid #e7e7e7;
  border-radius: 10px;
  border-collapse: separate;
  width: 100%;
  word-break: break-word;
  text-indent: 0;
  }
  table {
  border-collapse: collapse;
  border-spacing: 0;
  }
  .knowAboutSection_lhsItem.active p{
  border-left: 4px solid #5c5cd0;
  padding-left: 32px;
  color: #5c5cd0;
  }
  @media only screen and (max-width: 767px){
  .boardExamHeader_title {
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  }
  .boardExamHeader_titleImage {
  width: 35px;
  height: 35px;
  margin-right: 7px;
  }
  .boardExamHeader {
  display: block;
  padding-bottom: 50px;
  margin-top: 100px;
  }
  .boardExamHeader_left {
  width: 256px;
  margin: auto;
  padding-top: 0;
  text-align: center;
  padding-left:0px;}
  .boardExamHeader_title {
  -webkit-box-pack: center;
  -ms-flex-pack: center;
  justify-content: center;
  }
  .boardExamHeader_description {
  margin: 9px 0 20px;
  font-size: 14px;
  line-height: 18px;
  }
  .boardExamHeader_cta {
  font-size: 11px;
  padding: 7px 36px 8px;
  letter-spacing: 1.24px;
  }
  .boardExamHeader_right {
  width: auto;
  padding: 24px 26px 30px;
  border-radius: 5px;
  margin: 0 auto;
  margin-top: 30px;
  max-width: 500px;
  }
  .boardExamHeader_detailsHeading {
  margin-bottom: 20px;
  }
  .boardExamHeader_details {
  display: block;}
  .boardExamHeader_details_list {
  width: 100%;}
  .boardExamHeader_details_label {
  font-size: 10px;
  line-height: 14px;
  letter-spacing: .83px;
  margin-bottom: 2px;
  }
  .boardExamHeader_details_value {
  font-size: 12px;
  line-height: 17px;
  font-weight: 400;
  letter-spacing: -.1px;
  color: #4a4a4a;
  }
  .knowAboutSection_lhsHeading, .knowAboutSection_lhsItem, .knowAboutSection_lhsSubListItem {
  font-size: 12px;
  color: #000;
  letter-spacing: 2px;
  text-transform: uppercase;
  line-height: 18px;
  margin-left: 0px;
  width: 258px;
  }
  }
  .examdetal .page-wrap .container{width:100%; padding:0 !important;}
  .examdetal .container1 {
  margin: 0 auto; 
  max-width: 1200px;
  padding: 0 15px;
  }
  .examdetal .page-wrap .row{margin:0;}
  .phn-tab-ma {
  margin-bottom: 30px;
  }
  .exambgas {
  background: #000;
  }
  .boardExamHeader a.button.button-smallHeight.boardExamHeader_cta.js-sign-up-btn {
    padding: 10px;
}
  .sybh , .patsn{
  height: 100px;
  }
  .examdetal header#masthead {
  background: #fff;
  box-shadow: #fff 0px 0px 20px 0px;
  }
  .examdetal #mega-menu-wrap-primary #mega-menu-primary > li.mega-menu-item > a.mega-menu-link{
  color:#464646;
  }
  .examdetal .site-logo {
  max-height: 100px;
  width: 88%;
  background: #fff;
  border-radius: 6px;
  box-shadow: #fff 0px 0px 13px 0px;
  }

  .knowAboutSection_rhsSubContent table th{border:1px solid #000;}
  .knowAboutSection_rhsSubContent table td{border:1px solid #000;}
  .knowAboutSection_rhsSubContent li{list-style-type: decimal;margin-left: 2%;}
</style>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$action = $_GET["action"];
$getExamDetails = $wpdb->get_results( 
    $wpdb->prepare( "SELECT * FROM wp_exam_details WHERE id = %d", $action)
);

?>
   <div class="exambgas">
      <div class="container1">
         <div class="boardExamHeader">
            <div class="col-md-4">
               <h1 class="headings_h1 boardExamHeader_title">
                  <img class="boardExamHeader_titleImage" src="http://edukeeda.com/wp-content/uploads/2018/11/images.png"> <?php echo $getExamDetails[0]->exam_title;?>
               </h1>
               <div class="boardExamHeader_description">
                  <?php echo $getExamDetails[0]->short_content;?>
               </div>
              <?php
                if($getExamDetails[0]->start_preparing == 1) {
              ?>
                  <a href="<?php echo site_url(); ?>/exams-subject-listing/?action=<?php echo $getExamDetails[0]->exam_id;?>" class="button button-smallHeight boardExamHeader_cta js-sign-up-btn" data-page="exam" data-element="start_preparing_now"> Start preparing now </a>
              <?php
                }
              ?>
            </div>
            <div class="col-md-7">
               <div class="boardExamHeader_right">
                  <div class="boardExamHeader_detailsHeading">
                     Info About <?php echo $getExamDetails[0]->exam_title;?>
                  </div>
                  <ul class="boardExamHeader_details">
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">Field</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->exam_field;?></div>
                     </li>
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">Eligibility</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->eligibility;?></div>
                     </li>
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">Subjects</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->subject;?></div>
                     </li>
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">Applicants</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->applicants;?></div>
                     </li>
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">Qualified</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->qualified;?></div>
                     </li>
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">Questions Type</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->questions_type;?></div>
                     </li>
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">Paper Pattern</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->paper_pattern;?></div>
                     </li>
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">Colleges</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->colleges;?> </div>
                     </li>
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">Difficulty Level</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->difficulty_level;?></div>
                     </li>
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">Time to Prepare</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->time_to_prepare;?></div>
                     </li>
                     <li class="boardExamHeader_details_list">
                        <div class="boardExamHeader_details_label">When</div>
                        <div class="boardExamHeader_details_value"><?php echo $getExamDetails[0]->exam_when;?></div>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   <section class="knowAboutSection">
      <div class="phn-tab-ma">
         <h2 class="knowAboutSection_mainHeading js-marker headings_h3" data-marker-id="1">
            All you need to know about <?php echo $getExamDetails[0]->exam_title;?>
         </h2>
      </div>
      <div class="container1">
         <div class="row">
            <div class="col-md-4">
               <div class="knowAboutSection_lhsItem js-marker-target active" data-marker-target="1" data-default-marker-target="2">
                  <p class="pp">Facts</p>
               </div>
               <div class="knowAboutSection_lhsItem js-marker-target" data-marker-target="5" data-default-marker-target="6">
                  <p class="syb">Details</p>
               </div>
               <div class="knowAboutSection_lhsItem js-marker-target" data-marker-target="10" data-default-marker-target="11">
                  <p class="pat">Syllabus & Pattern</p>
               </div>
            </div>
            <div class="col-md-8">
               <div class="knowAboutSection_rhsSection">
                  <h2 class="knowAboutSection_rhsHeading headings_h4 js-marker js-exam-sections-heading open" data-marker-id="1">
                     Facts, Syllabus and Pattern
                     <svg width="10" height="6" viewBox="0 0 10 6" xmlns="http://www.w3.org/2000/svg">
                        <title>Stroke 396</title>
                        <path d="M9 1L5 5 1 1" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"></path>
                     </svg>
                  </h2>
                  <div class="facts"></div>
                  <div class="knowAboutSection_rhsContent ">
                     <h2 class="knowAboutSection_rhsSubHeading js-marker" data-marker-id="2">What is <?php echo $getExamDetails[0]->exam_title;?>?</h2>
                     <div class="knowAboutSection_rhsSubContent">
                        <?php echo $getExamDetails[0]->facts;?>
                     </div>
                     <div class="sybh">
                     </div>
                     <h2 class="knowAboutSection_rhsSubHeading js-marker" data-marker-id="3">Syllabus for <?php echo $getExamDetails[0]->exam_title;?></h2>
                     <div class="knowAboutSection_rhsSubContent">
                        <p><?php echo $getExamDetails[0]->syllabus;?></p>              
                     </div>
                     <div class="patsn"></div>
                     <h2 class="knowAboutSection_rhsSubHeading js-marker" data-marker-id="4">Exam Pattern for <?php echo $getExamDetails[0]->exam_title;?></h2>
                     <div class="knowAboutSection_rhsSubContent pp1">
                        <div><?php echo $getExamDetails[0]->pattern;?> </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
</div>
</div>
</div>
</section>
</div>
<script>
jQuery(".syb").click(function() {
jQuery('html,body').animate({
scrollTop: jQuery(".sybh").offset().top},
'slow');
});
jQuery(".pat").click(function() {
jQuery('html,body').animate({
scrollTop: jQuery(".patsn").offset().top},
'slow');
});

</script
<?php get_footer(); ?>

