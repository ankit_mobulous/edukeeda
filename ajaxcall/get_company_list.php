<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$spos = $_POST['spos'];
$sedu = $_POST['sedu'];
$lan = $_POST['lan'];

if(!empty($spos)) {

    $sendRes = " ";
    $getPosition = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM wp_profileStepFour WHERE id = %d", $spos)
    );
    
    $getcompname = $wpdb->get_results( 
        $wpdb->prepare( "SELECT companyName FROM wp_companies WHERE id = %d", $getPosition[0]->company_name)
    );
    
    if($getPosition) {
        $data = array("pid"=>$getPosition[0]->id,
                      "title"=>$getPosition[0]->title,
                      "comapnyName"=>$getcompname[0]->companyName,
                      "country" => $getPosition[0]->country,
                      "location" => $getPosition[0]->location,
                      "from" => $getPosition[0]->from,
                      "to" => $getPosition[0]->to,
                      "currentWorking" => $getPosition[0]->current_working,
                      "desc" => $getPosition[0]->desc 
                  );
    }
    echo json_encode($data);
}

if(!empty($sedu)) {

    $sendRes = " ";
    $getPosition = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM wp_profileStepThree WHERE id = %d", $sedu)
    );
    
    $getschname = $wpdb->get_results( 
        $wpdb->prepare( "SELECT schoolName FROM wp_schools WHERE id = %d", $getPosition[0]->school)
    );
    
    if($getPosition) {
        $data = array("sid"=>$getPosition[0]->id,
                      "schoolName"=>$getschname[0]->schoolName,
                      "degree"=> $getPosition[0]->degree,
                      "branch" => $getPosition[0]->branch,
                      "grade" => $getPosition[0]->grade,
                      "from" => $getPosition[0]->from,
                      "to" => $getPosition[0]->to,
                      "desc" => $getPosition[0]->desc 
                  );
    }
    echo json_encode($data);
}

if(!empty($lan)) {

    $sendRes = " ";
    $getLang = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM wp_profileStepEighth WHERE id = %d", $lan)
    );

    $getLangName= $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM languages WHERE id = %s", $getLang[0]->user_lang)
    );
    if($getLangName) {
        $data = array("lid"=>$getLang[0]->id,
                      "llid"=>$getLangName[0]->id,
                      "langName"=>$getLangName[0]->name,
                  );
    }
    echo json_encode($data);
}

?> 
