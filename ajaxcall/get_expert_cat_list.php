<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$cone = $_POST["cid"];
$ctwo = $_POST['ccid'];

if(!empty($cone) && empty($ctwo)){

    if($cone == "industry") {
        $categories = get_categories( array(
           'child_of'            => 0,
           'parent'              => 0,
           'current_category'    => 0,
           'depth'               => 0,
           'echo'                => 1,
           'exclude'             => '',
           'exclude_tree'        => '',
           'feed'                => '',
           'feed_image'          => '',
           'feed_type'           => '',
           'hide_empty'          => 0,
           'hide_title_if_empty' => false,
           'hierarchical'        => true,
           'order'               => 'ASC',
           'orderby'             => 'name',
           'separator'           => '<br />',
           'show_count'          => 0,
           'show_option_all'     => '',
           'show_option_none'    => __( 'No categories' ),
           'style'               => 'list',
           'taxonomy'            => 'expert_gyan_category',
           'title_li'            => __( 'Categories' ),
           'use_desc_for_title'  => 1,
        ));
    } else if($cone == "home_appliances") {
        $categories = get_categories( array(
             'child_of'            => 0,
             'current_category'    => 0,
             'parent'              => 0,
             'depth'               => 0,
             'echo'                => 1,
             'exclude'             => '',
             'exclude_tree'        => '',
             'feed'                => '',
             'feed_image'          => '',
             'feed_type'           => '',
             'hide_empty'          => 0,
             'hide_title_if_empty' => false,
             'hierarchical'        => true,
             'order'               => 'ASC',
             'orderby'             => 'name',
             'separator'           => '<br />',
             'show_count'          => 0,
             'show_option_all'     => '',
             'show_option_none'    => __( 'No categories' ),
             'style'               => 'list',
             'taxonomy'            => 'explore_gyan_category',
             'title_li'            => __( 'Categories' ),
             'use_desc_for_title'  => 1,
          ));
    } else if($cone == "student_stuff") {
        $categories = get_categories( array(
           'child_of'            => 0,
           'current_category'    => 0,
           'parent'              => 0,
           'depth'               => 0,
           'echo'                => 1,
           'exclude'             => '',
           'exclude_tree'        => '',
           'feed'                => '',
           'feed_image'          => '',
           'feed_type'           => '',
           'hide_empty'          => 0,
           'hide_title_if_empty' => false,
           'hierarchical'        => true,
           'order'               => 'ASC',
           'orderby'             => 'name',
           'separator'           => '<br />',
           'show_count'          => 0,
           'show_option_all'     => '',
           'show_option_none'    => __( 'No categories' ),
           'style'               => 'list',
           'taxonomy'            => 'social_stuff_category',
           'title_li'            => __( 'Categories' ),
           'use_desc_for_title'  => 1,
        ));
    } else if($cone == "blogs") {
        $categories = get_categories( array(
           'child_of'            => 0,
           'current_category'    => 0,
           'parent'              => 0,
           'depth'               => 0,
           'echo'                => 1,
           'exclude'             => '',
           'exclude_tree'        => '',
           'feed'                => '',
           'feed_image'          => '',
           'feed_type'           => '',
           'hide_empty'          => 0,
           'hide_title_if_empty' => false,
           'hierarchical'        => true,
           'order'               => 'ASC',
           'orderby'             => 'name',
           'separator'           => '<br />',
           'show_count'          => 0,
           'show_option_all'     => '',
           'show_option_none'    => __( 'No categories' ),
           'style'               => 'list',
           'taxonomy'            => 'blog_category',
           'title_li'            => __( 'Categories' ),
           'use_desc_for_title'  => 1,
       ));
    }
    $sendRes = " ";
    
    if(!empty($categories))
    {
      $sendRes .= "<option value=' '> Select Category </option>";

        foreach ($categories as $terms) {
          $term_id = $terms->term_id;
          $sendRes .= "<option value='".$term_id."'>".$terms->name."</option>";
        }
        echo json_encode($sendRes);
    } else {
        echo json_encode($sendRes);
    }
}

if(!empty($ctwo)){
    $sendRes = "";
    $parent_cat_ID = $ctwo;
    
    if($cone == "industry") {
        $args = array(
           'hierarchical' => 1,
           'show_option_none' => '',
           'hide_empty' => 0,
           'parent' => $parent_cat_ID,
          'taxonomy' => 'expert_gyan_category'
        );
    } else if($cone == "home_appliances") {
        $args = array(
           'hierarchical' => 1,
           'show_option_none' => '',
           'hide_empty' => 0,
           'parent' => $parent_cat_ID,
          'taxonomy' => 'explore_gyan_category'
        );
    } else if($cone == "student_stuff") {
        $args = array(
           'hierarchical' => 1,
           'show_option_none' => '',
           'hide_empty' => 0,
           'parent' => $parent_cat_ID,
          'taxonomy' => 'social_stuff_category'
        );
    } else if($cone == "blogs") {
        $args = array(
           'hierarchical' => 1,
           'show_option_none' => '',
           'hide_empty' => 0,
           'parent' => $parent_cat_ID,
          'taxonomy' => 'blog_category'
        );
    }
    $subcats = get_categories($args);
    if($subcats) {
        $sendRes .= "<option> Select Sub Category </option>";
        foreach($subcats as $subcat) {
          $sendRes .= "<option value='".$subcat->term_id."'>".$subcat->name."</option>";
        }
        echo $sendRes;
    } else {
        echo json_encode($sendRes);
    }
}

?> 
