<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
global $phpmailer;
if ( ! ( $phpmailer instanceof PHPMailer ) ) {
    require_once ABSPATH . WPINC . '/class-phpmailer.php';
    require_once ABSPATH . WPINC . '/class-smtp.php';
}
$phpmailer = new PHPMailer;
// SMTP configuration
$phpmailer->isSMTP();                    
$phpmailer->Host = 'ssl://smtp.gmail.com';
$phpmailer->SMTPAuth = true;
$phpmailer->Username = 'testmobulous123@gmail.com';
$phpmailer->Password = 'testmobulous123';
$phpmailer->SMTPSecure = 'tls';
$phpmailer->Port = 465;

global $wpdb;

$pemail = $_POST["pemail"];

if(!empty($pemail)) {
      
    $email = $pemail;
    $pass_generate = randomNumberCode();

    $phpmailer->setFrom('info@medukeeda.com', 'Edukeeda');
    // Add a recipient
    $phpmailer->addAddress($pemail);
    // Set email format to HTML
    $phpmailer->isHTML(true);
    // Email subject
    $phpmailer->Subject = 'Edukkeda - Email Verification';

    $mailContent = "Hi ,<br>";
    $mailContent .= "A Verification Code has been created for email address ".$email."<br>";
    $mailContent .= "Verification Code for your account: ". $pass_generate ."<br>";
    $phpmailer->Body    = $mailContent;

    $data = array("email"=>$email, "verify_code"=>$pass_generate);
    $wpdb->insert("wp_verify_email", $data);
    $s = "success";
    
    if(!$phpmailer->send()){
        echo json_encode($s);
    } else{
        $s = "failure";
        echo json_encode($s);
    }
}

function randomNumberCode() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyz1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 4; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
?>
