<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$eid = (int) str_replace('"', '', $_POST['eid']);
$of = str_replace('"', '', $_POST['selectc']);

if(!empty($eid)) {

    $getExamDetails = $wpdb->get_results( 
        $wpdb->prepare( "SELECT id,exam_title,purpose,eligibility,exam_date,exam_img FROM wp_exam_uploads WHERE exam_id = %d AND exams_of = %s", $eid,$of)
    );

    $sendRes = " ";

    if($getExamDetails) {

        foreach($getExamDetails as $getExamDetail) {

            $sendRes .=  '<section class="examlistcolm ">';
            $sendRes .=  '<div class="row">';
            $sendRes .=  '<div class="col-md-2">';
            $sendRes .=  '<div class="elementor-column-wrap elementor-element-populated">';
            $sendRes .=  '<div class="elementor-image">';
            $sendRes .=  '<img width="45" height="45" src="'. $getExamDetail->exam_img.'" class="attachment-full size-full" alt="">';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '<div class="col-md-8">';
            $sendRes .=  '<div class="elementor-column-wrap elementor-element-populated">';
            $sendRes .=  '<div class="elementor-text-editor elementor-clearfix previouspapas">';
            $sendRes .=  '<p><a href="#">'. $getExamDetail->exam_title.'</a>';
            $sendRes .=  '<br><strong>Purpose: </strong> '. $getExamDetail->purpose .' ';
            $sendRes .=  '<br><strong>Eligibility:</strong> '. $getExamDetail->eligibility .' ';
            $sendRes .=  '<br><strong>Exam Date: </strong> '. date("d/m/Y", strtotime($getExamDetail->exam_date)) .' ';
            $sendRes .=  '</p>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '<div class="col-md-2">';
            $sendRes .=  '<div class="elementor-column-wrap elementor-element-populated">';
            $sendRes .=  '<div class="elementor-button-wrapper">';
            $sendRes .=  '<a href="'. site_url() .'/previous-year-papers-listing?action='. $getExamDetail->exam_title .'" class="elementor-button-link elementor-button elementor-size-sm" role="button">';
            $sendRes .=  '<span class="elementor-button-content-wrapper">';
            $sendRes .=  '<span class="elementor-button-text">Download</span>';
            $sendRes .=  '</span>';
            $sendRes .=  '</a>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '</section>';
        }
          echo json_encode($sendRes);
      } else{echo json_encode($sendRes);}
} else {

    $getExamDetails = $wpdb->get_results( 
        $wpdb->prepare( "SELECT id,exam_title,purpose,eligibility,exam_date,exam_img FROM wp_exam_uploads WHERE exams_of = %s", $of)
    );

    $sendRes = " ";

    if($getExamDetails) {

        foreach($getExamDetails as $getExamDetail) {

            $sendRes .=  '<section class="examlistcolm ">';
            $sendRes .=  '<div class="row">';
            $sendRes .=  '<div class="col-md-2">';
            $sendRes .=  '<div class="elementor-column-wrap elementor-element-populated">';
            $sendRes .=  '<div class="elementor-image">';
            $sendRes .=  '<img width="45" height="45" src="'. $getExamDetail->exam_img.'" class="attachment-full size-full" alt="">';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '<div class="col-md-8">';
            $sendRes .=  '<div class="elementor-column-wrap elementor-element-populated">';
            $sendRes .=  '<div class="elementor-text-editor elementor-clearfix previouspapas">';
            $sendRes .=  '<p><a href="#">'. $getExamDetail->exam_title.'</a>';
            $sendRes .=  '<br><strong>Purpose: </strong> '. $getExamDetail->purpose .' ';
            $sendRes .=  '<br><strong>Eligibility:</strong> '. $getExamDetail->eligibility .' ';
            $sendRes .=  '<br><strong>Exam Date: </strong> '. date("d/m/Y", strtotime($getExamDetail->exam_date)) .' ';
            $sendRes .=  '</p>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '<div class="col-md-2">';
            $sendRes .=  '<div class="elementor-column-wrap elementor-element-populated">';
            $sendRes .=  '<div class="elementor-button-wrapper">';
            $sendRes .=  '<a href="'. site_url() .'/previous-year-papers-listing?action='. $getExamDetail->exam_title .'" class="elementor-button-link elementor-button elementor-size-sm" role="button">';
            $sendRes .=  '<span class="elementor-button-content-wrapper">';
            $sendRes .=  '<span class="elementor-button-text">Download</span>';
            $sendRes .=  '</span>';
            $sendRes .=  '</a>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '</div>';
            $sendRes .=  '</section>';
        }
          echo json_encode($sendRes);
      } else{echo json_encode($sendRes);}
}
?> 
