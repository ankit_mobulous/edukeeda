<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$eid = $_POST['eid'];
$sid = $_POST['sid'];
$edid = $_POST['edid'];
$oid = $_POST['oid'];

if(!empty($eid)){

    $results = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM wp_subject_type WHERE exam_id = %d", $eid) 
    );

    $sendRes = " ";
    $sendRes .= "<option value=' '> Select Subject</option>";
    if($results) {
        foreach($results as $result){

          $sendRes .= "<option value='".$result->id."'>".$result->subject_name."</option>";

        }
        echo json_encode($sendRes);
      } else {
          echo json_encode($sendRes);
      }
}

if(!empty($sid)){
  
    $results = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM wp_topic_subject WHERE subject_id = %d", $sid)
    );

    $sendRes = " ";
    $sendRes .= "<option value=' '> Select Topic</option>";
    if($results) {
        foreach($results as $result){

          $sendRes .= "<option value='".$result->id."'>".$result->topic_name."</option>";

        }
        echo json_encode($sendRes);
      } else {
          echo json_encode($sendRes);
      }
}

if(!empty($edid)){
  
    $results = $wpdb->get_results( 
        $wpdb->prepare( "SELECT id, exam_date FROM wp_exam_details WHERE exam_id = %d", $edid)
    );

    $sendRes = " ";
    $sendRes .= "<option value=' '> Select Exam </option>";
    if($results) {
        foreach($results as $result){
            $getExamName = $wpdb->get_results( $wpdb->prepare("SELECT examName FROM wp_exam_type", $result->exam_id));

            $sendRes .= "<option value='".$result->id."'>".$getExamName[0]->examName." - ".$result->exam_date."</option>";

        }
        echo json_encode($sendRes);
      } else {
          echo json_encode($sendRes);
      }
}

if(!empty($oid)){
    echo json_encode($oid);
    $option = explode("-",$oid);
    
    
    if($option[0] == "a") {

        $ans = $option[2];
        echo json_encode($id);
        
        $results = $wpdb->get_results( 
            $wpdb->prepare( "SELECT id FROM wp_sample_paper WHERE id = %d AND optiona=%s", $id, $ans)
        );

        if($results){
            echo json_encode("a");
        }
    } else if($option[0] == "b") {
        $id = (int) $option[1];
        $ans = $option[2];
        $results = $wpdb->get_results( 
            $wpdb->prepare( "SELECT id FROM wp_sample_paper WHERE id = %d AND optiona=%s", $id, $ans)
        );

        if($results){
            echo json_encode($id);
        }
    } else if($option[0] == "c") {
        $id = (int) $option[1];
        $ans = $option[2];
        $results = $wpdb->get_results( 
            $wpdb->prepare( "SELECT id FROM wp_sample_paper WHERE id = %d AND optiona=%s", $id, $ans)
        );

        if($results){
            echo json_encode("c");
        }
    } else if($option[0] == "d") {
        $id = (int) $option[1];
        $ans = $option[2];
        $results = $wpdb->get_results( 
            $wpdb->prepare( "SELECT id FROM wp_sample_paper WHERE id = %d AND optiona=%s", $id, $ans)
        );

        if($results){
            echo json_encode("d");
        }
    }
}


?> 
