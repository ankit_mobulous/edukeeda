<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';
global $wpdb;

$eeid = $_POST['eeid'];

if(!empty($eeid)){

    $results = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM wp_discussion_sub WHERE main_id = %d", $eeid) 
    );
    $sendRes = " ";
    
    if($results) {
      $sendRes .= "<option value=' '> Select Sub Category </option>";
  
        foreach($results as $result){

          $sendRes .= "<option value='".$result->id."'>".$result->sub_name."</option>";

        }
        $sendRes .= "<option value='0'> Complete </option>";    
        echo json_encode($sendRes);
    } else {
        $sendRes .= "<option value='0'> Parent </option>";
        echo json_encode($sendRes);
    }
}

?> 
