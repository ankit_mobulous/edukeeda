<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$cid = $_POST['cid'];
if(!empty($cid)) {

    $sendRes = " ";
    $getStates = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM states WHERE country_id =%d", $cid)
    );

    if($getStates) {
        $data = " ";

        foreach($getStates as $getState) {
            $data .= "<option value='".$getState->id ."'> ".$getState->name."</option>";
        }
    }
    echo json_encode($data);
}

?> 
