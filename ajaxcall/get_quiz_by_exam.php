<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$eid = $_POST['eid'];
$eeid = $_POST['eeid'];

if(!empty($eid)){

    $results = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM wp_premium_quiz WHERE exam_id = %d", $eid) 
    );

    $sendRes = " ";
    $sendRes .= "<option value=' '> Select Quiz</option>";
    
    if($results) {

        foreach($results as $result){

          $sendRes .= "<option value='".$result->id."'>".$result->quiz_title."</option>";

        }
        echo json_encode($sendRes);
      } else {
          echo json_encode($sendRes);
      }
}

if(!empty($eeid)){

    $results = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM wp_premium_exam_subject WHERE exam_id = %d", $eeid) 
    );

    $sendRes = " ";
    $sendRes .= "<option value=' '> Select Subject </option>";
    
    if($results) {

        foreach($results as $result){

          $sendRes .= "<option value='".$result->id."'>".$result->subject."</option>";

        }
        $sendRes .= "<option value='0'> Complete </option>";    
        echo json_encode($sendRes);
      } else {
          echo json_encode($sendRes);
      }
}

?> 
