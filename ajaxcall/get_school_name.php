<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$sname = $_POST['sname'];

if(!empty($sname)) {

    $sendRes = " ";
    $getSchools = $wpdb->get_results( 
        $wpdb->prepare( "SELECT schoolName FROM wp_schools WHERE schoolName LIKE %s", '%'.$sname.'%')
    );

    if($getSchools) {
        $data = " ";

        foreach($getSchools as $getSchool) {
            $data .= "<option value='". $getSchool->schoolName ."'>";
        }
    }
    echo json_encode($data);
}
?> 
