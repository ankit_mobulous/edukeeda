<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$cname = $_POST['cname'];

if(!empty($cname)) {

    $sendRes = " ";
    $getcompanies = $wpdb->get_results( 
        $wpdb->prepare( "SELECT companyName FROM wp_companies WHERE companyName LIKE %s", '%'.$cname.'%')
    );

    if($getcompanies) {
        $data = " ";

        foreach($getcompanies as $getcompanie) {
            $data .= "<option value='".$getcompanie->companyName ."'>";
        }
    }
    echo json_encode($data);
}
?> 
