<?php 
session_start();
/* Template Name: Signup */ ?>


<?php 
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;
   

if ( ! function_exists( 'wp_handle_upload' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
}

$website = "http://example.com";
$userdata = array(
    'user_login' =>  $_POST['e_email'],
    'user_nicename' =>  $_POST['e_name'],
    'original_password' =>$_POST['e_password'],
    'user_email' =>  $_POST['e_email'],
    'display_name' =>  $_POST['e_name'],
    'user_url'   =>  $website,
    'user_pass'  =>  $_POST['e_password']);
 
$user_id = wp_insert_user($userdata);

if($user_id){
 $_SESSION['user_data'] = get_userdata($user_id);

$data =array("user_id"=>$user_id,
			 "mobile"=>$_POST['e_mobile'],	
			 "exam"=>$_POST['e_exam'],
			 "expected_date"=>$_POST['e_date'],
			 "school_name"=>$_POST['e_school_name'],
			 "place"=>$_POST['e_place'],
			 "state"=>$_POST['e_state'],
			 "pincode"=>$_POST['e_pincode'],
			 "amount"=>$_POST['e_amount'],
			 "qualification"=>$_POST['qualification'],
			 "created_at"=>date('Y-m-d'));
$insert_user_data = $wpdb->insert('user_detail',$data); 

}
 
// On success.
if ( ! is_wp_error( $user_id ) ) {

 //    $creds = array();
 //    $creds['user_login'] = $_POST['e_email'];
 //    $creds['user_password'] = $_POST['e_password'];
 //    $creds['remember'] = false;
 //    $user = wp_signon( $creds, false );
	// print_r($user);die;

     $url = site_url()."/student-sign-up/";
     $_SESSION['message']="Your account is going to review please waiting for approval.";
     wp_redirect($url);
     exit();
}


  ?>