<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
global $wpdb;

$pemail = $_POST["email"];
$code = $_POST["code"];

if(!empty($pemail)) {
      
	$email = $pemail;
	
	$getCheck = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM wp_verify_email WHERE email = %s AND verify_code = %s", $email, $code));

	if($getCheck) {
		echo json_encode(true);
	} else {
		echo json_encode(false);
	}
}
?>
