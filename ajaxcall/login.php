<?php 
session_start();
/* Template Name: Login */ ?>


<?php 
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;
   

if ( ! function_exists( 'wp_handle_upload' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
}

$username = $_POST['e_login_email'];
$password = $_POST['e_login_password'];

$creds = array();
$creds['user_login'] = $_POST['e_login_email'];
$creds['user_password'] = $_POST['e_login_password'];
$creds['remember'] = false;
$user = wp_signon( $creds, false );
if(!empty($user->errors['pending_approval']))
	{
	$_SESSION['message'] = "Your account is still pending approval.";
     $url=site_url().'/student-sign-up/';
     wp_redirect($url);
    exit();
	}

if ( is_wp_error($user) )
{
    
    $_SESSION['message'] = "Email or Password is incorrect.Try another!";
    $url=site_url().'/student-sign-up/';
     wp_redirect($url);
    exit();
}
else
{
    $url=site_url().'/student-home-profile/';
    $_SESSION['user_data']=$user;
    wp_redirect($url);
    exit();
}

?>