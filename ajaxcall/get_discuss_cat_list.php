<?php
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

$cone = $_POST["cid"];
$ctwo = $_POST['ccid'];
$cthree = $_POST['sid'];

if(!empty($cone)){
    $sendRes = " ";
    
    $catTwos = $wpdb->get_results( 
      $wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE main_id = %d", $cone));

    if(!empty($catTwos))
    {
      $sendRes .= "<option value=' '> Select Category </option>";

        foreach ($catTwos as $catTwo) {
          $sendRes .= "<option value='".$catTwo->id."'>".$catTwo->sub_name."</option>";
        }
        echo json_encode($sendRes);
    } else {
        echo json_encode($sendRes);
    }
}

if(!empty($ctwo)){
    $sendRes = " ";
    
    $catHouses = $wpdb->get_results( 
      $wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d", $ctwo));

    if(!empty($catHouses))
    {
      $sendRes .= "<option value=' '> Select House </option>";

        foreach ($catHouses as $catHouse) {
          $sendRes .= "<option value='".$catHouse->id."'>".$catHouse->title."</option>";
        }
        echo $sendRes;
    } else {
        echo json_encode($sendRes);
    }
}

if(!empty($cthree)){
    $sendRes = " ";
    
    $catHouses = $wpdb->get_results( 
      $wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d", $cthree));

    if(!empty($catHouses))
    {
      $sendRes .= "<option value=' '> Select House </option>";

        foreach ($catHouses as $catHouse) {
          $sendRes .= "<option value='".$catHouse->id."'>".$catHouse->title."</option>";
        }
        echo $sendRes;
    } else {
        echo json_encode($sendRes);
    }
}

?> 
