<div class="eventnew">
<?php
session_start();
/**
/*
 Template Name: Coal&Lignite
 */
get_header(); 

if(isset($_GET['id']) && isset($_GET['action'])) {
} else{
?>
<script>window.location.href = 'https://edukeeda.com/expertzone/';</script>
<?php
}
?>
<style type="text/css">
.widget-area .widget ul li{padding: 2px 0;}
  .followbtn {
    padding: 3px 23px!important;
    margin-bottom: 13px!important;
    float: right;
  }
 .colh.singlepost .btn-warning {
    color: #fff;
    background-color: #f68e2f;
    border-color: #f68e2f;
}
  .searchform input{
      height: 36px!important;
  }
  div#secondary h3.widget-title {
    float: left;
    width: 100%;
    border-bottom: 1px solid #f68e2f;
}
@media only screen and (max-width: 767px) {
  .leftoption {
    float: left;
    width: 100%;
}
.rightoption{
    float: left;
    width: 100%;
}
  .rightoption p {
    margin: 0px 0px;
    font-size: 20px;
    font-weight: 700;
}  
.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
}
.arteduro {
    float: left;
    width: 100%;

}


}

</style> 
<?php
  if(isset($_POST["follow"])) {
      global $wpdb;
      $catType = $_POST["catType"];
      $catId = $_POST["catId"];
      $userid = $_SESSION["login"]["id"];
      $data = ["user_id"=>$userid, "cat_id"=>$catId, "cat_type"=>$catType];
      $wpdb->insert("wp_follow_cat", $data);
  }
  if(isset($_POST["unfollow"])) {
      global $wpdb;
      $catType = $_POST["catType"];
      $catId = $_POST["catId"];
      $userid = $_SESSION["login"]["id"];
      $wpdb->query($wpdb->prepare("DELETE FROM wp_follow_cat WHERE user_id = %d AND cat_id = %d", $userid, $catId));
  }
?>
   <div class="container">
      <div class="arteduro">
         <div class="row">
            <div class="col-md-8 col-sm-8">
               <div class="colh singlepost">
                <?php
                    global $wpdb;
                    
                    if(isset($_POST["fitercat"])) {
                      $catIdd = $_POST["fitercat"];
                      $catId = $_GET["id"];
                      $getCName = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $catId));
                      
                      $articles = $wpdb->get_results( 
                       $wpdb->prepare( "SELECT COUNT(object_id) as article FROM wp_term_relationships WHERE term_taxonomy_id = %d", $catIdd));
                    } else {
                      $catId = $_GET["id"];  

                      $getCName = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $catId));
                      $articles = $wpdb->get_results( 
                         $wpdb->prepare( "SELECT COUNT(wp_term_relationships.object_id) as article FROM wp_term_relationships INNER JOIN wp_posts ON wp_term_relationships.object_id = wp_posts.ID WHERE wp_term_relationships.term_taxonomy_id = %d AND post_status='publish'", $catId)
                        );
                    }
                ?>
                  <h4><?php echo $getCName[0]->name; ?>  <span>(<?php echo $articles[0]->article; ?> Posts)</span> </h4>
                      <?php
                        if(isset($_SESSION["login"])) {
                          $userId = $_SESSION["login"]["id"];
                          $type = $_GET["action"];
                          $term_id = $_GET["id"];
                          $checkFollow = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_follow_cat WHERE user_id=%d AND cat_id=%d", $userId, $term_id) 
                          );
                          if($checkFollow){
                        ?>
                              <form method="post" action="">
                                <input type="hidden" name="catId" value="<?php echo $term_id;?>">
                                <input type="hidden" name="catType" value="<?php echo $type;?>">
                                <input type="submit" name="unfollow" value="Unfollow" class="btn btn-warning followbtn">
                              </form>
                        <?php
                          } else{
                        ?>
                              <form method="post" action="">
                                <input type="hidden" name="catId" value="<?php echo $term_id;?>">
                                <input type="hidden" name="catType" value="<?php echo $type;?>">
                                <input type="submit" name="follow" value="Follow" class="btn btn-warning followbtn">
                              </form>
                        <?php 
                          }
                        } else {
                      ?>
                          <button type="button" class="btn btn-warning followbtn" data-toggle="modal" data-target="#myModalExpert">Follow</button>
                      <?php   
                        }
                      ?>
                 
               </div>
            </div>
            <div class="col-md-8 col-sm-8">
               <div class="fullwd">
                  <div class="rightoption">
                     <form action="" method="post" name="myFormName">
                     <p>Sort By:</p>
                        <select name="top_filter" id="topsuccessfilter" onchange='this.form.submit()'>
                          <option value=""> Select </option>
                           <option value="recent">Recent</option>
                           <option value="views">Most Views</option>
                        </select>
                     </form>
                  </div>
                  <div class="leftoption">
                    <form class="searchform" action=" " method="post" class="search-form">
                       <input type="text" placeholder="Search.." name="search" class="searchtext">
                       <button type="submit"><i class="fa fa-search"></i></button>
                     </form>
                  </div>
               </div>
                 <div class="col-md-4 customsideedus">
               <div id="secondary" class="widget-area col-md-3" role="complementary">
                  <span style="float:right;"> <a href="<?php echo site_url();?>/expertise/?id=<?php echo $_GET['id']; ?>&action=<?php echo $action;?>"> Clear All </a> </span>
                  <aside id="recent-posts-3" class="widget widget_recent_entries" style="padding-top: 12px;">
                   <button class="accordion">  <h3 class="widget-title">Filter</h3></button>
                <div class="panel">
  

                     <?php
                        $parent_cat_ID = $_GET["id"];
                        if($_GET['action'] == "industry"){
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                              'taxonomy' => 'expert_gyan_category'
                           );
                        } else if($_GET['action'] == "home_appliances"){
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                              'taxonomy' => 'explore_gyan_category'
                           );
                        } else if($_GET['action'] == "student_stuff"){
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                              'taxonomy' => 'social_stuff_category'
                           );
                        } else if($_GET['action'] == "blogs"){
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                              'taxonomy' => 'blog_category'
                           );
                        }

                        $subcats = get_categories($args);
                        //var_dump($subcats);
                        echo "<ul>";
                        if($subcats) {
                        foreach($subcats as $subcat) {

                            $term_id = $subcat->term_id;
                            $getListCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id));

                      ?>
                                <li>
                                    <form method="post" action="" id="form-id"> 
                                      <a href="<?php echo site_url(); ?>/expertise?id=<?php echo $_GET['id']; ?>&action=<?php echo $_GET['action'];?>&action1=<?php echo $term_id; ?>" onclick="document.getElementById('form-id').submit();"> <?php echo $subcat->name; ?>  ( <?php echo $getListCount[0]->countPost; ?> ) </a>
                                      <input type="hidden" name="fitercat" value="<?php echo $subcat->term_id;?>">
                                    </form>
                                </li>
                            <?php
                        }
                        } else{
                    ?>
                        <li> General </li>
                    <?php
                        }
                        echo "</ul>";
                     ?></div>
                  </aside>

                 
               </div>
            </div>
               <?php

                  if(isset($_POST["search"])) {

                     global $wpdb;
                     $searchFilter = $_POST["search"];
                     if(isset($_GET['action1'])){
                        $catId = $_GET['action1'];
                     } else{
                        $catId = $_GET["id"];
                     }
                     $action = $_GET["action"];

                     $results = $wpdb->get_results( 
                         $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id = %d ORDER BY object_id DESC", $catId) 
                     );

                     foreach($results as $result) {
                       $results1 = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d AND post_status='publish' AND post_type=%s", $result->object_id,$action) 
                       );
                       
                       foreach($results1 as $result1) {
                         
                         if(stripos($result1->post_title, $searchFilter) !== false) {
                         
                            $getCatName = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]) 
                            );
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                            if($image){
                              $image = $image[0];
                          } else {
                              $mkey = "_thumbnail_id";
                            $checkimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                            );
                            $getimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                            );
                            $image = $getimg[0]->guid;  
                          }
                            $table_name = "wp_post_views";
                            $checkViews = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
                            );
                            $author_name = get_the_author_meta( 'display_name', $result1->post_author );

                            $getAuthor = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                            $author_name = $getAuthor[0]->display_name;

                            $countPostTitle = strlen($result1->post_title);
                            if($countPostTitle >= 80) {
                              $postTitle = substr($result1->post_title,0, 80);
                              $postTitle .= "...";
                            } else {
                              $postTitle = $result1->post_title;
                            }
                ?>
                           <div class="col-md-12 blo">
                              <div class="">
                                 <div class="col-md-4 col-sm-4">
                                     <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">  <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                 </div>
                              </div>
                              <div class="col-md-8 col-sm-8">
                                  <p class="categtp"><a href="<?php echo site_url();?>/expertise/?id=<?php echo $_GET["id"];?>&action=<?php echo $action;?>"><?php echo strtoupper($getCatName[0]->name); ?></a></p>
                                   <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"> <?php echo $postTitle; ?></a>
                                   <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                                   </p>
                                   <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                                   <p class="rightwd"><strong> </strong> <?php echo date('F jS, Y . g:i a',strtotime($result1->post_date)); ?></p>
                              </div>
                           </div>
                     <?php
                           }
                        } 
                      }

                  } else if(isset($_POST["top_filter"])) {
                     
                     if(isset($_GET['action1'])){
                        $catId = $_GET['action1'];
                     } else{
                        $catId = $_GET["id"];
                     }
                     
                     $action = $_GET["action"];
                     $filter = $_POST["top_filter"];
                     echo "<div style='clear:both;'></div>";
                     global $wpdb;

                     if($filter == "views") {
                        echo "<div><p>Sort by: Most Views</p></div>";
                        $results = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_term_relationships LEFT JOIN wp_post_views ON wp_term_relationships.object_id = wp_post_views.post_id WHERE wp_term_relationships.term_taxonomy_id = %d ORDER BY wp_post_views.view_count DESC", $catId));


                       foreach($results as $result) {
                          $results1 = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d AND post_status='publish' AND post_type=%s", $result->object_id, $action));
                       
                          foreach($results1 as $result1) {
                      
                             $getCatName = $wpdb->get_results( 
                                 $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]));

                             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                              if($image){
                                  $image = $image[0];
                              } else {
                                  $mkey = "_thumbnail_id";
                                $checkimg = $wpdb->get_results( 
                                      $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                                );
                                $getimg = $wpdb->get_results( 
                                      $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                                );
                                $image = $getimg[0]->guid;  
                              }

                             $table_name = "wp_post_views";
                              $checkViews = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
                              );
                              $author_name = get_the_author_meta( 'display_name', $result1->post_author );

                              $getAuthor = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                              $author_name = $getAuthor[0]->display_name;

                              $countPostTitle = strlen($result1->post_title);
                              if($countPostTitle >= 80) {
                                $postTitle = substr($result1->post_title,0, 80);
                                $postTitle .= "...";
                              } else {
                                $postTitle = $result1->post_title;
                              }               
                ?>
                      <div class="col-md-12 blo">
                          <div class="">
                             <div class="col-md-4 col-sm-4">
                                <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">  <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                             </div>
                          </div>
                          <div class="col-md-8 col-sm-8">
                             <p class="categtp"><a href="<?php echo site_url();?>/expertise/?id=<?php echo $_GET["id"];?>&action=<?php echo $action;?>"><?php echo strtoupper($getCatName[0]->name); ?></a></p>
                             <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"> <?php echo $postTitle; ?></a>
                             <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                             </p>
                             <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                             <p class="rightwd"><strong> </strong> <?php echo date('F jS, Y . g:i a',strtotime($result1->post_date)); ?></p>
                          </div>
                       </div>
                <?php 
                        }
                      }

                     } else if($filter == "recent") {
                      echo "<div><p>Sort by: Recent Post</p></div>";
                        $results = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_posts INNER JOIN wp_term_relationships ON wp_term_relationships.object_id = wp_posts.ID WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish' AND wp_posts.post_type=%s ORDER BY wp_posts.post_date DESC", $catId, $action)
                        );

                        foreach($results as $result) {

                    
                         $getCatName = $wpdb->get_results( 
                             $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]));
                         
                         $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result->ID ), 'single-post-thumbnail' );
                          if($image){
                              $image = $image[0];
                          } else {
                              $mkey = "_thumbnail_id";
                            $checkimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result->ID,$mkey) 
                            );
                            $getimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                            );
                            $image = $getimg[0]->guid;  
                          }

                         $table_name = "wp_post_views";
                          $checkViews = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result->ID) 
                          );
                          $author_name = get_the_author_meta( 'display_name', $result->post_author );

                          $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result->post_author));
                          $author_name = $getAuthor[0]->display_name;

                          $countPostTitle = strlen($result->post_title);
                          if($countPostTitle >= 80) {
                            $postTitle = substr($result->post_title,0, 80);
                            $postTitle .= "...";
                          } else {
                            $postTitle = $result->post_title;
                          }               
                ?>
                      <div class="col-md-12 blo">
                          <div class="">
                             <div class="col-md-4 col-sm-4">
                                <a href="<?php echo esc_url( get_permalink($result->ID) ); ?>">  <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                             </div>
                          </div>
                          <div class="col-md-8 col-sm-8">
                             <p class="categtp"><a href="<?php echo site_url();?>/expertise/?id=<?php echo $_GET["id"];?>&action=<?php echo $action;?>"><?php echo strtoupper($getCatName[0]->name); ?></a></p>
                             <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result->ID) ); ?>"> <?php echo $postTitle; ?></a>
                             <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                             </p>
                             <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                             <p class="rightwd"><strong> </strong> <?php echo date('F jS, Y . g:i a',strtotime($result->post_date)); ?></p>
                          </div>
                       </div>
                <?php 
                     }
                     
                    }

                  } else if(isset($_POST["fitercat"])) {
                     if(isset($_GET['action1'])){
                        $catId = $_GET['action1'];
                     } else{
                        $catId = $_GET["id"];
                     }
                     $action = $_GET["action"];

                       global $wpdb;
                       $results = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_posts INNER JOIN wp_term_relationships ON wp_term_relationships.object_id = wp_posts.ID WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish' AND wp_posts.post_type=%s ORDER BY wp_posts.post_date DESC", $catId, $action)

                       );

                       foreach($results as $result) {
                      
                           $getCatName = $wpdb->get_results( 
                               $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]) 
                           );
                           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result->ID ), 'single-post-thumbnail' );
                            if($image){
                                $image = $image[0];
                            } else {
                                $mkey = "_thumbnail_id";
                              $checkimg = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result->ID,$mkey) 
                              );
                              $getimg = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                              );
                              $image = $getimg[0]->guid;  
                            }

                            $table_name = "wp_post_views";
                            $checkViews = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result->ID) 
                            );
                            $author_name = get_the_author_meta( 'display_name', $result->post_author );

                            $getAuthor = $wpdb->get_results( 
                              $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result->post_author));
                            $author_name = $getAuthor[0]->display_name;

                            $countPostTitle = strlen($result->post_title);
                            if($countPostTitle >= 80) {
                              $postTitle = substr($result->post_title,0, 80);
                              $postTitle .= "...";
                            } else {
                              $postTitle = $result->post_title;
                            }
                            ?>
                          <div class="col-md-12 blo">
                              <div class="">
                                 <div class="col-md-4 col-sm-4">
                                    <a href="<?php echo esc_url( get_permalink($result->ID) ); ?>">  <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                 </div>
                              </div>
                              <div class="col-md-8 col-sm-8">

                                 <p class="categtp"><a href="<?php echo site_url();?>/expertise/?id=<?php echo $_GET["id"];?>&action=<?php echo $action;?>"><?php echo strtoupper($getCatName[0]->name); ?></a></p>
                                 <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result->ID) ); ?>"> <?php echo $postTitle; ?></a>
                                 <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                                 </p>
                                 <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                                 <p class="rightwd"><strong> </strong> <?php echo date('F jS, Y . g:i a',strtotime($result->post_date)); ?></p>
                              </div>
                           </div>
                  <?php 
                    }

                  } else{
                     
                     if(isset($_GET['action1'])) {
                        $catId = $_GET['action1'];
                     } else{
                        $catId = $_GET["id"];
                     }
                     $action = $_GET["action"];

                     global $wpdb;
                     $results = $wpdb->get_results( 
                         $wpdb->prepare( "SELECT * FROM wp_posts INNER JOIN wp_term_relationships ON wp_term_relationships.object_id = wp_posts.ID WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish' AND wp_posts.post_type=%s ORDER BY wp_posts.post_date DESC", $catId, $action) 
                     );

                     foreach($results as $result) {

                         $getCatName = $wpdb->get_results( 
                             $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]) 
                         );

                         $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result->ID ), 'single-post-thumbnail' );
                          if($image){
                              $image = $image[0];
                          } else {
                              $mkey = "_thumbnail_id";
                            $checkimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result->ID,$mkey) 
                            );
                            $getimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                            );
                            $image = $getimg[0]->guid;  
                          }

                          $table_name = "wp_post_views";
                          $checkViews = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result->ID) 
                          );

                          $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result->post_author));
                           $author_name = $getAuthor[0]->display_name;

                          $countPostTitle = strlen($result->post_title);
                          if($countPostTitle >= 80) {
                            $postTitle = substr($result->post_title,0, 80);
                            $postTitle .= "...";
                          } else {
                            $postTitle = $result->post_title;
                          }
                ?>
                      <div class="col-md-12 blo">
                          <div class="">
                             <div class="col-md-4 col-sm-4">
                                <a href="<?php echo esc_url( get_permalink($result->ID) ); ?>">  <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                             </div>
                          </div>
                          <div class="col-md-8 col-sm-8">

                             <p class="categtp"><a href="<?php echo site_url();?>/expertise/?id=<?php echo $_GET["id"];?>&action=<?php echo $action;?>"><?php echo strtoupper($getCatName[0]->name); ?></a></p>
                             <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result->ID) ); ?>"> <?php echo $postTitle; ?></a>
                             <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                             </p>
                             <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                             <p class="rightwd"><strong> </strong> <?php echo date('F jS, Y . g:i a',strtotime($result->post_date)); ?></p>
                          </div>
                       </div>
                <?php 
                    }
                 }
                ?>
            </div>
            <div class="col-md-4 customsideedu">
               <div id="secondary" class="widget-area col-md-3" role="complementary">
                  <span style="float:right;"> <a href="<?php echo site_url();?>/expertise/?id=<?php echo $_GET['id']; ?>&action=<?php echo $action;?>"> Clear All </a> </span>
                  <aside id="recent-posts-3d" class="widget widget_recent_entries" style="padding-top: 12px;">
                   <button class="accordion">  <h3 class="widget-title">Filter</h3></button>
                <div class="panel">
  

                     <?php
                        $parent_cat_ID = $_GET["id"];
                        if($_GET['action'] == "industry"){
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                              'taxonomy' => 'expert_gyan_category'
                           );
                        } else if($_GET['action'] == "home_appliances"){
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                              'taxonomy' => 'explore_gyan_category'
                           );
                        } else if($_GET['action'] == "student_stuff"){
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                              'taxonomy' => 'social_stuff_category'
                           );
                        } else if($_GET['action'] == "blogs"){
                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                              'taxonomy' => 'blog_category'
                           );
                        }

                        $subcats = get_categories($args);
                        //var_dump($subcats);
                        echo "<ul>";
                        if($subcats) {
                        foreach($subcats as $subcat) {

                            $term_id = $subcat->term_id;
                            $getListCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id));

                      ?>
                                <li>
                                    <form method="post" action="" id="form-id"> 
                                      <a href="<?php echo site_url(); ?>/expertise?id=<?php echo $_GET['id']; ?>&action=<?php echo $_GET['action'];?>&action1=<?php echo $term_id; ?>" onclick="document.getElementById('form-id').submit();"> <?php echo $subcat->name; ?>  ( <?php echo $getListCount[0]->countPost; ?> ) </a>
                                      <input type="hidden" name="fitercat" value="<?php echo $subcat->term_id;?>">
                                    </form>
                                </li>
                            <?php
                        }
                        } else{
                    ?>
                        <li> General </li>
                    <?php
                        }
                        echo "</ul>";
                     ?></div>
                  </aside>

                  <aside id="recent-posts-3" class="widget widget_recent_entries" style="margin-bottom: 0px;">
                     <h3 class="widget-title">TOP TRENDING POSTS</h3>
                     <?php
                        $typePost = $_GET["action"];
                        $topPosts = $wpdb->get_results( 
                          $wpdb->prepare("SELECT * FROM wp_posts INNER JOIN wp_post_views ON wp_posts.ID = wp_post_views.post_id WHERE wp_posts.post_type = %s AND wp_posts.post_status = 'publish' ORDER BY wp_post_views.view_count DESC LIMIT 5", $typePost)
                        );

                        if(!empty($topPosts))
                        {
                          foreach ($topPosts as $topPost) {
                            $getCatID = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = %d", $topPost->ID) 
                            );
                            $getCatName = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $getCatID[0]->term_taxonomy_id) 
                            );
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $topPost->ID ), 'single-post-thumbnail' );
                            if($image){
                                $image = $image[0];
                            } else {
                                $mkey = "_thumbnail_id";
                              $checkimg = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $topPost->ID,$mkey) 
                              );
                              $getimg = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                              );
                              $image = $getimg[0]->guid;
                            }

                            $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $topPost->post_author));
                           $author_name = $getAuthor[0]->display_name;

                            $countPostTitle1 = strlen($topPost->post_title);
                            if($countPostTitle1 >= 20) {
                              $postTitle1 = substr($topPost->post_title,0, 30);
                              $postTitle1 .= "...";
                            } else {
                              $postTitle1 = $topPost->post_title;
                            }
                   ?>
                         <div class="othermemd">
                            <div class="col-md-5">
                               <div class="simg">
                                  <a href="<?php echo esc_url( get_permalink($topPost->ID) ); ?>"> <img src="<?php echo $image; ?>" class="img-thumbnail"></a>
                               </div>
                            </div>
                            <div class="col-md-7">
                               <div class="pimg">
                                  <p> <strong><a href="<?php echo site_url();?>/expertise/?id=<?php echo $getCatID[0]->term_taxonomy_id;?>&action=<?php echo $action;?>"><?php echo strtoupper($getCatName[0]->name); ?></a></strong></p>
                                  <a href="<?php echo esc_url( get_permalink($topPost->ID) ); ?>">
                                     <span class="authorname"><?php echo !empty($postTitle1)?$postTitle1:""; ?></span>
                                  </a>
                                  <span>By <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $topPost->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?></span>
                               </div>
                            </div>
                         </div>
                    <?php 
                       }
                     }
                    ?>
                     
                  </aside>
               </div>
            </div>
         </div>
      </div>
      <?php while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'content', 'page' ); ?>
      <?php
         // If comments are open or we have at least one comment, load up the comment template
         if ( comments_open() || get_comments_number() ) :
           comments_template();
         endif;
         ?>
      <?php endwhile; // end of the loop. ?>
      </main><!-- #main -->
   </div>
   <!-- #primary -->
   <?php get_footer(); ?>
</div>

<!-- Modal -->
<div id="myModalExpert" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Follow</h4>
      </div>
      <div class="modal-body">
        <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
      </div>
    </div>
  </div>
</div>
<style>
.widget-area .widget{
    padding-top: 0px;
}
#secondary button.accordion {
    background-color: transparent;
    border: none;
    width: 100%;
    text-align: left;
   
    padding:0px;
}
#secondary button.accordion:hover {
    outline: none;
}
.panel {
  display: none;
  background-color: #f4f4f4;
}
.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    margin-top: -37px;
    font-size: 43px;
}

.active:after {
    content: "\2212";
}
   div#secondary h3.widget-title{
   float: left;
   width: 100%;
   }
   .blo a.bloghdaedu {
   font-size: 20px;
   margin: 0;
   padding: 0;
   line-height: 20px;
   text-align: left;
   width: 100%;
   font-weight: 600;
   }
   
   p.leftwd {
   float: left;
   width: 89%;
   }
   a.rightwd.views {
   color: #000;
   font-size: 13px;
   float: right;
   }
   .sid input.search-field {
   width: 103%;
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   padding: 0 16px;
   }
   .fullwd{width:100%; float:left;}
   .leftwd{float:left;}
   .rightwd{float:left;}
   .pimg1 p { 
   margin-bottom: 0px;
   line-height: 18px;
   font-size: 16px;
   margin-top: 10px;
   }
   .othermemd .col-md-5{
   padding:0px;
   } 
   .othermemd .cat1 {
   text-align: center;
   padding: 0px;
   }
   .pimg1 span {
   color: #f68e2f;
   }
   .pimg strong {
   font-weight: 100;
   font-size: 13px;
   }
   .pimg sapn {
   line-height: 16px;}
   .cat1 {
   text-align: center;
   }
   .pimg a {
   line-height: 20px;
   }
   .pimg p {
   margin-bottom: 6px;
   }
   .pimg1 strong {
   font-weight: 500;
   }
  
   #secondary {
   background-color: #f4f4f4;
   width: 100%;
   float:left;
   }
   .blo a.bloghdaedu {
   font-size: 20px;
   margin: 0;
   padding: 0;
   line-height: 20px;
   float: left;
   width: 100%;
   font-weight: 600;
   }
   .cat {
   padding: 0;
   }
   .blo p {
   margin-bottom: 7px;
   font-size: 15px;
   text-align: left;
   }
   .othermemd {
   box-shadow: -1px 0px 4px 0 rgba(0, 0, 0, 0.07);
   float: left;
   width: 100%;
   border: 1px solid #ddd;
   padding: 10px;
   margin-bottom:20px;
   }
   h4 {
   margin: 0 0 17px 0;
   }
   .blo img.img-thumbnail {
   height: 147px;
   }
   .headarea {
   float: left;
   width: 100%;
   }
   .other a {
   float: right;
   }
   .other h4 {
   float: left;
   margin: 0 0 18px 0;
   }
   .profile ul {
   padding: 0;
   list-style-type: none;
   }
   .memb .profile h3{
   color: #828282;
   }
   .profile {
   float: left;
   width: 100%;
   text-align: center;
   background: #efeded;
   padding: 10px;
   }
   .memb .profile p{   text-align: center; float: none;}
   p.categtypeedus {
   font-size: 15px;
   color: #7f7a7a;
   }
   .memb .profile h2 {
   font-size: 21px;
   color: #060658;
   }
   .profile ul li {
   float: left;
   display: block;
   }
   .profile ul li span, .profile ul li p{
   float:left;
   font-size: 15px;
   }
   .memb .profile .fa.fa-eye{
   margin-right: 0px !important;
   }
   .profile ul li span {
   width: 185px;
   }
   .memb {
   text-align:left;
   }
   .memb h2 {
   font-size: 30px;
   padding: 0px;
   margin: 10px 0px 0px 0px;
   }
   .memb1 {
   background-color: #fff;
   border: 1px solid #ddd;
   border-radius: 4px;
   padding: 48px;
   margin-top: 0px;
   float: left;
   width: 100%;
   }
   .memb h3 {
   font-size: 17px;
   margin: 5px;
   }
   .memb p {
   margin: 0 0 0px;
   float: left;
   }
   .memb1 p{
   float: left;
   text-align: center;
   font-size: 23px;
   font-weight: 600;
   }
   .memb1 button{
   float: right;
   }
   .memb spam1{
   font-weight:600;
   } 
   .memb spam{
   font-weight:600;
   }
   .memb .fa.fa-eye {
   display: initial;
   }
   .blo {
   border-radius: 4px;
   background-color: #ffffff;
   box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.07);
   border: solid 1px #ececec;
   margin-bottom: 30px;
   padding: 10px;
   float: left;
   width: 100%;
   }
   .arteduro {
   float: left;
   width: 100%;
   margin: 30px 0;
   }
   .blo2 a {
   float: right;
   }
   
   .rightoption p {
   float: left;
   margin: 0px 29px;
   }
   .rightoption {
   float: right;
   }
   .colh {
   text-align: left;
   }
   .sid {
   float: left;
   }
   .pimg {
   TEXT-ALIGN: LEFT;
   }
   .pimg1 {
   TEXT-ALIGN: LEFT;
   }
.col-md-4.customsideedus {
    display: none;
}

   @media only screen and (max-width: 767px){
   .rightwd{
   float: left;
   }
   .col-md-4.customsideedus {
    display: block;
    padding: 0px;
}
aside#recent-posts-3d {
    display: none;
}
   .leftwd {
   width: 100%;
   }
  .rightoption select {
    background: #fff;
    border: 1px solid #ddd;
    height: 35px;
    line-height: 3px;
    padding: 1px 60px;
    margin: 1px 0px 8px 0px;
    width: 70%;
    float: right;
}
   .rightoption p {
   margin: 0px;
   }
   .searchform button {
    margin: 0px 3px;
}
.searchform input {
    height: 36px!important;
    padding: 4px 20px!important;
}
.single .entry-header, .page .entry-header {
    margin-top: 0px!important;
}
.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
}
   }
</style>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
