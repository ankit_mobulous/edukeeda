<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

    if(isset($_POST['submit'])) {

        $title = trim($_POST["eventname"]);
        
        if($_POST["country"]) {
          $country = $_POST["country"];
          $getCountry = $wpdb->get_results($wpdb->prepare( "SELECT name FROM countries WHERE id = %d", $country));
          $country = $getCountry[0]->name;
        } else{
            $country = "";
        }

        if($_POST["state"]) {
            $state = $_POST["state"];
            $getState = $wpdb->get_results($wpdb->prepare( "SELECT name FROM states WHERE id = %d", $state));
            $state = $getState[0]->name;
        } else{
            $state = "";
        }
        $city = $_POST["city"];
        $zipcode = $_POST["zipcode"];
        $address = $_POST["address"];
        $from = $_POST["from"];
        $to = $_POST["to"];
        // $eventtime = $_POST["eventtime"];
        // $eventto = $_POST["eventto"];
        $eventlink = $_POST["eventlink"];
        if($_POST["hosted"]){
            $hosted = $_POST["hosted"];
        } else{
            $hosted = " ";
        }
        $desc = $_POST["desc"];
$desc = stripslashes($desc);
        $eventCat = $_POST["eventCat"];
        $eventCat1 = $_POST["eventCat1"];

        $location = $address.", ".$city.", ".$state.", ".$country.", ".$zipcode; 
        $userId = $_SESSION["login"]["id"];
        $post_date = date('Y-m-d H:i:s');
        $title = trim($title);
        $temp_post_title = explode(" ", $title);
  
        if(count($temp_post_title > 1)) {
          $temp_post_title = implode("-", $temp_post_title);
          $string = trim(strtolower($temp_post_title));
          $post_name = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        } else {
          $post_name = preg_replace('/[^A-Za-z0-9\-]/', '', $title);
        }

        $data = array(
                "post_author" => $userId,
                "post_date" => $post_date,
                "post_content" => $desc,
                "post_title" => $title,
                "post_status" => "pending",
                "comment_status" => "open",
                "ping_status" => "closed",
                "post_name" => $post_name,
                "post_parent" => 0,
                "menu_order" => 0,
                "post_type" => "event",
                "comment_count" => 0
              );
       
        $result = $wpdb->insert("wp_posts", $data);
        
        if($result) {
          $lastid = $wpdb->insert_id;
          $siteurls = site_url();
          $guid = $siteurls."/?post_type=events&#038;p=$lastid";
          
          $results2 = $wpdb->update("wp_posts", array('guid'=> $guid), array('ID' => $lastid));

          $tablename2 = "wp_term_relationships";
          $data1 = array(
                  'object_id' => $lastid,
                  'term_taxonomy_id' => $eventCat,
                  'term_order' => 0
                );
          $wpdb->insert($tablename2, $data1);

          $data100 = array(
                  'object_id' => $lastid,
                  'term_taxonomy_id' => $eventCat1,
                  'term_order' => 1
                );
          $wpdb->insert($tablename2, $data100);


          $tablename3 = "wp_postmeta";
          //Hosted
          $data4 = array(
                  'post_id' => $lastid,
                  'meta_key' => "hosted_by",
                  'meta_value' => $hosted
                );
          $wpdb->insert($tablename3, $data4);
          $data5 = array(
                  'post_id' => $lastid,
                  'meta_key' => "_hosted_by",
                  'meta_value' => "field_5c0653e84dc3b"
                );
          $wpdb->insert($tablename3, $data5);
          //event time
          // $eeeeee = $eventtime.' '.$_POST["eventtimeee"];
          // $data6 = array(
          //         'post_id' => $lastid,
          //         'meta_key' => "event_time",
          //         'meta_value' => $eeeeee
          //       );
          // $wpdb->insert($tablename3, $data6);
          // $data7 = array(
          //         'post_id' => $lastid,
          //         'meta_key' => "_event_time",
          //         'meta_value' => "field_5c067004b1efe"
          //       );
          // $wpdb->insert($tablename3, $data7);

          // $eeeeee1 = $eventto.' '.$_POST["eventtooo"];
          // $data60 = array(
          //         'post_id' => $lastid,
          //         'meta_key' => "event_time_to",
          //         'meta_value' => $eeeeee1
          //       );
          // $wpdb->insert($tablename3, $data60);
          // $data70 = array(
          //         'post_id' => $lastid,
          //         'meta_key' => "_event_time_to",
          //         'meta_value' => "field_5c6e3ce980998"
          //       );
          // $wpdb->insert($tablename3, $data70);


          //event date start
          $data8 = array(
                  'post_id' => $lastid,
                  'meta_key' => "event_date",
                  'meta_value' => $from
                );
          $wpdb->insert($tablename3, $data8);
          $data9 = array(
                  'post_id' => $lastid,
                  'meta_key' => "_event_date",
                  'meta_value' => "field_5c06701bb1eff"
                );
          $wpdb->insert($tablename3, $data9);
          //event date end
          $data10 = array(
                  'post_id' => $lastid,
                  'meta_key' => "event_date_end",
                  'meta_value' => $to
                );
          $wpdb->insert($tablename3, $data10);
          $data11 = array(
                  'post_id' => $lastid,
                  'meta_key' => "_event_date_end",
                  'meta_value' => "field_5c349cf179a75"
                );
          $wpdb->insert($tablename3, $data11);
          //official authentic link
          $data12 = array(
                  'post_id' => $lastid,
                  'meta_key' => "officialauthentic_link",
                  'meta_value' => $eventlink
                );
          $wpdb->insert($tablename3, $data12);
          $data13 = array(
                  'post_id' => $lastid,
                  'meta_key' => "_officialauthentic_link",
                  'meta_value' => "field_5c067038b1f01"
                );
          $wpdb->insert($tablename3, $data13);
          //event_location
          $data14 = array(
                  'post_id' => $lastid,
                  'meta_key' => "event_location",
                  'meta_value' => $location
                );
          $wpdb->insert($tablename3, $data14);
          $data15 = array(
                  'post_id' => $lastid,
                  'meta_key' => "_event_location",
                  'meta_value' => "field_5c06702db1f00"
                );
          $wpdb->insert($tablename3, $data15);


          if($_FILES["eventpic"]["name"]) {
            $upload_dir  = wp_upload_dir();
            $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

            $extension = pathinfo($_FILES["eventpic"]["name"], PATHINFO_EXTENSION);
            $newfilename = substr(sha1(mt_rand().time()), 0, 11).".".$extension;

            $file = $upload_path . $newfilename;
            $raw_file_name = $_FILES['eventpic']['tmp_name'];
            $raw_file_name1 = $_FILES['eventpic']['name'];
            $temp_img_title = explode(" ", $raw_file_name1);
            
            if(count($temp_img_title > 1)) {
              $temp_img_title = implode("-", $temp_img_title);
              $string1 = trim(strtolower($temp_img_title));
              $post_name1 = preg_replace('/[^A-Za-z0-9\-]/', '', $string1);
            } else {
              $post_name1 = preg_replace('/[^A-Za-z0-9\-]/', '', $raw_file_name1);
            }

            $siteurl = site_url();
            $saveFile = $siteurl.'/wp-content/uploads/'.date('Y/m').'/'. $newfilename;  
            
            if (move_uploaded_file($_FILES['eventpic']['tmp_name'], $file)) {
            } else {
                $saveFile = " ";
            }

              $data16 = array(
                  'post_author' => $userId,
                  'post_date'=> $post_date,
                  'post_title' => $raw_file_name1,
                  'post_status'=> "inherit",
                  'comment_status' => "open",
                  'ping_status' => "closed",
                  'post_name' => $post_name1,
                  'post_parent' => $lastid,
                  "guid" => $saveFile,
                  'menu_order' => 0,
                  'post_type' => "attachment",
                  'post_mime_type' => "image/$extension",
                  'comment_count' => 0,
              );

              $results8 = $wpdb->insert("wp_posts", $data16);
              $lastid1 = $wpdb->insert_id;
              
              $data17 = array(
                  'post_id' => $lastid,
                  'meta_key' => "_thumbnail_id",
                  'meta_value' => $lastid1
                );
              $results9 = $wpdb->insert($tablename3, $data17);

              $_SESSION["postdone"] = "Your Post has been Successfully Submitted"; 
              header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
              exit;

          } else {
            header('Location: https://edukeeda.com/eventdetails/');
            exit;
          }
        } else{
              header('Location: https://edukeeda.com/eventdetails/');
              exit;
        }
    }
?>
