<div class="homepagemenedu discussionback">
   <?php
      /*
      Template Name: mydiscusion
      */
      get_header(); ?>
   <?php
      $path = $_SERVER['DOCUMENT_ROOT'];
      include_once $path . '/wp-config.php';
      include_once $path . '/wp-load.php';
      include_once $path . '/wp-includes/wp-db.php';
      include_once $path . '/wp-includes/pluggable.php';
      
      global $wpdb;
  ?>
  <?php
      if(isset($_POST['joinhousesubmit'])) {
        $houseId = $_POST["hid"];
        $userid = $_SESSION["login"]["id"];
        $data = ["joinId"=>$houseId, "user_id"=>$userid];
        $wpdb->insert("wp_discussion_join", $data);
  ?>
         <script type="text/javascript">
            if ( window.history.replaceState ) {
                window.history.replaceState( null, null, window.location.href );
            }
         </script>
  <?php
      }
      if(isset($_POST['unjoinhousesubmit'])) {
        $houseId = $_POST["hid"];
        $userid = $_SESSION["login"]["id"];
        $wpdb->query($wpdb->prepare("DELETE FROM wp_discussion_join WHERE user_id = %d AND joinId=%d", $userid, $houseId));
?>
        <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
        </script>
<?php
    }
?>
   <style>
      .repostmodal {
      border: 1px solid #C0C0C0;
      padding: 10px;
      }
      .memberpage {
      float: left;
      width: 100%;
      margin-top: 37px;
      }
      .imgforr {
      text-align: center;
      }
      .imgforr img.img-thumbnail {
      width: 60px;
      height: 60!important;;
      border-radius: 100%;
      }
      .post span {
      margin-right: 6px;
      }
      .plugin {
      float: right;
      color:#9a9a9a;
      }
      .post .dotsseprate {
      margin: 0 6px;
      }
      #articleblous .text1 a{
      color:#000;
      font-size: 18px;
      }
      #articleblous .post
      {
      color:#9a9a9a;
      }
      #articleblous .post strong
      {
      color:#9a9a9a;
      }
      #articleblous .share
      {
      color:#9a9a9a;
      }
      #articleblous .share1{
      color:#9a9a9a;
      }
      #articleblous .share2{
      color:#9a9a9a;
      }
      #articleblous .share .fa{
      color:#9a9a9a;
      }
      #articleblous .share1 .fa{
      color:#9a9a9a;
      }
      #articleblous .share2 .fa{
      color:#9a9a9a;
      }
      #articleblous .share1 button{
      color:#9a9a9a;
      }
      #articleblous .plugin{
      color:#9a9a9a;
      }
      .text2 span {
      margin: 0px 5px;
      }
      #wallposts .text2 span {
      margin: 0px 5px;
      color:#f68e2f;
      }
      .text2 p {
      display: initial;
      }
      .questio p.rigwd
      {
      color:#9a9a9a;
      }.direct a {
      color: #9a9a9a!important;
      }
      .plug button {
      background: none;
      border: none;
      color: #9a9a9a;
      margin: 0px;
      padding: 0px;
      float: left;
      }
      .like {
      color: #9a9a9a;
      }
      .pluging{
      color: #9a9a9a;
      }
      .arteduro {
      margin: 25px 0px!important;
      }
      .pp {
      float: left;
      width: 100%;
      display: -webkit-box;
      margin: 10px 0px;
      }
      #anwerspat .text a{
      color:#000;
      font-size: 18px;
      }
      #anwerspat .post
      {
      color:#9a9a9a;
      }
      #anwerspat .post strong
      {
      color:#9a9a9a;
      }
      #anwerspat .share
      {
      color:#9a9a9a;
      }
      #anwerspat .share1{
      color:#9a9a9a;
      }
      #anwerspat .share2{
      color:#9a9a9a;
      }
      #anwerspat .share .fa{
      color:#9a9a9a;
      }
      #anwerspat .share1 .fa{
      color:#9a9a9a;
      }
      #anwerspat .share2 .fa{
      color:#9a9a9a;
      }
      #anwerspat .share1 button{
      color:#9a9a9a;
      }
      #anwerspat .plugin{
      color:#9a9a9a;
      }
      #anwerspat .pp strong{
      color:#9a9a9a;
      }
      #anwerspat .pp{
      color:#9a9a9a;
      }
      .pp span {
      margin-right: 10px;
      color:#9a9a9a;
      }
      #questforyou .pp {
      margin-bottom: 45px;
      color: #9a9a9a;
      }
      #questforyou .pp strong {
      color: #9a9a9a;
      }
      #questforyou .plugin {
      float: right;
      color: #9a9a9a;
      }
      #questforyou .share, .share2 p {
      color: #9a9a9a;
      }
      .pp a {
      background: #f68e2f;
      padding: 10px;
      margin: 15px;
      color: #fff;
      border-radius: 4px;
      }
      .pp strong {
      margin-right: 10px;
      color: #9a9a9a;
      }
      .catp strong {
      color: #060658;
      font-weight: 600;
      }
      .catp span {
      margin: 5px;
      color: #4c4242;
      }
      .text {
      float: left;
      width: 100%;
      margin-bottom: 15px;
      margin-top: 15px;
      }
      .text p {
      font-weight: 800;
      color: #4c4242;
      line-height: 30px;
      font-size: 29px!important;
      }
      .share .fa {
      color: #9a9a9a;
      }
      .share1 .fa {
      color: #9a9a9a;
      }
      .share2 .fa {
      color: #9a9a9a;
      }
      .dot {
      float: right;
      }
      .questio {
      float: left;
      width: 100%;
      margin-bottom: 20px;
      }
      .dot span.countno {
      margin: 0px;
      float: left;
      }
      .dot ul.dropdown-menu {
      min-width: 217px;
      padding: 3px;
      }
      .modal-body p {
      margin-bottom: 0px;
      padding: 3px;
      }
      .post{
      float: left;
      width: 100%;
      display: -webkit-box;
      margin-bottom: 20px;
      color: #9a9a9a;
      }
      #articleblous2 .share
      {
      color:#9a9a9a;
      }
      #articleblous2 .share1
      {
      color:#9a9a9a;
      }
      #articleblous2 .share2
      {
      color:#9a9a9a;
      }
      .text1 {
      float: left;
      width: 100%;
      margin-bottom: 15px;
      }
      .share1 button {
      color: #9a9a9a;
      font-size: 14px;
      text-transform: capitalize;
      background: transparent;
      border: none;
      padding: 0 6px;
      }
      .share1 img {
      width: 15px;
      height: auto;
      }  
      .text1 p {
      font-weight: 800;
      color: #f68e2f!important;
      line-height: 23px;
      font-size: 21px!important;
      }
      .text1 h2 {
      font-size: 20px;
      color: #f68e2f;
      }
      .house button {
      font-size: 12px;
      margin: 0px 0px 0px 30px;
      padding: 5px;
      color: #000;
      font-weight: 600;
      }
      #wallposts .share2 p {
      color: #060658;
      }
     .like span {
    margin: 0px 3px;
}
      .house {
      float: left;
      width: 100%;
      }
      .plug img {
      width: 20px;
      }
      .plug {
      text-align:right;}
      .cat a {
      color: #000000bd;
      font-weight: 600;
      }.catp a {
      color: #060658;
      font-weight: 600;
      }
      .cat span {
      color: #f68e2f;
      margin: 7px;
      }
      .plug {
      text-align: right;
      margin-top: 8px;
      }
      .cat {
      margin: 6px 0px;
      }
      .headdiscus h1 {
      font-size: 25px;
      text-align:left;
      }
      .arteduro{
      margin: 0px;
      }
      .direct p {
      color: #9a9a9a;
      }
      .accordion {
      background-color: #eee;
      color: #444;
      cursor: pointer;
      padding: 18px;
      width: 100%;
      border: none;
      text-align: left;
      outline: none;
      font-size: 15px;
      transition: 0.4s;
      }
      .active, .accordion:hover {
      background-color: #ccc; 
      }
      .headarea .active, .accordion:hover {
      background-color: transparent;
      }
      .active:after {
      content: "\2212";
      }
      .accordion:after { 
      content: ' \002B';
      font-size: 21px;
      margin: 1px;
      padding: 0px;
      float: right;
      }
      .panel {
      padding: 0 18px;
      display: none;
      background-color: white;
      overflow: hidden;
      }
      .headarea button.accordion {
      background: none;
      border: none;
      color: #616161;
      margin: 0px;
      padding: 0px;
      font-weight: 500;
      text-transform: capitalize;
      }
      .thumbprofile img {
      width: 50px;
      border: 1px solid #ddd;
      padding: 2px;
      height: 50px;
      border-radius: 100%;
      }
      .thumbprofile {
      float: left;
      width: 60px;
      }
      .areadetails {
      float: left;
      width: 75%;
      }
      .areadetails p {
      line-height: 17px;
      }
      .areadetails h5 {
      margin: 0;
      }
      p.joineon {
      text-transform: capitalize;
      font-size: 13px;
      margin: 4px 0 0 0;
      float: left;
      }
      .profileares {
      float: left;
      width: 100%;
      border: 1px solid #ddd;
      padding: 5px;
      margin-bottom: 20px;
      }
      .reportingsets ul li {
      float: left;
      width: 100%;
      text-align: left;
      }
      .reportingsets ul {
      list-style-type: circle;
      }
      .reportingsets ul li span {
      float: left;
      }
      .reportingsets {
      width: 100%;
      border-bottom: 1px solid #ddd;
      padding: 20px 0;
      margin: 0 0 15px 0;
      }
      .reportingsets .modal-footer{
      border-top:none;
      }
      .bottom-btns {
      padding: 0 21px;
      text-align: right;
      }
      .repolinks label {
      color: #797977;
      cursor: pointer;
      }
      .repolinks label:hover {
      color: #de7514;
      }
      .reportingsets input[type="radio"] {
      margin: 0 8px 0 0;
      }
      .groupname p{margin:0;}
      .postpanels ul li:first-child {
      border-bottom: 1px solid #ddd;
      }
      .filtertab {
      float: left;
      width: 50%;
      text-align: center;
      padding: 14px 0;
      background:#fff;
      }
      .filtertab span {
      font-size: 12px;
      }
      .filtertabans {
      float: left;
      width: 50%;
      text-align: center;
      padding: 14px 0;
      background:#fff;
      color:#9a9a9a;
      }
      .filtertabans span {
      font-size: 12px;
      }
      .filtertabhouse {
      float: left;
      width: 50%;
      text-align: center;
      padding: 14px 0; 
      background: #fff;
      color: #9a9a9a;
      }
      .filtertabhouse span {
      font-size: 12px;
      }
      .spacetp {
      margin: 5px 0;
      }
      .addposituon.bottomspc p a{color:#616161!important;}
      .spacetp .accordion a{color:#616161!important;}
      .share {
      float: left;
      width: 100%;
      }
      p.catp {
      width: 100%;
      float: left;
      }
      p.rigwd {
      width: 100%;
      float: left;
      }
      .headdiscus {
      background: #fff;
      padding: 10;
      margin-bottom: -11px;
      border: 1px solid #9a9a9a;
      }
      @media only screen and (max-width: 767px){
      .memberpage {
      margin-top: 0px!important;
      }
      .post span {
      margin-right: 2px;
      }
      .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
}
      .page-wrap
      {
          padding:0px;
      }
      .memberpage h4 {
      font-size: 25px!important;
      }
      .memberpage .advtspace {
      height: 256px;
      float: left;
      width: 100%;
      }
      .headdiscus {
      float: left;
      width: 100%;
      }
      .share1,.share2{
      width:100%;
      float:left;
      text-align: center;
      }
      .share1 img {
      width: 24px;
      height: auto;
      }
      .text1 p a {
      font-weight: 800;
      color: #4c4242;
      line-height: 21px;
      font-size: 16px!important;
      }
      .post .dotsseprate {
      margin: 0 2px;
      }
      .post p{
      font-size: 12px!important;
      }
      .plugin {
      float: right;
      margin-top: -43px;
      }
      .memberpage .advtspace {
      float: left;
      width: 100%;
      }
      .dot ul.dropdown-menu {
      min-width: 160px;
      padding: 3px;
      width: 100%;
      margin-left: -126px;
      }
      .blogaresnews .dot ul.dropdown-menu {
      margin-left: 0px;
      }
      .text p {
      font-weight: 800;
      color: #4c4242;
      line-height: 25px;
      font-size: 20px!important;
      }
      .text2 {
      float: left;
      width: 100%;
      }
      }
      .share.like button {
      padding: 0px;
      background: #fff;
      color: #9a9a9a;
      border: 0px;
      }
      .headdiscus h1{margin-bottom: 50px;}
      div#DiscussionHouses .post p span {
    margin: 0px 5px;
}
   </style>
   <div class="memberpage">
      <div class="row">
         <div class="col-md-9">
            <div class="col-xs-12 col-md-4">
               <div class="headarea">
                  <div class="other">
                     <h4>Discussion Group</h4>
                  </div>
                  <div class="headarea">
                     <div class="addposituon bottomspc">
                        <p class="tablinks1"><a href="http://edukeeda.com/opinion-poll/">Opinion Poll</a> <span class="countno"></span></p>
                        <?php
                           $getSidebarCats = $wpdb->get_results("SELECT * FROM wp_discussion_main");
                           
                           foreach($getSidebarCats as $getSidebarCat) {
                              if($getSidebarCat->have_sub == 0) {
                            ?>
                        <p class=""><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>"><?php echo $getSidebarCat->name;?> <span class="countno"></span></a></p>
                        <?php
                           } else {
                           ?>
                        <div class="spacetp">
                           <button class="accordion"><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>"><?php echo $getSidebarCat->name;?></a></button>
                           <div class="panel">
                              <?php
                                 $getSubCats = $wpdb->get_results( 
                                     $wpdb->prepare( "SELECT * FROM wp_discussion_sub WHERE main_id = %d",$getSidebarCat->id) 
                                 );
                                 foreach($getSubCats as $getSubCat) {
                                 ?>
                              <p class="tablinks"><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>&actions=<?php echo $getSubCat->id; ?>"><?php echo $getSubCat->sub_name;?><span class="countno"></span></a></p>
                              <?php
                                 }
                                 ?>
                           </div>
                        </div>
                        <?php
                           }
                           }
                           ?>
                        <?php
                           if(isset($_SESSION["login"]["id"])) {
                           ?>
                        <p class="tablinks"><a href="<?php echo site_url();?>/peoples-wallposts"> People's Wallposts </a><span class="countno"></span></p>
                        <p class="tablinks"><a href="<?php echo site_url();?>/my-discussion-house">My Discussion Houses</a><span class="countno"></span></p>
                        <p class="tablinks"><a href="<?php echo site_url();?>/my-discussion-group">My Groups</a><span class="countno"></span></p>
                        <?php
                           }
                           ?>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-8">
               <div class="headdiscus">
                  <h1>My Discussion Houses</h1>
               </div>
               <div class="arteduro">
                  <div class="row">
                     <div class="col-md-12 col-sm-12 basictabsed">


                        <div id="DiscussionHouses" class="tabcontent" style="display:block;">
                           <div class="blogaresnews">
                              <?php
                                $userId1 = $_SESSION["login"]["id"];
                                $housePosts = $wpdb->get_results($wpdb->prepare("SELECT wp_discussion_house.id,wp_discussion_house.cat_id,wp_discussion_house.subcat_id, wp_discussion_house.title, wp_discussion_house.house_desc,wp_discussion_join.created_at FROM wp_discussion_house INNER JOIN wp_discussion_join ON wp_discussion_join.joinId = wp_discussion_house.id WHERE wp_discussion_join.user_id = %d", $userId1));
                                 
                                if($housePosts) {
                                    $z=1;
                                foreach($housePosts as $housePost) {
                                    if($housePost->subcat_id == 0) {
                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $housePost->cat_id));
                                        $cName1 = $catName[0]->name;
                                    } else{
                                        $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $housePost->cat_id));

                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $housePost->subcat_id));
                                        $cName1 = $catName[0]->sub_name;
                                    }

                                    $quesPostCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as quesCount FROM wp_discussion_ques WHERE house_id=%d ORDER BY id DESC", $housePost->id));

                                    $joinHouseCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as hCount FROM wp_discussion_join WHERE joinId=%d ORDER BY id DESC", $housePost->id));
                                  
                                    $timeAnsGet = time_elapsed_string($housePost->created_at);

                                    $table_name = "wp_discuss_house_views";
                                    $getViews = $wpdb->get_results($wpdb->prepare( "SELECT * FROM $table_name WHERE house_id = %d", $housePost->id));
                                 ?>
                                      <div class="rap firstpostdefult" >
                                      <div class="direct">
                                         <div class="col-md-10 col-sm-10 col-xs-10">

                                            <p> <?php if($housePost->subcat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$housePost->cat_id."'>".$catMainName[0]->name .'</a> . ';}?> <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $housePost->cat_id;?><?php if($housePost2->subcat_id == 0){}else { echo "&actions=". $housePost2->subcat_id;}?>"><?php echo $cName1; ?></a> </p>
                                         </div>
                                      </div>

                                      <div class="col-md-9">
                                         <div class="text1">
                                            <p><a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost->id;?>"><?php echo $housePost->title;?></a></p>
                                         </div>
                                      </div>
                                      <div class="col-md-3">
                                        <div class="cutjion">
                                          <?php
                                            if(isset($_SESSION["login"])) {
                                              $userId = $_SESSION["login"]["id"];
                                              $checkJoin = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_discussion_join WHERE user_id=%d AND joinid=%d", $userId, $housePost->id));
                                              
                                              if($checkJoin) {
                                          ?>
                                                <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost->id;?>">Joined</a>
                                          <?php
                                              } else{
                                          ?>
                                                <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost->id;?>">Join House</a>
                                          <?php
                                              }
                                            } else{
                                          ?>
                                                <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost->id;?>">Join House</a>
                                          <?php
                                            }
                                          ?>
                                        </div>
                                      </div>
                                      <div class="join">
                                         <div class="col-xs-12 col-md-9">
                                            <div class="like">
                                               <p><span><?php echo $quesPostCount[0]->quesCount; ?></span><strong> Posts </strong></p>
                                               <div class="dotsseprate">.</div>
                                               <!--<p data-toggle="modal" data-target="#myModalhousepeoplelist1<?php echo $z;?>"><span> <?php echo $joinHouseCount[0]->hCount; ?> </span><strong> People </strong></p>
                                               <div class="dotsseprate">.</div>
                                               <p><span><?php if($getViews){ echo $getViews[0]->view_count; }else{ echo "0";} ?></span><strong> Views </strong></p>
                                               <div class="dotsseprate">.</div>-->
                                               <p><span><?php echo $timeAnsGet;?></p>
                                            </div>

                                                  <!---- Modal  --->
                                                  <div id="myModalhousepeoplelist1<?php echo $z;?>" class="modal fade" role="dialog">
                                                  <div class="modal-dialog" style="width: 700px;">
                                                     <!-- Modal content-->
                                                     <div class="modal-content">
                                                        <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                           <h3></h3>
                                                        </div>
                                                        <div class="modal-body">
                                                           <div class="row">
                                                              <div clss="col-md-12">
                                                              <?php
                                                                  $opJoins = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_join WHERE joinId=%d", $housePost->id));
                                                                  
                                                                  if($opJoins) {
                                                                      foreach($opJoins as $opJoin) {
                                                                          $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $opJoin->user_id));
                                                                  
                                                                          $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $opJoin->user_id));
                                                                  
                                                                          $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $opJoin->user_id));

                                                              ?>   
                                                                 <div class="col-md-6">
                                                                  <div class="poll">
                                                                     <div class="col-md-3 col-sm-2 col-xs-3">
                                                                        <div class="imgforr">
                                                                           <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                        </div>
                                                                     </div>
                                                                     <div class="col-md-9 col-sm-10 col-xs-9">
                                                                        <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                        <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                              <?php
                                                                      }
                                                                  }
                                                              ?>
                                                              </div>
                                                           </div>
                                                        </div>
                                                     </div>
                                                  </div>
                                                  </div>

                                         </div>
                                         <div class="col-md-3 col-xs-12">
                                            <div class="pluging">
                                               <div class="dropdown customs">      
                                                  <button class="dropbtn">
                                                    <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                                                  </button>
                                                  <div class="dropdown-content">
                                                    <?php //$urlshare = "http://edukeeda.com/discussion-house/?action=".$housePost->id;
                                                      //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                                    ?>
                                                      <?php //echo do_shortcode("$urlsharing"); ?>
                                                  </div>
                                                </div>
                                            </div>
                                         </div>
                                      </div>
                                   </div>
                              <?php
                                    $z++;
                                  }
                                }
                              ?>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- #main -->
               </div>
            </div>
         </div>
         <div class="col-md-3">
            <div class="advtspace" style=""></div>
         </div>
         <!-- #primary -->
      </div>
   </div>
   <?php get_footer(); ?>
   <script>
      var acc = document.getElementsByClassName("accordion");
      var i;
      
      for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
          this.classList.toggle("active");
          var panel = this.nextElementSibling;
          if (panel.style.display === "block") {
            panel.style.display = "none";
          } else {
            panel.style.display = "block";
          }
        });
      }
   </script>
</div>
<?php
   function time_elapsed_string($datetime, $full = false) {
       $now = new DateTime;
       $ago = new DateTime($datetime);
       $diff = $now->diff($ago);
   
       $diff->w = floor($diff->d / 7);
       $diff->d -= $diff->w * 7;
   
       $string = array(
           'y' => 'year',
           'm' => 'month',
           'w' => 'week',
           'd' => 'day',
           'h' => 'hour',
           'i' => 'minute',
           's' => 'second',
       );
       foreach ($string as $k => &$v) {
           if ($diff->$k) {
               $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
           } else {
               unset($string[$k]);
           }
       }
   
       if (!$full) $string = array_slice($string, 0, 1);
       return $string ? implode(', ', $string) . ' ago' : 'just now';
   }
   ?>
<script> 
   function openCity(evt, cityName) {
       var i, tabcontent, tablinks;
       tabcontent = document.getElementsByClassName("tabcontent");
       for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
       }
       tablinks = document.getElementsByClassName("tablinks");
       for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace(" active", "");
       }
       document.getElementById(cityName).style.display = "block";
       evt.currentTarget.className += " active";
   
       if(cityName == "articleblous") {
          document.getElementById("filtertabans").style.display = "none";
          document.getElementById("filtertabques").style.display = "block";
          document.getElementById("filtertabhouse").style.display = "none";
       } else if(cityName == "anwerspat") {
          document.getElementById("filtertabans").style.display = "block";
          document.getElementById("filtertabques").style.display = "none";
          document.getElementById("filtertabhouse").style.display = "none";
       } else if(cityName == "dischouses") {
          document.getElementById("filtertabans").style.display = "none";
          document.getElementById("filtertabques").style.display = "none";
          document.getElementById("filtertabhouse").style.display = "block";
       }
   }
   
   function openCity1(evt, cityName) {
       var i, tabcontent, tablinks;
       tabcontent = document.getElementsByClassName("tabcontent");
       for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
       }
       tablinks = document.getElementsByClassName("tablinks");
       for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace(" active", "");
       }
       document.getElementById(cityName).style.display = "block";
       evt.currentTarget.className += " active";
   }
   
   function openReport(evt, reportName) {
     var i, repocontent, repolinks;
     repocontent = document.getElementsByClassName("repocontent");
     for (i = 0; i < repocontent.length; i++) {
       repocontent[i].style.display = "none";
     }
     repolinks = document.getElementsByClassName("repolinks");
     for (i = 0; i < repolinks.length; i++) {
       repolinks[i].className = repolinks[i].className.replace(" active", "");
     }
     document.getElementById(reportName).style.display = "block";
     evt.currentTarget.className += " active";
   }
</script>
<div id="myModalmemb1" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h1>Way to post your question</h1>
         </div>
         <div class="modal-body">
            <ul>
               <li>
                  <a href="#">Click on the appropriate <b>discussion</b> group from the list given in <b>Discussion Forum home page</b>.</a>
               </li>
               </br>
               <li><a href="#">Go to the list of <b>discussion houses</b> of desired group & choose appropriate discussion house for your question.</a></li>
               </br>
               <li><a href="#">  Join <b>discussion house</b> & post your question.</a></li>
            </ul>
         </div>
      </div>
   </div>
</div>
<div id="myModalpostany" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3>Way to post your question</h3>
         </div>
         <div class="modal-body">
            <p> Please login to start any discussion. <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>

