<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Sydney
 */

get_header(); 

$layout = sydney_blog_layout();

?>

	<?php do_action('sydney_before_content'); ?>

	<div id="primary" class="content-area col-md-9">

		<?php //sydney_yoast_seo_breadcrumbs(); ?>

		<main id="main" class="post-wrap" role="main">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
			'<h3 class="archive-title"><?php echo $title = single_cat_title( '', false ); ?></h3>
				
			</header><!-- .page-header -->

			<div class="entry-content">
			<?php echo do_shortcode('[recent_post_slider design="design-2"]
'); ?>
			<div class="posts-layout">
				
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					if ( $layout != 'classic-alt' ) {
						get_template_part( 'content', get_post_format() );
					} else {
						get_template_part( 'content', 'classic-alt' );
					}
				?>

			<?php endwhile; ?>
			</div>
			</div>
</article><!-- #post-## -->
			
		<?php
			the_posts_pagination( array(
				'mid_size'  => 1,
			) );
		?>	

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php do_action('sydney_after_content'); ?>


<?php get_sidebar(); ?>
<?php get_footer(); ?>