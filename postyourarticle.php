<?php
ob_clean(); ob_start();
   /*
   Template Name: Post Article Form
   */
   get_header();
?>
<?php
   session_start();

   if(isset($_SESSION["login"])){
      $url = "https://edukeeda.com/welcome-user/";
      wp_redirect( $url );
      exit;
   }
?>

<div id="primary" class="content-area">
   <div class="row signpagsecss">
      <div class="col-md-6 signupcontainer">
      <?php 
        if(isset($_SESSION["registersuccess"])){
      ?>
           <p> <?php echo $_SESSION["registersuccess"];?> </p>
      <?php
          unset($_SESSION["registersuccess"]);
        } else if(isset($_SESSION["registerfailed"])) {
      ?>
          <p> <?php echo $_SESSION["registerfailed"];?> </p>
      <?php
          unset($_SESSION["registerfailed"]);
        }
      ?>
         <h5>New users? Sign up & become an eduKeeda Member.</h5>
         <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/register-code.php" method="post" id="postarticle">
            <p>Name (required)<br>
               <input type="text" placeholder="name" name="p_name" id="p_name">
            </p>
            <p>Mobile (required)<br>
               <input type="text" placeholder="Mobile Number" name="p_mobile" id="p_mobile">
            </p>
            <p>Email (required)<br>
               <input type="email" placeholder="Email" name="p_email" id="p_email" class="emailp"><button type="button" class="brn btn-warning" id="getcode">Get Code</button>
            </p>
            <p>Verification Code (required)<br>
               <input type="text" placeholder="Code" name="p_code" id="p_code">
               <span id="verifyStatus"></span>
            </p>
            <p>Password(required)<br>
               <input type="password" name="p_password" id="p_password" placeholder="Password">
            </p>
            <p> Confirm Password (required)<br>
               <input type="password" name="p_c_password" id="e_c_password" placeholder="Confirm Password">
            </p>
            <input type="submit" value="Register" name="submit" disabled="disabled" class="verifybutton">
         </form>
      </div>
      <div class="col-md-6 signupcontainer">
         <h5>Login to explore more</h5>
        <?php 
          if(isset($_SESSION["failed"])){
        ?>
              <p style="color: red" class="text-center"> <?php echo $_SESSION["failed"];?> </p>
        <?php
          }
        ?>
         <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/login-code.php?page=<?php echo $_GET["page"]; ?>" method="post" id="postlogin">
            <p>Email (required)<br>
               <input type="email" placeholder="Email" name="p_email" id="p_email1"> 
            </p>
            <p>Password(required)<br>
               <input type="password" name="p_password" id="p_password1" placeholder="Password">
            </p>
            <a href="<?php echo site_url(); ?>/forgot-password/"> Forgot Password </a><BR><BR>
            <input type="hidden" name="login_Sbumit">
            <input type="hidden" name="redirect_to" value="">
            <input type="submit" value="Login" name="submit">
         </form>
      </div>
   </div>
   <style>
      header.entry-header {
      display: none;
      }
      .signpagecss input{
      height: 26px;
      margin: 0 0 10px 0;
      background: transparent;
      border: none;
      border-bottom: 1px solid #ddd;
      width: 100%;
      }
      .signpagecss p {
      margin: 0;
      font-size:14px;
      }
      .signpagecss input{
      padding: 5px 20px;
      }
      .signupcontainer {
      background: #fff;
      box-shadow: #ded8d5 0px 0px 8px 0px;
      padding: 23px;
      margin: 0 18px;
      width: 45%;
      height: 100%;
      float: left;
      }
      div#content {
      background: url(https://edukeeda.com/wp-content/uploads/2018/10/edukeeda-web.png);
      padding: 20px;
      }
      .container.content-wrapper {
      background: rgba(255,255,255,0.9);
      
      border: 1px solid #4848;
      }
      .signupcontainer label{
      float: left;
      width: 100%;
      }
      .signupcontainer select{
      width: 100%;
      float: left;
      display: block;
      background: transparent;
      height: 30px;
      margin-bottom: 10px;
      }
      .signupcontainer input[type="date"] {
      width: 100%;
      background: #fff;
      height: 30px !important;
      }
      button#getcode {
        margin: 10px 0px;
        }
      @media only screen and (max-width: 767px){
      .signupcontainer {
      margin: 15px 0px;
      width: 100%;
      height: 100%;
      }
      .row.signpagsecss {
       padding: 0px 10px;
       }
      div#content {
       margin-top: 20px;
      }
      
      }
      
       
      .signupcontainer input[type="text"], .signupcontainer input[type="password"], .signupcontainer input[type="email"]{
      width: 100%;
      background: #fff;
      height: 32px;
      }
   </style>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
   <script type="text/javascript">
      jQuery(document).ready(function($){
      
          jQuery.validator.addMethod("phoneno", function(phone_number, element) {
              phone_number = phone_number.replace(/\s+/g, "");
              return this.optional(element) || phone_number.length > 9 && 
              phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
          }, "<br />Please specify a valid phone number");
      
          $.validator.addMethod(
            "regex",
            function(value, element, regexp) {
             var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please enter valid input."
          ); 
         
            $.validator.addMethod("zipCodeValidation", function() {
               var zipCode = $('input#e_pincode').val();
               return (/(^[1-9][0-9]{5}$)/).test(zipCode); // returns boolean
            }, "Please enter a valid pincode");
      
      
      // validate signup form on keyup and submit
            $("#postlogin").validate({ 
               rules: {
                     p_email: {
                     required: true,
                     email: true,
                     remote: {
                        url:"<?php echo site_url(); ?>/check-email",
                        type: "post",
      
                     },
                  },
                  p_password: {
                     required: true,
                     minlength: 5
                  },
               },
               messages: {
                  p_email: {required:"Please enter a valid email address",remote:"Email id is not registered.Try another!"},
                  p_password: {
                     required: "Please provide a password",
                     minlength: "Your password must be at least 5 characters long"
                  },
               },
            });
      
            // validate signup form on keyup and submit
            $("#postarticle").validate({
               rules: {
                  p_name:{required:true,minlength:3,maxlength:30,lettersonly: true},
                  p_password: {
                     required: true,
                     pwcheck: true,
                     minlength: 6
                  },
                  p_c_password: {
                     required: true,
                     minlength: 6, 
                     equalTo: "#p_password"
                  },
                  p_email: {
                     required: true,
                     email: true,
                          remote: {
                              url:"<?php echo site_url(); ?>/check-email-post-signup",
                              type: "post",
                          },
                  },
                  p_code:{required:true},
                  p_mobile:{required: true,number: true,minlength:8,maxlength:10},
               },
               messages: {
                  p_name:{required:"Please enter your name",regex:"Please Enter valid name.",minlength: "Your name must be at least 3 characters long",
                    },
                  p_password: {
                     required: "Please provide a password",
                     minlength: "Your password must be at least 5 characters long"
                  },
                  p_c_password: {
                     required: "Please provide a confirm password",
                     minlength: "Your password must be at least 5 characters long",
                     equalTo: "Please enter the same password as above"
                  },
                  p_email: {required:"Please enter a valid email address",remote:"Email id is already registered.Try another!"},
                  p_code: {required:"Please enter a valid Code"},
                  p_mobile:{required:"Please enter mobile no",maxlength:"Please enter no more than {0} number.",
                          minlength:"Please enter at least {0} number."},                  
               }
            });
            jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
            }, "Only alphabetical characters");
            $.validator.addMethod("pwcheck", function(value) {
               return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
                   && /[a-z]/.test(value) // has a lowercase letter
                   && /[A-Z]/.test(value) // has a lowercase letter
                   && /\d/.test(value) // has a digit
            }, "password must contain a digit, lowercase letter, uppercase letter");
        });
   </script>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
<script type="text/javascript">
$("#getcode").on('click',function(e){
   var pemail =  $("#p_email").val();
   
   if(pemail) {
    $.ajax({
          type: "POST",
          dataType: "json",
          url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/getcode.php",
          data:{pemail:pemail},
          success : function(data) {           
              //$("#resCode").html("Please Check Your Email for Enter Verification code");
              alert("Please Check Your Email for Enter Verification code");
          },
          error: function(jqXHR, textStatus, errorThrown){
              console.log(textStatus, errorThrown);
          }
      });
    }
});

$("#p_code").on('keyup',function(e){
   var pemail =  $(".emailp").val();
    var code =  $(this).val();

   if(pemail) {
    $.ajax({
          type: "POST",
          dataType: "json",
          url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/checkverifycode.php",
          data:{email:pemail,code:code},
          success : function(data) {
              console.log(data);
              if(data == true) {
                  $("#verifyStatus").html("");
                  $(".verifybutton").prop("disabled", false);
              } else if(data == false){
                $("#verifyStatus").html("Verification Code is not valid");
                $(".verifybutton").prop("disabled", true);
              }
          },
          error: function(jqXHR, textStatus, errorThrown){
              console.log(textStatus, errorThrown);
          }
      });
    }
});
</script>
