

<div class="eventnew">
<?php
   /**
   /*
   Template Name:aboutus
   */
   get_header();
   ?>

   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php endwhile; // end of the loop. ?>
   <?php get_footer(); ?>

<style>
   .pageheading h1:before {
   content: "";
   width: 120px;
   height: 9px;
   background: #060658;
   position: absolute;
   bottom: -8px;
   left: 0;
   }
   .pageheading h1:after {
   content: "";
   width: 26px;
   height: 9px;
   background: #f68e2f;
   position: absolute;
   bottom: -8px;
   left: 0;
   z-index: 1;
   }
   .vision h2:after {
   content: "";
   width: 26px;
   height: 9px;
   background: #f68e2f;
   position: absolute;
   bottom: -5px;
   z-index: 1;
   left: 46%;
   }
   .vision h2:before {
   content: "";
   width: 99px;
   height: 9px;
   background: #060658;
   position: absolute;
   bottom: -5px;
   left: 46%;
   }
   p.space {
   height: 200px;
   }
   .visionp {
   float: left;
   width: 100%;
   text-align: center;
   margin-bottom: 50px;
   }
   .visionp p {
   font-size: 20px;
   margin-bottom: 20px;
   }
   .visionp a {
   background: #f68e2f;
   padding: 5px 10px;
   border-radius: 5px;
   color: #fff;
   }
   .vision {
   position: relative;
   text-align: center;
   }
   .entry-content a {
   background: #f68e2f;
   padding: 5px 5px;
   border-radius: 5px;
   color: #fff;
   }
   .entry-content a:hover {
   background: #fff;
   padding: 5px 5px;
   border-radius: 5px;
   color: #f68e2f;
   border: 1px solid #f68e2f;
   }
   .entry-content ul li {
   font-size: 20px;
   }
   .pageheading sapn {
   color: #f68e2f;
   }
   .pageheading {
   position: relative;
   margin: 57px 0px;
   }
   .right {
   float: left;
   width: 100%;
   margin-top: 163px;
   margin-bottom: 63px;
   }
   .right img {
   width: 100%;
   max-height: 337px;
   height: 310px;
   }
   .kefts {
   margin-top: 91px;
   margin-bottom: 40px;
   }
   .kefts img {
   max-height: 200px;
   height: 200px;
   }
   .catchybga{
   padding: 13% 0;
   background: url(/img/image-education.png);
   background-size: cover;
   background-position: 100%;
   height: 400px;
   margin-bottom: 45px;
   position: relative;
   }
   .entry-content {
   margin-bottom: 50px;
   margin-top: 40px;
   }
   .entry-content p {
   margin-bottom: 0px;
    font-weight: 400;
    font-size: 26px!important;
    text-align: center;
   }
   .entry-content h2 {
   margin-top: 30px;
   }
   .want{
   position: absolute;
   top: 0;
   width: 100%;
   height: 100%;
   display: table;
   text-align: center;
   }
   .content {
   display: table-cell;
   vertical-align: middle;
   }
   .content span {
   background: rgba(255, 255, 255, 0.5) none repeat scroll 0 0;
   color: #000;
   display: block;
   font-size: 45px;
   margin: 0 auto;
   padding: 90px 0;
   text-transform: uppercase;
   width: 550px;
   }
   .eventnew .content-wrapper.container > .row {
   margin: 0;
   background:#f5f5f5;
   }
   .eventnew .content-wrapper.container {
   max-width: 100%;
   width: 100%;
   padding: 0 !important;
   }
   .page-wrap {
   padding-top: 75px;
   }
   @media only screen and (max-width: 767px)
   {
   .about .row {
   width: 100%;
   }
   .content span
   {
   width: 100%;
   }
   .content span
   {
   padding: 35px 0;
   }
   p.space {
   display: none;
   }
   .page-wrap {
   padding: 5px!important;
   }
   .entry-content ul li {
   font-size: 15px;
   }
   .vision h2:after {
   content: "";
   width: 26px;
   height: 6px;
   background: #f68e2f;
   position: absolute;
   bottom: -5px;
   z-index: 1;
   left: 37%;
   }
   .vision h2:before{content: "";
   width: 71px;
   height: 6px;
   background: #060658;
   position: absolute;
   bottom: -5px;
   left: 37%;
   }
   .visionp {
   float: left;
   width: 100%;
   margin-bottom: 40px;
   }
   .catchybga {
   margin-top: 0px;
   }
   .entry-content {
   margin-bottom: 10px;
   margin-top: 30px;
   }
   .entry-content p {
    margin-bottom: 17px;
    font-weight: 400;
    font-size: 17px!important;
    line-height: 25px;
    text-align: center!important;
}
 .right {
   float: left;
   width: 100%;
   margin-top: 30px;
   margin-bottom: 63px;
   }
   }
</style>


