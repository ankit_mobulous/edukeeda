<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
global $phpmailer;
if ( ! ( $phpmailer instanceof PHPMailer ) ) {
    require_once ABSPATH . WPINC . '/class-phpmailer.php';
    require_once ABSPATH . WPINC . '/class-smtp.php';
}
$phpmailer = new PHPMailer;
// SMTP configuration
$phpmailer->isSMTP();                    
$phpmailer->Host = 'ssl://smtp.gmail.com';
$phpmailer->SMTPAuth = true;
$phpmailer->Username = 'testmobulous123@gmail.com';
$phpmailer->Password = 'testmobulous123';
$phpmailer->SMTPSecure = 'tls';
$phpmailer->Port = 465;

global $wpdb;

     if(isset($_POST['submit'])) {

      $creds = array();
      $email = $_POST["p_email"]; 
      $user = get_user_by( 'email', $email );

      if($user) {
          $firstname = $user->first_name;
          $email = $user->user_email;
          $pass_generate = randomPasswordString();
          $user_login = $user->user_login;
          

         $phpmailer->setFrom('info@medukeeda.com', 'Edukeeda');

         // Add a recipient
         $phpmailer->addAddress($email);

         // Set email format to HTML
         $phpmailer->isHTML(true);

         // Email subject
         $phpmailer->Subject = 'Edukkeda - Forget password';

         // Email body content
         if ($firstname == "") {
             //$mailContent = "<h1>Send HTML Email using SMTP in WordPress</h1>
               
             $firstname = " ";
             $mailContent = "Hi ".$firstname.",<br>";
             $mailContent .= "A new password has been created on ".get_bloginfo( 'name' )." for email address ".$email."<br>";
             $mailContent .= "Password for your account: ". $pass_generate ."<br>";
         } else{
             $mailContent = "Hi ".$firstname.",<br>";
             $mailContent .= "A new password has been created on ".get_bloginfo( 'name' )." for email address ".$email."<br>";
             $mailContent .= "Password for your account: ". $pass_generate ."<br>";
         }

         $phpmailer->Body = $mailContent;

         if(!$phpmailer->send()){
             echo 'Message could not be sent.';
             echo 'Mailer Error: ' . $phpmailer->ErrorInfo;
         }else{
             echo 'Message has been sent';
             wp_set_password( $pass_generate, $user->ID );
         }

          $success = "New Password has been sent in your mailid";
          $_SESSION["success"] = $success;
          header('Location: http://edukeeda.com/forgot-password/');
      } else{
          $wrongCred = "Email ID is incorrect";
          $_SESSION["failed"] = $wrongCred;
          header('Location: http://edukeeda.com/forgot-password/');
      }
   }

   function randomPasswordString() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

?>
