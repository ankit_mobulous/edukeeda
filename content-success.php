<?php
/**
 * @package Sydney
 */
?>
<style>
.postshare1 p {
    text-align: center;
    margin-bottom: 0px;
}
.simg {
    float: left;
    width: 100%;
}
.othermemd {
    float: left;
    width: 100%;
  margin-bottom: 15px;
    border-bottom: 1px solid#ffaf8c;
}
.simg a {
    float: left;
    width: 100%;
}
.pimg {
    float: left;
    width: 100%;
}
.pimg p {
    margin-bottom: 0px!important;
}
.pimg span {
    font-size: 15px!important;
    line-height: 0px!important;
}
.postshare span {
    margin: 0px 76px 0px 6px;
  display: -webkit-box;
}
.postshare {
    float: left;
    width: 100%;
    display: -webkit-box;
      margin: 0px 16px;
}

.pimg a {
    line-height: 15px;
    float: left;
    width: 100%;
    margin: 4px 0px;
}
.postshare .btn-primary {
    color: #f68e2f;
    background-color: transparent;
    border: none;
  padding: 0px
}
.postshare .btn-primary:hover {
    color: #fff;
    background-color: #f68e2f;
    border-color: #f68e2f;
}
.postshare .pshare{margin-left: 70px;}
.postshare p{
  margin-bottom: 4px!important;
}
.postshare1 p {
    text-align: center;
}
.postshare1 .heateor_sss_sharing_container{margin-left: 20px;}
</style>
<?php
    $bid = get_the_ID();
    global $wpdb;
    $table_name = "wp_post_views";
    $checkViews = $wpdb->get_results( 
          $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $bid) 
    );
    if($checkViews){
        $valView = $checkViews[0]->view_count + 1;
        $wpdb->update($table_name, array('view_count'=>$valView), array('post_id'=>$bid));
    } else {
        $data = array("post_id" =>$bid,"view_count"=>1);
        $wpdb->insert($table_name, $data);
    }

    if(isset($_POST["bookpost"])) {
        $post_id = $_POST["postId"];
        $userId = $_SESSION["login"]["id"];
        $checkBooks = $wpdb->get_results( 
          $wpdb->prepare( "SELECT * FROM wp_bookmaark_post WHERE post_id = %d", $post_id, $userId));
        
        if($checkBooks) {
        } else {
            $dataBook = array("post_id" =>$post_id,"user_id"=>$userId);
            $wpdb->insert("wp_bookmaark_post", $dataBook);
        }
    }
    if(isset($_POST["unbookpost"])) {
        $post_id = $_POST["postId"];
        $userId = $_SESSION["login"]["id"];
        $wpdb->query($wpdb->prepare("DELETE FROM wp_bookmaark_post WHERE post_id = %d AND user_id=%d", $post_id, $userId));
    }

?>
   <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
         <header class="entry-header">
            <div class="meta-post">
                <span> <a href="<?php site_url();?>/success-story/">Success Story </a></span>
            </div>
            <?php the_title( '<h1 class="title-post entry-title">', '</h1>' ); ?>
            <div class="single-meta" style="width: 98%;">
                  <span class="byline">
                     <span class="author vcard"> <?php the_author(); ?></span> | <span class="author vcard space"> <?php the_time('F jS, Y'); ?> . <?php the_time('g:i a'); ?></span>
                  </span>
                  <span style="float:right;"> </span>
            </div>
            <!-- .entry-meta -->
         </header>
         <!-- .entry-header -->
         <!--<div class="entry-content">
            </div>-->
         <?php
            $pid = get_the_ID();
         ?>
         <div class="row">
            <div class="col-md-12">
                <hr style="border:1px solid #000;">
            </div>
        </div>
        <div class="row" style="margin-bottom: 30px;">
          <div class="col-md-3 col-xs-6 postshare1">
            <p><strong>Views</strong><span></p>
             <p><span>
              <?php
                $views = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_post_views WHERE post_id = %d", $pid) 
                );
              ?>
                <?php if($views){echo $views[0]->view_count;}else{ echo "0";} ?></span>
            </p>
          </div>

         
          <div class="col-md-3 col-xs-6 postshare1">
            <p class="pshare"><strong> Bookmark</strong> </p>
            <?php
              if(isset($_SESSION["login"])) {
                $userId = $_SESSION["login"]["id"];
                $post_id = $pid;
                $checkBook = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_bookmaark_post WHERE user_id=%d AND post_id=%d", $userId, $post_id) 
                );
                if($checkBook){
              ?>
                  <form method="post" action="">
                    <input type="hidden" name="postId" value="<?php echo $pid;?>">
                    <button type="submit" name="unbookpost" style="background-color: white;border: 0px;padding: 0px;">
                      <img src="<?php echo site_url();?>/wp-content/uploads/2019/02/bookmarked.png" style="width: 50px;height: 38px;">
                    </button>
                </form>
            <?php
                } else {
            ?>
                    <form method="post" action="">
                        <input type="hidden" name="postId" value="<?php echo $pid;?>">
                        <button type="submit" name="bookpost" style="background-color: white;border: 0px;padding: 0px;">
                          <img src="<?php echo site_url();?>/wp-content/uploads/2019/02/bookmark-outline.png" style="width: 50px;height: 38px;">
                        </button>
                    </form>
            <?php
                }
              } else {
            ?>
                  <p>
                      <button type="button" data-toggle="modal" data-target="#myModalBookmark" style="background-color: white;border: 0px;padding: 0px;"><img src="<?php echo site_url();?>/wp-content/uploads/2019/02/bookmark-outline.png" style="width: 50px;height: 38px;"></button>
                  </p>
            <?php
              }
            ?>
          </div>
           <div class="col-md-6 col-xs-12 postshare1">
            <p class="pshare"><strong> Share</strong> </p>
            <p><?php echo do_shortcode('[Sassy_Social_Share type="standard"]') ?> </p>
          </div>
    </div>

         <div class="row">
            <div class="col-md-12 italicfontdesc">
               <?php echo get_field( "description", $pid); ?>
            </div>   
         </div>

         <div class="entry-thumb">
            <div class="col-md-12">
                <?php if ( has_post_thumbnail() && ( get_theme_mod( 'post_feat_image' ) != 1 ) ) : ?>
                 <div class="entry-thumb">
                     <?php the_post_thumbnail('large-thumb'); ?>
                 </div>
             <?php endif; ?>
            </div>
         </div>

         <div style="clear: both;"></div>
         <div class="entry-content">
            <?php the_content(); ?>
            
            

            <?php
               wp_link_pages( array(
                   'before' => '<div class="page-links">' . __( 'Pages:', 'sydney' ),
                   'after'  => '</div>',
               ) );
           ?>
         </div>
         <!-- .entry-content -->
         <footer class="entry-footer">
            <?php sydney_entry_footer(); ?>
         </footer>
         <!-- .entry-footer -->
    </article>
<!-- Modal -->
<div id="myModalBookmark" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Follow</h4>
      </div>
      <div class="modal-body">
        <p> For Bookmark, Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
      </div>
    </div>
  </div>
</div>
