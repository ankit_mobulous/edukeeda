<div class="">
<?php
/**
/*
 Template Name: Letest Articles
 */
get_header(); ?>
   <div class="container">
      <div class="arteduro">
         <div class="row">
            <div class="col-md-8 col-sm-8">
               <div class="colh">
                  <h4>Latest Posts</h4>
               </div>
            </div>
            <div class="col-md-8 col-sm-8">
               <div class="">
                  <div class="rightoption">
                     <form action="" method="post" name="myFormName">
                     <p>Sort By:</p>
                        <select name="top_filter" id="topsuccessfilter" onchange='this.form.submit()'>
                          <option value=""> Select </option>
                           <option value="recent">Recent</option>
                           <option value="views">Most Views</option>
                        </select>
                     </form>
                  </div>
                  <div class="sid">
                     <form class="searchform" action=" " method="post" class="search-form">
                       <input type="text" placeholder="Search.." name="search" class="searchtext">
                       <button type="submit"><i class="fa fa-search"></i></button>
                     </form>
                  </div>
               </div>
                <div class="col-md-4 customsideedus">
               <div id="secondary" class="widget-area col-md-3" role="complementary">
                  <aside id="recent-posts-3" class="widget widget_recent_entries latestsidebar">
                  <button class="accordion"> <h3 class="widget-title">Explore By Industry / Business</h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">   
                     <?php
                       $categories = get_categories( array(
                           'child_of'            => 0,
                           'current_category'    => 0,
                           'parent'              => 0,
                           'depth'               => 0,
                           'echo'                => 1,
                           'exclude'             => '',
                           'exclude_tree'        => '',
                           'feed'                => '',
                           'feed_image'          => '',
                           'feed_type'           => '',
                           'hide_empty'          => 0,
                           'hide_title_if_empty' => false,
                           'hierarchical'        => true,
                           'order'               => 'ASC',
                           'orderby'             => 'name',
                           'separator'           => '<br />',
                           'show_count'          => 0,
                           'show_option_all'     => '',
                           'show_option_none'    => __( 'No categories' ),
                           'style'               => 'list',
                           'taxonomy'            => 'expert_gyan_category',
                           'title_li'            => __( 'Categories' ),
                           'use_desc_for_title'  => 1,
                       ) );
                        echo "<ul class='catLatest'>";
                        if(!empty($categories))
                        {
                            foreach ($categories as $terms) {
                            
                            $term_id = $terms->term_id;
                            $image   = category_image_src( array('term_id'=>$term_id) , false );
                            $category_link = get_category_link( $term_id );
                            $getListCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id));
                        ?>
                            <li><a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id; ?>&action=industry"> <?php echo $terms->name; ?> ( <?php echo $getListCount[0]->countPost; ?> )</a></li>
                        <?php 
                            }
                          }
                        ?>
                        </ul></div>
                  </aside>
                  
                  <aside id="recent-posts-3" class="widget widget_recent_entries latestsidebar">
                     <button class="accordion"> <h3 class="widget-title">College Stuff & Career </h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">   
                     <?php
                       $categories = get_categories( array(
                           'child_of'            => 0,
                           'current_category'    => 0,
                           'parent'              => 0,
                           'depth'               => 0,
                           'echo'                => 1,
                           'exclude'             => '',
                           'exclude_tree'        => '',
                           'feed'                => '',
                           'feed_image'          => '',
                           'feed_type'           => '',
                           'hide_empty'          => 0,
                           'hide_title_if_empty' => false,
                           'hierarchical'        => true,
                           'order'               => 'ASC',
                           'orderby'             => 'name',
                           'separator'           => '<br />',
                           'show_count'          => 0,
                           'show_option_all'     => '',
                           'show_option_none'    => __( 'No categories' ),
                           'style'               => 'list',
                           'taxonomy'            => 'social_stuff_category',
                           'title_li'            => __( 'Categories' ),
                           'use_desc_for_title'  => 1,
                       ) );
                        echo "<ul class='catLatest'>";
                        if(!empty($categories))
                        {
                            foreach ($categories as $terms) {
                            
                            $term_id2 = $terms->term_id;
                            $image   = category_image_src( array('term_id'=>$term_id2) , false );
                            $category_link = get_category_link( $term_id2 );
                            $getListCount2 = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id2));
                        ?>
                            <li><a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id2; ?>&action=student_stuff"> <?php echo $terms->name; ?> ( <?php echo $getListCount2[0]->countPost; ?> )</a></li>
                        <?php 
                            }
                          }
                        ?>
                        </ul></div>
                  </aside>
                  <aside id="recent-posts-3" class="widget widget_recent_entries latestsidebar">
                   <button class="accordion"> <h3 class="widget-title">Blogs & Opinion</h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">     
                     <?php
                       $categories = get_categories( array(
                           'child_of'            => 0,
                           'current_category'    => 0,
                           'parent'              => 0,
                           'depth'               => 0,
                           'echo'                => 1,
                           'exclude'             => '',
                           'exclude_tree'        => '',
                           'feed'                => '',
                           'feed_image'          => '',
                           'feed_type'           => '',
                           'hide_empty'          => 0,
                           'hide_title_if_empty' => false,
                           'hierarchical'        => true,
                           'order'               => 'ASC',
                           'orderby'             => 'name',
                           'separator'           => '<br />',
                           'show_count'          => 0,
                           'show_option_all'     => '',
                           'show_option_none'    => __( 'No categories' ),
                           'style'               => 'list',
                           'taxonomy'            => 'blog_category',
                           'title_li'            => __( 'Categories' ),
                           'use_desc_for_title'  => 1,
                       ) );
                        echo "<ul class='catLatest'>";
                        if(!empty($categories))
                        {
                            foreach ($categories as $terms) {
                            
                            $term_id3 = $terms->term_id;
                            $image   = category_image_src( array('term_id'=>$term_id3) , false );
                            $category_link = get_category_link( $term_id3 );
                            $getListCount3 = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id3));
                        ?>
                            <li><a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id3; ?>&action=blogs"> <?php echo $terms->name; ?> ( <?php echo $getListCount3[0]->countPost; ?> )</a></li>
                        <?php 
                            }
                          }
                        ?>
                        </ul></div>
                  </aside>

                  <aside id="recent-posts-3" class="widget widget_recent_entries latestsidebar">
                    <button class="accordion">  <h3 class="widget-title">Home Appliances </h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">  
                     <?php
                       $categories = get_categories( array(
                           'child_of'            => 0,
                           'current_category'    => 0,
                           'parent'              => 0,
                           'depth'               => 0,
                           'echo'                => 1,
                           'exclude'             => '',
                           'exclude_tree'        => '',
                           'feed'                => '',
                           'feed_image'          => '',
                           'feed_type'           => '',
                           'hide_empty'          => 0,
                           'hide_title_if_empty' => false,
                           'hierarchical'        => true,
                           'order'               => 'ASC',
                           'orderby'             => 'name',
                           'separator'           => '<br />',
                           'show_count'          => 0,
                           'show_option_all'     => '',
                           'show_option_none'    => __( 'No categories' ),
                           'style'               => 'list',
                           'taxonomy'            => 'explore_gyan_category',
                           'title_li'            => __( 'Categories' ),
                           'use_desc_for_title'  => 1,
                       ) );
                        echo "<ul class='catLatest'>";
                        if(!empty($categories))
                        {
                            foreach ($categories as $terms) {
                            
                            $term_id1 = $terms->term_id;
                            $image   = category_image_src( array('term_id'=>$term_id1) , false );
                            $category_link = get_category_link( $term_id1 );
                            $getListCount1 = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id1));
                        ?>
                            <li><a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id1; ?>&action=home_appliances"> <?php echo $terms->name; ?> ( <?php echo $getListCount1[0]->countPost; ?> )</a></li>
                        <?php 
                            }
                          }
                        ?>
                        </ul></div>
                  </aside>
               </div>
            </div>
               <?php
               if(isset($_POST["search"])) {
                 global $wpdb;

                 $searchFilter = $_POST["search"];
                 $special_entries =  array('industry', 'home_appliances', 'student_stuff','blogs');
                 $how_many = count($special_entries);
                 $placeholders = array_fill(0, $how_many, '%s');
                 $format = implode(', ', $placeholders);

                 $query = "SELECT * FROM wp_posts WHERE post_type IN($format) AND post_status='publish' ORDER BY ID DESC";

                 $results = $wpdb->get_results($wpdb->prepare( $query, $special_entries));

                 foreach($results as $result) {
                     
                     if(stripos($result->post_title, $searchFilter) !== false) {
                     
                        $getTaxonomy = $wpdb->get_results($wpdb->prepare("SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = %d", $result->ID));

                        $getCatName = $wpdb->get_results($wpdb->prepare("SELECT name FROM wp_terms WHERE term_id = %d", $getTaxonomy[0]->term_taxonomy_id));

                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result->ID ), 'single-post-thumbnail' );
                        
                        if($image){
                          $image = $image[0];
                        } else {
                            $mkey = "_thumbnail_id";
                            $checkimg = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result->ID,$mkey));

                            $getimg = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value));
                            $image = $getimg[0]->guid;  
                        }
                        $table_name = "wp_post_views";
                        $checkViews = $wpdb->get_results($wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result->ID));
                       $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result->post_author));
                           $author_name = $getAuthor[0]->display_name;
                        $countPostTitle1 = strlen($result->post_title);
                        if($countPostTitle1 >= 80) {
                          $postTitle1 = substr($result->post_title,0, 80);
                          $postTitle1 .= "...";
                        } else {
                          $postTitle1 = $result->post_title;
                        }
                ?>
                         <div class="col-md-12 blo">
                            <div class="">
                               <div class="col-md-4 col-sm-4">
                                   <a href="<?php echo esc_url( get_permalink($result->ID) ); ?>">  <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                               </div>
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <p class="categtp"><strong></strong><?php echo strtoupper($getCatName[0]->name); ?></p>
                                 <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result->ID) ); ?>"> <?php echo $postTitle1; ?></a>
                                 <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                                 </p>

                                 <div class="dropdown customs">      
                                        <button class="dropbtn">
                                          <a href="#" class="rightwd views"> <i class="fa fa-share-alt" aria-hidden="true"></i><i class="fa fa-eye"></i><?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?> </a>
                                        </button>
                                    <div class="dropdown-content">
                                      <?php $urlshare = esc_url( get_permalink($result->ID) );
                                        $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                      ?>
                                        <?php echo do_shortcode("$urlsharing"); ?>
                                    </div>
                                </div>

                                 <p class="rightwd"><strong>  <?php echo date("F jS, Y . g:i a", strtotime($result->post_date)); ?></strong></p>
                            </div>
                         </div>
                 <?php
                       }
                      }
                  } else if(isset($_POST["top_filter"])) {
                     
                     $action = "industry";
                     $filter = $_POST["top_filter"];
                     global $wpdb;
                     
                     $special_entries =  array('industry', 'home_appliances', 'student_stuff','blogs');
                     $how_many = count($special_entries);
                     $placeholders = array_fill(0, $how_many, '%s');
                     $format = implode(', ', $placeholders);

                     if($filter == "views") {
                        
                        $query = "SELECT * FROM wp_posts INNER JOIN wp_post_views ON wp_posts.ID = wp_post_views.post_id WHERE wp_posts.post_type IN($format) AND post_status='publish' ORDER BY wp_post_views.view_count DESC";

                        $results = $wpdb->get_results($wpdb->prepare($query, $special_entries));

                     } else if($filter == "recent") {
                        
                        $query = "SELECT * FROM wp_posts WHERE post_type IN($format) AND post_status='publish' ORDER BY ID DESC";

                        $results = $wpdb->get_results($wpdb->prepare($query, $special_entries));
                     }

                     foreach($results as $result) {
                        
                        $getTaxonomy = $wpdb->get_results($wpdb->prepare("SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = %d", $result->ID));

                        $getCatName = $wpdb->get_results($wpdb->prepare("SELECT name FROM wp_terms WHERE term_id = %d", $getTaxonomy[0]->term_taxonomy_id));

                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result->ID ), 'single-post-thumbnail' );
                          
                        if($image){
                           $image = $image[0];
                        } else {
                              $mkey = "_thumbnail_id";
                              $checkimg = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result->ID, $mkey) 
                              );
                              $getimg = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value));
                              $image = $getimg[0]->guid;
                        }
                          $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result->post_author));
                           $author_name = $getAuthor[0]->display_name;
                          $table_name = "wp_post_views";
                          $checkViews = $wpdb->get_results($wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result->ID));
                          $countPostTitle1 = strlen($result->post_title);
                          if($countPostTitle1 >= 80) {
                            $postTitle1 = substr($result->post_title,0, 80);
                            $postTitle1 .= "...";
                          } else {
                            $postTitle1 = $result->post_title;
                          }               
                ?>
                      <div class="col-md-12 blo">
                          <div class="">
                             <div class="col-md-4 col-sm-4">
                                <a href="<?php echo esc_url( get_permalink($result->ID) ); ?>">  <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                             </div>
                          </div>
                          <div class="col-md-8 col-sm-8">
                             <p class="categtp"><strong></strong><?php echo strtoupper($getCatName[0]->name); ?></p>
                             <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result->ID) ); ?>"> <?php echo $postTitle1; ?></a>
                             <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                             </p>

                             <div class="dropdown customs">      
                               <button class="dropbtn">
                                  <a href="#" class="rightwd views"> <i class="fa fa-share-alt" aria-hidden="true"></i><i class="fa fa-eye"></i><?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?> </a>
                               </button>
                                 <div class="dropdown-content">
                                   <?php $urlshare = esc_url( get_permalink($result->ID) );
                                     $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                   ?>
                                     <?php echo do_shortcode("$urlsharing"); ?>
                                 </div>
                             </div>
                             <p class="rightwd"><strong> <?php echo date("F jS, Y . g:i a", strtotime($result->post_date)); ?> </strong></p>
                          </div>
                       </div>
                <?php 
                     }
                  }  else {
                      $args = array(
                        'numberposts' => 60,
                        'offset' => 0,
                        'category' => 0,
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' =>'',
                        'post_type' => array('industry', 'home_appliances', 'student_stuff','blogs'),
                        'post_status' => 'publish',
                        'suppress_filters' => true
                      );
                    
                    $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
                    
                    if(!empty($recent_posts))
                    {
                      
                      foreach ($recent_posts as $value) {
                        $i=0;
                        $cat = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE object_id = %d", $value['ID']));

                        $getCatName = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT term_id, name FROM wp_terms WHERE term_id = %d", $cat[0]->term_taxonomy_id));

                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
                        
                        if($image){
                            $image = $image[0];
                        } else {
                          $mkey = "_thumbnail_id";
                          $checkimg = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $value['ID'],$mkey));

                          $getimg = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value));
                          $image = $getimg[0]->guid;  
                        }
                        $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d",  $value['post_author']));
                           $author_name = $getAuthor[0]->display_name;
                        
                        $table_name = "wp_post_views";
                        $checkViews1 = $wpdb->get_results($wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $value['ID']));

                        $countPostTitle1 = strlen($value['post_title']);
                        if($countPostTitle1 >= 80) {
                          $postTitle1 = substr($value['post_title'],0, 80);
                          $postTitle1 .= "...";
                        } else {
                          $postTitle1 = $value['post_title'];
                        }
                     ?>
                 <div class="col-md-12 blo">
                    <div class="">
                       <div class="col-md-4 col-sm-4">
                          <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">   <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                       </div>
                    </div>
                    <div class="col-md-8 col-sm-8">
                    <?php
                      if($value['post_type'] == "industry") {
                    ?>
                       <p class="categtp"><strong></strong><a href="<?php echo site_url(); ?>/expertise?id=<?php echo $getCatName[0]->term_id; ?>&action=<?php echo $value['post_type'];?>"><?php echo strtoupper($getCatName[0]->name); ?>
                       </a></p>
                    <?php
                      } else if($value['post_type'] == "home_appliances") {
                    ?>
                        <p class="categtp"><strong></strong><a href="<?php echo site_url(); ?>/explore-gyan?id=<?php echo $getCatName[0]->term_id; ?>&action=<?php echo $value['post_type'];?>"><?php echo strtoupper($getCatName[0]->name); ?>
                       </a></p>
                    <?php
                      } else if($value['post_type'] == "student_stuff") {
                    ?>
                          <p class="categtp"><strong></strong><a href="<?php echo site_url(); ?>/student_stuff?id=<?php echo $getCatName[0]->term_id; ?>&action=<?php echo $value['post_type'];?>"><?php echo strtoupper($getCatName[0]->name); ?>
                       </a></p>
                    <?php
                      } else if($value['post_type'] == "blogs") {
                    ?>
                          <p class="categtp"><strong></strong><a href="<?php echo site_url(); ?>/expertise?id=<?php echo $getCatName[0]->term_id; ?>&action=blogs"><?php echo strtoupper($getCatName[0]->name); ?> </a></p>
                    <?php
                      }
                    ?>
                       <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($value['ID']) ); ?>"> <?php echo !empty($postTitle1)?$postTitle1:""; ?></a>
                       <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $value['post_author'];?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                       </p>

                       <div class="dropdown customs">      
                            <button class="dropbtn">
                              <a href="#" class="rightwd views"> <i class="fa fa-share-alt" aria-hidden="true"></i><i class="fa fa-eye"></i><?php if($checkViews1){echo $checkViews1[0]->view_count;}else{echo "0";}?> </a>
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($value['ID']) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>

                       
                       
                       <p class="rightwd"><strong> <?php echo date("F jS, Y . g:i a", strtotime($value['post_date'])); ?></strong> </p>
                    </div>
                 </div>
                <?php 
                      }
                    }
                  }
                ?>
            </div>

            <div class="col-md-4 customsideedu">
               <div id="secondary" class="widget-area col-md-3" role="complementary">
                  <aside id="recent-posts-3" class="widget widget_recent_entries latestsidebar">
                  <button class="accordion"> <h3 class="widget-title">Explore By Industry / Business</h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">   
                     <?php
                       $categories = get_categories( array(
                           'child_of'            => 0,
                           'current_category'    => 0,
                           'parent'              => 0,
                           'depth'               => 0,
                           'echo'                => 1,
                           'exclude'             => '',
                           'exclude_tree'        => '',
                           'feed'                => '',
                           'feed_image'          => '',
                           'feed_type'           => '',
                           'hide_empty'          => 0,
                           'hide_title_if_empty' => false,
                           'hierarchical'        => true,
                           'order'               => 'ASC',
                           'orderby'             => 'name',
                           'separator'           => '<br />',
                           'show_count'          => 0,
                           'show_option_all'     => '',
                           'show_option_none'    => __( 'No categories' ),
                           'style'               => 'list',
                           'taxonomy'            => 'expert_gyan_category',
                           'title_li'            => __( 'Categories' ),
                           'use_desc_for_title'  => 1,
                       ) );
                        echo "<ul class='catLatest'>";
                        if(!empty($categories))
                        {
                            foreach ($categories as $terms) {
                            
                            $term_id = $terms->term_id;
                            $image   = category_image_src( array('term_id'=>$term_id) , false );
                            $category_link = get_category_link( $term_id );
                            $getListCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id));
                        ?>
                            <li><a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id; ?>&action=industry"> <?php echo $terms->name; ?> ( <?php echo $getListCount[0]->countPost; ?> )</a></li>
                        <?php 
                            }
                          }
                        ?>
                        </ul></div>
                  </aside>
                  
                  <aside id="recent-posts-3" class="widget widget_recent_entries latestsidebar">
                     <button class="accordion"> <h3 class="widget-title">College Stuff & Career </h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">   
                     <?php
                       $categories = get_categories( array(
                           'child_of'            => 0,
                           'current_category'    => 0,
                           'parent'              => 0,
                           'depth'               => 0,
                           'echo'                => 1,
                           'exclude'             => '',
                           'exclude_tree'        => '',
                           'feed'                => '',
                           'feed_image'          => '',
                           'feed_type'           => '',
                           'hide_empty'          => 0,
                           'hide_title_if_empty' => false,
                           'hierarchical'        => true,
                           'order'               => 'ASC',
                           'orderby'             => 'name',
                           'separator'           => '<br />',
                           'show_count'          => 0,
                           'show_option_all'     => '',
                           'show_option_none'    => __( 'No categories' ),
                           'style'               => 'list',
                           'taxonomy'            => 'social_stuff_category',
                           'title_li'            => __( 'Categories' ),
                           'use_desc_for_title'  => 1,
                       ) );
                        echo "<ul class='catLatest'>";
                        if(!empty($categories))
                        {
                            foreach ($categories as $terms) {
                            
                            $term_id2 = $terms->term_id;
                            $image   = category_image_src( array('term_id'=>$term_id2) , false );
                            $category_link = get_category_link( $term_id2 );
                            $getListCount2 = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id2));
                        ?>
                            <li><a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id2; ?>&action=student_stuff"> <?php echo $terms->name; ?> ( <?php echo $getListCount2[0]->countPost; ?> )</a></li>
                        <?php 
                            }
                          }
                        ?>
                        </ul></div>
                  </aside>
                  <aside id="recent-posts-3" class="widget widget_recent_entries latestsidebar">
                   <button class="accordion"> <h3 class="widget-title">Blogs & Opinion</h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">     
                     <?php
                       $categories = get_categories( array(
                           'child_of'            => 0,
                           'current_category'    => 0,
                           'parent'              => 0,
                           'depth'               => 0,
                           'echo'                => 1,
                           'exclude'             => '',
                           'exclude_tree'        => '',
                           'feed'                => '',
                           'feed_image'          => '',
                           'feed_type'           => '',
                           'hide_empty'          => 0,
                           'hide_title_if_empty' => false,
                           'hierarchical'        => true,
                           'order'               => 'ASC',
                           'orderby'             => 'name',
                           'separator'           => '<br />',
                           'show_count'          => 0,
                           'show_option_all'     => '',
                           'show_option_none'    => __( 'No categories' ),
                           'style'               => 'list',
                           'taxonomy'            => 'blog_category',
                           'title_li'            => __( 'Categories' ),
                           'use_desc_for_title'  => 1,
                       ) );
                        echo "<ul class='catLatest'>";
                        if(!empty($categories))
                        {
                            foreach ($categories as $terms) {
                            
                            $term_id3 = $terms->term_id;
                            $image   = category_image_src( array('term_id'=>$term_id3) , false );
                            $category_link = get_category_link( $term_id3 );
                            $getListCount3 = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id3));
                        ?>
                            <li><a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id3; ?>&action=blogs"> <?php echo $terms->name; ?> ( <?php echo $getListCount3[0]->countPost; ?> )</a></li>
                        <?php 
                            }
                          }
                        ?>
                        </ul></div>
                  </aside>

                  <aside id="recent-posts-3" class="widget widget_recent_entries latestsidebar">
                    <button class="accordion">  <h3 class="widget-title">Home Appliances </h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">  
                     <?php
                       $categories = get_categories( array(
                           'child_of'            => 0,
                           'current_category'    => 0,
                           'parent'              => 0,
                           'depth'               => 0,
                           'echo'                => 1,
                           'exclude'             => '',
                           'exclude_tree'        => '',
                           'feed'                => '',
                           'feed_image'          => '',
                           'feed_type'           => '',
                           'hide_empty'          => 0,
                           'hide_title_if_empty' => false,
                           'hierarchical'        => true,
                           'order'               => 'ASC',
                           'orderby'             => 'name',
                           'separator'           => '<br />',
                           'show_count'          => 0,
                           'show_option_all'     => '',
                           'show_option_none'    => __( 'No categories' ),
                           'style'               => 'list',
                           'taxonomy'            => 'explore_gyan_category',
                           'title_li'            => __( 'Categories' ),
                           'use_desc_for_title'  => 1,
                       ) );
                        echo "<ul class='catLatest'>";
                        if(!empty($categories))
                        {
                            foreach ($categories as $terms) {
                            
                            $term_id1 = $terms->term_id;
                            $image   = category_image_src( array('term_id'=>$term_id1) , false );
                            $category_link = get_category_link( $term_id1 );
                            $getListCount1 = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id1));
                        ?>
                            <li><a href="<?php echo site_url(); ?>/expertise?id=<?php echo $term_id1; ?>&action=home_appliances"> <?php echo $terms->name; ?> ( <?php echo $getListCount1[0]->countPost; ?> )</a></li>
                        <?php 
                            }
                          }
                        ?>
                        </ul></div>
                  </aside>
                  
               </div>
            </div>

         </div>
      </div>
      <?php while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'content', 'page' ); ?>
      <?php
         // If comments are open or we have at least one comment, load up the comment template
         if ( comments_open() || get_comments_number() ) :
           comments_template();
         endif;
         ?>
      <?php endwhile; // end of the loop. ?>
      </main><!-- #main -->
   </div>
   <!-- #primary -->
   <?php get_footer(); ?>
</div>
<style>
#secondary button.accordion {
    background-color: transparent;
    border: none;
    width: 100%;
    text-align: left;
    border-bottom: 1px solid #f68e2f;
    padding:0px;
}
#secondary button.accordion:hover {
    outline: none;
}
.latestsidebar{padding-top: 0px!important;}
.searchform input {
    height: 36px !important;
}
.panel {
  display: none;
  background-color: #f4f4f4;
}
.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    margin-top: -37px;
    font-size: 43px;
}
.col-md-4.customsideedus {
    display: none;
}
.active:after {
    content: "\2212";
}
   div#secondary h3.widget-title{
   float: left;
   width: 100%;
   }
   .blo a.bloghdaedu {
   font-size: 20px;
   margin: 0;
   padding: 0;
   line-height: 20px;
   text-align: left;
   width: 100%;
   font-weight: 600;
   }
   p.leftwd {
   float: left;
   width: 89%;
   color: #9e9292;
   }
   a.rightwd.views {
   color: #000;
   font-size: 13px;
   float: right;
   }
   .sid input.search-field {
   width: 103%;
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   padding: 0 16px;
   }
   .fullwd{width:100%; float:left;}
   .leftwd{float:left;}
   .rightwd{float:left;}
   .pimg1 p { 
   margin-bottom: 0px;
   line-height: 18px;
   font-size: 16px;
   margin-top: 10px;
   }
   .othermemd .col-md-5{
   padding:0px;
   } 
   .othermemd .cat1 {
   text-align: center;
   padding: 0px;
   }
   .pimg1 span {
   color: #f68e2f;
   }
   .pimg strong {
   font-weight: 100;
   font-size: 13px;
   }
   .pimg sapn {
   font-size: 16px;
   line-height: 16px;}
   .cat1 {
   text-align: center;
   }
   .pimg a {
   line-height: 20px;
   }
   .pimg p {
   margin-bottom: 6px;
   }
   .pimg1 strong {
   font-weight: 500;
   }
   .pimg span {
   font-size: 13px;
   COLOR: #aa9292;
   margin-top: 4px;
   display: block;
   }
   #secondary {
   background-color: #f4f4f4;
   width: 100%;
   float:left;
   }
   .blo a.bloghdaedu {
   font-size: 20px;
   margin: 0;
   padding: 0;
   line-height: 20px;
   float: left;
   width: 100%;
   font-weight: 600;
   }
   .cat {
   padding: 0;
   }
   .blo p {
   margin-bottom: 7px;
   font-size: 15px;
   text-align: left;
   }
   .othermemd {
   box-shadow: -1px 0px 4px 0 rgba(0, 0, 0, 0.07);
   float: left;
   width: 100%;
   border: 1px solid #ddd;
   padding: 10px;
   margin-bottom:20px;
   }
   h4 {
   margin: 0 0 17px 0;
   }
   .blo img.img-thumbnail {
   height: 147px;
   }
   .headarea {
   float: left;
   width: 100%;
   }
   .other a {
   float: right;
   }
   .other h4 {
   float: left;
   margin: 0 0 18px 0;
   }
   .profile ul {
   padding: 0;
   list-style-type: none;
   }
   .memb .profile h3{
   color: #828282;
   }
   .profile {
   float: left;
   width: 100%;
   text-align: center;
   background: #efeded;
   padding: 10px;
   }
   .memb .profile p{   text-align: center; float: none;}
   p.categtypeedus {
   font-size: 15px;
   color: #7f7a7a;
   }
   .memb .profile h2 {
   font-size: 21px;
   color: #060658;
   }
   .profile ul li {
   float: left;
   display: block;
   }
   .profile ul li span, .profile ul li p{
   float:left;
   font-size: 15px;
   }
   .memb .profile .fa.fa-eye{
   margin-right: 0px !important;
   }
   .profile ul li span {
   width: 185px;
   }
   .memb {
   text-align:left;
   }
   .memb h2 {
   font-size: 30px;
   padding: 0px;
   margin: 10px 0px 0px 0px;
   }
   .memb1 {
   background-color: #fff;
   border: 1px solid #ddd;
   border-radius: 4px;
   padding: 48px;
   margin-top: 0px;
   float: left;
   width: 100%;
   }
   .memb h3 {
   font-size: 17px;
   margin: 5px;
   }
   .memb p {
   margin: 0 0 0px;
   float: left;
   }
   .memb1 p{
   float: left;
   text-align: center;
   font-size: 23px;
   font-weight: 600;
   }
   .memb1 button{
   float: right;
   }
   .memb spam1{
   font-weight:600;
   } 
   .memb spam{
   font-weight:600;
   }
   .memb .fa.fa-eye {
   display: initial;
   }
   .blo {
   border-radius: 4px;
   background-color: #ffffff;
   box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.07);
   border: solid 1px #ececec;
   margin-bottom: 30px;
   padding: 10px;
   float: left;
   width: 100%;
   }
   .arteduro {
   float: left;
   width: 100%;
   margin: 30px 0;
   }
   .blo2 a {
   float: right;
   }
   .rightoption select {
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   line-height: 3px;
   padding: 1px 18px;
   margin: 0 0px 0 0px;
   }
   .rightoption p {
   float: left;
   margin: 0px 29px;
   }
   .rightoption {
   float: right;
   
   }
   .colh {
   text-align: left;
   margin-bottom:26px;
   }
   .sid {
   float: left;
   }
   .pimg {
   TEXT-ALIGN: LEFT;
   }
   .pimg1 {
   TEXT-ALIGN: LEFT;
   }
   @media only screen and (max-width: 767px){
   .rightwd{
   float: left;
   }
   .leftwd {
   width: 100%;
   }
   .col-md-4.customsideedus {
    display: block;
        padding: 0px;
}
.sid {
    float: left;
    width: 100%;
    margin-bottom: 20px;
}
.blo {
    margin-top: 40px;
}
.col-md-4.customsideedu {
    display: none;
}
   .rightoption select {
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   line-height: 3px;
   padding: 1px 35px;
   margin: 1px 0px 8px 0px;
   }
   .rightoption p {
   margin: 0px 18px;
   flaot:left;
   }
   .arteduro {
    float: left;
    width: 100%;
    margin: 0px!important;

}
.rightoption{
    width: 100%;
}
.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
}
.searchform input {
    height: 36px!important;
    padding: 4px 55px!important;
}
   }
</style>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>

