<div class="ajeetbaba">
<?php
   /**
   /*
    Template Name: Exam Pattern Scheme
    */   
get_header(); ?>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;
?>
<div id="primary" class="content-area">
   <ul class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php site_url();?>/studentmore/">Student Corner</a></li>
      <li class="breadcrumb-item"><a href="<?php site_url();?>/studentmore/">
        <?php
          $breadid = $_GET["action"];
          $getBreadcrumb = $wpdb->get_results( 
              $wpdb->prepare( "SELECT examName FROM wp_exam_type WHERE id = %d", $breadid)
          );
        ?>
        <?php echo $getBreadcrumb[0]->examName; ?>
      </a></li>
      <li class="breadcrumb-item active" aria-current="page"><strong>Details</strong></li>
   </ul>
   <style>
      ul.breadcrumb li {
      display: inline-block;
      margin: 16px 13px;
      }
      .elementor p {
      margin: 1px 0;
      font-size: 12px;
      }
      .elementor a {
      font-size: 14px;
      color: #000;
      }
      .elementor a:hover{
      color: #4a4aca;
      }
      div {
      font-size: 14px;
      }
      h6{
      color: #5c5cd0;
      }
      .coursepremuim {
      float: left;
      width: 100%;
      }
      article#post-1343 {
      float: left;
      }
      button.btn.btn-info i.fa-plus{
      display:none;
      }
      button.btn.btn-info i.fa-minus{
      display:block;
      }
      button.btn.btn-info.collapsed i.fa-plus{
      display:block;
      }
      button.btn.btn-info.collapsed i.fa-minus{
      display:none;
      }
      .coursepremuim button {
      background: transparent;
      width: 100%;
      text-align: left;
      border: 1px solid #bbb;
      color: rgb(92, 92, 208);
      padding: 13px 15px;
      border-radius: 0px;
      font-weight: 800;
      }
      .coursepremuim button h5{margin:0; color: rgb(92, 92, 208); text-transform: capitalize;     font-family: "Roboto", Sans-serif;}
      .coursepremuim button.btn.btn-info i {
      float: left;
      margin: 0 9px 0 0;
      padding: 0;
      }
      .coursepremuim button.btn-info:hover{ 
      background: transparent;
      color: rgb(92, 92, 208);
      border: 1px solid #ddd;
      outline: none;
      box-shadow: none;
      }
      .coursepremuim .btn-info.focus, .coursepremuim .btn-info:focus{
      background: transparent;
      border: 1px solid #ddd;
      outline: none;
      box-shadow: none;
      }
      .syllabpanel{
      border: 1px solid #ddd;
      float: left;
      }
      .container2{
      margin:0 auto;
      max-width:1170px;
      }
      .coursepremuim a {
      font-size: 14px;
      color: #000;
      font-family: "Roboto", Sans-serif;
      font-weight: 400;
      }
      .coursepremuim  p {
      margin-bottom: 0;
      }
      .premuimbtn a{
      font-family: "Roboto", Sans-serif;
      font-weight: 500;
      background-color: #61ce70;
      color:#fff;
      }
	  .ajeetbaba .container {
    width: 100%;
}
      @media only screen and (max-width: 767px)
   {
      .premuimbtn {
    float: left;
    width: 100%;
    margin: 11px 0px;
    text-align: center;
}
h5.elementor-heading-title.elementor-size-default {
    text-align: center;
    font-size: 25px;
}
.breadcrumb{
	    margin-top: 106px;
}
ul.breadcrumb li {
    display: inline-block;
    margin: 14px 3px;
}
h5.elementor-heading-title.elementor-size-default {
    float: left;
    width: 100%;
}
   }
   </style>
   <div class="studentprofilesmt">
      <div class="coursepremuim">
         <div class="container2">
            <div class="col-md-8">
               <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo">
                  <h5 class="">
                     <span class="elementor-accordion-icon elementor-accordion-icon-left" aria-hidden="true">
                     <i class="elementor-accordion-icon-closed fa fa-plus"></i>
                     <i class="elementor-accordion-icon-opened fa fa-minus"></i>
                     </span>
                     Syllabus
                  </h5>
               </button>
               <div id="demo" class="collapse in syllabpanel">
                <?php
                    $action = $_GET["action"];
                    $getSubjects = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT * FROM wp_subject_type WHERE exam_id = %d", $action)
                    );

                    foreach($getSubjects as $getSubject) {
                ?>
                  <div class="col-sm-4">
                     <h6><i class="fa fa-star" aria-hidden="true"></i><?php echo $getSubject->subject_name; ?></h6>
                     <?php
                        $getTopics = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_topic_subject WHERE exam_id = %d AND subject_id = %d", $action, $getSubject->id)
                        );
                        $x=1;
                        foreach($getTopics as $getTopic) {
                     ?>
                            <p><?php echo $x;?>. <a href="http://edukeeda.com/practice-exercise?action=<?php echo $action;?>&&action1=<?php echo $getSubject->id;?>&&action2=<?php echo $getTopic->id;?>"><?php echo $getTopic->topic_name; ?></a></p>
                        <?php
                            $x++;
                            }
                        ?>
                  </div>
                  <?php
                    }
                  ?>
               </div>
            </div>
            <div class="col-md-4">
               <div class="elementor-widget-container">
                  <h5 class="elementor-heading-title elementor-size-default">About Exams</h5>
               </div>
               <div class="premuimbtn">
                  <a href="http://edukeeda.com/premium-test-series/" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                  <span class="elementor-button-content-wrapper">
                  <span class="elementor-button-text">Premium Tests</span>
                  </span>
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div id="primary" class="content-area">
            <main id="main" class="post-wrap" role="main">
               <?php while ( have_posts() ) : the_post();
                  global $post;
                  
                              ?>
               <?php
                  if($post->ID=="921")
                  {
                          get_template_part( 'content', 'student' );
                  }
                  else{
                              get_template_part( 'content', 'page' );
                  }
                  ?>
               <?php
                  // If comments are open or we have at least one comment, load up the comment template
                  if ( comments_open() || get_comments_number() ) :
                      comments_template();
                  endif;
                  ?>
               <?php endwhile; // end of the loop. ?>
            </main>
            <!-- #main -->
         </div>
         <!-- #primary -->
      </div>
      <?php get_footer(); ?>
   </div>
</div>

