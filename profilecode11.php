<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;

    if(isset($_POST['submitsteppass'])) {

      $userId = $_SESSION["login"]["id"];
      $emailcheck = $_POST["emailcheck"];
      
      if($_POST["privacyId"]) {
          
          if($emailcheck == "on" || $emailcheck == "ON") {
              $results = $wpdb->update("wp_mobile_privacy", array('status'=>1), array('user_id'=>$userId));
          } else {
              $results = $wpdb->update("wp_mobile_privacy", array('status'=>0), array('user_id'=>$userId));
          }
      }else{
          
          if($emailcheck == "on" || $emailcheck == "ON") {
              $data = ["user_id"=>$userId, "status" => 1];
          
          } else {
              $data = ["user_id"=>$userId, "status" => 0];
          }
          $wpdb->insert("wp_mobile_privacy", $data);
      }
      header("Location: https://edukeeda.com/edit-profile/");
    }
?>
