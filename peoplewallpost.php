

<div class="homepagemenedu discussionback">
   <?php
      /*
       Template Name: People Wallpost
       */
      get_header(); ?>
   <?php
      $path = $_SERVER['DOCUMENT_ROOT'];
      include_once $path . '/wp-config.php';
      include_once $path . '/wp-load.php';
      include_once $path . '/wp-includes/wp-db.php';
      include_once $path . '/wp-includes/pluggable.php';
      global $wpdb;
      global $current_user;
      if(isset($_POST["wallsubmit"])) {
          $walltext = $_POST["walltext"];
          $userid = $_SESSION["login"]["id"];
          $data = ["user_id"=>$userid, "content"=>$walltext];
          $wpdb->insert("wp_wallpost", $data);
          $msgWall = "Wallpost posted successfully.";
      
          if($msgWall) {
      ?>
   <script type="text/javascript">
      alert("WallPost has been successfully updated on wall");
   </script>
   <?php
      }
      ?>
   <script type="text/javascript">
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
   </script>
   <?php
      }
      
      if(isset($_POST['likeSubmit'])) {
        $wallid = $_POST['wallid'];
        $userid = $_SESSION["login"]["id"];
        $data = ["user_id"=>$userid, "wallid"=>$wallid];
        $wpdb->insert("wp_wall_like", $data);
      ?>
   <script type="text/javascript">
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
   </script>
   <?php
      }
      
      if(isset($_POST['unlikeSubmit'])) {
        $wallid = $_POST['wallid'];
        $userid = $_SESSION["login"]["id"];
        $wpdb->query($wpdb->prepare("DELETE FROM wp_wall_like WHERE wallid = %d AND user_id=%d", $wallid, $userid));
      ?>
   <script type="text/javascript">
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
   </script>
   <?php
      }
      
      if(isset($_POST['replypostsubmit'])) {
        $wallid = $_POST['wallid'];
        $userid = $_SESSION["login"]["id"];
        $replyPost = $_POST['replyPost'];
      
        $data = ["user_id"=>$userid, "wallid"=>$wallid, "reply"=>$replyPost];
        
        $wpdb->insert("wp_wall_reply", $data);
        $msgReply = "Reply successfully";
      }
      
      if(isset($msgReply)) { ?>
           <script type="text/javascript">
              alert("Reply posted successfully");
           </script>
   <?php
      unset($msgReply);
      ?>
       <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
       </script>
   <?php
      }
        if(isset($_POST['repostsubmit'])) {
            $wallid = $_POST['wallid'];
            $walltext = $_POST['walltext'];
            $reposttext = $_POST['reposttext'];
            $userid = $_SESSION["login"]["id"];
            $data = ["user_id"=>$userid, "content"=>$reposttext, "shareType"=>"wall", "shareid"=>$wallid, "sharetext"=>$walltext];
            $wpdb->insert("wp_wallpost", $data);
      ?>
           <script type="text/javascript">
              if ( window.history.replaceState ) {
                  window.history.replaceState( null, null, window.location.href );
              }
           </script>
   <?php
      }

          if(isset($_POST['walldeletesubmit'])) {
            $wid = $_POST['wid'];
            $wpdb->query($wpdb->prepare("DELETE FROM wp_wallpost WHERE id = %d", $wid));
    ?>
        <script type="text/javascript">
            if ( window.history.replaceState ) {
                window.history.replaceState( null, null, window.location.href );
            }
        </script>
    <?php
      }
    ?>
   <style>
      .imgforr {
      text-align: center;
      float: left;
      width: 100%;
      }
      .imgforr img.img-thumbnail {
      width: 60px;
      height: 60!important;;
      border-radius: 100%;
      }
      #DiscussionHouses .post strong {
      color: #9a9a9a;
      }
      #DiscussionHouses .post {
      color: #9a9a9a;
      }
      #DiscussionHouses .post span {
      margin-right: 4px;
      }
      p.rigwd {
      width: 100%;
      }
      #DiscussionHouses .post p {
      margin: 0px 4px;
      }
      .post span {
      margin-right: 6px;
      }
      #articleblous .text1 a {
      color: #000;
      }
      #myModalreps .modal-content {
      MAX-HEIGHT: 600px;
      overflow-y: scroll;
      }
      .post strong {
      color: #373779;
      }
      #articleblous .post
      {
      color:#9a9a9a;
      }
      #articleblous .post strong {
      color: #9a9a9a;
      }
      #articleblous .share, .share1, .share2
      {
      color:#9a9a9a;
      }
      #wallposts .share, .share1, .share2
      {
      color:#060658;
      }
      #articleblous .share .fa
      {
      color:#9a9a9a; 
      }
      #anwerspat .share
      {
      color:#9a9a9a;
      }
      #anwerspat .share p
      {
      color:#9a9a9a;
      }
      {
      color:#9a9a9a;
      } 
      #articleblous .share1 button
      {
      color:#9a9a9a; 
      }
      #articleblous .fa
      {
      color:#9a9a9a; 
      }
      #articleblous .share2 p
      {
      color:#9a9a9a;
      }
      #anwerspat .share1 button
      {
      color:#9a9a9a;
      } 
      #articleblous .share2 .fa
      {
      color:#9a9a9a;
      }
      #anwerspat .share2 .fa
      {
      color:#9a9a9a;
      }
      #anwerspat .share1 .fa
      {
      color:#9a9a9a;
      }
      #anwerspat .share .fa
      {
      color:#9a9a9a;
      }
      #anwerspat .share2 p
      {
      color:#9a9a9a;
      } 
      #anwerspat .share1 p
      {
      color:#9a9a9a;
      } 
      #anwerspat .pp
      {
      color:#9a9a9a;
      }
      #anwerspat .pp strong
      {
      color:#9a9a9a;
      }
      #dischouses .like, .pluging
      {
      color:#9a9a9a;
      }
.memb p {
    margin: 0 0 0px;
    float: initial;
}

      .direct p {
      color: #9a9a9a;
      }
      .plugin {
      float: right;
      }
      .post .dotsseprate {
      margin: 0 6px;
      }
      .pp {
      float: left;
      width: 100%;
      display: -webkit-box;
      margin: 10px 0px;
      }
      .pp span {
      margin-right: 5px;
      }
      .memb .pp {
      float: left;
      width: 100%;
      margin: 10px 0px;
      display: block;
      }
      .pp strong {

      color: #373779;
      }
      .catp strong {
      color: #060658;
      font-weight: 600;
      }
      .catp span {
      margin: 5px;
      color: #4c4242;
      }
      .text {
      float: left;
      width: 100%;
      margin-bottom: 15px;
      margin-top: 15px;
      }
      .text p {
      font-weight: 800;
      color: #4c4242;
      line-height: 30px;
      font-size: 29px!important;
      }
      .text p a{
      font-weight: 800;
      color: #000;
      line-height: 30px;
      font-size: 20px!important;
      }
      .share .fa {
      color: #060658;
      }
      .share1 .fa {
      color: #060658;
      }
      .share2 .fa {
      color: #060658;
      }
      .dot {
      float: right;
      }
      .questio {
      float: left;
      width: 100%;
      margin-bottom: 20px;
      }
      .dot span.countno {
      margin: 0px;
      float: left;
      }
      .dot ul.dropdown-menu {
      min-width: 217px;
      padding: 3px;
      }
      .modal-body p {
      margin-bottom: 0px;
      padding: 3px;
      }
      .post{
      float: left;
      width: 100%;
      display: -webkit-box;
      margin-bottom: 20px;
      }
      .text1 {
      float: left;
      width: 100%;
      margin-bottom: 15px;
      }
      .share1 button {
      color: #102358;
      font-size: 14px;
      text-transform: capitalize;
      background: transparent;
      border: none;
      padding: 0 6px;
      }
      .share1 img {
      width: 15px;
      height: auto;
      }  
      .text1 p {
      font-weight: 800;
      color: #000;
      line-height: 30px;
      font-size: 20px!important;
      }
      .text1 p a{
      font-weight: 800;
      color: #000;
      line-height: 30px;
      font-size: 20px!important;
      }
      .text1 h2 {
      font-size: 20px;
      color: #000;
      }
      .house button {
      font-size: 12px;
      margin: 0px;
      padding: 5px;
      color: #000;
      font-weight: 600;
      }
      #wallposts .share2 p {
      color: #060658;
      }
      .house {
      float: left;
      width: 100%;
      margin: 35px;
      margin-top: 50px;
      }
      .plug img {
      width: 20px;
      float:right;
      }
      .plug {
      text-align: right;
      float: left;
      margin-top: 7px;
      }
      .plug {
      text-align:right;}
      .cat a {
      color: #000000bd;
      font-weight: 600;
      }.catp a {
      color: #060658;
      font-weight: 600;
      }
      .cat span {
      color: #f68e2f;
      margin: 7px;
      }
      .cat {
      margin: 6px 0px;
      }
      .headdiscus h1 {
      font-size: 25px;
      text-align: center;
      }
      .accordion {
      background-color: #eee;
      color: #444;
      cursor: pointer;
      padding: 18px;
      width: 100%;
      border: none;
      text-align: left;
      outline: none;
      font-size: 15px;
      transition: 0.4s;
      }
      .active, .accordion:hover {
      background-color: #ccc; 
      }
      .headarea .active, .accordion:hover {
      background-color: transparent;
      }
      .accordion:after { 
      content: ' \002B';
      font-size: 21px;
      margin: 1px;
      padding: 0px;
      float: right;
      }
      .active:after {
      content: "\2212";
      }
      .panel {
      padding: 0 18px;
      display: none;
      background-color: white;
      overflow: hidden;
      }
      .headarea button.accordion {
      background: none;
      border: none;
      color: #616161;
      margin: 0px;
      padding: 0px;
      font-weight: 500;
      text-transform: capitalize;
      }
      .thumbprofile img {
      width: 50px;
      border: 1px solid #ddd;
      padding: 2px;
      height: 50px;
      border-radius: 100%;
      }
      .thumbprofile {
      float: left;
      width: 60px;
      }
      .areadetails {
      float: left;
      width: 75%;
      }
      .areadetails p {
      line-height: 17px;
      }
      .areadetails h5 {
      margin: 0;
      }
      p.joineon {
      text-transform: capitalize;
      font-size: 13px;
      margin: 4px 0 0 0;
      float: left;
      }
      .profileares {
      float: left;
      width: 100%;
      border: 1px solid #ddd;
      padding: 5px;
      margin-bottom: 20px;
      }
      .reportingsets ul li {
      float: left;
      width: 100%;
      text-align: left;
      }
      .reportingsets ul {
      list-style-type: circle;
      }
      .reportingsets ul li span {
      float: left;
      }
      .reportingsets {
      width: 100%;
      border-bottom: 1px solid #ddd;
      padding: 20px 0;
      margin: 0 0 15px 0;
      }
      .reportingsets .modal-footer{
      border-top:none;
      }
      .bottom-btns {
      padding: 0 21px;
      text-align: right;
      }
      .repolinks label {
      color: #797977;
      cursor: pointer;
      }
      .repolinks label:hover {
      color: #de7514;
      }
      .reportingsets input[type="radio"] {
      margin: 0 8px 0 0;
      }
      .groupname p{margin:0;}
      .postpanels ul li:first-child {
      border-bottom: 1px solid #ddd;
      }
      .filtertab {
      float: left;
      width: 50%;
      text-align: right;
      padding: 10px;
      background:#fff;
      color: #9a9a9a;
      }
      .filtertab span {
      font-size: 12px;
      }
      .pp a {
      background: #f68e2f;
      padding: 10px;
      margin: 15px;
      color: #fff;
      border-radius:4px;
      }
      .pp a:hover{
      background:#fff;
      color: #f68e2f;
      border: 1px solid #f68e2f;
      } 
      .share2 p {
      float: right;
      }
      #questforyou .pp {
      margin-bottom: 45px;
      color:#9a9a9a;
      }
      #wallposts .pp {
      float: left;
      width: 100%;
      margin: 10px 0px;
      display: flex;
      color: #9a9a9a;
      }
      #wallposts .pp strong {
      color: #9a9a9a;
      }
      .catp strong {
      color: #9a9a9a;
      font-weight: 600;
      font-size: 10px;
      }
      .plug button {
      background: none;
      border: none;
      color: #9a9a9a;
      margin: 0px;
      padding: 0px;
      }
      #wallposts .share, .share1, .share2
      {
      color: #9a9a9a;
      }
      #wallposts .share button {
      background: none;
      border: none;
      color: #9a9a9a;
      padding: 0px;
      }
      #wallposts .share .fa  
      {
      color: #9a9a9a;
      display: -webkit-box;
      }
      .text2 img {
    width: 30%;
    height: auto;
}
      #wallposts .share1 .fa  
      {
      color: #9a9a9a;
          display: -webkit-box;
      }
      #wallposts .share2 .fa  
      {
      color: #9a9a9a;
      }
      #wallposts .share2 p
      {
      color: #9a9a9a;
      }
      #questforyou .share .fa {
      color: #9a9a9a;
      }
      #questforyou .share, .share2  p {
      color: #9a9a9a;
      }
      #questforyou .pp strong
      {
      color:#9a9a9a;
      }
      #questforyou .plugin
      {
      color:#9a9a9a;
      }
      #questforyou .text2 strong {
      margin: 0px 6px;
      }
      .text2 span {
      margin: 0px 5px;
      }
      .plug button {
      background: none;
      border: none;
      color: #9a9a9a;
      margin: 0px; 
      padding: 0px;
      float: left;
      }
      #wallposts .text2 span {
      margin: 0px 5px;
      color:#f68e2f;
      }


      @media only screen and (max-width: 767px){

      .text2 {
      float: left;
      width: 100%;
      }
       #wallposts .share1 .fa  
      {
      color: #9a9a9a;
          display: -webkit-box;
      }
      p.catp {
      float: left;
      width: 100%;
      font-size:13px!important;
          display: grid;
      }
      .catp span {
   margin: 8px 0px;
    color: #9a9a9a!important;
    line-height: 18px;
    font-size: 14px;
}
.text2 img {
    width: 30%;
   max-height: 200px;
}
      .memberpage {
      margin-top: 100px;
      }
      .memberpage .advtspace {
      float: left;
      width: 100%;
      }
      .headdiscus {
      background: #fff;
      padding: 10;
      margin-bottom: -11px;
      border: 1px solid #f68e2f;
      }
      .headdiscus.dynamiccategory {
      float: left;
      width: 100%;
      color:#9a9a9a;
      }
      .plugin {
      float: right;
      margin-top: -44px;
      }
      .pluging {
      float: right;
      margin-right: -50px;
      margin-top: -27px;
      }
      .like p {
      margin: 0 2px 0 0;
      }
      .plug button {
      background: none;
      border: none;
      color: #9a9a9a;
      margin: 0px;
      padding: 0px;
      }
      .roll-socials li a:hover, .roll-socials li a, .rol
      .like {
      float: left;
      width: 100%;
      display: -webkit-inline-box;
      margin-top: 10px;
      }
      .text p a {
      font-weight: 800;
      color: #000;
      line-height: 26px;
      font-size: 20px!important;
      }
      .post .dotsseprate {
      margin: 0 2px;
      }
      .post span {
      margin-right: 3px;
      }
      .share1, .share2 {
      float: left;
      width: 100%;
      text-align: center;
      }
      .share1 img {
      width: 23px;
      height: auto;
      }
      .pp strong {
      margin-right: 3px;
      color: #373779;
      }
      .pp span {
      margin-right: 3px;
      }
      .text p {
      font-weight: 800;
      color: #4c4242;
      line-height: 30px;
      font-size: 20px!important;
      }
      .text1 p {
      font-weight: 800;
      color: #4c4242;
      line-height: 28px;
      font-size: 20px!important;
      }
      p.rigwd {
      float: left;
      width: 100%;
      font-size:14px!important;
      line-height: 12px;
      }
      .join {
      float: left;
      width: 100%;
      }
      .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
      padding: 15px;

      }
      .page-wrap {
    padding: 5px 15px!important;
}
.col-md-9.dist {
    padding: 0px;
}
      }

.memb1 p a{
    float: left;
    text-align: center;
    font-size: 23px;
    font-weight: 600;
    margin: 0;
}
   </style>
   <div class="memberpage">
      <div class="row">
         <div class="col-md-9 dist">
            <div class="col-md-4">
               <div class="headarea">
                  <div class="other">
                     <h4>Discussion Group</h4>
                  </div>
                  <div class="headarea">
                     <div class="addposituon bottomspc">
                        <p class="tablinks"><a href="http://edukeeda.com/opinion-poll/">Opinion Poll</a><span class="countno"></span></p>
                        <?php
                           $getSidebarCats = $wpdb->get_results("SELECT * FROM wp_discussion_main");
                           
                           foreach($getSidebarCats as $getSidebarCat) {
                              if($getSidebarCat->have_sub == 0) {
                            ?>
                        <p class=""><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>"><?php echo $getSidebarCat->name;?> <span class="countno"></span></a></p>
                        <?php
                           } else {
                           ?>
                        <div class="spacetp">
                           <button class="accordion"><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>"><?php echo $getSidebarCat->name;?></a></button>
                           <div class="panel">
                              <?php
                                 $getSubCats = $wpdb->get_results( 
                                     $wpdb->prepare( "SELECT * FROM wp_discussion_sub WHERE main_id = %d",$getSidebarCat->id) 
                                 );
                                 foreach($getSubCats as $getSubCat) {
                                 ?>
                              <p class="tablinks"><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>&actions=<?php echo $getSubCat->id; ?>"><?php echo $getSubCat->sub_name;?><span class="countno"></span></a></p>
                              <?php
                                 }
                              ?>
                           </div>
                        </div>
                        <?php
                            }
                          }
                        ?>

                        <?php
                            if(isset($_SESSION["login"]["id"]) || $current_user->ID == 1) {
                        ?>
                              <p class="tablinks"><a href="<?php echo site_url();?>/peoples-wallposts"> People's Wallposts </a><span class="countno"></span></p>
                              <p class="tablinks"><a href="<?php echo site_url();?>/my-discussion-house">My Discussion Houses</a><span class="countno"></span></p>
                              <p class="tablinks"><a href="<?php echo site_url();?>/my-discussion-group">My Groups</a><span class="countno"></span></p>
                        <?php
                            }
                        ?>

                     </div>
                  </div>
               </div>
            </div>

            <div class="col-md-8">
               <div class="headdiscus dynamiccategory">
                  <h1> People's WallPost </h1>
               </div>
               <div class="arteduros">
                  <div class="row">
                     <div class="col-md-12 col-sm-12 basictabsed">
                        <div class="customtabfilter">
                           <div class="filtertab" style="height: 42px;">
                           </div>
                           <div class="filtertab">
                              <span>Recent</span>
                           </div>
                        </div>
                        <div class="memb1">
                           <p><a href="http://edukeeda.com/wallpost-query-form/"> Want to Post Any Update? </a></p>
                        </div>
                        <div id="wallposts" class="tabcontent" style="display:block;">
                           <?php
                              $userId1 = $_SESSION["login"]["id"];
                              $walls =   $wpdb->get_results("SELECT * FROM wp_wallpost ORDER BY id desc");
                              
                              if($walls) {
                                $z=1;
                              
                                foreach($walls as $wall) {
                                  $userRe = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT user_registered FROM wp_users WHERE ID=%d" ,$wall->user_id));
                                  $userPic = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$wall->user_id));
                                  $userName = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$wall->user_id));
                                  $userHeadline = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$wall->user_id));
                              
                                  $countLike = $wpdb->get_results($wpdb->prepare( "SELECT COUNT(id) as cLike FROM wp_wall_like WHERE wallid=%d", $wall->id));
                                  $countReply = $wpdb->get_results($wpdb->prepare( "SELECT COUNT(id) as cReply FROM wp_wall_reply WHERE wallid=%d", $wall->id));
                                  $countRepost = $wpdb->get_results($wpdb->prepare( "SELECT COUNT(id) as cReport FROM wp_wallpost WHERE shareid=%d", $wall->id));
                              ?>
                           <div class="blogaresnews">
                              <div class="memb">
                                  

                                 <div class="col-md-12 blo">

                                    <div class="col-md-3 col-sm-2 col-xs-3">
                                       <div class="imgforr">
                                          <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                       </div>
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-xs-7">
                                       <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $wall->user_id;?>"><?php echo $userName[0]->name;?></a></strong> <?php if($userHeadline[0]->headline) {?> <span><?php if(strlen($userHeadline[0]->headline) > 30) {echo substr($userHeadline[0]->headline, 0,30) .' .. ';} else { echo $userHeadline[0]->headline; }?></span> <?php } ?></p>
                                       <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($wall->created_at )); ?></p>
                                    </div>
                                    <div class="col-md-1 col-sm-1 col-xs-2">
                                      <div class="crose">
                                        <?php
                                            $userid11 = $_SESSION["login"]["id"];
                                            if($userid11 == $wall->user_id || $current_user->ID == 1) {
                                        ?>
                                           <p data-toggle="modal" data-target="#myModalwalldelete<?php echo $z;?>"><i class="fa fa-times" aria-hidden="true"></i></p>

                                              <div id="myModalwalldelete<?php echo $z; ?>" class="modal fade" role="dialog">
                                                 <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                       <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                          <h3>Delete this Wallpost</h3>
                                                       </div>
                                                       <div class="modal-body">
                                                          <p> Do you really want to Delete this Wallpost? </p>
                                                            <form method="post" action="">
                                                              <input type="hidden" name="wid" value="<?php echo $wall->id;?>">
                                                              <input type="submit" name="walldeletesubmit" value="Delete">
                                                            </form>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                        <?php
                                            }
                                        ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                       <div class="text2">
                                          <p> <?php echo $wall->content; ?> </p>
                                        
                                        <?php
                                            if(!empty($wall->shareType)) {
                                                
                                                if($wall->shareType == "ques") {
                                                    $getuserid11 = $wpdb->get_results($wpdb->prepare("SELECT user_id FROM wp_discussion_ques WHERE id=%d", $wall->shareid));
                                                
                                                } else if($wall->shareType == "ans") {
                                                    $getuserid11 = $wpdb->get_results($wpdb->prepare("SELECT user_id FROM wp_discussion_ans WHERE id=%d", $wall->shareid));
                                                
                                                } else if($wall->shareType == "wall") {
                                                    $getuserid11 = $wpdb->get_results($wpdb->prepare("SELECT user_id FROM wp_wallpost WHERE id=%d", $wall->shareid));
                                                }

                                                $userPic11 = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$getuserid11[0]->user_id));
                                                $userName11 = $wpdb->get_results($wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$getuserid11[0]->user_id));
                                                $userHeadline11 = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$getuserid11[0]->user_id));

                                        ?>  

                                        
                                            <!--<div class="sharing">
                                            
                                                <div class="left">
                                                   <div class="imgforr">
                                                      <img src="<?php //echo $userPic11[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">
                                                   </div>
                                                </div>
                                                <div class="right">
                                                   <p class="catp"><strong><a href="<?php //echo site_url();?>/profile?action=<?php //echo $getuserid11[0]->user_id;?>"><?php //echo $userName11[0]->name;?></a></strong> <?php //if($userHeadline11[0]->headline) {?> <span><?php //if(strlen($userHeadline11[0]->headline) > 40) {echo substr($userHeadline11[0]->headline, 0,40) .' .. ';} else { echo $userHeadline11[0]->headline; }?></span> <?php// } ?></p>
                                                </div>
                                                <BR>
                                                <p> <?php //echo $wall->sharetext; ?> </p>                                          
                                          </div>-->

                                        <?php
                                            }
                                        ?>
                                       </div>
                                    </div>
                                    <div class="col-md-12">
                                       <div class="pp">
                                          <p><span> <?php if($countLike){ echo $countLike[0]->cLike; } else{ echo "0";}?> </span><strong>Likes</strong><div class="dotsseprate">.</div></p>
                                          <p>
                                             <span> <?php if($countReply){echo $countReply[0]->cReply; }else{ echo "0";}?> </span>
                                             <strong>
                                          <p data-toggle="modal" data-target="#myModalreply<?php echo $z;?>">Reply</p></strong><div class="dotsseprate"></div></p>
                                          
                                          <!--<p data-toggle="modal" data-target="#myModalrepostlist<?php //echo $z;?>"><span> <?php //if($countRepost){echo $countRepost[0]->cReport; }else{ echo "0";}?> </span><strong>Repost</strong></p>-->
                                       </div>
                                    </div>
                                            <!---- Modal  --->
                                            <div id="myModalreply<?php echo $z;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                               <!-- Modal content-->
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                     <h3> Reply </h3>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row">
                                                        <div clss="col-md-12">
                                                        <?php
                                                            $wallReplys = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wall_reply WHERE wallid=%d", $wall->id));
                                                            if($wallReplys) {

                                                                foreach($wallReplys as $wallReply) {
                                                                    $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $wallReply->user_id));
                                                            
                                                                    $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $wallReply->user_id));
                                                            
                                                                    $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $wallReply->user_id));

                                                        ?>   
                                                           <div class="col-md-12">
                                                              <div class="poll">
                                                                 <div class="left">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="right">
                                                                    <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallReply->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong></p>
                                                                 </div>
                                                                 <div class="custext">
                                                                    <div class="text2">
                                                                       <p> <?php echo $wallReply->reply; ?></p>
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                        <?php
                                                                }
                                                            }
                                                        ?>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                            </div>

                                            <!---- Modal  --->
                                            <div id="myModalrepostlist<?php echo $z;?>" class="modal fade" role="dialog">
                                            <div class="modal-dialog" style="width: 700px;">
                                               <!-- Modal content-->
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                     <h3></h3>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row">
                                                        <div clss="col-md-12">
                                                        <?php
                                                            $wallRes = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareid=%d", $wall->id));
                                                            if($wallRes) {
                                                                foreach($wallRes as $wallRe) {
                                                                if($wallRe->shareid != 0) {
                                                                    $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $wallRe->user_id));
                                                            
                                                                    $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $wallRe->user_id));
                                                            
                                                                    $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $wallRe->user_id));

                                                        ?>   
                                                           <div class="col-md-6">
                                                            <div class="poll">
                                                               <div class="col-md-3 col-sm-2 col-xs-3">
                                                                  <div class="imgforr">
                                                                     <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-9 col-sm-10 col-xs-9">
                                                                  <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                  <p class="rigwd"> <?php echo $userDate;?> </p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                        <?php
                                                                }
                                                                }
                                                            }
                                                        ?>
                                                        </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                            </div>

                                    <div class="col-xs-3 col-md-4">
                                       <div class="share">
                                          <?php
                                               $uid = $_SESSION["login"]['id'];
                                               $checkLike = $wpdb->get_results( 
                                                 $wpdb->prepare( "SELECT * FROM wp_wall_like WHERE user_id=%d AND wallid=%d", $uid, $wall->id));
                                               
                                                if($checkLike){
                                             ?>
                                                  <form method="post" action="">
                                                     <input type="hidden" name="wallid" value="<?php echo $wall->id;?>">
                                                     <button type="submit" name="unlikeSubmit" class="unlikecolor"><strong><i class="fa fa-thumbs-up" aria-hidden="true"></i>like</strong></button>
                                                  </form>
                                          <?php
                                                } else{
                                             ?>
                                                   <form method="post" action="">
                                                     <input type="hidden" name="wallid" value="<?php echo $wall->id;?>">
                                                     <button type="submit" name="likeSubmit"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                                  </form>
                                          <?php
                                                }
                                          ?>
                                       </div>
                                    </div>
                                    <div class="col-xs-3 col-md-4">
                                       
                                    </div>

                                    <div class="col-xs-4 col-md-4">
                                        <div class="share1">
                                          <p data-toggle="modal" data-target="#myModalrep<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                       </div>
                                       <!-- Modal -->
                                       <div id="myModalrep<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h4 class="modal-title">Reply</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <form method="post" action="">
                                                      <input type="hidden" name="wallid" value="<?php echo $wall->id;?>">
                                                      <textarea name="replyPost"> </textarea>
                                                      <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!--<div class="share2">
                                          <p data-toggle="modal" data-target="#myModalrepost<?php //echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>
                                       </div>-->

                                           <!-- Modal -->
                                           <div id="myModalrepost<?php echo $z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       <h4 class="modal-title"> Repost </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                       <form method="post" action="">
                                                          <textarea name="reposttext" class="form-control"> </textarea>
                                                          <div class="row text2">
                                                            <div class="col-md-2 col-sm-2 col-xs-4">
                                                                <div class="imgforr">
                                                                   <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                </div>
                                                             </div>
                                                             <div class="col-md-10 col-sm-10 col-xs-8">
                                                                <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $ansPost->user_id;?>"><?php echo $userName[0]->name;?></a></strong>,<span><?php echo $userHeadline[0]->headline;?></span></p>
                                                                <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($lastAns[0]->created_at )); ?> </p>
                                                             </div>
                                                             <div class="col-md-12">
                                                                <div class="text2">
                                                                   <p><?php echo $wall->content; ?></p>
                                                                </div>
                                                             </div>
                                                             <input type="hidden" name="wallid" value="<?php echo $wall->id;?>">
                                                             <input type="hidden" name="walltext" value="<?php echo $wall->content;?>">
                                                             <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                          </div>
                                                    
                                                       </form>
                                                    </div>
                                                 </div>
                                              </div>
                                           </div>

                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php
                              $z++;
                              }
                            }
                           ?>
                        </div>

                        
                     </div>
                  </div>
               </div>
               <!-- #main -->
            </div>
         </div>
      </div>
      <div class="col-md-3">
         <div class="advtspace" style=""></div>
      </div>
      <!-- #primary -->
   </div>
</div>
<?php get_footer(); ?>
<script>
   var acc = document.getElementsByClassName("accordion");
   var i;
   
   for (i = 0; i < acc.length; i++) {
     acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
         panel.style.display = "none";
       } else {
         panel.style.display = "block";
       }
     });
   }
</script>
</div>
<?php
   function time_elapsed_string($datetime, $full = false) {
       $now = new DateTime;
       $ago = new DateTime($datetime);
       $diff = $now->diff($ago);
   
       $diff->w = floor($diff->d / 7);
       $diff->d -= $diff->w * 7;
   
       $string = array(
           'y' => 'year',
           'm' => 'month',
           'w' => 'week',
           'd' => 'day',
           'h' => 'hour',
           'i' => 'minute',
           's' => 'second',
       );
       foreach ($string as $k => &$v) {
           if ($diff->$k) {
               $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
           } else {
               unset($string[$k]);
           }
       }
   
       if (!$full) $string = array_slice($string, 0, 1);
       return $string ? implode(', ', $string) . ' ago' : 'just now';
   }
   ?>
<script> 
   function openCity(evt, cityName) {
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("tabcontent");
     for (i = 0; i < tabcontent.length; i++) {
       tabcontent[i].style.display = "none";
     }
     tablinks = document.getElementsByClassName("tablinks");
     for (i = 0; i < tablinks.length; i++) {
       tablinks[i].className = tablinks[i].className.replace(" active", "");
     }
     document.getElementById(cityName).style.display = "block";
     evt.currentTarget.className += " active";
   }
   
   function openReport(evt, reportName) {
     var i, repocontent, repolinks;
     repocontent = document.getElementsByClassName("repocontent");
     for (i = 0; i < repocontent.length; i++) {
       repocontent[i].style.display = "none";
     }
     repolinks = document.getElementsByClassName("repolinks");
     for (i = 0; i < repolinks.length; i++) {
       repolinks[i].className = repolinks[i].className.replace(" active", "");
     }
     document.getElementById(reportName).style.display = "block";
     evt.currentTarget.className += " active";
   }
</script>

<div id="myModalmemb1post" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h1>Want to Post Any Update?</h1>
         </div>
         <div class="modal-body">
            <form method="post" action="">
               <textarea name="walltext"></textarea>
               <input type="submit" name="wallsubmit" value="Submit">
            </form>
         </div>
      </div>
   </div>
</div>


