<div class="eventnew">
<?php
  /**
  /*
    Template Name:Student1
  */
        
get_header(); ?>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;
?>
   <div class="catchybga">
      <div class="want">
        
          <h2><p>Write & Earn</p></h2>
    
         <p>"I'm writing my story so that others might see fragments of themselves"<span>- Lena Waithe</span></p>
     
      <div class="belowlinksedus">
      <ul>
        <li><a href="<?php if($_SESSION["login"]["id"]){echo "https://edukeeda.com/student-query-form/";}else{echo "#";}?>" <?php if($_SESSION["login"]["id"]){}else{ echo "data-toggle='modal' data-target='#myModalpost'"; }?>>Post Story/Experience</a></li>.
        <li><a href="#section1000" id="2000" class="click" "="">Explore</a></li>.
        <li><a href="https://edukeeda.com/discussion-forum/">Discuss</a></li>
      </ul>
     </div>
     
      </div>
      <div class="wantpost">
         
      </div>
   </div>
  <div class="simplegrid" id="section1000">
      <div class="insidecontainer">
      <div class="stu">
         <h1> Stories/Experiences </h1>
         <div class="ardo">
            <div class="col-md-4">
               <div class="cont">
                  <a href="https://edukeeda.com/success-story/">
                     <p>Success Story</p>
                  </a>
               </div>
            </div>
            <div class="col-md-4">
               <div class="cont">
                  <a href="https://edukeeda.com/inside-campus-story/">
                     <p>Inside Campus Story</p>
                  </a>
               </div>
            </div>
            <div class="col-md-4">
               <div class="cont">
                  <a href="https://edukeeda.com/training-placement-experiences/">
                     <p>Training / Placement Experiences</p>
                  </a>
               </div>
            </div>
            <div style="height:300px;"></div>
          <?php 
              global $wpdb;
              $check = $wpdb->get_results("SELECT * FROM wp_student_setting WHERE id=1");
              if($check[0]->corner_hidden == 1) {
          ?>


            <div class="conhed">
               <h2> Exam Corner-India</h2>
            </div>
            <div class="col-md-4">
               <div class="cont">
                  <a href="https://edukeeda.com/list-of-competitive-examinations-details/">
                     <p>List Of Competitive Exam</p>
                  </a>
               </div>
            </div>
            <div class="col-md-4">
               <div class="cont">
                  <a href="https://edukeeda.com/previous-year-papers/">
                     <p>Download Previous Years Paper</p>
                  </a>
               </div>
            </div>
            <div class="col-md-4">
               <div class="cont">
                <a href="https://edukeeda.com/studentmore/">
                     <p>Practice Exercise &amp; Tests by experts</p>
                 </a>
               </div>
            </div>
         </div>
         <!--<div class="col-md-12">
            <div class="stud">
               <h1>Practice Exercise &amp; Tests by experts</h1>
               <a href="https://edukeeda.com/studentmore/">More</ahref>
            </div>
         </div>-->
        <?php
          }
        ?>
        
      

        
        
      </div>
    </div>
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
</div>  
<?php get_footer(); ?>
</div>
<style>
.stud a{
    float: right;
    margin: 0px 125px;
    background: #060658;
    color: #fff;
    padding: 7px 25px;
    font-size: 17px;
    border-radius: 50px;
}
.want h2 p {
    font-size: 22px;
    position: absolute;
    top: 100%;
    left: 44%;
    color: #fff;
}
.want span {
    color: #f68e2f;
}
.want p {
    font-size: 20px;
    position: absolute;
    left: 25%;
    top: 325px;
}
   .stu .active:after {
   content: "\2212";
   color: #7171d2;
   font-weight: bold;
   float: right;
   margin-left: 5px;
   font-size: 40px;
   }
   .stu h1 {
   font-size: 25px;
   font-weight: 600;
   margin-bottom: 50px;
   margin-left: 18px;
   }
   .conhed h2 {
   font-size: 25px;
   font-weight: 600;
   margin-bottom: 50px;
    margin-left: 18px;
   }
   .conhed {
   float: left;
   width: 100%;
   }
   .student button.accordion {
   display: block;
   }
   .student button.accordion.active {
   background: transparent;
   border: none;
   color: black;
   font-size: 16px;
   }
   .student button.accordion {
   background: transparent;
   border: none;
   color: black;
   font-size: 16px;
   }
   .active:after {
   content: "\2212";
   }
   .accordion:after {
   content: "\002B";
   color: #7171d2;
   font-weight: bold;
   float: right;
   margin-left: 5px;
   font-size: 40px;
   }
   .student li {
   text-align: left;
   margin: 8px 18px;
   }
   .student p {
   text-align: left;
   }
   .student2 p {
   text-align: center;
   }
   .jeem {
   float: left;
   width: 100%;
   margin-bottom: 60px;
   }
   .jee p {
   margin: 0px 0px 0px 64px;
   }
   .jeem p {
   margin: 0px 43px;
   font-size: 25px;
   font-weight: 600;
   }
   .jeem a {
   color: #060658;
   }
   .jee {
   float: left;
   width: 100%;
   margin-bottom: 60px;
   }
   .jee h3{
   font-size: 24px;
   font-weight: 600;
   color: #54595f;
   margin: 10px 50px;
   }
   .student1 a {
   background: #439a37;
   color: #fff;
   padding: 6px 18px;
   font-size: 16px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .ardo {
   float: left;
   width: 100%;
   }
   .student1 {
   text-align: center;
   }
   .student .panel {
   box-shadow: none;
   }
   .student h3 {
   font-size: 24px;
   font-weight: 600;
   color: #6ec1e4;
   margin: 10px 50px;
   }
   .stud {
   float: left;
   width: 100%;
   margin: 29px;
   }
   .stud h1 {
   font-size: 32px;
   float: left;
   font-weight: 600;
   color: #6ec1e4;
   }
   .want a {
   border-radius: 10px;
   background: #f68e2f;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .wantpost a {
   border-radius: 10px;
   background: #f68e2f;
   color: #ffffff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .cont p {
   font-size: 17px;
   font-weight: 600;
   margin: 5px 0px;
   padding: 50px 0;
   color: #060658;
   }
   .cont {
   float: left;
   width: 100%;
   text-align: center;
   padding: 10px;
   box-shadow: 5px 6px 4px 0 rgba(0, 0, 0, 0.07);
   border: 1px solid #f68e2f;
   border-radius: 10px;
   margin: 5px 0px;
   }
   .even1 img {
   width: 100%;
   border-radius: 10px 10px 10px 10px;
   max-height: 200px;
   height: 180px;
   }
   .altimg img {
   border-radius: 7px 7px 0px 0px;
   }
   .EVEN {
   float: left;
   width: 100%;
   margin-bottom: 35px;
   }
   span.dateDisplay-day {
   color: #f68e2f;
   font-size: 20px;
   font-weight: 800;
   }
   span.dateDisplay-month.text--tiny {
   color: #020253;
   font-size: 20px;
   font-weight: 800;
   }
   .spn img {
   border-radius: 60%;
   width: 56px;
   height: 56px;
   }
   .spn a {
   color: black;
   }
   .spn a:hover {
   color: orange;
   }
   .alt{
   box-shadow: 0 0 2px 0 rgba(46,62,72,.12), 0 2px 4px 0 rgba(46,62,72,.12);
   border-radius: 0px 0px 10px 10px;
   float: left;
   }
   .odd {
   float: left;
   width: 100%;
   margin-bottom: 60px;
   margin-top: 30px;
   }
   div#sidebar-footer {
   float: left;
   width: 100%;
   }
   .eventhead a {
   float: right;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 79px;
   }
   .eventnew .container2 {
   margin: 0 auto;
   max-width: 1170px;
   }
   .eventnew .content-wrapper.container {
   width: 100%;
   margin: 0 auto;
   padding: 0 ;
   }
   
   .eventnew .content-wrapper.container > .row{
     margin:0;
   } 
   .belowlinksedus ul li a
   {
     font-size: 30px!important;
   }
   
   
   .catchybga {
   padding: 14% 0;
   background: url(/img/student-corner-45.png);
   background-size: cover;
   background-position: 100%;
   position: relative;
   margin-bottom: 50px;
   }
   .want {
      text-align: left;
    color: white;
    position: absolute;
    top: 0%;
    width: 100%;
   }
   .want h3 {
   color: white;
   font-size: 22px;
   font-weight: 100;
   margin-bottom: 0px;
   }
   .belowlinksedus {
   position: absolute!important;
    bottom: 0;
    right: 0;
    left: 33%!important;
    top: 150px;
}
   .want a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .wantpost {
   TEXT-ALIGN: CENTER;
   }
   .wantpost p {
   color: white;
   font-size: 20px;
   position: ABSOLUTE;
   left: 0;
   right: 0;
   margin: 10 AUTO;
   FLOAT: NONE;
   WIDTH: AUTO;
   BOTTOM: 0px;
   }
   .wantpost a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
  
   .stu {
   float: left;
   width: 100%;
   }
   .page-wrap{
         padding-top: 68px;
   }
   @media only screen and (max-width: 767px)
   {
   .want h3 {
   color: white;
       font-size: 11px;
    font-weight: 100;
    padding: 5px;
   }
.want h2 p {
    font-size: 13px;
    position: absolute;
    top: 100%;
    left: 40%;
    color: #fff;
}
   .page-wrap {
    padding-top: 0px;
}
.belowlinksedus ul li a {
    font-size: 13px!important;
}
   .wantpost p {
   color: white;
   font-size: 17px;
   position: ABSOLUTE; 
   margin: 0 AUTO;
   BOTTOM: 0px;
   font-weight: unset;
   }
   .want p {
       font-size: 12px;
    position: absolute;
    top: 519%;
    left: 32px;
    line-height: 18px;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 20px;
   }
   .eventhead h1 {
   font-size: 31px;
   float: left;
   margin: 0 0 20px 10px;
   }
   .eventhead a {
   float: right;
   MARGIN: 0px 19px;
   }
   .alt{
   margin-bottom: 12px;
   }
   .hentry .meta-post{
     margin-bottom:0px!important;
   }
   .stu h1 {
   text-align:center;
   font-size: 20px;
    font-weight: 600;
   }
   .conhed h2{
     text-align:center;
   }
   .ardo .col-md-4 {
    padding: 0;
}
.belowlinksedus {
    position: absolute!important;
    bottom: 0;
    right: 0;
    left: 11%!important;
    top: 290%!important;
}
.catchybga{
  padding: 23% 0;

}
   }
</style>
<script>
   var acc = document.getElementsByClassName("accordion");
   var i;
   
   for (i = 0; i < acc.length; i++) {
     acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.display === "block") {
         panel.style.display = "none";
       } else {
         panel.style.display = "block";
       }
     });
   }
</script>
<!-- Modal -->
<div id="myModalpost" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Posting</h4>
         </div>
         <div class="modal-body">
            <p> For Posting please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>
