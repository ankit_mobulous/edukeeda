<div class="eventnew">
   <?php
      /**
      /*
      Template Name:faq
      */
      get_header();
      ?>
  
   <div class="about">
      <div class="container">
         <div class="row">
           <div class="pageheading"> 
         <h1 style=" font-size: 45px;
    ">FAQs</h1>
    </div>
            
            <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php endwhile; // end of the loop. ?>
         </div>
      </div>
   </div>
   <?php get_footer(); ?>
</div>
<style>
  .pageheading h1:before {
    content: "";
    width: 120px;
    height: 9px;
    background: #060658;
    position: absolute;
    bottom: -8px;
    left: 0;
}
button.btn.btn-link {
    width: 100%;
    text-align: left;
    color: #000;
    font-size: 20px;
    margin: 0px;
    font-weight: 700;

    border-bottom: 1px solid #ddd;
}

div#heading10 button.btn.btn-link.collapsed:after {
    content: "\2227";
    font-family: 'Glyphicons Halflings';
    color: #080859;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    font-size: 16px;
    background: #f68e2f;
    padding: 2px 10px;
    border-radius: 100px;
    height: 30px;
    width: 30px;
    position: absolute;
    right: 1.2%;
    top: 30%;
    }
div#heading10 button.btn.btn-link:after {
    font-family: 'Glyphicons Halflings';
    content: "\2228";
    color: #080859;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    font-size: 16px;
    background: #f68e2f;
    padding: 2px 10px;
    border-radius: 100px;
    height: 30px;
    width: 30px;
    position: absolute;
     right: 1.2%;
    top: 30%;
}
div#heading0 button.btn.btn-link.collapsed:after {
    content: "\2227";
    font-family: 'Glyphicons Halflings';
    color: #080859;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    font-size: 16px;
    background: #f68e2f;
    padding: 2px 10px;
    border-radius: 100px;
    height: 30px;
    width: 30px;
    position: absolute;
   top: 30%;
    right: 1%;
    }
div#heading0 button.btn.btn-link:after {
    font-family: 'Glyphicons Halflings';
    content: "\2228";
    color: #080859;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    font-size: 16px;
    background: #f68e2f;
    padding: 2px 10px;
    border-radius: 100px;
    height: 30px;
    width: 30px;
    position: absolute;
    top: 30%;
    right: 1%;
}
.card-body {
  padding: 20px 20px;
    font-size: 18px;
    line-height: 25px;
    text-align: initial;
    padding-bottom: 20px;
}

.card.z-depth-0.bordered {
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
    border-top: 1px solid #ddd;
    background: #ddd;
    margin: 20px 0px;
}
h5.mb-0 {
    margin: 0px;
}
div#collapseOne {
    background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapseTwo{
  background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapseThree{
    background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapse4{
    background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapse5{
    background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapse6{
    background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapse7{
    background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapse8{
    background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapse9{
    background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapse10{
    background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapse11{
    background: #fff;
    border-bottom: 1px solid #ddd;
}
div#collapse0{
    background: #fff;
    border-bottom: 1px solid #ddd;
}
.pageheading h1:after {
    content: "";
    width: 26px;
    height: 9px;
    background: #f68e2f;
    position: absolute;
    bottom: -8px;
    left: 0;
    z-index: 1;
}
.pageheading {
    position: relative;
    margin: 57px 0px;
}
   
   .about h4 {
   margin: 20px 0;
   font-family: inherit;
   font-weight: 500;
   line-height: 1.1;
   color: inherit;
   text-align:center;
   color:#0a0a56;
   font-size: 33px;
   }
   .about h3 {
   color: #000;
   display: inline-block;
   margin: 0px 0 0px;
   font-weight: normal;
   font-size: 20px;
   padding-right: 20px;
   text-transform: initial;
   }
 .about  span {
    margin-left:30px;
    font-size: 21px;
    font-weight: 600;
}
   .about p {
 padding: 10px 0px;
    text-align: initial;
}
   
   .about ul li {
   line-height: 25px;
   list-style-type: none;
   margin-bottom: 15px;
   margin-left: 20px;
   }
   .about ul li::before
   {
   background: #09e2b9 none repeat scroll 0 0;
   border-radius: 50%;
   content: "";
   display: inline-block;
   height: 6px;
   margin-left: -20px;
   margin-right: 10px;
   width: 6px;
   }
   .eventnew .content-wrapper.container > .row {
   margin: 0;
   background:#f5f5f5;
   }
   .eventnew .content-wrapper.container {
   max-width: 100%;
   width: 100%;
   padding: 0 !important;
   }
   .page-wrap {
   padding-top: 75px;
   }
   .about {
   float: left;
   width: 100%;
   margin-bottom: 100px;
   background: #f5f5f5;
   }
   button.btn.btn-link:after {
      font-family: 'Glyphicons Halflings';
    content: "\2228";
    color: #080859;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    font-size: 16px;
    background: #f68e2f;
    padding: 2px 10px;
    border-radius: 100px;
        height: 30px;
    width: 30px;
}
   button.btn.btn-link.collapsed:after {
   content: "\2227";
   font-family: 'Glyphicons Halflings';
   color: #080859;
   font-weight: bold;
   float: right;
   margin-left: 5px;
     font-size: 16px;
    background: #f68e2f;
    padding: 2px 10px;
    border-radius: 100px;
        height: 30px;
    width: 30px;
   }
   .accordion {
  
    color: #444;
    cursor: pointer;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
    margin: 15px 0px;
   }
  
  .btn-link:focus, .btn-link:hover {
    text-decoration: none;
    outline: none;
}
.btn.active.focus, .btn.active:focus, .btn.focus, .btn:active.focus, .btn:active:focus, .btn:focus {
  outline: 0px auto -webkit-focus-ring-color; 
    outline-offset: -2px;
}
   .about .row {
   width: 1000px;
   float: none;
   margin: 0 auto;
   }
   @media only screen and (max-width: 767px)
   {
   .about .row {
   width: 100%;
   }
   .content span
   {
   width: 100%;
   }
   .content span
   {
   padding: 35px 0;
   }
   .page-wrap {
   padding-top: 0px;
   }
   .catchybga {
   margin-top: 0px;
   }

button.btn.btn-link {
    width: 100%;
    text-align: left;
    color: #000;
    font-size: 15px;
    margin: 0px;
    font-weight: 500;
        white-space: inherit; 
    border-bottom: 1px solid #ddd;
}

div#heading10 button.btn.btn-link.collapsed:after {
    content: "\2227";
    font-family: 'Glyphicons Halflings';
    color: #080859;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    font-size: 16px;
    background: #f68e2f;
    padding: 2px 10px;
    border-radius: 100px;
    height: 30px;
    width: 30px;
    position: absolute;
  right: 3.2%;
    top: 65%;
    }
div#heading10 button.btn.btn-link:after {
    font-family: 'Glyphicons Halflings';
    content: "\2228";
    color: #080859;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    font-size: 16px;
    background: #f68e2f;
    padding: 2px 10px;
    border-radius: 100px;
    height: 30px;
    width: 30px;
    position: absolute;
    right: 3.2%;
    top: 65%;
}
div#heading0 button.btn.btn-link.collapsed:after {
    content: "\2227";
    font-family: 'Glyphicons Halflings';
    color: #080859;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    font-size: 16px;
    background: #f68e2f;
    padding: 2px 10px;
    border-radius: 100px;
    height: 30px;
    width: 30px;
    position: absolute;
   right: 3.2%;
    top: 43%;
    }
div#heading0 button.btn.btn-link:after {
    font-family: 'Glyphicons Halflings';
    content: "\2228";
    color: #080859;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    font-size: 16px;
    background: #f68e2f;
    padding: 2px 10px;
    border-radius: 100px;
    height: 30px;
    width: 30px;
    position: absolute;
   right: 3.2%;
    top: 43%;
}
.about span {
    margin-left: 0px;
    font-size: 16px;
    font-weight: 600;
}
.about strong{
  font-size: 18px;
}
.pageheading {
    position: relative;
    margin: 100px 0px;
}
.about p{
  line-height: 30px;
      padding: 16px 0px;
}

   }


</style>
<script>
   var acc = document.getElementsByClassName("accordion");
   var i;
   
   for (i = 0; i < acc.length; i++) {
     acc[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var panel = this.nextElementSibling;
       if (panel.style.maxHeight){
         panel.style.maxHeight = null;
       } else {
         panel.style.maxHeight = panel.scrollHeight + "px";
       } 
     });
   }
</script>


