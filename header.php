<?php
session_start();
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Sydney
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>"> 
<meta charset="ISO-8859-5">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property=’og:url’ content=”/>
<meta property=’og:type’ content=”/>
<meta property=’og:title’ content=”/>
<meta property=’og:image’ content=”/>
<meta property=’og:description’ content=”/>

  <script src="/wp-content/themes/sydney/custome1/owl.carousel.min.js"></script>
  <script src="<?php echo site_url(); ?>/wp-content/themes/sydney/custom2/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  
<link rel="icon" type="image/png" sizes="16x16" href="https://edukeeda.com/wp-content/uploads/2018/11/edukeedanew.png">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) : ?>
  <?php if ( get_theme_mod('site_favicon') ) : ?>
    <link rel="shortcut icon" href="<?php echo esc_url(get_theme_mod('site_favicon')); ?>" />
  <?php endif; ?>
<?php endif; ?>

<?php wp_head(); ?>
<script type="text/javascript">
  template_directory_uri = "<?php echo site_url(); ?>";
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.8.0/highlight.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.8.0/languages/go.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
<style>
.wpuf_customs{display:none!important;}
.entry-header h4{text-align: justify;}
.dropbtn {
  background-color: #4CAF50;
  color: white;
  padding: 16px;
  font-size: 16px;
  border: none;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #ddd;}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {background-color: #3e8e41;}
.pimg p strong a {
    text-transform: uppercase;
}
.author.vcard.space {
    text-transform: none!important;
}
.elementor-image img{width: 119px;height: 100px;}
.nav-links .nav-previous{width: 100%!important;float:none!important;}
.nav-links .nav-next{width: 100%!important;float:none!important;}
.post-navigation .screen-reader-text{font-size: 12px!important;width: 89px!important;overflow: inherit!important;position: inherit!important;}
.screen-reader-text:hover, .screen-reader-text:active, .screen-reader-text:focus{padding: 0px!important;color: #000!important;box-shadow: 0px 0px 0px 0px!important;background-color: #f7f7f7!important;left: 0px!important;top: 0px!important;}
.post-navigation a {color: #f79800!important;}
.hrtagstyle{
  border-top: 1px solid #000!important;
}
.comment-respond{display:none;}
.author.vcard{margin-right:6px!important;}
.author.vcard.space{margin-left:6px!important;}
.italicfontdesc p{font-style: italic;
font-weight: 600;text-align: justify;}
.heateor_sss_sharing_container.heateor_sss_vertical_sharing.heateor_sss_bottom_sharing{display:none;}
.searchform input {
height: 36px!important;}
.comments-area .comment-title{display:none;}
#respond .comment-reply-title{display:none;}
.comment-body .reply{display:none;}
.entry-content p{
margin-bottom: 0px;
font-size: 18px;
font-weight: 400;
font-family: Roboto, sans-serif;
line-height: 37px;
text-align: justify;
}
.rightoption {
float: right;
margin-bottom: 26px;
}
.colh.singlepost {
float: left;
width: 100%;
margin-bottom: 26px;
}
.colh.singlepost h4 {
float: left;
}
.colh.singlepost input.btn.btn-warning.followbtn {
padding: 3px 25px;
}
div#secondary a{

line-height:15px;
}
.leftwd a {
color: #9e9292;
}
.pimg span {
font-size: 16px!important;
line-height: 16px!important;
COLOR: #aa9292;
} 
p.leftwd strong{
width: 100%;
}
.pimg strong {
font-weight: 600;
line-height: 13px;
}
p.leftwd {
color: #9e9292;
}
.pimg span a {
color: #aa9292!important;
}
.pimg a {
line-height: 15px;
float: left;
width: 100%;
margin: 0px 0px!important;
}
.pimg span {
margin-top: 5px;
display: inline-flex;
}
.commenttoggle #cmtToggleButton{    margin-left: 29%;}
.pimg span a {float:none;}
.entry-thumb .entry-thumb{margin-top: 10px;}
.author.vcard.space{text-transform: capitalize;}
.commenttoggle .btn-lg {
padding: 10px 16px;
font-size: 18px;
line-height: 1.3333333;
border-radius: 6px;
float: none!important;
width: 257px;
left: 35%;
}
.logouts {
position: absolute;
top: 20px;
left: 97%;
}
.logouts a {
padding: 8px;
margin: 0px;
border: 1px solid#f68e2f;
background: #f68e2f;
color: #fff;
border-radius: 4px;
}
.logouts a:hover {

margin: 0px;
border: 1px solid#f68e2f;
background: #fff;
color: #f68e2f;
border-radius: 4px;
}

@media only screen and (max-width: 767px) {

.logouts {
position: initial;
margin-top: 7px;
}
.commenttoggle .btn-lg {
padding: 10px 16px;
font-size: 18px;
line-height: 1.3333333;
border-radius: 6px;
float: none!important;
width: 257px;
left: 0%!important;
}
.postshare1 .heateor_sss_sharing_container {
margin-left: 50px!important;
}
.rightoption p {
float: left;
margin: 0px 29px;
}
.postshare1 {
text-align: center;
}
.rightoption p {
margin: 0px 0px;
font-size: 20px;
font-weight: 700;
}
.rightoption select {
background: #fff;
border: 1px solid #ddd;
height: 35px;
line-height: 3px;
padding: 1px 55px;
margin: 1px 0px 8px 0px;
width: 70%;
float: right;
}
.rightoption p {
margin: 0px 0px!important;
}
.searchform input {
height: 36px!important;
padding: 4px 13px!important;
}
.searchform button {
margin: 0px 3px;
}
.tophdsa {
float: left;
margin: 0px 45px;
}

}
.entry-footer .edit-link{display:none;}
ol.comments-list{display:none;}
.cmmtcheck{display:none;}
.must-log-in{display:none;}
.dropdown:hover .dropbtn {
    background-color: #fff;
    color: #f68e2f;
    border: 1px solid #f68e2f;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: #f68e2f;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown:hover .dropdown-content {display: block;}

.dropdown:hover .dropbtn {
      background-color: #fff;
    color: #f68e2f;
  }
</style>
<script>
    $(document).ready(function(){
        $("#cmtToggleButton").on("click",function(){
            $(".comment-respond").toggle(1000);
            $("ol.comments-list").toggle(1000);
            $(".cmmtcheck").toggle(1000);
        });
    });
    $(document).ready(function(){
        $(".post-navigation .screen-reader-text").html("Read Also");
    });
</script>
</head>

<body <?php body_class(); ?> <?php if($_GET["action2"]){ echo 'onload="setCountDown();"';}?>>

<?php do_action('sydney_before_site'); //Hooked: sydney_preloader() ?>

<div id="page" class="hfeed site">
  <a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'sydney' ); ?></a>

  <?php do_action('sydney_before_header'); //Hooked: sydney_header_clone() ?>

  <header id="masthead" class="site-header" role="banner">
    
    <div class="header-wrap">
            <div class="container">
                <div class="row">
        <div class="col-md-3 col-sm-8 col-xs-9">
            <?php if ( get_theme_mod('site_logo') ) : ?>
          <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo('name'); ?>"><img class="site-logo" src="<?php echo esc_url(get_theme_mod('site_logo')); ?>" alt="<?php bloginfo('name'); ?>" /></a>
            <?php else : ?>
          <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
          <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>         
            <?php endif; ?>
        </div>
        <div class="col-md-9 col-sm-4 col-xs-3 mycustmenu">
          <div class="btn-menu"></div>
          <nav id="mainnav" class="mainnav" role="navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'fallback_cb' => 'sydney_menu_fallback' ) ); ?>
      <div class="tophdsa">              
      <div class="dropdown">
        <?php 
          if(isset($_SESSION['login'])){
            $userID = $_SESSION['login']["id"];
            $userName = $wpdb->get_results($wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d", $userID));
            $userPic = $wpdb->get_results($wpdb->prepare("SELECT profilePic FROM wp_user_pics WHERE user_id=%d", $userID));
        ?>
            <a href="<?php echo site_url(); ?>/all-members/homepage?action=<?php echo $userID;?>">
              <button class="dropbtn">
                  <img src="<?php if($userPic){echo $userPic[0]->profilePic;}else{echo "https://edukeeda.com/wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>" style="width: 36px;height: 33px;border-radius: 18px;"> <?php echo "Welcome";//echo $userName[0]->name; ?>
              </button>
            </a>

            <div class="dropdown-content">
                <a href="#" data-toggle="modal" data-target="#myModal_pass">Change Password</a>
                <a href="<?php echo site_url(); ?>/wp-content/themes/sydney/logoutuser.php">Logout</a>
            </div>
        <?php 
          }else{
        ?>
            <a href="<?php echo site_url(); ?>/signin/"><button class="dropbtn">Sign In / Sign Up</button></a>
        <?php } ?>
            
      </div>
    </div>
    
  </nav><!-- #site-navigation -->
        </div>
          
        </div>
      </div>
    </div>
  </header><!-- #masthead -->

  <?php do_action('sydney_after_header'); ?>

  <div class="sydney-hero-area">
    <?php sydney_slider_template(); ?>
    <div class="header-image">
      <?php sydney_header_overlay(); ?>
      <img class="header-inner" src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>">
    </div>
    <?php sydney_header_video(); ?>

    <?php do_action('sydney_inside_hero'); ?>
  </div>

  <?php do_action('sydney_after_hero'); ?>

  <div id="content" class="page-wrap">
    <div class="container content-wrapper">
      <div class="row"> 

