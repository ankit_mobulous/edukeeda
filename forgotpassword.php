<?php
ob_clean(); ob_start();
/*
Template Name: Forgot Password
*/
get_header();
?>
<?php
   session_start();
?>

<div id="primary" class="content-area">
   <div class="row signpagsecss">
      <div class="col-md-3">
      </div>
      <div class="col-md-6 signupcontainer">
         <h5>Forgot Password </h5>
         <?php
            if(isset($_SESSION["success"])) {
         ?>
                <p><center> <?php echo $_SESSION["success"];?> </center></p>
         <?php
            } else if(isset($_SESSION["failed"])) {
         ?>
                <p><center> <?php echo $_SESSION["failed"];?> </center></p>
         <?php
            }
         ?>
         <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/forgotpassword-code.php" method="post" id="postarticle">
            <p>Email (required)<br>
               <input type="email" placeholder="Email" name="p_email" id="p_email"> 
            </p>
            <input type="submit" value="Submit" name="submit">
         </form>
      </div>
   </div>
   <style>
      header.entry-header {
      display: none;
      }
      .signpagecss input{
      height: 26px;
      margin: 0 0 10px 0;
      background: transparent;
      border: none;
      border-bottom: 1px solid #ddd;
      width: 100%;
      }
      .signpagecss p {
      margin: 0;
      font-size:14px;
      }
      .signpagecss input{
      padding: 5px 20px;
      }
      .signupcontainer {
      background: #fff;
      box-shadow: #ded8d5 0px 0px 8px 0px;
      padding: 23px;
      margin: 0 18px;
      width: 45%;
      height: 100%;
      float: left;
      }
      div#content {
      background: url(http://edukeeda.com/wp-content/uploads/2018/10/edukeeda-web.png);
      padding: 20px;
      }
      .container.content-wrapper {
      background: rgba(255,255,255,0.9);
      padding: 48px 5% !important;
      border: 1px solid #4848;
      }
      .signupcontainer label{
      float: left;
      width: 100%;
      }
      .signupcontainer select{
      width: 100%;
      float: left;
      display: block;
      background: transparent;
      height: 30px;
      margin-bottom: 10px;
      }
      .signupcontainer input[type="date"] {
      width: 100%;
      background: #fff;
      height: 30px !important;
      }
      @media only screen and (max-width: 767px){
      .signupcontainer {
      margin: 15px 0px;
      width: 100%;
      height: 100%;
      }
      }
      .signupcontainer input[type="text"], .signupcontainer input[type="password"], .signupcontainer input[type="email"]{
      width: 100%;
      background: #fff;
      height: 32px;
      }
   </style>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
   <script type="text/javascript">
         jQuery(document).ready(function($){
            // validate signup form on keyup and submit
            $("#postarticle").validate({
               rules: {
                  p_email: {
                     required: true,
                     email: true,
                  },
               },
               messages: {
                  p_email: {required:"Please enter a valid email address"},
               }
            });
      
         });
   </script>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>

