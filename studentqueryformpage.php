<div class="Postyourpage">
<?php
session_start();
/**
/*
 Template Name: student Query Form Page
 */
if(isset($_SESSION["posterror"])) {
    echo "<script>alert('Minimum 250 words required in description');</script>";
    unset($_SESSION["posterror"]);
}
if($_SESSION["login"]) {

} else {
  header('Location: https://edukeeda.com/signin/');
}  
$userId = $_SESSION["login"]["id"];
get_header(); ?>
   <div class="col-md-12 col-sm-12">
      <div class="filteropt">
         <p class="text-center">Student Corner</p>
         <p style="font-size: 20px;">Want to post on any InsideCampus Story or Training / Placement Experiences?  
            <select id="selectPost"> 
                <option value=""> Select </option>
                <option value="campus"> InsideCampus Story </option>
                <option value="training"> Training & Placement Experiences </option>
            </select>
         </p>
         <div class="row">
           <div class="col-md-10 col-md-offset-1">
              <div class="panel panel-warning" style="display: none;" id="campusshow">
                <div class="panel-heading">InsideCampus Story</div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12">
                      <?php //echo  do_shortcode('[wpuf_form id="3256"]');?>
                      <form method="post" action="<?php echo site_url(); ?>/wp-content/themes/sydney/studentquerycode.php" enctype="multipart/form-data" id="myformcampus">
                          <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" name="title" class="form-control" id="title" required="required">
                          </div>
                          <div class="form-group">
                            <label for="title">Short Description (Write best suited 1 to 3 lines to highlight appropriate subject/concept/summary of your post):</label>
                            <?php
                                // $my_content1 =" ";
                                // $editor_id1 = 'shortDesc';
                                // $option_name1 ='shortDesc';
                                // wp_editor($my_content1, $editor_id1, array('wpautop'=> true,'textarea_name' => $option_name1, "media_buttons" => true,'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                              $content = '';
                              $editor_id = 'shortDesc';
                              $settings =   array(
                                  'wpautop' => true, // use wpautop?
                                  'media_buttons' => true, // show insert/upload button(s)
                                  'textarea_name' => $editor_id, // set the textarea name to something different, square brackets [] can be used here
                                  'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
                                  'tabindex' => '',
                                  'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
                                  'editor_class' => '', // add extra class(es) to the editor textarea
                                  'teeny' => false, // output the minimal editor config used in Press This
                                  'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
                                  'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                                  'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
                                  'drag_drop_upload' => true
                              );
                              wp_editor( $content, $editor_id, $settings = array());
                            ?>
                          </div>

                          <div class="form-group">
                            <label for="title">Description (Write Minimum 450 Words For A Good Post):</label>
                            <?php
                                // $my_content2 =" ";
                                // $editor_id2 = 'desc';
                                // $option_name2 ='desc';
                                // wp_editor($my_content2, $editor_id2, array('wpautop'=> true,'textarea_name' => $option_name2, "media_buttons" => true, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                              $editor_id1 = 'desc';
                              wp_editor( $content, $editor_id1, $settings = array() ); 
                            ?>
                          </div>
                          <div class="form-group">
                            <label for="title">Upload Image (Min 200 KB, Max 1 MB):</label>
                            <input type="file" name="fileCampus" class="form-control" id="fileCampusimg" accept="image/*"  required="required">
                          </div>
                          <div class="form-group">
                            <label for="title">Campus:</label>
                            <input type="text" name="campus" class="form-control" id="campus"  required="required">
                          </div>
                          <div class="form-group">
                              <input type="submit" name="submitcampus" value="Submit" class="btn btn-primary">
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel panel-warning" style="display: none;" id="trainingshow">
                <div class="panel-heading">Training & Placement Experiences</div>
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-12">
                      <?php //echo  do_shortcode('[wpuf_form id="3277"]');?>
                      <form method="post" id="train" action="<?php echo site_url(); ?>/wp-content/themes/sydney/studentquerycode.php" enctype="multipart/form-data">
                          <div class="form-group">
                            <label for="title">Title:</label>
                            <input type="text" name="title1" class="form-control" id="title1"  required="required">
                          </div>
                          <div class="form-group shortDesc">
                            <label for="title">Short Description (Write best suited 1 to 3 lines to highlight appropriate subject/concept/summary of your post):</label>
                            <?php
                                // $my_content3 =" ";
                                // $editor_id3 = 'shortDesc1';
                                // $option_name3 ='shortDesc1';
                                // wp_editor($my_content3, $editor_id3, array('wpautop'=> true,'textarea_name' => $option_name3, "media_buttons" => true, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true)); 
                            $editor_id1 = 'shortDesc1';
                              wp_editor( $content, $editor_id1, $settings = array() );
                            ?>
                          </div>
                          <div class="form-group">
                            <label for="title">Description (Write Minimum 450 Words For A Good Post):</label>
                            <?php
                                // $my_content4 =" ";
                                // $editor_id4 = 'desc1';
                                // $option_name4 ='desc1';
                                // wp_editor($my_content4, $editor_id4, array('wpautop'=> true,'textarea_name' => $option_name4, "media_buttons" => true, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true)); 
                            $editor_id1 = 'desc1';
                              wp_editor( $content, $editor_id1, $settings = array() );
                            ?>
                          </div>
                          <div class="form-group">
                            <label for="title">Upload Image (Min 200 KB, Max 1 MB):</label>
                            <input type="file" name="filetraining" class="form-control" id="short1" accept="image/*" required="required">
                          </div>
                          <div class="form-group">
                            <label for="title">Type of the Training / Recruitment Process alongwith Company Name:</label>
                            <input type="text" name="trainingIn" class="form-control" id="campus"  required="required">
                          </div>
                          <div class="form-group">
                              <input type="submit" name="submittraining" value="Submit" class="btn btn-primary">
                          </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
           </div>
         </div>
      </div>
   </div>

   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>
<style>
  .myTextEditor{width:100%!important;}
   .stu {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   margin-bottom: 50px;
   }
   .stu h1 {
   font-size: 27px;
   margin: 15px 30px;
   }
   .optstu {
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   float: left;
   width: 100%;
   margin: 14px 3px;
   }   
   .optstu p {
   margin-bottom: 0px;
   font-size: 13px;
   color: #060658;
   padding: 20px;
   }
   .optstu input[type="checkbox"] {
   margin: 0px 11px;
   }
   .filteropt {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   margin-bottom: 30px;
   margin-top: 50px;
   padding-bottom: 30px;
   }
   .Query {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   }
   .Query p {
   font-size: 13px;
   margin: 36px 28px;
   float: left;
   }
   .Queryw{
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   }
   .Queryw p {
   font-size: 24px;
   margin: 36px 28px;
   float: left;
   font-weight: 600;
   color: #060658;
   }
   .sub {
   float: left;
   width: 100%;
   margin: 15px 0px;
   text-align: center;
   }
   .sub button.btn.btn-primary {
   background: #f68e2f;
   }
   .filteropt select {
    background: #fff;
    border: 1px solid #ddd;
    height: 42px;
    width: 27%;
    margin-left: 50px;
   }
   .filteropt p{
    padding: 28px 10px 10px 30px;
    color: #0a0a5b;
    font-weight: 600;
    font-size: 24px;
    margin-bottom: 0px;
   }
   p.wantpost {
    margin-bottom: 0px!important;
    padding-bottom: 0px;
    font-weight: 300;
    margin-left: 5%;
  }
  .tab-content {
    border: 1px solid #ddd;
    min-height: 500px;
        padding: 33px;
}
.nav-tabs>li {
    width: 50%;
}
.panel-body p {
    font-weight: 600!important;
}
.panel-heading center {
    color: #fff!important;
    font-weight: 600!important;
    font-size: 25px!important;
}
.panel-warning {
    border-color: #52c641!important;
}
.panel-warning>.panel-heading {
    color: #000!important;
    background-color: #52c641!important;
    border-color: #f68e2f!important;
}
   .panel-warning>.panel-heading{
      color:#fff!important;
   }
</style>
<script type="text/javascript">
  $(document).ready(function(){
    $("#selectPost").on("change", function(){
        var selectValue = $(this).val();

        if(selectValue == "campus"){
          $("#campusshow").css("display","block");
          $("#trainingshow").css("display","none");
        } else if(selectValue == "training"){
          $("#trainingshow").css("display","block");
          $("#campusshow").css("display","none");
        } else{
          $("#trainingshow").css("display","none");
          $("#campusshow").css("display","none");
        }
    });
  });
</script>

<script type="text/javascript">
   var uploadField = document.getElementById("fileCampusimg");
   uploadField.onchange = function() {
       if(this.files[0].size > 1024000) {
          alert("File size is greater than 1MB");
          this.value = "";
       
       } else if(this.files[0].size < 102400) {
            alert("File size is less than 100kb");
            this.value = "";
       }
   };

   var uploadField = document.getElementById("short1");
   uploadField.onchange = function() {
       if(this.files[0].size > 1024000) {
          alert("File size is greater than 1MB");
          this.value = "";
       
       } else if(this.files[0].size < 102400) {
            alert("File size is less than 100kb");
            this.value = "";
       }
   };
</script>
