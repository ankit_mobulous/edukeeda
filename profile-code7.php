<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;

    if(isset($_POST['submit'])) {
      $userId = $_SESSION["login"]["id"];
      $page = $_POST["lmember"];

      $checkImg = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $userId));

      if($checkImg) {
          if($_FILES["pic"]["name"]) {
              $upload_dir  = wp_upload_dir();
              $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

              $extension = pathinfo($_FILES["pic"]["name"], PATHINFO_EXTENSION);
              $newfilename = substr(sha1(mt_rand().time()), 0, 11).".".$extension;

              $file = $upload_path . $newfilename;
              
              $raw_file_name = $_FILES['pic']['tmp_name'];
              $siteurl = site_url();
              $saveFile = $siteurl.'/wp-content/uploads/'.date('Y/m').'/'. $newfilename;

              if (move_uploaded_file($_FILES['pic']['tmp_name'], $file)) { } 
                $data = array(
                    "user_id" => $userId,
                    "profilePic" => $saveFile
                  );
              $wpdb->update("wp_user_pics", array('profilePic'=>$saveFile), array('user_id'=>$userId));

              if(isset($_POST["lmember"])) {
                header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
              }else {
                header('Location: https://edukeeda.com/edit-profile/');
              }
          } else {
              if(isset($_POST["lmember"])) {
                header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
              }else {
                header('Location: https://edukeeda.com/edit-profile/');
              }
          }
      } else {

          if($_FILES["pic"]["name"]) {
              $upload_dir  = wp_upload_dir();
              $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

              $extension = pathinfo($_FILES["pic"]["name"], PATHINFO_EXTENSION);
              $newfilename = substr(sha1(mt_rand().time()), 0, 11).".".$extension;

              $file = $upload_path . $newfilename;
              
              $raw_file_name = $_FILES['pic']['tmp_name'];
              $siteurl = site_url();
              $saveFile = $siteurl.'/wp-content/uploads/'.date('Y/m').'/'. $newfilename;

              if (move_uploaded_file($_FILES['pic']['tmp_name'], $file)) { } 
                $data = array(
                    "user_id" => $userId,
                    "profilePic" => $saveFile
                );
                $result = $wpdb->insert("wp_user_pics", $data);

                if(isset($_POST["lmember"])) {
                  header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
                }else {
                  header('Location: https://edukeeda.com/edit-profile/');
                }
          } else {
              if(isset($_POST["lmember"])) {
                header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
              }else {
                header('Location: https://edukeeda.com/edit-profile/');
              }
          }
      }
  }

?>
