<div class="homepagemenedu discussionback">
<?php
/*
Template Name: Discussion1
*/
get_header(); ?>
<?php
  $path = $_SERVER['DOCUMENT_ROOT'];
  include_once $path . '/wp-config.php';
  include_once $path . '/wp-load.php';
  include_once $path . '/wp-includes/wp-db.php';
  include_once $path . '/wp-includes/pluggable.php';
  
  global $wpdb;
  global $current_user;
    if(isset($_POST["follow"])) {
  
        $quesId = $_POST["quesId"];
        $userid = $_SESSION["login"]["id"];
        $data = ["ques_id"=>$quesId, "user_id"=>$userid];
        $wpdb->insert("wp_discussion_ques_follow", $data);
?>
        <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
        </script>
<?php
    }
    if(isset($_POST["unfollow"])) {
        $quesId = $_POST["quesId"];
        $userid = $_SESSION["login"]["id"];
        $wpdb->query($wpdb->prepare("DELETE FROM wp_discussion_ques_follow WHERE ques_id = %d AND user_id=%d", $quesId, $userid));
?>
        <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
        </script>
<?php
    }
?>
<?php
if(isset($_POST["followgroup"])) {
        $maingroupId = $_POST["maingroupId"];
        $subgroupId = $_POST["subgroupId"];
        $userid = $_SESSION["login"]["id"];
        $data = ["main_group_id"=>$maingroupId, "sub_group_id"=>$subgroupId, "user_id"=>$userid];
        $wpdb->insert("wp_discussion_group_follow", $data);
?>
        <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
        </script>
<?php
    }
    if(isset($_POST["unfollowgroup"])) {
        $maingroupId = $_POST["maingroupId"];
        $subgroupId = $_POST["subgroupId"];
        $userid = $_SESSION["login"]["id"];
        $wpdb->query($wpdb->prepare("DELETE FROM wp_discussion_group_follow WHERE main_group_id = %d AND sub_group_id = %d AND user_id=%d", $maingroupId, $subgroupId, $userid));
?>
        <script type="text/javascript">
          if ( window.history.replaceState ) {
              window.history.replaceState( null, null, window.location.href );
          }
        </script>
<?php
    }

  if(isset($_POST['submitReport'])) {
    $qid = $_POST['qid'];
    $reason = $_POST['reason'];
    $userid = $_SESSION["login"]["id"];

    $data = ["user_id"=>$userid, "quesid"=>$qid, "status"=>"report", "reason"=>$reason];
    $wpdb->insert("wp_question_hide", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['anshidesubmit'])) {
        $aid = $_POST['qid'];
        $userid = $_SESSION["login"]["id"];
        $data = ["user_id"=>$userid, "ansid"=>$aid, "status"=>"hide"];
        $wpdb->insert("wp_answer_hide", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['submitansReport'])) {
    $aid = $_POST['aid'];
    $reason = $_POST['reason'];
    $userid = $_SESSION["login"]["id"];

    $data = ["user_id"=>$userid, "ansid"=>$aid, "status"=>"report", "reason"=>$reason];
    $wpdb->insert("wp_answer_hide", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['anspostsubmit'])) {
    $qid = $_POST['qid'];
    $userid = $_SESSION["login"]["id"];
    $ansPost = $_POST['ansPost'];
$ansPost = stripslashes($ansPost);
    $data = ["ques_id"=>$qid, "user_id"=>$userid, "ans_content"=>$ansPost];
    $wpdb->insert("wp_discussion_ans", $data);
    $msgAns = "Answer posted successfully";
  }

 if(isset($msgAns)) { ?>
    <script type="text/javascript">
        alert("Answer posted successfully");
    </script>
<?php  
    unset($msgAns); 
?>
    <script type="text/javascript">
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
    </script>
<?php
}
    if(isset($_POST['replypostsubmit'])) {
    $rid = $_POST['rid'];
    $userid = $_SESSION["login"]["id"];
    $replyPost = $_POST['replyPost'];
    $data = ["user_id"=>$userid, "ans_id"=>$rid, "reply"=>$replyPost];
    $wpdb->insert("wp_discussion_reply", $data);
    $msgReply = "Reply on answer posted successfully.";
  }

 if(isset($msgReply)) { ?>
    <script type="text/javascript">
        alert("Reply on answer posted successfully.");
    </script>
<?php
    unset($msgReply);
?>
    <script type="text/javascript">
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
    </script>
<?php
} 

  if(isset($_POST['likeSubmit'])) {
    $ansid = $_POST['ansid'];
    $userid = $_SESSION["login"]["id"];
    $data = ["user_id"=>$userid, "ans_id"=>$ansid];
    $wpdb->insert("wp_answer_like", $data);
?>
      <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
     </script>
<?php
  }

  if(isset($_POST['unlikeSubmit'])) {
    $ansid = $_POST['ansid'];
    $userid = $_SESSION["login"]["id"];
    $wpdb->query($wpdb->prepare("DELETE FROM wp_answer_like WHERE ans_id = %d AND user_id=%d", $ansid, $userid));
?>
      <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
     </script>
<?php
  }
  if(isset($_POST['repostsubmit'])) {

      $type = $_POST['type'];
      $quesid = $_POST['quesid'];
      $ques = $_POST['ques'];
      $reposttext = $_POST['reposttext'];
      $userid = $_SESSION["login"]["id"];
      $data = ["user_id"=>$userid, "content"=>$ques, "shareType"=>$type, "shareid"=>$quesid, "sharetext"=>$reposttext]; 
      $wpdb->insert("wp_wallpost", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['repostanssubmit'])) {
      $type = $_POST['type'];
      $ans = $_POST['ans'];
      $ansid = $_POST['ansid'];
      $reposttext = $_POST['reposttext'];
      $userid = $_SESSION["login"]["id"];

      $data = ["user_id"=>$userid, "content"=>$ans, "shareType"=>$type, "shareid"=>$ansid, "sharetext"=>$reposttext];
      $wpdb->insert("wp_wallpost", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }
?>
   <style>
    .repostmodal {
        border: 1px solid #C0C0C0;
        padding: 10px;
    }
      .memberpage {
      float: left;
      width: 100%;
      margin-top: 37px;
      }
      .imgforr {
      text-align: center;
      }
      .imgforr img.img-thumbnail {
      width: 60px;
      height: 60!important;;
      border-radius: 100%;
      }
      .post span {
      margin-right: 6px;
      }
     


      .plugin {
      float: right;
    color:#9a9a9a;
      }
      .post .dotsseprate {
      margin: 0 6px;
      }
      #articleblous .text1 a{
      color:#000;
      font-size: 18px;
      }
      #articleblous .post
      {
      color:#9a9a9a;
      }
      #articleblous .post strong
      {
      color:#9a9a9a;
      }
      #articleblous .share
      {
      color:#9a9a9a;
      }
      #articleblous .share1{
      color:#9a9a9a;
      }
      #articleblous .share2{
      color:#9a9a9a;
      }
      #articleblous .share .fa{
      color:#9a9a9a;
      }
      #articleblous .share1 .fa{
      color:#9a9a9a;
      }
      #articleblous .share2 .fa{
      color:#9a9a9a;
      }
      #articleblous .share1 button{
      color:#9a9a9a;
      }
      #articleblous .plugin{
      color:#9a9a9a;
      }


#wallposts .text2 span {
    margin: 0px 5px;
    color:#f68e2f;
}
.text2 p {
    display: initial;
}
.questio p.rigwd
{
  color:#9a9a9a;
}.direct a {
    color: #9a9a9a!important;
}
.plug button {
    background: none;
    border: none;
    color: #9a9a9a;
    margin: 0px;
    padding: 0px;
    float: left;
}
.like {

    color: #9a9a9a;
}
.pluging{
  color: #9a9a9a;
}
.arteduro {

    margin: 0px!important;
}

  
  .pp {
      float: left;
      width: 100%;
      display: -webkit-box;
      margin: 10px 0px;
      }
      #anwerspat .text a{
      color:#000;
      font-size: 18px;
      }
      #anwerspat .post
      {
      color:#9a9a9a;
      }
      #anwerspat .post strong
      {
      color:#9a9a9a;
      }
      #anwerspat .share
      {
      color:#9a9a9a;
      }
      #anwerspat .share1{
      color:#9a9a9a;
      }
      #anwerspat .share2{
      color:#9a9a9a;
      }
      #anwerspat .share .fa{
      color:#9a9a9a;
      }
      #anwerspat .share1 .fa{
      color:#9a9a9a;
      }
      #anwerspat .share2 .fa{
      color:#9a9a9a;
      }
      #anwerspat .share1 button{
      color:#9a9a9a;
      }
      #anwerspat .plugin{
      color:#9a9a9a;
      }
      #anwerspat .pp strong{
      color:#9a9a9a;
      }
      #anwerspat .pp{
      color:#9a9a9a;
      }
      .pp span {
      margin-right: 10px;
    color:#9a9a9a;
      }
      #questforyou .pp {
        margin-bottom: 45px;
        color: #9a9a9a;
      }
      #questforyou .pp strong {
          color: #9a9a9a;
        }
        #questforyou .plugin {
              float: right;
              color: #9a9a9a;
       }
       #questforyou .share, .share2 p {
        color: #9a9a9a;
        }
        .pp a {
          background: #f68e2f;
          padding: 10px;
          margin: 15px;
          color: #fff;
          border-radius: 4px;
        }
      .pp strong {
      margin-right: 10px;
      color: #9a9a9a;
      }
      .catp strong {
      color: #060658;
      font-weight: 600;
      }
      .catp span {
      margin: 5px;
      color: #4c4242;
      }
      .text {
      float: left;
      width: 100%;
      margin-bottom: 15px;

      }
      .text p {
      font-weight: 800;
      color: #4c4242;
      line-height: 30px;
      font-size: 29px!important;
      }
      .share .fa {
      color: #9a9a9a;
      }
      .share1 .fa {
      color: #9a9a9a;
      }
      .share2 .fa {
      color: #9a9a9a;
      }
      .dot {
      float: right;
      }
      .questio {
      float: left;
      width: 100%;
      margin-bottom: 20px;
      }
      .dot span.countno {
      margin: 0px;
      float: left;
      }
      .dot ul.dropdown-menu {
      min-width: 217px;
      padding: 3px;
      }
      .modal-body p {
      margin-bottom: 0px;
      padding: 3px;
      }
      .post{
      float: left;
      width: 100%;
      display: -webkit-box;
      margin-bottom: 20px;
          color: #9a9a9a;
    }
    #articleblous2 .share
    {
      color:#9a9a9a;
    }
    #articleblous2 .share1
    {
      color:#9a9a9a;
    }
    #articleblous2 .share2
    {
      color:#9a9a9a;
    }
    
      .text1 {
      float: left;
      width: 100%;
      margin-bottom: 15px;
      }
      .share1 button {
      color: #9a9a9a;
      font-size: 14px;
      text-transform: capitalize;
      background: transparent;
      border: none;
      padding: 0 6px;
      }
      .share1 img {
      width: 15px;
      height: auto;
      }  
      .text1 p {
      font-weight: 800;
      color: #f68e2f!important;
      line-height: 23px;
      font-size: 21px!important;
      }
      .text1 h2 {
      font-size: 20px;
      color: #000;
      }
      .house button {
      font-size: 12px;
      margin: 14px;
      padding: 5px;
      color: #000;
    font-weight: 600;
      }
      #wallposts .share2 p {
    color: #060658;
}
      .house {
      float: left;
      width: 100%;
      margin: 35px;
      margin-top: 50px;
      }
      .plug img {
      width: 20px;
      }
      .plug {
      text-align:right;}
      .cat a {
      color: #000000bd;
      font-weight: 600;
      }.catp a {
      color: #060658;
      font-weight: 600;
      }
      .cat span {
      color: #f68e2f;
      margin: 7px;
      }
    .plug {
    text-align: right;
    margin-top: 8px;
}
      .cat {
      margin: 6px 0px;
      }
      .headdiscus h1 {
      font-size: 25px;
      text-align:left;
      }
      .arteduro{
      margin: 0px;
      }
      .direct p {
      color: #9a9a9a;
      }
      .accordion {
      background-color: #eee;
      color: #444;
      cursor: pointer;
      padding: 18px;
      width: 100%;
      border: none;
      text-align: left;
      outline: none;
      font-size: 15px;
      transition: 0.4s;
      }
      .active, .accordion:hover {
      background-color: #ccc; 
      }
      .headarea .active, .accordion:hover {
      background-color: transparent;
      }
      .active:after {
      content: "\2212";
      }
      .accordion:after { 
      content: ' \002B';
      font-size: 21px;
      margin: 1px;
      padding: 0px;
      float: right;
      }
      .panel {
      padding: 0 18px;
      display: none;
      background-color: white;
      overflow: hidden;
      }
      .headarea button.accordion {
      background: none;
      border: none;
      color: #616161;
      margin: 0px;
      padding: 0px;
      font-weight: 500;
      text-transform: capitalize;
      }
      .thumbprofile img {
      width: 50px;
      border: 1px solid #ddd;
      padding: 2px;
      height: 50px;
      border-radius: 100%;
      }
      .thumbprofile {
      float: left;
      width: 60px;
      }
      .areadetails {
      float: left;
      width: 75%;
      }
      .areadetails p {
      line-height: 17px;
      }
      .areadetails h5 {
      margin: 0;
      }
      p.joineon {
      text-transform: capitalize;
      font-size: 13px;
      margin: 4px 0 0 0;
      float: left;
      }
      .profileares {
      float: left;
      width: 100%;
      border: 1px solid #ddd;
      padding: 5px;
      margin-bottom: 20px;
      }
      .reportingsets ul li {
      float: left;
      width: 100%;
      text-align: left;
      }
      .reportingsets ul {
      list-style-type: circle;
      }
      .reportingsets ul li span {
      float: left;
      }
      .reportingsets {
      width: 100%;
      border-bottom: 1px solid #ddd;
      padding: 20px 0;
      margin: 0 0 15px 0;
      }
      .reportingsets .modal-footer{
      border-top:none;
      }
      .bottom-btns {
      padding: 0 21px;
      text-align: right;
      }
      .repolinks label {
      color: #797977;
      cursor: pointer;
      }
      .repolinks label:hover {
      color: #de7514;
      }
      .reportingsets input[type="radio"] {
      margin: 0 8px 0 0;
      }
      .groupname p{margin:0;}
      .postpanels ul li:first-child {
      border-bottom: 1px solid #ddd;
      }
      .filtertab {
      float: left;
      width: 50%;
      text-align: center;
      padding: 14px 0;
      background:#fff;
  
      }
      .filtertab span {
      font-size: 12px;
      }

      .filtertabans {
      float: left;
      width: 50%;
      text-align: center;
      padding: 14px 0;
      background:#fff;
      color:#9a9a9a;
 
      }
      .filtertabans span {
      font-size: 12px;
      }

      .filtertabhouse {
         float: left;
    width: 50%;
    text-align: center;
    padding: 14px 0; 
    background: #fff;
    color: #9a9a9a;

      
      }
      .filtertabhouse span {
      font-size: 12px;
      }

      .spacetp {
      margin: 5px 0;
      }
      .addposituon.bottomspc p a{color:#616161!important;}
      .spacetp .accordion a{color:#616161!important;}
      .share {
      float: left;
      width: 100%;
      }
      p.catp {
      width: 100%;
      float: left;
      }
      p.rigwd {
      width: 100%;
      float: left;
      }
      .headdiscus {
      background: #fff;
      padding: 10;
      margin-bottom: -11px;
      border: 1px solid #9a9a9a;
      }
     
     
      @media only screen and (max-width: 767px){
        
       .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
} 
      .memberpage {
      margin-top:15px!important;
      }
      .post span {
      margin-right: 2px;
      }
      .memberpage h4 {
      font-size: 25px!important;
      }
      .memberpage .advtspace {
      height: 256px;
      float: left;
      width: 100%;
      }
      .headdiscus h1 {
    font-size: 19px;
    text-align: left;
    float: left;
    margin: 0px!important;
    width: 48%;
}
      .headdiscus {
      float: left;
      width: 100%;
      }
      .share1,.share2{
      width:100%;
      float:left;
      text-align: center;
      }
      .share1 img {
      width: 15px;
      height: auto;
      }
      .dotsseprate {
    margin: 0px 5px;
    color: #9a9a9a;
}
.like p {
    display: -webkit-box;
}
.like {
    width: 100%;
    padding-top: 14px;
}
      .text1 p a {
      font-weight: 800;
      color: #f68e2f;
      line-height: 21px;
      font-size: 16px!important;
      }
      .post .dotsseprate {
      margin: 0 2px;
      }
      .post p{
      font-size: 12px!important;
      }
      .plugin {
      float: right;
      margin-top: -43px;
      }
      .memberpage .advtspace {
      float: left;
      width: 100%;
      }
      .dot ul.dropdown-menu {
      min-width: 160px;
      padding: 3px;
      width: 100%;
      margin-left: -126px;
      }
      .blogaresnews .dot ul.dropdown-menu {
      margin-left: 0px;
      }
      .text p {
      font-weight: 800;
      color: #4c4242;
      line-height: 25px;
      font-size: 20px!important;
      }
      .text2 {
      float: left;
      width: 100%;
      }
    .page-wrap {
    padding: 64px 15px;
}
     .share .fa {
    color: #9a9a9a;
    display: -webkit-box;
}
.share1 button {
    color: #9a9a9a;
    font-size: 12px;
    text-transform: capitalize;
    background: transparent;
    border: none;
    padding: 0 6px;
}
     .memberpage .col-xs-12 {
    padding: 0px 15px;
}
      }
      .share.like button {
          padding: 0px;
          background: #fff;
          color: #9a9a9a;
          border: 0px;
      }
      .like {
          width: 100%;
      }
      .headdiscus h1{margin-bottom: 50px;}
      .ansmorecolor{color:#f68e2f;}
      #articleblous1 .text1 a {color: #000;}
      #articleblous1 .share {color: #9a9a9a;}
      #anwerspat1 .text a {color: #000;font-size: 18px;}
      #anwerspat2 .text a {color: #000;font-size: 18px;}
      div#dischouses1 .like span {margin: 0px 5px;}
.like span {
    margin: 0px 6px;
}
      .join a{background: none;}

 .headdiscus.dynamiccategory p{ display: inline-block;
    color: orange;    font-size: 16px;
}
.headdiscus.dynamiccategory p {
    display: inline-block;
    color: #f68e2f;
    font-size: 14px;
    float: right;
    width: 52%;
}
.cutjion a {
    padding: 11px 5px;
    margin: 0px 0px;
    font-size: 13px;
}

}
   </style>
   <div class="memberpage">
      <div class="row">
         <div class="col-md-9">
            <div class="col-xs-12 col-md-4">
               <div class="headarea">
                
                  <div class="other">
                     <h4>Discussion Group</h4>
                  </div>
                  <div class="headarea">
                     <div class="addposituon bottomspc">
                        <p class="tablinks1"><a href="http://edukeeda.com/opinion-poll/">Opinion Poll</a> <span class="countno"></span></p>
                        <?php
                           $getSidebarCats = $wpdb->get_results("SELECT * FROM wp_discussion_main");
                           
                           foreach($getSidebarCats as $getSidebarCat) {
                              if($getSidebarCat->have_sub == 0) {
                            ?>
                        <p class=""><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>"><?php echo $getSidebarCat->name;?> <span class="countno"></span></a></p>
                        <?php
                           } else {
                           ?>
                        <div class="spacetp">
                           <button class="accordion"><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>"><?php echo $getSidebarCat->name;?></a></button>
                           <div class="panel">
                              <?php
                                 $getSubCats = $wpdb->get_results( 
                                     $wpdb->prepare( "SELECT * FROM wp_discussion_sub WHERE main_id = %d",$getSidebarCat->id) 
                                 );
                                 foreach($getSubCats as $getSubCat) {
                                 ?>
                              <p class="tablinks"><a href="<?php echo site_url();?>/discussioncategory?action=<?php echo $getSidebarCat->id; ?>&actions=<?php echo $getSubCat->id; ?>"><?php echo $getSubCat->sub_name;?><span class="countno"></span></a></p>
                              <?php
                                 }
                              ?>
                           </div>
                        </div>
                        <?php
                            }
                          }
                        ?>

                        <?php
                            if(isset($_SESSION["login"]["id"]) || $current_user->ID == 1) {
                        ?>
                              <p class="tablinks"><a href="<?php echo site_url();?>/peoples-wallposts"> People's Wallposts </a><span class="countno"></span></p>
                              <p class="tablinks"><a href="<?php echo site_url();?>/my-discussion-house">My Discussion Houses</a><span class="countno"></span></p>
                              <p class="tablinks"><a href="<?php echo site_url();?>/my-discussion-group">My Groups</a><span class="countno"></span></p>
                        <?php
                            }
                        ?>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-8 col-xs-12">
               <div class="headdiscus dynamiccategory">
               <?php
                  $houseCount = $wpdb->get_results("SELECT COUNT(id) as houseCount FROM wp_discussion_house");
                  $postCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as postCount FROM wp_discussion_ques WHERE status = '1'"));
                  $postCount = $postCount[0]->postCount;
               ?>
                  <h1>Discussion Forum </h1> <p> (<span class="tablinks" onclick="openCity(event, 'dischouses')"><?php echo $houseCount[0]->houseCount; ?> Discussion Houses</span>, <span class="tablinks" onclick="openCity(event, 'articleblous')"><?php echo $postCount; ?> Posts</span>) </p>
               </div>
               <div class="arteduro">
                  <div class="row">
                     <div class="col-md-12 col-sm-12 basictabsed">
                        <div class="customtabfilter">
                            <div class="filtertab">
                               <span class="tablinks active" onclick="openCity(event, 'articleblous')">Questions</span> <!--/ <span class="tablinks" onclick="openCity(event, 'anwerspat2')">Answers</span>--> / <span class="tablinks" onclick="openCity(event, 'dischouses2')">Discussion Houses</span>
                            </div>
                            <div class="filtertab" id="filtertabques">
                               <span class="tablinks1 tabactive1" onclick="openCity1(event, 'articleblous1')">All Time Top</span> / <span  class="tablinks1 active tabactive2" onclick="openCity1(event, 'articleblous2')">One Month Top</span> / <span class="tablinks1 tabactive3" onclick="openCity1(event, 'articleblous')">Recent</span>
                            </div>
                            <div class="filtertabans" id="filtertabans" style="display:none">
                               <span class="tablinks1 tabactive1" onclick="openCity1(event, 'anwerspat1')">All Time Top</span> / <span class="tablinks1 active tabactive2" onclick="openCity1(event, 'anwerspat2')">One Month Top</span> / <span class="tablinks1 tabactive3" onclick="openCity1(event, 'anwerspat')">Recent</span>
                            </div>
                            <div class="filtertabhouse" id="filtertabhouse" style="display:none">
                               <span class="tablinks1 tabactive1" onclick="openCity1(event, 'dischouses1')">All Time Top</span> / <span class="tablinks1 active tabactive2" onclick="openCity1(event, 'dischouses2')">One Month Top</span> / <span class="tablinks1 tabactive3" onclick="openCity1(event, 'dischouses')">Recent</span>
                            </div>
                        </div>
                        <?php
                          if(isset($_SESSION["login"])) {
                        ?>
                           <div class="memb1"> <p data-toggle="modal" data-target="#myModalmemb1">Want to post any Question ?</p>
                           </div>
                        <?php
                          } else{
                        ?>
                              <div class="memb1"> <p data-toggle="modal" data-target="#myModalpostany">Want to post any Question ?</p></div>
                        <?php
                          }
                        ?>
                        
                        <div id="articleblous" class="tabcontent" style="display:none;">
                           <div class="blogaresnews">
                              <?php
                                 $quesPosts = $wpdb->get_results("SELECT * FROM wp_discussion_ques WHERE status='1' ORDER BY id DESC LIMIT 30");
                                 $z=1;
                                 foreach($quesPosts as $quesPost) {
                                   
                                    if($quesPost->sub_cat_id == 0) {
                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost->main_id));
                                        $cName = $catName[0]->name;
                                    } else{
                                        $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost->main_id));
                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $quesPost->sub_cat_id));
                                        $cName = $catName[0]->sub_name;
                                    }

                                    $houseName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d AND subcat_id=%d", $quesPost->main_id, $quesPost->sub_cat_id));

                                   $viewsCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT * FROM wp_discussion_views WHERE ques_id = %d", $quesPost->id) 
                                   );
                                   $ansCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT COUNT(id) as countAns FROM wp_discussion_ans WHERE ques_id = %d", $quesPost->id) 
                                   );
                                   $ansTime = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT created_at FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $quesPost->id));
                                    $repostCount = $wpdb->get_results(
                                      $wpdb->prepare("SELECT COUNT(id) as countRepost FROM wp_wallpost WHERE shareType='ques' AND shareid = %d", $quesPost->id));
                                   $quesFollow = $wpdb->get_results( 
                                   $wpdb->prepare("SELECT COUNT(id) as countFollowed FROM wp_discussion_ques_follow WHERE ques_id = %d", $quesPost->id));
                                 
                                   if($ansTime) {
                                     $timeAnsGet = "<strong>Answered </strong> " . time_elapsed_string($ansTime[0]->created_at);
                                   } else {
                                     $timeAnsGet = "<strong>Posted </strong> " . time_elapsed_string($quesPost->created_at);
                                   }

                              ?>
                                    <div class="col-md-12 blo postpanels">
                                       <div class="direct">
                                          <div class="col-md-10 col-sm-10 col-xs-10">

                                             <p> <?php if($quesPost->sub_cat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$quesPost->main_id."'>".$catMainName[0]->name .'</a> . ';}?>
                                                <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $quesPost->main_id;?> <?php if($quesPost->sub_cat_id != 0) { echo "&actions=". $quesPost->sub_cat_id;}?>"><?php echo $cName; ?></a> . 
                                                <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $quesPost->house_id;?>"><?php if(strlen($houseName[0]->title) > 40){echo substr($houseName[0]->title, 0,40). ' ...';} else{echo $houseName[0]->title;} ?></a> </p>
                                          </div>
                                       </div>
                                       <div class="col-md-2 col-sm-2 col-xs-2">
                                          <div class="dot">
                                             <div class="dropdown">
                                                <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...
                                                </p>
                                                <ul class="dropdown-menu">
                                                <?php
                                                     if($_SESSION['login']['id']) {
                                                ?>
                                                   <li data-toggle="modal" data-target="#myModalreport1<?php echo $z; ?>">Report This Post</li>
                                                <?php
                                                    } else {
                                                ?>
                                                    <li data-toggle="modal" data-target="#myModalreportnota<?php echo $z; ?>">Report This Post</li>
                                                <?php
                                                    }
                                                ?>
                                                </ul>
                                             </div>
                                                <div id="myModalreportnota<?php echo $z; ?>" class="modal fade" role="dialog">
                                                          <div class="modal-dialog">
                                                             <!-- Modal content-->
                                                             <div class="modal-content">
                                                                <div class="modal-header">
                                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                   <h1>Report this post</h1>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <p> Please Login and Register first! </p>
                                                                </div>
                                                             </div>
                                                          </div>
                                                      </div>
                                                <div class="modal fade" id="myModalreport1<?php echo $z; ?>" role="dialog">
                                                   <div class="modal-dialog vertical-align-center">
                                                      <div class="modal-content">
                                                         <div class="repocontent modal-body reportingsets" id="gridone<?php echo $z; ?>" style="display:block;">
                                                            <ul>
                                                               <li>
                                                                  <span  class="countno repolinks" onclick="openReport(event, 'gridtwo<?php echo $z; ?>')">
                                                                     <div class="funkyradio-default">
                                                                        <label for="checkbox1">I think it's spam, Promotional</label>
                                                                     </div>
                                                                  </span>
                                                               </li>
                                                               <li>
                                                                  <span class="countno repolinks" onclick="openReport(event, 'gridthree<?php echo $z; ?>')">
                                                                     <div class="funkyradio-default">
                                                                        <label for="checkbox1">I think  it's objectionable</label>
                                                                     </div>
                                                                  </span>
                                                               </li>
                                                               <li>
                                                                  <span class="countno repolinks" onclick="openReport(event, 'gridfour<?php echo $z; ?>')">
                                                                     <div class="funkyradio-default">
                                                                        <label for="checkbox1">I think it false or misinformation</label>
                                                                     </div>
                                                                  </span>
                                                               </li>
                                                            </ul>
                                                            <div class="modal-footer">
                                                            </div>
                                                         </div>

                                                         <div class="repocontent modal-body reportingsets" id="gridtwo<?php echo $z; ?>" style="display:none;">
                                                            <form method="post" action="">
                                                            <div class="modal-body">
                                                               <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                               <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                            </div>
                                                            <div class="modal-footer">
                                                               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z; ?>')">Back</button>
                                                               <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                            </div>
                                                            </form>
                                                         </div>
                                                         
                                                         <div class="repocontent modal-body reportingsets" id="gridthree<?php echo $z; ?>" style="display:none;">
                                                            <form method="post" action="">
                                                            <div class="modal-body">
                                                               <div class="funkyradio-default">
                                                                  <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent" checked="checked">It's pornographic or extremely violent.</label>
                                                               </div>
                                                               <div class="funkyradio-default">
                                                                  <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                               </div>
                                                               <div class="funkyradio-default">
                                                                  <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                               </div>
                                                            </div>
                                                            <div class="bottom-btns">
                                                              <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z; ?>')">Back</button>
                                                               <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                            </div>
                                                            </form>
                                                         </div>
                                                         <div class="repocontent modal-body reportingsets" id="gridfour<?php echo $z; ?>" style="display:none;">
                                                            <form method="post" action="">
                                                            <div class="modal-body">
                                                               <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                            </div>
                                                            <div class="bottom-btns">
                                                               <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z; ?>')">Back</button>
                                                               <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                            </div>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>

                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="text1">
                                             <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $quesPost->id;?>"><?php echo $quesPost->ques; ?></a></p>
                                          </div>
                                       </div>
                                       <div class="questio">
                                          <div class="col-md-12 col-sm-12">
                                             <p class="rigwd"><?php echo $timeAnsGet; ?></p>
                                          </div>
                                       </div>
                                       <div class="col-xs-9 col-md-9">
                                          <div class="post">
                                             <p><span><?php echo $ansCount[0]->countAns; ?></span><strong>Answers</strong>
                                             <div class="dotsseprate">.</div>
                                             </p>
                                             <!--<p data-toggle="modal" data-target="#myModalrepostlista<?php echo $z;?>"><span><?php if($repostCount){echo $repostCount[0]->countRepost;}else{echo "0";}?></span><strong> Repost</strong>
                                             <div class="dotsseprate">.</div>
                                             </p>-->

                                             <p><span><?php if($viewsCount){echo $viewsCount[0]->view_count;}else{ echo "0";} ?></span><strong>Views</strong>
                                             <div class="dotsseprate">.</div>
                                             </p>
                                             <p data-toggle="modal" data-target="#myModalfollowlista<?php echo $z;?>"><span><?php if($quesFollow){ echo $quesFollow[0]->countFollowed; }else{ echo "0";} ?></span><strong>Followers</strong></p>
                                          </div>

                                                <!---- Modal  --->
                                                <div id="myModalrepostlista<?php echo $z;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog" style="width: 700px;">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                         <h3></h3>
                                                      </div>
                                                      <div class="modal-body">
                                                         <div class="row">
                                                            <div clss="col-md-12">
                                                            <?php
                                                                $reLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ques' AND shareid=%d", $quesPost->id));
                                                                if($reLists) {
                                                                    foreach($reLists as $reList) {
                                                                        $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $reList->user_id));
                                                                
                                                                        $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $reList->user_id));
                                                                
                                                                        $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $reList->user_id));

                                                            ?>   
                                                               <div class="col-md-6">
                                                                <div class="poll">
                                                                   <div class="col-md-3 col-sm-2 col-xs-3">
                                                                      <div class="imgforr">
                                                                         <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                      </div>
                                                                   </div>
                                                                   <div class="col-md-9 col-sm-10 col-xs-9">
                                                                      <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                      <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                   </div>
                                                                </div>
                                                             </div>
                                                            <?php
                                                                    }
                                                                }
                                                            ?>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                </div>

                                              <!---- Modal  --->
                                                <div id="myModalfollowlista<?php echo $z;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog" style="width: 700px;">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                         <h3></h3>
                                                      </div>
                                                      <div class="modal-body">
                                                         <div class="row">
                                                            <div clss="col-md-12">
                                                            <?php
                                                                $foLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ques_follow WHERE ques_id=%d", $quesPost->id));
                                                                if($foLists) {
                                                                    foreach($foLists as $foList) {
                                                                      $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $foList->user_id));
                                                                
                                                                      $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $foList->user_id));
                                                                
                                                                      $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $foList->user_id));

                                                            ?>   
                                                               <div class="col-md-6">
                                                                <div class="poll">
                                                                   <div class="col-md-3 col-sm-2 col-xs-3">
                                                                      <div class="imgforr">
                                                                         <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                      </div>
                                                                   </div>
                                                                   <div class="col-md-9 col-sm-10 col-xs-9">
                                                                      <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                      <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                   </div>
                                                                </div>
                                                             </div>
                                                            <?php
                                                                    }
                                                                }
                                                            ?>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                </div>

                                       </div>
                                       <div class="col-xs-3 col-md-3">
                                          <div class="plugin">
                                             <div class="dropdown customs">      
                                              <button class="dropbtn">
                                                <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                                              </button>
                                              <div class="dropdown-content">
                                                <?php //$urlshare = "http://edukeeda.com/questionanswers?action=".$quesPost->id;
                                                  //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                                ?>
                                                  <?php //echo do_shortcode("$urlsharing"); ?>
                                              </div>
                                            </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-4 col-md-4">
                                          <div class="share">
                                            <?php
                                              if(isset($_SESSION["login"])) {
                                            ?>
                                              <p data-toggle="modal" data-target="#myModalnbans<?php echo $z.$z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>  
                                            <?php
                                              } else {
                                            ?>
                                                <p data-toggle="modal" data-target="#myModalnbans<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                            <?php
                                              }
                                            ?>
                                          </div>
                                              <!-- Modal -->
                                              <div id="myModalnbans<?php echo $z;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title">Post Answer</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                      <p> For post an answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>

                                              <!-- Modal -->
                                              <div id="myModalnbans<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title">Post Answer</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                      <form method="post" action="">
                                                        <input type="hidden" name="qid" value="<?php echo $quesPost->id;?>">
                                                        <?php
                                                           $my_content1 =" ";
                                                           $editor_id1 = 'ansPosta'.$z;
                                                           $option_name1 ='ansPost';
                                                           wp_editor($my_content1, $editor_id1, array('wpautop'=> false,'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                                                        ?>
                                                        <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                      </form>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                       </div>
                                       <div class="col-xs-4 col-md-4">
                                          <div class="share1">
                                             <?php
                                                if(isset($_SESSION["login"])) {
                                                  $userId = $_SESSION["login"]["id"];
                                                  
                                                  $checkFollow = $wpdb->get_results( 
                                                  $wpdb->prepare( "SELECT * FROM wp_discussion_ques_follow WHERE user_id=%d AND ques_id=%d", $userId, $quesPost->id));
                                                  if($checkFollow){
                                                ?>
                                                      <form method="post" action="">
                                                          <input type="hidden" name="quesId" value="<?php echo $quesPost->id;?>">
                                                          <img src="/img/follow-icon-selected.png"><button type="submit" name="unfollow"> follow </button>
                                                      </form>
                                             <?php
                                                } else{
                                                ?>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="quesId" value="<?php echo $quesPost->id;?>">
                                                      <img src="/img/Follow.png"><button type="submit" name="follow"> Follow </button>
                                                   </form>
                                             <?php 
                                                }
                                                } else {
                                                ?>
                                                    <img src="/img/Follow.png"><button types="button" data-toggle="modal" data-target="#myModalques">Follow</button>
                                             <?php
                                                }
                                                ?>
                                          </div>
                                       </div>
                                       <div class="col-xs-4 col-md-4">
                                          <div class="share2">
                                            <?php
                                                if(isset($_SESSION["login"])) {
                                                  $userId = $_SESSION["login"]["id"];
                                            ?>
                                                  <!--<p data-toggle="modal" data-target="#myModalnbrepost<?php //echo $z.$z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                            <?php
                                                } else{
                                            ?>
                                                  <!--<p data-toggle="modal" data-target="#myModalnbrepost<?php //echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                            <?php
                                                }
                                            ?>
                                          </div>

                                              <!-- Modal -->
                                              <div id="myModalnbrepost<?php echo $z;?>" class="modal fade" role="dialog">
                                                 <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                       <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                          <h4 class="modal-title"> Repost </h4>
                                                       </div>
                                                       <div class="modal-body">
                                                          <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                              <!-- Modal -->
                                              <div id="myModalnbrepost<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                                 <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                       <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                          <h4 class="modal-title"> Repost </h4>
                                                       </div>
                                                       <div class="modal-body">
                                                        <form method="post" action="">
                                                          <p> <textarea name="reposttext"> </textarea></p>
                                                          <div class="repostmodal">
                                                            <?php
                                                            $userPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$quesPost->user_id));
                                                            
                                                            $userReg = $wpdb->get_results($wpdb->prepare("SELECT user_registered FROM wp_users WHERE ID=%d",$quesPost->user_id));

                                                            $userName = $wpdb->get_results($wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$quesPost->user_id));

                                                            $userHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$quesPost->user_id));
                                                         ?>
                                                            <div class="col-md-2 col-sm-2 col-xs-4">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                            <div class="col-md-10 col-sm-10 col-xs-8">
                                                              <p class="catp"><strong><a href="<?php echo site_url();?>/ newmembers?action=<?php echo $quesPost->user_id;?>"><?php echo $userName[0]->name;?></a></strong>,<span><?php if($userHeadline[0]->headline){echo substr($userHeadline[0]->headline, 0,20 );} ?></span></p>

                                                              <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($userReg[0]->user_registered )); ?> </p>
                                                           </div>

                                                            <p> Repost this question </p>
                                                            <p> Question :  <?php echo $quesPost->ques; ?> </p>
                                                          </div>
                                                          <input type="hidden" name="type" value="ques">
                                                          <input type="hidden" name="quesid" value="<?php echo $quesPost->id;?>">
                                                          <input type="hidden" name="ques" value="<?php echo $quesPost->ques;?>">
                                                          <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                        </form>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                       </div>
                                    </div>
                                        
                              <?php
                                    $z++;
                                 }
                              ?>
                           </div>
                        </div>

                        <div id="articleblous1" class="tabcontent" style="display:none;">
                           <div class="blogaresnews">
                              <?php

                                 $quesPosts1 = $wpdb->get_results("SELECT wp_discussion_ques.id, wp_discussion_ques.main_id, wp_discussion_ques.sub_cat_id, wp_discussion_ques.house_id, wp_discussion_ques.user_id, wp_discussion_ques.ques, wp_discussion_ques.status, wp_discussion_ques.created_at FROM wp_discussion_ques INNER JOIN wp_discussion_views ON wp_discussion_views.ques_id = wp_discussion_ques.id AND wp_discussion_ques.status='1' ORDER BY wp_discussion_views.view_count DESC LIMIT 70");
                                 $z=1;
                                 foreach($quesPosts1 as $quesPost1) {
                                   
                                    if($quesPost1->sub_cat_id == 0) {
                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost1->main_id));
                                        $cName1 = $catName[0]->name;
                                    } else{
                                        $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost1->main_id));

                                        $catName1 = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $quesPost1->sub_cat_id));
                                        $cName1 = $catName1[0]->sub_name;
                                    }

                                    $houseName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d AND subcat_id=%d", $quesPost1->main_id, $quesPost1->sub_cat_id));

                                    $viewsCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT * FROM wp_discussion_views WHERE ques_id = %d", $quesPost1->id));

                                    $ansCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT COUNT(id) as countAns FROM wp_discussion_ans WHERE ques_id = %d", $quesPost1->id));

                                    $ansTime = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT created_at FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $quesPost1->id));

                                    $repostCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as countRepost FROM wp_wallpost WHERE shareType='ques' AND shareid = %d", $quesPost1->id));
                                   
                                    $quesFollow = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as countFollowed FROM wp_discussion_ques_follow WHERE ques_id = %d", $quesPost1->id));
                                 
                                    if($ansTime) {
                                     $timeAnsGet = "<strong>Answered </strong> " . time_elapsed_string($ansTime[0]->created_at);
                                   } else {
                                     $timeAnsGet = "<strong>Posted </strong> " . time_elapsed_string($quesPost->created_at);
                                   }
                              ?>
                                          <div class="col-md-12 blo postpanels">
                                             <div class="direct">
                                                <div class="col-md-10 col-sm-10 col-xs-10">

                                                   <p> <?php if($quesPost1->sub_cat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$quesPost1->main_id."'>".$catMainName[0]->name .'</a> . ';}?>
                                                      
                                                      <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $quesPost1->main_id;?> <?php if($quesPost1->sub_cat_id != 0) { echo "&actions=". $quesPost1->sub_cat_id;}?>"><?php echo $cName1; ?></a> . 
                                                      
                                                      <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $quesPost1->house_id;?>"><?php if(strlen($houseName[0]->title) > 40){echo substr($houseName[0]->title, 0,40). ' ...';} else{echo $houseName[0]->title;} ?></a> 
                                                   </p>
                                                </div>
                                             </div>
                                             <div class="col-md-2 col-sm-2 col-xs-2">
                                                <div class="dot">
                                                   <div class="dropdown">
                                                      <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...
                                                      </p>
                                                      <ul class="dropdown-menu">
                                                      <?php
                                                         if($_SESSION['login']['id']) {
                                                      ?>
                                                         <li data-toggle="modal" data-target="#myModalreportb<?php echo $z; ?>">Report This Post</li>
                                                      <?php
                                                         } else{
                                                      ?>
                                                            <li data-toggle="modal" data-target="#myModalreportnotb<?php echo $z; ?>">Report This Post</li>
                                                      <?php
                                                         }
                                                      ?>
                                                      </ul>
                                                   </div>
                                                      
                                                      <div id="myModalreportnotb<?php echo $z; ?>" class="modal fade" role="dialog">
                                                          <div class="modal-dialog">
                                                             <!-- Modal content-->
                                                             <div class="modal-content">
                                                                <div class="modal-header">
                                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                   <h1>Report this post</h1>
                                                                </div>
                                                                <div class="modal-body">
                                                                   <p> Please Login and Register first! </p>
                                                                </div>
                                                             </div>
                                                          </div>
                                                      </div>
                                                      
                                                      <div class="modal fade" id="myModalreportb<?php echo $z; ?>" role="dialog">
                                                         <div class="modal-dialog vertical-align-center">
                                                            <div class="modal-content">
                                                               <div class="repocontent modal-body reportingsets" id="gridone<?php echo $z.$z; ?>" style="display:block;">
                                                                  <ul>
                                                                     <li>
                                                                        <span  class="countno repolinks" onclick="openReport(event, 'gridtwo<?php echo $z.$z; ?>')">
                                                                           <div class="funkyradio-default">
                                                                              <label for="checkbox1">I think it's spam, Promotional</label>
                                                                           </div>
                                                                        </span>
                                                                     </li>
                                                                     <li>
                                                                        <span class="countno repolinks" onclick="openReport(event, 'gridthree<?php echo $z.$z; ?>')">
                                                                           <div class="funkyradio-default">
                                                                              <label for="checkbox1">I think  it's objectionable</label>
                                                                           </div>
                                                                        </span>
                                                                     </li>
                                                                     <li>
                                                                        <span class="countno repolinks" onclick="openReport(event, 'gridfour<?php echo $z.$z; ?>')">
                                                                           <div class="funkyradio-default">
                                                                              <label for="checkbox1">I think it false or misinformation</label>
                                                                           </div>
                                                                        </span>
                                                                     </li>
                                                                  </ul>
                                                                  <div class="modal-footer">
                                                                  </div>
                                                               </div>

                                                               <div class="repocontent modal-body reportingsets" id="gridtwo<?php echo $z.$z; ?>" style="display:none;">
                                                                  <form method="post" action="">
                                                                  <div class="modal-body">
                                                                     <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                                     <input type="hidden" name="qid" value="<?php echo $quesPost1->id;?>">
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                     <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z; ?>')">Back</button>
                                                                     <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                                  </div>
                                                                  </form>
                                                               </div>
                                                               
                                                               <div class="repocontent modal-body reportingsets" id="gridthree<?php echo $z.$z; ?>" style="display:none;">
                                                                  <form method="post" action="">
                                                                  <div class="modal-body">
                                                                     <div class="funkyradio-default">
                                                                        <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent"  checked="checked">It's pornographic or extremely violent.</label>
                                                                     </div>
                                                                     <div class="funkyradio-default">
                                                                        <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                                     </div>
                                                                     <div class="funkyradio-default">
                                                                        <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                                     </div>
                                                                  </div>
                                                                  <div class="bottom-btns">
                                                                    <input type="hidden" name="qid" value="<?php echo $quesPost1->id;?>">
                                                                     <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z; ?>')">Back</button>
                                                                     <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                                  </div>
                                                                  </form>
                                                               </div>
                                                               <div class="repocontent modal-body reportingsets" id="gridfour<?php echo $z.$z; ?>" style="display:none;">
                                                                  <form method="post" action="">
                                                                  <div class="modal-body">
                                                                     <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                                  </div>
                                                                  <div class="bottom-btns">
                                                                     <input type="hidden" name="qid" value="<?php echo $quesPost1->id;?>">
                                                                     <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z; ?>')">Back</button>
                                                                     <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                                  </div>
                                                                  </form>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>

                                                </div>
                                             </div>
                                             <div class="col-md-12">
                                                <div class="text1">
                                                   <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $quesPost1->id;?>"><?php echo $quesPost1->ques; ?></a></p>
                                                </div>
                                             </div>
                                             <div class="questio">
                                                <div class="col-md-12 col-sm-12">
                                                   <p class="rigwd"> <?php echo $timeAnsGet; ?></p>
                                                </div>
                                             </div>
                                             <div class="col-xs-9 col-md-9">
                                                <div class="post">
                                                   <p><span><?php echo $ansCount[0]->countAns; ?></span><strong>Answers</strong>
                                                   <div class="dotsseprate">.</div>
                                                   </p>
                                                   <!--<p data-toggle="modal" data-target="#myModalrepostlistb<?php //echo $z;?>"><span><?php //if($repostCount){echo $repostCount[0]->countRepost;}else{echo "0";}?></span><strong>Repost</strong>
                                                   <div class="dotsseprate">.</div>
                                                   </p>-->
                                                   <p><span><?php if($viewsCount[0]->view_count){echo $viewsCount[0]->view_count;}else{ echo "0";} ?></span><strong>Views</strong>
                                                   <div class="dotsseprate">.</div>
                                                   </p>
                                                   <p data-toggle="modal" data-target="#myModalfollowlistb<?php echo $z;?>"><span><?php if($quesFollow){ echo $quesFollow[0]->countFollowed; }else{ echo "0";} ?></span><strong>Followers</strong></p>
                                                </div>

                                                      <!---- Modal  --->
                                                      <div id="myModalrepostlistb<?php echo $z;?>" class="modal fade" role="dialog">
                                                      <div class="modal-dialog" style="width: 700px;">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                               <h3></h3>
                                                            </div>
                                                            <div class="modal-body">
                                                               <div class="row">
                                                                  <div clss="col-md-12">
                                                                  <?php
                                                                      $reLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ques' AND shareid=%d", $quesPost1->id));
                                                                      if($reLists) {
                                                                          foreach($reLists as $reList) {
                                                                              $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $reList->user_id));
                                                                      
                                                                              $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $reList->user_id));
                                                                      
                                                                              $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $reList->user_id));

                                                                  ?>   
                                                                     <div class="col-md-6">
                                                                      <div class="poll">
                                                                         <div class="col-md-3 col-sm-2 col-xs-3">
                                                                            <div class="imgforr">
                                                                               <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                            </div>
                                                                         </div>
                                                                         <div class="col-md-9 col-sm-10 col-xs-9">
                                                                            <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                            <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                         </div>
                                                                      </div>
                                                                   </div>
                                                                  <?php
                                                                          }
                                                                      }
                                                                  ?>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      </div>

                                                    <!---- Modal  --->
                                                      <div id="myModalfollowlistb<?php echo $z;?>" class="modal fade" role="dialog">
                                                      <div class="modal-dialog" style="width: 700px;">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                               <h3></h3>
                                                            </div>
                                                            <div class="modal-body">
                                                               <div class="row">
                                                                  <div clss="col-md-12">
                                                                  <?php
                                                                      $foLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ques_follow WHERE ques_id=%d", $quesPost1->id));
                                                                      
                                                                      if($foLists) {
                                                                          foreach($foLists as $foList) {
                                                                            $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $foList->user_id));
                                                                      
                                                                            $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $foList->user_id));
                                                                      
                                                                            $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $foList->user_id));

                                                                  ?>   
                                                                     <div class="col-md-6">
                                                                      <div class="poll">
                                                                         <div class="col-md-3 col-sm-2 col-xs-3">
                                                                            <div class="imgforr">
                                                                               <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                            </div>
                                                                         </div>
                                                                         <div class="col-md-9 col-sm-10 col-xs-9">
                                                                            <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                            <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                         </div>
                                                                      </div>
                                                                   </div>
                                                                  <?php
                                                                          }
                                                                      }
                                                                  ?>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      </div>

                                             </div>
                                             <div class="col-xs-3 col-md-3">
                                                <div class="plugin">
                                                       <div class="dropdown customs">      
                                                  <button class="dropbtn">
                                                    <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                                                  </button>
                                                  <div class="dropdown-content">
                                                    <?php //$urlshare = "http://edukeeda.com/questionanswers?action=".$quesPost3->id;
                                                      //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                                    ?>
                                                      <?php //echo do_shortcode("$urlsharing"); ?>
                                                  </div>
                                                </div>
                                                </div>
                                             </div>
                                             <div class="col-xs-4 col-md-4">
                                                <div class="share">
                                                  <?php
                                                    if(isset($_SESSION["login"])) {
                                                  ?>
                                                    <p data-toggle="modal" data-target="#myModalnmans<?php echo $z.$z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>  
                                                  <?php
                                                    } else {
                                                  ?>
                                                      <p data-toggle="modal" data-target="#myModalnmans<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                                  <?php
                                                    }
                                                  ?>
                                                </div>
                                                    <!-- Modal -->
                                                    <div id="myModalnmans<?php echo $z;?>" class="modal fade" role="dialog">
                                                      <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Post Answer</h4>
                                                          </div>
                                                          <div class="modal-body">
                                                            <p> For post an answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>

                                                    <!-- Modal -->
                                                    <div id="myModalnmans<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                                      <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Post Answer</h4>
                                                          </div>
                                                          <div class="modal-body">
                                                            <form method="post" action="">
                                                              <input type="hidden" name="qid" value="<?php echo $quesPost1->id;?>">
                                                              <?php
                                                                 $my_content1 =" ";
                                                                 $editor_id1 = 'ansPostb'.$z;
                                                                 $option_name1 ='ansPost';
                                                                 wp_editor($my_content1, $editor_id1, array('wpautop'=> true,'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                                                              ?>
                                                              <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                            </form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                             </div>
                                             <div class="col-xs-4 col-md-4">
                                                <div class="share1">
                                                   <?php
                                                      if(isset($_SESSION["login"])) {
                                                        $userId = $_SESSION["login"]["id"];
                                                        
                                                        $checkFollow = $wpdb->get_results( 
                                                        $wpdb->prepare( "SELECT * FROM wp_discussion_ques_follow WHERE user_id=%d AND ques_id=%d", $userId, $quesPost1->id));
                                                        if($checkFollow){
                                                      ?>
                                                            <form method="post" action="">
                                                                <input type="hidden" name="quesId" value="<?php echo $quesPost1->id;?>">
                                                                <img src="/img/follow-icon-selected.png"><button type="submit" name="unfollow"> follow </button>
                                                             </form>
                                                   <?php
                                                      } else{
                                                      ?>
                                                         <form method="post" action="">
                                                            <input type="hidden" name="quesId" value="<?php echo $quesPost1->id;?>">
                                                            <img src="/img/Follow.png"><button type="submit" name="follow"> Follow </button>
                                                         </form>
                                                   <?php 
                                                      }
                                                      } else {
                                                      ?>
                                                          <img src="/img/Follow.png"><button types="button" data-toggle="modal" data-target="#myModalques">Follow</button>
                                                   <?php
                                                      }
                                                      ?>
                                                </div>
                                             </div>
                                             <div class="col-xs-4 col-md-4">
                                                <div class="share2">
                                                  <?php
                                                      if(isset($_SESSION["login"])) {
                                                        $userId = $_SESSION["login"]["id"];
                                                  ?>
                                                        <!--<p data-toggle="modal" data-target="#myModalnmrepost<?php echo $z.$z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                                  <?php
                                                      } else{
                                                  ?>
                                                        <!--<p data-toggle="modal" data-target="#myModalnmrepost<?php echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                                  <?php
                                                      }
                                                  ?>
                                                </div>

                                                    <!-- Modal -->
                                                    <div id="myModalnmrepost<?php echo $z;?>" class="modal fade" role="dialog">
                                                       <div class="modal-dialog">
                                                          <!-- Modal content-->
                                                          <div class="modal-content">
                                                             <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title"> Repost </h4>
                                                             </div>
                                                             <div class="modal-body">
                                                                <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                             </div>
                                                          </div>
                                                       </div>
                                                    </div>

                                                    <!-- Modal -->
                                                    <div id="myModalnmrepost<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                                       <div class="modal-dialog">
                                                          <!-- Modal content-->
                                                          <div class="modal-content">
                                                             <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title"> Repost </h4>
                                                             </div>
                                                             <div class="modal-body">
                                                              <form method="post" action="">
                                                                <p> <textarea name="reposttext"> </textarea></p>
                                                                <div class="col-md-12 repostmodal">
                                                                <?php
                                                                  $userPic = $wpdb->get_results( 
                                                                    $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$quesPost1->user_id));
                                                                  $userReg = $wpdb->get_results($wpdb->prepare("SELECT user_registered FROM wp_users WHERE ID=%d",$quesPost1->user_id));

                                                                  $userName = $wpdb->get_results( 
                                                                    $wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$quesPost1->user_id));
                                                                  $userHeadline = $wpdb->get_results( 
                                                                   $wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$quesPost1->user_id));
                                                                ?>
                                                                <div class="col-md-2 col-sm-2 col-xs-4">
                                                                        <div class="imgforr">
                                                                           <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                        </div>
                                                                     </div>
                                                                <div class="col-md-10 col-sm-10 col-xs-8">
                                                                  <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $ansPost->user_id;?>"><?php echo $userName[0]->name;?></a></strong>,<span><?php if($userHeadline[0]->headline){echo substr($userHeadline[0]->headline, 0,20 );} ?></span></p>
                                                                  <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($userReg[0]->user_registered )); ?> </p>
                                                               </div>
                                                                
                                                                
                                                                  <p> Repost this question </p>
                                                                  <p> Question :  <?php echo $quesPost1->ques; ?> </p>
                                                                </div>
                                                                <input type="hidden" name="type" value="ques">
                                                                <input type="hidden" name="quesid" value="<?php echo $quesPost1->id;?>">
                                                                <input type="hidden" name="ques" value="<?php echo $quesPost1->ques;?>">
                                                                <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                              </form>
                                                             </div>
                                                          </div>
                                                       </div>
                                                    </div>

                                             </div>
                                          </div>      
                              <?php
                                    $z++;
                                 }
                              ?>
                           </div>
                        </div>

                        <div id="articleblous2" class="tabcontent" style="display:block;">
                           <div class="blogaresnews">
                              <?php
                                 $top = date("Y-m-d", strtotime("-1 months"));
                                 $quesPosts3 = $wpdb->get_results($wpdb->prepare("SELECT wp_discussion_ques.id, wp_discussion_ques.main_id, wp_discussion_ques.sub_cat_id, wp_discussion_ques.house_id, wp_discussion_ques.user_id, wp_discussion_ques.ques, wp_discussion_ques.status, wp_discussion_ques.created_at FROM wp_discussion_ques INNER JOIN wp_discussion_views ON wp_discussion_views.ques_id = wp_discussion_ques.id WHERE wp_discussion_ques.created_at >= %s AND wp_discussion_ques.status=1 ORDER BY wp_discussion_views.view_count DESC LIMIT 40", $top));
                                 $z=1;

                                 foreach($quesPosts3 as $quesPost3) {
                                   
                                    if($quesPost3->sub_cat_id == 0) {
                                        $catName3 = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost3->main_id));
                                        $cName3 = $catName3[0]->name;
                                    } else{
                                        $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $quesPost3->main_id));

                                        $catName3 = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $quesPost3->sub_cat_id));
                                        $cName3 = $catName3[0]->sub_name;
                                    }

                                    $houseName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d AND subcat_id=%d", $quesPost3->main_id, $quesPost3->sub_cat_id));

                                    $viewsCount = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_views WHERE ques_id = %d", $quesPost3->id));

                                    $ansCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as countAns FROM wp_discussion_ans WHERE ques_id = %d", $quesPost3->id));

                                    $repostCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as countRepost FROM wp_wallpost WHERE shareType='ques' AND shareid = %d", $quesPost3->id));

                                    $ansTime = $wpdb->get_results($wpdb->prepare("SELECT created_at FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $quesPost3->id));
                                 
                                    $quesFollow = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as countFollowed FROM wp_discussion_ques_follow WHERE ques_id = %d", $quesPost3->id));
                                 
                                    if($ansTime) {
                                     $timeAnsGet = "<strong>Answered </strong> " . time_elapsed_string($ansTime[0]->created_at);
                                   } else {
                                     $timeAnsGet = "<strong>Posted </strong> " . time_elapsed_string($quesPost->created_at);
                                   }

                              ?>
                                    <div class="col-md-12 blo postpanels">
                                       <div class="direct">
                                          <div class="col-md-10 col-sm-10 col-xs-10">
                                             
                                             <p> <?php if($quesPost3->sub_cat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$quesPost3->main_id."'>".$catMainName[0]->name .'</a> . ';}?>
                                                <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $quesPost3->main_id;?> <?php if($quesPost3->sub_cat_id != 0) { echo "&actions=". $quesPost3->sub_cat_id;}?>"><?php echo $cName3; ?></a> . 
                                                <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $quesPost3->house_id;?>"><?php if(strlen($houseName[0]->title) > 40){echo substr($houseName[0]->title, 0,40). ' ...';} else{echo $houseName[0]->title;} ?></a> </p>
                                          </div>
                                       </div>
                                       <div class="col-md-2 col-sm-2 col-xs-2">
                                          <div class="dot">
                                             <div class="dropdown">
                                                <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...
                                                </p>
                                                <ul class="dropdown-menu">
                                                <?php
                                                   if($_SESSION['login']['id']) {
                                                ?>
                                                      <li data-toggle="modal" data-target="#myModalreportc<?php echo $z; ?>">Report This Post</li>
                                                <?php
                                                   } else{
                                                ?>
                                                      <li data-toggle="modal" data-target="#myModalreportnotc<?php echo $z; ?>">Report This Post</li>
                                                <?php
                                                   }
                                                ?>
                                                </ul>
                                             </div>
                                                
                                                <div id="myModalreportnotc<?php echo $z; ?>" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                       <!-- Modal content-->
                                                       <div class="modal-content">
                                                          <div class="modal-header">
                                                             <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                             <h1>Report this post</h1>
                                                          </div>
                                                          <div class="modal-body">
                                                             <p> Please Login and Register first! </p>
                                                          </div>
                                                       </div>
                                                    </div>
                                                 </div>

                                                <div class="modal fade" id="myModalreportc<?php echo $z; ?>" role="dialog">
                                                   <div class="modal-dialog vertical-align-center">
                                                      <div class="modal-content">
                                                         <div class="repocontent modal-body reportingsets" id="gridone<?php echo $z.$z.$z; ?>" style="display:block;">
                                                            <ul>
                                                               <li>
                                                                  <span  class="countno repolinks" onclick="openReport(event, 'gridtwo<?php echo $z.$z.$z; ?>')">
                                                                     <div class="funkyradio-default">
                                                                        <label for="checkbox1">I think it's spam, Promotional</label>
                                                                     </div>
                                                                  </span>
                                                               </li>
                                                               <li>
                                                                  <span class="countno repolinks" onclick="openReport(event, 'gridthree<?php echo $z.$z.$z; ?>')">
                                                                     <div class="funkyradio-default">
                                                                        <label for="checkbox1">I think  it's objectionable</label>
                                                                     </div>
                                                                  </span>
                                                               </li>
                                                               <li>
                                                                  <span class="countno repolinks" onclick="openReport(event, 'gridfour<?php echo $z.$z.$z; ?>')">
                                                                     <div class="funkyradio-default">
                                                                        <label for="checkbox1">I think it false or misinformation</label>
                                                                     </div>
                                                                  </span>
                                                               </li>
                                                            </ul>
                                                            <div class="modal-footer">
                                                            </div>
                                                         </div>

                                                         <div class="repocontent modal-body reportingsets" id="gridtwo<?php echo $z.$z.$z; ?>" style="display:none;">
                                                            <form method="post" action="">
                                                            <div class="modal-body">
                                                               <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                               <input type="hidden" name="qid" value="<?php echo $quesPost3->id;?>">
                                                            </div>
                                                            <div class="modal-footer">
                                                               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z; ?>')">Back</button>
                                                               <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                            </div>
                                                            </form>
                                                         </div>
                                                         
                                                         <div class="repocontent modal-body reportingsets" id="gridthree<?php echo $z.$z.$z; ?>" style="display:none;">
                                                            <form method="post" action="">
                                                            <div class="modal-body">
                                                               <div class="funkyradio-default">
                                                                  <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent" checked="checked">It's pornographic or extremely violent.</label>
                                                               </div>
                                                               <div class="funkyradio-default">
                                                                  <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                               </div>
                                                               <div class="funkyradio-default">
                                                                  <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                               </div>
                                                            </div>
                                                            <div class="bottom-btns">
                                                              <input type="hidden" name="qid" value="<?php echo $quesPost3->id;?>">
                                                               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z; ?>')">Back</button>
                                                               <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                            </div>
                                                            </form>
                                                         </div>
                                                         <div class="repocontent modal-body reportingsets" id="gridfour<?php echo $z.$z.$z; ?>" style="display:none;">
                                                            <form method="post" action="">
                                                            <div class="modal-body">
                                                               <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                            </div>
                                                            <div class="bottom-btns">
                                                               <input type="hidden" name="qid" value="<?php echo $quesPost3->id;?>">
                                                               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z; ?>')">Back</button>
                                                               <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                            </div>
                                                            </form>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>

                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="text1">
                                             <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $quesPost3->id;?>"><?php echo $quesPost3->ques; ?></a></p>
                                          </div>
                                       </div>
                                       <div class="questio">
                                          <div class="col-md-12 col-sm-12">
                                             <p class="rigwd"> <?php echo $timeAnsGet; ?></p>
                                          </div>
                                       </div>
                                       <div class="col-xs-9 col-md-9">
                                          <div class="post">
                                             <p><span><?php echo $ansCount[0]->countAns; ?></span><strong>Answers</strong>
                                             <div class="dotsseprate">.</div>
                                             </p>
                                             
                                             <!--<p data-toggle="modal" data-target="#myModalrepostlistc<?php //echo $z;?>"><span><?php //if($repostCount){echo $repostCount[0]->countRepost;}else{echo "0";}?></span><strong>Repost</strong>
                                             <div class="dotsseprate">.</div>
                                             </p>-->

                                             <p><span><?php if($viewsCount[0]->view_count){echo $viewsCount[0]->view_count;}else{ echo "0";} ?></span><strong>Views</strong>
                                             <div class="dotsseprate">.</div>
                                             </p>
                                             
                                             <p data-toggle="modal" data-target="#myModalfollowlistc<?php echo $z;?>"><span><?php if($quesFollow){ echo $quesFollow[0]->countFollowed; }else{ echo "0";} ?></span><strong>Followers</strong></p>
                                          </div>

                                                <!---- Modal  --->
                                                <div id="myModalrepostlistc<?php echo $z;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog" style="width: 700px;">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                         <h3></h3>
                                                      </div>
                                                      <div class="modal-body">
                                                         <div class="row">
                                                            <div clss="col-md-12">
                                                            <?php
                                                                $reLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ques' AND shareid=%d", $quesPost3->id));
                                                                if($reLists) {
                                                                    foreach($reLists as $reList) {
                                                                        $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $reList->user_id));
                                                                
                                                                        $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $reList->user_id));
                                                                
                                                                        $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $reList->user_id));

                                                            ?>   
                                                               <div class="col-md-6">
                                                                <div class="poll">
                                                                   <div class="col-md-3 col-sm-2 col-xs-3">
                                                                      <div class="imgforr">
                                                                         <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                      </div>
                                                                   </div>
                                                                   <div class="col-md-9 col-sm-10 col-xs-9">
                                                                      <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                      <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                   </div>
                                                                </div>
                                                             </div>
                                                            <?php
                                                                    }
                                                                }
                                                            ?>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                </div>

                                              <!---- Modal  --->
                                                <div id="myModalfollowlistc<?php echo $z;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog" style="width: 700px;">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                         <h3></h3>
                                                      </div>
                                                      <div class="modal-body">
                                                         <div class="row">
                                                            <div clss="col-md-12">
                                                            <?php
                                                                $foLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ques_follow WHERE ques_id=%d", $quesPost3->id));
                                                                if($foLists) {
                                                                    foreach($foLists as $foList) {
                                                                      $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $foList->user_id));
                                                                
                                                                      $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $foList->user_id));
                                                                
                                                                      $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $foList->user_id));

                                                            ?>   
                                                               <div class="col-md-6">
                                                                <div class="poll">
                                                                   <div class="col-md-3 col-sm-2 col-xs-3">
                                                                      <div class="imgforr">
                                                                         <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                      </div>
                                                                   </div>
                                                                   <div class="col-md-9 col-sm-10 col-xs-9">
                                                                      <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                      <p class="rigwd"> <?php echo $userDate;?> </p>
                                                                   </div>
                                                                </div>
                                                             </div>
                                                            <?php
                                                                    }
                                                                }
                                                            ?>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                </div>

                                       </div>
                                       <div class="col-xs-3 col-md-3">
                                          <div class="plugin">
                                            <div class="dropdown customs">      
                                              <button class="dropbtn">
                                                <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                                              </button>
                                              <div class="dropdown-content">
                                                <?php //$urlshare = "http://edukeeda.com/questionanswers?action=".$quesPost3->id;
                                                  //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                                ?>
                                                  <?php //echo do_shortcode("$urlsharing"); ?>
                                              </div>
                                            </div>
                                          </div>
                                       </div>
                                       <div class="col-xs-4 col-md-4">
                                          <div class="share">
                                            <?php
                                              if(isset($_SESSION["login"])) {
                                            ?>
                                              <p data-toggle="modal" data-target="#myModalmjans<?php echo $z.$z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>  
                                            <?php
                                              } else {
                                            ?>
                                                <p data-toggle="modal" data-target="#myModalmjans<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                            <?php
                                              }
                                            ?>
                                          </div>
                                              <!-- Modal -->
                                              <div id="myModalmjans<?php echo $z;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title">Post Answer</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                      <p> For post an answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>

                                              <!-- Modal -->
                                              <div id="myModalmjans<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                    <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                      <h4 class="modal-title">Post Answer</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                      <form method="post" action="">
                                                        <input type="hidden" name="qid" value="<?php echo $quesPost3->id;?>">
                                                        <?php
                                                        $my_content1 =" ";
                                                        $editor_id1 = 'ansPostc'.$z;
                                                        $option_name1 ='ansPost';
                                                        wp_editor($my_content1, $editor_id1, array('wpautop'=> false,'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                                                     ?>
                                                        <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                      </form>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                       </div>
                                       <div class="col-xs-4 col-md-4">
                                          <div class="share1">
                                             <?php
                                                if(isset($_SESSION["login"])) {
                                                  $userId = $_SESSION["login"]["id"];
                                                  
                                                  $checkFollow = $wpdb->get_results( 
                                                  $wpdb->prepare( "SELECT * FROM wp_discussion_ques_follow WHERE user_id=%d AND ques_id=%d", $userId, $quesPost3->id));
                                                  if($checkFollow){
                                                ?>
                                                      <form method="post" action="">
                                                          <input type="hidden" name="quesId" value="<?php echo $quesPost3->id;?>">
                                                          <img src="/img/follow-icon-selected.png"><button type="submit" name="unfollow"> follow </button>
                                                       </form>
                                             <?php
                                                } else{
                                                ?>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="quesId" value="<?php echo $quesPost3->id;?>">
                                                      <img src="/img/Follow.png"><button type="submit" name="follow"> Follow </button>
                                                   </form>
                                             <?php 
                                                }
                                                } else {
                                                ?>
                                                    <img src="/img/Follow.png"><button types="button" data-toggle="modal" data-target="#myModalques">Follow</button>
                                             <?php
                                                }
                                                ?>
                                          </div>
                                       </div>
                                       <div class="col-xs-4 col-md-4">
                                          <div class="share2">
                                            <?php
                                                if(isset($_SESSION["login"])) {
                                                  $userId = $_SESSION["login"]["id"];
                                            ?>
                                                  <!--<p data-toggle="modal" data-target="#myModalmjrepost<?php //echo $z.$z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                            <?php
                                                } else{
                                            ?>
                                                  <!--<p data-toggle="modal" data-target="#myModalmjrepost<?php //echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                            <?php
                                                }
                                            ?>
                                          </div>

                                              <!-- Modal -->
                                              <div id="myModalmjrepost<?php echo $z;?>" class="modal fade" role="dialog">
                                                 <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                       <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                          <h4 class="modal-title"> Repost </h4>
                                                       </div>
                                                       <div class="modal-body">
                                                          <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                              <!-- Modal -->
                                              <div id="myModalmjrepost<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                                 <div class="modal-dialog">
                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                       <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                          <h4 class="modal-title"> Repost </h4>
                                                       </div>
                                                       <div class="modal-body">
                                                        <form method="post" action="">
                                                          <p> <textarea name="reposttext"> </textarea></p>
                                                          <div class="repostmodal">
                                                          <?php
                                                                  $userPic = $wpdb->get_results( 
                                                                    $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$quesPost1->user_id));
                                                                  $userReg = $wpdb->get_results($wpdb->prepare("SELECT user_registered FROM wp_users WHERE ID=%d",$quesPost3->user_id));
                                                                  $userName = $wpdb->get_results( 
                                                                    $wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$quesPost1->user_id));
                                                                  $userHeadline = $wpdb->get_results( 
                                                                   $wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$quesPost1->user_id));
                                                                ?>
                                                                <div class="col-md-2 col-sm-2 col-xs-4">
                                                                        <div class="imgforr">
                                                                           <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                        </div>
                                                                     </div>
                                                                <div class="col-md-10 col-sm-10 col-xs-8">
                                                                  <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $ansPost->user_id;?>"><?php echo $userName[0]->name;?></a></strong>,<span><?php if($userHeadline[0]->headline){echo substr($userHeadline[0]->headline, 0,20 );}?></span></p>
                                                                  <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($userReg[0]->user_registered )); ?> </p>
                                                               </div>
                                                          
                                                            <p> Repost this question </p>
                                                            <p> Question :  <?php echo $quesPost3->ques; ?> </p>
                                                          </div>
                                                          <input type="hidden" name="type" value="ques">
                                                          <input type="hidden" name="quesid" value="<?php echo $quesPost3->id;?>">
                                                          <input type="hidden" name="ques" value="<?php echo $quesPost3->ques;?>">
                                                          <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                        </form>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                       </div>
                                    </div>
                              <?php
                                    $z++;
                                 }
                              ?>
                           </div>
                        </div>
                        

                        <div id="anwerspat" class="tabcontent" style="display:none;">
                           <div class="blogaresnews">
                              <?php
                                 $ansPosts = $wpdb->get_results("SELECT * FROM wp_discussion_ques WHERE status='1' ORDER BY id DESC LIMIT 60");
                                  $z=1;
                                 foreach($ansPosts as $ansPost) {
                                   
                                    if($ansPost->sub_cat_id == 0) {
                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $ansPost->main_id));
                                        $cName1 = $catName[0]->name;
                                    } else{
                                        $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $ansPost->main_id));

                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $ansPost->sub_cat_id));
                                        $cName1 = $catName[0]->sub_name;
                                    }
                                    $houseName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d AND subcat_id=%d", $ansPost->main_id, $ansPost->sub_cat_id));
                                    $viewsCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT * FROM wp_discussion_views WHERE ques_id = %d", $ansPost->id));

                                   $lastAns = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $ansPost->id));
                                 
                                   $ansTime1 = $wpdb->get_results($wpdb->prepare("SELECT created_at FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $ansPost->id));

                                   if($ansTime) {
                                     $timeAnsGet = time_elapsed_string($ansTime[0]->created_at);
                                   } else {
                                     $timeAnsGet = " - ";
                                   }
                                 
                                   $userPic = $wpdb->get_results( 
                                     $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$lastAns[0]->user_id));

                                   $userName = $wpdb->get_results($wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$lastAns[0]->user_id));
                                   
                                   $userHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$lastAns[0]->user_id));
                                    
                                    if($lastAns) {
                                      $countLike = $wpdb->get_results( 
                                        $wpdb->prepare("SELECT COUNT(id) as cLike FROM wp_answer_like WHERE ans_id=%d", $lastAns[0]->id));

                                      $countReply = $wpdb->get_results( 
                                        $wpdb->prepare("SELECT COUNT(id) as cReply FROM wp_discussion_reply WHERE ans_id=%d",$lastAns[0]->id));
                                    
                                      $repostCount = $wpdb->get_results( 
                                        $wpdb->prepare("SELECT COUNT(id) as countRepost FROM wp_wallpost WHERE shareType='ans' AND shareid=%d", $lastAns[0]->id));
                                    }
                                   if($lastAns) {

                                ?>
                                  <div class="col-md-12 blo">
                                     <div class="direct">
                                        <div class="col-md-10 col-sm-10 col-xs-10">

                                           <p> <?php if($ansPost->sub_cat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$ansPost->main_id."'>".$catMainName[0]->name .'</a> . ';}?>  <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $ansPost->main_id;?> <?php if($ansPost->sub_cat_id != 0) { echo "&actions=". $ansPost->sub_cat_id;}?>"><?php echo $cName1; ?></a> . <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $ansPost->house_id;?>"><?php if(strlen($houseName[0]->title) > 30){echo substr($houseName[0]->title, 0,30). ' ...';} else{echo $houseName[0]->title;} ?></a> </p>
                                           
                                        </div>
                                     </div>
                                     <div class="col-md-2 col-sm-2 col-xs-2">
                                        <div class="dot">
                                           <div class="dropdown">
                                              <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...
                                              </p>
                                              <ul class="dropdown-menu">
                                             <?php
                                                if($_SESSION['login']['id']) {
                                             ?>
                                                 <li data-toggle="modal" data-target="#myModalansreport1<?php echo $z; ?>">Report This Post</li>
                                             <?php
                                                 } else{
                                              ?>
                                                    <li data-toggle="modal" data-target="#myModalreportnot1<?php echo $z; ?>">Report This Post</li>
                                              <?php
                                                 }
                                              ?>
                                              </ul>
                                           </div>

                                                <div id="myModalreportnot1<?php echo $z; ?>" class="modal fade" role="dialog">
                                                  <div class="modal-dialog">
                                                     <!-- Modal content-->
                                                     <div class="modal-content">
                                                        <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                           <h1>Report this post</h1>
                                                        </div>
                                                        <div class="modal-body">
                                                           <p> Please Login and Register first! </p>
                                                        </div>
                                                     </div>
                                                  </div>
                                                </div>

                                                <div class="modal fade" id="myModalansreport1<?php echo $z; ?>" role="dialog">
                                                 <div class="modal-dialog vertical-align-center">
                                                    <div class="modal-content">
                                                       <div class="repocontent modal-body reportingsets" id="gridone<?php echo $z.$z.$z.$z; ?>" style="display:block;">
                                                          <ul>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridtwo<?php echo $z.$z.$z.$z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it's spam, Promotional</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridthree<?php echo $z.$z.$z.$z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think  it's objectionable</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridfour<?php echo $z.$z.$z.$z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it false or misinformation</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                          </ul>
                                                          <div class="modal-footer">
                                                          </div>
                                                       </div>

                                                       <div class="repocontent modal-body reportingsets" id="gridtwo<?php echo $z.$z.$z.$z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                             <input type="hidden" name="aid" value="<?php echo $lastAns[0]->id;?>">
                                                          </div>
                                                          <div class="modal-footer">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z.$z; ?>')">Back</button>
                                                             <button type="submit" name="submitansReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       
                                                       <div class="repocontent modal-body reportingsets" id="gridthree<?php echo $z.$z.$z.$z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent"  checked="checked">It's pornographic or extremely violent.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                             </div>
                                                          </div>
                                                          <div class="bottom-btns">
                                                            <input type="hidden" name="aid" value="<?php echo $lastAns[0]->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z.$z; ?>')">Back</button>
                                                             <button type="submit" name="submitansReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       <div class="repocontent modal-body reportingsets" id="gridfour<?php echo $z.$z.$z.$z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                          </div>
                                                          <div class="bottom-btns">
                                                             <input type="hidden" name="aid" value="<?php echo $lastAns[0]->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z.$z; ?>')">Back</button>
                                                             <button type="submit" name="submitansReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                        </div>
                                     </div>
                                     <div class="col-md-12">
                                        <div class="text">
                                           <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $ansPost->id;?>"><?php echo $ansPost->ques; ?></a></p>
                                        </div>
                                     </div>
                                     <div class="imgforr">
                                        <div class="col-md-2 col-sm-2 col-xs-3">
                                           <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                        </div>
                                     </div>
                                     <div class="col-md-10 col-sm-10 col-xs-9">
                                        <p class="catp"><strong><a href="#"><?php echo $userName[0]->name;?></a></strong>,<span><?php echo $userHeadline[0]->headline;?></span></p>
                                        <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($lastAns[0]->created_at )); ?> </p>
                                     </div>
                                     <div class="col-md-12">
                                        <div class="text2">
                                           <span><?php if(strlen($lastAns[0]->ans_content) > 180){echo substr($lastAns[0]->ans_content, 0,180). '';} else{echo $lastAns[0]->ans_content;} ?></span>
                                       <?php
                                          if(strlen($lastAns[0]->ans_content > 180)) {
                                       ?>
                                           <span class="anscomplete<?php echo $z;?>" style="display:none;"><?php echo substr($lastAns[0]->ans_content, 180,300); ?></span>
                                           
                                           <span class="ansmore<?php echo $z;?> ansmorecolor" id="<?php echo $z;?>" onClick="moreFunction(this.id)">more...</span>
                                       <?php
                                          }
                                       ?>
                                        </div>
                                     </div>
                                     <div class="col-md-12">
                                        <div class="pp">
                                           <p><span><?php if($countLike){echo $countLike[0]->cLike;}else{echo "0";} ?></span><strong>Likes.</strong></p>
                                           <p data-toggle="modal" data-target="#myModalreplylistsa<?php echo $z;?>"><span><?php if($countReply){echo $countReply[0]->cReply;}else{echo "0";} ?></span><strong>Reply.</strong></p>
                                           <!--<p data-toggle="modal" data-target="#myModalrepostlistd<?php //echo $z;?>"><span><?php //if($repostCount){echo $repostCount[0]->countRepost;}else{echo "0";}?></span><strong>Repost.</strong></p>-->
                                        </div>

                                             <!---- Modal  --->
                                          <div id="myModalreplylistsa<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h3> Reply </h3>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                      <div clss="col-md-12">
                                                      <?php
                                                        $replies = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_reply WHERE ans_id=%d", $lastAns[0]->id));
                                                        
                                                        if($replies) {
                                                          foreach($replies as $replie) {
                                                            $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $replie->user_id));
                                                          
                                                            $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $replie->user_id));
                                                          
                                                            $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $replie->user_id));

                                                      ?>   
                                                         <div class="col-md-12">
                                                            <div class="poll">
                                                               <div class="left">
                                                                  <div class="imgforr">
                                                                     <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                  </div>
                                                               </div>
                                                               <div class="right">
                                                                  <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallReply->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong></p>
                                                               </div>
                                                               <div class="custext">
                                                                  <div class="text2">
                                                                     <p> <?php echo $replie->reply; ?></p>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      <?php
                                                              }
                                                          }
                                                      ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>

                                          <!---- Modal  --->
                                          <div id="myModalrepostlistd<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog" style="width: 700px;">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h3></h3>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                      <div clss="col-md-12">
                                                      <?php
                                                          $reLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ans' AND shareid=%d", $lastAns[0]->id));
                                                          if($reLists) {
                                                              foreach($reLists as $reList) {
                                                                  $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $reList->user_id));
                                                          
                                                                  $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $reList->user_id));
                                                          
                                                                  $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $reList->user_id));

                                                      ?>   
                                                         <div class="col-md-6">
                                                          <div class="poll">
                                                             <div class="col-md-3 col-sm-2 col-xs-3">
                                                                <div class="imgforr">
                                                                   <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                </div>
                                                             </div>
                                                             <div class="col-md-9 col-sm-10 col-xs-9">
                                                                <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                <p class="rigwd"> <?php echo $userDate;?> </p>
                                                             </div>
                                                          </div>
                                                       </div>
                                                      <?php
                                                              }
                                                          }
                                                      ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>

                                     </div>
                                     <div class="col-xs-4 col-md-4">
                                        <div class="share like">
                                           <?php
                                              if(isset($_SESSION["login"])) {
                                                $userId = $_SESSION["login"]['id'];
                                                $checkLike = $wpdb->get_results( 
                                                  $wpdb->prepare("SELECT * FROM wp_answer_like WHERE user_id=%d AND ans_id=%d", $userId, $lastAns[0]->id));
                                                
                                                if($checkLike){
                                            ?>
                                                  <form method="post" action="">
                                                      <input type="hidden" name="ansid" value="<?php echo $lastAns[0]->id;?>">
                                                      <button type="submit" name="unlikeSubmit" class="unlikecolor"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Like</button>
                                                  </form>
                                            <?php
                                                 } else{
                                            ?>
                                                    <form method="post" action="">
                                                        <input type="hidden" name="ansid" value="<?php echo $lastAns[0]->id;?>">
                                                        <button type="submit" name="likeSubmit"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                                    </form>
                                            <?php
                                                 }
                                              } else {
                                            ?>
                                                <p data-toggle="modal" data-target="#myModallikea<?php echo $z;?>"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</p>
                                            <?php
                                              }
                                            ?>
                                        </div>
                                                <!-- Modal -->
                                                  <div id="myModallikea<?php echo $z;?>" class="modal fade" role="dialog">
                                                     <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                           <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                              <h4 class="modal-title">Like</h4>
                                                           </div>
                                                           <div class="modal-body">
                                                              <p> For Like this answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                           </div>
                                                        </div>
                                                     </div>
                                                  </div>
                                     </div>
                                     <div class="col-xs-4 col-md-4">
                                        <div class="share1">
                                           <?php
                                              if(isset($_SESSION["login"])) {
                                            ?>
                                              <p data-toggle="modal" data-target="#myModalnhrep<?php echo $z.$z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                            <?php
                                              } else {
                                            ?>
                                                <p data-toggle="modal" data-target="#myModalnhrep<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                            <?php
                                              }
                                            ?>
                                        </div>
                                            <!-- Modal -->
                                            <div id="myModalnhrep<?php echo $z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Reply</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <p> For reply on answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- Modal -->
                                            <div id="myModalnhrep<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Reply</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form method="post" action="">
                                                      <input type="hidden" name="rid" value="<?php echo $lastAns[0]->id;?>">
                                                      <?php
                                                       $my_content1 =" ";
                                                       $editor_id1 = 'replyPosta'.$z;
                                                       $option_name1 ='replyPost';
                                                       wp_editor($my_content1, $editor_id1, array('wpautop'=> false,'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                                                       ?>
                                                      <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                    </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                     </div>
                                     <div class="col-xs-4 col-md-4">
                                        <div class="share2">
                                           <?php
                                              if(isset($_SESSION["login"])) {
                                                $userId = $_SESSION["login"]["id"];
                                          ?>
                                                <!--<p data-toggle="modal" data-target="#myModalnhrepost1<?php //echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                          <?php
                                              } else{
                                          ?>
                                                <!--<p data-toggle="modal" data-target="#myModalnhrepost1<?php //echo $z.$z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                          <?php
                                              }
                                          ?>
                                        </div>
                                              <!-- Modal -->
                                        <div id="myModalnhrepost1<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                           <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                 <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"> Repost </h4>
                                                 </div>
                                                 <div class="modal-body">
                                                    <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>

                                        <!-- Modal -->
                                        <div id="myModalnhrepost1<?php echo $z;?>" class="modal fade" role="dialog">
                                           <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                 <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"> Repost </h4>
                                                 </div>
                                                 <div class="modal-body">
                                                  <form method="post" action="">
                                                    <p> <textarea name="reposttext"> </textarea></p>
                                                    <div class="row repostmodal">
                                                        <?php
                                                            $userPica = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$ansPost->user_id));
                                                            
                                                            $userRega = $wpdb->get_results($wpdb->prepare("SELECT user_registered FROM wp_users WHERE ID=%d",$ansPost->user_id));

                                                            $userNamea = $wpdb->get_results($wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$ansPost->user_id));

                                                            $userHeadlinea = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$ansPost->user_id));
                                                         ?>
                                                        <div class="col-md-12">
                                                            <div class="text">
                                                               <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $ansPost->id;?>"><?php echo $ansPost->ques; ?></a></p>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-2 col-sm-2 col-xs-4">
                                                            <div class="imgforr">
                                                               <a href="#"><img src="<?php echo $userPica[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-10 col-sm-10 col-xs-8">
                                                            <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $ansPost->user_id;?>"><?php echo $userNamea[0]->name;?></a></strong>,<span><?php if($userHeadlinea[0]->headline){echo substr($userHeadlinea[0]->headline, 0,20 );} ?></span></p>
                                                            <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($userRega[0]->user_registered )); ?> </p>
                                                         </div>
                                                         <div class="col-md-12">
                                                            <div class="text2">
                                                               <p><?php echo $lastAns[0]->ans_content; ?></p>
                                                            </div>
                                                         </div>
                                                    </div>
                                                    
                                                    <input type="hidden" name="type" value="ans">
                                                    <input type="hidden" name="ansid" value="<?php echo $lastAns[0]->id;?>">
                                                    <input type="hidden" name="ans" value="<?php echo $lastAns[0]->ans_content;?>">
                                                    <p> <input type="submit" name="repostanssubmit" value="Repost"> </p>
                                                  </form>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>


                                     </div>
                                  </div>

                                <?php
                                  }
                                  $z++;
                                }
                              ?>
                           </div>
                        </div>


                        <div id="anwerspat1" class="tabcontent" style="display:none;">
                           <div class="blogaresnews">
                              <?php
                                 $ansPosts = $wpdb->get_results("SELECT wp_discussion_ques.id, wp_discussion_ques.main_id, wp_discussion_ques.sub_cat_id, wp_discussion_ques.house_id, wp_discussion_ques.user_id, wp_discussion_ques.ques, wp_discussion_ques.status, wp_discussion_ques.created_at FROM wp_discussion_ques INNER JOIN wp_discussion_views ON wp_discussion_views.ques_id = wp_discussion_ques.id AND wp_discussion_ques.status='1' ORDER BY wp_discussion_views.view_count DESC LIMIT 70");
                                  $z=1;
                                 foreach($ansPosts as $ansPost) {
                                   
                                    if($ansPost->sub_cat_id == 0) {
                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $ansPost->main_id));
                                        $cName1 = $catName[0]->name;
                                    } else{
                                        $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $ansPost->main_id));

                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $ansPost->sub_cat_id));
                                        $cName1 = $catName[0]->sub_name;
                                    }
                                    $houseName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d AND subcat_id=%d", $ansPost->main_id, $ansPost->sub_cat_id));
                                    $viewsCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT * FROM wp_discussion_views WHERE ques_id = %d", $ansPost->id));

                                   $lastAns = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $ansPost->id));
                                 
                                   $ansTime1 = $wpdb->get_results($wpdb->prepare("SELECT created_at FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $ansPost->id));

                                   if($ansTime) {
                                     $timeAnsGet = time_elapsed_string($ansTime[0]->created_at);
                                   } else {
                                     $timeAnsGet = " - ";
                                   }
                                 
                                   $userPic = $wpdb->get_results( 
                                     $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$lastAns[0]->user_id) 
                                   );
                                   $userName = $wpdb->get_results( 
                                     $wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$lastAns[0]->user_id));
                                   $userHeadline = $wpdb->get_results( 
                                     $wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$lastAns[0]->user_id));
                                    
                                    if($lastAns) {
                                        $countLike = $wpdb->get_results( 
                                         $wpdb->prepare("SELECT COUNT(id) as cLike FROM wp_answer_like WHERE ans_id=%d", $lastAns[0]->id));

                                        $countReply = $wpdb->get_results( 
                                         $wpdb->prepare("SELECT COUNT(id) as cReply FROM wp_discussion_reply WHERE ans_id=%d",$lastAns[0]->id));

                                        $repostCount = $wpdb->get_results( 
                                          $wpdb->prepare("SELECT COUNT(id) as countRepost FROM wp_wallpost WHERE shareType='ans' AND shareid=%d", $lastAns[0]->id));
                                    }

                                   if($lastAns) {
                                ?>
                                  <div class="col-md-12 blo">
                                     <div class="direct">
                                        <div class="col-md-10 col-sm-10 col-xs-10">
                                           <p> <?php if($ansPost->sub_cat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$ansPost->main_id."'>".$catMainName[0]->name .'</a> . ';}?>  <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $ansPost->main_id;?> <?php if($ansPost->sub_cat_id != 0) { echo "&actions=". $ansPost->sub_cat_id;}?>"><?php echo $cName1; ?></a> . <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $ansPost->house_id;?>"><?php if(strlen($houseName[0]->title) > 30){echo substr($houseName[0]->title, 0,30). ' ...';} else{echo $houseName[0]->title;} ?></a> </p>
                                        </div>
                                     </div>
                                     <div class="col-md-2 col-sm-2 col-xs-2">
                                        <div class="dot">
                                           <div class="dropdown">
                                              <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...
                                              </p>
                                              <ul class="dropdown-menu">
                                              <?php
                                                if($_SESSION['login']['id']) {
                                              ?>
                                                 <li data-toggle="modal" data-target="#myModalansreport11<?php echo $z; ?>">Report This Post</li>
                                              <?php
                                                 } else{
                                              ?>
                                                    <li data-toggle="modal" data-target="#myModalreportnot11<?php echo $z; ?>">Report This Post</li>
                                              <?php
                                                 }
                                              ?>
                                              </ul>
                                           </div>

                                                <div id="myModalreportnot11<?php echo $z; ?>" class="modal fade" role="dialog">
                                                  <div class="modal-dialog">
                                                     <!-- Modal content-->
                                                     <div class="modal-content">
                                                        <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                           <h1>Report this post</h1>
                                                        </div>
                                                        <div class="modal-body">
                                                           <p> Please Login and Register first! </p>
                                                        </div>
                                                     </div>
                                                  </div>
                                              </div>

                                                <div class="modal fade" id="myModalansreport11<?php echo $z; ?>" role="dialog">
                                                 <div class="modal-dialog vertical-align-center">
                                                    <div class="modal-content">
                                                       <div class="repocontent modal-body reportingsets" id="gridone<?php echo $z.$z.$z.$z.$z; ?>" style="display:block;">
                                                          <ul>
                                                             <li>
                                                                <span  class="countno repolinks" onclick="openReport(event, 'gridtwo<?php echo $z.$z.$z.$z.$z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it's spam, Promotional</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridthree<?php echo $z.$z.$z.$z.$z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think  it's objectionable</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridfour<?php echo $z.$z.$z.$z.$z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it false or misinformation</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                          </ul>
                                                          <div class="modal-footer">
                                                          </div>
                                                       </div>

                                                       <div class="repocontent modal-body reportingsets" id="gridtwo<?php echo $z.$z.$z.$z.$z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                             <input type="hidden" name="aid" value="<?php echo $lastAns[0]->id;?>">
                                                          </div>
                                                          <div class="modal-footer">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z.$z.$z; ?>')">Back</button>
                                                             <button type="submit" name="submitansReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       
                                                       <div class="repocontent modal-body reportingsets" id="gridthree<?php echo $z.$z.$z.$z.$z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent" checked="checked">It's pornographic or extremely violent.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                             </div>
                                                          </div>
                                                          <div class="bottom-btns">
                                                            <input type="hidden" name="aid" value="<?php echo $lastAns[0]->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z.$z.$z; ?>')">Back</button>
                                                             <button type="submit" name="submitansReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       <div class="repocontent modal-body reportingsets" id="gridfour<?php echo $z.$z.$z.$z.$z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                          </div>
                                                          <div class="bottom-btns">
                                                             <input type="hidden" name="aid" value="<?php echo $lastAns[0]->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z.$z.$z; ?>')">Back</button>
                                                             <button type="submit" name="submitansReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                        </div>
                                     </div>
                                     <div class="col-md-12">
                                        <div class="text">
                                           <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $ansPost->id;?>"><?php echo $ansPost->ques; ?></a></p>
                                        </div>
                                     </div>
                                     <div class="imgforr">
                                        <div class="col-md-2 col-sm-2 col-xs-3">
                                           <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                        </div>
                                     </div>
                                     <div class="col-md-10 col-sm-10 col-xs-9">
                                        <p class="catp"><strong><a href="#"><?php echo $userName[0]->name;?></a></strong>,<span><?php echo $userHeadline[0]->headline;?></span></p>
                                        <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($lastAns[0]->created_at )); ?> </p>
                                     </div>
                                     <div class="col-md-12">
                                        <div class="text2">
                                           <span><?php if(strlen($lastAns[0]->ans_content) > 180){echo substr($lastAns[0]->ans_content, 0,180). '';} else{echo $lastAns[0]->ans_content;} ?></span>
                                          <?php
                                             if(strlen($lastAns[0]->ans_content) > 180) {
                                          ?>
                                           <span class="anscomplete<?php echo $z.$z.$z;?>" style="display:none;"><?php echo substr($lastAns[0]->ans_content, 180,300); ?></span>
                                           
                                           <span class="ansmore<?php echo $z.$z.$z;?> ansmorecolor" id="<?php echo $z.$z.$z;?>" onClick="moreFunction(this.id)">more...</span>
                                          <?php
                                             }
                                          ?>
                                        </div>
                                     </div>
                                     <div class="col-md-12">
                                        <div class="pp">
                                           <p><span><?php if($countLike){echo $countLike[0]->cLike;}else{echo "0";} ?></span><strong>Likes.</strong></p>
                                           <p data-toggle="modal" data-target="#myModalreplylistsb<?php echo $z;?>"><span><?php if($countReply){echo $countReply[0]->cReply;}else{echo "0";} ?></span><strong>Reply.</strong></p>
                                           <!--<p data-toggle="modal" data-target="#myModalrepostliste<?php //echo $z;?>"><span><?php //if($repostCount){echo $repostCount[0]->countRepost;}else{echo "0";}?></span><strong>Repost.</strong></p>-->
                                        </div>

                                                <!---- Modal  --->
                                          <div id="myModalreplylistsb<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h3> Reply </h3>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                      <div clss="col-md-12">
                                                      <?php
                                                        $replies = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_reply WHERE ans_id=%d", $lastAns[0]->id));
                                                        
                                                        if($replies) {
                                                          foreach($replies as $replie) {
                                                            $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $replie->user_id));
                                                          
                                                            $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $replie->user_id));
                                                          
                                                            $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $replie->user_id));

                                                      ?>   
                                                         <div class="col-md-12">
                                                            <div class="poll">
                                                               <div class="left">
                                                                  <div class="imgforr">
                                                                     <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                  </div>
                                                               </div>
                                                               <div class="right">
                                                                  <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallReply->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong></p>
                                                               </div>
                                                               <div class="custext">
                                                                  <div class="text2">
                                                                     <p> <?php echo $replie->reply; ?></p>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      <?php
                                                              }
                                                          }
                                                      ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>

                                          <!---- Modal  --->
                                          <div id="myModalrepostliste<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog" style="width: 700px;">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h3></h3>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                      <div clss="col-md-12">
                                                      <?php
                                                          $reLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ans' AND shareid=%d", $lastAns[0]->id));
                                                          if($reLists) {
                                                              foreach($reLists as $reList) {
                                                                  $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $reList->user_id));
                                                          
                                                                  $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $reList->user_id));
                                                          
                                                                  $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $reList->user_id));

                                                      ?>   
                                                         <div class="col-md-6">
                                                          <div class="poll">
                                                             <div class="col-md-3 col-sm-2 col-xs-3">
                                                                <div class="imgforr">
                                                                   <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                </div>
                                                             </div>
                                                             <div class="col-md-9 col-sm-10 col-xs-9">
                                                                <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                <p class="rigwd"> <?php echo $userDate;?> </p>
                                                             </div>
                                                          </div>
                                                       </div>
                                                      <?php
                                                              }
                                                          }
                                                      ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>

                                     </div>
                                     <div class="col-xs-3 col-md-4">
                                        <div class="share like">
                                           <?php
                                              if(isset($_SESSION["login"])) {
                                                $userId = $_SESSION["login"]['id'];
                                                $checkLike = $wpdb->get_results( 
                                                  $wpdb->prepare("SELECT * FROM wp_answer_like WHERE user_id=%d AND ans_id=%d", $userId, $lastAns[0]->id));
                                                
                                                if($checkLike){
                                            ?>
                                                  <form method="post" action="">
                                                      <input type="hidden" name="ansid" value="<?php echo $lastAns[0]->id;?>">
                                                      <button type="submit" name="unlikeSubmit" class="unlikecolor"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Like</button>
                                                  </form>
                                            <?php
                                                 } else{
                                            ?>
                                                    <form method="post" action="">
                                                        <input type="hidden" name="ansid" value="<?php echo $lastAns[0]->id;?>">
                                                        <button type="submit" name="likeSubmit"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                                    </form>
                                            <?php
                                                 }
                                              } else {
                                            ?>
                                                <p data-toggle="modal" data-target="#myModallikeb<?php echo $z;?>"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</p>
                                            <?php
                                              }
                                            ?>
                                        </div>
                                                <!-- Modal -->
                                                  <div id="myModallikeb<?php echo $z;?>" class="modal fade" role="dialog">
                                                     <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                           <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                              <h4 class="modal-title">Like</h4>
                                                           </div>
                                                           <div class="modal-body">
                                                              <p> For Like this answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                           </div>
                                                        </div>
                                                     </div>
                                                  </div>
                                     </div>
                                     <div class="col-xs-3 col-md-4">
                                        <div class="share1">
                                           <?php
                                              if(isset($_SESSION["login"])) {
                                            ?>
                                              <p data-toggle="modal" data-target="#myModalbgrep<?php echo $z.$z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                            <?php
                                              } else {
                                            ?>
                                                <p data-toggle="modal" data-target="#myModalbgrep<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                            <?php
                                              }
                                            ?>
                                        </div>
                                            <!-- Modal -->
                                            <div id="myModalbgrep<?php echo $z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Reply</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <p> For reply on answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- Modal -->
                                            <div id="myModalbgrep<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Reply</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form method="post" action="">
                                                      <input type="hidden" name="rid" value="<?php echo $lastAns[0]->id;?>">
                                                      <?php
                                                       $my_content1 =" ";
                                                       $editor_id1 = 'replyPostb'.$z;
                                                       $option_name1 ='replyPost';
                                                       wp_editor($my_content1, $editor_id1, array('wpautop'=> false,'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                                                       ?>
                                                      <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                    </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                     </div>
                                     <div class="col-xs-4 col-md-4">
                                        <div class="share2">
                                           <?php
                                              if(isset($_SESSION["login"])) {
                                                $userId = $_SESSION["login"]["id"];
                                          ?>
                                                <!--<p data-toggle="modal" data-target="#myModalbgrepost1<?php //echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                          <?php
                                              } else{
                                          ?>
                                                <!--<p data-toggle="modal" data-target="#myModalbgrepost1<?php echo $z.$z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                          <?php
                                              }
                                          ?>
                                        </div>
                                              <!-- Modal -->
                                        <div id="myModalbgrepost1<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                           <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                 <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"> Repost </h4>
                                                 </div>
                                                 <div class="modal-body">
                                                    <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>

                                        <!-- Modal -->
                                        <div id="myModalbgrepost1<?php echo $z;?>" class="modal fade" role="dialog">
                                           <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                 <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"> Repost </h4>
                                                 </div>
                                                 <div class="modal-body">
                                                  <form method="post" action="">
                                                    <p> <textarea name="reposttext"> </textarea></p>
                                                    <div class="row repostmodal">
                                                        <?php
                                                            $userPica = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$ansPost->user_id));
                                                            
                                                            $userRega = $wpdb->get_results($wpdb->prepare("SELECT user_registered FROM wp_users WHERE ID=%d",$ansPost->user_id));

                                                            $userNamea = $wpdb->get_results($wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$ansPost->user_id));

                                                            $userHeadlinea = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$ansPost->user_id));
                                                         ?>
                                                        <div class="col-md-12">
                                                            <div class="text">
                                                               <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $ansPost->id;?>"><?php echo $ansPost->ques; ?></a></p>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-2 col-sm-2 col-xs-4">
                                                            <div class="imgforr">
                                                               <a href="#"><img src="<?php echo $userPica[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-10 col-sm-10 col-xs-8">
                                                            <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $ansPost->user_id;?>"><?php echo $userNamea[0]->name;?></a></strong>,<span><?php if($userHeadlinea[0]->headline){echo substr($userHeadlinea[0]->headline, 0,20 );} ?></span></p>
                                                            <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($userRega[0]->user_registered )); ?> </p>
                                                         </div>
                                                         <div class="col-md-12">
                                                            <div class="text2">
                                                               <p><?php echo $lastAns[0]->ans_content; ?></p>
                                                            </div>
                                                         </div>
                                                    </div>
                                                    
                                                    <input type="hidden" name="type" value="ans">
                                                    <input type="hidden" name="ansid" value="<?php echo $lastAns[0]->id;?>">
                                                    <input type="hidden" name="ans" value="<?php echo $lastAns[0]->ans_content;?>">
                                                    <p> <input type="submit" name="repostanssubmit" value="Repost"> </p>
                                                  </form>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>


                                     </div>
                                  </div>
   
                                <?php
                                  }
                                  $z++;
                                }
                              ?>
                           </div>
                        </div>

                        <div id="anwerspat2" class="tabcontent" style="display:none;">
                           <div class="blogaresnews">
                              <?php
                                 $top = date("Y-m-d", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );

                                 $ansPosts = $wpdb->get_results($wpdb->prepare("SELECT wp_discussion_ques.id, wp_discussion_ques.main_id, wp_discussion_ques.sub_cat_id, wp_discussion_ques.house_id, wp_discussion_ques.user_id, wp_discussion_ques.ques, wp_discussion_ques.status, wp_discussion_ques.created_at FROM wp_discussion_ques INNER JOIN wp_discussion_views ON wp_discussion_views.ques_id = wp_discussion_ques.id WHERE wp_discussion_ques.created_at >= %d  AND wp_discussion_ques.status='1' ORDER BY wp_discussion_views.view_count DESC LIMIT 40", $top));
                                 $z=1;
                                 foreach($ansPosts as $ansPost) {
                                   
                                    if($ansPost->sub_cat_id == 0) {
                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $ansPost->main_id));
                                        $cName1 = $catName[0]->name;
                                    } else{
                                        $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $ansPost->main_id));

                                        $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $ansPost->sub_cat_id));
                                        $cName1 = $catName[0]->sub_name;
                                    }
                                    $houseName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_house WHERE cat_id = %d AND subcat_id=%d", $ansPost->main_id, $ansPost->sub_cat_id));
                                    $viewsCount = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT * FROM wp_discussion_views WHERE ques_id = %d", $ansPost->id));

                                   $lastAns = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $ansPost->id));
                                 
                                   $ansTime1 = $wpdb->get_results($wpdb->prepare("SELECT created_at FROM wp_discussion_ans WHERE ques_id = %d ORDER BY id DESC LIMIT 1", $ansPost->id));

                                   if($ansTime) {
                                     $timeAnsGet = time_elapsed_string($ansTime[0]->created_at);
                                   } else {
                                     $timeAnsGet = " - ";
                                   }
                                 
                                   $userPic = $wpdb->get_results( 
                                     $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $lastAns[0]->user_id) 
                                   );
                                   $userName = $wpdb->get_results( 
                                     $wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d", $lastAns[0]->user_id));
                                   $userHeadline = $wpdb->get_results( 
                                     $wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $lastAns[0]->user_id));
                                    
                                    if($lastAns) {
                                      $countLike = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT COUNT(id) as cLike FROM wp_answer_like WHERE ans_id=%d", $lastAns[0]->id));

                                      $countReply = $wpdb->get_results( 
                                       $wpdb->prepare("SELECT COUNT(id) as cReply FROM wp_discussion_reply WHERE ans_id=%d",$lastAns[0]->id));

                                      $repostCount = $wpdb->get_results( 
                                        $wpdb->prepare("SELECT COUNT(id) as countRepost FROM wp_wallpost WHERE shareType='ans' AND shareid=%d", $lastAns[0]->id));
                                    }
                                   if($lastAns) {

                                ?>
                                  <div class="col-md-12 blo">
                                     <div class="direct">
                                        <div class="col-md-10 col-sm-10 col-xs-10">
                                           <p> <?php if($ansPost->sub_cat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$ansPost->main_id."'>".$catMainName[0]->name .'</a> . ';}?>  <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $ansPost->main_id;?> <?php if($ansPost->sub_cat_id != 0) { echo "&actions=". $ansPost->sub_cat_id;}?>"><?php echo $cName1; ?></a> . <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $ansPost->house_id;?>"><?php if(strlen($houseName[0]->title) > 30){echo substr($houseName[0]->title, 0,30). ' ...';} else{echo $houseName[0]->title;} ?></a> </p>
                                        </div>
                                     </div>
                                     <div class="col-md-2 col-sm-2 col-xs-2">
                                        <div class="dot">
                                           <div class="dropdown">
                                              <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...
                                              </p>
                                              <ul class="dropdown-menu">
                                              <?php
                                                if($_SESSION['login']['id']) {
                                              ?>
                                                 <li data-toggle="modal" data-target="#myModalansreport111<?php echo $z; ?>">Report This Post</li>
                                              <?php
                                                 } else{
                                              ?>
                                                    <li data-toggle="modal" data-target="#myModalreportnot111<?php echo $z; ?>">Report This Post</li>
                                              <?php
                                                 }
                                              ?>
                                              </ul>
                                           </div>

                                                <div id="myModalreportnot111<?php echo $z; ?>" class="modal fade" role="dialog">
                                                  <div class="modal-dialog">
                                                     <!-- Modal content-->
                                                     <div class="modal-content">
                                                        <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                           <h1>Report this post</h1>
                                                        </div>
                                                        <div class="modal-body">
                                                           <p> Please Login and Register first! </p>
                                                        </div>
                                                     </div>
                                                  </div>
                                              </div>

                                                <div class="modal fade" id="myModalansreport111<?php echo $z; ?>" role="dialog">
                                                 <div class="modal-dialog vertical-align-center">
                                                    <div class="modal-content">
                                                       <div class="repocontent modal-body reportingsets" id="gridone<?php echo $z.$z.$z.$z.$z.$z; ?>" style="display:block;">
                                                          <ul>
                                                             <li>
                                                                <span  class="countno repolinks" onclick="openReport(event, 'gridtwo<?php echo $z.$z.$z.$z.$z.$z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it's spam, Promotional</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridthree<?php echo $z.$z.$z.$z.$z.$z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think  it's objectionable</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                             <li>
                                                                <span class="countno repolinks" onclick="openReport(event, 'gridfour<?php echo $z.$z.$z.$z.$z.$z; ?>')">
                                                                   <div class="funkyradio-default">
                                                                      <label for="checkbox1">I think it false or misinformation</label>
                                                                   </div>
                                                                </span>
                                                             </li>
                                                          </ul>
                                                          <div class="modal-footer">
                                                          </div>
                                                       </div>

                                                       <div class="repocontent modal-body reportingsets" id="gridtwo<?php echo $z.$z.$z.$z.$z.$z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                             <input type="hidden" name="aid" value="<?php echo $lastAns[0]->id;?>">
                                                          </div>
                                                          <div class="modal-footer">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z.$z.$z.$z; ?>')">Back</button>
                                                             <button type="submit" name="submitansReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       
                                                       <div class="repocontent modal-body reportingsets" id="gridthree<?php echo $z.$z.$z.$z.$z.$z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent" checked="checked">It's pornographic or extremely violent.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                             </div>
                                                             <div class="funkyradio-default">
                                                                <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                             </div>
                                                          </div>
                                                          <div class="bottom-btns">
                                                            <input type="hidden" name="aid" value="<?php echo $lastAns[0]->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z.$z.$z.$z; ?>')">Back</button>
                                                             <button type="submit" name="submitansReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                       <div class="repocontent modal-body reportingsets" id="gridfour<?php echo $z.$z.$z.$z.$z.$z; ?>" style="display:none;">
                                                          <form method="post" action="">
                                                          <div class="modal-body">
                                                             <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                          </div>
                                                          <div class="bottom-btns">
                                                             <input type="hidden" name="aid" value="<?php echo $lastAns[0]->id;?>">
                                                             <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone<?php echo $z.$z.$z.$z.$z.$z; ?>')">Back</button>
                                                             <button type="submit" name="submitansReport" class="btn btn-default">Submit</button>
                                                          </div>
                                                          </form>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>

                                        </div>
                                     </div>
                                     <div class="col-md-12">
                                        <div class="text">
                                           <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $ansPost->id;?>"><?php echo $ansPost->ques; ?></a></p>
                                        </div>
                                     </div>
                                     <div class="imgforr">
                                        <div class="col-md-2 col-sm-2 col-xs-3">
                                           <a href="#"><img src="<?php echo $userPic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                        </div>
                                     </div>
                                     <div class="col-md-10 col-sm-10 col-xs-9">
                                        <p class="catp"><strong><a href="#"><?php echo $userName[0]->name;?></a></strong>,<span><?php echo $userHeadline[0]->headline;?></span></p>
                                        <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($lastAns[0]->created_at )); ?> </p>
                                     </div>
                                     <div class="col-md-12">
                                        <div class="text2">
                                           <span><?php if(strlen($lastAns[0]->ans_content) > 180){echo substr($lastAns[0]->ans_content, 0,180). '';} else{echo $lastAns[0]->ans_content;} ?></span>
                                          <?php
                                             if(strlen($lastAns[0]->ans_content) > 180){
                                          ?>
                                           <span class="anscomplete<?php echo $z.$z;?>" style="display:none;"><?php echo substr($lastAns[0]->ans_content, 180,300); ?></span>
                                           
                                           <span class="ansmore<?php echo $z.$z;?> ansmorecolor" id="<?php echo $z.$z;?>" onClick="moreFunction(this.id)">more...</span>
                                          <?php
                                             }
                                          ?>
                                        </div>
                                     </div>
                                     <div class="col-md-12">
                                        <div class="pp">
                                           <p><span><?php if($countLike){echo $countLike[0]->cLike;}else{echo "0";} ?></span><strong>Likes</strong><div class="dotsseprate">.</div></p>
                                           <p data-toggle="modal" data-target="#myModalreplylistsc<?php echo $z;?>"><span><?php if($countReply){echo $countReply[0]->cReply;}else{echo "0";} ?></span><strong>Reply</strong><div class="dotsseprate">.</div></p>
                                           <!--<p data-toggle="modal" data-target="#myModalrepostlistf<?php //echo $z;?>"><span><?php //if($repostCount){echo $repostCount[0]->countRepost;}else{echo "0";}?></span><strong>Repost</strong></p>-->
                                        </div>

                                            <!---- Modal  --->
                                          <div id="myModalreplylistsc<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h3> Reply </h3>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                      <div clss="col-md-12">
                                                      <?php
                                                        $replies = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_reply WHERE ans_id=%d", $lastAns[0]->id));
                                                        
                                                        if($replies) {
                                                          foreach($replies as $replie) {
                                                            $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $replie->user_id));
                                                          
                                                            $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $replie->user_id));
                                                          
                                                            $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $replie->user_id));

                                                      ?>   
                                                         <div class="col-md-12">
                                                            <div class="poll">
                                                               <div class="left">
                                                                  <div class="imgforr">
                                                                     <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                  </div>
                                                               </div>
                                                               <div class="right">
                                                                  <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallReply->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong></p>
                                                               </div>
                                                               <div class="custext">
                                                                  <div class="text2">
                                                                     <p> <?php echo $replie->reply; ?></p>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      <?php
                                                              }
                                                          }
                                                      ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>

                                          <!---- Modal  --->
                                          <div id="myModalrepostlistf<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog" style="width: 700px;">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h3></h3>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                      <div clss="col-md-12">
                                                      <?php
                                                          $reLists = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_wallpost WHERE shareType='ans' AND shareid=%d", $lastAns[0]->id));
                                                          if($reLists) {
                                                              foreach($reLists as $reList) {
                                                                  $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $reList->user_id));
                                                          
                                                                  $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $reList->user_id));
                                                          
                                                                  $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $reList->user_id));

                                                      ?>   
                                                         <div class="col-md-6">
                                                          <div class="poll">
                                                             <div class="col-md-3 col-sm-2 col-xs-3">
                                                                <div class="imgforr">
                                                                   <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                </div>
                                                             </div>
                                                             <div class="col-md-9 col-sm-10 col-xs-9">
                                                                <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                <p class="rigwd"> <?php echo $userDate;?> </p>
                                                             </div>
                                                          </div>
                                                       </div>
                                                      <?php
                                                              }
                                                          }
                                                      ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>

                                     </div>
                                     <div class="col-xs-3 col-md-4">
                                        <div class="share like">
                                           <?php
                                              if(isset($_SESSION["login"])) {
                                                $userId = $_SESSION["login"]['id'];
                                                $checkLike = $wpdb->get_results( 
                                                  $wpdb->prepare("SELECT * FROM wp_answer_like WHERE user_id=%d AND ans_id=%d", $userId, $lastAns[0]->id));
                                                
                                                if($checkLike){
                                            ?>
                                                  <form method="post" action="">
                                                      <input type="hidden" name="ansid" value="<?php echo $lastAns[0]->id;?>">
                                                      <button type="submit" name="unlikeSubmit" class="unlikecolor"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Like</button>
                                                  </form>
                                            <?php
                                                 } else{
                                            ?>
                                                    <form method="post" action="">
                                                        <input type="hidden" name="ansid" value="<?php echo $lastAns[0]->id;?>">
                                                        <button type="submit" name="likeSubmit"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Like</button>
                                                    </form>
                                            <?php
                                                 }
                                              } else {
                                            ?>
                                                <p data-toggle="modal" data-target="#myModallikec<?php echo $z;?>"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Like</p>
                                            <?php
                                              }
                                            ?>
                                        </div>
                                                <!-- Modal -->
                                                  <div id="myModallikec<?php echo $z;?>" class="modal fade" role="dialog">
                                                     <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                           <div class="modal-header">
                                                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                              <h4 class="modal-title">Like</h4>
                                                           </div>
                                                           <div class="modal-body">
                                                              <p> For Like this answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                           </div>
                                                        </div>
                                                     </div>
                                                  </div>
                                     </div>
                                     <div class="col-xs-3 col-md-4">
                                        <div class="share1">
                                           <?php
                                              if(isset($_SESSION["login"])) {
                                            ?>
                                              <p data-toggle="modal" data-target="#myModalcdrep<?php echo $z.$z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                            <?php
                                              } else {
                                            ?>
                                                <p data-toggle="modal" data-target="#myModalcdrep<?php echo $z;?>"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                            <?php
                                              }
                                            ?>
                                        </div>
                                            <!-- Modal -->
                                            <div id="myModalcdrep<?php echo $z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Reply</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <p> For reply on answer. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <!-- Modal -->
                                            <div id="myModalcdrep<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                              <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Reply</h4>
                                                  </div>
                                                  <div class="modal-body">
                                                    <form method="post" action="">
                                                      <input type="hidden" name="rid" value="<?php echo $lastAns[0]->id;?>">
                                                      <?php
                                                       $my_content1 =" ";
                                                       $editor_id1 = 'replyPostc'.$z;
                                                       $option_name1 ='replyPost';
                                                       wp_editor($my_content1, $editor_id1, array('wpautop'=> false,'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                                                       ?>
                                                      <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                    </form>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                     </div>
                                     <div class="col-xs-4 col-md-4">
                                        <div class="share2">
                                           <?php
                                              if(isset($_SESSION["login"])) {
                                                $userId = $_SESSION["login"]["id"];
                                          ?>
                                                <!--<p data-toggle="modal" data-target="#myModalcdrepost1<?php //echo $z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                          <?php
                                              } else{
                                          ?>
                                                <!--<p data-toggle="modal" data-target="#myModalcdrepost1<?php //echo $z.$z;?>"><i class="fa fa-mail-forward"></i>Repost</p>-->
                                          <?php
                                              }
                                          ?>
                                        </div>
                                              <!-- Modal -->
                                        <div id="myModalcdrepost1<?php echo $z.$z;?>" class="modal fade" role="dialog">
                                           <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                 <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"> Repost </h4>
                                                 </div>
                                                 <div class="modal-body">
                                                    <p> For Repost this question. Please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>

                                        <!-- Modal -->
                                        <div id="myModalcdrepost1<?php echo $z;?>" class="modal fade" role="dialog">
                                           <div class="modal-dialog">
                                              <!-- Modal content-->
                                              <div class="modal-content">
                                                 <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"> Repost </h4>
                                                 </div>
                                                 <div class="modal-body">
                                                  <form method="post" action="">
                                                    <p> <textarea name="reposttext"> </textarea></p>
                                                    <div class="row repostmodal">
                                                        <?php
                                                            $userPica = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d",$ansPost->user_id));
                                                            
                                                            $userRega = $wpdb->get_results($wpdb->prepare("SELECT user_registered FROM wp_users WHERE ID=%d",$ansPost->user_id));

                                                            $userNamea = $wpdb->get_results($wpdb->prepare("SELECT name FROM wp_profileStepTwo WHERE user_id=%d",$ansPost->user_id));

                                                            $userHeadlinea = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d",$ansPost->user_id));
                                                         ?>
                                                        <div class="col-md-12">
                                                            <div class="text">
                                                               <p><a href="http://edukeeda.com/questionanswers?action=<?php echo $ansPost->id;?>"><?php echo $ansPost->ques; ?></a></p>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-2 col-sm-2 col-xs-4">
                                                            <div class="imgforr">
                                                               <a href="#"><img src="<?php echo $userPica[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                            </div>
                                                         </div>
                                                         <div class="col-md-10 col-sm-10 col-xs-8">
                                                            <p class="catp"><strong><a href="<?php echo site_url();?>/profile?action=<?php echo $ansPost->user_id;?>"><?php echo $userNamea[0]->name;?></a></strong>,<span><?php if($userHeadline[0]->headline){echo substr($userHeadline[0]->headline, 0,20 );}?></span></p>
                                                            <p class="rigwd"> <?php echo date("F d, Y. g:i a ", strtotime($userReg[0]->user_registered )); ?> </p>
                                                         </div>
                                                         <div class="col-md-12">
                                                            <div class="text2">
                                                               <p><?php echo $lastAns[0]->ans_content; ?></p>
                                                            </div>
                                                         </div>
                                                    </div>
                                                    
                                                    <input type="hidden" name="type" value="ans">
                                                    <input type="hidden" name="ansid" value="<?php echo $lastAns[0]->id;?>">
                                                    <input type="hidden" name="ans" value="<?php echo $lastAns[0]->ans_content;?>">
                                                    <p> <input type="submit" name="repostanssubmit" value="Repost"> </p>
                                                  </form>
                                                 </div>
                                              </div>
                                           </div>
                                        </div>
                                     </div>
                                  </div>

                                <?php
                                  }
                                  $z++;
                                }
                              ?>
                           </div>
                        </div>


                        <div id="dischouses" class="tabcontent" style="display:none;">
                           <?php
                              $housePosts = $wpdb->get_results("SELECT * FROM wp_discussion_house ORDER BY id DESC");
                              
                              foreach($housePosts as $housePost) {
                                
                                if($housePost->subcat_id == 0) {
                                    $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $housePost->cat_id));
                                    $cName1 = $catName[0]->name;
                                } else{
                                    $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $housePost->cat_id));

                                    $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $housePost->subcat_id));
                                    $cName1 = $catName[0]->sub_name;
                                }

                                $quesPostCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as quesCount FROM wp_discussion_ques WHERE house_id=%d ORDER BY id DESC", $housePost->id));

                                $joinHouseCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as hCount FROM wp_discussion_join WHERE joinId=%d ORDER BY id DESC", $housePost->id));
                              
                                // $ansTime = $wpdb->get_results( 
                                //   $wpdb->prepare("SELECT created_at FROM wp_discussion_ques WHERE house_id = %d ORDER BY id DESC LIMIT 1", $housePost->id));
                                 $timeAnsGet = time_elapsed_string($housePost->updated_at);
                                // if($ansTime) {
                                //   $timeAnsGet = time_elapsed_string($ansTime[0]->created_at);
                                // } else {
                                //   $timeAnsGet = " - ";
                                // }
                                $table_name = "wp_discuss_house_views";
                                $getViews = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM $table_name WHERE house_id = %d", $housePost->id)
                                );
                              
                              ?>
                           <div class="rap firstpostdefult" >
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">

                                    <p> <?php if($housePost->subcat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$housePost->cat_id."'>".$catMainName[0]->name .'</a> . ';}?> <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $housePost->cat_id;?><?php if($housePost2->subcat_id == 0){}else { echo "&actions=". $housePost2->subcat_id;}?>"><?php echo $cName1; ?></a> </p>
                                 </div>
                              </div>

                              <div class="col-md-9 col-xs-9">
                                 <div class="text1">
                                    <p><a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost->id;?>"><?php echo $housePost->title;?></a></p>
                                 </div>
                              </div>
                              <div class="col-md-3 col-xs-3">
                                <div class="cutjion">
                                  <?php
                                    if(isset($_SESSION["login"])) {
                                      $userId = $_SESSION["login"]["id"];
                                      $checkJoin = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_discussion_join WHERE user_id=%d AND joinid=%d", $userId, $housePost->id));
                                      
                                      if($checkJoin) {
                                  ?>
                                        <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost->id;?>">Joined</a>
                                  <?php
                                      } else{
                                  ?>
                                        <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost->id;?>">Join House</a>
                                  <?php
                                      }
                                    } else{
                                  ?>
                                        <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost->id;?>">Join House</a>
                                  <?php
                                    }
                                  ?>
                                </div>
                              </div>
                              <div class="join">
                                 <div class="col-xs-9 col-md-9">
                                    <div class="like">
                                       <p><span><?php echo $quesPostCount[0]->quesCount; ?></span><strong> Posts </strong></p>
                                       <div class="dotsseprate">.</div>
                                       <!--<p data-toggle="modal" data-target="#myModalhousepeoplelist1<?php //echo $z;?>"><span> <?php //echo $joinHouseCount[0]->hCount; ?> </span><strong> People </strong></p>
                                       <div class="dotsseprate">.</div>
                                       <p><span><?php //if($getViews){ echo $getViews[0]->view_count; }else{ echo "0";} ?></span><strong> Views </strong></p>
                                       <div class="dotsseprate">.</div>-->
                                       <p><span><?php echo $timeAnsGet;?></p>
                                    </div>

                                          <!---- Modal  --->
                                          <div id="myModalhousepeoplelist1<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog" style="width: 700px;">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h3></h3>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                      <div clss="col-md-12">
                                                      <?php
                                                          $opJoins = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_join WHERE joinId=%d", $housePost->id));
                                                          
                                                          if($opJoins) {
                                                              foreach($opJoins as $opJoin) {
                                                                  $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $opJoin->user_id));
                                                          
                                                                  $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $opJoin->user_id));
                                                          
                                                                  $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $opJoin->user_id));

                                                      ?>   
                                                         <div class="col-md-6">
                                                          <div class="poll">
                                                             <div class="col-md-3 col-sm-2 col-xs-3">
                                                                <div class="imgforr">
                                                                   <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                </div>
                                                             </div>
                                                             <div class="col-md-9 col-sm-10 col-xs-9">
                                                                <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                <p class="rigwd"> <?php echo $userDate;?> </p>
                                                             </div>
                                                          </div>
                                                       </div>
                                                      <?php
                                                              }
                                                          }
                                                      ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>

                                 </div>
                                 <div class="col-md-3 col-xs-3">
                                    <div class="pluging">
                                       <div class="dropdown customs">      
                                          <button class="dropbtn">
                                            <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                                          </button>
                                          <div class="dropdown-content">
                                            <?php //$urlshare = "http://edukeeda.com/discussion-house/?action=".$housePost->id;
                                              //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                            ?>
                                              <?php //echo do_shortcode("$urlsharing"); ?>
                                          </div>
                                        </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php
                              }
                              ?>
                        </div>

                        <div id="dischouses1" class="tabcontent" style="display:none;">
                           <?php
                              $housePosts1 = $wpdb->get_results("SELECT wp_discussion_house.id,wp_discussion_house.cat_id,wp_discussion_house.subcat_id, wp_discussion_house.title, wp_discussion_house.house_desc,wp_discussion_house.created_at,wp_discussion_house.updated_at FROM wp_discussion_house INNER JOIN wp_discuss_house_views ON wp_discuss_house_views.house_id = wp_discussion_house.id ORDER BY wp_discuss_house_views.view_count DESC");

                              foreach($housePosts1 as $housePost1) {
                                
                                if($housePost1->subcat_id == 0) {
                                    $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $housePost1->cat_id));
                                    $cName1 = $catName[0]->name;
                                } else{
                                    $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $housePost1->cat_id));

                                    $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $housePost1->subcat_id));
                                    $cName1 = $catName[0]->sub_name;
                                }

                                $quesPostCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as quesCount FROM wp_discussion_ques WHERE house_id=%d ORDER BY id DESC", $housePost1->id));

                                $joinHouseCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as hCount FROM wp_discussion_join WHERE joinId=%d", $housePost1->id));
                              
                                // $ansTime = $wpdb->get_results( 
                                //   $wpdb->prepare("SELECT created_at FROM wp_discussion_ques WHERE house_id = %d ORDER BY id DESC LIMIT 1", $housePost1->id));
                                 $timeAnsGet = time_elapsed_string($housePost1->updated_at);
                                // if($ansTime) {
                                //   $timeAnsGet = time_elapsed_string($housePost1->created_at);
                                // } else {
                                //   $timeAnsGet = " - ";
                                // }
                                $table_name = "wp_discuss_house_views";
                                $getViews = $wpdb->get_results($wpdb->prepare( "SELECT * FROM $table_name WHERE house_id = %d", $housePost1->id));
                              
                              ?>
                           <div class="rap firstpostdefult" >
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">
                                    <p> <?php if($housePost1->subcat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$housePost1->cat_id."'>".$catMainName[0]->name .'</a> . ';}?> <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $housePost1->cat_id;?><?php if($housePost1->subcat_id == 0){}else { echo "&actions=". $housePost1->subcat_id;}?>"><?php echo $cName1; ?></a> </p>
                                 </div>
                              </div>
 
                              <div class="col-md-9 col-xs-9">
                                 <div class="text1">
                                    <p><a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost1->id;?>"><?php echo $housePost1->title;?></a></p>
                                 </div>
                              </div>
                              <div class="col-md-3 col-xs-3">
                                <div class="cutjion">
                                  <?php
                                    if(isset($_SESSION["login"])) {
                                      $userId = $_SESSION["login"]["id"];
                                      $checkJoin = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_discussion_join WHERE user_id=%d AND joinid=%d", $userId, $housePost1->id));
                                      
                                      if($checkJoin) {
                                  ?>
                                        <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost1->id;?>">Joined</a>
                                  <?php
                                      } else{
                                  ?>
                                        <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost1->id;?>">Join House</a>
                                  <?php
                                      }
                                    } else{
                                  ?>
                                        <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost1->id;?>">Join House</a>
                                  <?php
                                    }
                                  ?>
                                </div>
                              </div>
                              <div class="join">
                                 <div class="col-xs-9 col-md-9">
                                    <div class="like">
                                       <p><span><?php echo $quesPostCount[0]->quesCount; ?></span><strong> Posts </strong></p>
                                       <div class="dotsseprate">.</div>
                                       <!--<p data-toggle="modal" data-target="#myModalhousepeoplelist2<?php //echo $z;?>"><span> <?php //echo $joinHouseCount[0]->hCount; ?> </span><strong> People </strong></p>
                                       <div class="dotsseprate">.</div>
                                       <p><span><?php //if($getViews){ echo $getViews[0]->view_count; }else{ echo "0";} ?></span><strong> Views </strong></p>
                                       <div class="dotsseprate">.</div>-->
                                       <p><span><?php echo $timeAnsGet;?></p>
                                    </div>

                                          <!---- Modal  --->
                                          <div id="myModalhousepeoplelist2<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog" style="width: 700px;">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h3></h3>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                      <div clss="col-md-12">
                                                      <?php
                                                          $opJoins3 = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_join WHERE joinId=%d", $housePost1->id));
                                                          if($opJoins3) {
                                                              foreach($opJoins3 as $opJoin3) {
                                                                  $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $opJoin3->user_id));
                                                          
                                                                  $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $opJoin3->user_id));
                                                          
                                                                  $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $opJoin3->user_id));

                                                      ?>   
                                                         <div class="col-md-6">
                                                          <div class="poll">
                                                             <div class="col-md-3 col-sm-2 col-xs-3">
                                                                <div class="imgforr">
                                                                   <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                </div>
                                                             </div>
                                                             <div class="col-md-9 col-sm-10 col-xs-9">
                                                                <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>

                                                                <p class="rigwd"> <?php echo $userDate;?> </p>
                                                             </div>
                                                          </div>
                                                       </div>
                                                      <?php
                                                              }
                                                          }
                                                      ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>

                                 </div>
                                 <div class="col-md-3 col-xs-3">
                                    <div class="pluging">
                                       <div class="dropdown customs">      
                                          <button class="dropbtn">
                                            <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                                          </button>
                                          <div class="dropdown-content">
                                            <?php //$urlshare = "http://edukeeda.com/discussion-house/?action=".$housePost1->id;
                                              //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                            ?>
                                              <?php //echo do_shortcode("$urlsharing"); ?>
                                          </div>
                                        </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php
                              }
                              ?>
                        </div>

                        <div id="dischouses2" class="tabcontent" style="display:none;">
                           <?php
                              $top = date("Y-m-d", strtotime(date("Y-m-d", strtotime( date("Y-m-d"))) . "-1 month" ));
                              
                              $housePosts2 = $wpdb->get_results($wpdb->prepare("SELECT wp_discussion_house.id,wp_discussion_house.cat_id,wp_discussion_house.subcat_id, wp_discussion_house.title, wp_discussion_house.house_desc,wp_discussion_house.created_at,wp_discussion_house.updated_at  FROM wp_discussion_house INNER JOIN wp_discuss_house_views ON wp_discuss_house_views.house_id = wp_discussion_house.id WHERE  wp_discussion_house.created_at >=%d ORDER BY wp_discuss_house_views.view_count DESC", $top));
                              
                              foreach($housePosts2 as $housePost2) {
                                
                                if($housePost2->subcat_id == 0) {
                                    $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $housePost2->cat_id));
                                    $cName1 = $catName[0]->name;
                                } else{
                                    $catMainName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_main WHERE id = %d", $housePost2->cat_id));

                                    $catName = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_sub WHERE id = %d", $housePost2->subcat_id));
                                    $cName1 = $catName[0]->sub_name;
                                }

                                $quesPostCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as quesCount FROM wp_discussion_ques WHERE house_id=%d ORDER BY id DESC", $housePost2->id));

                                $joinHouseCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(id) as hCount FROM wp_discussion_join WHERE joinId=%d", $housePost2->id));
                              
                                //$ansTime = $wpdb->get_results( 
                                  //$wpdb->prepare("SELECT created_at FROM wp_discussion_ques WHERE house_id = %d ORDER BY id DESC LIMIT 1", $housePost2->id));
                                 $timeAnsGet = time_elapsed_string($housePost2->updated_at);
                                // if($ansTime) {
                                //   $timeAnsGet = time_elapsed_string($housePost2->created_at);
                                // } else {
                                //   $timeAnsGet = " - ";
                                // }
                                $table_name = "wp_discuss_house_views";
                                $getViews = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM $table_name WHERE house_id = %d", $housePost2->id));
                              
                              ?>
                           <div class="rap firstpostdefult" >
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">
                                    <p> <?php if($housePost2->subcat_id != 0) { echo "<a href='".site_url()."/discussioncategory?action=".$housePost2->cat_id."'>".$catMainName[0]->name .'</a> . ';}?> 

                                    <a href="<?php echo site_url();?>/discussioncategory/?action=<?php echo $housePost2->cat_id;?>&<?php if($housePost2->subcat_id == 0){}else { echo "&actions=". $housePost2->subcat_id;}?>"><?php echo $cName1; ?></a> </p>
                                 </div>
                              </div>

                              <div class="col-md-9 col-xs-9">
                                 <div class="text1">
                                    <p><a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost2->id;?>"><?php echo $housePost2->title;?></a></p>
                                 </div>
                              </div>
                              <div class="col-md-3 col-xs-3">
                                <div class="cutjion">
                                  <?php
                                    if(isset($_SESSION["login"])) {
                                      $userId = $_SESSION["login"]["id"];
                                      $checkJoin = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_discussion_join WHERE user_id=%d AND joinId=%d", $userId, $housePost2->id));
                                      
                                      if($checkJoin) {
                                  ?>
                                        <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost2->id;?>">Joined</a>
                                  <?php
                                      } else{
                                  ?>
                                        <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost2->id;?>">Join House</a>
                                  <?php
                                      }
                                    } else{
                                  ?>
                                        <a href="<?php echo site_url();?>/discussion-house?action=<?php echo $housePost2->id;?>">Join House</a>
                                  <?php
                                    }
                                  ?>
                                </div>
                              </div>
                              <div class="join">
                                 <div class="col-xs-9 col-md-9">
                                    <div class="like">
                                       <p><span><?php echo $quesPostCount[0]->quesCount; ?></span><strong> Posts </strong></p>
                                       <div class="dotsseprate">.</div>
                                       <!--<p data-toggle="modal" data-target="#myModalhousepeoplelist3<?php //echo $z;?>"><span> <?php //echo $joinHouseCount[0]->hCount; ?> </span><strong> People </strong></p>
                                       <div class="dotsseprate">.</div>
                                       <p><span><?php //if($getViews){ echo $getViews[0]->view_count; }else{ echo "0";} ?></span><strong> Views </strong></p>
                                       <div class="dotsseprate">.</div>-->
                                       <p><span><?php echo $timeAnsGet;?></p>
                                    </div>

                                          <!---- Modal  --->
                                          <div id="myModalhousepeoplelist3<?php echo $z;?>" class="modal fade" role="dialog">
                                          <div class="modal-dialog" style="width: 700px;">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                   <h3></h3>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                      <div clss="col-md-12">
                                                      <?php
                                                          $opJoins3 = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_discussion_join WHERE joinId=%d", $housePost2->id));
                                                          var_dump($housePost2->id);
                                                          if($opJoins3) {
                                                              foreach($opJoins3 as $opJoin3) {
                                                                  $ansAuthor = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id=%d", $opJoin3->user_id));
                                                          
                                                                  $authorHeadline = $wpdb->get_results($wpdb->prepare("SELECT headline FROM wp_profileStepOne WHERE user_id=%d", $opJoin3->user_id));
                                                          
                                                                  $ansAuthorPic = $wpdb->get_results($wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id=%d", $opJoin3->user_id));

                                                      ?>   
                                                         <div class="col-md-6">
                                                          <div class="poll">
                                                             <div class="col-md-3 col-sm-2 col-xs-3">
                                                                <div class="imgforr">
                                                                   <a href="#"><img src="<?php echo $ansAuthorPic[0]->profilePic; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                </div>
                                                             </div>
                                                             <div class="col-md-9 col-sm-10 col-xs-9">
                                                                <p class="catp"><strong><a href="http://edukeeda.com/profile?action=<?php echo $wallRe->user_id;?>"><?php echo $ansAuthor[0]->name; ?></a></strong> <?php if($authorHeadline[0]->headline) {echo ', '. substr($authorHeadline[0]->headline,0,15).'..';}?></p>
                                                             </div>
                                                          </div>
                                                       </div>
                                                      <?php
                                                              }
                                                          }
                                                      ?>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>

                                 </div>
                                 <div class="col-md-3 col-xs-3">
                                    <div class="pluging">
                                       <div class="dropdown customs">      
                                          <button class="dropbtn">
                                            <!--<i class="fa fa-share-alt" aria-hidden="true" style="color: #c0c0c0;"></i>-->
                                          </button>
                                          <div class="dropdown-content">
                                            <?php //$urlshare = "http://edukeeda.com/discussion-house/?action=".$housePost->id;
                                              //$urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                                            ?>
                                              <?php //echo do_shortcode("$urlsharing"); ?>
                                          </div>
                                        </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php
                              }
                              ?>
                        </div>

                     </div>
                  </div>
                  <!-- #main -->
               </div>
            </div>
         </div>
         <div class="col-md-3">
            <div class="advtspace" style=""></div>
         </div>
         <!-- #primary -->
      </div>
   </div>
   <?php get_footer(); ?>
   <script>
      var acc = document.getElementsByClassName("accordion");
      var i;
      
      for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
          this.classList.toggle("active");
          var panel = this.nextElementSibling;
          if (panel.style.display === "block") {
            panel.style.display = "none";
          } else {
            panel.style.display = "block";
          }
        });
      }
   </script>
</div>
<div class="modal fade" id="myModal1" role="dialog">
   <div class="modal-dialog vertical-align-center">
      <div class="modal-content">
         <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
            <ul>
               <li>
                  <span  class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                     <div class="funkyradio-default">
                        <label for="checkbox1">I think it's spam, Promotional</label>
                     </div>
                  </span>
               </li>
               <li>
                  <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                     <div class="funkyradio-default">
                        <label for="checkbox1">I think  it's objectionable</label>
                     </div>
                  </span>
               </li>
               <li>
                  <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                     <div class="funkyradio-default">
                        <label for="checkbox1">I think it false or misinformation</label>
                     </div>
                  </span>
               </li>
            </ul>
            <div class="modal-footer">
            </div>
         </div>
         <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
            <div class="modal-body">
               <p>You Are Reorting that this is spam, Promotional</p>
            </div>
            <div class="modal-footer">
               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
               <button type="button" class="btn btn-default">Submit</button>
            </div>
         </div>
         <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
            <div class="modal-body">
               <div class="funkyradio-default">
                  <label class="checkbox-inline"><input type="radio" value="">It's pornographic or extremely violent.</label>
               </div>
               <div class="funkyradio-default">
                  <label class="checkbox-inline"><input type="radio" value="Education Level">It's hate speech.</label>
               </div>
               <div class="funkyradio-default">
                  <label class="checkbox-inline"><input type="radio" value="Education Level">Topic or language is offensive/Abusive.</label>
               </div>
            </div>
            <div class="bottom-btns">
               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
               <button type="button" class="btn btn-default">Submit</button>
            </div>
         </div>
         <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
            <div class="modal-body">
               <p>You are reporting that it is a salse information or misinformation.</p>
            </div>
            <div class="bottom-btns">
               <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
               <button type="button" class="btn btn-default">Submit</button>
            </div>
         </div>
      </div>
   </div>
</div>
<?php
   function time_elapsed_string($datetime, $full = false) {
       $now = new DateTime;
       $ago = new DateTime($datetime);
       $diff = $now->diff($ago);
   
       $diff->w = floor($diff->d / 7);
       $diff->d -= $diff->w * 7;
   
       $string = array(
           'y' => 'year',
           'm' => 'month',
           'w' => 'week',
           'd' => 'day',
           'h' => 'hour',
           'i' => 'minute',
           's' => 'second',
       );
       foreach ($string as $k => &$v) {
           if ($diff->$k) {
               $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
           } else {
               unset($string[$k]);
           }
       }
   
       if (!$full) $string = array_slice($string, 0, 1);
       return $string ? implode(', ', $string) . ' ago' : 'just now';
      $etime = time() - strtotime($datetime);
   }
   //  if ($etime < 1)
   //  {
   //      return '0 seconds';
   //  }

   //  $a = array( 365 * 24 * 60 * 60  =>  'year',
   //               30 * 24 * 60 * 60  =>  'month',
   //                    24 * 60 * 60  =>  'day',
   //                         60 * 60  =>  'hour',
   //                              60  =>  'minute',
   //                               1  =>  'second'
   //              );
   //  $a_plural = array( 'year'   => 'years',
   //                     'month'  => 'months',
   //                     'day'    => 'days',
   //                     'hour'   => 'hours',
   //                     'minute' => 'minutes',
   //                     'second' => 'seconds'
   //              );

   //  foreach ($a as $secs => $str)
   //  {
   //      $d = $etime / $secs;
   //      if ($d >= 1)
   //      {
   //          $r = round($d);
   //          return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
   //      }
   //  }
   // }
   ?>
<script> 
   function openCity(evt, cityName) {
       var i, tabcontent, tablinks;
       tabcontent = document.getElementsByClassName("tabcontent");
       for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
       }
       tablinks = document.getElementsByClassName("tablinks");
       for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace(" active", "");
       }
       document.getElementById(cityName).style.display = "block";
       evt.currentTarget.className += " active";

       if(cityName == "articleblous") {
          document.getElementById("filtertabans").style.display = "none";
          document.getElementById("filtertabques").style.display = "block";
          document.getElementById("filtertabhouse").style.display = "none";
          $("#filtertabques .tabactive2").addClass("active");
          $("#filtertabques .tabactive1").removeClass("active");
          $("#filtertabques .tabactive3").removeClass("active");
       } else if(cityName == "anwerspat2") {
          document.getElementById("filtertabans").style.display = "block";
          document.getElementById("filtertabques").style.display = "none";
          document.getElementById("filtertabhouse").style.display = "none";
          $("#filtertabans .tabactive2").addClass("active");
          $("#filtertabans .tabactive1").removeClass("active");
          $("#filtertabans .tabactive3").removeClass("active");
       } else if(cityName == "dischouses2") {
          document.getElementById("filtertabans").style.display = "none";
          document.getElementById("filtertabques").style.display = "none";
          document.getElementById("filtertabhouse").style.display = "block";
          $("#filtertabhouse .tabactive2").addClass("active");
          $("#filtertabhouse .tabactive1").removeClass("active");
          $("#filtertabhouse .tabactive3").removeClass("active");
       }
   }

   function openCity1(evt, cityName) {
       var i, tabcontent, tablinks;
       tabcontent = document.getElementsByClassName("tabcontent");
       for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
       }
       tablinks = document.getElementsByClassName("tablinks1");
       for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace(" active", "");
       }
       document.getElementById(cityName).style.display = "block";
       evt.currentTarget.className += " active";
   }
   
   function openReport(evt, reportName) {
     var i, repocontent, repolinks;
     repocontent = document.getElementsByClassName("repocontent");
     for (i = 0; i < repocontent.length; i++) {
       repocontent[i].style.display = "none";
     }
     repolinks = document.getElementsByClassName("repolinks");
     for (i = 0; i < repolinks.length; i++) {
       repolinks[i].className = repolinks[i].className.replace(" active", "");
     }
     document.getElementById(reportName).style.display = "block";
     evt.currentTarget.className += " active";
   }

   function moreFunction(id) {
      var cusId = ".anscomplete" + id;
      var moreId = ".ansmore" + id;

      $(cusId).css("display", "inline");
      $(moreId).css("display", "none");
   }
</script>

<!-- Modal -->
<div id="myModalques" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Follow</h4>
         </div>
         <div class="modal-body">
            <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>

<div id="myModalmemb1" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h1>Way to post your question</h1>
      </div>
      <div class="modal-body">
        <ul><li>
        <a href="#">Click on the appropriate <b>discussion</b> group from the list given in <b>Discussion Forum home page</b>.</a></li></br>
         <li><a href="#">Go to the list of <b>discussion houses</b> of desired group & choose appropriate discussion house for your question.</a></li></br>
          <li><a href="#">  Join <b>discussion house</b> & post your question.</a></li>
          </ul>
      </div>
    </div>
  </div>
</div>

<div id="myModalpostany" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       <h3>Way to post your question</h3>
      </div>
      <div class="modal-body">
        <p> Please login to start any discussion. <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
      </div>
    </div>
  </div>
</div>
