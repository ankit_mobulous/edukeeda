<div class="eventnew">
<?php
/**
/*
 Template Name: Blog listing
 */

get_header(); ?>
<?php
  if(isset($_POST["follow"])) {
      global $wpdb;
      $catType = $_POST["catType"];
      $catId = $_POST["catId"];
      $userid = $_SESSION["login"]["id"];
      $data = ["user_id"=>$userid, "cat_id"=>$catId, "cat_type"=>$catType];
      $wpdb->insert("wp_follow_cat", $data);
  }
  if(isset($_POST["unfollow"])) {
        $cid = $_POST["catId"];
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $wpdb->query($wpdb->prepare("DELETE FROM wp_follow_cat WHERE user_id = %d AND cat_id = %d", $userid, $cid));
    }
?>
   <div class="container">
      <div class="arteduro">
         <div class="row">
            <div class="col-md-8 col-sm-8">
               <div class="colh singlepost">
                  <?php
                        global $wpdb;
                        $catId = $_GET["id"];
                        $getCName = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $catId) 
                            );
                        $articles = $wpdb->get_results( 
                         $wpdb->prepare( "SELECT COUNT(wp_term_relationships.object_id) as article FROM wp_term_relationships INNER JOIN wp_posts ON wp_term_relationships.object_id = wp_posts.ID WHERE wp_term_relationships.term_taxonomy_id = %d AND post_status='publish'", $catId)
                        );
                    ?>
                  <h4><?php echo $getCName[0]->name; ?> <span>(<?php echo $articles[0]->article; ?> Blogs)</span> </h4>
                      <?php
                        if(isset($_SESSION["login"])) {
                          $userId = $_SESSION["login"]["id"];
                          $type = $_GET["action"];
                          $term_id = $_GET["id"];
                          $checkFollow = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_follow_cat WHERE user_id=%d AND cat_id=%d", $userId, $term_id) 
                          );
                          if($checkFollow){
                        ?>
                              <form method="post" action="">
                                <input type="hidden" name="catId" value="<?php echo $term_id;?>">
                                <input type="hidden" name="catType" value="<?php echo $type;?>">
                                <input type="submit" name="unfollow" value="Unfollow" class="btn btn-warning followbtn" style="float: right;">
                              </form>
                        <?php
                          } else{
                        ?>
                              <form method="post" action="">
                                <input type="hidden" name="catId" value="<?php echo $term_id;?>">
                                <input type="hidden" name="catType" value="<?php echo $type;?>">
                                <input type="submit" name="follow" value="Follow" class="btn btn-warning followbtn" style="float: right;">
                              </form>
                        <?php 
                          }
                        } else {
                      ?>
                          <button type="button" class="btn btn-warning followbtn" data-toggle="modal" data-target="#myModal" style="float: right;">Follow</button>
                      <?php   
                        }
                      ?>
               </div>
            </div>
            <div class="col-md-8 col-xs-12">
               <div class="">
                  <div class="rightoption">
                     <form action="" method="post" name="myFormName">
                     <p>Sort By:</p>
                        <select name="top_filter" id="topsuccessfilter" onchange='this.form.submit()'>
                          <option value=""> Select </option>
                           <option value="recent">Recent</option>
                           <option value="views">Most Views</option>
                        </select>
                     </form>
                  </div>
                  <div class="sid">
                     <form class="searchform" action=" " method="post" class="search-form">
                       <input type="text" placeholder="Search.." name="search" class="searchtext">
                       <button type="submit"><i class="fa fa-search"></i></button>
                     </form>
                  </div>
               </div>
                <div class="col-md-4 customsideedus">
               <div id="secondary" class="widget-area col-md-3" role="complementary">
                  <span style="float:right;"> <a href="<?php echo site_url();?>/expertise/blog-posting/?id=<?php echo $_GET['id']; ?>&action=<?php echo $_GET['action'];?>"> Clear All </a> </span>

                  <aside id="recent-posts-3" class="widget widget_recent_entries" style="padding-top: 12px;">
                   <button class="accordion">  <h3 class="widget-title">Filter</h3></button>
                   <div class="panel">
                     <?php
                        $parent_cat_ID = $_GET["id"];

                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                              'taxonomy' => 'blog_category'
                           );

                        $subcats = get_categories($args);
                        //var_dump($subcats);
                        echo "<ul>";
                        foreach($subcats as $subcat) {

                            $term_id = $subcat->term_id;
                            $getListCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id));

                      ?>
                                <li>
                                    <form method="post" action="" id="form-id"> 
                                      <a href="<?php echo site_url(); ?>/expertise/blog-posting?id=<?php echo $_GET['id']; ?>&action=<?php echo $_GET['action'];?>&action1=<?php echo $term_id; ?>" onclick="document.getElementById('form-id').submit();"> <?php echo $subcat->name; ?>  ( <?php echo $getListCount[0]->countPost; ?> ) </a>
                                      <input type="hidden" name="fitercat" value="<?php echo $subcat->term_id;?>">
                                    </form>
                                </li>
                            <?php
                        }
                        echo "</ul>";
                     ?></div>
                  </aside>


               </div>
            </div>
               <?php
               if(isset($_POST["search"])) {

                 global $wpdb;
                 $searchFilter = $_POST["search"];
                 
                 if(isset($_GET['action1'])){
                        $catId = $_GET['action1'];
                     } else{
                        $catId = $_GET["id"];
                     }
                     $action = $_GET["action"];

                  global $wpdb;

                  $results = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id = %d  ORDER BY object_id DESC", $catId) 
                  );

                  foreach($results as $result) {
                    $results1 = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d AND post_status='publish' AND post_type='blogs'", $result->object_id) 
                    );
                    
                    foreach($results1 as $result1) {

                      if(stripos($result1->post_title, $searchFilter) !== false) {
                      
                      $getCatName = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]) 
                      );
                      $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                      if($image){
                          $image = $image[0];
                      } else {
                          $mkey = "_thumbnail_id";
                          $checkimg = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                          );
                          $getimg = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                          );
                          $image = $getimg[0]->guid;  
                      }
                      $table_name = "wp_post_views";
                      $checkViews = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
                      );
                      $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                           $author_name = $getAuthor[0]->display_name;
                           
                      $countPostTitle = strlen($result1->post_title);
                      if($countPostTitle >= 80) {
                        $postTitle = substr($result1->post_title,0, 80);
                        $postTitle .= "...";
                      } else {
                        $postTitle = $result1->post_title;
                      }
                ?>
                          <div class="col-md-12 blo">
                            <div class="">
                               <div class="col-md-4 col-sm-4">
                                  <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">  <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                               </div>
                            </div>
                            <div class="col-md-8 col-sm-8">
                               <p class="categtp"><a href="<?php echo site_url();?>/expertise/blog-posting/?id=<?php echo $_GET["id"];?>&action=<?php echo $action;?>"><?php echo strtoupper($getCatName[0]->name); ?></a></p>
                               <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"> <?php echo $postTitle; ?></a>
                               <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/newmembers/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                               </p>
                               <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                               <p class="rightwd"><strong> </strong> <?php echo date('F jS, Y . g:i a',strtotime($result1->post_date)); ?></p>
                            </div>
                         </div>
                     <?php
                           }
                        } 
                      }
                  } else if(isset($_POST["top_filter"])) {
                     if(isset($_GET['action1'])){
                        $catId = $_GET['action1'];
                     } else{
                        $catId = $_GET["id"];
                     }
                     $action = $_GET["action"];
                     $filter = $_POST["top_filter"];
                     echo "<div style='clear:both;'></div>";
                     global $wpdb;

                     if($filter == "views") {
                      echo "<div><p>Sort by: Most Views</p></div>";

                       $results = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_term_relationships LEFT JOIN wp_post_views ON wp_term_relationships.object_id = wp_post_views.post_id WHERE wp_term_relationships.term_taxonomy_id = %d ORDER BY wp_post_views.view_count DESC", $catId) 
                       );
                     } else if($filter == "recent") {
                        echo "<div><p>Sort by: Recent Post</p></div>";
                        $results = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id = %d ORDER BY object_id DESC", $catId) 
                        );
                     }
                     foreach($results as $result) {
                       $results1 = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d AND post_status='publish' AND post_type='blogs'", $result->object_id) 
                       );
                       
                        foreach($results1 as $result1) {
                    
                         $getCatName = $wpdb->get_results( 
                             $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]) 
                         );
                         $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                          if($image){
                              $image = $image[0];
                          } else {
                              $mkey = "_thumbnail_id";
                            $checkimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                            );
                            $getimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                            );
                            $image = $getimg[0]->guid;  
                          } 
                          $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                           $author_name = $getAuthor[0]->display_name;

                          $table_name = "wp_post_views";
                          $checkViews = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
                          );
                          $countPostTitle = strlen($result1->post_title);
                          if($countPostTitle >= 80) {
                            $postTitle = substr($result1->post_title,0, 80);
                            $postTitle .= "...";
                          } else {
                            $postTitle = $result1->post_title;
                          }               
                ?>
                      <div class="col-md-12 blo">
                          <div class="">
                             <div class="col-md-4 col-sm-4">
                                <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">  <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                             </div>
                          </div>
                          <div class="col-md-8 col-sm-8">
                             <p class="categtp"><a href="<?php echo site_url();?>/expertise/blog-posting/?id=<?php echo $_GET["id"];?>&action=<?php echo $action;?>"><?php echo strtoupper($getCatName[0]->name); ?></a></p>
                             <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"> <?php echo $postTitle; ?></a>
                             <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/newmembers/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                             </p>
                             <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                             <p class="rightwd"><strong> </strong> <?php echo date('F jS, Y . g:i a',strtotime($result1->post_date)); ?></p>
                          </div>
                       </div>
                <?php 
                       }
                    }
                  }  else{
                    if(isset($_GET['action1'])){
                        $catId = $_GET['action1'];
                     } else{
                        $catId = $_GET["id"];
                     }
                     $action = $_GET["action"];
                    
                    global $wpdb;
                    $results = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE term_taxonomy_id = %d ORDER BY object_id DESC", $catId) 
                    );

                  foreach($results as $result) {
                    $results1 = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d AND post_status='publish' AND post_type='blogs'", $result->object_id) 
                    );
                    
                    foreach($results1 as $result1) {
                 
                      $getCatName = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $_GET["id"]) 
                      );
                      $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                      if($image){
                          $image = $image[0];
                      } else {
                          $mkey = "_thumbnail_id";
                          $checkimg = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                          );
                          $getimg = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                          );
                          $image = $getimg[0]->guid;  
                      }
                      $table_name = "wp_post_views";
                      $checkViews = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
                      );
                      $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                           $author_name = $getAuthor[0]->display_name;

                      $countPostTitle = strlen($result1->post_title);
                      if($countPostTitle >= 80) {
                        $postTitle = substr($result1->post_title,0, 80);
                        $postTitle .= "...";
                      } else {
                        $postTitle = $result1->post_title;
                      }
                                         
                ?>
               <div class="col-md-12 blo">
                  <div class="">
                     <div class="col-md-4 col-sm-4">
                        <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">  <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                     </div>
                  </div>
                  <div class="col-md-8 col-sm-8">
                     <p class="categtp"><a href="<?php echo site_url();?>/expertise/blog-posting/?id=<?php echo $_GET["id"];?>&action=<?php echo $action;?>"><?php echo strtoupper($getCatName[0]->name); ?></a></p>
                     <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"> <?php echo $postTitle; ?></a>
                     <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/newmembers/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                     </p>
                     <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                     <p class="rightwd"><strong> </strong> <?php echo date('F jS, Y . g:i a',strtotime($result1->post_date)); ?></p>
                  </div>
               </div>
               <?php 
                    }
                  }
                }
              ?>
            </div>
            <div class="col-md-4 customsideedu">
               <div id="secondary" class="widget-area col-md-3" role="complementary">
                  <span style="float:right;"> <a href="<?php echo site_url();?>/expertise/blog-posting/?id=<?php echo $_GET['id']; ?>&action=<?php echo $_GET['action'];?>"> Clear All </a> </span>

                  <aside id="recent-posts-3d" class="widget widget_recent_entries" style="padding-top: 12px;">
                   <button class="accordion">  <h3 class="widget-title">Filter</h3></button>
                   <div class="panel">
                     <?php
                        $parent_cat_ID = $_GET["id"];

                            $args = array(
                               'hierarchical' => 1,
                               'show_option_none' => '',
                               'hide_empty' => 0,
                               'parent' => $parent_cat_ID,
                              'taxonomy' => 'blog_category'
                           );

                        $subcats = get_categories($args);
                        //var_dump($subcats);
                        echo "<ul>";
                        foreach($subcats as $subcat) {

                            $term_id = $subcat->term_id;
                            $getListCount = $wpdb->get_results($wpdb->prepare("SELECT COUNT(wp_term_relationships.object_id) as countPost FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id WHERE wp_term_relationships.term_taxonomy_id = %d AND wp_posts.post_status='publish'", $term_id));

                      ?>
                                <li>
                                    <form method="post" action="" id="form-id"> 
                                      <a href="<?php echo site_url(); ?>/expertise/blog-posting?id=<?php echo $_GET['id']; ?>&action=<?php echo $_GET['action'];?>&action1=<?php echo $term_id; ?>" onclick="document.getElementById('form-id').submit();"> <?php echo $subcat->name; ?>  ( <?php echo $getListCount[0]->countPost; ?> ) </a>
                                      <input type="hidden" name="fitercat" value="<?php echo $subcat->term_id;?>">
                                    </form>
                                </li>
                            <?php
                        }
                        echo "</ul>";
                     ?></div>
                  </aside>


                  <aside id="recent-posts-3" class="widget widget_recent_entries">
                     <h3 class="widget-title">TOP TRENDING ARTICLES</h3>
                      <?php
                        $typePost = "blogs";
                        $topPosts = $wpdb->get_results( 
                          $wpdb->prepare("SELECT * FROM wp_posts INNER JOIN wp_post_views ON wp_posts.ID = wp_post_views.post_id WHERE wp_posts.post_type = %s AND wp_posts.post_status = 'publish' ORDER BY wp_post_views.view_count DESC LIMIT 5", $typePost)
                        );

                        if(!empty($topPosts))
                        {
                          
                          foreach ($topPosts as $topPost) {

                            $getCatID = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT term_taxonomy_id FROM wp_term_relationships WHERE object_id = %d", $topPost->ID) 
                            );
                            $getCatName = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $getCatID[0]->term_taxonomy_id) 
                            );

                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $topPost->ID ), 'single-post-thumbnail' );
                            if($image){
                                $image = $image[0];
                            } else {
                                $mkey = "_thumbnail_id";
                                $checkimg = $wpdb->get_results( 
                                      $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $topPost->ID,$mkey) 
                                );
                                $getimg = $wpdb->get_results( 
                                      $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                                );
                                $image = $getimg[0]->guid;  
                            }
                            $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $topPost->post_author));
                           $author_name = $getAuthor[0]->display_name;



                            $countPostTitle = str_word_count($topPost->post_title);
                            if($countPostTitle >= 10) {
                              $postTitle = substr($topPost->post_title,0, 30);
                              $postTitle .= "...";
                            } else {
                              $postTitle = $topPost->post_title;
                            }
                   ?>

                     <div class="othermemd">
                        <div class="col-md-5">
                           <div class="simg">
                              <a href="<?php echo esc_url( get_permalink($topPost->ID) ); ?>">   <img src="<?php echo $image; ?>" class="img-thumbnail"></a>
                           </div>
                        </div>
                        <div class="col-md-7">
                           <div class="pimg">
                              <p> <strong><a href="<?php echo site_url();?>/expertise/blog-posting/?id=<?php echo $getCatID[0]->term_taxonomy_id;?>&action=<?php echo $action;?>"><?php echo strtoupper($getCatName[0]->name); ?></a></strong></p>
                              <a href="<?php echo esc_url( get_permalink($topPost->ID) ); ?>">
                                 <sapn>
                                 <?php echo !empty($postTitle)?$postTitle:""; ?></span>
                              </a>
                              <span>By <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/newmembers/?action=<?php echo $topPost->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?> 
                              </span>
                           </div>
                        </div>
                     </div>
                     <?php 
                       }
                     }
                    ?>
                  </aside>
               </div>
            </div>
         </div>
      </div>
     
      </main><!-- #main -->
   </div>
   <!-- #primary -->
   <?php get_footer(); ?>
</div>
<style>
#secondary button.accordion {
    background-color: transparent;
    border: none;
    width: 100%;
    text-align: left;
   
    padding:0px;
}
#secondary button.accordion:hover {
    outline: none;
}
.panel {
  display: none;
  background-color: #f4f4f4;
}
.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    margin-top: -37px;
    font-size: 43px;
}
   div#secondary h3.widget-title{
   float: left;
   width: 100%;
   border-bottom: 1px solid #f68e2f;
   }
   .blo a.bloghdaedu {
   font-size: 20px;
   margin: 0;
   padding: 0;
   line-height: 20px;
   text-align: left;
   width: 100%;
   font-weight: 600;
   }
.widget-area .widget {
    position: relative;
    margin-bottom: 30px;
    padding-top: 0px;
    float: left;
    width: 100%;
}
   p.leftwd {
   float: left;
   width: 89%;
   color: #9e9292;
   }
   a.rightwd.views {
   color: #000;
   font-size: 13px;
   float: right;
   }
   .sid input.search-field {
   width: 103%;
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   padding: 0 16px;
   }
   .fullwd{width:100%; float:left;}
   .leftwd{float:left;}
   .rightwd{float:left;}
   .pimg1 p { 
   margin-bottom: 0px;
   line-height: 18px;
   font-size: 16px;
   margin-top: 10px;
   }
   .othermemd .col-md-5{
   padding:0px;
   } 
   .othermemd .cat1 {
   text-align: center;
   padding: 0px;
   }
   .pimg1 span {
   color: #f68e2f;
   }
   .pimg strong {
   font-weight: 100;
   font-size: 13px;
   }
   .pimg sapn {
   font-size: 16px;
   line-height: 16px;}
   .cat1 {
   text-align: center;
   }
   .pimg a {
   line-height: 20px;
   }
   .pimg p {
   margin-bottom: 6px;
   }
   .pimg1 strong {
   font-weight: 500;
   }
   .pimg span {
   font-size: 13px;
   COLOR: #aa9292;
   margin-top: 4px;
   display: block;
   }
   #secondary {
   background-color: #f4f4f4;
   width: 100%;
   float:left;
   }
   .blo a.bloghdaedu {
   font-size: 20px;
   margin: 0;
   padding: 0;
   line-height: 20px;
   float: left;
   width: 100%;
   font-weight: 600;
   }
   .cat {
   padding: 0;
   }
   .blo p {
   margin-bottom: 7px;
   font-size: 14px;
   text-align: left;
   }
   .othermemd {
   box-shadow: -1px 0px 4px 0 rgba(0, 0, 0, 0.07);
   float: left;
   width: 100%;
   border: 1px solid #ddd;
   padding: 10px;
   margin-bottom:20px;
   }
   h4 {
   margin: 0 0 17px 0;
   }
   .blo img.img-thumbnail {
   height: 147px;
   }
   .headarea {
   float: left;
   width: 100%;
   }
   .other a {
   float: right;
   }
   .other h4 {
   float: left;
   margin: 0 0 18px 0;
   }
   .profile ul {
   padding: 0;
   list-style-type: none;
   }
   .memb .profile h3{
   color: #828282;
   }
   .profile {
   float: left;
   width: 100%;
   text-align: center;
   background: #efeded;
   padding: 10px;
   }
   .memb .profile p{   text-align: center; float: none;}
   p.categtypeedus {
   font-size: 15px;
   color: #7f7a7a;
   }
   .memb .profile h2 {
   font-size: 21px;
   color: #060658;
   }
   .profile ul li {
   float: left;
   display: block;
   }
   .profile ul li span, .profile ul li p{
   float:left;
   font-size: 15px;
   }
   .memb .profile .fa.fa-eye{
   margin-right: 0px !important;
   }
   .profile ul li span {
   width: 185px;
   }
   .memb {
   text-align:left;
   }
   .memb h2 {
   font-size: 30px;
   padding: 0px;
   margin: 10px 0px 0px 0px;
   }
   .memb1 {
   background-color: #fff;
   border: 1px solid #ddd;
   border-radius: 4px;
   padding: 48px;
   margin-top: 0px;
   float: left;
   width: 100%;
   }
   .memb h3 {
   font-size: 17px;
   margin: 5px;
   }
   .memb p {
   margin: 0 0 0px;
   float: left;
   }
   .memb1 p{
   float: left;
   text-align: center;
   font-size: 23px;
   font-weight: 600;
   }
   .memb1 button{
   float: right;
   }
   .memb spam1{
   font-weight:600;
   } 
   .memb spam{
   font-weight:600;
   }
   .memb .fa.fa-eye {
   display: initial;
   }
   .blo {
   border-radius: 4px;
   background-color: #ffffff;
   box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.07);
   border: solid 1px #ececec;
   margin-bottom: 30px;
   padding: 10px;
   float: left;
   width: 100%;
   }
   .arteduro {
   float: left;
   width: 100%;
   margin: 30px 0;
   }
   .blo2 a {
   float: right;
   }
   .rightoption select {
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   line-height: 3px;
   padding: 1px 18px;
   margin: 0 0px 0 0px;
   }
   .rightoption p {
   float: left;
   margin: 0px 29px;
   }
   .rightoption {
   float: right;
   }
   .colh {
   text-align: left;
   }
   .sid {
   float: left;
   }
   .pimg {
   TEXT-ALIGN: LEFT;
   }
   .pimg1 {
   TEXT-ALIGN: LEFT;
   }


   .col-md-4.customsideedus {
    display: none;
}
   @media only screen and (max-width: 767px){
   .rightwd{
   float: left;
   }
   .col-md-4.customsideedus {
    display: block;
        padding: 0px;
}
aside#recent-posts-3d {
    display: none;
}
.blo {

    margin-top: 30px;
}
.widget-area .widget {
    margin-bottom: 0px;
}
   .leftwd {
   width: 100%;
   }
   .rightoption select {
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   line-height: 3px;
   padding: 1px 55px;
   margin: 1px 0px 8px 0px;
   }
   .rightoption p {
   margin: 0px 29px;
   }
   .arteduro {
   margin-top:10px;
   }
   .searchform input {
    height: 36px!important;
    padding: 4px 17px!important;
  }
  .rightoption select {
    padding: 1px 60px;
  }
  .single .entry-header, .page .entry-header {
    margin-bottom: 30px;
    margin-top: 100px;
}
.rightoption {
    float: right;
    width: 100%;
}
   .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
}
   }
</style>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
