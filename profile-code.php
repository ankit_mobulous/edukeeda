<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;

    if(isset($_POST['submitstepone'])) {
        $headline = trim($_POST["headline"]);
        $other = trim($_POST["other"]);
        $userId = $_SESSION["login"]["id"];
        $page = trim($_POST["lmember"]);
        $tablename = "wp_profileStepOne";
        $getStepOne = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_profileStepOne WHERE user_id = %d",$userId) 
        );

        if($getStepOne) {
            
            if($page == "homemember") {
              $wpdb->update($tablename, array('headline'=>$headline), array('user_id'=>$userId));
              header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
            } else if($page == "homememberabout") {
              $wpdb->update($tablename, array('other'=>$other), array('user_id'=>$userId));
              header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
            } else {
              $wpdb->update($tablename, array('headline'=>$headline), array('user_id'=>$userId));
              header('Location: https://edukeeda.com/edit-profile/');
            }
        } else {

            if($page == "homemember") {
                $data = array(
                      "user_id" => $userId,
                      "headline" => $headline
                    );
                $result = $wpdb->insert($tablename, $data);
              header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
            } else if($page == "homememberabout") {
                $data = array(
                      "user_id" => $userId,
                      "other" => $other
                    );
                $result = $wpdb->insert($tablename, $data);
                header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
            } else {
                $data = array(
                      "user_id" => $userId,
                      "headline" => $headline
                    );
                $result = $wpdb->insert($tablename, $data);
              header('Location: https://edukeeda.com/edit-profile/');
            } 
        }
    } else{
          $wrongCred = "Profile updation failed";
          $_SESSION["failed"] = $wrongCred;
          if($_POST["lmember"] == "homemember") {
            header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
          }else {
            header('Location: https://edukeeda.com/edit-profile/');
          }
    }

    if(isset($_POST['submitsteptwo'])) {
        $name = trim($_POST["name"]);
        $gender = trim($_POST["gender"]);
        $email = trim($_POST["email"]);
        $mobile = trim($_POST["mobile"]);
        $country = trim($_POST["country"]);
        $state = trim($_POST["state"]);
        $city = trim($_POST["city"]);
        $zip = trim($_POST["zip"]);
        $userId = $_SESSION["login"]["id"];
        
        $tablename = "wp_profileStepTwo";
        $getStepTwo = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_profileStepTwo WHERE user_id = %d",$userId) 
        );
        if($getStepTwo) {

          $wpdb->update($tablename, array('name'=>$name), array('user_id'=>$userId));
          $wpdb->update($tablename, array('gender'=>$gender), array('user_id'=>$userId));
          $wpdb->update($tablename, array('mobile'=>$mobile), array('user_id'=>$userId));
          $wpdb->update($tablename, array('country'=>$country), array('user_id'=>$userId));
          $wpdb->update($tablename, array('state'=>$state), array('user_id'=>$userId));
          $wpdb->update($tablename, array('city'=>$city), array('user_id'=>$userId));
          $wpdb->update($tablename, array('zipcode'=>$zip), array('user_id'=>$userId));

        } else {
          if(empty($state)){
            $state = " ";
          }
          if(empty($zip)){
            $zip = " ";
          }
            $data = array(
                    "user_id" => $userId,
                    "name" => $name,
                    "gender" => $gender,
                    "mobile" => $mobile,
                    "country" => $country,
                    "state" => $state,
                    "city" => $city,
                    "zipcode" => $zip
                  );
          $result = $wpdb->insert($tablename, $data);
        }

        header('Location: https://edukeeda.com/edit-profile/');

    } else{
          $wrongCred = "Profile updation failed";
          $_SESSION["failed"] = $wrongCred;
          header('Location: https://edukeeda.com/edit-profile/');
    }

    if(isset($_POST['submitstepthree'])) {

        $school = trim($_POST["school"]);
        $degree = trim($_POST["degree"]);
        $branch = trim($_POST["branch"]);
        $grade = trim($_POST["grade"]);
        $from = trim($_POST["from"]);
        $to = trim($_POST["to"]);
        $desc = trim($_POST["desc"]);
        $userId = $_SESSION["login"]["id"];
        
        $tablename = "wp_profileStepThree";
        $getStepThree = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_profileStepThree WHERE user_id = %d",$userId) 
        );

        if($getStepThree) {

          $checkSchool= $wpdb->get_results( 
              $wpdb->prepare( "SELECT * FROM wp_schools WHERE schoolName = %s",$school) 
          );

          if($checkSchool) {

              $wpdb->update($tablename, array('school'=>$checkSchool[0]->id), array('user_id'=>$userId));
              $wpdb->update($tablename, array('degree'=>$degree), array('user_id'=>$userId));
              $wpdb->update($tablename, array('branch'=>$branch), array('user_id'=>$userId));
              $wpdb->update($tablename, array('grade'=>$grade), array('user_id'=>$userId));
              $wpdb->update($tablename, array('from'=>$from), array('user_id'=>$userId));
              $wpdb->update($tablename, array('to'=>$to), array('user_id'=>$userId));
              $wpdb->update($tablename, array('desc'=>$desc), array('user_id'=>$userId));
          } else {

              if($_FILES["edufile"]["name"]) {
                $upload_dir  = wp_upload_dir();
                $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

                $extension = pathinfo($_FILES["edufile"]["name"], PATHINFO_EXTENSION);
                $newfilename = substr(sha1(mt_rand().time()), 0, 11).".".$extension;

                $file = $upload_path . $newfilename;
                
                $raw_file_name = $_FILES['edufile']['tmp_name'];
                $siteurl = site_url();
                $saveFile = $siteurl.'/wp-content/uploads/'.date('Y/m').'/'. $newfilename;

                if (move_uploaded_file($_FILES['edufile']['tmp_name'], $file)) { } 

              } else {
                  $saveFile = " ";
              }

            $sdata = ["schoolName"=>$school, "schoolImg"=> $saveFile];
            $wpdb->insert("wp_schools", $sdata);
            $checkSchool1 = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_schools WHERE schoolName = %s",$school) 
            );
              $wpdb->update($tablename, array('school'=>$checkSchool1[0]->id), array('user_id'=>$userId));
              $wpdb->update($tablename, array('degree'=>$degree), array('user_id'=>$userId));
              $wpdb->update($tablename, array('branch'=>$branch), array('user_id'=>$userId));
              $wpdb->update($tablename, array('grade'=>$grade), array('user_id'=>$userId));
              $wpdb->update($tablename, array('from'=>$from), array('user_id'=>$userId));
              $wpdb->update($tablename, array('to'=>$to), array('user_id'=>$userId));
              $wpdb->update($tablename, array('desc'=>$desc), array('user_id'=>$userId));
          }

        } else {

          $checkSchool= $wpdb->get_results( 
              $wpdb->prepare( "SELECT * FROM wp_schools WHERE schoolName = %s",$school) 
          );
          
          if($checkSchool) {
            
              $data = array(
                    "user_id" => $userId,
                    "school" => $checkSchool[0]->id,
                    "degree" => $degree,
                    "branch" => $branch,
                    "grade" => $grade,
                    "from" => $from,
                    "to" => $to,
                    "desc" => $desc
                  );
              $result = $wpdb->insert($tablename, $data);
          
          } else {

            if($_FILES["edufile"]["name"]) {
              $upload_dir  = wp_upload_dir();
              $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

              $extension = pathinfo($_FILES["edufile"]["name"], PATHINFO_EXTENSION);
              $newfilename = substr(sha1(mt_rand().time()), 0, 11).".".$extension;

              $file = $upload_path . $newfilename;
              
              $raw_file_name = $_FILES['edufile']['tmp_name'];
              $siteurl = site_url();
              $saveFile = $siteurl.'/wp-content/uploads/'.date('Y/m').'/'. $newfilename;

              if (move_uploaded_file($_FILES['edufile']['tmp_name'], $file)) { } 

            } else {
                $saveFile = " ";
            }

            $sdata = ["schoolName"=>$school, "schoolImg"=> $saveFile];
            $wpdb->insert("wp_schools", $sdata);

            $checkSchool1 = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_schools WHERE schoolName = %s",$school) 
            );
              $data = array(
                    "user_id" => $userId,
                    "school" => $checkSchool1[0]->id,
                    "degree" => $degree,
                    "branch" => $branch,
                    "grade" => $grade,
                    "from" => $from,
                    "to" => $to,
                    "desc" => $desc
                  );
          $result = $wpdb->insert($tablename, $data);
          }
        }
        header('Location: https://edukeeda.com/edit-profile/');

    } else{
          $wrongCred = "Profile updation failed";
          $_SESSION["failed"] = $wrongCred;
          header('Location: https://edukeeda.com/edit-profile/');
    }


    if(isset($_POST['submitstepfour'])) {

        $title = trim($_POST["title"]);
        $company_name = trim($_POST["company_name"]);
        $country = trim($_POST["country"]);
        $location = trim($_POST["location"]);
        $from = trim($_POST["from"]);
        $to = trim($_POST["to"]);

        if($_POST["current_working"]) {
            $current_working = trim($_POST["current_working"]);
        } else {
            $current_working = 0;
            $to = " ";
        }
        
        $desc = trim($_POST["desc"]);
        $userId = $_SESSION["login"]["id"];
        
        $tablename = "wp_profileStepFour";
        $getStepFour = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_profileStepFour WHERE user_id = %d",$userId) 
        );

        if($getStepFour) {

          $checkSchool= $wpdb->get_results( 
              $wpdb->prepare( "SELECT * FROM wp_companies WHERE companyName = %s",$company_name) 
          );

          if($checkSchool) {

              $wpdb->update($tablename, array('company_name'=>$checkSchool[0]->id), array('user_id'=>$userId));
              $wpdb->update($tablename, array('country'=>$country), array('user_id'=>$userId));
              $wpdb->update($tablename, array('location'=>$location), array('user_id'=>$userId));
              $wpdb->update($tablename, array('grade'=>$grade), array('user_id'=>$userId));
              $wpdb->update($tablename, array('from'=>$from), array('user_id'=>$userId));
              $wpdb->update($tablename, array('to'=>$to), array('user_id'=>$userId));
              $wpdb->update($tablename, array('current_working'=>$current_working), array('user_id'=>$userId));
              $wpdb->update($tablename, array('desc'=>$desc), array('user_id'=>$userId));
          } else {

              if($_FILES["companyLogo"]["name"]) {
                $upload_dir  = wp_upload_dir();
                $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

                $extension = pathinfo($_FILES["companyLogo"]["name"], PATHINFO_EXTENSION);
                $newfilename = substr(sha1(mt_rand().time()), 0, 12).".".$extension;

                $file = $upload_path . $newfilename;
                
                $raw_file_name = $_FILES['companyLogo']['tmp_name'];
                $siteurl = site_url();
                $saveFile = $siteurl.'/wp-content/uploads/'.date('Y/m').'/'. $newfilename;

                if (move_uploaded_file($_FILES['companyLogo']['tmp_name'], $file)) { } 

              } else {
                  $saveFile = " ";
              }

            $sdata = ["companyName"=>$company_name, "companyImg"=> $saveFile];
            $wpdb->insert("wp_companies", $sdata);

            $checkSchool1 = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_companies WHERE companyName = %s",$company_name) 
            );
              $wpdb->update($tablename, array('company_name'=>$checkSchool1[0]->id), array('user_id'=>$userId));
              $wpdb->update($tablename, array('country'=>$country), array('user_id'=>$userId));
              $wpdb->update($tablename, array('location'=>$location), array('user_id'=>$userId));
              $wpdb->update($tablename, array('grade'=>$grade), array('user_id'=>$userId));
              $wpdb->update($tablename, array('from'=>$from), array('user_id'=>$userId));
              $wpdb->update($tablename, array('to'=>$to), array('user_id'=>$userId));
              $wpdb->update($tablename, array('current_working'=>$current_working), array('user_id'=>$userId));
              $wpdb->update($tablename, array('desc'=>$desc), array('user_id'=>$userId));
          }

        } else {

          $checkSchool = $wpdb->get_results( 
              $wpdb->prepare( "SELECT * FROM wp_companies WHERE companyName = %s",$company_name) 
          );

          if($checkSchool) {
            
              $data = array(
                    "user_id" => $userId,
                    "company_name" => $checkSchool[0]->id,
                    "country" => $country,
                    "location" => $location,
                    "grade" => $grade,
                    "from" => $from,
                    "to" => $to,
                    "current_working" => $current_working,
                    "desc" => $desc
                  );
              var_dump($data);
              $result = $wpdb->insert($tablename, $data);
          
          } else {

            if($_FILES["companyLogo"]["name"]) {
              $upload_dir  = wp_upload_dir();
              $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

              $extension = pathinfo($_FILES["companyLogo"]["name"], PATHINFO_EXTENSION);
              $newfilename = substr(sha1(mt_rand().time()), 0, 12).".".$extension;

              $file = $upload_path . $newfilename;
              
              $raw_file_name = $_FILES['companyLogo']['tmp_name'];
              $siteurl = site_url();
              $saveFile = $siteurl.'/wp-content/uploads/'.date('Y/m').'/'. $newfilename;

              if (move_uploaded_file($_FILES['companyLogo']['tmp_name'], $file)) { } 

            } else {
                $saveFile = " ";
            }

            $sdata = ["companyName"=>$company_name, "companyImg"=> $saveFile];
            $wpdb->insert("wp_companies", $sdata);

            $checkSchool1 = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_companies WHERE companyName = %s",$company_name) 
            );
              $data = array(
                    "user_id" => $userId,
                    "company_name" => $checkSchool1[0]->id,
                    "country" => $country,
                    "location" => $location,
                    "grade" => $grade,
                    "from" => $from,
                    "to" => $to,
                    "current_working" => $current_working,
                    "desc" => $desc
                  );
          $result = $wpdb->insert($tablename, $data);
          }
        }
        header('Location: https://edukeeda.com/edit-profile/');

    } else{
          $wrongCred = "Profile updation failed";
          $_SESSION["failed"] = $wrongCred;
          header('Location: https://edukeeda.com/edit-profile/');
    }


    if(isset($_POST['submitstepsix'])) {

        $social = trim($_POST["social"]);
        $userId = $_SESSION["login"]["id"];

        $tablename = "wp_profileStepSix";
        $getStepSix = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_profileStepSix WHERE user_id = %d",$userId) 
        );
        if($getStepSix) {

          $wpdb->update($tablename, array('social'=>$social), array('user_id'=>$userId));

        } else {
          $data = array(
                "user_id" => $userId,
                "social" => $social
              );
          
          $result = $wpdb->insert($tablename, $data);
      }

        header('Location: https://edukeeda.com/edit-profile/');

    } else{
          $wrongCred = "Profile updation failed";
          $_SESSION["failed"] = $wrongCred;
          header('Location: https://edukeeda.com/edit-profile/');
    }

    if(isset($_POST['submitstepseven'])) {

        $accomplishment = trim($_POST["accomplishment"]);
        $userId = $_SESSION["login"]["id"];

        $tablename = "wp_profileStepSeven";
        $getStepSeven = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_profileStepSeven WHERE user_id = %d",$userId) 
        );
        if($getStepSeven) {

          $wpdb->update($tablename, array('accomplishment'=>$accomplishment), array('user_id'=>$userId));

        } else {
          $data = array(
                "user_id" => $userId,
                "accomplishment" => $accomplishment
              );
         
          $result = $wpdb->insert($tablename, $data);
      }

        header('Location: https://edukeeda.com/edit-profile/');

    } else{
          $wrongCred = "Profile updation failed";
          $_SESSION["failed"] = $wrongCred;
          header('Location: https://edukeeda.com/edit-profile/');
    }

?>
