<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;

     if(isset($_POST['login_Sbumit'])) {

        unset($_SESSION["failed"]);
      
        $creds = array();
        $email = stripslashes( trim($_POST["p_email"]));
        $creds['user_password'] =  stripslashes($_POST["p_password"]);
        $user = get_user_by( 'email', $email );
        $creds['user_login'] = $user->data->user_login;
        $creds['remember'] = true;
        $redirect_to = esc_url_raw( $_POST['redirect_to'] );
        $secure_cookie = null;

        if($redirect_to == '')
            $redirect_to = get_site_url(). '/all-members/homepage?action=$user->data->ID' ; 
            
            if ( ! force_ssl_admin() ) {
              $user = is_email( $creds['user_login'] ) ? get_user_by( 'email', $creds['user_login'] ) : get_user_by( 'login', sanitize_user( $creds['user_login'] ) );

            if ( $user && get_user_option( 'use_ssl', $user->data->ID ) ) {
              $secure_cookie = true;
              force_ssl_admin( true );
            }
        }
        if ( force_ssl_admin() ) {
          $secure_cookie = true;
        }

        if ( is_null( $secure_cookie ) && force_ssl_login() ) {
          $secure_cookie = false;
        }

      if($user) {
          $user = wp_signon( $creds, $secure_cookie );
       
          if ( is_wp_error($user) ) {
              $wrongCred = "Email and password are incorrect";
              $_SESSION["failed"] = $wrongCred;
              header('Location: https://edukeeda.com/signin/');
          }
          else {
            if ( $secure_cookie && strstr( $redirect_to, 'wp-admin' ) ) {
              $redirect_to = str_replace( 'http:', 'https:', $redirect_to );
            }

             $response = ['id' => $user->data->ID,'email'=> $user->data->user_email];
             $_SESSION["login"] = $response;

             if(empty($_GET["page"])) {
                wp_safe_redirect( $redirect_to );
             } else {
                header('Location: https://edukeeda.com/premium-test-series/'); 
             }
          }
      } else{
          $wrongCred = "Email and password are incorrect";
          $_SESSION["failed"] = $wrongCred;
          header('Location: https://edukeeda.com/signin/');
      }
   }

?>
