<?php

/*

Template Name: Signup Template

*/
	get_header();


?>

	<div id="primary" class="content-area">

	<div class="row signpagsecss">
	<div class="col-md-6 signupcontainer">
	<h5>New User Sign in Here</h5>

<form action="<?php echo site_url(); ?>/signup" method="post" id="signupForm">

<p>Name (required)<br>
<input type="text" placeholder="name" name="e_name" id="e_name">
</p>

<p>Mobile (required)<br>
<input type="text" placeholder="Mobile Number" name="e_mobile" id="e_mobile">
</p>

<p>Email (required)<br>
<input type="email" placeholder="Email" name="e_email" id="e_email">	
</p>

<p>Exam (required)<br>
<select name="e_exam" id="e_exam">
<option value="JEEE main">JEEE main</option>
<option value="JEE Advance">JEE Advance</option>
<option value="NEET">NEET</option>
</select>
</p>

<p>Qualification (required)
<select name="qualification" id ="qualification">
<option value="9th">9th</option>
<option value="10th">10th</option>
<option value="11th">11th</option>
<option value="12th">12th</option>
<option value="12th pass">12th pass</option>
</select>
</p>

<p>Date(required)</br>
<input type="date" name="e_date" id="e_date">
</p>

<p>School Name(required)<br>
	<input type="text" name="e_school_name" id="e_school_name">
</p>

<p>State(required)<br>
	

<select name="e_state" id="e_state">
    <option value="">Select State</option>
    <option value="Andhra Pradesh">Andhra Pradesh</option>
    <option value="Arunachal Pradesh">Arunachal Pradesh</option>
    <option value="Assam">Assam</option>
    <option value="Bihar">Bihar</option>
    <option value="Chhattisgarh">Chhattisgarh</option>
    <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
    <option value="Daman and Diu">Daman and Diu</option>
    <option value="Delhi">Delhi</option>
    <option value="Goa">Goa</option>
    <option value="Gujarat">Gujarat</option>
    <option value="Haryana">Haryana</option>
    <option value="Himachal Pradesh">Himachal Pradesh</option>
    <option value="Jammu and Kashmir">Jammu and Kashmir</option>
    <option value="Jharkhand">Jharkhand</option>
    <option value="Karnataka">Karnataka</option>
    <option value="Kerala">Kerala</option>
    <option value="Madhya Pradesh">Madhya Pradesh</option>
    <option value="Maharashtra">Maharashtra</option>
    <option value="Manipur">Manipur</option>
    <option value="Meghalaya">Meghalaya</option>
    <option value="Mizoram">Mizoram</option>
    <option value="Nagaland">Nagaland</option>
    <option value="Orissa">Orissa</option>
    <option value="Puducherry">Puducherry</option>
    <option value="Punjab">Punjab</option>
    <option value="Rajasthan">Rajasthan</option>
    <option value="Sikkim">Sikkim</option>
    <option value="Tamil Nadu">Tamil Nadu</option>
    <option value="Telangana">Telangana</option>
    <option value="Tripura">Tripura</option>
    <option value="Uttar Pradesh">Uttar Pradesh</option>
    <option value="Uttarakhand">Uttarakhand</option>
    <option value="West Bengal">West Bengal</option>
</select>
</p>

<p>Place(required)<br>
	<input type="text" name="e_place" id="e_place">
</p>


<p>Pincode(required)<br>
	<input type="text" name="e_pincode" id="e_pincode">
</p>

<p>Amount to be paid(Rs)(required)<br>
	<input type="text" name="e_amount" id="e_amount">
</p>



<p>Password(required)<br>
<input type="password" name="e_password" id="e_password">
</p>

<p> Confirm Password (required)<br>
<input type="password" name="e_c_password" id="e_c_password">
</p>

<input type="submit" value="Send" name="submit">

</form>
</div>

<div class="col-md-6 signupcontainer">

<h5>Login</h5>

<form action="<?php echo site_url(); ?>/login-user/" method="post" id="loginForm">
<p>Email(required)<br>
<input type="email" name="e_login_email" id="e_login_email">
</p>

<p>Password(required)<br>
<input type="password" name="e_login_password" id="e_login_password">
</p>

<input type="submit" value="Send" name="login_submit">
</form>
</div>

</div>



<style>
header.entry-header {
    display: none;
}
.signpagecss input{
    height: 26px;
    margin: 0 0 10px 0;
    background: transparent;
    border: none;
    border-bottom: 1px solid #ddd;
    width: 100%;
}
.signpagecss p {
    margin: 0;
font-size:14px;
}
.signpagecss input{
    padding: 5px 20px;
}
.signupcontainer {
    background: #fff;
    box-shadow: #ded8d5 0px 0px 8px 0px;
    padding: 23px;
    margin: 0 18px;
    width: 45%;
    height: 100%;
    float:left;
}
div#content {
    background: url(http://edukeeda.com/wp-content/uploads/2018/10/edukeeda-web.png);
    padding: 20px;
}
.container.content-wrapper {
    background: rgba(255,255,255,0.9);
    padding: 48px 5% !important;
    border: 1px solid #4848;
}

.signupcontainer label{
    float: left;
    width: 100%;
}
.signupcontainer select{
    width: 100%;
    float: left;
    display: block;
    background: transparent;
    height: 30px;
    margin-bottom: 10px;
}
.signupcontainer input[type="date"] {
    width: 100%;
    background: #fff;
    height: 30px !important;
}
@media only screen and (max-width: 767px){
.signupcontainer {
    margin: 15px 0px;
    width: 100%;
    height: 100%;
}
}

.signupcontainer input[type="text"], .signupcontainer input[type="password"], .signupcontainer input[type="email"]{
    width: 100%;
    background: #fff;
    height: 32px;
}
</style>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>

<script type="text/javascript">
jQuery(document).ready(function($){

	jQuery.validator.addMethod("phoneno", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 && 
            phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
        }, "<br />Please specify a valid phone number");

		$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
			var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please enter valid input."
	);	
	
	$.validator.addMethod("zipCodeValidation", function() {
	var zipCode = $('input#e_pincode').val();
	return (/(^[1-9][0-9]{5}$)/).test(zipCode); // returns boolean
}, "Please enter a valid pincode");


// validate signup form on keyup and submit
		$("#loginForm").validate({
			rules: {
					e_login_email: {
					required: true,
					email: true,
					remote: {
						url:"<?php echo site_url(); ?>/check-email",
						type: "post",

					}

				},
				e_login_password: {
					required: true,
					minlength: 5
				},
			},
			messages: {
				e_login_email: {required:"Please enter a valid email address",remote:"Email id is not registered.Try another!"},
				e_login_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},

			}

			});

		// validate the comment form when it is submitted

		// validate signup form on keyup and submit
		$("#signupForm").validate({
			rules: {
				e_name:{required:true,minlength:3,maxlength:30,regex: "[A-Za-z]+(?:[ _-][A-Za-z]+)*$" },
				e_date: "required",
				e_school_name:{required:true,minlength:3,maxlength:30,regex: "[A-Za-z]+(?:[ _-][A-Za-z]+)*$" },
				e_place:{required:true,minlength:3,maxlength:30,regex: "[A-Za-z]+(?:[ _-][A-Za-z]+)*$" },
				e_state:{required:true},

				e_password: {
					required: true,
					minlength: 5
				},
				e_c_password: {
					required: true,
					minlength: 5, 
					equalTo: "#e_password"
				},
				e_email: {
					required: true,
					email: true,
					remote: {
                        url:"<?php echo site_url(); ?>/check-email-post-signup",
                        type: "post",

                    },
				},
				e_mobile: 
				{
				 required: true,
				 number: true,
				 minlength:8,
				 maxlength:10
				},
				e_pincode:{required:true,zipCodeValidation: true},
				e_amount:{required:true,number:true,minlength:1,
				 maxlength:5},
	
			},
			messages: {
				e_name:{required:"Please enter your name",regex:"Please Enter valid name.",minlength: "Your name must be at least 3 characters long",
						maxlength:"Your name not more than 30 characters"},
				e_school_name:{required:"Please enter school name",regex:"Please Enter valid school name.",minlength: "Your school name must be at least 3 characters long",
						maxlength:"Your school name not more than 30 characters"},	
				e_date: "Please enter expected/actual year of 12th paas",
				e_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				e_place:{required:"Please enter your place",regex:"Please Enter valid place.",minlength: "Your place must be at least 3 characters long",
						maxlength:"Your place not more than 30 characters"},
			    e_state:{required:"Please select your state"},
				e_c_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				},
				e_email: {required:"Please enter a valid email address",remote:"Email id is already registered.Try another!"},
				e_mobile:{required:"Please enter mobile no",maxlength:"Please enter no more than {0} number.",
                    minlength:"Please enter at least {0} number."},
               e_pincode:{required:"Please enter  pincode"},
                e_amount:{required:"Please enter amount",maxlength:"Please enter no more than {0} number.",
                    minlength:"Please enter at least {0} number."},


				
			}
		});

	});	

</script>




		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>

