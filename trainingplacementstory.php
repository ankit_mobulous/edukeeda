<div class="eventnew">
<?php
/**
/*
Template Name: Training & Placement Story
*/

get_header(); ?>
 <?php
    if(isset($_POST["follow"])) {
        $cid = $_POST["catId"];
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $data = ["user_id"=>$userid, "cat_id"=>$cid, "cat_type"=>$catType];
        $wpdb->insert("wp_follow_cat", $data);
    }
    if(isset($_POST["unfollow"])) {
        $cid = $_POST["catId"];
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $wpdb->query($wpdb->prepare("DELETE FROM wp_follow_cat WHERE user_id = %d AND cat_id = %d", $userid, $cid));
    }
?>
   <div class="container">
      <div class="arteduro">
         <div class="row">
            <div class="col-md-8 col-sm-8">
               <div class="colh singlepost">
                    <?php
                        global $wpdb;
                        $catId = 81;
                        $getCName = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $catId) 
                            );
                        $articles = $wpdb->get_results( 
                         $wpdb->prepare( "SELECT COUNT(wp_term_relationships.object_id) as article FROM wp_term_relationships INNER JOIN wp_posts ON wp_term_relationships.object_id = wp_posts.ID WHERE wp_term_relationships.term_taxonomy_id = %d AND post_status='publish'", $catId)
                        );
                    ?>
                  <h4 class="colh"><?php echo $getCName[0]->name; ?> <span>(<?php echo $articles[0]->article; ?> Experiences)</span> </h4>
                      <?php
                        if(isset($_SESSION["login"])) {
                          $userId = $_SESSION["login"]["id"];
                          $type = "training_and_placement";
                          $term_id = $catId;
                          $checkFollow = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_follow_cat WHERE user_id=%d AND cat_id=%d", $userId, $term_id) 
                          );
                          if($checkFollow){
                        ?>
                             <form method="post" action="">
                                <input type="hidden" name="catId" value="<?php echo $term_id;?>">
                                <input type="hidden" name="catType" value="<?php echo $type;?>">
                                <input type="submit" name="unfollow" value="Unfollow" class="btn btn-warning followbtn" style="float:right;">
                              </form>
                        <?php
                          } else{
                        ?>
                              <form method="post" action="">
                                <input type="hidden" name="catId" value="<?php echo $term_id;?>">
                                <input type="hidden" name="catType" value="<?php echo $type;?>">
                                <input type="submit" name="follow" value="Follow" class="btn btn-warning followbtn" style="float:right;">
                              </form>
                        <?php 
                          }
                        } else {
                      ?>
                          <button type="button" class="btn btn-warning followbtn" data-toggle="modal" data-target="#myModalTraining" style="float:right;">Follow</button>
                      <?php   
                        }
                      ?>
               </div>
            </div>
            <!--<div class="col-md-8 col-sm-8">
               <div class="colh">
                  <h1>Training & Placement Story</h1>
               </div>
            </div>-->
            
            <div class="col-md-8 col-sm-8">
               <div class="">
                  <div class="rightoption">
                     <form action="" method="post" name="myFormName">
                     <p>Sort By:</p>
                        <select name="top_filter" id="topsuccessfilter" onchange='this.form.submit()'>
                           <option value=""> Select </option>
                           <option value="recent">Recent</option>
                           <option value="views">Most Viewed</option>
                        </select>
                     </form>
                  </div>
                  <div class="sid">
                     <form class="searchform" action=" " method="post" class="search-form">
                       <input type="text" placeholder="Search.." name="search" class="searchtext">
                       <button type="submit"><i class="fa fa-search"></i></button>
                     </form>
                  </div>
               </div>
               <?php

                  if(isset($_POST["search"])) {

                     global $wpdb;
                     $action = "training_placement";
                     $searchFilter = $_POST["search"];
                    
                     $results1 = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT * FROM wp_posts WHERE post_status='publish' AND post_type=%s", $action) 
                     );
                       
                      foreach($results1 as $result1) {

                        if(stripos($result1->post_title, $searchFilter) !== false) {
                    
                         $getCatName = $wpdb->get_results( 
                             $wpdb->prepare( "SELECT name FROM wp_terms WHERE term_id = %d", $catId) 
                         );
                         $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                         if($image){
                              $image = $image[0];
                          } else {
                              $mkey = "_thumbnail_id";
                            $checkimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                            );
                            $getimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                            );
                            $image = $getimg[0]->guid;
                          }
                          $table_name = "wp_post_views";
                          $checkViews = $wpdb->get_results( 
                              $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
                          ); 
                          
                          $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                           $author_name = $getAuthor[0]->display_name;
                          
                          $countPostTitle = strlen($result1->post_title);
                          if($countPostTitle >= 80) {
                            $postTitle = substr($result1->post_title,0, 80);
                            $postTitle .= "...";
                          } else {
                            $postTitle = $result1->post_title;
                          }
                     ?>

                        <div class="col-md-12 blo">
                           <div class="">
                              <div class="col-md-4 col-sm-4">
                                 <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">   <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-8">
                              <p class="categtp"><strong></strong><a href="<?php echo site_url();?>/training-placement-experiences/"> TRAINING / PLACEMENT EXPERIENCES </a></p>
                              <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"> <?php echo $postTitle; ?></a>
                              <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                              </p>
                              <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                              <p class="rightwd"><strong> <?php echo get_field( "training", $idd); ?> </strong> </p>
                           </div>
                        </div>

                     <?php
                        } 
                      }

                  } else if(isset($_POST["top_filter"])){
                     $pType = "training_placement";
                     $filter = $_POST["top_filter"];
                     echo "<div style='clear:both;'></div>";
                     if($filter == "views") {
                      echo "<div><p>Sort by: Most Views</p></div>";
                        $results = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_posts LEFT JOIN wp_post_views ON wp_posts.ID = wp_post_views.post_id WHERE wp_posts.post_status = 'publish' AND wp_posts.post_type = %s ORDER BY wp_post_views.view_count DESC", $pType) 
                            );
                      
                      } else if($filter == "recent") {
                        echo "<div><p>Sort by: Recent Post</p></div>";
                          $results = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_posts WHERE post_status = 'publish' AND post_type = %s ORDER BY ID DESC", $pType) 
                          );
                     }

                     foreach($results as $result) {
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result->ID ), 'single-post-thumbnail' );
                        if($image){
                              $image = $image[0];
                          } else {
                              $mkey = "_thumbnail_id";
                              $checkimg = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result->ID,$mkey) 
                              );
                              $getimg = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                              );
                              $image = $getimg[0]->guid;
                          }
                          $table_name = "wp_post_views";
                          $checkViews = $wpdb->get_results( 
                              $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result->ID) 
                          );     
                          
                          $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result->post_author));
                          $author_name = $getAuthor[0]->display_name;

                          $countPostTitle = strlen($result->post_title);
                          if($countPostTitle >= 80) {
                            $postTitle = substr($result->post_title,0, 80);
                            $postTitle .= "...";
                          } else {
                            $postTitle = $result->post_title;
                          }
                  ?>
                          <div class="col-md-12 blo">
                              <div class="">
                                 <div class="col-md-4 col-sm-4">
                                    <a href="<?php echo esc_url( get_permalink($result->ID) ); ?>">   <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                 </div>
                              </div>
                              <div class="col-md-8 col-sm-8">
                                 <p class="categtp"><strong></strong> <a href="<?php echo site_url();?>/training-placement-experiences/"> TRAINING / PLACEMENT EXPERIENCES </a></p>
                                 <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result->ID) ); ?>"> <?php echo $postTitle; ?></a>
                                 <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $result->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                                 </p>
                                 <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                                 <p class="rightwd"><strong> <?php echo get_field( "training", $result->ID); ?></strong></p>
                              </div>
                           </div>
                    <?php
                        }

                  } else{
                      $args = array(
                        'numberposts' => 25,
                        'offset' => 0,
                        'category' => 0,
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' =>'',
                        'post_type' => 'training_placement',
                        'post_status' => 'publish',
                        'suppress_filters' => true
                      );
                      
                      $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
                      //print_r($recent_posts);
                      if(!empty($recent_posts))
                      {
                        
                        foreach ($recent_posts as $value) {
                          $i=0;
                          $idd =$value["ID"];
                          $cats = get_the_category($value["ID"]);
                          $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
                          if($image){
                              $image = $image[0];
                          } else {
                              $mkey = "_thumbnail_id";
                              $checkimg = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $idd,$mkey) 
                              );
                              $getimg = $wpdb->get_results( 
                                    $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                              );
                              $image = $getimg[0]->guid;
                          }
                          
                          $table_name = "wp_post_views";
                          $checkViews = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $value['ID']) 
                          );
                          
                          $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $value['post_author']));
                           $author_name = $getAuthor[0]->display_name;

                          $countPostTitle = strlen($value['post_title']);
                          if($countPostTitle >= 80) {
                            $postTitle = substr($value['post_title'],0, 80);
                            $postTitle .= "...";
                          } else {
                            $postTitle = $value['post_title'];
                          }
                       ?>
                   <div class="col-md-12 blo">
                      <div class="">
                         <div class="col-md-4 col-sm-4">
                            <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">   <img src="<?php echo $image; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                         </div>
                      </div>
                      <div class="col-md-8 col-sm-8">
                         <p class="categtp"><strong></strong><a href="<?php echo site_url();?>/training-placement-experiences/"> TRAINING / PLACEMENT EXPERIENCES</a></p>
                         <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($value['ID']) ); ?>"> <?php echo $postTitle; ?></a>
                         <p class="leftwd"><strong>By </strong> <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $value['post_author'];?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                         </p>
                         <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> <?php if($checkViews){echo $checkViews[0]->view_count;}else{echo "0";}?>
                                  </a>        
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($value['ID']) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                         <p class="rightwd"><strong> <?php echo get_field( "training", $idd); ?></strong>  </p>
                      </div>
                   </div>
               <?php 
                  }
                  }
                }
               ?>
            </div>
            <div class="col-md-4 customsideedu">
               <div id="secondary" class="widget-area col-md-3" role="complementary">
                  <aside id="recent-posts-3" class="widget widget_recent_entries">
                     <h3 class="widget-title">LATEST inside campus story</h3>
                     <?php
                      $args1 = array(
                        'numberposts' => 5,
                        'offset' => 0,
                        'category' => 0,
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'include' => '',
                        'exclude' => '',
                        'meta_key' => '',
                        'meta_value' =>'',
                        'post_type' => 'inside_campus_story',
                        'post_status' => 'publish',
                        'suppress_filters' => true
                      );
                      
                      $recent_posts1 = wp_get_recent_posts( $args1, ARRAY_A );
                      //print_r($recent_posts);
                      if(!empty($recent_posts1))
                      {
                        
                        foreach ($recent_posts1 as $value1) {
                          $i1=0;
                          $cats1 = get_the_category($value1["ID"]);
                          $image1 = wp_get_attachment_image_src( get_post_thumbnail_id( $value1['ID'] ), 'single-post-thumbnail' );
                    
                          if($image1){
                              $image1 = $image1[0];
                          } else {
                              $mkey = "_thumbnail_id";
                            $checkimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $value1["ID"],$mkey) 
                            );
                            $getimg = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                            );
                            $image1 = $getimg[0]->guid;
                          }
                           $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $value1['post_author']));
                           $author_name = $getAuthor[0]->display_name;

                          $countPostTitle = strlen($value1['post_title']);
                          if($countPostTitle >= 30) {
                            $postTitle = substr($value1['post_title'],0, 32);
                            $postTitle .= "...";
                          } else {
                            $postTitle = $value1['post_title'];
                          }
                      ?>
                     <div class="othermemd">
                        <div class="col-md-5">
                           <div class="simg">
                              <a href="<?php echo esc_url( get_permalink($value1['ID']) ); ?>">   <img src="<?php echo $image1; ?>" class="img-thumbnail"></a>
                           </div>
                        </div>
                        <div class="col-md-7">
                           <div class="pimg">
                              <p> <strong><a href="<?php echo site_url();?>/inside-campus-story/">INSIDE CAMPUS STORY</a></strong></p>
                              <a href="<?php echo esc_url( get_permalink($value1['ID']) ); ?>">
                                 <sapn>
                                 <?php echo $postTitle; ?></span>
                              </a>
                              <span>By <?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/profile/?action=<?php echo $value1['post_author'];?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?></span>
                           </div>
                        </div>
                     </div>
                      <?php
                        }
                        }
                     ?>
                     
                  </aside>
<aside id="recent-posts-3" class="widget widget_recent_entries">
<div class="panel1">   
<h3 class="widget-title">See Also -<a href="http://edukeeda.com/success-story/">  Success Story </a></h3>
            </div>
         </aside>
               </div>
            </div>
         </div>
      </div>
      <?php while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'content', 'page' ); ?>
      <?php
         // If comments are open or we have at least one comment, load up the comment template
         if ( comments_open() || get_comments_number() ) :
           comments_template();
         endif;
         ?>
      <?php endwhile; // end of the loop. ?>
      </main><!-- #main -->
   </div>
   <!-- #primary -->
   <?php get_footer(); ?>
</div>
<style>
   div#secondary h3.widget-title{
   float: left;
   width: 100%;
   }
   .blo a.bloghdaedu {
   font-size: 20px;
   margin: 0;
   padding: 0;
   line-height: 20px;
   text-align: left;
   width: 100%;
   font-weight: 600;
   }
  
   p.leftwd {
   float: left;
   width: 100%;
   color: #9e9292;
   }
   .colh h1 {
   font-size: 40px;
   }
   a.rightwd.views {
   color: #000;
   font-size: 13px;
   float: right;
   }
   .sid input.search-field {
   width: 103%;
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   padding: 0 16px;
   }
   .fullwd{width:100%; float:left;}
   .leftwd{float:left;}
   .rightwd{float:left;}
   .pimg1 p { 
   margin-bottom: 0px;
   line-height: 18px;
   font-size: 16px;
   margin-top: 10px;
   }
   .othermemd .col-md-5{
   padding:0px;
   } 
   .othermemd .cat1 {
   text-align: center;
   padding: 0px;
   }
   .pimg1 span {
   color: #f68e2f;
   }
   .pimg strong {
   font-weight: 100;
   font-size: 13px;
   }
   .pimg sapn {
   font-size: 16px;
   line-height: 16px;}
   .cat1 {
   text-align: center;
   }
   .pimg a {
   line-height: 20px;
   }
   .pimg p {
   margin-bottom: 6px;
   }
   .pimg1 strong {
   font-weight: 500;
   }
   .pimg span {
   font-size: 13px;
   COLOR: #aa9292;
   margin-top: 4px;
   display: inline-block;
   }
   #secondary {
   background-color: #f4f4f4;
   width: 100%;
   float:left;
   }
   .blo a.bloghdaedu {
   font-size: 20px;
   margin: 0;
   padding: 0;
   line-height: 20px;
   float: left;
   width: 100%;
   font-weight: 600;
   }
   .cat {
   padding: 0;
   }
   .blo p {
   margin-bottom: 7px;
   font-size: 15px;
   text-align: left;
   }
   .othermemd {
   box-shadow: -1px 0px 4px 0 rgba(0, 0, 0, 0.07);
   float: left;
   width: 100%;
   border: 1px solid #ddd;
   padding: 10px;
   margin-bottom:20px;
   }
   h4 {
   margin: 0 0 17px 0;
   }
   .blo img.img-thumbnail {
   height: 147px;
   }
   .headarea {
   float: left;
   width: 100%;
   }
   .other a {
   float: right;
   }
   .other h4 {
   float: left;
   margin: 0 0 18px 0;
   }
   .profile ul {
   padding: 0;
   list-style-type: none;
   }
   .memb .profile h3{
   color: #828282;
   }
   .profile {
   float: left;
   width: 100%;
   text-align: center;
   background: #efeded;
   padding: 10px;
   }
   .memb .profile p{   text-align: center; float: none;}
   p.categtypeedus {
   font-size: 15px;
   color: #7f7a7a;
   }
   .memb .profile h2 {
   font-size: 21px;
   color: #060658;
   }
   .profile ul li {
   float: left;
   display: block;
   }
   .profile ul li span, .profile ul li p{
   float:left;
   font-size: 15px;
   }
   .memb .profile .fa.fa-eye{
   margin-right: 0px !important;
   }
   .profile ul li span {
   width: 185px;
   }
   .memb {
   text-align:left;
   }
   .memb h2 {
   font-size: 30px;
   padding: 0px;
   margin: 10px 0px 0px 0px;
   }
   .memb1 {
   background-color: #fff;
   border: 1px solid #ddd;
   border-radius: 4px;
   padding: 48px;
   margin-top: 0px;
   float: left;
   width: 100%;
   }
   .memb h3 {
   font-size: 17px;
   margin: 5px;
   }
   .memb p {
   margin: 0 0 0px;
   float: left;
   }
   .memb1 p{
   float: left;
   text-align: center;
   font-size: 23px;
   font-weight: 600;
   }
   .memb1 button{
   float: right;
   }
   .memb spam1{
   font-weight:600;
   } 
   .memb spam{
   font-weight:600;
   }
   .memb .fa.fa-eye {
   display: initial;
   }
   .blo {
   border-radius: 4px;
   background-color: #ffffff;
   box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.07);
   border: solid 1px #ececec;
   margin-bottom: 30px;
   padding: 10px;
   float: left;
   width: 100%;
   }
   .arteduro {
   float: left;
   width: 100%;
   margin: 30px 0;
   }
   .blo2 a {
   float: right;
   }
   .rightoption select {
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   line-height: 3px;
   padding: 1px 18px;
   margin: 0 0px 0 0px;
   }
   .rightoption p {
   float: left;
   margin: 0px 29px;
   }
   .rightoption {
   float: right;
   }
   .colh {
   text-align: left;
   }
   .sid {
   float: left;
   }
   .pimg {
   TEXT-ALIGN: LEFT;
   }
   .pimg1 {
   TEXT-ALIGN: LEFT;
   }
   @media only screen and (max-width: 767px){
   .rightwd{
   float: left;
   }
   .sid {
   float: left;
    width: 100%;
   }
   .leftwd {
   width: 100%;
   }
   .rightoption{
       width: 100%;
   }
  .rightoption select {
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   line-height: 3px;
   padding: 1px 60px;
   margin: 1px 0px 8px 0px;
       width: 70%;
    float: right;
   }
   .rightoption p {
       margin: 0px 0px;
    font-size: 20px;
    font-weight: 700;
   }
   .searchform button {
    margin: 0px 3px;
   }
   
   .searchform input {
    height: 36px!important;
    padding: 4px 46px!important;
}
 .arteduro {
   float: left;
   width: 100%;
   margin: 0px!important;
   padding:10px;
   }
   .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
}
   }
</style>

<!-- Modal -->
<div id="myModalTraining" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Follow</h4>
      </div>
      <div class="modal-body">
        <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
      </div>
    </div>
  </div>
</div>
