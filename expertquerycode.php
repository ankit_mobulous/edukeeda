<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;

    if(isset($_POST['submitexpert'])) {

        $title = trim($_POST["title"]);
        $shortDesc = $_POST["shortDesc"];
        $desc = $_POST["desc"];
	$desc = stripslashes($desc);
        unset($_SESSION['posterror']);
        $catone = $_POST["catone"];
        $cattwo = $_POST["cattwo"];
        $catthree = $_POST["catthree"];
        $userId = $_SESSION["login"]["id"];
        $post_date = date('Y-m-d H:i:s');
        $title = trim($title);
        $temp_post_title = explode(" ", $title);
  
        if(count($temp_post_title > 1)) {
          $temp_post_title = implode("-", $temp_post_title);
          $string = trim(strtolower($temp_post_title));
          $post_name = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        } else {
          $post_name = preg_replace('/[^A-Za-z0-9\-]/', '', $title);
        }
        $data = array(
                "post_author" => $userId,
                "post_date" => $post_date,
                "post_content" => $desc,
                "post_title" => $title,
                "post_status" => "pending",
                "comment_status" => "open",
                "ping_status" => "closed",
                "post_name" => $post_name,
                "post_parent" => 0,
                "menu_order" => 0,
                "post_type" => $catone,
                "comment_count" => 0
              );
        $result = $wpdb->insert("wp_posts", $data);
        
        if($result) {
          $lastid = $wpdb->insert_id;
          $siteurls = site_url();
          $guid = $siteurls."/?post_type=inside_campus_story&#038;p=$lastid";
 
          $results2 = $wpdb->update("wp_posts", array('guid'=> $guid), array('ID' => $lastid));

          $tablename2 = "wp_term_relationships";
          $data1 = array(
                  'object_id' => $lastid,
                  'term_taxonomy_id' => $cattwo,
                  'term_order' => 0
                );
          $results3 = $wpdb->insert($tablename2, $data1);

          $data2 = array(
                  'object_id' => $lastid,
                  'term_taxonomy_id' => $catthree,
                  'term_order' => 1
                );
          $results4 = $wpdb->insert($tablename2, $data2);

          $tablename3 = "wp_postmeta";
          //Description
          if(!empty($shortDesc)) {
            $data4 = array(
                    'post_id' => $lastid,
                    'meta_key' => "description",
                    'meta_value' => $shortDesc
                  );
            $results6 = $wpdb->insert($tablename3, $data4);
            $data5 = array(
                    'post_id' => $lastid,
                    'meta_key' => "_description",
                    'meta_value' => "field_5c3c77df36e92"
                  );
            $results7 = $wpdb->insert($tablename3, $data5);
          }

          if($_FILES["fileexpert"]["name"]) {
            $upload_dir  = wp_upload_dir();
            $upload_path = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

            $extension = pathinfo($_FILES["fileexpert"]["name"], PATHINFO_EXTENSION);
            $newfilename = substr(sha1(mt_rand().time()), 0, 11).".".$extension;

            $file = $upload_path . $newfilename;
            
            $raw_file_name = $_FILES['fileexpert']['tmp_name'];
            $raw_file_name1 = $_FILES['fileexpert']['name'];
            $temp_img_title = explode(" ", $raw_file_name1);
            if(count($temp_img_title > 1)) {
              $temp_img_title = implode("-", $temp_img_title);
              $string1 = trim(strtolower($temp_img_title));
              $post_name1 = preg_replace('/[^A-Za-z0-9\-]/', '', $string1);
            } else {
              $post_name1 = preg_replace('/[^A-Za-z0-9\-]/', '', $raw_file_name1);
            }

            $siteurl = site_url();
            $saveFile = $siteurl.'/wp-content/uploads/'.date('Y/m').'/'. $newfilename;

            if (move_uploaded_file($_FILES['fileexpert']['tmp_name'], $file)) { } 

              $data6 = array(
                  'post_author' => $userId,
                  'post_date'=> $post_date,
                  'post_title' => $raw_file_name1,
                  'post_status'=> "inherit",
                  'comment_status' => "open",
                  'ping_status' => "closed",
                  'post_name' => $post_name1,
                  'post_parent' => $lastid,
                  "guid" => $saveFile,
                  'menu_order' => 0,
                  'post_type' => "attachment",
                  'post_mime_type' => "image/$extension",
                  'comment_count' => 0,
              );

              $results8 = $wpdb->insert("wp_posts", $data6);
              $lastid1 = $wpdb->insert_id;
              
              $data7 = array(
                  'post_id' => $lastid,
                  'meta_key' => "_thumbnail_id",
                  'meta_value' => $lastid1
                );
              $results9 = $wpdb->insert($tablename3, $data7);

            } else {
                $saveFile = " ";
            }
             $_SESSION["postdone"] = "Your Post has been Successfully Submitted";
         header("Location: https://edukeeda.com/all-members/homepage?action=$userId");
        } else {
          header('Location: https://edukeeda.com/student-query-form/');
        }
    } else{
          header('Location: https://edukeeda.com/student-query-form/');
    }

?>
