<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
global $wpdb;

  if(isset($_POST['submitstepthree'])) {

    $school = trim($_POST["school"]);
    $degree = trim($_POST["degree"]);
    $branch = trim($_POST["branch"]);
    $grade = trim($_POST["grade"]);
    $from = trim($_POST["from"]);
    $to = trim($_POST["to"]);
    $desc = trim($_POST["desc"]);
    $userId = $_SESSION["login"]["id"];
    
    $tablename = "wp_profileStepThree";
    
    if(!empty($_POST["sid"])) {
        $sid = $_POST["sid"];
        
        $checkEdu = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_schools WHERE schoolName = %s", $school));

        if($checkEdu) {
          $wpdb->update("wp_profileStepThree", array('school' => $checkEdu[0]->id), array('id'=>$sid));
        } else {
          $compData = ["schoolName"=>$company_name];
          $wpdb->insert("wp_schools", $compData);
          $lastid = $wpdb->insert_id;
          $wpdb->update("wp_profileStepThree", array('school' => $lastid), array('id'=>$sid));
        }

        $wpdb->update("wp_profileStepThree", array('degree' => $degree), array('id'=>$sid));
        $wpdb->update("wp_profileStepThree", array('branch' => $branch), array('id'=>$sid));
        $wpdb->update("wp_profileStepThree", array('grade' => $grade), array('id'=>$sid));
        $wpdb->update("wp_profileStepThree", array('from' => $from), array('id'=>$sid));
        $wpdb->update("wp_profileStepThree", array('to' => $to), array('id'=>$sid));
        $wpdb->update("wp_profileStepThree", array('desc' => $desc), array('id'=>$sid));
        
        if(!empty($_POST["smember"])){
          header("Location: https://edukeeda.com/all-members/homepage?action=$userId");
        } else{
          $session["sessaddedu"] = "done";
          header('Location: https://edukeeda.com/edit-profile/');
        }
    
    } else {
        
        $checkEdu = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_schools WHERE schoolName = %s", $school));
        if($checkEdu) {
            $data = array(
              "user_id" => $userId,
              "school" => $checkEdu[0]->id,
              "degree" => $degree,
              "branch" => $branch,
              "grade" => $grade,
              "from" => $from,
              "to" => $to,
              "desc" => $desc
            );
        } else {
          $compData = ["schoolName"=>$school];
          $wpdb->insert("wp_schools", $compData);
          $lastid = $wpdb->insert_id;
          
          $data = array(
              "user_id" => $userId,
              "school" => $lastid,
              "degree" => $degree,
              "branch" => $branch,
              "grade" => $grade,
              "from" => $from,
              "to" => $to,
              "desc" => $desc
            );
        }
        //var_dump($data);die;
        $result = $wpdb->insert($tablename, $data);
      
        if(!empty($_POST["smember"])) {
            header("Location: https://edukeeda.com/all-members/homepage?action=$userId");
        } else{
          header('Location: https://edukeeda.com/user-profile?addeducation=Education added successfully');
        }
      }
  }

  if(isset($_POST["stepThreeDelete"])) {
      $id = $_POST["stepThreeId"];
      $wpdb->query($wpdb->prepare("DELETE FROM wp_profileStepThree WHERE id = %d", $id));
      header('Location: https://edukeeda.com/edit-profile/');
  }

?>
