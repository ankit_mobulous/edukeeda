<?php
session_start();
/**
/*
 Template Name: Start Test
 */
if($_SESSION["login"]) {

} else {
  header('Location: https://edukeeda.com/signin/');
}
$userId = $_SESSION["login"]["id"];
get_header(); ?>
<?php
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

  $eid2 = $_GET["action"];
  $sid2 = $_GET["action1"];
  $qid2 = $_GET["action2"];
  
  if(!isset($_POST["selectAnswer"])) {
    $array = array("exam_id"=>$eid2, "subject_id"=>$sid2, "quiz_id"=> $qid2, "user_id"=>$_SESSION["login"]["id"]);
    $wpdb->insert("wp_user_premium_start", $array);

    $getTimevalue = $wpdb->get_results( 
        $wpdb->prepare( "SELECT * FROM wp_premium_quiz WHERE exam_id = %d AND subject_id=%d", $eid2, $sid2) 
    );

    $tValue = (int) $getTimevalue[0]->time_limit;          
    $dateFormat = "d F Y -- g:i a";
    $targetDate = time() + ($tValue*60);//Change the 25 to however many minutes you want to countdown
    $actualDate = time();
    $secondsDiff = $targetDate - $actualDate;
    $remainingDay  = floor($secondsDiff/60/60/24);
    $remainingHour  = floor(($secondsDiff-($remainingDay*60*60*24))/60/60);
    $remainingMinutes = floor(($secondsDiff-($remainingDay*60*60*24)-($remainingHour*60*60))/60);
    $remainingSeconds = floor(($secondsDiff-($remainingDay*60*60*24)-($remainingHour*60*60))-($remainingMinutes*60));
    $actualDateDisplay = date($dateFormat,$actualDate);
    $targetDateDisplay = date($dateFormat,$targetDate);
  }
?>

<script type="text/javascript">
  var days = <?php echo $remainingDay; ?>  
  var hours = <?php echo $remainingHour; ?>  
  var minutes = <?php echo $remainingMinutes; ?>  
  var seconds = <?php echo $remainingSeconds; ?> 
  
  function setCountDown ()
  {
      seconds--;
      if (seconds < 0){
        minutes--;
        seconds = 59;
      }
      if (minutes < 0){
        hours--;
        minutes = 59;
      }
      if (hours < 0){
        days--;
        hours = 23;
      }
      document.getElementById("remain").innerHTML = hours+" hours, "+minutes+" minutes, "+seconds+" seconds";
      SD = window.setTimeout( "setCountDown()", 1000 );
      
      if (minutes == '00' && seconds == '00') { 
          seconds = "00"; 
          // window.clearTimeout(SD);
          // window.setInterval(function(){
          //     document.getElementById("formquiz").submit();
          // }, 1000);
          window.clearTimeout(SD);
          //window.alert("Time is up. Press OK to continue."); // change timeout message as required
          //document.quizforms.submit();
          document.getElementById("formquiz").submit();
      } 
  }

</script>
<style>
    ol.breadcrumb li {
    display: inline-block;
    margin: 16px 13px;
    }
    .container .jumbotron, .container-fluid .jumbotron{
    margin-left: -16px;
    margin-right: -35px;
    background-color: #4054b2;
    }
   .catchybga {
   padding: 2% 0;
   background-position: 100%;
   position: relative;
   margin-bottom: 45px;
   background: #4054b2;
       margin-top: -16px!important;
   }
   .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper
   {
	   padding:15px;
	       width: 100%;
   }
   
    .want h3 {
   color:#fff;
   text-align:center;
	}
    
	form{
    margin-top: 23px;
    }
    h3 a{
    border-radius: 1px 1px 1px 4px;
    box-shadow: 1px 1px 1px 2px;
    padding: 7px;
    }
    hr {
    margin-top: 0px!important;
    margin-bottom: 0px!important;
    border: 0;
    border-top: 1px solid #eee;
    width: 280px!important;
    }
    input[type=checkbox], input[type=radio]{
    margin: 10px 0 0!important;
    }
    @media screen and (min-width: 768px){
    .jumbotron .h1, .jumbotron h1 {
    font-size: 30px !important;
    color: white!important;
    }
    }
    .container .jumbotron, .container-fluid .jumbotron{
    border-radius: 0
    }
    .jumbotron.text-center {
    margin: 0;
    width: 100%;
    border-radius: 0px !important;
    }
    .content-area .post-wrap, .contact-form-wrap {
    padding-right: 0 !important;
    }
    .container1 {
    margin: 0 auto;
    max-width: 1200px;
    }
    .quesitionset {
    float: left;
    width: 100%;
    border-bottom: 1px solid #ddd;
    }
    p.questionset {
    font-size: 23px;
    margin: 0;
    }
    .showopn ul{list=style-type:none; padding:0;}
    .showopn ul li{display:inline-block; margin-right:10px;}
    .showopn {
    margin: 10px 0;
    }
    .answersection{
    -webkit-transition: 1s;
    transition: .2s;
    }
    .answersection, .Workspace {
    opacity: 0;
    visibility: hidden;
    -webkit-transition: 1s;
    transition: .2s;
    height: 0px;
    }
    .answersection.openansedu, .Workspace.openworksps{    opacity: 1;
    visibility: visible;
    -webkit-transition: 1s;
    transition: .2s;
    width: 45%;
    }
    .answersection.openansedu{
    overflow-y: scroll;
    height: auto;
    }
    .Workspace.openworksps{
    height:100px;
    }
    .quesitionset .correctans{
        border: 1px solid #008000;
        padding-left: 20px;
        padding-right: 20px;
		}
    .quesitionset .realans{
        border: 1px solid #008000;
        padding-left: 20px;
        padding-right: 20px;
		}
    .quesitionset .wrongans{
        border: 1px solid red;
        padding-left: 20px;
        padding-right: 20px;
		}
        #remain{
			    float: initial;
                color: #f68e2f;
               font-size: 20px;
               margin-top: 8px;
          }
		  .panel-body p {
    font-weight: 600!important;
}
.panel-heading center {
    color: #fff!important;
    font-weight: 600!important;
    font-size: 25px!important;
}
.panel-warning {
    border-color: #52c641!important;
}
.panel-warning>.panel-heading {
    color: #000!important;
    background-color: #52c641!important;
    border-color: #f68e2f!important;
}
        .redWrong{
			margin-left: 40%;
			color: #f00;
			text-transform: uppercase;
			}
        .greenCorrect{
			margin-left: 40%;
		color: darkgreen;
		text-transform: uppercase;
		}
        .jumbotron.text-center h3 {

    color: #fff;
    font-size: 28px;

}
 </style>
<div class="ajeetbaba">
   <div id="primary" class="content-area">
      <main id="main" class="post-wrap" role="main">
         <div class="catchybga">
      <div class="want">
                 <h3> Premium Test </h3>
      </div>
   </div>
		
         
         <div class="container">
            <div class="row">
			    <div class="timeleft">
               <div class="col-sm-6">
                <?php
                 
                  $eid = $_GET["action"];
                  $sid = $_GET["action1"];
                  $getexam = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_premium_exam_type WHERE id = %d", $eid) 
                  );
                  $getSub = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_premium_exam_subject WHERE exam_id = %d AND id=%d", $eid, $sid) 
                  );
                  $getTime = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_premium_quiz WHERE exam_id = %d AND subject_id=%d", $eid, $sid) 
                  );
                ?>
                 <h3> <?php echo $getexam[0]->exam_title; ?> - <?php echo $getSub[0]->subject;  ?></h3>
                 <input type="hidden" name="timer" value="<?php echo $getTime[0]->time_limit; ?>" id="gettimer">
               </div>
			   
			    <?php if(isset($_POST["selectAnswer"])) { ?>
        
        <?php } else{?>
         <div class="col-md-6">
            <div><p id="remain"> Time Left <?php echo "$remainingHour hours, $remainingMinutes minutes, $remainingSeconds seconds";?></p></div>
        </div>
        <?php } ?>
            </div>
		 </div>	
         </div>
        
          <div class="difcuttype1 tabcontent" id="">
            <div  class="container1">
               
               <div class="row">
                <?php
                  if(isset($_POST["selectAnswer"])) {
                    $eid = $_POST["eid"];
                    $qid = $_POST["qid"];
                    $cid = $_POST["cid"];
                    $sAnswer = $_POST["selectAnswer"];
                    $ans = array();
                    ?>
                      
                    <?php
                    for($x=1; $x<=$cid-1; $x++) {
                      $op = "selectAnswer$x";
                      $ans[] = $_POST[$op];
                    }
                    
                    $saveAnswer = "";
                    $attempt = 0;
                    foreach($ans as $answer1) {
                      if($answer1) {
                        $saveAnswer .= $answer1[0]."=";
                        $attempt = $attempt + 1; 
                      }
                    }
                    $answerData = array("user_id"=>$userId, "test_id"=>$eid, "quiz_id"=>$qid, "saveAnswer"=>$saveAnswer);
                    $wpdb->insert("wp_user_premium_answer", $answerData);

                    $totalQues = count($ans);
                    $cor = 0;
                    $wro = 0;
                    $preCount = 0;
                    foreach($ans as $answer1) {
                        
                        $val1 = explode("-", $answer1[0]);
                        $quizid1= $val1[1];

                        $checkAnswer1 = $wpdb->get_results( 
                              $wpdb->prepare( "SELECT * FROM wp_premium_paper WHERE exam_id = %d AND quiz_id = %d AND id=%d", $eid, $qid, $quizid1)
                        );
                        if($checkAnswer1) {
                          if($val1[0] == "A"){if($val1[0] == $checkAnswer1[0]->answer){ $cor = $cor+1; }else{ $wro = $wro+1; }}
                          if($val1[0] == "B"){if($val1[0] == $checkAnswer1[0]->answer){ $cor = $cor+1; }else{ $wro = $wro+1; }}
                          if($val1[0] == "C"){if($val1[0] == $checkAnswer1[0]->answer){ $cor = $cor+1; }else{ $wro = $wro+1; }}
                          if($val1[0] == "D"){if($val1[0] == $checkAnswer1[0]->answer){ $cor = $cor+1; }else{ $wro = $wro+1; }}
                        }
                        $preCount++;
                    }
                    $performanceData = array("user_id"=> (int)$userId, "test_id"=>(int)$eid, "quiz_id"=>(int)$qid, "totalQues"=>$totalQues, "attemptQues"=> $attempt, "wrondAnswer"=>$wro, "correctAnswer"=>$cor);
                    //var_dump($performanceData);
                    $wpdb->insert("wp_user_performance_premium", $performanceData);
                    ?>
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item active" aria-current="page"> <a href="<?php site_url();?>/premium-test-series/"> Premium Tests home page / </a></li>
                        </ol>
                      </nav>

                      <div class="panel panel-warning">
                          <?php
                            $incorr = $attempt - $cor;
                            $incorrPoint = $incorr * $getTime[0]->minus_point;
                            $corrPoint = $cor * $getTime[0]->points;
                            $finalPoint =  $corrPoint - $incorrPoint;
                          ?>
                        <div class="panel-heading"><center>Total Score: <?php echo $finalPoint;?>  / <?php echo $getResult[0]->totalQues * $getTime[0]->points;?></center></div>
                        <div class="panel-body">
                          <p>Marks for Correct Answers : <?php echo $getTime[0]->points;?></p>
                          <p>Negative Marks for Wrong Answers : <?php echo $getTime[0]->minus_point;?></p>
                          <p>Total Number of Questions : <?php echo $totalQues;?></p>
                          <p>Number of Answered Questions : <?php echo $attempt;?></p>
                          <p>Number of Incorrect Questions : <?php echo $incorr; ?></p>
                          <p>Number of Unanswered Questions : <?php echo $unanswer = $totalQues-$attempt; ?></p>
                        </div>
                      </div>
                    <?php
                    $i=1;$y=0;
                    foreach($ans as $answer) {
                        if($answer == NULL) {
                          $val[0] = NULL;
                          $quizid= (int) $sAnswer[$y];
                            $checkAnswer = $wpdb->get_results( 
                                $wpdb->prepare( "SELECT * FROM wp_premium_paper WHERE exam_id = %d AND quiz_id = %d AND id=%d", $eid, $qid, $quizid)
                            );
                        } else{
                            
                            $val = explode("-", $answer[0]);
                            $quizid= $val[1];

                            $checkAnswer = $wpdb->get_results( 
                                  $wpdb->prepare( "SELECT * FROM wp_premium_paper WHERE exam_id = %d AND quiz_id = %d AND id=%d", $eid, $qid, $quizid)
                            );
                        }

                  ?>
                        <div class="col-sm-12 quesitionset" >
                            <p class="questionset"><?php echo $i;?>. <?php echo $checkAnswer[0]->question_title ;?>?<br></p>

                            A. <span class="optiona <?php if($val[0] == "A"){if($val[0] == $checkAnswer[0]->answer){echo "correctans";}else if($val[0] == NULL){}else{ echo "wrongans";}} ?> <?php if($val[0] != "A"){if($checkAnswer[0]->answer == "A"){ echo "realans";}} ?>"> <?php echo $checkAnswer[0]->optiona; ?> </span> <?php if($val[0] == "A"){if($val[0] == $checkAnswer[0]->answer){echo "<span class='greenCorrect'>Correct</span>";}else if($val[0] == NULL){}else{ echo "<span class='redWrong'>wrong</span>";}} ?> <BR>

                            B. <span class="optionb <?php if($val[0] == "B"){if($val[0] == $checkAnswer[0]->answer){echo "correctans";}else if($val[0] == NULL){}else{ echo "wrongans";}} ?> <?php if($val[0] != "B"){if($checkAnswer[0]->answer == "B"){ echo "realans";}} ?>"><?php echo $checkAnswer[0]->optionb; ?></span> <?php if($val[0] == "B"){if($val[0] == $checkAnswer[0]->answer){echo "<span class='greenCorrect'>Correct</span>";}else if($val[0] == NULL){}else{ echo "<span class='redWrong'>wrong</span>";}} ?><BR>
                            <?php
                              if($checkAnswer[0]->choice != 2) {
                            ?>
                            C. <span class="optionc <?php if($val[0] == "C"){if($val[0] == $checkAnswer[0]->answer){echo "correctans";}else if($val[0] == NULL){}else{ echo "wrongans";}} ?> <?php if($val[0] != "C"){if($checkAnswer[0]->answer == "C"){ echo "realans";}} ?>"><?php echo $checkAnswer[0]->optionc; ?></span> <?php if($val[0] == "C"){if($val[0] == $checkAnswer[0]->answer){echo "<span class='greenCorrect'>Correct</span>";}else if($val[0] == NULL){}else{ echo "<span class='redWrong'>wrong</span>";}} ?><BR>

                            D. <span class="optiond <?php if($val[0] == "D"){if($val[0] == $checkAnswer[0]->answer){echo "correctans";}else if($val[0] == NULL){}else{ echo "wrongans";}} ?> <?php if($val[0] != "D"){if($checkAnswer[0]->answer == "D"){ echo "realans";}} ?>"><?php echo $checkAnswer[0]->optiond; ?></span> <?php if($val[0] == "D"){if($val[0] == $checkAnswer[0]->answer){echo "<span class='greenCorrect'>Correct</span>";}else if($val[0] == NULL){}else{ echo "<span class='redWrong'>wrong</span";}} ?><BR>
                            <?php
                              }
                            ?>
                            <hr>
                            <br>
                        </div>

                        <?php
                        $i++;$y++;
                    }

                  } else {
                      $action = $_GET["action"];
                      $action1 = $_GET["action1"];
                      $action2 = $_GET["action2"];
                  ?>
                  <form method="POST" action="" name="quizforms" id="formquiz">
                  <?php

                    $getPremiums = $wpdb->get_results( 
                        $wpdb->prepare( "SELECT * FROM wp_premium_paper WHERE exam_id = %d AND quiz_id = %d", $action,$action2)
                    );
                    $i=1;
                    foreach($getPremiums as $getPremium) {
                ?> 
                  <div class="col-sm-12 quesitionset" >
                        <p class="questionset"><?php echo $i;?>. <?php echo $getPremium->question_title ;?>?<br></p>

                        <input type="radio" name="selectAnswer<?php echo $i;?>[]" class="a<?php echo $i;?>" value="A-<?php echo $getPremium->id ;?>"> A. <span class="optiona<?php echo $i;?>"><?php echo $getPremium->optiona; ?></span> <BR>

                        <input type="radio" name="selectAnswer<?php echo $i;?>[]" class="b<?php echo $i;?>" value="B-<?php echo $getPremium->id ;?>"> B. <span class="optionb<?php echo $i;?>"><?php echo $getPremium->optionb; ?></span> <BR>
                        <?php
                          if($getPremium->choice != 2) {
                        ?>
                        <input type="radio" name="selectAnswer<?php echo $i;?>[]" class="c<?php echo $i;?>" value="C-<?php echo $getPremium->id ;?>"> C. <span class="optionc<?php echo $i;?>"><?php echo $getPremium->optionc; ?></span> <BR>

                        <input type="radio" name="selectAnswer<?php echo $i;?>[]" class="d<?php echo $i;?>" value="D-<?php echo $getPremium->id ;?>"> D. <span class="optiond<?php echo $i;?>"><?php echo $getPremium->optiond; ?></span> <BR>
                        

                        <?php
                          }
                        ?>
                        <input type="hidden" name="selectAnswer[]" value="<?php echo $getPremium->id ;?>">
                        <div class="showopn">
                           <ul>
                              <li>
                                 <p class="worksps"><strong>Work Space</strong></p>
                              </li>
                           </ul>
                        </div>
                        <div class="Workspace"><textarea></textarea></div>
                       
                        <br>

                  </div>
                  <?php
                        $i++;
                    }
                  }
                  ?>
               </div>
               <?php  if(isset($_POST["selectAnswer"])) {}else{?>
               <div class="row" style="margin-bottom: 40px;">
                  <div class="col-md-12">
                      <center><input type="submit" name="submitquiz" value="Submit" class="btn btn-lg btn-warning" id="formsubmit"></center>
                  </div>
               </div> 
                  <input type="hidden" name="eid" value="<?php echo $action;?>">
                  <input type="hidden" name="qid" value="<?php echo $action2;?>">
                  <input type="hidden" name="cid" value="<?php echo $i;?>">
                  <?php }?>
               </form>
            </div>
         </div>
  </div>
  </main><!-- #main  -->

</div>
<!-- #primary -->
<script>
   function openCity(evt, cityName) {
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("tabcontent");
     for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
     }
     tablinks = document.getElementsByClassName("tablinks");
     for (i = 0; i < tablinks.length; i++) {
         tablinks[i].className = tablinks[i].className.replace(" active", "");
     }
     document.getElementById(cityName).style.display = "block";
     evt.currentTarget.className += " active";
   }
   
   
   jQuery(".showans1").click(function (e) {
     e.stopPropagation();
     jQuery(".answersection").toggleClass('openansedu');
   });
   
   jQuery(".worksps").click(function (e) {
     e.stopPropagation();
     jQuery(".Workspace").toggleClass('openworksps');
   });
   
</script>
<script type="text/javascript">
   function myFunction(obj,id){
      var option = id;
      var clickOption = $(obj).attr('class');
      var checkOption = ".option"+clickOption;

      var ans = $(checkOption).html();
      console.log(id);
      console.log(ans);

      if(id == ans){
         $(checkOption).css('color','green');
      } else{
         $(checkOption).css('color','red');
      }
   }

   $(document).ready(function(){
      $(".tablinks").on('click',function(){
            $(".col-sm-12.quesitionset span").css('color','black');
      });
   });
</script>
<?php get_footer(); ?>
</div>

