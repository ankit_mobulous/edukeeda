<?php
   /**
   /*
    Template Name: Home Page interview
    */
   get_header(); ?>
<style type="text/css">
  .page-template header.entry-header {
    display: block;
  }
   .entry-thumb img {
    width: 100%;
    height: 400px;
   }
   .customsideedu h3.widget-title {
   border: 1px solid #ffaf8c;
   padding: 6px 0px 4px 0;
   margin: 0 0 10px 0;
   border-left: none;
   border-right: none;
   font-size: 16px;
   line-height: normal;
   font-weight: 600;
   text-transform: uppercase;
   }
   .pimg span {
   font-size: 16px!important;
   line-height: 16px;
   }
</style>
<?php if (get_theme_mod('fullwidth_single')) { //Check if the post needs to be full width
   $fullwidth = 'fullwidth';
   } else {
   $fullwidth = '';
   } ?>
<?php do_action('sydney_before_content'); ?>
<div id="primary" class="content-area col-md-9 <?php echo $fullwidth; ?>">
   <main id="main" class="post-wrap" role="main">
<?php
   $bid = $_GET['action'];
   global $wpdb;
   $table_name = "wp_post_views";
   $checkViews = $wpdb->get_results( 
         $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $bid) 
   );
   if($checkViews){
       $valView = $checkViews[0]->view_count + 1;
       $wpdb->update($table_name, array('view_count'=>$valView), array('post_id'=>$bid));
   } else {
       $data = array("post_id" =>$bid,"view_count"=>1);
       $wpdb->insert($table_name, $data);
   }

   $table_name1 = "wp_posts";
   $getPost = $wpdb->get_results( 
         $wpdb->prepare( "SELECT * FROM $table_name1 WHERE ID = %d", $bid) 
   );
  $image = wp_get_attachment_image_src( get_post_thumbnail_id( $bid ), 'single-post-thumbnail' );
  $author_name = get_the_author_meta( 'display_name', $getPost[0]->post_author );
?>
      <article id="post-<?php echo $bid; ?>">
         <header class="entry-header">
            <div class="meta-post">
               <span> <a href="<?php site_url();?>/interviewhome/"> Interview </a></span>
            </div>
            <h1 class="title-post entry-title"> <?php echo $getPost[0]->post_title; ?></h1>
            <div class="single-meta" style="width: 98%;">
               <span class="byline">
               <span class="author vcard"> <?php echo $author_name; ?></span> | <span class="author vcard"> <?php echo get_the_time('d-M-Y', $bid); ?></span>
               </span>
               <span style="float:right;"> </span>
            </div>
            <!-- .entry-meta -->
         </header>
         <!-- .entry-header -->
         <!--<div class="entry-content">
            </div>-->
         <div class="row">
            <div class="col-md-12">
               <?php echo get_field( "big_description", $bid); ?>
            </div>
         </div>
         <div class="entry-thumb">

            <div class="col-md-12">
               <div class="entry-thumb">
                  <img src="<?php echo $image[0]; ?>" class="attachment-large-thumb size-large-thumb wp-post-image" alt="">
               </div>
            </div>
            
         </div>
         <div style="clear: both;"></div>
         <div class="entry-content">
            <?php echo $getPost[0]->post_content; ?>
         </div>
         <!-- .entry-content -->
      </article>
   </main>
   <!-- #main -->
</div>
<!-- #primary -->
<?php do_action('sydney_after_content'); ?>
<?php if ( get_theme_mod('fullwidth_single', 0) != 1 ) {
   ?>
<div class="col-md-3 customsideedu">
   <aside id="recent-posts-3" class="widget widget_recent_entries">
      <h3 class="widget-title">Latest Interviews</h3>
      <?php
         $args1 = array(
           'numberposts' => 3,
           'offset' => 0,
           'category' => 0,
           'orderby' => 'post_date',
           'order' => 'DESC',
           'include' => '',
           'exclude' => '',
           'meta_key' => '',
           'meta_value' =>'',
           'post_type' => 'big_interview',
           'post_status' => 'publish',
           'suppress_filters' => true
         );
         
         $recent_posts1 = wp_get_recent_posts( $args1, ARRAY_A );
         //print_r($recent_posts);
         if(!empty($recent_posts1))
         {
           
           foreach ($recent_posts1 as $value1) {
             $i1=0;
            $cats1 = get_the_category($value1["ID"]);
             $image1 = wp_get_attachment_image_src( get_post_thumbnail_id( $value1['ID'] ), 'single-post-thumbnail' );
         
          ?>
      <div class="othermemd">
         <div class="col-md-5">
            <div class="simg">
               <a href="<?php echo esc_url( get_permalink($value1['ID']) ); ?>">   <img src="<?php echo $image1[0]; ?>" class="img-thumbnail"></a>
            </div>
         </div>
         <div class="col-md-7">
            <div class="pimg">
               <p> <strong>Success story</strong></p>
               <a href="<?php echo esc_url( get_permalink($value1['ID']) ); ?>">
               <span>
               <?php echo !empty($value1['post_title'])?$value1['post_title']:""; ?></span>
               </a>
               <span>By Admin</span>
            </div>
         </div>
      </div>
      <?php
         }
         }
         ?>
   </aside>
</div>
<?php
   } ?>
<?php get_footer(); ?>

