<div class="eventdetails">
<?php
session_start();
/**
/*
Template Name: Home Page eventdetails
*/
if($_SESSION["login"]) {

} else {
  header('Location: https://edukeeda.com/signin/');
}
global $wpdb;
get_header(); ?>

<script type="text/javascript">
    $(document).ready(function(){
      $("#catone").on("change", function(){
            var cid = $(this).val();

            $.ajax({
                type: "POST",
                dataType: "json",
                url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_event_cat_list.php",
                data:{cid:cid},
                success : function(data) {
                    $("#cattwo").css("display","block");
                    $("#cattwo").html(data);                
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(textStatus, errorThrown);
                }
            });
      });
  });
</script>
   <div class="col-md-8">
      <div class="after">
         <h1> Place of Event  </h1>
      </div>
      <form method="post" action="<?php echo site_url(); ?>/wp-content/themes/sydney/eventquerycode11.php" enctype="multipart/form-data" name="myForms11">
      <div class="kol">
          <div class="ss">
             <div class="col-md-5">
                <lable> Address:  </lable>
             </div>
             <div class="col-md-7">
                <input type="text" name="address" id="name" placeholder="Address"> 
             </div>
          </div>
       </div>

      <div class="kol">
         <div class="details">
            <h3></h3>
            <div class="col-md-5">
               <lable> Country/Region:  </lable>
            </div>
            <div class="col-md-7">
               <select name="country" id="countryselect">
                  <option value=""> Select </option>
                  <?php
                    $getCountries = $wpdb->get_results("SELECT * FROM countries");
                    foreach($getCountries as $getCountrie) {
                  ?>
                  <option value="<?php echo $getCountrie->id; ?>" <?php if($getStepTwo[0]->country == $getCountrie->id){ echo "selected";}?> > <?php echo $getCountrie->name; ?></option>
                  <?php
                    }
                  ?>
               </select>
            </div>
         </div>
      </div>
      
      <div class="kol">
        <div class="ss">
           <div class="col-md-5">
              <lable> State:  </lable>
           </div>
           <div class="col-md-7">
              <select name="state" id="stateselect">
                <option value=""> Select </option>
              </select> 
           </div>
        </div>
     </div>

     <div class="kol">
          <div class="ss">
             <div class="col-md-5">
                <lable> City:  </lable>
             </div>
             <div class="col-md-7">
                <input type="text" name="city" id="city" placeholder="City">
             </div>
          </div>
       </div>

      <div class="">
         <div class="kol">
            <div class="ss">
               <div class="col-md-5">
                  <lable> Zipcode/Pincode:  </lable>
               </div>
               <div class="col-md-7">
                  <input type="text" name="zipcode" id="name" placeholder="Zipcode"> 
               </div>
            </div>
         </div>
      </div>
        <div class="col-md-8">
            <button type="button" class="btn btn-warning btn-lg stepfirst" style="float: right;"> Next </button>
        </div>
    </div> 

    <div class="col-md-8 secondstep" style="display: none;">
      <div class="after">
         <h1> Event Category </h1>
      </div>
      <div class="kol">
        <div class="col-md-5">
           <lable> What will be Event about:  </lable>
        </div>
        <div class="col-md-7">
           <select name="eventCat" id="catone">
              <option value=""> Select Category</option>
              <?php
                 $categories = get_categories( array(
                     'child_of'            => 0,
                     'current_category'    => 0,
                     'parent'    => 0,
                     'depth'               => 0,
                     'echo'                => 1,
                     'exclude'             => '',
                     'exclude_tree'        => '',
                     'feed'                => '',
                     'feed_image'          => '',
                     'feed_type'           => '',
                     'hide_empty'          => 0,
                     'hide_title_if_empty' => false,
                     'hierarchical'        => true,
                     'order'               => 'ASC',
                     'orderby'             => 'name',
                     'separator'           => '<br />',
                     'show_count'          => 0,
                     'show_option_all'     => '',
                     'show_option_none'    => __( 'No categories' ),
                     'style'               => 'list',
                     'taxonomy'            => 'events_categories',
                     'title_li'            => __( 'Categories' ),
                     'use_desc_for_title'  => 1,
                 ) );
                 
                if(!empty($categories))
                {
                  foreach ($categories as $terms) {
                    $term_id = $terms->term_id;
              ?>
                  <option value="<?php echo $term_id; ?>"> <?php echo $terms->name; ?> </option>
              <?php 
                     }
                  }
              ?>
           </select>

           <select name="eventCat1" style="display: none;margin-top: 20px;" id="cattwo" required>
              <option value=""> Select Subcategory </option>
           </select>
        </div>
      </div>
        <div class="col-md-8">
            <button type="button" class="btn btn-warning btn-lg stepsecond" style="float: right;"> Next </button>
        </div>
    </div>

    <div class="col-md-8 stepthird" style="display: none;">
      <div class="after">
         <h1> Details of Event  </h1>
      </div> 
      <div class="kol">
         <div class="col-md-5">
            <div class="ch">
               <lable> Name Of the event:  </lable>
            </div>
         </div>
         <div class="col-md-7">
            <div class="ch">
               <input type="text" name="eventname" id="name" placeholder="Event name"> 
            </div>
         </div>
      </div>
      <div class="kol">
         <div class="col-md-5">
            <div class="ch">
               <p>From:</p>
               <div class="form-group">
                  <div class='input-group date' id='datetimepicker1'>
                     <input type='date' name="from" class="form-control" />
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-7">
            <div class="ch">
               <p>To:</p>
               <div class="form-group">
                  <div class='input-group date' id='datetimepicker1'>
                     <input type='date' name="to" class="form-control"/>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="kol">
         <div class="col-md-5">
            <lable> Event Related Pic/Poster<BR> (Min 50 KB, Max 1 MB):  </lable>
         </div>
         <div class="col-md-7">
            <input type="file" name="eventpic" id="eventimg" accept="image/jpeg,image/jpg" required="required"><br>
         </div>
      </div>
      <div class="kol">
         <div class="col-md-5">
            <lable> Any official/Authentic Link:  </lable>
         </div>
         <div class="col-md-7">
            <div class="ss">
               <input type="text" name="eventlink" id="name" placeholder="Event Official Link"> 
            </div>
         </div>
      </div>
         <div>
            <div class="kol">
               <div class="col-md-5">
                  <lable> Hosted By:  </lable>
               </div>
               <div class="col-md-7">
                  <div class="col-md-6">
                     <div class="form-check">
                        <input type="text" name="hosted" id="name" placeholder="Hosted By"> 
                     </div>
                  </div>
                  <div class="col-md-6">
                  </div>
               </div>
            </div>
            <div class="col-md-12">
               <div class="desci">
                  <div class="form-group">
                     <label for="exampleFormControlTextarea1">About Event (Who should join & what event will do)</label>

                     <?php
                      $my_content1 =" ";
                      $editor_id1 = 'desc';
                      $option_name1 ='desc';
                      wp_editor($my_content1, $editor_id1, array('wpautop'=> false, 'textarea_name' => $option_name1, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> false));
                      ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
        <div class="col-md-8">
            <button type="button" class="btn btn-warning btn-lg fourstep style="float: right;"> Next </button>
        </div>
    </div>  
    <div class="col-md-8 stepfive" style="display: none;">
        <center><input type="submit" name="submit" value="Submit" class="btn btn-warning"></center>
    </div>
    </form>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>
<style>
  .gj-picker.gj-picker-md.timepicker{top: 155.5px!important;}
   .abo h2 {
   font-size: 25px;
   }
   .after h1{
   font-size: 25px;
   margin: 0 0 0 0;
   border-bottom: 1px solid #000050;
   }
   .kol {
   float: left;
   width: 100%;
   margin-bottom: 13px;
   }
   .kol input[type="text"], .kol input[type="date"], .kol select {
   width: 100%;
   padding: 0 !important;
   height: 30px;
   box-shadow: none;
   border-radius: 0px;
   background: transparent;
   margin-bottom: 0;
   padding: 1px 8px !important;
   font-size: 15px;
   }
   .ch input[type="text"], .kol input[type="date"], .kol select{
   width: 100%;
   padding: 0 !important;
   height: 30px;
   box-shadow: none;
   border-radius: 0px;
   background: transparent;
   margin-bottom: 0;
   padding: 1px 8px !important;
   font-size: 15px;
   }
   .ss input[type="text"], .kol input[type="date"]{
   width: 100%;
   padding: 0 !important;
   height: 30px;
   box-shadow: none;
   border-radius: 0px;
   background: transparent;
   margin-bottom: 0;
   padding: 1px 8px !important;
   font-size: 15px;
   }
   .ch input.form-control {
   height: 30px;
   }
   .form{
   margin-bottom: 30px;
   }
   lable {
   color: #060658;
   }
   .che label {
   display: inline-block;
   max-width: 100%;
   margin-bottom: 5px;
   font-weight: 400;
   color: #060658;
   }
   .abo h3 {
   font-size: 25px;
   font-weight: 600;
   }
   .details h3 {
   font-size: 20px;
   font-weight: 600;
   }
   .after{
   margin: 0px 0 20px 0;
   }
   .From select {
   height: auto;
   width: 100%;
   }.From select {
   height: auto;
   width: 100%;
   }
   .to select {
   height: auto;
   width: 100%;
   }
   .input-group {
   position: relative;
   display: table;
   border-collapse: separate;
   width: 100%;
   }
   .lol input.form-control {
   height: auto;
   }
   .ss {
   margin-bottom: 10px;
   }
   @media only screen and (max-width: 767px)
   {
   .desci {
   float: left;
   width: 100%;
   }
   .che {
   float: left;
   width: 100%;
   }
   .abo {
   float: left;
   width: 100%;
   }
   .side
   {
   float: left;
   width: 100%;
   }
   .desci {
   text-align: center;
   }
   }
</style>

<script type="text/javascript">
  $("#countryselect").on('change',function(e){
    var cid =  $(this).val();
    $.ajax({
          type: "POST",
          dataType: "json",
          url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_state_name.php",
          data:{cid:cid},
          success : function(data) {           
              $("#stateselect").html(data);
              //console.log(data);
          },
          error: function(jqXHR, textStatus, errorThrown){
              console.log(textStatus, errorThrown);
          }
      });
 });
</script>
<script type="text/javascript">
  $(document).ready(function(){
      $(".stepfirst").on("click", function(){
          var a = document.forms["myForms11"]["country"].value;
          var b = document.forms["myForms11"]["city"].value;
          var c = document.forms["myForms11"]["zipcode"].value;
          var d = document.forms["myForms11"]["address"].value;

          if (a == "") {
            alert("Country must be filled out");
            return false;
          } else if (b == "") {
            alert("City must be filled out");
            return false;
          } else if (c == "") {
            alert("Zipcode must be filled out");
            return false;
          } else if (c == "") {
            alert("Zipcode must be filled out");
            return false;
          } else if (d == "") {
            alert("Address must be filled out");
            return false;
          } else {
              $(".secondstep").css("display", "block");
              $(".stepfirst").css("display", "none");
          }
      });

      $(".stepsecond").on("click", function(){
          var e = document.forms["myForms11"]["eventCat"].value;
          
          if (e == "") {
            alert("Event Category must be filled out");
            return false;
          } else {
              $(".stepthird").css("display", "block");
              $(".stepsecond").css("display", "none");
          }
      });
      $(".fourstep").on("click", function(){
          var f = document.forms["myForms11"]["eventname"].value;
          var h = document.forms["myForms11"]["from"].value;
          var i = document.forms["myForms11"]["to"].value;
          var j = document.forms["myForms11"]["eventpic"].value;
          var k = document.forms["myForms11"]["eventlink"].value;
          var l = document.forms["myForms11"]["hosted"].value;
          var m = document.forms["myForms11"]["desc"].value;

          if (f == "") {
            alert("Event Name must be filled out");
            return false;
          } else if (h == "") {
            alert("Event Date From must be filled out");
            return false;
          } else if (i == "") {
            alert("Event Date To must be filled out");
            return false;
          } else if (j == "") {
            alert("Event Picture must be filled out");
            return false;
          } else if (k == "") {
            alert("Event official link must be filled out");
            return false;
          } else if (m == "") {
            alert("Event Description must be filled out");
            return false;
          } else {
              $(".stepfive").css("display", "block");
              $(".fourstep").css("display", "none");
          }
      });
  });
</script>
<script>
    $('#timepicker').timepicker();
</script>
<script>
    $('#timepicker1').timepicker();
</script>
<script type="text/javascript">
   var uploadField = document.getElementById("eventimg");
   uploadField.onchange = function() {
       if(this.files[0].size > 1024000) {
          alert("File size is greater than 1MB");
          this.value = "";
       
       } else if(this.files[0].size < 51200) {
            alert("File size is less than 50kb");
            this.value = "";
       }
   };
</script>a
