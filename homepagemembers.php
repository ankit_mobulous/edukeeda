<div class="homepagemenedu">


<?php
/**
/*
 Template Name: Home Page Members
 */

get_header(); ?>


<div class="memberpage">
  <div class="row">
    <div class="col-sm-3">
    <div class="memb">
		<div class="profile">
			 <img src="/img/user.jpg" class="img-thumbnail" alt="Cinque Terre" width="304" height="236">
			  <h2>Sanjay kumar</h2>
				<p class="categtypeedus">Education Experts with 23 Years of Experience.</p>
    <div class="seperation">
    </div>
				<span><i class="fa fa-eye"></i>No of Views:<strong>10</strong></span>
         <span>Number of Articles/Blogs:<strong>50</strong></span>
		</div>
    </div>
    </div>

    <div class="col-sm-6">
    <div class="memb1">
      <p>Want to post article/blogs/events?</p>
      <button>Click Here</button>
      </div>
    <div class="arteduro">
  <div class="row">
  
	<div class="col-md-12 col-sm-8">
		<?php
$args = array(
  'numberposts' => 5,
  'offset' => 0,
  'category' => 0,
  'orderby' => 'post_date',
  'order' => 'DESC',
  'include' => '',
  'exclude' => '',
  'meta_key' => '',
  'meta_value' =>'',
  'post_type' => 'post',
  'post_status' => 'draft, publish, future, pending, private',
  'suppress_filters' => true
);

$recent_posts = wp_get_recent_posts( $args, ARRAY_A );
//print_r($recent_posts);
if(!empty($recent_posts))
{
  
  foreach ($recent_posts as $value) {
    $i=0;
   $cats = get_the_category($value["ID"]);
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
  
                       
 ?>

   <div class="col-md-12 blo">
   

   <div class="">
       <div class="col-md-4 col-sm-4">
         <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>">   <img src="<?php echo $image[0]; ?>" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
       
       </div>
      </div>

       <div class="col-md-8 col-sm-8"> 
		 <p class="categtp"><strong></strong><?php  echo $cats[$i]->name; ?></p>
         <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($value['ID']) ); ?>"> <?php echo !empty($value['post_title'])?$value['post_title']:""; ?></a>
          <p class="fullwd"> <?php echo !empty($value['post_content'])?substr($value['post_content'], 0,60).'...':""; ?></p>
          <p class="leftwd"><strong>By </strong> <?php the_author_meta( 'display_name', $value['post_author'] ); ?> 
</p>
          
                  
           <a href="#" class="rightwd views"><i class="fa fa-eye"></i> <?php echo wpb_get_post_views(get_the_ID()); ?></a>
		    <p class="rightwd"><strong> </strong> <?php the_time('F jS, Y'); ?></p>
       </div>
    </div>

<?php 
}
}
 ?>

	</div>
	
	 
	
	
  </div>







			
				

<article id="post-1801" class="post-1801 page type-page status-publish hentry">
	<header class="entry-header">
		<h1 class="title-post entry-title">members</h1>	</header><!-- .entry-header -->

	<div class="entry-content">
				<div class="elementor elementor-1801">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
							<section data-id="3a7a8ddb" class="elementor-element elementor-element-3a7a8ddb elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="705836b8" class="elementor-element elementor-element-705836b8 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="46a97739" class="elementor-element elementor-element-46a97739 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix">
</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
					</div><!-- .entry-content -->

	<footer class="entry-footer">
		<span class="edit-link"><a class="post-edit-link" href="https://www.mobulous.app/edukeedademoweb/wp-admin/post.php?post=1801&amp;action=edit">Edit</a></span>	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

				
			
		<!-- #main -->
	</div></div><div class="col-md-3 col-sm-4" style="
    padding: 7%;
">
</div>



<div class="col-md-3 col-sm-4">
         <div class="headarea">
         <div class="other">
            <h4>Top Contributors</h4>
             <a href="https://www.mobulous.app/edukeedademoweb/allprofile/">View all</a>
             </div>
         </div>
  
  <div class="othermemd">
       <div class="col-md-4">
       <div class="simg">
         <a href="https://www.mobulous.app/edukeedademoweb/memberauthors/">   <img src="https://www.mobulous.app/edukeedademoweb/wp-content/uploads/2018/10/pexels-photo-539694.jpeg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
        </div>
       </div>
    

       <div class="col-md-8"> 
       <div class="pimg">  
         <a href="https://www.mobulous.app/edukeedademoweb/memberauthors/"> Nilesh Mishra</a>
          <p> <strong>Corporate Professional</strong></p>
          </div>
       </div>
   </div>
   
    <div class="othermemd">
       <div class="col-md-4">
       <div class="simg">
         <a href="https://www.mobulous.app/edukeedademoweb/memberauthors/">   <img src="https://www.mobulous.app/edukeedademoweb/wp-content/uploads/2018/10/pexels-photo-539694.jpeg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
        </div>
       </div>
    

       <div class="col-md-8"> 
       <div class="pimg">  
         <a href="https://www.mobulous.app/edukeedademoweb/memberauthors/"> Nilesh Mishra</a>
          <p> <strong>Corporate Professional</strong></p>
          </div>
       </div>
   </div>
   
   
    <div class="othermemd">
       <div class="col-md-4">
       <div class="simg">
         <a href="https://www.mobulous.app/edukeedademoweb/memberauthors/">   <img src="https://www.mobulous.app/edukeedademoweb/wp-content/uploads/2018/10/pexels-photo-539694.jpeg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
        </div>
       </div>
    

       <div class="col-md-8"> 
       <div class="pimg">  
         <a href="https://www.mobulous.app/edukeedademoweb/memberauthors/"> Nilesh Mishra</a>
          <p> <strong>Corporate Professional</strong></p>
          </div>
       </div>
   </div>
  
	</div>
  </div>
  
  <!-- #primary -->

	

			</div>
			
			
			

<?php get_footer(); ?>

<style>

p.leftwd {
    float: left;
    width: 80%;
}
p.leftwd {
    font-size: 16px;
    font-weight: 600;
    text-transform: capitalize;
}
.seperation {
    border-bottom: 1px solid #f68e2f;
    padding: 2px 0;
    float: left;
    width: 100%;
    margin: 6px 0;
}
p.categtypeedus {
    margin: 5px 0 0 0;
}

.profile span{
  display:block;s
}
.profile span {
    display: block;
    float: left;
    margin-left: 40px;
}

.fullwd{width:100%; float:left;}
.leftwd{float:left;}
.rightwd {
    float: left;
}
.homepagemenedu .container{width:100%; }
.blo a.bloghdaedu {
    font-size: 20px;
    margin: 0;
    padding: 0;
    line-height: 20px;
    float: left;
    width: 100%;
    font-weight: 600;
	margin-top:17px;
	
	
}
.cat {
    padding: 0;
}

.blo p {
    margin-bottom: 0px;
    font-size: 15px;
}

.profile img {
    border-radius: 100%;
    width: 80px;
    margin: 0 auto;
    float: none;
    height: 80px;
}

.othermemd {
    box-shadow: -1px 0px 4px 0 rgba(0, 0, 0, 0.07);
    float: left;
    width: 100%;
    border: 1px solid #ddd;
    padding: 10px;
	margin-bottom:20px;
}

h4 {
    margin: 0 0 17px 0;
}

.blo img.img-thumbnail {
    height: 147px;
}


.headarea {
    float: left;
    width: 100%;
}

.other a {
    float: right;
}

.other h4 {
    float: left;
    margin: 0 0 18px 0;
}

.profile ul {
    padding: 0;
    list-style-type: none;
}

.memb .profile h3{
	color: #828282;
}
.profile {
    float: left;
    width: 100%;
    text-align: center;
        background: #efeded;
    padding: 10px;
}

.memb .profile p{  
 text-align: center;
  float: none;
line-height: 17px;
    font-weight: 600;
    color: #4c4242;
}

p.categtypeedus {
    font-size: 15px;
    color: #7f7a7a;
}

.memb .profile h2 {
    font-size: 21px;
    color: #060658;
}

.profile ul li {
    float: left;
    display: block;
}
.profile strong {
    font-weight: 600;
    margin: 6px;
}

.profile ul li span, .profile ul li p{
	float:left;
	    font-size: 15px;
}

.memb .profile .fa.fa-eye{
	margin-right: 0px !important;
}

.profile ul li span {
    width: 185px;
}

.memb {
    text-align:left;
    
}
.memb h2 {
    font-size: 30px;
    padding: 0px;
    margin: 10px 0px 0px 0px;
}

.memb1 {
    background-color: #fff;
    border: 1px solid #ddd;
    border-radius: 4px;
    padding: 48px;
    margin-top: 0px;
    float: left;
	width: 100%;
}

.memb h3 {
    font-size: 17px;
    margin: 5px;
   
}
.memb p {
    margin: 0 0 0px;
	    float: left;
}
  .memb1 p{
    float: left;
    text-align: center;
   font-size: 23px;
    font-weight: 600;
}
.memb1 button{
    float: right;
}
.memb spam1{
	    
	    font-weight:600;
} 
 .memb spam{
	   
	    font-weight:600;
}
 .memb .fa.fa-eye {
    display: initial;
}

.blo {
    border-radius: 4px;
    background-color: #ffffff;
    box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.07);
    border: solid 1px #ececec;
    margin-bottom: 30px;
	padding: 10px;
	float: left;
	width: 100%;
}

.blo p {
    margin-bottom: 0px;
}

.arteduro {
    float: left;
    width: 100%;
    margin: 30px 0;
}


@media only screen and (max-width: 780px)
{
 .memberpage {
    margin-top: 180px;
       
} 
.memb1 button {
    float: left;
    width: 100%;
}
.memberpage h4 {
    font-size: 30px;
}

.blo a.bloghdaedu {
    font-size: 25px;
    margin: 0;
    padding: 0;
    line-height: 25px;
    float: left;
    width: 100%;
    font-weight: 600;
}
.blo img.img-thumbnail{
      width: 100%;
    height: 175px;
}
.simg img.img-thumbnail{
  width: 100%;
}
.memb1 p {
    font-size: 24px;
    font-weight: 600;
}

}

p.fullwd {
    opacity: 0;
}

p.leftwd {
    color: #9e9292;
}
p.leftwd {
    float: left;
    width: 89%;
}

a.rightwd.views {
    color: #000;
    font-size: 13px;
	    float: right;
}
</style>

</div>
