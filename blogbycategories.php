<div class="Blog">
   <?php
      /**
      /*
       Template Name: Blog and Articles
       */
       
       get_header(); ?>
   <div class="catchybga">
      <div class="want">
         <h3>Would You Like To Post A Blog?- </h3>
         <p><a href="#">Post Here</a></p>
      </div>
      <div class="wantpost">
         <p>Do You Have Any Question In Mind?- <a href="#"> Discussion Forum</a></p>
      </div>
   </div>
   <div class="container2">
      <div class="col-md-12">
         <div class="eventhead">
            <h1>Latest Articles</h1>
            <a href="http://edukeeda.com/latest-blogs/">See all</a>
         </div>
      </div>
      <?php
         $args = array(
           'numberposts' => 4,
           'offset' => 0,
           'category' => 0,
           'orderby' => 'post_date',
           'order' => 'DESC',
           'include' => '',
           'exclude' => '',
           'meta_key' => '',
           'meta_value' =>'',
           'post_type' => 'blogs',
           'post_status' => 'publish',
           'suppress_filters' => true
         );
         
         $recent_posts = wp_get_recent_posts( $args, ARRAY_A );
         //print_r($recent_posts);
         if(!empty($recent_posts))
         {
           foreach ($recent_posts as $value) {
             $i=0;
            $cats = get_the_category($value["ID"]);
             $image1 = wp_get_attachment_image_src( get_post_thumbnail_id( $value['ID'] ), 'single-post-thumbnail' );
             $author_name = get_the_author_meta( 'display_name', $value['post_author'] );
        ?>
      <div class="blog-post col-md-3">
         <div class="blogs">
            <div class="ent">
               <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>" title="<?php echo !empty($value['post_title'])?$value['post_title']:""; ?>">
               <img width="525" height="350" src="<?php echo $image1[0]; ?>" alt="">  </a>      
            </div>
            <h4 class="ent">
               <a href="<?php echo esc_url( get_permalink($value['ID']) ); ?>" rel="bookmark"><?php echo !empty($value['post_title'])?$value['post_title']:""; ?></a>
            </h4>
            <div class="fit">
               <?php 
                  $category_detail=get_the_category($value['ID']);//$post->ID
                    foreach($category_detail as $cd){
                    ?>
               <p><?php echo $cd->cat_name; ?></p>
               <?php } ?> 
            </div>
            <div class="entr">
               <span>By <?php echo $author_name;?></span>
               <strong><i class="fa fa-share-alt" aria-hidden="true"></i> <i class="fa fa-eye"></i> 20</strong>
               <p><?php the_time('F jS, Y'); ?></p>
            </div>
         </div>
      </div>
      <?php 
         } 
         } ?>
      <div class="col-md-12 nextsection">
         <div class="odd">
            <h2>Explore by Category</h2>
               <?php
               $categories = get_categories( array(
                   'child_of'            => 0,
                   'current_category'    => 0,
                   'depth'               => 0,
                   'echo'                => 1,
                   'exclude'             => '',
                   'exclude_tree'        => '',
                   'feed'                => '',
                   'feed_image'          => '',
                   'feed_type'           => '',
                   'hide_empty'          => 0,
                   'hide_title_if_empty' => false,
                   'hierarchical'        => true,
                   'order'               => 'ASC',
                   'orderby'             => 'name',
                   'separator'           => '<br />',
                   'show_count'          => 0,
                   'show_option_all'     => '',
                   'show_option_none'    => __( 'No categories' ),
                   'style'               => 'list',
                   'taxonomy'            => 'blog_category',
                   'title_li'            => __( 'Categories' ),
                   'use_desc_for_title'  => 1,
               ) );
               
                if(!empty($categories))
                {
                    foreach ($categories as $terms) {
                    
                    $term_id = $terms->term_id;
                    $image   = category_image_src( array('term_id'=>$term_id) , false );
                    $category_link = get_category_link( $term_id );
                ?>
                  <div class="col-md-3">
                     <a href="<?php echo site_url(); ?>/expertise/blog-posting?id=<?php echo $term_id; ?>">
                        <div class="even1">
                           <img src="<?php echo !empty($image)?$image:"/wp-content/themes/sydney/images/person-interviewing-200n-012110.jpg"; ?> ">
                           <p><?php echo $terms->name; ?>
                           </p>
                        </div>
                     </a>
                  </div>
            <?php 
               }
                 }
            ?>
         </div>
      </div>
      <?php while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'content', 'page' ); ?>
      <?php
         // If comments are open or we have at least one comment, load up the comment template
         if ( comments_open() || get_comments_number() ) :
         comments_template();
         endif;
         ?>
      <?php endwhile; // end of the loop. ?>
      </main><!-- #main -->
   </div>
   <!-- #primary -->
   <?php get_footer(); ?>
</div>
<style>
   .want a{
   border-radius: 10px;
   background: #f68c2f;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .wantpost a {
   border-radius: 10px;
   background: #f68c2f;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .entr span {
   display: block;
   font-size: 15px;
   color: #9e9292;
   }
   .entr p {
   font-size: 15px;
   }
   .fit {
   padding: 10px;
   }
   h4.ent {
   padding: 10px;
   font-size: 19px;
   margin: 0px;
   line-height: 0px;
   }
   .entr {
   padding: 10px;
   align-items: 0px 14px;
   margin-top: 0px;
   margin-bottom: 0px;
   }
   .entr strong {
   float: right;
   font-weight: 100;
   font-size: 13px;
   }
   .fit p {
   margin-bottom: 0px;
   font-size: 16px;
   font-weight: 100;
   }
   .late h1 {
   font-size: 25px;
   } 
   .blog-post .entry-title{
   margin: 0px 0;
   font-size: 16px;
   padding: 10px;
   }
   .entry-summary {
   padding: 10px;
   }
   .blogs {
   box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.07);
   }
   }
   .even {
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   margin-bottom: 35px;
   }
   .even p{
   margin-bottom: 0px;
   }
   .even1 p{
   color: black;
   top: 35%;
   right: 0;
   left: 0;
   font-size: 20px;
   font-weight: 600;
   }
   .alt h1 {
   font-size: 20px;
   }
   .odd h2 {
   font-size: 20px;
   }
   .even1{
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   margin-bottom: 35px;
   background-size: cover;
   text-align: center;
   color: white;
   }
   .even1 img {
   width: 100%;
   }
   .eventdst {
   position: absolute;
   top: 29%;
   right: 0;
   left: 0;
   }
   .eventdst i{
   color: #f68e2f;
   margin-right: 0px!important;
   }
   p.mainhdev {
   font-size: 24px;
   margin: 0;
   }
   strong {
   font-weight: 600;
   }
   p.mainhdev{
   position: initial;
   }
   .blo2 a {
   float: right;
   }
   .catchybga {
   padding: 14% 0;
   background: url(/img/slide3.jpg);
   background-size: cover;
   background-position: 100%;
   position: relative;
   }
   .container {
   width: 100%;
   margin: 0 auto;
   padding: 0 !important;
   }
   .Blog .container2 {
   margin: 0 auto;
   max-width: 1170px;
   }
   .Blog .row{margin:0;}
   .nextsection{margin-bottom:50px;
   }
   .want {
   text-align: center;
   color: white;
   }
   .want h3 {
   color: white;
   font-size: 36px;
   font-weight: 600;
   margin-bottom: 0px;
   }
   .want a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .wantpost {
   TEXT-ALIGN: CENTER;
   }
   .wantpost p {
   color: white;
   font-size: 20px;
   position: ABSOLUTE;
   left: 0;
   right: 0;
   margin: 10 AUTO;
   FLOAT: NONE;
   WIDTH: AUTO;
   BOTTOM: 0px;
   }
   .wantpost a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .want p {
   font-size: 32px;
   margin-top: 10px;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 50px;
   }
   .eventhead h1 {
   font-size: 31px;
   float: left;
   margin: 0 0 60px 15px;
   }
   .eventhead a {
   float: right;
   }
   @media only screen and (max-width: 767px)
   {
   .want h3 {
   color: white;
   font-size: 23px;
   font-weight: 100;
   }
   .wantpost p {
   color: white;
   font-size: 17px;
   position: ABSOLUTE; 
   margin: 0 AUTO;
   BOTTOM: 0px;
   font-weight: unset;
   }
   .want p {
   font-size: 17px;
   }
   }
</style>

  
