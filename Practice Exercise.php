<div class="ajeetbaba">

<?php
/**
/*
 Template Name: Exam Detail Page
 */

get_header(); ?>
<style type="text/css">
	.page-wrap .content-wrapper{
	background-color:transparent;
}
.boardExamHeader {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    padding-bottom: 100px;
	padding-top: 72px;
background-color: black;}
.boardExamHeader_left {
         width: 674px;
    padding-top: 35px;
    padding-left: 361px;
}
.boardExamHeader_title {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    line-height: 1;
	color: #60b91f;;}
.boardExamHeader_titleImage {
    width: 90px;
    height: 90px;
    background: hsla(0,0%,100%,.2);
    border-radius: 50%!important;
    margin-right: 20px;
}
img {
    max-width: 100%;
}
.ajeetbaba .container{
	    width: 100%;
}
.headings_h1 {
    font-size: 34px;
    letter-spacing: -1.67px;
}
.boardExamHeader_description {
    margin: 32px 0 50px;
    font-size: 14px;
    line-height: 21px;
    font-weight: 600;
    color: white;
	}
.button-smallHeight {
    height: 40px;
	font-size: 12px;}

.button {
    font-size: 14px;
    font-weight: 700;
    text-align: center;
    border-radius: 100px;
    text-transform: uppercase;
    padding: 0 20px;
    letter-spacing: 1px;
    color: #fff;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    height: 50px;
    -webkit-transition: background .2s ease;
    transition: background .2s ease;
    background: #60b91f;
}
.boardExamHeader_right {
    font-size: 15px;
	    margin-right: 369px;
    line-height: 20px;
    font-weight: 600;
    width: 441px;
    padding: 20px 35px 35px;
     background-color: white;
	border-radius: 100px 4px 100px 4px;
}
.boardExamHeader_detailsHeading {
    color: #5c5cd3;
	text-align:center;
	margin-bottom: 25px;
font-size: 23;}
.boardExamHeader_details {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
}

.boardExamHeader_details_list {
    margin-bottom: 20px;
    width: calc(50% - 15px);
}

li {
    list-style-type: none;
}
.boardExamHeader_details_label {
    font-size: 12px;
    line-height: 17px;
    letter-spacing: 1px;
    color: #6666d3;
	text-transform: uppercase;}

.boardExamHeader_details_value {
    color: #4a4a4a;
}
.knowAboutSection {
    padding-top: 50px;
    padding-bottom: 100px;
    background: #fff;
}
.knowAboutSection_mainHeading {
    text-align: center;
    font-size: 32;
        color: #5c5cd0;
   
}
.knowAboutSection_lhsRhsWrap {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    margin-top: 50px;
}
.knowAboutSection_lhs {
    width: 30.22%;
padding-top: 43px;
}
.flex_centered {
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
}
.knowAboutSection_lhsHeading {
    background: #f9f9f9;
    font-weight: 700;
    margin-bottom: 4px;
	text-align:left;
	    padding-left: 35px;
}
.knowAboutSection_lhsHeading, .knowAboutSection_lhsItem, .knowAboutSection_lhsSubListItem {
    font-size: 12px;
    color: #000;
    letter-spacing: 2px;
    text-transform: uppercase;
    line-height: 18px;
        margin-left: 345px;
    width: 295px;
}
.knowAboutSection_lhsItem {
    border-bottom: 1px solid #efefef;
    background: #fafafa;
    cursor: pointer;
}
.knowAboutSection_lhsItem p {
    height: 50px;
    padding-left: 36px;
    padding-right: 15px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-transition: color .1s ease-in;
    transition: color .1s ease-in;
    font-size: 16px;
}

.knowAboutSection_lhsSubList {
    display: none;
}
.knowAboutSection_lhsSubListItem:first-child {
    margin-top: 0;
}
.knowAboutSection_lhsSubListItem {
    color: #969696;
    cursor: pointer;
    padding: 12px 15px 12px 56px;
    -webkit-transition: color .1s ease-in;
    transition: color .1s ease-in;
}
.knowAboutSection_rhs {
    width: 60.24%;
}
.knowAboutSection_rhsHeading {
   color: #5c5cd0;
    margin-bottom: 16px;
    font-weight: 700;
    font-size: 32px;
    margin-top: 30px;
}
.knowAboutSection_rhsHeading svg {
    display: none;
}
.knowAboutSection_rhsContent {
    color: #7f7f7f;
    line-height: 26px;
}
.knowAboutSection_rhsSubHeading {
    font-weight: 600;
    font-size: 20px;
    color: #000;
    margin-bottom: 10px;
}
.knowAboutSection_rhsSubContent {
    margin-bottom: 22px;
}

.knowAboutSection_list>li:first-child {
    margin-top: 10px;
}
.knowAboutSection_list li {
    margin-top: 15px;
    line-height: 26px;
}
.knowAboutSection_list>li, .knowAboutSection_list li>ul li {
    padding-left: 25px;
    text-indent: -25px;
}

li {
    list-style-type: none;
}
.knowAboutSection_rhsSubHeading {
    font-weight: 600;
    font-size: 20px;
    color: #000;
    margin-bottom: 10px;
}
.knowAboutSection_rhsSubContent {
    margin-bottom: 22px;
}
.knowAboutSection_rhs li, .knowAboutSection_rhs p, .knowAboutSection_rhs table {
    line-height: 26px;
}
.knowAboutSection_subject {
    width: 100%;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    font-size: 16px;
    line-height: 26px;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    font-weight: 600;
    margin-top: 20px;
}

.knowAboutSection_subject svg {
    margin-right: 10px;
    width: 40px;
    height: 40px;
}
.knowAboutSection_subject {
    width: 100%;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    font-size: 16px;
    line-height: 26px;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    font-weight: 600;
    margin-top: 20px;
}
.knowAboutSection_blockWrap {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    line-height: 26px;
}
.knowAboutSection_blockWrap .knowAboutSection_orderedList {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}
.knowAboutSection_orderedList {
    padding-left: 30px;
}
.knowAboutSection_orderedList>li:first-child {
    margin-top: 10px;
}
.knowAboutSection_orderedList>li {
    margin-top: 15px;
    list-style-type: decimal-leading-zero;
}
.knowAboutSection_rhs li, .knowAboutSection_rhs p, .knowAboutSection_rhs table {
    line-height: 26px;
}
li {
    list-style-type: none;
}
.knowAboutSection_blockWrap .knowAboutSection_orderedList:nth-child(2n) {
    margin-left: 15px;
}
.knowAboutSection_blockWrap .knowAboutSection_orderedList {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}
.knowAboutSection_orderedList {
    padding-left: 30px;
}
.knowAboutSection_subject svg {
    margin-right: 10px;
    width: 40px;
    height: 40px;
}
.knowAboutSection_blockWrap {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
	line-height: 26px;}


.knowAboutSection_blockWrap .knowAboutSection_orderedList {
    -webkit-box-flex: 1;
    -ms-flex: 1;
    flex: 1;
}
.knowAboutSection_orderedList {
    padding-left: 30px;
}

.knowAboutSection_rhsSubContent:last-child {
    margin-bottom: 0;
}
.knowAboutSection_rhs li, .knowAboutSection_rhs p, .knowAboutSection_rhs table {
    line-height: 26px;
}
.knowAboutSection_table {
    margin-top: 20px;
    table-layout: fixed;
    border: 1px solid #e7e7e7;
    border-radius: 10px;
    border-collapse: separate;
    width: 100%;
    word-break: break-word;
    text-indent: 0;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
.knowAboutSection_table thead {
    background: #ededed;
}
.knowAboutSection_table th:last-child {
    border-top-right-radius: 10px;
    border-right: 0;
}
.knowAboutSection_table th:first-child {
    border-top-left-radius: 10px;
}
.knowAboutSection_table th {
    text-align: center;
    padding: 15px 5px;
    font-size: 14px;
    font-weight: 400;
    letter-spacing: 1px;
    border-right: 1px solid #e7e7e7;
}
.knowAboutSection_table td {
    width: 25%;
    padding: 15px 20px;
    border-right: 1px solid #e7e7e7;
    border-bottom: 1px solid #e7e7e7;
    text-align: left;
}
.knowAboutSection_table td:last-child:not(:first-child):not(.keepBorder) {
    border-right: 0;
}
.knowAboutSection_table td {
    width: 25%;
    padding: 15px 20px;
    border-right: 1px solid #e7e7e7;
    border-bottom: 1px solid #e7e7e7;
}
abbr, address, article, aside, audio, b, blockquote, body, canvas, caption, cite, code, dd, del, details, dfn, div, dl, dt, em, fieldset, figcaption, figure, footer, form, h1, h2, h3, h4, h5, h6, header, hgroup, html, i, iframe, img, ins, kbd, label, legend, li, mark, menu, nav, object, ol, p, pre, q, samp, section, small, span, strong, sub, summary, sup, table, tbody, td, tfoot, th, thead, time, tr, ul, var, video {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 100%;
    background: transparent;
}
*, :after, :before {
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}
user agent stylesheet
td, th {
    display: table-cell;
    vertical-align: inherit;
}
.knowAboutSection_rhs li, .knowAboutSection_rhs p, .knowAboutSection_rhs table {
    line-height: 18px;
}
.knowAboutSection_table {
    margin-top: 20px;
    table-layout: fixed;
    border: 1px solid #e7e7e7;
    border-radius: 10px;
    border-collapse: separate;
    width: 100%;
    word-break: break-word;
    text-indent: 0;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
.knowAboutSection_lhsItem.active p{
	    border-left: 4px solid #5c5cd0;
    padding-left: 32px;
    color: #5c5cd0;


}








@media only screen and (max-width: 767px){
.boardExamHeader_title {
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
}
	.boardExamHeader_titleImage {
    width: 35px;
    height: 35px;
    margin-right: 7px;
}
	.boardExamHeader {
    display: block;
    padding-bottom: 50px;
}
	.boardExamHeader_left {
    width: 256px;
    margin: auto;
    padding-top: 0;
		text-align: center;
	padding-left:0px;}
	.boardExamHeader_title {
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
}

	.boardExamHeader_description {
    margin: 9px 0 20px;
    font-size: 14px;
    line-height: 18px;
}
	.boardExamHeader_cta {
    font-size: 11px;
    padding: 7px 36px 8px;
    letter-spacing: 1.24px;
}
	.boardExamHeader_right {
    width: auto;
    padding: 24px 26px 30px;
    border-radius: 5px;
    margin: 0 auto;
    margin-top: 30px;
    max-width: 500px;
}
	.boardExamHeader_detailsHeading {
    margin-bottom: 20px;
}
	.boardExamHeader_details {
		display: block;}
	.boardExamHeader_details_list {
		width: 100%;}
	.boardExamHeader_details_label {
    font-size: 10px;
    line-height: 14px;
    letter-spacing: .83px;
    margin-bottom: 2px;
}
	.boardExamHeader_details_value {
    font-size: 12px;
    line-height: 17px;
    font-weight: 400;
    letter-spacing: -.1px;
    color: #4a4a4a;
}
	.knowAboutSection_lhsHeading, .knowAboutSection_lhsItem, .knowAboutSection_lhsSubListItem {
    font-size: 12px;
    color: #000;
    letter-spacing: 2px;
    text-transform: uppercase;
    line-height: 18px;
        margin-left: 0px;
    width: 258px;
}
	
}



</style>

 <style>
$(".pp").click(function() {
    $('html,body').animate({
        scrollTop: $(".pp1").offset().top},
        'slow');
});

 </style>

 <div class="container1">
                    
<div class="boardExamHeader">
    <div class="boardExamHeader_left">
        <h1 class="headings_h1 boardExamHeader_title">
            <img class="boardExamHeader_titleImage" src="http://edukeeda.com/wp-content/uploads/2018/11/images.png"> JEE Main
        </h1>
        <div class="boardExamHeader_description">
            An all-India level exam for admission into IITs, NITs and some of India’s best engineering colleges.
        </div>
        <button class="button button-smallHeight boardExamHeader_cta js-sign-up-btn" data-page="exam" data-element="start_preparing_now">
            Start preparing now
        </button>
    </div>
    <div class="boardExamHeader_right">
        <div class="boardExamHeader_detailsHeading">
            Info About JEE Main
        </div>

        <ul class="boardExamHeader_details">
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">Field</div>
                <div class="boardExamHeader_details_value">Engineering</div>
            </li>
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">Eligibility</div>
                <div class="boardExamHeader_details_value">12th Standard Science Students</div>
            </li>
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">Subjects</div>
                <div class="boardExamHeader_details_value">Physics, Chemistry and Maths</div>
            </li>
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">Applicants</div>
                <div class="boardExamHeader_details_value">1.2 Million</div>
            </li>
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">Qualified</div>
                <div class="boardExamHeader_details_value">2,20,000</div>
            </li>
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">Questions Type</div>
                <div class="boardExamHeader_details_value">Objective MCQs</div>
            </li>
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">Paper Pattern</div>
                <div class="boardExamHeader_details_value">90 Questions in 3 Hours</div>
            </li>
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">Colleges</div>
                <div class="boardExamHeader_details_value">IITs, NITs and 100 Colleges </div>
            </li>
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">Difficulty Level</div>
                <div class="boardExamHeader_details_value">Medium to Hard</div>
            </li>
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">Time to Prepare</div>
                <div class="boardExamHeader_details_value">2 Years</div>
            </li>
            
            <li class="boardExamHeader_details_list">
                <div class="boardExamHeader_details_label">When</div>
                <div class="boardExamHeader_details_value">January and April</div>
            </li>
            
        </ul>
    </div>
</div>

                </div>

<section class="knowAboutSection">
    <div class="container phn-tab-ma">
        <h2 class="knowAboutSection_mainHeading js-marker headings_h3" data-marker-id="1">
            All you need to know about JEE Main
        </h2>
        <div class="knowAboutSection_lhsRhsWrap">
    <div class="knowAboutSection_lhs">
        <div class="js-exam-sections-lhs is_stuck" style=" width: 291px; top: 70px;">
            
            <div class="knowAboutSection_lhsList">
               
                <div class="knowAboutSection_lhsItem js-marker-target active" data-marker-target="1" data-default-marker-target="2">
                    <p class="pp">Facts, Syllabus &amp; Pattern</p>
                    
                </div>
                <div class="knowAboutSection_lhsItem js-marker-target" data-marker-target="5" data-default-marker-target="6">
                    <p>How to apply</p>
    
                </div>
                <div class="knowAboutSection_lhsItem js-marker-target" data-marker-target="10" data-default-marker-target="11">
                    <p>Important Dates</p>
                  
                </div>
                
            </div>
        </div><div style="position: static; width: 291.313px; height: 649px; display: block; vertical-align: baseline; float: none;"></div>
    </div>
    <div class="knowAboutSection_rhs ">
        <div class="knowAboutSection_rhsSection">
            <h2 class="knowAboutSection_rhsHeading headings_h4 js-marker js-exam-sections-heading open" data-marker-id="1">
                Facts, Syllabus and Pattern
                <svg width="10" height="6" viewBox="0 0 10 6" xmlns="http://www.w3.org/2000/svg"><title>Stroke 396</title><path d="M9 1L5 5 1 1" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round"></path></svg>
            </h2>
            <div class="knowAboutSection_rhsContent ">
                <h2 class="knowAboutSection_rhsSubHeading js-marker" data-marker-id="2">What is JEE Main?</h2>

                <div class="knowAboutSection_rhsSubContent  ">
                    <p>JEE Main is among the <a rel="noopener" target="_blank" >toughest exams</a> in the world. Every year, over 12 lakh students appear while only 2,20,000 <a rel="noopener" target="_blank" >qualify for JEE Advanced</a>. </p>
                    <ul class="knowAboutSection_list">
                        <li>The Joint Entrance Examination (JEE) is an All-India Common Entrance Exam for getting admissions into engineering colleges across India</li>
                        <li>JEE Main will be conducted twice a year in the month of January and April respectively by National Testing Agency (NTA).</li>
                        <li>It is conducted in two phases: JEE Main and JEE Advanced</li>
                        <li>If you clear the given cut-off for JEE Main, you qualify for JEE Advanced</li>
                        <li>With a JEE Main score you can apply to institutions like NITs, IIITs and other Central or State Funded Institutes</li>
                        <li>Whereas, IITs and ISM, Dhanbad consider results of JEE Advanced for admissions</li>
                        <li>From 2019 the examination will be held only in the computer based mode.</li>
                    </ul>
                </div>

                <h2 class="knowAboutSection_rhsSubHeading js-marker" data-marker-id="3">Syllabus for JEE Main</h2>
                <div class="knowAboutSection_rhsSubContent">
                    <p>JEE Main 2019 follows the CBSE prescribed NCERT syllabus. The difficulty level of the examination will also be based on the difficulty level of NCERT.</p>

              
                    <div class="knowAboutSection_blockWrap">
                        <ol class="knowAboutSection_orderedList">
                            <li>Physics and Measurement</li>
                            <li>Kinematics</li>
                            <li>Laws of Motion</li>
                            <li>Work, Energy and Power</li>
                            <li>Rotational Motion</li>
                            <li>Gravitation</li>
                            <li>Properties of Solid and Liquids</li>
                            <li>Thermodynamics</li>
                            <li>Kinetic Theory of Gases</li>
                            <li>Oscillations and Waves</li>
                            <li>Electrostatics</li>
                        </ol>
                        <ol class="knowAboutSection_orderedList" start="12">
                            <li>Current Electricity</li>
                            <li>Magnetic Effects of Currents and Magnetism</li>
                            <li>Electromagnetic Induction</li>
                            <li>Alternating Currents</li>
                            <li>Electromagnetic Waves</li>
                            <li>Optics</li>
                            <li>Dual Nature of Matter and Radiation</li>
                            <li>Atoms and Nuclei</li>
                            <li>Electronic Devices</li>
                            <li>Communication Systems and Experimental Skills</li>
                        </ol>
                    </div>

                  
                    <div class="knowAboutSection_blockWrap">
                        <ol class="knowAboutSection_orderedList">
                            <li>Some Basic Concepts in Chemistry</li>
                            <li>The States of Matter</li>
                            <li>Atomic Structure</li>
                            <li>Chemical Bonding and Molecular Structure</li>
                            <li>Chemical Thermodynamics</li>
                            <li>Solutions</li>
                            <li>Equilibrium</li>
                            <li>Redox Reactions and Electrochemistry</li>
                            <li>Chemical Kinetics</li>
                            <li>Surface Chemistry</li>
                            <li>Classification of Elements and Periodicity in Properties</li>
                            <li>General Principles and Processes of Isolation of Metals</li>
                            <li>Hydrogen</li>
                            <li>s-Block Elements</li>
                        </ol>
                        <ol class="knowAboutSection_orderedList" start="15">
                            <li>p-Block Elements</li>
                            <li>d- and f-Block Elements</li>
                            <li>Coordination Compounds</li>
                            <li>Environmental Chemistry</li>
                            <li>Purification and Characterisation of Organic Compounds</li>
                            <li>Some Basic Principles of Organic Chemistry</li>
                            <li>Hydrocarbons</li>
                            <li>Organic Compounds Containing Halogens</li>
                            <li>Organic Compounds Containing Oxygen</li>
                            <li>Organic Compounds Containing Nitrogen</li>
                            <li>Polymers</li>
                            <li>Biomolecules</li>
                            <li>Chemistry in Everyday Life</li>
                            <li>Principles Related to Practical Chemistry</li>
                        </ol>
                    </div>

                   
                    <div class="knowAboutSection_blockWrap">
                        <ol class="knowAboutSection_orderedList">
                            <li>Sets, Relations, and Functions</li>
                            <li>Complex Numbers and Quadratic Equations</li>
                            <li>Matrices and Determinants</li>
                            <li>Permutations and Combinations</li>
                            <li>Mathematical Induction</li>
                            <li>Binomial Theorem and its Simple Applications</li>
                            <li>Sequences and Series</li>
                            <li>Limits Continuity and Differentiability</li>
                        </ol>
                        <ol class="knowAboutSection_orderedList" start="9">
                            <li>Integral Calculus</li>
                            <li>Differential Equations</li>
                            <li>Coordinate Geometry</li>
                            <li>Three-dimensional Geometry</li>
                            <li>Vector Algebra</li>
                            <li>Statistics and Probability</li>
                            <li>Trigonometry</li>
                            <li>Mathematical Reasoning</li>
                        </ol>
                    </div>
                </div>

                <h2 class="knowAboutSection_rhsSubHeading js-marker" data-marker-id="4">Exam Pattern for JEE Main</h2>
                <div class="knowAboutSection_rhsSubContent pp1">
                    <div>The JEE Main 2019 Examination is divided into two parts: </div>
                    <ul class="knowAboutSection_list">
                        <li>Paper 1 for B.E. and B. Tech. (Computer based only)</li>
                        <li>Paper 2 for B. Arch. &amp; B. Planning (Pen and paper only)</li>
                    </ul>
                    <table class="knowAboutSection_table">
                        <thead>
                            <tr>
                                <th class="ac" colspan="2">
                                    <strong>Paper 1</strong>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Particulars</strong>
                                </td>
                                <td>
                                    <strong>Details</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Subjects
                                </td>
                                <td>
                                    Physics, Chemistry and Mathematics
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Type of questions
                                </td>
                                <td>
                                    Objective (MCQs)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Mode of examination
                                </td>
                                <td>
                                    Computer based mode only
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Exam duration
                                </td>
                                <td>
                                    3 hours
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Number of questions
                                </td>
                                <td>
                                    90 (30 questions from each subject)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Marking scheme
                                </td>
                                <td>
                                    4 marks awarded for each correct answer
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Negative marking
                                </td>
                                <td>
                                    1/4 marks deducted for each incorrect response
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="knowAboutSection_table">
                        <thead>
                            <tr>
                                <th class="ac" colspan="2">
                                    <strong>Paper 2</strong>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Sections</strong>
                                </td>
                                <td>
                                    <strong>Type of Question</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Section 1: Mathematics
                                </td>
                                <td>
                                    Objective (MCQs)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Section 2: Aptitude test
                                </td>
                                <td>
                                    Objective (MCQs)
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Section 3: Drawing test
                                </td>
                                <td>
                                    -
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Particulars</strong>
                                </td>
                                <td>
                                    <strong>Details</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Mode of Examination
                                </td>
                                <td>
                                    Offline
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Exam Duration
                                </td>
                                <td>
                                    3 hours
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div class="mt-15 phn-tab-mt-10"><a rel="noopener" >Language Mediums</a> in JEE Main 2019</div>
                    <table class="knowAboutSection_table">
                        <thead>
                            <tr>
                                <th>
                                    <strong>Medium of Language</strong>
                                </th>
                                <th>
                                    <strong>Exam Centres</strong>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    English and Hindi
                                </td>
                                <td>
                                    All examination centres
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Gujarati
                                </td>
                                <td>
                                    Exam centres in Gujarat, Daman &amp; Diu and Dadra &amp; Nagar Haveli
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Marathi
                                </td>
                                <td>
                                    Exam centres in Maharashtra
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Urdu
                                </td>
                                <td>
                                    Exam centres in Maharashtra
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="mt-15 phn-tab-mt-10">Here are a few important guidelines you need to keep in <a rel="noopener" target="_blank" class="js-modal-trigger" data-modal-id="1">mind</a>.</div>
                    <div class="js-modal-trigger-target hide" data-modal-id="1">
                        <ul class="knowAboutSection_list">
                            <li>There is only one correct response for a question. More than one response(s) to any question is considered incorrect and marks are deducted accordingly.</li>
                            <li>Electronic gadgets are not allowed in the exam hall.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

       
            </div>
        </div>
    </div>
</div>

    </div>
</section>



 
	<div id="primary" class="content-area col-md-12">
		<main id="main" class="post-wrap" role="main">
		
	

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'page' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
</div>
