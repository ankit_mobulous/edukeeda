<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

	if(isset($_POST['submitwall'])) {

		//$title = $_POST["title"];
		$desc = $_POST["desc"];
		$desc = stripslashes($desc);
		$userId = $_SESSION["login"]["id"];

		if($desc) {
			$data = array("user_id"=>$userId, "content"=>$desc);
			    $wpdb->insert("wp_wallpost", $data);
			    $_SESSION["wallpostdone"] = "Your Post has been Successfully Submitted";
			    header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
		} else {
		    //$_SESSION["posterror"] = "Wallpost Field is required at least 250 words";
			header("Location: https://edukeeda.com/wallpost-query-form/");
		}
	} else {
		header("Location: https://edukeeda.com/wallpost-query-form/");
	}


?>
