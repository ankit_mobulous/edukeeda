function getprofile_post(id) {     
      $('.loader').toggle();

      $.ajax({    //create an ajax request to load_page.php
        type: "post",
        url: template_directory_uri+"/ajax/profile_all_post.php", 
        data:{ id : id },            
        dataType: "html",   //expect html to be returned                
        success: function(response){ 
   	  $('.loader').toggle();                    
           $("#allposts").html(response); 
          //alert(response);
        }

    });
}

function display_fav()
{
  $('.loader').toggle();     
   $.ajax({    //create an ajax request to load_page.php
          type: "post",
          url: template_directory_uri+"/ajax/get_favorites.php", 
          success: function(response){ 
            
             $('.loader').toggle();                    
             $("#favoritePosts").html(response); 
          }

      });
}

