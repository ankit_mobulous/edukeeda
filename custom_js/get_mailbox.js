function get_mailbox(id) {    

      $('.loader').toggle();

      $.ajax({  
        type: "post",
        url: template_directory_uri+"/ajax/fetch_mail_data.php", 
        data:{ id : id,'totalMessagesPerPage':totalMessagesPerPage },            
        dataType: "html",   //expect html to be returned                
        success: function(response){ 
         // alert(response);
   	        $('.loader').toggle();                    
            //$("#").html(response); 
            
        }

    });
}




var currentInboxPage=1;
var inboxFrom=0;
var mailType=1;
var tmails=totalMailsInInbox;
function getInbox(page,tp,showLoader)
{
  $('#searched_results').hide();
  $('#searchText').val('');
  if(tp)
  {   
    $('.pagination').show();
    mailType=tp; 
    $('.list-group-item').removeClass('active'); 
    $('#Rtab_'+tp).addClass('active');
    currentInboxPage=1;
    inboxFrom=0;
    $('.stPage1').html(1);
    $('.stPage2').html(totalMessagesPerPage);
    
    if(tp==1){   tmails=totalMailsInInbox;  }
    if(tp==2){   tmails=totalMailsInStared;  }
    if(tp==3){   tmails=totalMailsInSent;  }
    if(tp==4){   tmails=totalMailsInTrash;  }
    
    if(tmails<totalMessagesPerPage){ $('.stPage1').html(1); $('.stPage2').html(tmails); }
    if(tmails==0){ $('.pagination').hide(); }
    $('.stPage3').html(tmails);
  }
  else
  {
    if(page){ inboxFrom=page; }
  }

  if(showLoader!='no')
  {
    $('.loader').toggle();  
  }
  

  $.ajax({
    url:template_directory_uri+"/ajax/fetch_inbox.php",
    data:"from="+inboxFrom+"&to="+totalMessagesPerPage+"&mailType="+mailType,
    type:"POST",
    success:function(suc)
          {
            if(showLoader!='no')
            {
              $('.loader').toggle();  
            }               
            $("#allMessages").html(suc); 
          }
  })
}

function nextPage()
{
  var checkMails=(tmails-totalMessagesPerPage);
  //alert(tmails+"  "+inboxFrom+'!='+checkMails +'&&'+ inboxFrom+'<'+checkMails);

  if(inboxFrom!=checkMails && inboxFrom<checkMails)
  {
    currentInboxPage++;
    inboxFrom=inboxFrom+totalMessagesPerPage;
    $('.stPage1').html(inboxFrom);
    var totalShow=inboxFrom+totalMessagesPerPage;
    if(totalShow>tmails)
    {
      totalShow=tmails;
    }
    $('.stPage2').html(totalShow);
    getInbox();
  }
}

function previousPage()
{
  if(inboxFrom!=0)
  {
    currentInboxPage--;
    inboxFrom=inboxFrom-totalMessagesPerPage;
    if(inboxFrom==0)
    {
      $('.stPage1').html(1);
    }
    else
    {
      $('.stPage1').html(inboxFrom);
    }
    var totalShow=inboxFrom+totalMessagesPerPage;
    $('.stPage2').html(totalShow);
    getInbox();
  }
}

function get_total_unread_mails_in_inbox()
{
    $.ajax({
      url:template_directory_uri+"/ajax/get_total_unread_messages.php",
      success:function(suc)
              {
                  suc=suc.trim();
                  suc=JSON.parse(suc);
                  if(totalMailsInInbox!=suc['totalMails'])
                  {
                    $('.totalUnreadMessages').html(suc['unreadMails']);
                    totalUnreadMessages=suc['unreadMails'];
                    totalMailsInInbox=suc['totalMails'];
                    tmails=suc['totalMails'];
                    $('.stPage3').html(totalMailsInInbox);

                    if(mailType==1 && currentInboxPage==1){ getInbox(1,1,'no');}
                  }
              }
    })
}

function call_starred(id,tp){

      $('#stared_'+id).css('pointer-events','none');
      if(tp==0)
      {
          totalMailsInStared++;
          $('#stared_'+id).addClass('staredItem');
          $('#stared_'+id).removeClass('blue');
          $('#stared_'+id).attr('onclick','call_starred('+id+',1)');
      }
      else
      {
          totalMailsInStared--;
          $('#stared_'+id).removeClass('staredItem');
          $('#stared_'+id).addClass('blue');
          $('#stared_'+id).attr('onclick','call_starred('+id+',0)');
      }

      $.ajax({    //create an ajax request to load_page.php
        type: "post",
        url: template_directory_uri+"/ajax/add_starred.php", 
        data:{ id : id ,'type':tp},            
        dataType: "html",   //expect html to be returned                
        success: function(response){ 
          $('#stared_'+id).css('pointer-events','all');
        }
    });

}



function call_to_trash(id,tp,utype){

$('.loader').toggle();  
      $.ajax({    
        type: "post",
        url: template_directory_uri+"/ajax/call_to_trash.php", 
        data:{ id : id,'type':tp,'userType': utype},            
        dataType: "html",          
        success: function(response){ 
   	       // $('#mail_'+id).hide();
           response=response.trim();
           if(response==2){ totalMailsInStared--; totalMailsInTrash++;}
           if(tp==3)
           {
              totalMailsInSent--;
              totalMailsInTrash++;
           }
           if(tp==1)
           {
            //totalMailsInInbox--;
            totalMailsInTrash++;
           }
           if(tp==4)
           {
            totalMailsInTrash--;
           }
           getInbox(1,tp);
            $('.loader').toggle();  
        }

    });

}


function call_view(id){

//alert(id);
      $.ajax({    //create an ajax request to load_page.php
        type: "post",
        url: template_directory_uri+"/ajax/call_to_view.php", 
        data:{ id : id },            
        dataType: "html",   //expect html to be returned                
        success: function(response){ 
   	
						
		// $('#successStarred').modal("show");
						
						 
		

        }

    });

}





function delete_mail(id){

//alert(id);
      $.ajax({    //create an ajax request to load_page.php
        type: "post",
        url: template_directory_uri+"/ajax/delete_mail.php", 
        data:{ id : id },            
        dataType: "html",   //expect html to be returned                
        success: function(response){ 
   	$('#RstPassForm')[0].reset();
	

        }

    });

}




function add_mail_replys(id,to,mine){

$('.loader').toggle();
 var replydata=$('#replymsg').val();
      	$.ajax({    //create an ajax request to load_page.php
        type: "post",
        url: template_directory_uri+"/ajax/add_mail_reply.php", 
        data:{ id : id , replydata : replydata,to : to, mine : mine},
        dataType: "html",   //expect html to be returned                
        success: function(response){ 

                    $('#replymsg').val('');
                    $('#chatMessages').append(response);
                    $('#chatMessages').show();
                    $('.loader').toggle();
        }
    });
}


function search_mails()
{
  var searchText=$('#searchText').val().trim();
  if(searchText)
  {
    $('.loader').toggle();  
      $.ajax({
        url:template_directory_uri+"/ajax/searchMail.php",
        data:"searchText="+searchText+"&mailType="+mailType,
        type:"POST",
        success:function(suc)
                {
                  $('#searched_results').html('Searched Results For '+searchText);
                  $('.pagination').hide();
                  $('.loader').toggle();  
                  $("#allMessages").html(suc); 
                  $('#searched_results').show();
                }
      })
      return false;
  }
  return false;
}

function get_chat_messages(cid,mid)
{
  $.ajax({
    url:template_directory_uri+"/ajax/get_chat_messages.php",
    data:"chatMessages="+chatMessages.toString()+"&cid="+cid+"&mid="+mid,
    type:"POST",
    success:function(suc)
            {

                  suc=JSON.parse(suc);
                  if(suc['html'])
                  {
                    var ids=suc['id'].split(',');
                    for(i=0;i<ids.length;i++)
                    {
                      chatMessages.push(ids[i]);
                    }
                    $('#chatMessages').append(suc['html']);
                    $('#chatMessages').show();
                  }
            }
  })
}


function restoreMail(id,type)
{
  $('.loader').toggle();  
      $.ajax({
        url:template_directory_uri+"/ajax/restoreMail.php",
        data:"id="+id+"&type="+type,
        type:"POST",
        success:function(suc)
                {
                  $('.loader').toggle();  
                  totalMailsInTrash--;
                  getInbox(1,4);
                }
      })
}
