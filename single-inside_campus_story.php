<?php
/**
 * The template for displaying all single posts.
 *
 * @package Sydney
 */

get_header(); ?>
<style type="text/css">

#secondary{
        margin-top: 50px;
}
.post-navigation .nav-next span{
        margin-left: 0px;
}
.nav-links .nav-next {
    width: 100%!important;
    float: none!important;
    text-align: left;
}
    .customsideedu h3.widget-title {
        border: 1px solid #ffaf8c;
        padding: 6px 0px 4px 0;
        margin: 0 0 10px 0;
        border-left: none;
        border-right: none;
        font-size: 16px;
        line-height: normal;
        font-weight: 600;
        text-transform: uppercase;
    }
   
    .simg {
    float: left;
    width: 100%;
}
.othermemd {
    float: left;
    width: 100%;
  margin-bottom: 15px;
    border-bottom: 1px solid#ffaf8c;
}
.simg a {
    float: left;
    width: 100%;
}
.pimg {
    float: left;
    width: 100%;
}
.pimg p {
    margin-bottom: 0px;
  line-height: 15px;
}
.pimg span {
    font-size: 15px!important;
    
}

.pimg a {
    line-height: 15px;
    float: left;
    width: 100%;
    margin: 4px 0px;
}
.italicfontdesc p {
    font-style: italic;
    font-weight: 600;
    float: left;
    width: 100%;
}
 .commenttoggle .btn-lg {
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
    float: none!important;
    width: 257px;
     left: 35%!important;
}
 @media only screen and (max-width: 767px){
  .postshare {
    float: left;
    width: 100%;
    display: -webkit-box;
    margin: 0px 16px;
   }
   .postshare strong {
    margin: 0px 9px;
    padding: 0px;
   }
  .postshare .pshare {
    margin-left: 10px;
    }  
  .postshare span {
    display: -webkit-box;
    margin: 5px 24px;
    }
    .commenttoggle .btn-lg {
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
    float: none!important;
    width: 257px;
    left: 0%!important;
}
.single .hentry .title-post {
    font-size: 25px;
}

 }
 .hrtagstyleborder{border-bottom: 1px solid #ddd;}
</style>
  <?php if (get_theme_mod('fullwidth_single')) { //Check if the post needs to be full width
    $fullwidth = 'fullwidth';
  } else {
    $fullwidth = '';
  } ?>

  <?php do_action('sydney_before_content'); ?>

  <div id="primary" class="content-area col-md-8 <?php echo $fullwidth; ?>">

    <?php sydney_yoast_seo_breadcrumbs(); ?>

    <main id="main" class="post-wrap" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

      <?php get_template_part( 'content', 'campus' ); ?>
      <hr class="hrtagstyle">
      <hr class="hrtagstyle">
      <?php sydney_post_navigation(); ?>
      <hr class="hrtagstyleborder">
      <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || get_comments_number() ) :
          comments_template();
        endif;
      ?>

    <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

  <?php do_action('sydney_after_content'); ?>

<?php if ( get_theme_mod('fullwidth_single', 0) != 1 ) {?>
<div class="col-md-4 customsideedu">
    <div id="secondary" class="widget-area col-md-3" role="complementary">
        <aside id="recent-posts-3" class="widget widget_recent_entries">
            <h3 class="widget-title">Latest Inside Campus Story</h3>
            <?php
              $args1 = array(
                'numberposts' => 5,
                'offset' => 0,
                'category' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'include' => '',
                'exclude' => '',
                'meta_key' => '',
                'meta_value' =>'',
                'post_type' => 'inside_campus_story',
                'post_status' => 'publish',
                'suppress_filters' => true
              );
              
              $recent_posts1 = wp_get_recent_posts( $args1, ARRAY_A );
              //print_r($recent_posts);
              if(!empty($recent_posts1))
              {
                
                foreach ($recent_posts1 as $value1) {
                  $i1=0;
                  $cats1 = get_the_category($value1["ID"]);
                  $image1 = wp_get_attachment_image_src( get_post_thumbnail_id( $value1['ID'] ), 'single-post-thumbnail' );
                  if($image1){
                    $image1 = $image1[0];
                  } else {
                      $mkey = "_thumbnail_id";
                    $checkimg = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $value1['ID'],$mkey) 
                    );
                    $getimg = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                    );
                    $image1 = $getimg[0]->guid;
                  }
                  $author_name = get_the_author_meta( 'display_name', $value1['post_author'] );
                  $countPostTitle = strlen($value1['post_title']);
                  if($countPostTitle >= 30) {
                    $postTitle = substr($value1['post_title'],0, 30);
                    $postTitle .= "...";
                  } else {
                    $postTitle = $value1['post_title'];
                  }
               ?>
             <div class="othermemd">
                <div class="col-md-5">
                   <div class="simg">
                      <a href="<?php echo esc_url( get_permalink($value1['ID']) ); ?>">   <img src="<?php echo $image1; ?>" class="img-thumbnail"></a>
                   </div>
                </div>
                <div class="col-md-7">
                   <div class="pimg">
                      <p> <strong><a href="<?php site_url();?>/inside-campus-story/">Inside Campus Story</a></strong></p>
                      <a href="<?php echo esc_url( get_permalink($value1['ID']) ); ?>">
                         
                         <?php echo $postTitle; ?>
                      </a>
                      <span>By <?php if($author_name != "admin"){?><a href="<?php echo site_url();?>/profile/?action=<?php echo $value1['post_author'];?>"> <?php echo $author_name; ?> </a><?php } else { echo $author_name; }?></span>
                   </div>
                </div>
             </div>
             <?php
                }
                }
             ?>
        </aside>
        </div>
    </div>
<?php } ?>
<?php get_footer(); ?>
