<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';

global $wpdb;

	if(isset($_POST['submitdiscuss'])) {
		$catone = $_POST["catone"];
		$catthree = $_POST["catthree"];
		$desc = $_POST["desc"];
		$userId = $_SESSION["login"]["id"];

		if($_POST["cattwo"]) {
			$cattwo = $_POST["cattwo"];

			$data = array("sub_cat_id"=>$cattwo, "house_id"=>$catthree, "user_id"=>$userId, "ques"=> $desc);
			$wpdb->insert("wp_discussion_ques", $data);
			$_SESSION["postdone"] = "Your Post has been Successfully Submitted";
			header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
		} else {
			$data = array("sub_cat_id" => 0, "house_id"=>$catthree, "user_id"=>$userId, "ques"=> $desc);
			$wpdb->insert("wp_discussion_ques", $data);
			$_SESSION["postdone"] = "Your Post has been Successfully Submitted";
			header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
		}
	} else {
		header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
	}


?>
