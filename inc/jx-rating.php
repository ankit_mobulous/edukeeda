<?php

function ajax_check_user_logged_in() {

	global $wpdb;
	$post_id=$_POST['post_id'];
	$rating=$_POST['rating'];
  	$user = wp_get_current_user();
	$table_name = $wpdb->prefix . "jx_rating"; 
  	$wpdb->get_results("SELECT * FROM $table_name WHERE user_id='".$user->ID."' AND post_id='".$post_id."'");  
	$rowcount = $wpdb->num_rows;

  	if($rowcount==0) {
		$wpdb->insert($table_name, array(
		   'rating'=>$rating,
		   'user_id'=>$user->ID,
		   'post_id' => $post_id
		),
		array('%s','%d'));
		$payment_id=$wpdb->insert_id; 
	    echo '<span class="success">Thank you for rate us!</span>';
  	} else {
	    echo '<span class="error">You have already rated this post!</span>';
  	}
    die();
}

add_action('wp_ajax_is_user_logged_in', 'ajax_check_user_logged_in');
add_action('wp_ajax_nopriv_is_user_logged_in', 'ajax_check_user_logged_in');

function jx_scripts_function() {
	wp_enqueue_style( 'dashicons' );
  	wp_enqueue_style( 'jx-css-rating', get_template_directory_uri() . '/css/jx-star-rating.css');
  	wp_enqueue_script( 'jx-rating', get_template_directory_uri() . '/js/jx-star-rating.js');
  	wp_enqueue_script( 'jx-rateus', get_template_directory_uri() . '/js/jx-rateus.js');

}

add_action('wp_enqueue_scripts','jx_scripts_function');

function jx_ui_rating() {

	global $wpdb;
	$table_name = $wpdb->prefix . "jx_rating"; 
	$post_id=get_the_ID();
  	$user = wp_get_current_user();
  	if(is_user_logged_in()){
  		$result=$wpdb->get_row("SELECT * FROM $table_name WHERE user_id='".$user->ID."' AND post_id='".$post_id."'");  
	  	$count=$result->rating;
  	} else {
  		$count=0;
	  	/*$res=$wpdb->get_results("SELECT  * FROM $table_name WHERE post_id='".$post_id."'");  
		$rowcount = $wpdb->num_rows;

	  	$totalcount=$wpdb->get_row("SELECT  SUM(rating) as total FROM $table_name WHERE post_id='".$post_id."'");  
		if($rowcount==1){
	  		$result=$wpdb->get_row("SELECT  * FROM $table_name WHERE post_id='".$post_id."'");  
	  		$count=$result->rating;
		} else if($rowcount==0) {
			$count=0;
		} else if($rowcount>0) {
			$total=$totalcount->total/$rowcount;
			$count=number_format($total,1);
		} */
	}

	$html='';
	$html.='<style type="text/css"> .rating-container .clear-rating { display: none; }</style>';
	$html.='<div id="ratingMsg"></div>';
	$html.='<form method="post" id="rateSearch">';
	$html.='<div class="rateText">Rate This Post !</div>';
	$html.='<input value="'.$count.'" id="rateus" post_id="'.get_the_ID().'" type="text" class="rating" data-min=0 data-max=5 data-step=1 data-size="sm" title="">';
	$html.='<input value="Submit Rating" type="button" id="submitRating" class="submitRating">';
	$html.='</form>';

	$html.='<div id="ratingModel" class="modal fade" role="dialog">';
  	$html.='<div class="modal-dialog">';
    $html.='<div class="modal-content">';
    $html.='<div class="modal-header">';
    $html.='<button type="button" class="close" data-dismiss="modal">&times;</button>';
    $html.='<h4 class="modal-title">Rating</h4>';
    $html.='</div>';
    $html.='<div class="modal-body">';
    $html.='<p> For Rating, Please <a href="'.get_bloginfo('url').'/signin/">login or register</a> </p>';
    $html.='</div>';
    $html.='</div>';
  	$html.='</div>';
	$html.='</div>';
	return $html;
}
add_shortcode( 'JX_RATING_UI', 'jx_ui_rating' );

function jx_total_rating(){
	global $wpdb;
	$post_id=get_the_ID();
  	$user = wp_get_current_user();
	$table_name = $wpdb->prefix . "jx_rating"; 
  	$res=$wpdb->get_results("SELECT  * FROM $table_name WHERE post_id='".$post_id."'");  
	$rowcount = $wpdb->num_rows;

  	$totalcount=$wpdb->get_row("SELECT  SUM(rating) as total FROM $table_name WHERE post_id='".$post_id."'");  
	if($rowcount==1){
  		$result=$wpdb->get_row("SELECT  * FROM $table_name WHERE post_id='".$post_id."'");  
  		//print_r($result);
		return '<div class="tatal-rating"> '.$result->rating.' / 5 <a href="#ratenow">('.$rowcount.' Ratings )</a></div>';
	} else if($rowcount==0) {
		return '<div class="tatal-rating"> 0 / 5 <a href="#ratenow">('.$rowcount.' Ratings )</a></div>';
	} else if($rowcount>0) {
		$total=$totalcount->total/$rowcount;
		return '<div class="tatal-rating"> '.number_format($total,1).' / 5 <a href="#ratenow">('.$rowcount.' Ratings )</a></div>';
	}
}
add_shortcode( 'JX_RATING', 'jx_total_rating' );