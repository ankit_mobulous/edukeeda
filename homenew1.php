<div class="homepagemenedu">
<?php session_start();
   /* Template Name: Homenew1 */
   if($_SESSION["login"]) {
   } else {
     header('Location: https://edukeeda.com/signin/');
   }
   $userId = $_SESSION["login"][ "id"];
   get_header();
   
   if(isset($_SESSION["postdone"])) {
       echo "<script>alert('Your Post has been Successfully Submitted. Will be in public domain after approval from Admin');</script>";
       unset($_SESSION["postdone"]);
   }
   
   if(isset($_SESSION["wallpostdone"])) {
       echo "<script>alert('Your Post has been Successfully Submitted.');</script>";
       unset($_SESSION["wallpostdone"]);
   }
   ?>
<style>
   .direct p {
   color: #9a9a9a;
   }
   .direct a {
   color: #9a9a9a;
   }
   #people p.catp {
   font-size: 13px;
   margin-bottom: 0px;
   }
   #people p.rigwd {
   font-size: 13px;
   margin-bottom: 0px;
   }
   div#bookmarksdtls {
   margin-top: 75px;
   }
   .rap1 img.img-thumbnail {
   width: 60px;
   height: 60px;
   border-radius: 100%;
   }
   .rap1 .col-xs-8.col-md-8 {
   padding: 0px;
   }
   .rap1 .col-xs-4.col-md-4 {
   padding: 0px;
   }
   .subc img.img-thumbnail {
   height: 147px;
   }
   .subcate span {
   display: block;
   }
   #bookmarksdtls .subc .rightwd {
   float: initial;
   }
   #bookmarksdtls .subc {
   margin-top: 80px;
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   float: left;
   width: 100%;
   margin-bottom: 40px;
   padding: 10px;
   border: 1px solid #ddd;
   }
   #eventedu span.dateDisplay-day {
   font-size: 20px;
   font-weight: 800;
   line-height: 21px;
   color: #e96125;
   }
   #bookmarksdtls span.dateDisplay-day {
   font-size: 20px;
   font-weight: 800;
   line-height: 21px;
   color: #e96125;
   }
   #eventedu .subc {
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   float: left;
   width: 100%;
   margin-bottom: 40px;
   padding: 10px;
   border: 1px solid #ddd;
   margin-top: 32px;
   }
   #bookmarksdtls .subc {
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   float: left;
   width: 100%;
   margin-bottom: 40px;
   padding: 10px;
   border: 1px solid #ddd;
   margin-top: 32px;
   }
   #eventedu .subcate .categtp {
   margin-bottom: 7px;
   font-size: 15px;
   text-align: left;
   }
   #bookmarksdtls .subcate .categtp {
   margin-bottom: 7px;
   font-size: 15px;
   text-align: left;
   }
   #eventedu a.bloghdaedu {
   font-size: 20px;
   margin: 0;
   padding: 0;
   line-height: 20px;
   float: left;
   width: 100%;
   font-weight: 600;
   }
   #bookmarksdtls a.bloghdaedu {
   font-size: 20px;
   margin: 0;
   padding: 0;
   line-height: 20px;
   float: left;
   width: 100%;
   font-weight: 600;
   }
   #eventedu p.leftwd {
   color: #9e9292;
   margin-bottom: 0px;
   width:100%;
   }
   .memberpage p.leftwd{
   width:100%;
   }
   #bookmarksdtls p.leftwd {
   color: #9e9292;
   margin-bottom: 0px;
   width:100%;
   }
   #eventedu p.rightwd {
   margin-bottom: 0px;
   }
   #bookmarksdtls p.rightwd {
   margin-bottom: 0px;
   }
   #eventedu .datespn {
   position: absolute;
   top: 11px;
   left: 24px;
   background: #fff;
   padding: 7px;
   color: #060658;
   font-size: 24px;
   text-align: center;
   border: 1px solid lavenderblush;
   border-radius: 7px;
   line-height: 17px;
   }
   #bookmarksdtls .datespn {
   position: absolute;
   top: 11px;
   left: 24px;
   background: #fff;
   padding: 7px;
   color: #060658;
   font-size: 24px;
   text-align: center;
   border: 1px solid lavenderblush;
   border-radius: 7px;
   line-height: 17px;
   }
   #eventedu i.fa.fa-share-alt {
   float: right;
   }
   #bookmarksdtls i.fa.fa-share-alt {
   float: right;
   }
   .crose{
   float: left;
   width: 100%;
   text-align:right;
   }
   div#articlesedu{
   margin-top:77px;
   }
   .imgforr {
   text-align: center;
   }
   .imgforr img.img-thumbnail {
   width: 60px;
   height: 60!important;;
   border-radius: 100%;
   }
   .post span {
   margin-right: 6px;
   }
   .post strong {

   color: #373779;
   }
   #discussquest .col-md-9 {
    padding: 0px;
}
div#anwerspat {
    margin-top: 70px;
}
div#questionare {
    margin-top: 70px;
}
   .plugin {
   float: right;
   }
   .pp {
   float: left;
   width: 100%;
   display: -webkit-box;
   margin: 10px 0px;
   color:#9a9a9a;
   }
   .pp span {
   margin-right: 6px;
   }
   .pp strong {
   }
   #questionare .plugin {
   float: right;
   color: #9a9a9a;
   }
   #anwerspat .pp {
   color: #9a9a9a;
   }
   #anwerspat .share, .share1, share2 {
   color: #9a9a9a;
   }
   #wallpost .share, .share1, .share2 {
   color: #9a9a9a!important;
   }
   #wallpost .share1 button
   {
   color: #9a9a9a;
   }
   #wallpost .share2 p{
   color: #060658!important;
   }
   #wallpost .share1 p{
   color: #060658!important;
   }
   #wallpost .share p{
   color: #060658!important;
   }
   #anwerspat .share1 p {
   color: #9a9a9a;
   }
   #anwerspat .share2  {
   color: #9a9a9a;
   }
   #anwerspat .text a {
   color: #303030;
   }
   #anwerspat .share .fa  {
   color: #9a9a9a;
   }
   #anwerspat .share button {
   background: none;
   border: none;
   color: #9a9a9a;
   margin: 0px;
   padding: 0px;
   }
   #anwerspat .share1 .fa  {
   color: #9a9a9a;
   }
   #anwerspat .share2 .fa  {
   color: #9a9a9a;
   }
   #questionare .share .fa  {
   color: #9a9a9a;
   }
   #questionare .share1 .fa  {
   color: #9a9a9a;
   }
   #questionare .share2 .fa  {
   color: #9a9a9a;
   }
   #questionare .post
   {
   color: #9a9a9a;
   }
   #questionare .post strong
   {color: #9a9a9a;
   }
   #questionare .share p {
   color: #9a9a9a;
   }
   #questionare .share1 p {
   color: #9a9a9a;
   }
   #questionare .share2 p {
   color: #9a9a9a;
   }
   #questionare .share1 button{
   color: #9a9a9a;
   }
   #questionare .share2 button{
   color: #9a9a9a;
   }
   #questionare .share button{
   color: #9a9a9a;
   }
   .catp span {
   margin: 5px;
   color: #4c4242;
   }
   .text {
   float: left;
   width: 100%;
   margin-bottom: 15px;
   margin-top: 15px;
   }
   .text1 a {
   color: #000;
   }
   div#people {
   margin-top: 61px;
   }
   .memberpage .profile span
   {
   position:relative;
   }

   #anwerspat .pp {
   color: #9a9a9a;
   }
   #anwerspat .share, .share1, share2 {
   color: #9a9a9a;
   }
   #wallpost .share2 p{
   color: #060658!important;
   }
   #wallpost .share1 p{
   color: #060658!important;
   }
   #wallpost .share p{
   color: #060658!important;
   }
   #discussquest .share1 p {
   color: #9a9a9a;
   }
   #anwerspat .share2  {
   color: #9a9a9a;
   }
   #discussquest .share .fa  {
   color: #9a9a9a;
   }
   #discussquest .share1 .fa  {
   color: #9a9a9a;
   }
   #discussquest .share2 .fa  {
   color: #9a9a9a;
   }
   #discussquest .share .fa  {
   color: #9a9a9a;
   }
   #discussquest .share1 .fa  {
   color: #9a9a9a;
   }
   #discussquest .share2 .fa  {
   color: #9a9a9a;
   }
   #discussquest .post
   {
   color: #9a9a9a;
   }
   #discussquest .post strong
   {color: #9a9a9a;
   }
   #discussquest .share p {
   color: #9a9a9a;
   }
   #discussquest .share1 p {
   color: #9a9a9a;
   }
   #discussquest .share2 p {
   color: #9a9a9a;
   }
   #discussquest .share1 button{
   color: #9a9a9a;
   }
   #discussquest .share2 button{
   color: #9a9a9a;
   }
   #discussquest .share button{
   color: #9a9a9a;
   }
   .text p {
   font-weight: 800;
   line-height: 30px;
   font-size: 25px!important;
   }
   .share .fa {
   color: #9a9a9a;
   }
   .share1 .fa {
   color: #9a9a9a;
   }
   .share2 .fa {
   color: #9a9a9a;
   }
   .dot {
   float: right;
   }
   .questio {
   float: left;
   width: 100%;
   margin-bottom: 20px;
   }
   .dot span.countno {
   margin: 0px;
   float: left;
   }
   .dot ul.dropdown-menu {
   min-width: 217px;
   padding: 3px;
   }
   .modal-body p {
   margin-bottom: 0px;
   padding: 3px;
   }
   .post{
   float: left;
   width: 100%;
   display: -webkit-box;
   margin-bottom: 20px;
   }
   .text1 {
   float: left;
   width: 100%;
   margin-bottom: 0px;
   margin-top: 15px;
   }
   .share1 button {
   color: #9a9a9a;
   font-size: 14px;
   text-transform: capitalize;
   background: transparent;
   border: none;
   padding: 0 6px;
   }
   .share1 img {
   width: 15px;
   height: auto;
   }
   .text1 p {
   font-weight: 800;
   color: #4c4242;
   line-height: 23px;
   font-size: 21px!important;
   }
   .text1 h2 {
   font-size: 25px;
   color: #f68e2f;
   }
   .house button {
   font-size: 12px;
   margin: 14px;
   padding: 5px;
   color: #000;
   }
   .house {
   float: left;
   width: 100%;
   margin: 35px;
   margin-top: 50px;
   }
   .plug img {
   width: 20px;
   }
   .plug {
   text-align:right;}
   .cat a {
   color: #000000bd;
   font-weight: 600;
   }.catp a {
   color: #060658;
   font-weight: 600;
   }
   .cat span {
   color: #f68e2f;
   margin: 7px;
   }
   .cat {
   margin: 6px 0px;
   }
   .kol{
   float:left;
   width:100%;
   }
   .kol input[type="text"], .kol input[type="date"], .kol select {
   width: 100%;
   padding: 0 !important;
   height: 30px;
   box-shadow: none;
   border-radius: 0px;
   background: transparent;
   margin-bottom: 0;
   padding: 1px 8px !important;
   font-size: 15px;
   }
   .kol{margin-bottom: 5px!important;}
   .editname p {
   color: #060658!important;
   font-size: 30px!important;
   }
   .bs-example {
   float: right;
   width: 100%;
   position: absolute;
   top: 9px;
   left: -45px;
   }
   .prelocation {
   margin: 10px 0px;
   position: relative;
   }
   .bs-example a, .bsexam a, .bs a, .bs-examp a {
   border: 1px solid#DDD;
   padding: 2px;
   border-radius: 100px;
   color: #000;
   float:left;
   }
   .bsexam {
   float: left;
   width: 100%;
   position: absolute;
   top: -10px;
   left: 156px;
   }
   .bs {
   float: left;
   width: 100%;
   position: absolute;
   top: -17px;
   left: 161px;
   }
   .bs-examp {
   float: left;
   width: 100%;
   position: absolute;
   top: -12px;
   left: -12px;
   }
   .categtypeedus.atest {
   margin-bottom: 20px;
   }
   .editname {
   float: left;
   width: 100%;
   margin-bottom: 13px;
   }
   .memb i.fa.fa-pencil {
   color: #cccccc;
   }
   .memb{
   width:100%;
   }
   @media only screen and (max-width: 767px){
	   
   .memberpage .filtersorted p {
   float: right;
   margin: 0 0;
   text-align: right;
   position: absolute;
   right: 0;
   top: -18px;
   right: 78%;
   }.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px;
}
   .editname {
   float: left;
   width: 100%;
   margin-bottom: 10px;
   }
   .memberpage .filtersorted {
   float: right;
   width: 100%;
   position: relative;
   margin-top: 30px;
   }
   .memberpage .filtersorted p {
   float: right;
   margin: 0 0;
   text-align: right;
   position: absolute;
   right: 0;
   top: -18px;
   right: 78%;
   }
   center {
   float: left;
   width: 100%;
   margin-top: 10px;
   }
   .memberpage .filtersorted select {
   float: left;
   padding: 4px 0;
   height: 27px;
   position: absolute;
   right: 0;
   top: -17px;
   width: 70%;
   }
   .advtspace {
   float: left;
   width: 100%;
   }
   .conditionalbtn .follwbtn {
   padding: 7px 12px;
   font-size: 10px;
   line-height: 10px;
   margin: 0px 11px 1px 0px;
   background: transparent;
   color: #060658;
   float: right;
   }
   div#people {
   margin-top: 50px;
   }
   div#articlesedu {
   margin-top: 60px;
   }
   div#articlesedu{
   margin-top:10px;
   }
   div#eventedu {
   margin-top: 50px;
   }
   .memberpage {
   margin-top:0px;
   }
   .page-wrap{
	   padding:0px;
   }
   .memberpage .addposituon {
    float: left;
    width: 100%;
    padding: 0px 20px;
}
.other {
    float: left;
    width: 100%;
    padding: 0px 20px;
}
   .imga {
   margin-bottom: 20px;
   }
   }
   .col-md-3 .house{margin: 0px;}
   .plug button{background: none;padding: 0px;border:0px;color:#9a9a9a;}
   
   #discussquest .pluging {
    float: right;
    color: #9a9a9a;
}
 #discussquest .direct .col-md-10.col-xs-9 {
    padding: 0px;
}
div#discussquest {
    margin-top: 75px;
}
div#discgroup{
    margin-top: 75px;
}
div#intrestquest{
    margin-top: 75px;
}
div#peowal {
    margin-top: 75px;
}
</style>
<?php
   global $wpdb;
   $status=1 ;
   
   $results = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_users WHERE user_status = %d AND ID = %d", $status, $userId) );
   $results1 = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_user_pics WHERE user_id = %d", $results[0]->ID) );
   $results2 = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_profileStepOne WHERE user_id = %d", $results[0]->ID) );
   $results3 = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_profileStepTwo WHERE user_id = %d", $results[0]->ID) );
   $getCountry = $wpdb->get_results( $wpdb->prepare("SELECT * FROM countries WHERE id = %d", $results3[0]->country) );
   $getstate = $wpdb->get_results( $wpdb->prepare("SELECT * FROM states WHERE id = %d", $results3[0]->state) );
   $results4 = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_profileStepFour WHERE user_id = %d", $results[0]->ID) );
   $results5 = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_profileStepThree WHERE user_id = %d", $results[0]->ID) );
   $results6 = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_profileStepSeven WHERE user_id = %d", $results[0]->ID) );
   $results7 = $wpdb->get_results( $wpdb->prepare("SELECT * FROM wp_profileStepSix WHERE user_id = %d", $results[0]->ID) );
   
   if(isset($_POST["follow"])) {
      $cid = $_POST["catId"];
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $data = ["user_id"=>$userid, "cat_id"=>$cid, "cat_type"=>$catType];
        $wpdb->insert("wp_follow_cat", $data);
   }
   if(isset($_POST["unfollow"])) {
      $cid = $_POST["catId"];
        $catType = $_POST["catType"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $wpdb->query($wpdb->prepare("DELETE FROM wp_follow_cat WHERE user_id = %d AND cat_id = %d", $userid, $cid));
   }
   
   if(isset($_POST['walllikeSubmit'])) {
    $wallid = $_POST['wallid'];
    $userid = $_SESSION["login"]["id"];
    $data = ["user_id"=>$userid, "wallid"=>$wallid];
    $wpdb->insert("wp_wall_like", $data);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   
   if(isset($_POST['wallunlikeSubmit'])) {
     $wallid = $_POST['wallid'];
     $userid = $_SESSION["login"]["id"];
     $wpdb->query($wpdb->prepare("DELETE FROM wp_wall_like WHERE wallid = %d AND user_id=%d", $wallid, $userid));
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   
   if(isset($_POST['wallreplypostsubmit'])) {
     $wallid = $_POST['wallid'];
     $userid = $_SESSION["login"]["id"];
     $replyPost = $_POST['replyPost'];
     $data = ["user_id"=>$userid, "wallid"=>$wallid, "reply"=>$replyPost];
     $wpdb->insert("wp_wall_reply", $data);
     $msgReply = "Reply successfully. Waiting for admin approval";
   }
   
   if(isset($msgReply)) { ?>
<script type="text/javascript">
   alert("Answer posted successfully. Waiting for admin approval");
</script>
<?php
   unset($msgReply);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
     if(isset($_POST['wallrepostsubmit'])) {
         $wallid = $_POST['wallid'];
         $walltext = $_POST['walltext'];
         $reposttext = $_POST['reposttext'];
         $userid = $_SESSION["login"]["id"];
         $data = ["user_id"=>$userid, "conetnt"=>$walltext, "share"=>1, "shareid"=>$wallid, "sharetext"=>$reposttext];
         $wpdb->insert("wp_wallpost", $data);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }

   if(isset($_POST['repostsubmit'])) {

      $type = $_POST['type'];
      $quesid = $_POST['quesid'];
      $ques = $_POST['ques'];
      $reposttext = $_POST['reposttext'];
      $userid = $_SESSION["login"]["id"];
      $data = ["user_id"=>$userid, "content"=>$ques, "shareType"=>$type, "shareid"=>$quesid, "sharetext"=>$reposttext]; 
      $wpdb->insert("wp_wallpost", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }

  if(isset($_POST['repostanssubmit'])) {
      $type = $_POST['type'];
      $ans = $_POST['ans'];
      $ansid = $_POST['ansid'];
      $reposttext = $_POST['reposttext'];
      $userid = $_SESSION["login"]["id"];

      $data = ["user_id"=>$userid, "content"=>$ans, "shareType"=>$type, "shareid"=>$ansid, "sharetext"=>$reposttext];
      $wpdb->insert("wp_wallpost", $data);
?>
    <script type="text/javascript">
        if ( window.history.replaceState ) {
            window.history.replaceState( null, null, window.location.href );
        }
    </script>
<?php
  }
   
   if(isset($_POST['submitReport'])) {
     $qid = $_POST['qid'];
     $reason = $_POST['reason'];
     $userid = $_SESSION["login"]["id"];
   
     $data = ["user_id"=>$userid, "quesid"=>$qid, "status"=>"report", "reason"=>$reason];
     $wpdb->insert("wp_question_hide", $data);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   if(isset($_POST['anspostsubmit'])) {
     $qid = $_POST['qid'];
     $userid = $_SESSION["login"]["id"];
     $ansPost = $_POST['ansPost'];
     $data = ["ques_id"=>$qid, "user_id"=>$userid, "ans_content"=>$ansPost];
     $wpdb->insert("wp_discussion_ans", $data);
     $msgAns = "Answer posted successfully. Waiting for admin approval";
   }
   
   if(isset($msgAns)) { ?>
<script type="text/javascript">
   alert("Answer posted successfully. Waiting for admin approval");
</script>
<?php  
   unset($msgAns); 
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   
   if(isset($_POST["quesfollow"])) {
         $quesId = $_POST["quesId"];
         $userid = $_SESSION["login"]["id"];
         $data = ["ques_id"=>$quesId, "user_id"=>$userid];
         $wpdb->insert("wp_discussion_ques_follow", $data);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   if(isset($_POST["quesunfollow"])) {
       $quesId = $_POST["quesId"];
       $userid = $_SESSION["login"]["id"];
       $wpdb->query($wpdb->prepare("DELETE FROM wp_discussion_ques_follow WHERE ques_id = %d AND user_id=%d", $quesId, $userid));
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   if(isset($_POST['repostanssubmit'])) {
       $qid = $_POST['qid'];
       $ans = $_POST['ans'];
       $ansid = $_POST['ansid'];
       $reposttext = $_POST['reposttext'];
       $userid = $_SESSION["login"]["id"];
   
     $data = ["ques_id" => $qid, "user_id"=>$userid, "ans_content"=>$ans, "share"=>1, "shareid"=>$ansid, "sharetext"=>$reposttext];
     $wpdb->insert("wp_discussion_ans", $data);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   if(isset($_POST['replypostsubmit'])) {
     $rid = $_POST['rid'];
     $userid = $_SESSION["login"]["id"];
     $replyPost = $_POST['replyPost'];
     $data = ["user_id"=>$userid, "ans_id"=>$rid, "reply"=>$replyPost];
     $wpdb->insert("wp_discussion_reply", $data);
     $msgReply = "Reply successfully. Waiting for admin approval";
   }
   
   if(isset($msgReply)) { ?>
<script type="text/javascript">
   alert("Answer posted successfully. Waiting for admin approval");
</script>
<?php
   unset($msgReply);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   if(isset($_POST['anslikeSubmit'])) {
       $ansid = $_POST['ansid'];
       $userid = $_SESSION["login"]["id"];
       $data = ["user_id"=>$userid, "ans_id"=>$ansid];
       $wpdb->insert("wp_answer_like", $data);
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   
   if(isset($_POST['ansunlikeSubmit'])) {
     $ansid = $_POST['ansid'];
     $userid = $_SESSION["login"]["id"];
     $wpdb->query($wpdb->prepare("DELETE FROM wp_answer_like WHERE ans_id = %d AND user_id=%d", $ansid, $userid));
   ?>
<script type="text/javascript">
   if ( window.history.replaceState ) {
       window.history.replaceState( null, null, window.location.href );
   }
</script>
<?php
   }
   ?>
   <div class="memberpage">
   <div class="row">
      <div class="col-md-9">
         <div class="">
            <div class="memb">
               <div class="profile">
                  <div class="col-md-3">
                     <div class="imga">
                        <span class="countno" data-toggle="modal" data-target="#myModalImg" style="float:right;margin-left: 0px;">
                           <div class="bs-example">
                              <a href="#" data-toggle="tooltip" title="Update Image"><i class="fa fa-pencil"></i></a>
                           </div>
                        </span>
                        <img src="<?php if($results1){echo $results1[0]->profilePic;}else{echo "https://edukeeda.com/wp-content/uploads/2019/02/blank-profile-picture-973460_640-300x300.png";}?>" class="img-thumbnail" alt="Cinque Terre" width="304" height="236">
                        <p class="joineon">Joined on <?php echo date( "d-M-Y",strtotime($results[0]->user_registered));?></p>
                     </div>
			    </div>
				<div class="seperation"></div>
				<div class="col-md-9">
            <a href="https://edukeeda.com/postyourquerypage/">
               <div class="memb1">
                  <p>Want to Post updates/Article/Events/Questions?</p>
               </div>
            </a>
            <div class="arteduro">
               <div class="row">
                  <div class="col-md-12 col-sm-12 basictabsed">
                     <div class="tab">
                        <button class="tablinks" onclick="openCity(event, 'peowal')">People's wallPosts</button>
                        <button class="tablinks" onclick="openCity(event, 'people')">People</button>
                        <button class="tablinks" onclick="openCity(event, 'dicfor')">Discussion Forum</button>
                        <button class="tablinks" onclick="openCity(event, 'articlesedu')">Post / Articles / Blogs</button>
                        <button class="tablinks" onclick="openCity(event, 'eventedu')">Events</button>
						 
                     </div>
                     <div class="filtersorted">
                        <form>
                           <p>Sort By:</p>
                           <select>
                              <option value="date">Most Views</option>
                              <option value="recent">Recent</option>
                           </select>
                        </form>
                     </div>
                     <div id="peowal" class="tabcontent" style="display:block;">
                          
                        <div class="blogaresnews">
                           <div class="col-md-12 blo">
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>, <span>Student Consultant</span>
                                 </p>
                                 <p class="rigwd">January 14, 2019. 6:20 am </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p></p><p>Not everyone finds themselves jumping in the air on exam results day for an enthusiastic newspaper photographer. While some students receive the grades they want, others will be more than disappointed.</p><p></p>
                                                                     </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>3</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>1</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>0</span><strong>Repost</strong>
                                    </p>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <form method="post" action="">
                                       <input type="hidden" name="wallid" value="1">
                                       <button type="submit" name="wallunlikeSubmit" class="unlikecolor"><strong><i class="fa fa-thumbs-up" aria-hidden="true"></i>like</strong></button>
                                    </form>
                                                                     </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalrep11"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrep1" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrep11" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <input type="hidden" name="wallid" value="1">
                                                <textarea name="replyPost"> </textarea>
                                                <input type="submit" name="wallreplypostsubmit" value="Reply Submit">
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost11"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost1" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost11" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                   <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p></p><p>Not everyone finds themselves jumping in the air on exam results day for an enthusiastic newspaper photographer. While some students receive the grades they want, others will be more than disappointed.</p><p></p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <input type="hidden" name="wallid" value="1">
                                                <input type="hidden" name="walltext" value="<p>Not everyone finds themselves jumping in the air on exam results day for an enthusiastic newspaper photographer. While some students receive the grades they want, others will be more than disappointed.</p>">
                                                <p> <input type="submit" name="wallrepostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                          
                        <div class="blogaresnews">
                           <div class="col-md-12 blo">
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>, <span>Student Consultant</span>
                                 </p>
                                 <p class="rigwd">January 14, 2019. 6:20 am </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                                     </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>1</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>0</span><strong>Repost</strong>
                                    </p>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <form method="post" action="">
                                       <input type="hidden" name="wallid" value="3">
                                       <button type="submit" name="wallunlikeSubmit" class="unlikecolor"><strong><i class="fa fa-thumbs-up" aria-hidden="true"></i>like</strong></button>
                                    </form>
                                                                     </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalrep22"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrep2" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrep22" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <input type="hidden" name="wallid" value="3">
                                                <textarea name="replyPost"> </textarea>
                                                <input type="submit" name="wallreplypostsubmit" value="Reply Submit">
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost22"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost2" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost22" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                   <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <input type="hidden" name="wallid" value="3">
                                                <input type="hidden" name="walltext" value="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.">
                                                <p> <input type="submit" name="wallrepostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                          
                        <div class="blogaresnews">
                           <div class="col-md-12 blo">
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>, <span>Student Consultant</span>
                                 </p>
                                 <p class="rigwd">January 14, 2019. 6:20 am </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p>

</p><blockquote>helo</blockquote>

<p></p>
                                                                     </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>0</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>0</span><strong>Repost</strong>
                                    </p>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <form method="post" action="">
                                       <input type="hidden" name="wallid" value="4">
                                       <button type="submit" name="walllikeSubmit"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                    </form>
                                                                     </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalrep33"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrep3" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrep33" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <input type="hidden" name="wallid" value="4">
                                                <textarea name="replyPost"> </textarea>
                                                <input type="submit" name="wallreplypostsubmit" value="Reply Submit">
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost33"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost3" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost33" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                   <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p>

</p><blockquote>helo</blockquote>

<p></p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <input type="hidden" name="wallid" value="4">
                                                <input type="hidden" name="walltext" value="

<blockquote>helo</blockquote>

">
                                                <p> <input type="submit" name="wallrepostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                          
                        <div class="blogaresnews">
                           <div class="col-md-12 blo">
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>, <span>Student Consultant</span>
                                 </p>
                                 <p class="rigwd">January 14, 2019. 6:20 am </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                      
                                    <div class="sharing">
                                       <div class="left">
                                          <div class="imgforr">
                                             <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></a>
                                          </div>
                                       </div>
                                       <div class="right">
                                          <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal</a></strong>  <span>Student Consultant</span> </p>
                                       </div>
                                       <p>  consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                    </div>
                                                                     </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>0</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>0</span><strong>Repost</strong>
                                    </p>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <form method="post" action="">
                                       <input type="hidden" name="wallid" value="5">
                                       <button type="submit" name="walllikeSubmit"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                    </form>
                                                                     </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalrep44"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrep4" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrep44" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <input type="hidden" name="wallid" value="5">
                                                <textarea name="replyPost"> </textarea>
                                                <input type="submit" name="wallreplypostsubmit" value="Reply Submit">
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost44"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost4" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost44" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                   <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <input type="hidden" name="wallid" value="5">
                                                <input type="hidden" name="walltext" value=" Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip">
                                                <p> <input type="submit" name="wallrepostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                          
                        <div class="blogaresnews">
                           <div class="col-md-12 blo">
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>, <span>Student Consultant</span>
                                 </p>
                                 <p class="rigwd">January 14, 2019. 6:20 am </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p>quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia v</p>
                                                                     </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>1</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>1</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>1</span><strong>Repost</strong>
                                    </p>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <form method="post" action="">
                                       <input type="hidden" name="wallid" value="8">
                                       <button type="submit" name="wallunlikeSubmit" class="unlikecolor"><strong><i class="fa fa-thumbs-up" aria-hidden="true"></i>like</strong></button>
                                    </form>
                                                                     </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalrep55"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrep5" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrep55" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <input type="hidden" name="wallid" value="8">
                                                <textarea name="replyPost"> </textarea>
                                                <input type="submit" name="wallreplypostsubmit" value="Reply Submit">
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost55"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost5" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost55" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                   <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p>quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia v</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <input type="hidden" name="wallid" value="8">
                                                <input type="hidden" name="walltext" value="quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia v">
                                                <p> <input type="submit" name="wallrepostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                          
                        <div class="blogaresnews">
                           <div class="col-md-12 blo">
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>, <span>Student Consultant</span>
                                 </p>
                                 <p class="rigwd">January 14, 2019. 6:20 am </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p> exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure</p>
                                      
                                    <div class="sharing">
                                       <div class="left">
                                          <div class="imgforr">
                                             <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></a>
                                          </div>
                                       </div>
                                       <div class="right">
                                          <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal</a></strong>  <span>Student Consultant</span> </p>
                                       </div>
                                       <p> quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia v </p>
                                    </div>
                                                                     </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>1</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>1</span><strong>Repost</strong>
                                    </p>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <form method="post" action="">
                                       <input type="hidden" name="wallid" value="9">
                                       <button type="submit" name="wallunlikeSubmit" class="unlikecolor"><strong><i class="fa fa-thumbs-up" aria-hidden="true"></i>like</strong></button>
                                    </form>
                                                                     </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalrep66"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrep6" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrep66" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <input type="hidden" name="wallid" value="9">
                                                <textarea name="replyPost"> </textarea>
                                                <input type="submit" name="wallreplypostsubmit" value="Reply Submit">
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost66"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost6" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost66" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                   <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p> exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <input type="hidden" name="wallid" value="9">
                                                <input type="hidden" name="walltext" value=" exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure">
                                                <p> <input type="submit" name="wallrepostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                          
                        <div class="blogaresnews">
                           <div class="col-md-12 blo">
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>, <span>Student Consultant</span>
                                 </p>
                                 <p class="rigwd">January 14, 2019. 6:20 am </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p> helo</p>
                                      
                                    <div class="sharing">
                                       <div class="left">
                                          <div class="imgforr">
                                             <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></a>
                                          </div>
                                       </div>
                                       <div class="right">
                                          <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal</a></strong>  <span>Student Consultant</span> </p>
                                       </div>
                                       <p>  hi </p>
                                    </div>
                                                                     </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>0</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>1</span><strong>Repost</strong>
                                    </p>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <form method="post" action="">
                                       <input type="hidden" name="wallid" value="10">
                                       <button type="submit" name="walllikeSubmit"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                    </form>
                                                                     </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalrep77"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrep7" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrep77" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <input type="hidden" name="wallid" value="10">
                                                <textarea name="replyPost"> </textarea>
                                                <input type="submit" name="wallreplypostsubmit" value="Reply Submit">
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost77"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost7" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost77" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                   <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p> helo</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <input type="hidden" name="wallid" value="10">
                                                <input type="hidden" name="walltext" value=" helo">
                                                <p> <input type="submit" name="wallrepostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                          
                        <div class="blogaresnews">
                           <div class="col-md-12 blo">
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>, <span>Student Consultant</span>
                                 </p>
                                 <p class="rigwd">January 14, 2019. 6:20 am </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p>boris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
                                      
                                    <div class="sharing">
                                       <div class="left">
                                          <div class="imgforr">
                                             <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="200" height="200"></a>
                                          </div>
                                       </div>
                                       <div class="right">
                                          <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal</a></strong>  <span>Student Consultant</span> </p>
                                       </div>
                                       <p>  liquip ex ea commodo consequat. Duis aute irure d </p>
                                    </div>
                                                                     </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>0</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>1</span><strong>Repost</strong>
                                    </p>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <form method="post" action="">
                                       <input type="hidden" name="wallid" value="11">
                                       <button type="submit" name="walllikeSubmit"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                    </form>
                                                                     </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalrep88"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrep8" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrep88" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Reply</h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <input type="hidden" name="wallid" value="11">
                                                <textarea name="replyPost"> </textarea>
                                                <input type="submit" name="wallreplypostsubmit" value="Reply Submit">
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-xs-3 col-md-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost88"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost8" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost88" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                   <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p>boris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu </p>
                                                      </div>
                                                   </div>
                                                </div>
                                                <input type="hidden" name="wallid" value="11">
                                                <input type="hidden" name="walltext" value="boris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu ">
                                                <p> <input type="submit" name="wallrepostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                                             </div>

                     <div id="people" class="tabcontent" style="display:none;">
                      
                        <div class="col-md-6">
                           <div class="allprofiles">
                              <div class="rap1">
                                 <div class="col-xs-4 col-md-4">
                                    <div class="imgg">
                                       <a href="https://edukeeda.com/newmembers?action=60">
                                       <img src="https://edukeeda.com/wp-content/uploads/2019/03/ca060a69056.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                       </a>
                                    </div>
                                 </div>
                                 <div class="col-xs-8 col-md-8">
                                    <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=60">Ashish Prasad, , 5 Years of Expe..</a></strong></p>
                                    <!--<p class="rigwd"><strong> Balakot, Ebon, Marshall Islands 
                                       </strong>
                                    </p>-->
                                 </div>
                              </div>
                           </div>
                        </div>
                      
                        <div class="col-md-6">
                           <div class="allprofiles">
                              <div class="rap1">
                                 <div class="col-xs-4 col-md-4">
                                    <div class="imgg">
                                       <a href="https://edukeeda.com/newmembers?action=49">
                                       <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                       </a>
                                    </div>
                                 </div>
                                 <div class="col-xs-8 col-md-8">
                                    <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal, , Student Consult..</a></strong></p>
                                    <!--<p class="rigwd"><strong> Balakot, Ebon, Marshall Islands 
                                       </strong>
                                    </p>-->
                                 </div>
                              </div>
                           </div>
                        </div>
                                           </div>


                     <div id="dicfor" class="tabcontent" style="display:none;">
                     </div>
                     

                     <div id="articlesedu" class="tabcontent" style="display:none;">
                                                <div class="col-md-12 blo">
                           <div class="">
                              <div class="col-md-4 col-sm-4">
                                 <a href="https://edukeeda.com/industry/rma-cementing-alliances-in-northwestern-bc/">
                                 <img src="https://edukeeda.com/wp-content/uploads/2019/02/31fe75f5b46.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                 </a>
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-8">
                              <p class="categtp">
                                 <a href="https://edukeeda.com/expertise/?id=82&amp;action=industry">MINING, COAL &amp; METAL</a>
                              </p>
                              <a class="bloghdaedu" href="https://edukeeda.com/industry/rma-cementing-alliances-in-northwestern-bc/"> RMA cementing alliances in northwestern BC </a>
                              <p class="fullwd"></p>
                              <p class="leftwd"><strong>By </strong> <a href="https://edukeeda.com/newmembers/?action=49"> Ankit Mittal </a>                                 <a href="#" class="rightwd views"><i class="fa fa-eye"></i> 357 Views</a>
                              </p>
                              <p class="rightwd"><strong> </strong> January 25th, 2019 . 7:16 am</p>
                           </div>
                        </div>
                                                <div class="col-md-12 blo">
                           <div class="">
                              <div class="col-md-4 col-sm-4">
                                 <a href="https://edukeeda.com/industry/advancing-women-in-science/">
                                 <img src="https://edukeeda.com/wp-content/uploads/2019/02/70244cb789c.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                 </a>
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-8">
                              <p class="categtp">
                                 <a href="https://edukeeda.com/expertise/?id=87&amp;action=industry">AUTOMOBILES</a>
                              </p>
                              <a class="bloghdaedu" href="https://edukeeda.com/industry/advancing-women-in-science/"> Advancing Women in Science </a>
                              <p class="fullwd"></p>
                              <p class="leftwd"><strong>By </strong> <a href="https://edukeeda.com/newmembers/?action=49"> Ankit Mittal </a>                                 <a href="#" class="rightwd views"><i class="fa fa-eye"></i> 111 Views</a>
                              </p>
                              <p class="rightwd"><strong> </strong> January 25th, 2019 . 7:16 am</p>
                           </div>
                        </div>
                                                                                                                     </div>
                     <div id="eventedu" class="tabcontent" style="display:none;">
                                                <div class="subc">
                           <div class="col-md-4 col-sm-5">
                              <div class="altimg">
                                 <a href="https://edukeeda.com/events/one-globe-uniting-knowledge-communities/"><img class="img-thumbnail" src="https://edukeeda.com/wp-content/uploads/2018/05/men-521540_640.jpg" style="height: 155px;"></a>
                                 <div class="datespn">
                                    <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                                    <span class="dateDisplay-day">
                                    <span>28</span><br>
                                    </span>
                                    <span class="dateDisplay-month text--tiny">
                                    <span>Mar</span>
                                    </span>
                                    </time>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-7">
                              <div class="subcate">
                                 <p class="categtp">
                                    <a href="https://edukeeda.com/events-2/"> Event</a>
                                    <a href="https://edukeeda.com/events-list/?id=117"> . Examination</a>
                                 </p>
                                 <a class="bloghdaedu" href="https://edukeeda.com/events/one-globe-uniting-knowledge-communities//">One Globe: Uniting Knowledge Communities</a>
                              </div>
                              <p class="leftwd">
                                 <strong>
                                 Post By :</strong><a href="https://edukeeda.com/newmembers/?action="> Ankit Mittal </a><i class="fa fa-share-alt" aria-hidden="true"></i>
                              </p>
                              <p class="rightwd"><strong> </strong> February 2nd, 2019 06:40AM</p>
                           </div>
                        </div>
                                                <div class="subc">
                           <div class="col-md-4 col-sm-5">
                              <div class="altimg">
                                 <a href="https://edukeeda.com/events/geophysical-information-for-teachers-2/"><img class="img-thumbnail" src="https://edukeeda.com/wp-content/uploads/2019/01/entertainment.jpeg" style="height: 155px;"></a>
                                 <div class="datespn">
                                    <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                                    <span class="dateDisplay-day">
                                    <span>01</span><br>
                                    </span>
                                    <span class="dateDisplay-month text--tiny">
                                    <span>Apr</span>
                                    </span>
                                    </time>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-7">
                              <div class="subcate">
                                 <p class="categtp">
                                    <a href="https://edukeeda.com/events-2/"> Event</a>
                                    <a href="https://edukeeda.com/events-list/?id=118"> . Seminar/Conference/Intellectual Discussion</a>
                                 </p>
                                 <a class="bloghdaedu" href="https://edukeeda.com/events/geophysical-information-for-teachers-2//">Geophysical Information For Teachers</a>
                              </div>
                              <p class="leftwd">
                                 <strong>
                                 Post By :</strong><a href="https://edukeeda.com/newmembers/?action="> Ankit Mittal </a><i class="fa fa-share-alt" aria-hidden="true"></i>
                              </p>
                              <p class="rightwd"><strong> </strong> February 20th, 2019 08:51AM</p>
                           </div>
                        </div>
                                                <div class="subc">
                           <div class="col-md-4 col-sm-5">
                              <div class="altimg">
                                 <a href="https://edukeeda.com/events/heads-and-chairs/"><img class="img-thumbnail" src="https://edukeeda.com/wp-content/uploads/2018/11/event.jpg" style="height: 155px;"></a>
                                 <div class="datespn">
                                    <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                                    <span class="dateDisplay-day">
                                    <span>20</span><br>
                                    </span>
                                    <span class="dateDisplay-month text--tiny">
                                    <span>Mar</span>
                                    </span>
                                    </time>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-7">
                              <div class="subcate">
                                 <p class="categtp">
                                    <a href="https://edukeeda.com/events-2/"> Event</a>
                                    <a href="https://edukeeda.com/events-list/?id=118"> . Seminar/Conference/Intellectual Discussion</a>
                                 </p>
                                 <a class="bloghdaedu" href="https://edukeeda.com/events/heads-and-chairs//">Heads and Chairs</a>
                              </div>
                              <p class="leftwd">
                                 <strong>
                                 Post By :</strong><a href="https://edukeeda.com/newmembers/?action="> Ankit Mittal </a><i class="fa fa-share-alt" aria-hidden="true"></i>
                              </p>
                              <p class="rightwd"><strong> </strong> February 20th, 2019 09:30AM</p>
                           </div>
                        </div>
                                             </div>

                     <div id="questionare" class="tabcontent" style="display:none;">
                        <div class="blogaresnews">
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">
                                    <p> <a href="https://edukeeda.com/discussioncategory?action=4">Industry/business</a> .  <a href="https://edukeeda.com/discussioncategory/?action=4&amp;actions=38">Railways</a> . <a href="https://edukeeda.com/discussion-house?action=7">longest railway platform</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                 <div class="dot">
                                    <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide1">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport1">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide1" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="12">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport1" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="12">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-12">
                                 <div class="text1">
                                    <p><a href="https://edukeeda.com/questionanswers?action=12"> helo</a></p>
                                 </div>
                              </div>
                              
                              <div class="questio">
                                 <div class="col-md-12 col-sm-12">
                                    <p class="rigwd"><strong>Answered on  </strong>  - </p>
                                 </div>
                              </div>

                              <div class="col-md-9">
                                 <div class="post">
                                    <p><span>0</span><strong>Answers</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlista1"><span>2</span><strong>Repost</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>5</span><strong>Views</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalfollowlista1"><span>1</span><strong>Followers</strong>
                                    </p>
                                 </div>
                                                   <!---- Modal  --->
                                                      <div id="myModalrepostlista1" class="modal fade" role="dialog">
                                                      <div class="modal-dialog" style="width: 700px;">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">×</button>
                                                               <h3></h3>
                                                            </div>
                                                            <div class="modal-body">
                                                               <div class="row">
                                                                  <div clss="col-md-12">
                                                                     
                                                                     <div class="col-md-6">
                                                                      <div class="poll">
                                                                         <div class="col-md-3 col-sm-2 col-xs-3">
                                                                            <div class="imgforr">
                                                                               <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                            </div>
                                                                         </div>
                                                                         <div class="col-md-9 col-sm-10 col-xs-9">
                                                                            <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ankit Mittal</a></strong> , Student Consult..</p>
                                                                         </div>
                                                                      </div>
                                                                   </div>
                                                                     
                                                                     <div class="col-md-6">
                                                                      <div class="poll">
                                                                         <div class="col-md-3 col-sm-2 col-xs-3">
                                                                            <div class="imgforr">
                                                                               <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/03/ca060a69056.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                            </div>
                                                                         </div>
                                                                         <div class="col-md-9 col-sm-10 col-xs-9">
                                                                            <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ashish Prasad</a></strong> , 5 Years of Expe..</p>
                                                                         </div>
                                                                      </div>
                                                                   </div>
                                                                                                                                    </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      </div>

                                                    <!---- Modal  --->
                                                      <div id="myModalfollowlista1" class="modal fade" role="dialog">
                                                         <div class="modal-dialog" style="width: 700px;">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                               <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal">×</button>
                                                                  <h3></h3>
                                                               </div>
                                                               <div class="modal-body">
                                                                  <div class="row">
                                                                     <div clss="col-md-12">
                                                                        
                                                                        <div class="col-md-6">
                                                                         <div class="poll">
                                                                            <div class="col-md-3 col-sm-2 col-xs-3">
                                                                               <div class="imgforr">
                                                                                  <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/03/ca060a69056.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                               </div>
                                                                            </div>
                                                                            <div class="col-md-9 col-sm-10 col-xs-9">
                                                                               <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ashish Prasad</a></strong> , 5 Years of Expe..</p>
                                                                            </div>
                                                                         </div>
                                                                        </div>
                                                                                                                                          </div>
                                                                  </div>
                                                               </div>

                                                            </div>
                                                         </div>
                                                      </div>

                              </div>

                              <div class="col-md-3">
                                 <div class="plugin"> <i class="fa fa-share-alt" aria-hidden="true"></i>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share">
                                                                        <p data-toggle="modal" data-target="#myModalans11"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                                                     </div>
                                       <!-- Modal -->
                                       <div id="myModalans1" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <p> For post an answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Modal -->
                                       <div id="myModalans11" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="12">
                                                      <textarea name="ansPost"> </textarea>
                                                      <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share1">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="quesId" value="12">
                                               <img src="/img/follow-icon-selected.png"><button type="submit" name="quesunfollow"> Follow </button>
                                            </form>
                                                                     </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="share2">
                                                                              <p data-toggle="modal" data-target="#myModalrepost1"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost11" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost1" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <form method="post" action="">
                                                         <p> <textarea name="reposttext"> </textarea></p>
                                                         <div class="repostmodal">
                                                           <p> Repost this question </p>
                                                           <p> Question :   helo </p>
                                                         </div>
                                                         <input type="hidden" name="type" value="ques">
                                                         <input type="hidden" name="quesid" value="12">
                                                         <input type="hidden" name="ques" value=" helo">
                                                         <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                      </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">
                                    <p> <a href="https://edukeeda.com/discussioncategory?action=4">Industry/business</a> .  <a href="https://edukeeda.com/discussioncategory/?action=4&amp;actions=38">Railways</a> . <a href="https://edukeeda.com/discussion-house?action=7">longest railway platform</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                 <div class="dot">
                                    <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide2">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport2">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide2" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="10">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport2" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="10">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="10">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="10">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-12">
                                 <div class="text1">
                                    <p><a href="https://edukeeda.com/questionanswers?action=10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</a></p>
                                 </div>
                              </div>
                              
                              <div class="questio">
                                 <div class="col-md-12 col-sm-12">
                                    <p class="rigwd"><strong>Answered on  </strong> 2 weeks ago</p>
                                 </div>
                              </div>

                              <div class="col-md-9">
                                 <div class="post">
                                    <p><span>4</span><strong>Answers</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlista2"><span>1</span><strong>Repost</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>88</span><strong>Views</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalfollowlista2"><span>2</span><strong>Followers</strong>
                                    </p>
                                 </div>
                                                   <!---- Modal  --->
                                                      <div id="myModalrepostlista2" class="modal fade" role="dialog">
                                                      <div class="modal-dialog" style="width: 700px;">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">×</button>
                                                               <h3></h3>
                                                            </div>
                                                            <div class="modal-body">
                                                               <div class="row">
                                                                  <div clss="col-md-12">
                                                                     
                                                                     <div class="col-md-6">
                                                                      <div class="poll">
                                                                         <div class="col-md-3 col-sm-2 col-xs-3">
                                                                            <div class="imgforr">
                                                                               <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                            </div>
                                                                         </div>
                                                                         <div class="col-md-9 col-sm-10 col-xs-9">
                                                                            <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ankit Mittal</a></strong> , Student Consult..</p>
                                                                         </div>
                                                                      </div>
                                                                   </div>
                                                                                                                                    </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      </div>

                                                    <!---- Modal  --->
                                                      <div id="myModalfollowlista2" class="modal fade" role="dialog">
                                                         <div class="modal-dialog" style="width: 700px;">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                               <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal">×</button>
                                                                  <h3></h3>
                                                               </div>
                                                               <div class="modal-body">
                                                                  <div class="row">
                                                                     <div clss="col-md-12">
                                                                        
                                                                        <div class="col-md-6">
                                                                         <div class="poll">
                                                                            <div class="col-md-3 col-sm-2 col-xs-3">
                                                                               <div class="imgforr">
                                                                                  <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                               </div>
                                                                            </div>
                                                                            <div class="col-md-9 col-sm-10 col-xs-9">
                                                                               <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ankit Mittal</a></strong> , Student Consult..</p>
                                                                            </div>
                                                                         </div>
                                                                        </div>
                                                                        
                                                                        <div class="col-md-6">
                                                                         <div class="poll">
                                                                            <div class="col-md-3 col-sm-2 col-xs-3">
                                                                               <div class="imgforr">
                                                                                  <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/03/ca060a69056.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                               </div>
                                                                            </div>
                                                                            <div class="col-md-9 col-sm-10 col-xs-9">
                                                                               <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ashish Prasad</a></strong> , 5 Years of Expe..</p>
                                                                            </div>
                                                                         </div>
                                                                        </div>
                                                                                                                                          </div>
                                                                  </div>
                                                               </div>

                                                            </div>
                                                         </div>
                                                      </div>

                              </div>

                              <div class="col-md-3">
                                 <div class="plugin"> <i class="fa fa-share-alt" aria-hidden="true"></i>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share">
                                                                        <p data-toggle="modal" data-target="#myModalans22"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                                                     </div>
                                       <!-- Modal -->
                                       <div id="myModalans2" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <p> For post an answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Modal -->
                                       <div id="myModalans22" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="10">
                                                      <textarea name="ansPost"> </textarea>
                                                      <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share1">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="quesId" value="10">
                                               <img src="/img/follow-icon-selected.png"><button type="submit" name="quesunfollow"> Follow </button>
                                            </form>
                                                                     </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="share2">
                                                                              <p data-toggle="modal" data-target="#myModalrepost2"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost22" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost2" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <form method="post" action="">
                                                         <p> <textarea name="reposttext"> </textarea></p>
                                                         <div class="repostmodal">
                                                           <p> Repost this question </p>
                                                           <p> Question :   Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip </p>
                                                         </div>
                                                         <input type="hidden" name="type" value="ques">
                                                         <input type="hidden" name="quesid" value="10">
                                                         <input type="hidden" name="ques" value=" Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip">
                                                         <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                      </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">
                                    <p> <a href="https://edukeeda.com/discussioncategory?action=4">Industry/business</a> .  <a href="https://edukeeda.com/discussioncategory/?action=4&amp;actions=38">Railways</a> . <a href="https://edukeeda.com/discussion-house?action=7">longest railway platform</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                 <div class="dot">
                                    <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide3">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport3">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide3" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="6">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport3" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="6">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="6">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="6">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-12">
                                 <div class="text1">
                                    <p><a href="https://edukeeda.com/questionanswers?action=6">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                 </div>
                              </div>
                              
                              <div class="questio">
                                 <div class="col-md-12 col-sm-12">
                                    <p class="rigwd"><strong>Answered on  </strong> 1 week ago</p>
                                 </div>
                              </div>

                              <div class="col-md-9">
                                 <div class="post">
                                    <p><span>3</span><strong>Answers</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlista3"><span>0</span><strong>Repost</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>36</span><strong>Views</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalfollowlista3"><span>1</span><strong>Followers</strong>
                                    </p>
                                 </div>
                                                   <!---- Modal  --->
                                                      <div id="myModalrepostlista3" class="modal fade" role="dialog">
                                                      <div class="modal-dialog" style="width: 700px;">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">×</button>
                                                               <h3></h3>
                                                            </div>
                                                            <div class="modal-body">
                                                               <div class="row">
                                                                  <div clss="col-md-12">
                                                                                                                                    </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      </div>

                                                    <!---- Modal  --->
                                                      <div id="myModalfollowlista3" class="modal fade" role="dialog">
                                                         <div class="modal-dialog" style="width: 700px;">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                               <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal">×</button>
                                                                  <h3></h3>
                                                               </div>
                                                               <div class="modal-body">
                                                                  <div class="row">
                                                                     <div clss="col-md-12">
                                                                        
                                                                        <div class="col-md-6">
                                                                         <div class="poll">
                                                                            <div class="col-md-3 col-sm-2 col-xs-3">
                                                                               <div class="imgforr">
                                                                                  <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                               </div>
                                                                            </div>
                                                                            <div class="col-md-9 col-sm-10 col-xs-9">
                                                                               <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ankit Mittal</a></strong> , Student Consult..</p>
                                                                            </div>
                                                                         </div>
                                                                        </div>
                                                                                                                                          </div>
                                                                  </div>
                                                               </div>

                                                            </div>
                                                         </div>
                                                      </div>

                              </div>

                              <div class="col-md-3">
                                 <div class="plugin"> <i class="fa fa-share-alt" aria-hidden="true"></i>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share">
                                                                        <p data-toggle="modal" data-target="#myModalans33"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                                                     </div>
                                       <!-- Modal -->
                                       <div id="myModalans3" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <p> For post an answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Modal -->
                                       <div id="myModalans33" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="6">
                                                      <textarea name="ansPost"> </textarea>
                                                      <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share1">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="quesId" value="6">
                                               <img src="/img/follow-icon-selected.png"><button type="submit" name="quesunfollow"> Follow </button>
                                            </form>
                                                                     </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="share2">
                                                                              <p data-toggle="modal" data-target="#myModalrepost3"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost33" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost3" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <form method="post" action="">
                                                         <p> <textarea name="reposttext"> </textarea></p>
                                                         <div class="repostmodal">
                                                           <p> Repost this question </p>
                                                           <p> Question :  Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                                         </div>
                                                         <input type="hidden" name="type" value="ques">
                                                         <input type="hidden" name="quesid" value="6">
                                                         <input type="hidden" name="ques" value="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                                                         <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                      </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">
                                    <p> <a href="https://edukeeda.com/discussioncategory?action=4">Industry/business</a> .  <a href="https://edukeeda.com/discussioncategory/?action=4&amp;actions=39">Textile</a> . <a href="https://edukeeda.com/discussion-house?action=6">Labour law reforms</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                 <div class="dot">
                                    <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide4">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport4">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide4" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="5">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport4" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="5">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="5">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="5">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-12">
                                 <div class="text1">
                                    <p><a href="https://edukeeda.com/questionanswers?action=5"></a></p><p><a href="https://edukeeda.com/questionanswers?action=5">The four-day annual event is a chance for mineral explorers, mining company employees, members of government, mining suppliers, geologists and academics to exchange ideas, technologies and project updates.</a></p><p></p>
                                 </div>
                              </div>
                              
                              <div class="questio">
                                 <div class="col-md-12 col-sm-12">
                                    <p class="rigwd"><strong>Answered on  </strong> 2 weeks ago</p>
                                 </div>
                              </div>

                              <div class="col-md-9">
                                 <div class="post">
                                    <p><span>3</span><strong>Answers</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlista4"><span>0</span><strong>Repost</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>15</span><strong>Views</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalfollowlista4"><span>0</span><strong>Followers</strong>
                                    </p>
                                 </div>
                                                   <!---- Modal  --->
                                                      <div id="myModalrepostlista4" class="modal fade" role="dialog">
                                                      <div class="modal-dialog" style="width: 700px;">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">×</button>
                                                               <h3></h3>
                                                            </div>
                                                            <div class="modal-body">
                                                               <div class="row">
                                                                  <div clss="col-md-12">
                                                                                                                                    </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      </div>

                                                    <!---- Modal  --->
                                                      <div id="myModalfollowlista4" class="modal fade" role="dialog">
                                                         <div class="modal-dialog" style="width: 700px;">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                               <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal">×</button>
                                                                  <h3></h3>
                                                               </div>
                                                               <div class="modal-body">
                                                                  <div class="row">
                                                                     <div clss="col-md-12">
                                                                                                                                          </div>
                                                                  </div>
                                                               </div>

                                                            </div>
                                                         </div>
                                                      </div>

                              </div>

                              <div class="col-md-3">
                                 <div class="plugin"> <i class="fa fa-share-alt" aria-hidden="true"></i>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share">
                                                                        <p data-toggle="modal" data-target="#myModalans44"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                                                     </div>
                                       <!-- Modal -->
                                       <div id="myModalans4" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <p> For post an answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Modal -->
                                       <div id="myModalans44" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="5">
                                                      <textarea name="ansPost"> </textarea>
                                                      <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share1">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="quesId" value="5">
                                               <img src="/img/follow-icon-selected.png"><button type="submit" name="quesunfollow"> Follow </button>
                                            </form>
                                                                     </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="share2">
                                                                              <p data-toggle="modal" data-target="#myModalrepost4"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost44" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost4" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <form method="post" action="">
                                                         <p> <textarea name="reposttext"> </textarea></p>
                                                         <div class="repostmodal">
                                                           <p> Repost this question </p>
                                                           <p> Question :  </p><p>The four-day annual event is a chance for mineral explorers, mining company employees, members of government, mining suppliers, geologists and academics to exchange ideas, technologies and project updates.</p> <p></p>
                                                         </div>
                                                         <input type="hidden" name="type" value="ques">
                                                         <input type="hidden" name="quesid" value="5">
                                                         <input type="hidden" name="ques" value="<p>The four-day annual event is a chance for mineral explorers, mining company employees, members of government, mining suppliers, geologists and academics to exchange ideas, technologies and project updates.</p>">
                                                         <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                      </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">
                                    <p>  <a href="https://edukeeda.com/discussioncategory/?action=3&amp;actions=0">Nation Building</a> . <a href="https://edukeeda.com/discussion-house?action=5">Educated society</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                 <div class="dot">
                                    <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide5">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport5">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide5" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="3">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport5" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="3">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="3">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="3">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-12">
                                 <div class="text1">
                                    <p><a href="https://edukeeda.com/questionanswers?action=3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                 </div>
                              </div>
                              
                              <div class="questio">
                                 <div class="col-md-12 col-sm-12">
                                    <p class="rigwd"><strong>Answered on  </strong> 1 week ago</p>
                                 </div>
                              </div>

                              <div class="col-md-9">
                                 <div class="post">
                                    <p><span>1</span><strong>Answers</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlista5"><span>0</span><strong>Repost</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>42</span><strong>Views</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalfollowlista5"><span>2</span><strong>Followers</strong>
                                    </p>
                                 </div>
                                                   <!---- Modal  --->
                                                      <div id="myModalrepostlista5" class="modal fade" role="dialog">
                                                      <div class="modal-dialog" style="width: 700px;">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">×</button>
                                                               <h3></h3>
                                                            </div>
                                                            <div class="modal-body">
                                                               <div class="row">
                                                                  <div clss="col-md-12">
                                                                                                                                    </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      </div>

                                                    <!---- Modal  --->
                                                      <div id="myModalfollowlista5" class="modal fade" role="dialog">
                                                         <div class="modal-dialog" style="width: 700px;">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                               <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal">×</button>
                                                                  <h3></h3>
                                                               </div>
                                                               <div class="modal-body">
                                                                  <div class="row">
                                                                     <div clss="col-md-12">
                                                                        
                                                                        <div class="col-md-6">
                                                                         <div class="poll">
                                                                            <div class="col-md-3 col-sm-2 col-xs-3">
                                                                               <div class="imgforr">
                                                                                  <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                               </div>
                                                                            </div>
                                                                            <div class="col-md-9 col-sm-10 col-xs-9">
                                                                               <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ankit Mittal</a></strong> , Student Consult..</p>
                                                                            </div>
                                                                         </div>
                                                                        </div>
                                                                        
                                                                        <div class="col-md-6">
                                                                         <div class="poll">
                                                                            <div class="col-md-3 col-sm-2 col-xs-3">
                                                                               <div class="imgforr">
                                                                                  <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/03/ca060a69056.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                               </div>
                                                                            </div>
                                                                            <div class="col-md-9 col-sm-10 col-xs-9">
                                                                               <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ashish Prasad</a></strong> , 5 Years of Expe..</p>
                                                                            </div>
                                                                         </div>
                                                                        </div>
                                                                                                                                          </div>
                                                                  </div>
                                                               </div>

                                                            </div>
                                                         </div>
                                                      </div>

                              </div>

                              <div class="col-md-3">
                                 <div class="plugin"> <i class="fa fa-share-alt" aria-hidden="true"></i>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share">
                                                                        <p data-toggle="modal" data-target="#myModalans55"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                                                     </div>
                                       <!-- Modal -->
                                       <div id="myModalans5" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <p> For post an answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Modal -->
                                       <div id="myModalans55" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="3">
                                                      <textarea name="ansPost"> </textarea>
                                                      <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share1">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="quesId" value="3">
                                               <img src="/img/follow-icon-selected.png"><button type="submit" name="quesunfollow"> Follow </button>
                                            </form>
                                                                     </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="share2">
                                                                              <p data-toggle="modal" data-target="#myModalrepost5"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost55" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost5" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <form method="post" action="">
                                                         <p> <textarea name="reposttext"> </textarea></p>
                                                         <div class="repostmodal">
                                                           <p> Repost this question </p>
                                                           <p> Question :  Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                                         </div>
                                                         <input type="hidden" name="type" value="ques">
                                                         <input type="hidden" name="quesid" value="3">
                                                         <input type="hidden" name="ques" value="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                                                         <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                      </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">
                                    <p>  <a href="https://edukeeda.com/discussioncategory/?action=2&amp;actions=0">Govt Policy/ Decision</a> . <a href="https://edukeeda.com/discussion-house?action=4">Digital Payment</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                 <div class="dot">
                                    <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide6">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport6">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide6" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="2">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport6" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="2">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="2">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="2">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-12">
                                 <div class="text1">
                                    <p><a href="https://edukeeda.com/questionanswers?action=2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                 </div>
                              </div>
                              
                              <div class="questio">
                                 <div class="col-md-12 col-sm-12">
                                    <p class="rigwd"><strong>Answered on  </strong> 2 months ago</p>
                                 </div>
                              </div>

                              <div class="col-md-9">
                                 <div class="post">
                                    <p><span>1</span><strong>Answers</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlista6"><span>0</span><strong>Repost</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>13</span><strong>Views</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalfollowlista6"><span>0</span><strong>Followers</strong>
                                    </p>
                                 </div>
                                                   <!---- Modal  --->
                                                      <div id="myModalrepostlista6" class="modal fade" role="dialog">
                                                      <div class="modal-dialog" style="width: 700px;">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">×</button>
                                                               <h3></h3>
                                                            </div>
                                                            <div class="modal-body">
                                                               <div class="row">
                                                                  <div clss="col-md-12">
                                                                                                                                    </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      </div>

                                                    <!---- Modal  --->
                                                      <div id="myModalfollowlista6" class="modal fade" role="dialog">
                                                         <div class="modal-dialog" style="width: 700px;">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                               <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal">×</button>
                                                                  <h3></h3>
                                                               </div>
                                                               <div class="modal-body">
                                                                  <div class="row">
                                                                     <div clss="col-md-12">
                                                                                                                                          </div>
                                                                  </div>
                                                               </div>

                                                            </div>
                                                         </div>
                                                      </div>

                              </div>

                              <div class="col-md-3">
                                 <div class="plugin"> <i class="fa fa-share-alt" aria-hidden="true"></i>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share">
                                                                        <p data-toggle="modal" data-target="#myModalans66"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                                                     </div>
                                       <!-- Modal -->
                                       <div id="myModalans6" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <p> For post an answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Modal -->
                                       <div id="myModalans66" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="2">
                                                      <textarea name="ansPost"> </textarea>
                                                      <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share1">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="quesId" value="2">
                                               <img src="/img/follow-icon-selected.png"><button type="submit" name="quesunfollow"> Follow </button>
                                            </form>
                                                                     </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="share2">
                                                                              <p data-toggle="modal" data-target="#myModalrepost6"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost66" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost6" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <form method="post" action="">
                                                         <p> <textarea name="reposttext"> </textarea></p>
                                                         <div class="repostmodal">
                                                           <p> Repost this question </p>
                                                           <p> Question :  Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                                         </div>
                                                         <input type="hidden" name="type" value="ques">
                                                         <input type="hidden" name="quesid" value="2">
                                                         <input type="hidden" name="ques" value="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                                                         <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                      </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10 col-xs-10">
                                    <p>  <a href="https://edukeeda.com/discussioncategory/?action=1&amp;actions=0">Trending Issues</a> . <a href="https://edukeeda.com/discussion-house?action=3">The Condition of Education The Condition of Education</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                 <div class="dot">
                                    <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide7">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport7">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide7" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="1">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport7" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="1">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="1">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="1">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="col-md-12">
                                 <div class="text1">
                                    <p><a href="https://edukeeda.com/questionanswers?action=1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                 </div>
                              </div>
                              
                              <div class="questio">
                                 <div class="col-md-12 col-sm-12">
                                    <p class="rigwd"><strong>Answered on  </strong> 2 months ago</p>
                                 </div>
                              </div>

                              <div class="col-md-9">
                                 <div class="post">
                                    <p><span>2</span><strong>Answers</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlista7"><span>0</span><strong>Repost</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p><span>48</span><strong>Views</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalfollowlista7"><span>0</span><strong>Followers</strong>
                                    </p>
                                 </div>
                                                   <!---- Modal  --->
                                                      <div id="myModalrepostlista7" class="modal fade" role="dialog">
                                                      <div class="modal-dialog" style="width: 700px;">
                                                         <!-- Modal content-->
                                                         <div class="modal-content">
                                                            <div class="modal-header">
                                                               <button type="button" class="close" data-dismiss="modal">×</button>
                                                               <h3></h3>
                                                            </div>
                                                            <div class="modal-body">
                                                               <div class="row">
                                                                  <div clss="col-md-12">
                                                                                                                                    </div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      </div>

                                                    <!---- Modal  --->
                                                      <div id="myModalfollowlista7" class="modal fade" role="dialog">
                                                         <div class="modal-dialog" style="width: 700px;">
                                                            <!-- Modal content-->
                                                            <div class="modal-content">
                                                               <div class="modal-header">
                                                                  <button type="button" class="close" data-dismiss="modal">×</button>
                                                                  <h3></h3>
                                                               </div>
                                                               <div class="modal-body">
                                                                  <div class="row">
                                                                     <div clss="col-md-12">
                                                                                                                                          </div>
                                                                  </div>
                                                               </div>

                                                            </div>
                                                         </div>
                                                      </div>

                              </div>

                              <div class="col-md-3">
                                 <div class="plugin"> <i class="fa fa-share-alt" aria-hidden="true"></i>
                                 </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share">
                                                                        <p data-toggle="modal" data-target="#myModalans77"><i class="fa fa-reply" aria-hidden="true"></i>Answer</p>
                                                                     </div>
                                       <!-- Modal -->
                                       <div id="myModalans7" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <p> For post an answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <!-- Modal -->
                                       <div id="myModalans77" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h4 class="modal-title">Post Answer</h4>
                                                </div>
                                                <div class="modal-body">
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="1">
                                                      <textarea name="ansPost"> </textarea>
                                                      <input type="submit" name="anspostsubmit" value="Answer Submit">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                              </div>

                              <div class="col-md-4">
                                 <div class="share1">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="quesId" value="1">
                                               <img src="/img/follow-icon-selected.png"><button type="submit" name="quesunfollow"> Follow </button>
                                            </form>
                                                                     </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="share2">
                                                                              <p data-toggle="modal" data-target="#myModalrepost7"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost77" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <!-- Modal -->
                                          <div id="myModalrepost7" class="modal fade" role="dialog">
                                             <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                   <div class="modal-header">
                                                      <button type="button" class="close" data-dismiss="modal">×</button>
                                                      <h4 class="modal-title"> Repost </h4>
                                                   </div>
                                                   <div class="modal-body">
                                                      <form method="post" action="">
                                                         <p> <textarea name="reposttext"> </textarea></p>
                                                         <div class="repostmodal">
                                                           <p> Repost this question </p>
                                                           <p> Question :  Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                                                         </div>
                                                         <input type="hidden" name="type" value="ques">
                                                         <input type="hidden" name="quesid" value="1">
                                                         <input type="hidden" name="ques" value="Lorem Ipsum is simply dummy text of the printing and typesetting industry.">
                                                         <p> <input type="submit" name="repostsubmit" value="Repost"> </p>
                                                      </form>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                              </div>
                           </div>
                                                   </div>
                     </div>

                     <div id="anwerspat" class="tabcontent" style="display:none;">
                        <div class="blogaresnews">
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10">
                                    <p> <a href="https://edukeeda.com/discussioncategory?action=4">Industry/business</a> .  <a href="https://edukeeda.com/discussioncategory/?action=4&amp;actions=38">Railways</a> . <a href="https://edukeeda.com/discussion-house?action=7">The Condition of Education The Condition ...</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2">
                                 <div class="dot">
								 <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide1">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport1">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide1" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="12">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport1" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="12">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="text">
                                    <p><a href="https://edukeeda.com/questionanswers?action=10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</a></p>
                                 </div>
                              </div>
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>
                                 </p>
                                 <p class="rigwd">April 21, 2019. 8:59 pm </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p>we need to write this answer here only</p>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>2</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalreplylistsa2"><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlistd2"><span>1</span><strong>Repost</strong>
                                    </p>
                                 </div>

                                          <!---- Modal  --->
                                            <div id="myModalreplylistsa2" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                               <!-- Modal content-->
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal">×</button>
                                                     <h3> Reply </h3>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row">
                                                        <div clss="col-md-12">
                                                           
                                                           <div class="col-md-12">
                                                              <div class="poll">
                                                                 <div class="left">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/03/ca060a69056.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="right">
                                                                    <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ashish Prasad</a></strong></p>
                                                                 </div>
                                                                 <div class="custext">
                                                                    <div class="text2">
                                                                       <p>  1234</p>
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                                                                                </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                            </div>

                                            <!---- Modal  --->
                                              <div id="myModalrepostlistd2" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">×</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                             
                                                             <div class="col-md-6">
                                                              <div class="poll">
                                                                 <div class="col-md-3 col-sm-2 col-xs-3">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/03/ca060a69056.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="col-md-9 col-sm-10 col-xs-9">
                                                                    <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ashish Prasad</a></strong> , 5 Years of Expe..</p>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                                                                                    </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="ansid" value="16">
                                               <button type="submit" name="ansunlikeSubmit" class="unlikecolor"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Like</button>
                                            </form>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModallike2" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Like</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Like this answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalanssrep22"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                                <!-- Modal -->
                                             <div id="myModalanssrep2" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- Modal -->
                                             <div id="myModalanssrep22" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <form method="post" action="">
                                                            <input type="hidden" name="rid" value="16">
                                                            <textarea name="replyPost"> </textarea>
                                                            <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                         </form>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>

                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost12"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost122" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost12" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                  <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text">
                                                         <p><a href="https://edukeeda.com/questionanswers?action=10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</a></p>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-2 col-sm-2 col-xs-4">
                                                      <div class="imgforr">
                                                         <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-10 col-sm-10 col-xs-8">
                                                      <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal</a></strong>,<span>Student Consultant</span></p>
                                                      <p class="rigwd"> April 21, 2019. 8:59 pm  </p>
                                                   </div>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p>we need to write this answer here only</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                
                                                <input type="hidden" name="qid" value="10">
                                                <input type="hidden" name="ansid" value="16">
                                                <input type="hidden" name="ans" value="we need to write this answer here only">
                                                <p> <input type="submit" name="repostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10">
                                    <p> <a href="https://edukeeda.com/discussioncategory?action=4">Industry/business</a> .  <a href="https://edukeeda.com/discussioncategory/?action=4&amp;actions=38">Railways</a> . <a href="https://edukeeda.com/discussion-house?action=7">The Condition of Education The Condition ...</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2">
                                 <div class="dot">
								 <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide1">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport1">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide1" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="12">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport1" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="12">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="text">
                                    <p><a href="https://edukeeda.com/questionanswers?action=6">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                 </div>
                              </div>
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>
                                 </p>
                                 <p class="rigwd">April 23, 2019. 7:46 pm </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p>Good sheer is provided</p>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>0</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalreplylistsa3"><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlistd3"><span>0</span><strong>Repost</strong>
                                    </p>
                                 </div>

                                          <!---- Modal  --->
                                            <div id="myModalreplylistsa3" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                               <!-- Modal content-->
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal">×</button>
                                                     <h3> Reply </h3>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row">
                                                        <div clss="col-md-12">
                                                                                                                </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                            </div>

                                            <!---- Modal  --->
                                              <div id="myModalrepostlistd3" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">×</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                                                                                    </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share">
                                                                              <form method="post" action="">
                                             <input type="hidden" name="ansid" value="18">
                                             <button type="submit" name="anslikeSubmit"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                          </form>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModallike3" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Like</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Like this answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalanssrep33"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                                <!-- Modal -->
                                             <div id="myModalanssrep3" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- Modal -->
                                             <div id="myModalanssrep33" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <form method="post" action="">
                                                            <input type="hidden" name="rid" value="18">
                                                            <textarea name="replyPost"> </textarea>
                                                            <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                         </form>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>

                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost13"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost133" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost13" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                  <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text">
                                                         <p><a href="https://edukeeda.com/questionanswers?action=6">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-2 col-sm-2 col-xs-4">
                                                      <div class="imgforr">
                                                         <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-10 col-sm-10 col-xs-8">
                                                      <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal</a></strong>,<span>Student Consultant</span></p>
                                                      <p class="rigwd"> April 23, 2019. 7:46 pm  </p>
                                                   </div>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p>Good sheer is provided</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                
                                                <input type="hidden" name="qid" value="6">
                                                <input type="hidden" name="ansid" value="18">
                                                <input type="hidden" name="ans" value="Good sheer is provided">
                                                <p> <input type="submit" name="repostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10">
                                    <p> <a href="https://edukeeda.com/discussioncategory?action=4">Industry/business</a> .  <a href="https://edukeeda.com/discussioncategory/?action=4&amp;actions=39">Textile</a> . <a href="https://edukeeda.com/discussion-house?action=6">The Condition of Education The Condition ...</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2">
                                 <div class="dot">
								 <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide1">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport1">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide1" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="12">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport1" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="12">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="text">
                                    <p><a href="https://edukeeda.com/questionanswers?action=5"></a></p><p><a href="https://edukeeda.com/questionanswers?action=5">The four-day annual event is a chance for mineral explorers, mining company employees, members of government, mining suppliers, geologists and academics to exchange ideas, technologies and project updates.</a></p><p></p>
                                 </div>
                              </div>
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>
                                 </p>
                                 <p class="rigwd">April 18, 2019. 12:21 pm </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type. </p>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>1</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalreplylistsa4"><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlistd4"><span>0</span><strong>Repost</strong>
                                    </p>
                                 </div>

                                          <!---- Modal  --->
                                            <div id="myModalreplylistsa4" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                               <!-- Modal content-->
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal">×</button>
                                                     <h3> Reply </h3>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row">
                                                        <div clss="col-md-12">
                                                                                                                </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                            </div>

                                            <!---- Modal  --->
                                              <div id="myModalrepostlistd4" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">×</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                                                                                    </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="ansid" value="12">
                                               <button type="submit" name="ansunlikeSubmit" class="unlikecolor"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Like</button>
                                            </form>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModallike4" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Like</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Like this answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalanssrep44"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                                <!-- Modal -->
                                             <div id="myModalanssrep4" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- Modal -->
                                             <div id="myModalanssrep44" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <form method="post" action="">
                                                            <input type="hidden" name="rid" value="12">
                                                            <textarea name="replyPost"> </textarea>
                                                            <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                         </form>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>

                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost14"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost144" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost14" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                  <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text">
                                                         <p><a href="https://edukeeda.com/questionanswers?action=5"></a></p><p><a href="https://edukeeda.com/questionanswers?action=5">The four-day annual event is a chance for mineral explorers, mining company employees, members of government, mining suppliers, geologists and academics to exchange ideas, technologies and project updates.</a></p><p></p>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-2 col-sm-2 col-xs-4">
                                                      <div class="imgforr">
                                                         <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-10 col-sm-10 col-xs-8">
                                                      <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal</a></strong>,<span>Student Consultant</span></p>
                                                      <p class="rigwd"> April 18, 2019. 12:21 pm  </p>
                                                   </div>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type. </p>
                                                      </div>
                                                   </div>
                                                </div>
                                                
                                                <input type="hidden" name="qid" value="5">
                                                <input type="hidden" name="ansid" value="12">
                                                <input type="hidden" name="ans" value="Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type. ">
                                                <p> <input type="submit" name="repostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10">
                                    <p>  <a href="https://edukeeda.com/discussioncategory/?action=3&amp;actions=0">Nation Building</a> . <a href="https://edukeeda.com/discussion-house?action=5">The Condition of Education The Condition ...</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2">
                                 <div class="dot">
								 <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide1">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport1">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide1" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="12">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport1" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="12">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="text">
                                    <p><a href="https://edukeeda.com/questionanswers?action=3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                 </div>
                              </div>
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>
                                 </p>
                                 <p class="rigwd">April 23, 2019. 7:31 pm </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p> Here is answer1</p>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>0</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalreplylistsa5"><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlistd5"><span>0</span><strong>Repost</strong>
                                    </p>
                                 </div>

                                          <!---- Modal  --->
                                            <div id="myModalreplylistsa5" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                               <!-- Modal content-->
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal">×</button>
                                                     <h3> Reply </h3>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row">
                                                        <div clss="col-md-12">
                                                                                                                </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                            </div>

                                            <!---- Modal  --->
                                              <div id="myModalrepostlistd5" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">×</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                                                                                    </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share">
                                                                              <form method="post" action="">
                                             <input type="hidden" name="ansid" value="17">
                                             <button type="submit" name="anslikeSubmit"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                          </form>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModallike5" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Like</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Like this answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalanssrep55"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                                <!-- Modal -->
                                             <div id="myModalanssrep5" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- Modal -->
                                             <div id="myModalanssrep55" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <form method="post" action="">
                                                            <input type="hidden" name="rid" value="17">
                                                            <textarea name="replyPost"> </textarea>
                                                            <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                         </form>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>

                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost15"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost155" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost15" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                  <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text">
                                                         <p><a href="https://edukeeda.com/questionanswers?action=3">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-2 col-sm-2 col-xs-4">
                                                      <div class="imgforr">
                                                         <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-10 col-sm-10 col-xs-8">
                                                      <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal</a></strong>,<span>Student Consultant</span></p>
                                                      <p class="rigwd"> April 23, 2019. 7:31 pm  </p>
                                                   </div>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p> Here is answer1</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                
                                                <input type="hidden" name="qid" value="3">
                                                <input type="hidden" name="ansid" value="17">
                                                <input type="hidden" name="ans" value=" Here is answer1">
                                                <p> <input type="submit" name="repostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10">
                                    <p>  <a href="https://edukeeda.com/discussioncategory/?action=2&amp;actions=0">Govt Policy/ Decision</a> . <a href="https://edukeeda.com/discussion-house?action=4">The Condition of Education The Condition ...</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2">
                                 <div class="dot">
								 <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide1">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport1">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide1" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="12">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport1" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="12">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="text">
                                    <p><a href="https://edukeeda.com/questionanswers?action=2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                 </div>
                              </div>
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>
                                 </p>
                                 <p class="rigwd">February 14, 2019. 12:15 pm </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>0</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalreplylistsa6"><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlistd6"><span>0</span><strong>Repost</strong>
                                    </p>
                                 </div>

                                          <!---- Modal  --->
                                            <div id="myModalreplylistsa6" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                               <!-- Modal content-->
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal">×</button>
                                                     <h3> Reply </h3>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row">
                                                        <div clss="col-md-12">
                                                                                                                </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                            </div>

                                            <!---- Modal  --->
                                              <div id="myModalrepostlistd6" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">×</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                                                                                    </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share">
                                                                              <form method="post" action="">
                                             <input type="hidden" name="ansid" value="4">
                                             <button type="submit" name="anslikeSubmit"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i>Like</button>
                                          </form>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModallike6" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Like</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Like this answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalanssrep66"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                                <!-- Modal -->
                                             <div id="myModalanssrep6" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- Modal -->
                                             <div id="myModalanssrep66" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <form method="post" action="">
                                                            <input type="hidden" name="rid" value="4">
                                                            <textarea name="replyPost"> </textarea>
                                                            <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                         </form>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>

                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost16"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost166" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost16" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                  <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text">
                                                         <p><a href="https://edukeeda.com/questionanswers?action=2">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-2 col-sm-2 col-xs-4">
                                                      <div class="imgforr">
                                                         <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-10 col-sm-10 col-xs-8">
                                                      <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal</a></strong>,<span>Student Consultant</span></p>
                                                      <p class="rigwd"> February 14, 2019. 12:15 pm  </p>
                                                   </div>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                
                                                <input type="hidden" name="qid" value="2">
                                                <input type="hidden" name="ansid" value="4">
                                                <input type="hidden" name="ans" value="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.">
                                                <p> <input type="submit" name="repostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                                                      <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10">
                                    <p>  <a href="https://edukeeda.com/discussioncategory/?action=1&amp;actions=0">Trending Issues</a> . <a href="https://edukeeda.com/discussion-house?action=3">The Condition of Education The Condition ...</a> </p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2">
                                 <div class="dot">
								 <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> ...
                                       </p>
                                       <ul class="dropdown-menu">
                                          <li data-toggle="modal" data-target="#myModalhide1">Hide Post</li>
                                          <li data-toggle="modal" data-target="#myModalreport1">Report This Post</li>
                                       </ul>
                                       <div id="myModalhide1" class="modal fade" role="dialog">
                                          <div class="modal-dialog">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1>Hide this post</h1>
                                                </div>
                                                <div class="modal-body">
                                                   <p> Do you really want to hide this post? </p>
                                                   <form method="post" action="">
                                                      <input type="hidden" name="qid" value="12">
                                                      <input type="submit" name="hidesubmit" value="Hide">
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="modal fade" id="myModalreport1" role="dialog">
                                          <div class="modal-dialog vertical-align-center">
                                             <div class="modal-content">
                                                <div class="repocontent modal-body reportingsets" id="gridone" style="display:block;">
                                                   <ul>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridtwo')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it's spam, Promotional</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridthree')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think  it's objectionable</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                      <li>
                                                         <span class="countno repolinks" onclick="openReport(event, 'gridfour')">
                                                            <div class="funkyradio-default">
                                                               <label for="checkbox1">I think it false or misinformation</label>
                                                            </div>
                                                         </span>
                                                      </li>
                                                   </ul>
                                                   <div class="modal-footer">
                                                   </div>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridtwo" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You Are Reorting that this is spam, Promotional" checked="checked"> You Are Reorting that this is spam, Promotional</p>
                                                         <input type="hidden" name="qid" value="12">
                                                      </div>
                                                      <div class="modal-footer">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridthree" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's pornographic or extremely violent">It's pornographic or extremely violent.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="It's hate speech">It's hate speech.</label>
                                                         </div>
                                                         <div class="funkyradio-default">
                                                            <label class="checkbox-inline"><input type="radio" name="reason" value="Topic or language is offensive/Abusive">Topic or language is offensive/Abusive.</label>
                                                         </div>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                                <div class="repocontent modal-body reportingsets" id="gridfour" style="display:none;">
                                                   <form method="post" action="">
                                                      <div class="modal-body">
                                                         <p><input type="radio" name="reason" value="You are reporting that it is a salse information or misinformation" checked="checked"> You are reporting that it is a salse information or misinformation.</p>
                                                      </div>
                                                      <div class="bottom-btns">
                                                         <input type="hidden" name="qid" value="12">
                                                         <button type="button" class="countno repolinks btn btn-default" onclick="openReport(event, 'gridone')">Back</button>
                                                         <button type="submit" name="submitReport" class="btn btn-default">Submit</button>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="text">
                                    <p><a href="https://edukeeda.com/questionanswers?action=1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                 </div>
                              </div>
                              <div class="imgforr">
                                 <div class="col-md-2 col-sm-2">
                                    <a href="#">
                                    <img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                    </a>
                                 </div>
                              </div>
                              <div class="col-md-10 col-sm-10">
                                 <p class="catp"><strong><a href="#">Ankit Mittal</a></strong>
                                 </p>
                                 <p class="rigwd">February 14, 2019. 12:14 pm </p>
                              </div>
                              <div class="col-md-12">
                                 <div class="text2">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="pp">
                                    <p><span>1</span><strong>Likes</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalreplylistsa7"><span>0</span><strong>Reply</strong></p><div class="dotsseprate">.</div>
                                    <p></p>
                                    <p data-toggle="modal" data-target="#myModalrepostlistd7"><span>0</span><strong>Repost</strong>
                                    </p>
                                 </div>

                                          <!---- Modal  --->
                                            <div id="myModalreplylistsa7" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                               <!-- Modal content-->
                                               <div class="modal-content">
                                                  <div class="modal-header">
                                                     <button type="button" class="close" data-dismiss="modal">×</button>
                                                     <h3> Reply </h3>
                                                  </div>
                                                  <div class="modal-body">
                                                     <div class="row">
                                                        <div clss="col-md-12">
                                                           
                                                           <div class="col-md-12">
                                                              <div class="poll">
                                                                 <div class="left">
                                                                    <div class="imgforr">
                                                                       <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/03/ca060a69056.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                    </div>
                                                                 </div>
                                                                 <div class="right">
                                                                    <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ashish Prasad</a></strong></p>
                                                                 </div>
                                                                 <div class="custext">
                                                                    <div class="text2">
                                                                       <p>  good answer</p>
                                                                    </div>
                                                                 </div>
                                                              </div>
                                                           </div>
                                                                                                                </div>
                                                     </div>
                                                  </div>
                                               </div>
                                            </div>
                                            </div>

                                            <!---- Modal  --->
                                              <div id="myModalrepostlistd7" class="modal fade" role="dialog">
                                              <div class="modal-dialog" style="width: 700px;">
                                                 <!-- Modal content-->
                                                 <div class="modal-content">
                                                    <div class="modal-header">
                                                       <button type="button" class="close" data-dismiss="modal">×</button>
                                                       <h3></h3>
                                                    </div>
                                                    <div class="modal-body">
                                                       <div class="row">
                                                          <div clss="col-md-12">
                                                                                                                    </div>
                                                       </div>
                                                    </div>
                                                 </div>
                                              </div>
                                              </div>

                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="ansid" value="2">
                                               <button type="submit" name="ansunlikeSubmit" class="unlikecolor"><i class="fa fa-thumbs-up" aria-hidden="true"></i>Like</button>
                                            </form>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModallike7" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title">Like</h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Like this answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share1">
                                                                        <p data-toggle="modal" data-target="#myModalanssrep77"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                                                                <!-- Modal -->
                                             <div id="myModalanssrep7" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <p> For reply on answer. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <!-- Modal -->
                                             <div id="myModalanssrep77" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                   <!-- Modal content-->
                                                   <div class="modal-content">
                                                      <div class="modal-header">
                                                         <button type="button" class="close" data-dismiss="modal">×</button>
                                                         <h4 class="modal-title">Reply</h4>
                                                      </div>
                                                      <div class="modal-body">
                                                         <form method="post" action="">
                                                            <input type="hidden" name="rid" value="2">
                                                            <textarea name="replyPost"> </textarea>
                                                            <input type="submit" name="replypostsubmit" value="Reply Submit">
                                                         </form>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>

                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-4">
                                 <div class="share2">
                                                                        <p data-toggle="modal" data-target="#myModalrepost17"><i class="fa fa-mail-forward"></i>Repost</p>
                                                                     </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost177" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <p> For Repost this question. Please <a href="https://edukeeda.com/signin/">login or register</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Modal -->
                                 <div id="myModalrepost17" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                       <!-- Modal content-->
                                       <div class="modal-content">
                                          <div class="modal-header">
                                             <button type="button" class="close" data-dismiss="modal">×</button>
                                             <h4 class="modal-title"> Repost </h4>
                                          </div>
                                          <div class="modal-body">
                                             <form method="post" action="">
                                                <div class="row">
                                                  <p> <textarea name="reposttext"> </textarea></p>
                                                   <div class="col-md-12">
                                                      <div class="text">
                                                         <p><a href="https://edukeeda.com/questionanswers?action=1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a></p>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-2 col-sm-2 col-xs-4">
                                                      <div class="imgforr">
                                                         <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                      </div>
                                                   </div>
                                                   <div class="col-md-10 col-sm-10 col-xs-8">
                                                      <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=49">Ankit Mittal</a></strong>,<span>Student Consultant</span></p>
                                                      <p class="rigwd"> February 14, 2019. 12:14 pm  </p>
                                                   </div>
                                                   <div class="col-md-12">
                                                      <div class="text2">
                                                         <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                                                      </div>
                                                   </div>
                                                </div>
                                                
                                                <input type="hidden" name="qid" value="1">
                                                <input type="hidden" name="ansid" value="2">
                                                <input type="hidden" name="ans" value="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.">
                                                <p> <input type="submit" name="repostanssubmit" value="Repost"> </p>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                                                   </div>
                     </div>

                     <div id="directquest" class="tabcontent" style="display:none;">
                        <div class="blogaresnews">
                           <div class="col-md-12 blo">
                              <div class="direct">
                                 <div class="col-md-10 col-sm-10">
                                    <p>Direct Question For ABCD</p>
                                 </div>
                              </div>
                              <div class="col-md-2 col-sm-2">
                                 <div class="dot">
                                    <div class="dropdown">
                                       <p class="dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">...</p>
                                       <ul class="dropdown-menu">
                                          <li>
                                             <span class="countno" data-toggle="modal" data-target="#myModal1">
                                                <div class="funkyradio-default">
                                                   <input type="checkbox" name="vehicle1" value="Education Level">
                                                   <label for="checkbox1">I think it's spam, Promotional</label>
                                                </div>
                                             </span>
                                          </li>
                                          <li>
                                             <span class="countno" data-toggle="modal" data-target="#myModal2">
                                                <div class="funkyradio-default">
                                                   <input type="checkbox" name="vehicle1" value="Education Level">
                                                   <label for="checkbox1">I think  it's objectionable</label>
                                                </div>
                                             </span>
                                          </li>
                                          <li>
                                             <span class="countno" data-toggle="modal" data-target="#myModal3">
                                                <div class="funkyradio-default">
                                                   <input type="checkbox" name="vehicle1" value="Education Level">
                                                   <label for="checkbox1">I think it false or misinformation</label>
                                                </div>
                                             </span>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 <div class="text">
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry..</p>
                                 </div>
                              </div>
                              <div class="questio">
                                 <div class="col-md-2 col-sm-2 col-xs-4">
                                    <div class="imgforr">
                                       <a href="#">
                                       <img src="https://edukeeda.com/wp-content/uploads/2019/01/financial.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                       </a>
                                    </div>
                                 </div>
                                 <div class="col-md-10 col-sm-10 col-xs-8">
                                    <p class="catp"><strong><a href="#">Satya Prabhu</a></strong>,<span>Profile Credential</span>
                                    </p>
                                    <p class="rigwd"><strong>January 25th </strong>, 2019. 10:15 AM</p>
                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share">
                                    <p><i class="fa fa-thumbs-up" aria-hidden="true"></i>Like</p>
                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-3">
                                 <div class="share1">
                                    <p data-toggle="modal" data-target="#myModalrep"><i class="fa fa-reply" aria-hidden="true"></i>Reply</p>
                                 </div>
                              </div>
                              <div class="col-md-4 col-xs-4">
                                 <div class="share2">
                                    <p>15 Posts</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>

                     <div id="intrestquest" class="tabcontent" style="display:none;">
                        <div class="blogaresnews">
                           <div class="col-md-12 blo">
                                                            <div class="col-md-12">
                                 <div class="col-md-9">
                                    <div class="cat"> <a href="#"> Mining, Coal &amp; Metal <span>(6 
                                       Articles )
                                       </span></a>
                                    </div>
                                 </div>
                                 <div class="col-md-3">
                                    <div class="plug">
                                                                               <form method="post" action=" ">
                                          <input type="hidden" value="82" name="catId">
                                          <input type="hidden" value="industry" name="catType">
                                          <button type="submit" name="unfollow" class="followbutton1" style="background-color:#fff;border: none;"><img src="/img/follow-icon-selected.png"> </button><p></p>
                                       </form>
                                                                           </div>
                                 </div>
                              </div>
                                                            <div class="col-md-12">
                                 <div class="col-md-9">
                                    <div class="cat"> <a href="#"> Success story <span>(4 
                                       Stories )
                                       </span></a>
                                    </div>
                                 </div>
                                 <div class="col-md-3">
                                    <div class="plug">
                                                                               <form method="post" action=" ">
                                          <input type="hidden" value="79" name="catId">
                                          <input type="hidden" value="success_story" name="catType">
                                          <button type="submit" name="unfollow" class="followbutton1" style="background-color:#fff;border: none;"><img src="/img/follow-icon-selected.png"> </button><p></p>
                                       </form>
                                                                           </div>
                                 </div>
                              </div>
                                                            <div class="col-md-12">
                                 <div class="col-md-9">
                                    <div class="cat"> <a href="#"> Examination <span>(2 
                                       Events )
                                       </span></a>
                                    </div>
                                 </div>
                                 <div class="col-md-3">
                                    <div class="plug">
                                                                               <form method="post" action=" ">
                                          <input type="hidden" value="117" name="catId">
                                          <input type="hidden" value="events" name="catType">
                                          <button type="submit" name="unfollow" class="followbutton1" style="background-color:#fff;border: none;"><img src="/img/follow-icon-selected.png"> </button><p></p>
                                       </form>
                                                                           </div>
                                 </div>
                              </div>
                                                         </div>
                        </div>
                     </div>


                     <div id="discussquest" class="tabcontent" style="display:none;">
                           <div class="blogaresnews">
                                                            <div class="col-md-12 blo">
                                 <div class="col-md-12">
                                    <div class="direct">
									  <div class="col-md-10 col-xs-9">
                                       <p> 
                                                                                      <a href="https://edukeeda.com/discussioncategory/?action=4"> Industry/business .  </a>
                                                                                      <a href="https://edukeeda.com/discussioncategory/?action=4&amp;actions=38"> Railways </a> </p>
                                    </div>
									
									</div>
									<div class="col-md-2 col-xs-3">
                                      <div class="crose">
                                        <p><i class="fa fa-times" aria-hidden="true"></i></p>
                                    </div>
                                 </div>
								 
                                    <div class="col-md-9">
                                      <div class="text1">
                                         <h2> longest railway platform </h2>
                                      </div>
                                    </div>

                                    <div class="col-md-3">
                                       <div class="house">
                                                                                    <form method="post" action="">
                                             <input type="hidden" name="hid" value="7">
                                             <button types="submit" name="unjoinhousesubmit" class="disjoinhouse">Joined</button>
                                          </form>
                                                                                 </div>
                                    </div>

                                    <div class="col-md-9">
                                       <div class="post">
                                          <p><span>4</span><strong>Posts</strong></p><div class="dotsseprate">.</div>
                                          <p></p>
                                          <p data-toggle="modal" data-target="#myModalpeople1"><span>2</span><strong>People</strong></p><div class="dotsseprate">.</div>
                                          <p></p>
                                          <p><span>1</span><strong>Views </strong>
                                          </p>
                                       </div>
                                    </div>
									<div class="col-md-3">
                                    <div class="pluging">
                                       <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    </div>
                                 </div>
                                          <!-----  Model  ----->
                                          <div id="myModalpeople1" class="modal fade" role="dialog">
                                          <div class="modal-dialog" style="width: 700px;">
                                             <!-- Modal content-->
                                             <div class="modal-content">
                                                <div class="modal-header">
                                                   <button type="button" class="close" data-dismiss="modal">×</button>
                                                   <h1></h1>
                                                </div>
                                                <div class="modal-body">
                                                   <div class="row">
                                                      <div clss="col-md-12">
                                                                                                               <div class="col-md-6">
                                                            <div class="poll">
                                                               <div class="col-md-3 col-sm-2 col-xs-3">
                                                                  <div class="imgforr">
                                                                     <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/02/88575ed2a1b.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-9 col-sm-10 col-xs-9">
                                                                  <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ankit Mittal</a></strong> , Student Consult..</p>

                                                                  <p class="rigwd"> April 19th, 2019 12:04 pm </p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                                                                               <div class="col-md-6">
                                                            <div class="poll">
                                                               <div class="col-md-3 col-sm-2 col-xs-3">
                                                                  <div class="imgforr">
                                                                     <a href="#"><img src="https://edukeeda.com/wp-content/uploads/2019/03/ca060a69056.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
                                                                  </div>
                                                               </div>
                                                               <div class="col-md-9 col-sm-10 col-xs-9">
                                                                  <p class="catp"><strong><a href="https://edukeeda.com/newmembers?action=">Ashish Prasad</a></strong> , 5 Years of Expe..</p>

                                                                  <p class="rigwd"> April 19th, 2019 3:42 am </p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                                                                            </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          </div>
                                    
                                 </div>
                              </div>
                                                         </div>
                     </div>


                     <div id="discgroup" class="tabcontent" style="display:none;">
                                             <div class="col-md-12 blo">
                             <div class="col-md-9">
                                <div class="cat"> 
                                                                    <a href="https://edukeeda.com/discussioncategory?action=4&amp;actions=38">
                                                                        Railways <span>(1 Discussion House, 4 Posts)</span>
                                    </a>
                                </div>
                             </div>
                             <div class="col-md-3">
                                <div class="plug">
                                                                                <form method="post" action="">
                                               <input type="hidden" name="maingroupId" value="4">
                                               <input type="hidden" name="subgroupId" value="38">
                                               <img src="/img/follow-icon-selected.png"><button type="submit" name="unfollowgroup">Follow </button>
                                            </form>
                                                                    </div>
                             </div>
                          </div>
                    
                     </div>

                     <div id="bookmarksdtls" class="tabcontent" style="display:none;">
                                                <div class="col-md-12 blo">
                           <div class="">
                              <div class="col-md-4 col-sm-4">
                                 <a href="https://edukeeda.com/industry/gum-disease-may-worsen-blood-pressure-problems/">
                                 <img src="https://edukeeda.com/wp-content/uploads/2019/01/gum-disease-blood-pressure.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                 </a>
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-8">
                              <p class="categtp">
                                 <a href="https://edukeeda.com/expertise/?id=82&amp;action=industry">MINING, COAL &amp; METAL</a>
                              </p>
                              <a class="bloghdaedu" href="https://edukeeda.com/industry/gum-disease-may-worsen-blood-pressure-problems/"> Gum Disease May Worsen Blood Pressure Problems Gum Disease May Worsen Blood Pres... </a>
                              <p class="fullwd"></p>
                              <p class="leftwd"><strong>By </strong> admin                                 <a href="#" class="rightwd views"><i class="fa fa-eye"></i> 294 Views</a>
                              </p>
                              <p class="rightwd"><strong> </strong> January 25th, 2019 . 7:16 am</p>
                           </div>
                        </div>
                        
                                                                                                                        <div class="col-md-12 blo">
                           <div class="">
                              <div class="col-md-4 col-sm-4">
                                 <a href="https://edukeeda.com/success_story/india-and-world-stories/">
                                 <img src="https://edukeeda.com/wp-content/uploads/2019/01/HeroShotFinal.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                 </a>
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-8">
                              <p class="categtp">
                                 <a href="https://edukeeda.com/expertise/?id=79&amp;action=success_story">SUCCESS STORY</a>
                              </p>
                              <a class="bloghdaedu" href="https://edukeeda.com/success_story/india-and-world-stories/"> India and World Stories </a>
                              <p class="fullwd"></p>
                              <p class="leftwd"><strong>By </strong> admin                                 <a href="#" class="rightwd views"><i class="fa fa-eye"></i> 8 Views</a>
                              </p>
                              <p class="rightwd"><strong> </strong> January 25th, 2019 . 7:16 am</p>
                           </div>
                        </div>
                                                <div class="col-md-12 blo">
                           <div class="">
                              <div class="col-md-4 col-sm-4">
                                 <a href="https://edukeeda.com/success_story/gum-disease-may-worsen-blood-pressure-problems/">
                                 <img src="https://edukeeda.com/wp-content/uploads/2019/01/gum-disease-blood-pressure.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                 </a>
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-8">
                              <p class="categtp">
                                 <a href="https://edukeeda.com/expertise/?id=79&amp;action=success_story">SUCCESS STORY</a>
                              </p>
                              <a class="bloghdaedu" href="https://edukeeda.com/success_story/gum-disease-may-worsen-blood-pressure-problems/"> Gum Disease May Worsen Blood Pressure Problems </a>
                              <p class="fullwd"></p>
                              <p class="leftwd"><strong>By </strong> admin                                 <a href="#" class="rightwd views"><i class="fa fa-eye"></i> 270 Views</a>
                              </p>
                              <p class="rightwd"><strong> </strong> January 25th, 2019 . 7:16 am</p>
                           </div>
                        </div>
                                                                        <div class="col-md-12 blo">
                           <div class="">
                              <div class="col-md-4 col-sm-4">
                                 <a href="https://edukeeda.com/inside_campus_story/advancing-women-in-science/">
                                 <img src="https://edukeeda.com/wp-content/uploads/2019/02/e559f99cbfe.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300">
                                 </a>
                              </div>
                           </div>
                           <div class="col-md-8 col-sm-8">
                              <p class="categtp">
                                 <a href="https://edukeeda.com/expertise/?id=80&amp;action=inside_campus_story">INSIDE CAMPUS STORY</a>
                              </p>
                              <a class="bloghdaedu" href="https://edukeeda.com/inside_campus_story/advancing-women-in-science/"> Advancing Women in Science </a>
                              <p class="fullwd"></p>
                              <p class="leftwd"><strong>By </strong> <a href="https://edukeeda.com/newmembers/?action=49"> Ankit Mittal </a>                                 <a href="#" class="rightwd views"><i class="fa fa-eye"></i> 153 Views</a>
                              </p>
                              <p class="rightwd"><strong> </strong> January 25th, 2019 . 7:16 am</p>
                           </div>
                        </div>
                                                                                                <div class="subc">
                           <div class="col-md-4 col-sm-5">
                              <a href="https://edukeeda.com/big_interview/quis-autem-vel-eum-iure-reprehenderit-qui-in-ea-voluptate/">
                              <img src="https://edukeeda.com/wp-content/uploads/2019/02/ct-st-interview-questions_1.jpg" "="" class="img-thumbnail" alt="Cinque Terre" width="200" height="200">
                              </a>
                           </div>
                           <div class="col-md-8 col-sm-7">
                              <p class="categtp" style="margin-bottom: 2px;"><strong></strong><a href="https://edukeeda.com/interviewhome/"> INTERVIEWS </a></p>
                              <div class="subcate">
                                 <p class="subb eventarticle"><a href="https://edukeeda.com/big_interview/quis-autem-vel-eum-iure-reprehenderit-qui-in-ea-voluptate/">Quis autem vel eum iure reprehenderit qui in ea voluptate</a></p>
                              </div>
                           </div>
                           <div class="col-md-4 col-sm-7">
                              <div class="subcate">
                                 <span><strong>Sanjay Kumar</strong>
                                 </span>
                                 <span>Sports </span>
                              </div>
                           </div>
                           <div class="col-md-4 col-sm-7">
                              <div class="rightwd views">
                                 <div class="dropdown customs">
                                    <button class="dropbtn">
                                    <a href="#" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." class="rightwd views"> 
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <i class="fa fa-eye"></i> 39                                    </a>        
                                    </button>
                                    <div class="dropdown-content">
                                                                              <div class="heateor_sss_sharing_container heateor_sss_horizontal_sharing" ss-offset="0" heateor-sss-data-href="https://edukeeda.com/big_interview/quis-autem-vel-eum-iure-reprehenderit-qui-in-ea-voluptate/"><ul class="heateor_sss_sharing_ul"><li class="heateorSssSharingRound"><i style="width:35px;height:35px;border-radius:999px;" alt="Facebook" title="Facebook" class="heateorSssSharing heateorSssFacebookBackground" onclick="heateorSssPopup(&quot;https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fedukeeda.com%2Fbig_interview%2Fquis-autem-vel-eum-iure-reprehenderit-qui-in-ea-voluptate%2F&quot;)"><ss style="display:block;border-radius:999px;" class="heateorSssSharingSvg heateorSssFacebookSvg"></ss></i></li><li class="heateorSssSharingRound"><i style="width:35px;height:35px;border-radius:999px;" alt="Twitter" title="Twitter" class="heateorSssSharing heateorSssTwitterBackground" onclick="heateorSssPopup(&quot;https://twitter.com/intent/tweet?text=homepagemembers&amp;url=http%3A%2F%2Fedukeeda.com%2Fbig_interview%2Fquis-autem-vel-eum-iure-reprehenderit-qui-in-ea-voluptate%2F&quot;)"><ss style="display:block;border-radius:999px;" class="heateorSssSharingSvg heateorSssTwitterSvg"></ss></i></li><li class="heateorSssSharingRound"><i style="width:35px;height:35px;border-radius:999px;" alt="Whatsapp" title="Whatsapp" class="heateorSssSharing heateorSssWhatsappBackground"><a href="https://api.whatsapp.com/send?text=homepagemembers http%3A%2F%2Fedukeeda.com%2Fbig_interview%2Fquis-autem-vel-eum-iure-reprehenderit-qui-in-ea-voluptate%2F" rel="nofollow noopener" target="_blank"><ss style="display:block" class="heateorSssSharingSvg heateorSssWhatsappSvg"></ss></a></i></li><li class="heateorSssSharingRound"><i style="width:35px;height:35px;border-radius:999px;" alt="Google Gmail" title="Google Gmail" class="heateorSssSharing heateorSssGoogleGmailBackground" onclick="heateorSssPopup(&quot;https://mail.google.com/mail/?ui=2&amp;view=cm&amp;fs=1&amp;tf=1&amp;su=homepagemembers&amp;body=Link:http%3A%2F%2Fedukeeda.com%2Fbig_interview%2Fquis-autem-vel-eum-iure-reprehenderit-qui-in-ea-voluptate%2F&quot;)"><ss style="display:block;border-radius:999px;" class="heateorSssSharingSvg heateorSssGoogleGmailSvg"></ss></i></li></ul><div class="heateorSssClear"></div></div>                                    </div>
                                 </div>
                              </div>
                              <div class="subcate">
                              </div>
                           </div>
                        </div>
                                             </div>

                     <div id="questforyou" class="tabcontent" style="display:none;">
                        <p>question for you added here</p>
                     </div>

                  </div>
               </div>

               <article id="post-1801" class="post-1801 page type-page status-publish hentry">
                  <header class="entry-header">
                     <h1 class="title-post entry-title">members</h1>
                  </header>
                  <!-- .entry-header -->
                  <div class="entry-content">
                     <div class="elementor elementor-1801">
                        <div class="elementor-inner">
                           <div class="elementor-section-wrap">
                              <section data-id="3a7a8ddb" class="elementor-element elementor-element-3a7a8ddb elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
                                 <div class="elementor-container elementor-column-gap-default">
                                    <div class="elementor-row">
                                       <div data-id="705836b8" class="elementor-element elementor-element-705836b8 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                          <div class="elementor-column-wrap elementor-element-populated">
                                             <div class="elementor-widget-wrap">
                                                <div data-id="46a97739" class="elementor-element elementor-element-46a97739 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
                                                   <div class="elementor-widget-container">
                                                      <div class="elementor-text-editor elementor-clearfix"></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- .entry-footer -->
               </article>
               <!-- #post-## -->
               <!-- #main -->
            </div>
         </div>
		 <div class="col-md-3">
         <div class="advtspace" style="">
         </div>
         <div class="headarea">
            <div class="other">
               <h4>Member's Activity</h4>
            </div>
                        <div class="addposituon bottomspc">
               <p class="tablinks" onclick="openCity(event, 'peowal')">WallPosts <span class="countno">8</span>
               </p>
               <p class="tablinks" onclick="openCity(event, 'articlesedu')">Posts / Articles / Blogs <span class="countno">2</span>
               </p>
               <p class="tablinks" onclick="openCity(event, 'eventedu')">Events <span class="countno">3</span>
               </p>
               <p class="tablinks" onclick="openCity(event, 'questionare')">Questions <span class="countno">7</span>
               </p>
               <p class="tablinks" onclick="openCity(event, 'anwerspat')">Answers <span class="countno">10</span>
               </p>
               <!--<p class="tablinks" onclick="openCity(event, 'directquest')">Direct Questions <span class="countno">0</span>
                  </p>-->
               <p class="tablinks" onclick="openCity(event, 'intrestquest')">Interested Activity <span class="countno"> 3 </span>
               </p>
               <p class="tablinks" onclick="openCity(event, 'discussquest')">Discussion Houses <span class="countno">1</span>
               </p>
               <p class="tablinks" onclick="openCity(event, 'discgroup')">Discussion Groups <span class="countno"> 1 </span>
               </p>
               <p class="tablinks" onclick="openCity(event, 'bookmarksdtls')">Bookmark <span class="countno"> 5 </span></p>
               <p class="tablinks" onclick="openCity(event, 'questforyou')">Questions For You <span class="countno">20</span>
               </p>
			    <p class="tablinks" onclick="#(event, '#')">Profile
               </p>
            </div>
         </div>
      </div>
		   </div>
	     </div>
	  </div>
    </div>  
  </div>
</div>