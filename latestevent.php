<div class="List Of Events">
<?php
/**
/*
Template Name: Letest Event
*/
get_header(); ?>
<style>
.subcate span a {
    color: #828282;
}
.spacetop.sortbyalphabet {
    float: left;
    width: 100%;
    margin: 10px 0px;
}
ul.catLatest {
    display: grid;
}

  .datespn {
  position: absolute;
  top: 11px;
  left: 24px;
  background: #fff;
  padding: 7px;
  color: #060658;
  font-size: 24px;
  text-align: center;
  border: 1px solid lavenderblush;
  border-radius: 7px;
  line-height: 17px;
  }
  .datespn span {
  font-weight: 600;
  }
  span.dateDisplay-day {
  color: #f68e2f;
  font-size: 20px;
  font-weight: 800;
  line-height: 21px;
  }
  .subc i.fa.fa-share-alt {
  float: right;
  }
  span.dateDisplay-day {
    font-size: 20px;
    font-weight: 800;
    line-height: 21px;
    color: #e96125;
}
span.dateDisplay-month.text--tiny {
    color: #060658!important;
}
p.rightwd, p.leftwd{
  font-size:15px; 
}
  .subc
  {
  box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
  float: left;
  width: 100%;
  margin-bottom: 30px;
  padding:10px;
  border:1px solid #ddd;
  }
  .intdate span {
  font-size: 13px;
  font-weight: 600;
  display: block;
  padding: 0px;
  background: #f4f4f4;
  border-radius:10px;
  margin: 0px 53px 0px 53px;
  text-align: center;
  line-height: 25px;
  }
  .detail a {
  background: #439a37;
  color: #fff;
  padding: 6px 18px;
  font-size: 13px;
  display: block;
  text-align: center;
  margin: 13px 0 0 0;
  text-transform: capitalize;
  }
  .detail a:hover {
  background-color: #f68e2f;
  }
  .subc i{
  margin-right: 0px!important;
  }
  p.subb {
  font-size: 20px;
  font-weight: 600;
  margin-bottom: 0px;
  float: left;
  }

  .subcate strong {
  font-size: 15px;
  }
  .subcate span {
  display: block;
  }
  .leftoption {
  float: left;
  }
  .leftoption input[type="Text"] {
  width: 100%;
  background: #fff;
  border: 1px solid #ddd;
  height: 31px;
  padding: 0px 16px;
  float: left;
  }
  .filterkeeda i.fa.fa-search {
  padding: 0px;
  margin-top: -3px;
  }
  .filterkeeda button {
  height: 35px;
  margin: 0px 4px;
  }
  .rightoption {
  float: right;
  }
  .rightoption select {
  background: #fff;
  border: 1px solid #ddd;
  height: 33px;
  line-height: 3px;
  margin: 0px;
  }
  .rightoption p{
  float:left;
  margin:0px 30px;
  }
  #secondary{width:100%;}
  input.applyfilt {
  padding: 4px 17px;
  float: right;
  }
  .sortbyalphabet ul li {
  display: inline-block;
  margin: 0 19px 0 0;
  }
  p.subb.eventarticle {
  line-height: 23px;
  min-height: 57px;
  }
 
  .simg {
    float: left;
    width: 100%;
}
.othermemd {
    float: left;
    width: 100%;
  margin-bottom: 15px;
    border-bottom: 1px solid#ffaf8c;
}
.simg a {
    float: left;
    width: 100%;
}
.pimg {
    float: left;
    width: 100%;
}
.pimg p {
    margin-bottom: 4px!important;
}
.pimg span {
    font-size: 15px!important;
    line-height: 0px!important;
}
.postshare span {
    margin: 0px 76px 0px 6px;
  display: -webkit-box;
}
.postshare {
    float: left;
    width: 100%;
    display: -webkit-box;
      margin: 0px 16px;
}
.postshare p{
  margin-bottom: 0px!important;
}
.pimg a {
    line-height: 15px;
    float: left;
    width: 100%;
    margin: 4px 0px;
}
div#secondary a:hover {
    color: #e86125;
}
div#secondary a {
    font-weight: 600;
    font-size: 14px;
    color: #2b221f;
  line-height: 25px;}

.simg img.img-thumbnail {
    width: 100%;
    height: auto;
}
.pimg span {
    font-size: 15px!important;
    line-height: 15px!important;
}
.pimg {
    float: left;
    width: 100%;
    margin-bottom: 11px;
}
#secondary {
    width: 100%;
    margin-top: 107px;
}
#secondary button.accordion {
    background-color: transparent;
    border: none;
    width: 100%;
    text-align: left;
    border-bottom: 1px solid #f68e2f;
    padding:0px;
}
#secondary button.accordion:hover {
    outline: none;
}
.latestsidebar{padding-top: 0px!important;}
.searchform input {
    height: 36px !important;
}
.panel {
  display: none;
  background-color: #f4f4f4;
}
.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
    margin-top: -37px;
    font-size: 43px;
}

.active:after {
    content: "\2212";
}
a.bloghdaedu {
    font-size: 20px;
    margin: 0;
    padding: 0;
    line-height: 20px;
    float: left;
    width: 100%;
    font-weight: 600;
   
}
.subcate .categtp{
    margin-bottom: 7px;
    font-size: 15px;
    text-align: left;
}
p.leftwd {
    color: #9e9292;
    margin-bottom: 7px;
}
p.rightwd {
    margin-bottom: 0px;
}
.col-md-2.postshare1 p {
    margin-bottom: 0px!important;
}
.letesheaderevent {
    float: left;
    width: 100%;
    margin-top: 20px;
   margin-bottom:26px;
}
.col-md-4.col-sm-12.eventside {
    display: none;
}
 @media only screen and (max-width: 767px) {
  .searchform input {
    height: 36px!important;
    padding: 4px 32px!important;
}   
.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 15px!important;
}
.page-wrap {
    padding-top: 60px;
}
.col-md-4.col-sm-12.eventside {
    display: block;
    padding: 0px;
}

.col-md-4.col-sm-12.eventdesk {
    display: none;
}
 #secondary {
    width: 100%;
    margin-top: 0px;
}
.subc {

    margin-top: 20px;
}
h3 {
    font-size: 22px;
    margin-top: 127px;
}
.rightoption {
    float: right;
    width: 100%;
}
.leftoption {
    float: left;
    width: 100%;
}
a.bloghdaedu {
    min-height: 50px;
    overflow: hidden;
}
.rightoption select{
padding:1px 20px;
}
.letesheaderevent {
    float: left;
    width: 100%;
    margin-top: 11px;
    margin-bottom: 0px;
}
 }
 p.categtp a{text-transform: uppercase;}
 a.rightwd.views {
    color: #000;
    font-size: 13px;
    float: right;
}
</style>
   <div class="col-md-8 col-sm-12">
      <div class="letesheaderevent">
          <h4>Latest Events</h4>
      </div>
      <div class="">
         <div class="filterkeeda">
            <div class="leftoption">
               <form class="searchform" action=" " method="post">
                  <input type="text" placeholder="Search.." name="search" class="searchtext">
                  <button type="submit"><i class="fa fa-search"></i></button>
               </form>
            </div>
            <div class="rightoption">
               <form action="" method="post" name="myFormName">
                  <p>Sort By:</p>
                  <select name="top_filter" id="topdropfilter" onchange="this.form.submit()">
                    <option value=""> Select </option>
                     <option value="date">Latest Events</option>
                     <option value="recent">New Posts</option>
                     <option value="views">Most Viewed </option>
                  </select>
               </form>
            </div>
         </div>
      </div>
       <div class="col-md-4 col-sm-12 eventside">
      <div id="secondary" class="widget-area col-md-3" role="complementary">
        <aside id="recent-posts-3" class="widget widget_recent_entries">
          <button class="accordion">  <h3 class="widget-title">EXPLORE BY EVENT CATEGORIES</h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">  
          <?php
            $categories = get_categories( array(
                 'child_of'            => 0,
                 'current_category'    => 0,
                  'parent'         => 0,
                 'depth'               => 0,
                 'echo'                => 1,
                 'exclude'             => '',
                 'exclude_tree'        => '',
                 'feed'                => '',
                 'feed_image'          => '',
                 'feed_type'           => '',
                 'hide_empty'          => 0,
                 'hide_title_if_empty' => false,
                 'hierarchical'        => true,
                 'order'               => 'ASC',
                 'orderby'             => 'name',
                 'separator'           => '<br />',
                 'show_count'          => 0,
                 'show_option_all'     => '',
                 'show_option_none'    => __( 'No categories' ),
                 'style'               => 'list',
                 'taxonomy'            => 'events_categories',
                 'title_li'            => __( 'Categories' ),
                 'use_desc_for_title'  => 1,
             ) );
              echo "<ul class='catLatest'>";
              if(!empty($categories))
              {
                  foreach ($categories as $terms) {
                  
                  $term_id = $terms->term_id;
                  $image   = category_image_src( array('term_id'=>$term_id) , false );
                  $category_link = get_category_link( $term_id );
                  
                  $getLists = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT wp_term_relationships.object_id FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE wp_term_relationships.term_taxonomy_id=%d AND wp_posts.post_status='publish' AND wp_postmeta.meta_key='_event_date_end' AND wp_postmeta.meta_value>='$current'", $term_id));

                  $totalPost = 0;
                  foreach($getLists as $getList) {
                    $eventDateEnd1 = get_field('event_date_end', $getList->object_id);
                    $currentDate1 = date("Y-m-d");
                    $eventDateEnd1 = date("Y-m-d", strtotime($eventDateEnd1));
                    
                    $eDate1 = (int) strtotime($eventDateEnd1);
                    $cDate1 = (int) strtotime($currentDate1);

                    if($eDate1 >= $cDate1) {
                        $totalPost++;
                    }
                  }

                  //$getListCount = $wpdb->get_results( 
                    //$wpdb->prepare( "SELECT COUNT(object_id) as countPost FROM wp_term_relationships WHERE term_taxonomy_id=%d", $term_id));
           ?>
                 
                   <li><a href="<?php echo site_url(); ?>/events-list/?id=<?php echo $term_id; ?>"> <?php echo $terms->name; ?> ( <?php echo $totalPost; ?> )</a></li>
              <?php 
                    }
                  }
              ?>
              </ul></div>
        </aside>
      </div>
   </div>
      <?php
      if(isset($_POST["search"])) {

         global $wpdb;
         $searchFilter = $_POST["search"];
         $action = "event";

         $results1 = $wpdb->get_results( 
             $wpdb->prepare( "SELECT * FROM wp_posts WHERE post_status='publish' AND post_type=%s", $action) 
         );
           
           foreach($results1 as $result1) {
             
             if(stripos($result1->post_title, $searchFilter) !== false) {
             
              $cat = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE object_id = %d", $result1->ID) 
              );
              $getCatName = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT term_id, name FROM wp_terms WHERE term_id = %d", $cat[0]->term_taxonomy_id) 
              );
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                
                if($image){
                  $image = $image[0];
                } else {
                    $mkey = "_thumbnail_id";
                    $checkimg = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                    );
                    $getimg = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                    );
                    $image = $getimg[0]->guid;  
                }
                
                $table_name = "wp_post_views";
                $checkViews = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
                );
                 $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                           $author_name = $getAuthor[0]->display_name;
                $countPostTitle = strlen($result1->post_title);
                if($countPostTitle >= 80) {
                  $postTitle = substr($result1->post_title,0, 80);
                  $postTitle .= "...";
                } else {
                  $postTitle = $result1->post_title;
                }
                 $eventDate = get_field('event_date', $result1->ID);
                 $eventDateEnd = get_field('event_date_end', $result1->ID);
                $currentDate = date("Y-m-d");
                $eventDateEnd = date("Y-m-d", strtotime($eventDateEnd));
                
                $eDate = (int) strtotime($eventDateEnd);
                $cDate = (int) strtotime($currentDate);
                if($eDate >= $cDate) {
    ?>
            <div class="subc">
                 <div class="col-md-4 col-sm-5">
                    <div class="altimg">
                       <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><img class="img-thumbnail" src="<?php echo $image; ?>"></a>
                       <div class="datespn">
                          <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                          <span class="dateDisplay-day">
                          <span><?php echo date('d',strtotime($eventDate)); ?></span><br>
                          </span>
                          <span class="dateDisplay-month text--tiny">
                          <span><?php echo date('M',strtotime($eventDate)); ?></span>
                          </span>
                          </time>
                       </div>
                    </div>
                 </div>
                 <div class="col-md-8 col-sm-7">
                    <div class="subcate">
                      <p class="categtp">
                        <a href="<?php echo site_url();?>/events/"> Event</a>
                        <a href="<?php echo site_url();?>/events-list/?id=<?php echo $getCatName[0]->term_id;?>"> . <?php echo $getCatName[0]->name; ?></a> 
                      </p>
                      <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><?php echo $postTitle; ?></a>
                    </div>
                    <p class="leftwd">
                    <strong>
                    Post By :<?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/newmembers/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                    </strong>
                    </p>
                    
                    <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" class="rightwd views"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                    
                    <p class="rightwd"><strong> </strong> <?php //echo get_the_date('F jS, Y h:iA',$result1->ID) ?></p>
                 </div>
              </div>
  <?php
            }
          }
        }
    } else if(isset($_POST["top_filter"])) {
         $catId = $_GET["id"];
         $action = "event";
         $filter = $_POST["top_filter"];
         global $wpdb;
         echo "<div style='clear:both;'></div>";
         
         if($filter == "views") {
         
           echo "<div><p>Sort by: Most Views</p></div>";
           $results1 = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_posts INNER JOIN wp_post_views ON wp_posts.ID = wp_post_views.post_id WHERE wp_posts.post_type=%s AND wp_posts.post_status='publish' ORDER BY wp_post_views.view_count DESC", $action));
         
         } else if($filter == "recent") {
            
            echo "<div><p>Sort by: Recent Post</p></div>";
            $results1 = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_posts WHERE post_status='publish' AND post_type=%s ORDER BY ID DESC", $action));
         } else if($filter == "date") {
            
            echo "<div><p>Sort by: Event date</p></div>";
            $results1 = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_posts INNER JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id WHERE wp_posts.post_type=%s AND wp_posts.post_status='publish' AND wp_postmeta.meta_key ='event_date' ORDER BY wp_postmeta.meta_value ASC", $action));
         }
           
          foreach($results1 as $result1) {
            
           $cat = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE object_id = %d", $result1->ID));
            $getCatName = $wpdb->get_results( 
                $wpdb->prepare( "SELECT term_id, name FROM wp_terms WHERE term_id = %d", $cat[0]->term_taxonomy_id));
           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail');
           
            if($image){
                $image = $image[0];
            } else {
              $mkey = "_thumbnail_id";
              $checkimg = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
              );
              $getimg = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
              );
              $image = $getimg[0]->guid;  
            }

             $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                           $author_name = $getAuthor[0]->display_name;
            $table_name = "wp_post_views";
            $checkViews = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM $table_name WHERE post_id = %d", $result1->ID) 
            );
            $countPostTitle = strlen($result1->post_title);
            if($countPostTitle >= 80) {
              $postTitle = substr($result1->post_title,0, 80);
              $postTitle .= "...";
            } else {
              $postTitle = $result1->post_title;
            }
            $eventDate = get_field('event_date', $result1->ID);
            $eventDateEnd = get_field('event_date_end', $result1->ID);
            $currentDate = date("Y-m-d");
            $eventDateEnd = date("Y-m-d", strtotime($eventDateEnd));
            
            $eDate = (int) strtotime($eventDateEnd);
            $cDate = (int) strtotime($currentDate);
            if($eDate >= $cDate) {       
    ?>
           <div class="subc">
                 <div class="col-md-4 col-sm-5">
                    <div class="altimg">
                       <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><img class="img-thumbnail" src="<?php echo $image; ?>"></a>
                       <div class="datespn">
                          <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                          <span class="dateDisplay-day">
                          <span><?php echo date('d',strtotime($eventDate)); ?></span><br>
                          </span>
                          <span class="dateDisplay-month text--tiny">
                          <span><?php echo date('M',strtotime($eventDate)); ?></span>
                          </span>
                          </time>
                       </div>
                    </div>
                 </div>
                 <div class="col-md-8 col-sm-7">
                    <div class="subcate">
                      <p class="categtp">
                        <a href="<?php echo site_url();?>/events/"> Event</a>
                        <a href="<?php echo site_url();?>/events-list/?id=<?php echo $getCatName[0]->term_id;?>"> . <?php echo $getCatName[0]->name; ?></a> 
                      </p>
                      
                      <a class="bloghdaedu"href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><?php echo $postTitle; ?></a>
                    </div>
                    <p class="leftwd">
                    <strong>
                    Post By :<?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/newmembers/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                    </strong>
                    </p>
                    <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" class="rightwd views"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($result1->ID) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                    <p class="rightwd"><strong> </strong> <?php //echo get_the_date('F jS, Y h:iA',$result1->ID) ?></p>
                 </div>
              </div>
  <?php
          }
        }
    } else {
        $action = "event";

       global $wpdb;

         $results1 = $wpdb->get_results( 
             $wpdb->prepare( "SELECT * FROM wp_posts WHERE post_status='publish' AND post_type=%s ORDER BY id DESC LIMIT 30", $action) 
         );
         
          foreach($results1 as $result1) {
      
          $cat = $wpdb->get_results( 
              $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE object_id = %d", $result1->ID) 
          );
          $getCatName = $wpdb->get_results( 
              $wpdb->prepare( "SELECT term_id, name FROM wp_terms WHERE term_id = %d", $cat[0]->term_taxonomy_id) 
          );
           
           $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
            if($image){
                $image = $image[0];
            } else {
                $mkey = "_thumbnail_id";
              $checkimg = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
              );
              $getimg = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
              );
              $image = $getimg[0]->guid;  
            }
            $getAuthor = $wpdb->get_results( 
                            $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d", $result1->post_author));
                           $author_name = $getAuthor[0]->display_name;
            $countPostTitle = strlen($result1->post_title);
            if($countPostTitle >= 80) {
              $postTitle = substr($result1->post_title,0, 80);
              $postTitle .= "...";
            } else {
              $postTitle = $result1->post_title;
            }
            $eventDate = get_field('event_date', $result1->ID);
            $eventDateEnd = get_field('event_date_end', $result1->ID);
            $currentDate = date("Y-m-d");
            $eventDateEnd = date("Y-m-d", strtotime($eventDateEnd));
            
            $eDate = (int) strtotime($eventDateEnd);
            $cDate = (int) strtotime($currentDate);
            
            if($eDate >= $cDate) {
      ?>
              <div class="subc">
                 <div class="col-md-4 col-sm-5">
                    <div class="altimg">
                       <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><img class="img-thumbnail" src="<?php echo $image; ?>"></a>
                       <div class="datespn">
                          <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                          <span class="dateDisplay-day">
                          <span><?php echo date('d',strtotime($eventDate)); ?></span><br>
                          </span>
                          <span class="dateDisplay-month text--tiny">
                          <span><?php echo date('M',strtotime($eventDate)); ?></span>
                          </span>
                          </time>
                       </div>
                    </div>
                 </div>
                 <div class="col-md-8 col-sm-7">
                    <div class="subcate">
                      <p class="categtp">
                       <a href="<?php echo site_url();?>/events/"> Event</a> 
                       <a href="<?php echo site_url();?>/events-list/?id=<?php echo $getCatName[0]->term_id;?>"> . <?php echo $getCatName[0]->name; ?></a>
                      </p>
                      
                      <a class="bloghdaedu" href="<?php echo esc_url( get_permalink($result1->ID) ); ?>"><?php echo $postTitle; ?></a>
                    </div>
                    <p class="leftwd">
                    <strong>
                    Post By :<?php if($author_name != "admin"){ ?> 
                                       <?php if($getAuthor[0]->user_status == 1){ ?>
                                          <a href="<?php echo site_url();?>/newmembers/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a>
                                          <?php } else{echo $author_name;} ?>
                                       <?php } else { echo $author_name; } ?>
                    </strong></p>

                    <div class="dropdown customs">      
                            <button class="dropbtn">
                               <a href="#" class="rightwd views"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                            </button>
                        <div class="dropdown-content">
                          <?php $urlshare = esc_url( get_permalink($value['ID']) );
                            $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                          ?>
                            <?php echo do_shortcode("$urlsharing"); ?>
                        </div>
                    </div>
                    
                    

                    <p class="rightwd"><strong> </strong> <?php //echo get_the_date('F jS, Y h:iA',$result1->ID) ?></p>
                 </div>
              </div>
      <?php
          }
        }
    }
  ?>
   </div>
   <div class="col-md-4 col-sm-12 eventdesk">
      <div id="secondary" class="widget-area col-md-3" role="complementary">
        <aside id="recent-posts-3" class="widget widget_recent_entries">
          <button class="accordion">  <h3 class="widget-title">EXPLORE BY EVENT CATEGORIES</h3></button> 
        <div class="panel">   <div class="spacetop sortbyalphabet">  
          <?php
            $categories = get_categories( array(
                 'child_of'            => 0,
                 'current_category'    => 0,
                  'parent'         => 0,
                 'depth'               => 0,
                 'echo'                => 1,
                 'exclude'             => '',
                 'exclude_tree'        => '',
                 'feed'                => '',
                 'feed_image'          => '',
                 'feed_type'           => '',
                 'hide_empty'          => 0,
                 'hide_title_if_empty' => false,
                 'hierarchical'        => true,
                 'order'               => 'ASC',
                 'orderby'             => 'name',
                 'separator'           => '<br />',
                 'show_count'          => 0,
                 'show_option_all'     => '',
                 'show_option_none'    => __( 'No categories' ),
                 'style'               => 'list',
                 'taxonomy'            => 'events_categories',
                 'title_li'            => __( 'Categories' ),
                 'use_desc_for_title'  => 1,
             ) );
              echo "<ul class='catLatest'>";
              if(!empty($categories))
              {
                  foreach ($categories as $terms) {
                  
                  $term_id = $terms->term_id;
                  $image   = category_image_src( array('term_id'=>$term_id) , false );
                  $category_link = get_category_link( $term_id );
                  
                  $getLists = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT wp_term_relationships.object_id FROM wp_term_relationships INNER JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id INNER JOIN wp_postmeta ON wp_postmeta.post_id = wp_posts.ID WHERE wp_term_relationships.term_taxonomy_id=%d AND wp_posts.post_status='publish' AND wp_postmeta.meta_key='_event_date_end' AND wp_postmeta.meta_value>='$current'", $term_id));

                  $totalPost = 0;
                  foreach($getLists as $getList) {
                    $eventDateEnd1 = get_field('event_date_end', $getList->object_id);
                    $currentDate1 = date("Y-m-d");
                    $eventDateEnd1 = date("Y-m-d", strtotime($eventDateEnd1));
                    
                    $eDate1 = (int) strtotime($eventDateEnd1);
                    $cDate1 = (int) strtotime($currentDate1);

                    if($eDate1 >= $cDate1) {
                        $totalPost++;
                    }
                  }

                  //$getListCount = $wpdb->get_results( 
                    //$wpdb->prepare( "SELECT COUNT(object_id) as countPost FROM wp_term_relationships WHERE term_taxonomy_id=%d", $term_id));
           ?>
                 
                   <li><a href="<?php echo site_url(); ?>/events-list/?id=<?php echo $term_id; ?>"> <?php echo $terms->name; ?> ( <?php echo $totalPost; ?> )</a></li>
              <?php 
                    }
                  }
              ?>
              </ul></div>
        </aside>
      </div>
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
