<div class="Postyourpage">
<?php
session_start();
/**
/*
 Template Name: discussion Query Form Page
 */
if($_SESSION["login"]) {

} else {
  header('Location: https://edukeeda.com/signin/');
}  
$userId = $_SESSION["login"]["id"];
get_header(); ?>
<script type="text/javascript">
    $(document).ready(function(){
      $("#catone").on("change", function(){
          $(".showone").css("display","none");
          $(".showtwo").css("display","none");
          $(".showthree").css("display","none");
            var cid = $(this).val();
            $.ajax({
                type: "POST",
                dataType: "json",
                url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_discuss_cat_list.php",
                data:{cid:cid},
                success : function(data) {
                    if(data != " ") {  
                      $(".showone").css("display","block");
                      $("#cattwo").html(data);

                    } else {
                      $(".showone").css("display","none");
                        $.ajax({
                          type: "POST",
                          url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_discuss_cat_list.php",
                          data:{ccid:cid},
                          success : function(data) {
                            $(".showtwo").css("display","block");       
                            $("#catthree").html(data);
                          },
                          error: function(jqXHR, textStatus, errorThrown){
                              console.log(textStatus, errorThrown);
                          }
                        });
                    } 
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(textStatus, errorThrown);
                }
            });
      });
  });

  $(document).ready(function(){
      $("#cattwo").on("change", function(){
            var sid = $(this).val();

            $.ajax({
                type: "POST",
                url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_discuss_cat_list.php",
                data:{sid:sid},
                success : function(data) {
                    $(".showtwo").css("display","block");       
                    $("#catthree").html(data);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(textStatus, errorThrown);
                }
            });
      });
  });
  $(document).ready(function(){
      $("#catthree").on("change", function(){
          $(".showthree").css("display","block"); 
      });
  });
</script>
   <div class="col-md-12 col-sm-12">
      <div class="filteropt">
         <p>Discussion Forum</p>
         <div class="row">
            <div class="col-md-12" style="padding: 45px;">
              <form method="post" action="<?php echo site_url(); ?>/wp-content/themes/sydney/discussquerycode.php" enctype="multipart/form-data" id="expertform">
                  
                  <div class="form-group"> 
                    <label for="title" style="font-size: 21px;">Which category of Discussion is best suited for your Forum?</label>
                    <select name="catone" class="form-control" id="catone" required>
                        <option value=""> Select </option>
                    <?php
                      $catOnes = $wpdb->get_results("SELECT * FROM wp_discussion_main");
                      foreach($catOnes as $catOne) {
                    ?>
                        <option value="<?php echo $catOne->id; ?>"> <?php echo $catOne->name; ?> </option>
                    <?php
                      }
                    ?>
                    </select>
                  </div>

                  <div class="form-group showone" style="display: none">
                    <label for="title">Sub Category</label>
                    <select name="cattwo" class="form-control" id="cattwo">
                        <option value=""> Select </option>
                    </select>
                  </div>
                 
                  <div class="form-group showtwo" style="display: none">
                    <label for="title">Discussion House</label>
                    <select name="catthree" class="form-control" id="catthree" required>
                        <option value=""> Select </option>
                    </select>
                  </div>

                  <div class="showthree" style="display: none">
                    <div class="form-group">
                      <label for="title">Question:</label>
                      <textarea name="desc" class="form-control myTextarea" minlength="250" id="short"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" name="submitdiscuss" value="Submit" class="btn btn-primary expertSubmit">
                    </div>
                  </div>
              </form>
            </div>
          </div>
      </div>
   </div>

   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>
<style>
   .stu {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   margin-bottom: 50px;
   }
   .stu h1 {
   font-size: 27px;
   margin: 15px 30px;
   }
   .optstu {
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   float: left;
   width: 100%;
   margin: 14px 3px;
   }   
   .optstu p {
   margin-bottom: 0px;
   font-size: 13px;
   color: #060658;
   padding: 20px;
   }
   .optstu input[type="checkbox"] {
   margin: 0px 11px;
   }
   .filteropt {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   margin-bottom: 30px;
   margin-top: 50px;
   padding-bottom: 30px;
   }
   .Query {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   }
   .Query p {
   font-size: 13px;
   margin: 36px 28px;
   float: left;
   }
   .Queryw{
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   }
   .Queryw p {
   font-size: 24px;
   margin: 36px 28px;
   float: left;
   font-weight: 600;
   color: #060658;
   }
   .sub {
   float: left;
   width: 100%;
   margin: 15px 0px;
   text-align: center;
   }
   .sub button.btn.btn-primary {
   background: #f68e2f;
   }
   .filteropt select {
   background: #fff;
   border: 1px solid #ddd;
   height: 42px;
   line-height: 8px;
   padding: 1px 16px;
   margin: 39px 0px 0 22px;
   }
   .filteropt p{
   float: left;
    padding: 28px 10px 10px 30px;
    color: #0a0a5b;
    font-weight: 600;
    font-size: 24px;
    margin-bottom: 0px;
   }
   p.wantpost {
    margin-bottom: 0px!important;
    padding-bottom: 0px;
    font-weight: 300;
    margin-left: 5%;
  }
  .tab-content {
    border: 1px solid #ddd;
    min-height: 500px;
        padding: 33px;
}
.nav-tabs>li {
    width: 50%;
}
.filteropt select {
    margin: 5px 0px 0 1px!important;
}
</style>
<script type="text/javascript">
  bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>
<script type="text/javascript">

    $(document).ready(function() {
  tinymce.init({
    selector: ".myTextarea",
    theme: "modern",
    paste_data_images: true,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview media | forecolor backcolor emoticons",
    image_advtab: true,
    file_picker_callback: function(callback, value, meta) {
      if (meta.filetype == 'image') {
        $('#upload').trigger('click');
        $('#upload').on('change', function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.onload = function(e) {
            // var imgVal = e.target.result;
            // $.ajax({
            //   url: "<?php echo site_url(); ?>/wp-content/themes/sydney/uploadbaseimage.php",
            //   type: "post",
            //   data: {imgVal:imgVal},
            //   success: function (response) {
            //      var imageVal = response;
            //      console.log(response);
            //   },
            //   error: function (jqXHR, exception) {
            //       console.log(jqXHR);
            //       var imageVal = "error";
            //       // Your error handling logic here..
            //   }
            // });
            callback(e.target.result, {
              alt: ''
            });

          };
          reader.readAsDataURL(file);
        });
      }
    },
    templates: [{
      title: 'Test template 1',
      content: 'Test 1'
    }, {
      title: 'Test template 2',
      content: 'Test 2'
    }]
  });
});

</script>
