<div class="eventnew">
<?php
/**
/*
Template Name: Event and Seminars
*/
get_header(); ?>
   <div class="catchybga">
      <div class="want">
        
         <p>"When you're sharing, it offers the opportunity for someone to help you."<span>- Marala Scott</span></p>
       
       <div class="belowlinksedus">
         <ul>
            <li><a href="<?php if($_SESSION["login"]["id"]){echo "http://edukeeda.com/eventdetails/";}else{echo "#";}?>" <?php if($_SESSION["login"]["id"]){}else{ echo "data-toggle='modal' data-target='#myModalpost'"; }?> >Post An Event</a></li>.
            <li><a href="#section1000" id="2000" class="click" "="">Explore</a></li>.
            <li><a href="http://edukeeda.com/discussion-forum/">Discuss</a></li>
         </ul>
       </div>
      </div>
   </div>
   <div class="simplegrid  interviewsas">
      <div  class="insidecontainer">
         <div class="eventhead">
            <h1>Latest Events</h1>
            <a href="http://edukeeda.com/latest-event/">See all</a>
         </div>
         <div class="EVEN">
            <?php
               $action = "event";
               $eventDate = "event_date";
               $limits = 1;
               $results1 = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_posts INNER JOIN wp_postmeta ON wp_posts.ID = wp_postmeta.post_id WHERE wp_posts.post_type =%s AND wp_posts.post_status ='publish' AND wp_postmeta.meta_key =%s ORDER BY wp_postmeta.meta_value ASC", $action, $eventDate));

               if(!empty($results1))
               {
                 
                 foreach ($results1 as $result1) {
                   $cat = $wpdb->get_results( 
                     $wpdb->prepare( "SELECT * FROM wp_term_relationships WHERE object_id = %d", $result1->ID) );
                   $getCatName = $wpdb->get_results( 
                     $wpdb->prepare( "SELECT term_id, name FROM wp_terms WHERE term_id = %d", $cat[0]->term_taxonomy_id));
                  
                   $image = wp_get_attachment_image_src( get_post_thumbnail_id( $result1->ID ), 'single-post-thumbnail' );
                   
                   if($image){
                       $image = $image[0];
                   } else {
                       $mkey = "_thumbnail_id";
                     $checkimg = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_postmeta WHERE post_id = %d AND meta_key=%s", $result1->ID,$mkey) 
                     );
                     $getimg = $wpdb->get_results( 
                           $wpdb->prepare( "SELECT * FROM wp_posts WHERE ID = %d", $checkimg[0]->meta_value) 
                     );
                     $image = $getimg[0]->guid;  
                   }
                    $author_name = get_the_author_meta( 'display_name', $result1->post_author );
                   $countPostTitle = strlen($result1->post_title);
                   if($countPostTitle >= 70) {
                     $postTitle = substr($result1->post_title,0, 70);
                     $postTitle .= "...";
                   } else {
                     $postTitle = $result1->post_title;
                   }
                   $eventDate = get_field('event_date', $result1->ID);
                   $eventDateEnd = get_field('event_date_end', $result1->ID);
                   $currentDate = date("Y-m-d");
                   $eventDateEnd = date("Y-m-d", strtotime($eventDateEnd));
                   
                   $eDate = (int) strtotime($eventDateEnd);
                   $cDate = (int) strtotime($currentDate);

                   if($eDate >= $cDate) {

                    if($limits <= 4) {


       ?>
            <div class="col-md-3">
               <div class="alt">
                  <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">
                     <div class="altimg">
                        <img src="<?php echo $image; ?>"> 
                        <div class="datespn">
                           <time datetime="1543469400000" class="dateDisplay float--left margin--top margin--left" aria-hidden="true">
                           <span class="dateDisplay-day">
                           <span><?php echo date('d',strtotime($eventDate)); ?></span><br>
                           </span>
                           <span class="dateDisplay-month text--tiny">
                           <span><?php echo date('M',strtotime($eventDate)); ?></span>
                           </span>
                           </time>
                        </div>
                     </div>
                  </a>
                  <div class="caption"> 
                  </div>
                  <div class="spndat">
                     <span><a href="<?php echo site_url();?>/events/">Event</a> . <a href="<?php echo site_url();?>/events-list?id=<?php echo $getCatName[0]->term_id;?>">
                         <?php if(strlen($getCatName[0]->name)>32) { echo substr($getCatName[0]->name,0,32)."..."; } else { echo $getCatName[0]->name;} ?>
                      </a></span>
                     <a href="<?php echo esc_url( get_permalink($result1->ID) ); ?>">
                        <p><?php echo $postTitle; ?></p>
                     </a>
                  </div>
                  <div class="profilesect">
                     <div class="userinf">
                        <p>
                           <span>Post By : <?php if($author_name != "admin"){?><a href="<?php echo site_url();?>/profile/?action=<?php echo $result1->post_author;?>"> <?php echo $author_name; ?> </a><?php } else { echo $author_name; }?> </span>

                           <div class="dropdown customs1">      
                            <button class="dropbtn">
                               <i class="fa fa-share-alt" aria-hidden="true"></i> </span> 
                            </button>
                            <div class="dropdown-content">
                             <?php $urlshare = esc_url( get_permalink($value['ID']) );
                               $urlsharing = "[Sassy_Social_Share type='standard' url='".$urlshare."']";
                             ?>
                               <?php echo do_shortcode("$urlsharing"); ?>
                            </div>
                           </div>
                           

                        </p>
                     </div>
                  </div>
               </div>
            </div>
            <?php
      $limits++;
                    }
                 }
                 
               }
             } 
            ?>
         </div>
      </div>
   </div>
   <div class="simplegrid categorybga interviewsas">
      <div  class="insidecontainer">
         <div class="odd" id="section1000">
            <h2>Explore by Category</h2>
            <?php

               $userId = $_SESSION["login"]["id"];
                  $categories = get_categories( array(
                   'child_of'            => 0,
                   'parent'              => 0,
                   'current_category'    => 0,
                   'depth'               => 0,
                   'echo'                => 1,
                   'exclude'             => '',
                   'exclude_tree'        => '',
                   'feed'                => '',
                   'feed_image'          => '',
                   'feed_type'           => '',
                   'hide_empty'          => 0,
                   'hide_title_if_empty' => false,
                   'hierarchical'        => true,
                   'order'               => 'ASC',
                   'orderby'             => 'name',
                   'separator'           => '<br />',
                   'show_count'          => 0,
                   'show_option_all'     => '',
                   'show_option_none'    => __( 'No categories' ),
                   'style'               => 'list',
                   'taxonomy'            => 'events_categories',
                   'title_li'            => __( 'Categories' ),
                   'use_desc_for_title'  => 1,
               ) );
               
               if(!empty($categories))
               {
                foreach ($categories as $terms) {
               
                $term_id = $terms->term_id;
                $image   = category_image_src( array('term_id'=>$term_id) , false );
                $category_link = get_category_link( $term_id );
               
               ?>
            <div class="col-md-3">
               <a href="<?php echo site_url(); ?>/events-list/?id=<?php echo $term_id; ?>">
                  <div class="even1">
                     <img src="<?php echo $image; ?> ">
                     <p><?php echo $terms->name; ?></p>
                  </div>
               </a>
            </div>
            <?php 
               }
                 }
                ?>
         </div>
         <?php while ( have_posts() ) : the_post(); ?>
         <?php get_template_part( 'content', 'page' ); ?>
         <?php
            // If comments are open or we have at least one comment, load up the comment template
            if ( comments_open() || get_comments_number() ) :
            comments_template();
            endif;
            ?>
         <?php endwhile; // end of the loop. ?>
         </main><!-- #main -->
      </div>
   </div>
   <!-- #primary -->
</div>
</div>  
</div>  
<?php get_footer(); ?>
</div>
<style>
.dropdown.customs1 .dropbtn{    background-color: #fff; color: #000;}
.spndat span a{text-transform: uppercase;}
.userinf a {
    color: #a2a2a2;
}
.insidecontainer
{
      padding: 0px;

}


.page-wrap{
          padding-top: 68px;
   }
.userinf span {
    color: #a2a2a2;
   float: left;
}
.userinf p {
    float: left;
}
.userinf {
    padding: 0 0 0 0px;
    float: left;
    width: 100%;

}
.userinf i.fa.fa-share-alt {
    float: right;
}
   .spn i.fa.fa-share-alt {
   float: right;
   }
   .want a{
   border-radius: 10px;
   background:#f68e2f;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .want span {
    margin: 0px 5px;
    color: #f68e2f;
}
   .wantpost a {
   border-radius: 10px;
   background: #f68e2f;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .altimg img {
   border-radius: 7px 7px 0px 0px;
   max-width: 100%;
   }
   .EVEN {
   float: left;
   width: 100%;
   margin-bottom: 40px;
   }
   span.dateDisplay-day {
   color: #f68e2f;
   font-size: 20px;
   font-weight: 800;
   }
   span.dateDisplay-month.text--tiny {
   color: #020253;
   font-size: 20px;
   font-weight: 800;
   }
   .spn img {
   border-radius: 60%;
   width: 56px;
   height: 56px;
   }
   .spn a {
   color: black;
   }
   .spn a:hover {
   color: orange;
   }
   
   .odd {
   float: left;
   width: 100%;
   margin-bottom: 60px;
   }
   div#sidebar-footer {
   float: left;
   width: 100%;
   }
   .eventhead a {
   float: right;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 20px;
   }
   .eventnew .container2 {
   margin: 0 auto;
   max-width: 1170px;
   }
   .eventnew .content-wrapper.container {
   width: 100%;
   margin: 0 auto;
   padding: 0 !important;
   }
   .eventnew .content-wrapper.container > .row{
   margin:0;
   }
   .catchybga {
   padding: 14% 0;
   background: url(/img/events.png);
   background-size: cover;
   background-position: 100%;
   position: relative;
   }
   div#sidebar-footer {
   float: left;
   width: 100%;
   }
   .alt thumbnail{
   float: left;
   position: relative;
   }
   .alt h1 {
   font-size: 31px;
   }
   .belowlinksedus ul li a
   {
      font-size: 30px!important;
   }
   .datespn {
   position: absolute;
   top: 11px;
   left: 24px;
   background: #fff;
   padding: 7px;
   color: #060658;
   font-size: 24px;
   text-align: center;
   border: 1px solid lavenderblush;
   border-radius: 7px;
   line-height: 17px;
   }
   .thumbnail strong {
   width: 100px;
   }
   .thumbnail .caption {
   padding: 9px;
   color: #333;
   float: left;
   width: 100%;
   }
   .spndat span {
   font-size: 17px;
   color: #b2b2bd;;
   float: left;
   width: 100%;
   }
   .spndat {
   padding: 16px;
       float: left;
    width: 100%;
    min-height: 100px;
    height: 115px;
    overflow: hidden;
   }
   
   .spndat p:hover{
   color: #f68e2f;
   }
   .spn span {
   display: block;
   color: #c2b2bd;
   }
   .alt .carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive, .thumbnail a>img, .thumbnail>img{
   display: block;
   max-width: 100%;
   height: 150px;
   width: 333px;
   }
   .alt:hover{
   box-shadow: 3px 6px 2px 0 rgba(46,62,72,.12), 0 2px 4px 0 rgba(46,62,72,.12)
   }
  
   
   .even1{
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   margin-bottom: 35px;
   background-size: cover;
   text-align: center;
   color: white;
   }
   .even1 img {
   width: 100%;
   border-radius: 10px 10px 10px 10px;
   }

   .even1 p {
    color: black;
    font-size: 17px;
    font-weight: 600;
    line-height: 16px;
    min-height: 20px;
    height: 35px;
    margin-top: 12px;

   }
   .thumbnail{
   box-shadow:0 0 2px 0 rgba(46,62,72,.12), 0 2px 4px 0 rgba(46,62,72,.12);
   border-radius: 0px 0px 10px 10px;
   }
   .thumbnail:hover{
   box-shadow: 2px 4px 0px 0 rgba(0, 0, 0, 0.07);
   }
   .want {
        text-align: center;
    color: white;
    position: absolute;
    top:110px;
    width: 100%;
   }
   .want h3 {
       color: white;
    font-size: 22px;
    font-weight: 100;
    margin-bottom: 13px;
   }
   .belowlinksedus {
 position: absolute!important;
    right: 0;
    left: 0%!important;
    top: 36px;
}
.want p {
    font-size: 20px;
    margin-top: 10px;
    position: absolute;
    top: 204px;
    left: 22%;
}
   .want a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .wantpost {
   TEXT-ALIGN: CENTER;
   }
   .wantpost p {
   color: white;
   font-size: 20px;
   position: ABSOLUTE;
   left: 0;
   right: 0;
   margin: 10 AUTO;
   FLOAT: NONE;
   WIDTH: AUTO;
   BOTTOM: 0px;
   }
   .wantpost a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .want p {
   font-size: 22px;
   margin-top: 10px;
   }
   .expview strong {
   color: black;
   margin: 0px 11px;
   }
   .hentry {
   float: left;
   width: 100%;
   }
   @media only screen and (max-width: 767px)
   {
   .want h3 {
       font-size: 11px;
    font-weight: 100;
    padding: 5px;
   }
.want {
    text-align: center;
    color: white;
    position: absolute;
    top: 0px;
    width: 100%;
}
   .belowlinksedus ul li a {
    font-size: 12px!important;
font-weight:600;
}
.belowlinksedus {
    position: absolute!important;
    bottom: 0;
    right: 0;
    left: 5%!important;
    top: 51px!important;
}
.catchybga {
    padding: 20% 0;
}
.simplegrid
{
      padding: 0px;
}
   .wantpost p {
   color: white;
   font-size: 17px;
   position: ABSOLUTE; 
   margin: 0 AUTO;
   BOTTOM: 0px;
   font-weight: unset;
   }
   .want p {
   font-size: 11px;
    margin-top: 10px;
    position: absolute;
    top: 82px;
    left: 0%;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 20px;
   }
   .eventhead h1 {
   font-size: 31px;
   float: left;
   margin: 0 0 30px 10px;
   }
   .eventhead a {
   float: right;
   MARGIN: 0px 19px;
   }
   .alt{
   margin-bottom: 12px;
   position:relative;
   }
   .datespn {
    position: absolute;
    top: 8px;
    left: 8px;
   }
   .page-wrap {
    padding-top: 0px;
}
   }
</style>
<!-- Modal -->
<div id="myModalpost" class="modal fade" role="dialog">
   <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Posting</h4>
         </div>
         <div class="modal-body">
            <p> For Posting please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
         </div>
      </div>
   </div>
</div>
