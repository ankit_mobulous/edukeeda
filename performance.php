<div class="join">
<?php
session_start();
/**
/*
  Template Name:testperformance
*/
if($_SESSION["login"]) {

} else {
  header('Location: https://edukeeda.com/signin/');
}
get_header(); ?>
<?php
  $path = $_SERVER['DOCUMENT_ROOT'];
  include_once $path . '/wp-config.php';
  include_once $path . '/wp-load.php';
  include_once $path . '/wp-includes/wp-db.php';
  include_once $path . '/wp-includes/pluggable.php';
  
  global $wpdb;
  $userId = $_SESSION["login"]["id"];
  $eid = $_GET["action"];
  $getAnswer = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_user_premium_answer WHERE user_id = %d AND test_id=%d", $userId, $eid));
  $getPerformance = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_user_performance_premium WHERE user_id = %d AND test_id=%d", $userId, $eid));

  $getTestName = $wpdb->get_results( 
                $wpdb->prepare( "SELECT * FROM wp_premium_exam_type WHERE id = %d", $getAnswer[0]->test_id ));

?>
   <div class="col-md-12" style="margin-top: 5%;">
      <div class="stu">
         <div class="col-md-12">
            <div class="cont">
               <h2 class="text-center"> Test Name <?php echo $getTestName[0]->exam_title; ?></h2>
            </div>
         </div>
      </div>

      <div class="stud">
         <div class="col-md-12">
            <div class="cont">
               <table class="table">
                  <thead>
                     <tr>
                        <th>S.No.</th>
                        <th> Test Name </th>
                        <th> Attempt Date </th>
                        <th> No. of student attempted so far </th>
                        <th> Total Marks </th>
                        <th> Marks Obtained </th>
                        <th> Percentile </th>
                        <th> Check your performance </th>
                        <th> Download Solution </th>
                     </tr>
                  </thead>
                  <tbody>
                  <?php
                      $getPerformance1 = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_user_performance_premium WHERE test_id=%d AND user_id=%d", $eid,$userId));
                      $i = 1;
                      foreach($getPerformance1 as $getPerformance11) {
                        $getSubId = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_user_premium_start WHERE exam_id = %d AND quiz_id=%d AND user_id=%d", $getPerformance11->test_id, $getPerformance11->quiz_id, $getPerformance11->user_id));

                        $getSubName = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_premium_exam_subject WHERE id = %d", $getSubId[0]->subject_id));

                        $getUserCount = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT COUNT(id) as userCount FROM wp_user_premium_start WHERE exam_id = %d AND subject_id=%d AND quiz_id=%d", $getPerformance11->test_id, $getSubId[0]->subject_id, $getPerformance11->quiz_id));

                        $usersMarks = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT COUNT(id) as userCount FROM wp_user_performance_premium WHERE user_id =%d AND test_id = %d AND quiz_id=%d AND correctAnswer >= %d", $userId, $getPerformance11->test_id, $getPerformance11->quiz_id, $getPerformance11->correctAnswer));


                        $getQuizFile = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT sheet_file,points,minus_point FROM wp_premium_quiz WHERE id=%d", $getPerformance11->quiz_id));
                  ?>
                     <tr>
                        <td><?php echo $i;?></td>
                        <td> <?php echo $getTestName[0]->exam_title; ?> - <?php if($getSubName){echo $getSubName[0]->subject;}?> </td>
                        <td><?php echo date("d-m-Y",strtotime($getPerformance11->created_at));?></td>
                        <td> <?php echo $getUserCount[0]->userCount; ?> </td>
                        <?php
                            $totalmarksques = $getPerformance11->totalQues * $getQuizFile[0]->points;
                        ?>
                        <td><?php echo $totalmarksques; ?></td>
                        
                        <?php
                          $attemptQues1 = $getPerformance11->attemptQues;
                          $correctAnswer = $getPerformance11->correctAnswer;
                          $incorr = $attemptQues1 - $correctAnswer;
                          $incorrPoint = $incorr * $getQuizFile[0]->minus_point;
                          $corrPoint = $getPerformance11->correctAnswer * $getQuizFile[0]->points;
                          $finalPoint =  $corrPoint - $incorrPoint;
                        ?>

                        <td><?php echo $finalPoint;?></td>
                        <td>
                          <?php
                              $numOfPeopleattempt = $getUserCount[0]->userCount;
                              $totalQ = $totalmarksques;
                              $coreectQ = $finalPoint;
                              $moreMarks =  $usersMarks[0]->userCount;

                              $percent = 100 - ($moreMarks / $numOfPeopleattempt * 100);
                              echo $percent;
                          ?>
                        </td>
                        <td> <a href="<?php site_url();?>/test-answer?action=<?php echo $getPerformance11->test_id;?>&action1=<?php echo $getSubId[0]->subject_id; ?>&action2=<?php echo $getPerformance11->quiz_id; ?>">Click Here</a> </td>
                        <td> <a href="<?php echo $getQuizFile[0]->sheet_file; ?>" target="_blank">Click Here</a> </td>
                     </tr>
                  <?php
                      $i++;
                    }
                  ?>
                     
                  </tbody>
               </table>
            </div>
         </div>
      </div>
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
</div>   
<?php get_footer(); ?>
</div>
<style>
.logged-in.admin-bar.no-customize-support #mainnav li {
    padding: 0px 14px;
}
.join a {
    background: none;
    border: none;
	padding:0px;
   }
   .join a:hover {
    border: none;
}
.table a {
    color: #f68e2f;
}

   .want a {
   border-radius: 10px;
   background: #f68e2f;
   color: #fff;
   padding: 6px 18px;
   font-size: 13px;
   text-align: center;
   margin: 13px 0 0 0;
   text-transform: capitalize;
   }
   .cont p {
   font-size: 18px;
   font-weight: 600;
   margin: 5px 0px;
   color: #060658;
   text-transform: uppercase;
   }
   .cont thead {
       background: #52c641;
    color: #fff;
   }
   .cont {
   float: left;
   width: 100%;
   padding: 10px;
   
   border: 0px solid #f68e2f;
   border-radius: 10px;
   margin: 10px;
   text-align: center;
   }
   .join .page-wrap .container {
   width: 100%;
   padding: 0;
   margin: 0 auto;
   }
   .join .page-wrap .container .row {
   margin: 0;
   }
   .catchybga {
   padding: 2% 0;
   background-position: 100%;
   position: relative;
   margin-bottom: 45px;
   background: #4054b2;
   }
   .want {
   text-align: center;
   color: white;
   }
   .want h3 {
   color: white;
   font-size: 36px;
   font-weight: 600;
   margin-bottom: 0px;
   }
   .want a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .stu {
   float: left;
   width: 100%;
   margin-bottom: 50px;
   }
   .cont:hover {
    box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0.07);
}
   @media only screen and (max-width: 767px)
   {
   .want h3 {
   color: white;
   font-size: 23px;
   font-weight: 100;
   }
   .want p {
   font-size: 17px;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 20px;
   }
   .cont .table {
    width: 1200px;
    max-width: 100%;
    margin-bottom: 20px;
    }
   .cont .table>thead>tr>th {
    width: 100px;
    padding: 0 !important;
    line-height: 17px;
    vertical-align: middle;
    font-size: 13px;
   } 
    .cont {
    float: left;
    width: 100%;
    max-width: 1000px;
    border: 0px solid #f68e2f;
    border-radius: 10px;
    margin: 0px;
    text-align: center;
    overflow: scroll;
}
   }
</style>

