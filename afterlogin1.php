
<div class="afterlogin1">
<?php
/**
/*
 Template Name: Home Page afterlogin1
 */

get_header(); ?>
<?php
   session_start();

   if(!isset($_SESSION["login"])){
      $url = "https://edukeeda.com/signin/";
      wp_redirect( $url );
      exit;
   }
?>
<div class="login-form">

    <div class="panel">
           <h2>Welcome to Edukeeda family</h2>
   <form id="loginn">

        <a class="gohomeprofi" href="<?php site_url();?>/edit-profile/">Complete/Edit your profile</a>
        </br><p></p>
        <?php $userId = $_SESSION["login"]["id"];?>
           <a href="<?php site_url();?>/all-members/homepage?action=<?php echo $userId; ?>" class="gohome">Go to your home page</a>

    </form>
   </div>
    
    

</div></div>






      <?php while ( have_posts() ) : the_post(); ?>

        <?php get_template_part( 'content', 'page' ); ?>

        <?php
          // If comments are open or we have at least one comment, load up the comment template
          if ( comments_open() || get_comments_number() ) :
            comments_template();
          endif;
        ?>

      <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

  

<?php get_footer(); ?>
</div>
<style>
#loginn p {
    margin-top: 5px;
    margin-bottom: 5px;
    font-size: 20px;
    font-weight: 700;
}
.login-form {
      background: #efefef;
    float: left;
    width: 100%;
    height: 500px;
    display: flex;
    flex-direction: column;
    flex: 1;
}
.panel {
       margin-bottom: 20px;
    background-color: #efefef;
    border-bottom: 1px solid #f68e2f;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
    border-radius: 0px;
    margin: auto;
}
.panel h2 {
    font-size: 24px;
} 
form#loginn {
    margin-top: 44px;
}
#loginn a{
           background: transparent;
    color: #02027e;
    padding: 9px 18px;
    font-size: 23px;
    padding: 9px 18px;
 
    display: inline;
    text-align: center;
    margin: 29px 59px;
    border-radius: 10px 10px 10px 10px;
}

.main-div {
  background: #ffffff none repeat scroll 0 0;
  border-radius: 2px;
  margin: 10px auto 30px;
  max-width: 38%;
  padding: 50px 70px 70px 71px;
}

.login-form .form-group {
  margin-bottom:10px;
}
.login-form{ text-align:center;}
.forgot a {
  color: #777777;
  font-size: 14px;
  text-decoration: underline;
}
.login-form  .btn.btn-primary {
  background: #f0ad4e none repeat scroll 0 0;
  border-color: #f0ad4e;
  color: #ffffff; 
  font-size: 14px;
  width: 100%;
  height: 50px;
  line-height: 50px;
  padding: 0;
}

.panel h2 {
    font-size: 39px;    margin: 0;
}
.login-form, .panel{background: transparent; box-shadow: none; border:none;}
form#loginn {
    margin-top: 0;
      border-bottom: none;
}

a.gohomeprofi {
    font-size: 20px !important;
    color: #f68e2f !important;
}

a.gohome {
    margin: 147px 0 0 0 !important;
    display: inline-block;
    text-align: center;
    width: 100%;
    float: left;
    padding: 0;
    border-top: 1px solid #f68e2f;
    border-radius: 0px !important;
    color: #74bd68 !important;
   
}
 @media only screen and (max-width: 767px)
   {
.panel h2 {
    font-size: 25px;
    margin: 0;
}
a.gohomeprofi {
    font-size: 20px !important;
    color: #f68e2f !important;
    margin: 0px!important;
    padding: 0px!important;
}
a.gohome {
    margin: 7px 0 0 0 !important;
}
.login-form {
    height: 213px;
}
}
</style>
