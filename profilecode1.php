<?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;

    if(isset($_POST['submitsteptwo'])) {

        $name = trim($_POST["name"]);
        $gender = trim($_POST["gender"]);
        $dob = trim($_POST["dob"]);
        $email = trim($_POST["email"]);
        $mobile = trim($_POST["mobile"]);
        $country = $_POST["country"];
        $state = $_POST["state"];
        $city = trim($_POST["city"]);
        $zip = trim($_POST["zip"]);
        $userId = $_SESSION["login"]["id"];
        $page = trim($_POST["lmember"]);
        
        $tablename = "wp_profileStepTwo";
        $getStepTwo = $wpdb->get_results( 
            $wpdb->prepare( "SELECT * FROM wp_profileStepTwo WHERE user_id = %d",$userId) 
        );
        if($getStepTwo) {

          if($page == "homemember") {
              $wpdb->update($tablename, array('country'=>$country), array('user_id'=>$userId));
              $wpdb->update($tablename, array('state'=>$state), array('user_id'=>$userId));
              $wpdb->update($tablename, array('city'=>$city), array('user_id'=>$userId));
              $wpdb->update($tablename, array('zipcode'=>$zip), array('user_id'=>$userId));
          
          } else if($page == "homemembername") { 
              $wpdb->update($tablename, array('name'=>$name), array('user_id'=>$userId));
          }else {
              $wpdb->update($tablename, array('name'=>$name), array('user_id'=>$userId));
              $wpdb->update($tablename, array('gender'=>$gender), array('user_id'=>$userId));
              $wpdb->update($tablename, array('dateofbirth'=>$dob), array('user_id'=>$userId));
              $wpdb->update($tablename, array('mobile'=>$mobile), array('user_id'=>$userId));
              $wpdb->update($tablename, array('country'=>$country), array('user_id'=>$userId));
              $wpdb->update($tablename, array('state'=>$state), array('user_id'=>$userId));
              $wpdb->update($tablename, array('city'=>$city), array('user_id'=>$userId));
              $wpdb->update($tablename, array('zipcode'=>$zip), array('user_id'=>$userId));
          }

        } else {
          if(empty($state)){
            $state = " ";
          }
          if(empty($zip)){
            $zip = " ";
          }

          if($page == "homemember") {
              $data = array(
                "user_id" => $userId,
                "country" => $country,
                "state" => $state,
                "city" => $city,
                "zipcode" => $zip
              );
          } else if($page == "homemembername") {
              $data = array(
                "user_id" => $userId,
                "name" => $name
              );
          } else {
              $data = array(
                "user_id" => $userId,
                "name" => $name,
                "gender" => $gender,
                "dateofbirth" => $dob,
                "mobile" => $mobile,
                "country" => $country,
                "state" => $state,
                "city" => $city,
                "zipcode" => $zip
              );
          }
          $result = $wpdb->insert($tablename, $data);
        }

        if($page == "homemember") {
           header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
        } else if($page == "homemembername") {
            header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
        } else {
            header('Location: https://edukeeda.com/edit-profile/');
        }
    } else{
        $wrongCred = "Profile updation failed";
        $_SESSION["failed"] = $wrongCred;
         if($page == "homemember") {
             header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
          } else if($page == "lmembername") {
            header("Location: https://edukeeda.com/all-members/homepage/?action=$userId");
          } else {
              header('Location: https://edukeeda.com/edit-profile/');
          }
    }
?>
