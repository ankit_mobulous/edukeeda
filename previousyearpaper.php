<div class="ajeetbaba bluheadpack">
<?php
/**
/*
 Template Name: Previous Year Papers
*/
get_header(); ?>
<style>
	.examlistcolm .elementor-button span{
		    margin: 0px -8px;
	}
  .dynamicprofile {
  float: left;
  margin: 0 auto;
  width: 100%; 
  padding: 10px 27px;
  background: #ffffff;
  color: #5c6b77;
  box-shadow: 0px 0px 4px 0px;
  margin-bottom: 60px;
  }
  .dynamicprofile img{
  width: 35px;
  height: 34px;
  float: left;
  margin-right: 10px;
  }
  .dynamicprofile p.username.info {
  font-size: 18px;
  margin: 0;
  color: #1553de;
  }
  .header-clone {
  display: none;
  }
  .settingsopn {
  float: right;
  }
  .settingsopn ul {list-style-type:none;    padding: 0; margin: 0;}
  .settingsopn ul li{display:inline-block;    margin-right: 30px;}
  .settingsopn ul li:last-child{ margin-right: 0px;}
  .usernamesopn{float:left;     width: 50%;}
  .studentprofilesmt {
  float: left;
  width: 100%;
  }
  .studentprofilesmt .elementor-widget-tabs .elementor-tab-title.elementor-active {
  color: #ffffff;
  background: #555ba2;
  border-radius: 1px 33px 1px 0px;
  box-shadow: none;
  border: 0px solid #fff;
  }
  .studentprofilesmt .elementor-widget-tabs .elementor-tab-title{
  color: #a3a9ab;
  }
  .studentprofilesmt .elementor-tabs {
  box-shadow: #bdbdbd 0px 0px 20px 0px;
  }
  .studentprofilesmt .elementor-column-gap-default>.elementor-row>.elementor-column>.elementor-element-populated {
  padding: 0 12px;
  }
  .advanceftr {
  float: left;
  }
  .advanceftr h5 {
  font-size: 20px;
  padding: 0 23px;
  }
  .mainhead {
    padding: 2% 0;
    background-position: 100%;
    position: relative;
    margin-bottom: 46px;
    background: #4054b2;
    margin: 6% 0%;
    text-align: center;

  }
  .mainhead h6 {
  color: #ffffff;
  font-size: 30px;
  font-family: "Roboto", Sans-serif;
  font-weight: 600;
      margin-top: 20px;
  }
  .sidebar.sidebarBox {
  box-shadow: 0px 0px 4px 0px;
  width: 100%;
  float: right;
  padding: 20px;
      margin-bottom: 20px;
  }
  .sortFilter {
  border-bottom: 1px solid #e5e5e5;
  margin: -15px -15px 0;
  padding: 10px 14px;
  font-size: 16px;
  color: #333;
  font-weight: 600;
  }
  a.clAll.clearAll {
  font-size: 14px;
  font-weight: 400;
  color: #3a4d95;
  float: right;
  }
  input[type="checkbox"], input[type="radio"] {
  margin: 0 10px 0 0px;
  }
  .filterBox h3 {
  font-size: 14px;
  color: #333;
  margin: 12px 0 11px;
  font-weight: 600;
  text-transform: uppercase;
  }
  .filterBox label {
  text-transform: uppercase;
  font-weight: 100;
  color: #000;
  }
  a.elementor-button-link.elementor-button.elementor-size-sm{
  font-weight: 400;
  font-style: normal;
  color: #000;
  background-color: #ffffff;
  display: block;
  width: 100%;
  box-shadow: 0px 0px 2px 0px;
  }
  .elementor-button-wrapper {
  width: 100%;
  }
  a.elementor-button-link.elementor-button.elementor-size-sm:hover{
  color: #ffffff;
  background-color: #23282d;
  }
  .previouspapas a {
  font-size: 21px;
  color: #060658;
  }
  .previouspapas a:hover{
  color: #f68e2f;
  }
  .examlistcolm{
  margin: 0 0 30px 16px;
  }
  #mega-menu-wrap-primary #mega-menu-primary {
    visibility: visible;
    text-align: left;
    padding: 16px 0px 0px 0px;
}
.catchybga {
    padding: 2% 0;
    background-position: 100%;
    position: relative;
    margin-bottom: 46px;
    margin: 7% 0%;
}
.want h3 {
      color: #060658;
      font-size: 28px;
      font-weight: 600;
      margin-bottom: 0px;
          
      }
	  .want {
    width: 100%;
    float: left;
    text-align: center;
}
  @media only screen and (max-width: 767px) {
       .mainhead {
     margin:161px 0px;
    text-align: center;
    background: #4054b2;
    padding: 0px;
    color: #fff;

    width: 100%;
  }
  .want h3 {
      color: white;
      font-size: 23px;
      font-weight: 100;
          width: 100%;
    float: left;
    padding: 0px 0px;
    margin: -19px 0px;
      }
  .elementor-image img {
    width: 100%;
}
.examlistcolm {
    margin: 10px 0 30px 0;
}
     .catchybga {
    padding: 12% 0;
    background-position: 100%;
    position: relative;
    margin-bottom: 46px;
    background: #4054b2;
    margin: 0% 0%;
}  
      .catchybga{
		  margin-top:80px;
  }
      
</style>
<script type="text/javascript">
  function mySubmit(theForm) {
      $.ajax({ 
          data: $(theForm).serialize(), // get the form data
          type: $(theForm).attr('method'), // GET or POST
          url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_exam_list4.php",
          success: function (response) { // on success..
               var data_array = $.parseJSON(response);
              $('.uploadExams').html(data_array); // update the DIV
          },
          error: function(jqXHR, textStatus, errorThrown){
              console.log(textStatus, errorThrown);
          }
    
      });
  }
</script>
   <div class="studentprofilesmt">
      <div class="row">
          <div class="catchybga">
      <div class="want">
         <h3>Download Previous Years Paper</h3>
      </div>
   </div>
         
         <div id="primary" class="content-area col-md-8 uploadExams">
          <?php
              if($_GET["action"]) {
                $action = $_GET["action"];
                $getExamDetails = $wpdb->get_results( 
                    $wpdb->prepare( "SELECT * FROM wp_exam_uploads WHERE exam_id = %d GROUP BY exam_title", $action)
                );
              } else {
                  $getExamDetails = $wpdb->get_results("SELECT * FROM wp_exam_uploads GROUP BY exam_title");
              }
                foreach($getExamDetails as $getExamDetail) {
          ?>
            <section class="examlistcolm">
               <div class="row">
                  <div  class="col-md-2">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-image">
                           <img width="45" height="45" src="<?php echo $getExamDetail->exam_img; ?>" class="attachment-full size-full" alt="">
                        </div>
                     </div>
                  </div>
                  <div  class="col-md-8">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-text-editor elementor-clearfix previouspapas">
                           <p><a href="#"><?php echo $getExamDetail->exam_title; ?></a>
                              <br><strong>Purpose: </strong> <?php echo $getExamDetail->purpose; ?>
                              <br><strong>Eligibility:</strong> <?php echo $getExamDetail->eligibility; ?>
                              <br><strong>Exam Date: </strong> <?php echo date("d/m/Y", strtotime($getExamDetail->exam_date)); ?>
                           </p>
                        </div>
                     </div>
                  </div>
                  <div  class="col-md-2">
                     <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-button-wrapper">
                           <a href="http://edukeeda.com/previous-year-papers-listing?action=<?php echo $getExamDetail->exam_title; ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                           <span class="elementor-button-content-wrapper">
                              <span class="elementor-button-text">Download</span>
                           </span>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <?php
              }
            ?>
            
            <main id="main" class="post-wrap" role="main">
               <?php while ( have_posts() ) : the_post();
                  global $post;
                  
                        ?>
               <?php
                  if($post->ID=="921")
                  {
                      get_template_part( 'content', 'student' );
                  }
                  else{
                        get_template_part( 'content', 'page' );
                  }
                  ?>
               <?php
                  // If comments are open or we have at least one comment, load up the comment template
                  if ( comments_open() || get_comments_number() ) :
                    comments_template();
                  endif;
                  ?>
               <?php endwhile; // end of the loop. ?>
            </main>
            <!-- #main -->
         </div>
         <!-- #primary -->
         <div class="col-md-4">
            <div class="sidebar sidebarBox">
               <div class="sortFilter">Sort &amp; Filter <a class="clAll clearAll" href="<?php echo site_url();?>/previous-year-papers/">Clear All</a></div>
               <form class="sortFilterForm" method="post">
                  <div class="filterBox examSort">
                     <h3>Sort By</h3>
                     <div class="box">
                        <input checked="" id="popularity" name="sort_filter" type="radio" value="pop">
                        <label for="popularity">Popularity</label>
                     </div>
                     <div class="box">
                        <input id="alphabetically" name="sort_filter" type="radio" value="alpha">
                        <label for="alphabetically">Alphabetically</label>
                     </div>
                  </div>
                  <div class="filterBox examLabel">
                     <h3>Exam Level</h3>
                     <div class="slecopt">
                        <form action="" method="post" name="theForm">
                            <?php $action1 = $_GET["action"]; ?>
                             <select name="selectc" id="select2" class="form-control" onchange="mySubmit(this.form)">
                                  <option value=" "> Select </option>
                                  <option value="job-related"> Job Related </option>
                                  <option value="higher-studies"> Higher Studies </option>
                             </select>
                            <input type="hidden" name="eid" value="<?php echo $action1;?>">
                         </form>
                     </div>
                  </div>
                  <input name="csrfmiddlewaretoken" type="hidden" value="JLIYsrNZXI6q8JJOPkDyJBEpEPJ4k7ZaPks7QcXl1I27uviYpcgOb5RcJLpcijT9">
               </form>
            </div>
         </div>
      </div>
      <?php get_footer(); ?>
   </div>
</div>

