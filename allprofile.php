<div class="allauthors">
<?php
session_start();
  /**
  /*
   Template Name:  all profile
   */
  
get_header(); ?>
<?php
    if(isset($_POST["followuser"])) {
        $memberid = $_POST["memberid"];
        $userid = $_SESSION["login"]["id"];
        global $wpdb;
        $data = ["follow_id"=>$memberid, "user_id"=>$userid];
        $wpdb->insert("wp_member_follow", $data);
    }
?>
   <div class="col-md-9 col-sm-9">
      <div class="col-md-12">
         <h4>Explore The World</h4>
      </div>
      <div class="col-md-8 col-sm-8">
         <div class="filterkeeda">
            <div class="leftoption">
               <form class="searchform" action=" " method="post" class="search-form">
                 <input type="text" placeholder="Search.." name="search" class="searchtext">
                 <button type="submit"><i class="fa fa-search"></i></button>
               </form>
            </div>
         </div>
      </div>
      
      <div class="col-md-4 col-sm-4">
         <div class="rightoptions1">
            <p>Sort By:
            <select name="top_filter" id="topsuccessfilter" onchange='this.form.submit()'>
              <option value=""> Select </option>
               <option value="popular">Most Viewed</option>
               <option value="active">Most Active</option>
               <option value="new">New Member</option>
               <option value="old">Old Member</option>
               <option value="name">Name</option>
            </select></p>
         </div>
      </div>
   </div>
   <div class="col-md-9">
     
       <div class="col-md-6">
           <div class="allprofiles">
                <div class="rap">
                   <div class="imgforr">
                       <div class="imgg">
                           <a href="#"><img src="/wp-content/uploads/2019/01/financial.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
       
                      </div>
                  </div>
	                 <div class="right"> 
	                    	 <p class="catp"><strong><a href="#">Satya Prabhu</a></strong>,<span>Profile Credential</span></p>
		                    <p class="rigwd"><strong> Noida/ India</strong></p>
                    </div>
	    
                    <div class="share1">
                        <button>  <img src="/img/Follow.png"> <span> 5.3k </span></button>
	                </div>
	            </div>
		    </div>
		</div>
	   <div class="col-md-6">
	       <div class="allprofiles">
	            <div class="rap1">
                   <div class="imgforr">
                         <div class="imgg">
                             <a href="#"><img src="/wp-content/uploads/2019/01/financial.jpg" class="img-thumbnail" alt="Cinque Terre" width="300" height="300"></a>
       
                        </div>
                  </div>
	              <div class="right"> 
		                   <p class="catp"><strong><a href="#">Satya Prabhu</a></strong>,<span>Profile Credential</span></p>
		                        <p class="rigwd"><strong> Noida/ India</strong></p>
                  </div>
	    
                  <div class="share1">
                       <button>  <img src="/img/Follow.png"> <span> 5.3k </span></button>
	              </div>
	        </div>
	   </div>
	</div>
	</div>
   
   <div class="col-md-3 customsideedu">
      <div id="secondary" class="widget-area col-md-3" role="complementary">
         
         <aside id="recent-posts-3" class="widget widget_recent_entries">
            <h4 class="widget-title">Name Start With</h4>
            <div class="spacetop sortbyalphabet">
               <ul>
                  <li><input type="checkbox" name="vehicle1" value="Education Level"> A</li>
                  <li><input type="checkbox" name="vehicle1" value="Education Level"> B</li>
                  <li><input type="checkbox" name="vehicle1" value="Education Level"> C</li>
                  <li><input type="checkbox" name="vehicle1" value="Education Level"> D</li>
                  <li><input type="checkbox" name="vehicle1" value="Education Level"> E</li>
                  <li><input type="checkbox" name="vehicle1" value="Education Level"> F</li>
                  <li><input type="checkbox" name="vehicle1" value="Education Level"> G</li>
                  <li><input type="checkbox" name="vehicle1" value="Education Level"> H</li>
               </ul>
               <input type="button" class="applyfilt" value="apply">
            </div>
         </aside>
      </div>
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
          comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Follow</h4>
      </div>
      <div class="modal-body">
        <p> For Follow please <a href="<?php echo site_url(); ?>/signin/">login or register</a> </p>
      </div>
    </div>
  </div>
</div>

<style>
.share1 button {
    padding: 0px 20px;
	    padding: 0px 20px;
    background: transparent;
    color: #060658;
    font-weight: 500;
    border: none;
}
.imgg {
    float: left;
    margin: 0px 7px;
}
.filterkeeda {
    float: left;
    width: 100%;
}
.allprofiles {
    float: left;
    box-shadow: -1px 4px 4px 0 rgba(0, 0, 0, 0.07);
    padding: 8px;
    margin-bottom: 20px;
    border: 1px solid #ddd;
    width: 100%;
}
p.rigwd {
    margin-bottom: 0px;
    font-size: 14px;

}





	
.imgforr {
    text-align: left;
}
.right {
    float: left;
}
.share1 {

    float: right;
    margin: 20px 15px;
}
.rap {
    
    float: left;
    width: 100%;
}
.imgforr img.img-thumbnail {
    width: 60px;
    height: 60!important;
    border-radius: 100%;
}
.catp strong {
    color: #060658;
    font-weight: 600;
}
.share1 img {
    width: 15px;
    height: auto;
}
   .rightoptions p {
   float: left;
   }
   .rightoptions1 p {
   float: left;
   }
   .leftoption input[type="Text"] {
   width: 50%;
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   padding: 0 16px;
   }
   .rightoptions select {
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   line-height: 3px;
   padding: 1px 18px;
   margin: 0 0px 0 27px;
   }
   button, input[type="button"], input[type="reset"], input[type="submit"] {
    position: relative;
    display: inline-block;
    font-family: "Raleway", sans-serif;
    font-size: 15px;
    line-height: 27px;
    font-weight: 700;
    padding: 6px 36px;
    color: #fff;
    text-transform: uppercase;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    -o-border-radius: 3px;
    border-radius: 3px;
    -webkit-transition: all 0.3s;
    -moz-transition: all 0.3s;
    -ms-transition: all 0.3s;
    -o-transition: all 0.3s;
    transition: all 0.3s;
}
   .rightoptions1 select {
   background: #fff;
   border: 1px solid #ddd;
   height: 35px;
   line-height: 3px;
   padding: 1px 18px;
   margin: 0 0px 0 27px;
   }
   .alau img.img-thumbnail {
   max-width: 243px;
   max-height: 156px;
   border-radius: 10%;
   }
   .spacetop {
   margin: 12px 0 0 0;
   
   padding: 20px 0; 
   }
   .customsideedu  .spacetop h4{    margin-bottom: 9px;}
   .alau:hover{
   background: #f9f9f9;
   }
   .alau:hover p{
   color:#f68e2f;
   }
   .alau:hover p.hedlineauedu{ color: #484848;}
   .customsideedu h4.widget-title {
   padding: 0;
   font-size: 13px;
   font-weight: 900;
   }
   div#secondary {
   
   float: left;
   width: 100%;
    margin-top: -50px!important;

   }
   .all p{
   margin-bottom: 0px;
   }
   .alla p{
   margin-bottom: 0px;
   font-size: 19px;
   font-weight: 600;
   }
   .alau {
   float: left;
   box-shadow: -1px 4px 4px 0 rgba(0, 0, 0, 0.07);
   padding: 8px;
   margin-bottom: 20px;
   border: 1px solid #ddd;
   text-align: center;
   width: 100%;
   }
   .more a{
   background: #439a37;
   color: #fff;
   padding: 6px 18px;
   font-size: 14px;
   display: inline;
   text-align: center;
   margin: 29px 0;
   }
   .more {
   text-align: center;
   }
   .alla span {
   color: #92797d;
   font-size: 13px;
   }
   .alau .col-md-3{
   padding:0;
   }
   p.hedlineauedu {
   font-size: 14px;
   color: #484848;
   margin-bottom: 5px;
   }
   .follow .followbutton {
   background: #439a37;
   color: #fff;
   padding: 5px 43px;
   font-size: 14px;
   display: inline-block;
   text-align: center;
   }
   .alla p{ color: #020253;}
   
   .allauthors .container2{
   max-width:1170px;
   margin:0 auto;
   }
    .allauthors .container{
   width:100%;
   }
   .blo h2{
   font-size: 28px;
   }
   .rightoptions1 {
   text-align: right;
   width: 88%;
   float: right;
   }
   .sortbyalphabet ul li {
   display: inline-block;
   margin: 0 19px 0 0;
   }
   input.applyfilt {
   padding: 3px 15px;
   float: right;
   }
   .widget-area .widget-title{    font-size: 22px;}
   .customsideedu h4.widget-title{    font-size: 16px;}
   div#secondary a{    color: #6f605b;}
   p.hedlineauedu strong{color: #777171;}
   
   
   
   @media only screen and (max-width: 767px)
   {
   .div#secondary {
   width: 100%;
   }
   .alau img.img-thumbnail {
   max-width: 100%;
   }
   .rightoptions1 select {
   background: #fff;
   border: 1px solid #ddd;
   height: 25px;
   line-height: 3px;
   padding: 1px 18px;
   float: right;
   margin-bottom: 20px;
   width: 100%;
   }
   .rightoptions1 p {
   margin-bottom: 5px;
   margin-top: 5px;
   }
   .rightoptions1 {
   text-align: right;
   width: 100%;
   float: right;
   }
   }
</style>

