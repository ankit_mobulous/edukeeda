<div class="join">
<?php
session_start();
/**
/*
Template Name: Premium Exam Start
*/
if($_SESSION["login"]) {

} else {
  header('Location: https://edukeeda.com/signin/');
}
get_header(); ?>
   <?php
      $path = $_SERVER['DOCUMENT_ROOT'];
      include_once $path . '/wp-config.php';
      include_once $path . '/wp-load.php';
      include_once $path . '/wp-includes/wp-db.php';
      include_once $path . '/wp-includes/pluggable.php';
      
      global $wpdb;
      ?>
   <div class="catchybga">
      <div class="want">
        <?php
          global $wpdb;
          $eid = $_GET["action"];
          $getexam = $wpdb->get_results( 
              $wpdb->prepare( "SELECT * FROM wp_premium_exam_type WHERE id = %d", $eid) 
          );
        ?>
         <h3> <?php echo $getexam[0]->exam_title; ?> </h3>
      </div>
   </div>
   <div class="col-md-12">
      <div class="stu">
         <div class="ardo">
            <div class="col-md-10 col-md-offset-1">
               <div class="cont">
                <?php
                  $user_id = $_SESSION["login"]["id"];
                  $sid = $_GET["action1"];
                  $qid = $_GET["action2"];
                  
                  $getQuizDetails = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_premium_quiz WHERE id = %d AND exam_id = %d AND subject_id=%d",$qid, $eid, $sid) 
                  );

                  $getSubject = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_premium_exam_subject WHERE id = %d", $sid) 
                  );
                  $newTime = getexamtimeFormat1($getQuizDetails[0]->time_limit);
                  
                ?>

                  <h3><?php echo $getexam[0]->exam_title; ?> - <?php echo $getSubject[0]->subject; ?></h3>
                  <p>Duration : <?php echo $newTime; ?></p>
                  <p><b>Instructions : </b></p>
                  <p><?php echo $getQuizDetails[0]->instruction; ?></p>
                  <p>
                    <a href="<?php site_url();?>/start-test?action=<?php echo $eid;?>&action1=<?php echo $sid; ?>&action2=<?php echo $getQuizDetails[0]->id; ?>">
                      <span style="float:right;font-size: 20px;" class="btn btn-warning"><b>Start Test</b></span>
                    </a>
                  </p>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
</div>   
<?php get_footer(); ?>
</div>
<?php
    function getexamtimeFormat1($timeExam){
        $tValue = (int) $timeExam;          
        $dateFormat = "d F Y -- g:i a";
        $targetDate = time() + ($tValue*60);//Change the 25 to however many minutes you want to countdown
        $actualDate = time();
        $secondsDiff = $targetDate - $actualDate;
        $remainingDay  = floor($secondsDiff/60/60/24);
        $remainingHour  = floor(($secondsDiff-($remainingDay*60*60*24))/60/60);
        $remainingMinutes = floor(($secondsDiff-($remainingDay*60*60*24)-($remainingHour*60*60))/60);
        $remainingSeconds = floor(($secondsDiff-($remainingDay*60*60*24)-($remainingHour*60*60))-($remainingMinutes*60));
        $actualDateDisplay = date($dateFormat,$actualDate);
        $targetDateDisplay = date($dateFormat,$targetDate);

        $newtime = $remainingHour .":". $remainingMinutes;
        return $newtime;
    }
?>
<style>
   .want a {
     border-radius: 10px;
     background: #f68e2f;
     color: #fff;
     padding: 6px 18px;
     font-size: 13px;
     text-align: center;
     margin: 13px 0 0 0;
     text-transform: capitalize;
   }
   
   .cont p {
     font-size: 18px;
     font-weight: 600;
     margin: 5px 0px;
     color: #060658;
   }
   .cont {
     float: left;
     width: 100%;
     padding: 10px;
     box-shadow: 5px 6px 4px 0 rgba(0, 0, 0, 0.07);
     border: 1px solid #f68e2f;
     border-radius: 10px;
   }
   .join .page-wrap .container {
     width: 100%;
     padding: 0;
     margin: 0 auto;
   }
   .join .page-wrap .container .row {
      margin: 0;
   }
   .catchybga {
   padding: 2% 0;
   background-position: 100%;
   position: relative;
   margin-bottom: 45px;
   background: #4054b2;
   margin-top: -60;
   }
   .want {
   text-align: center;
   color: white;
   }
   .want h3 {
   color: white;
   font-size: 36px;
   font-weight: 600;
   margin-bottom: 0px;
   }
   .join a {
    background: none;
    border: none;
	padding:0px;
   }
   .join a:hover {
    border: none;
}
   .want a:hover {
   color: #f68c2f;
   background: white;
   border: 1px solid #f68c2f;
   }
   .stu {
   float: left;
   width: 100%;
       margin-bottom: 50px;
   }
   @media only screen and (max-width: 767px)
   {
   .want h3 {
   color: white;
   font-size: 23px;
   font-weight: 100;
   }
   .want p {
   font-size: 17px;
   }
   .eventhead {
   width: 100%;
   float: left;
   margin-top: 20px;
   }
   }
</style>

