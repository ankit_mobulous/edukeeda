<div class="afterlogin">
<?php
ob_clean(); ob_start();
session_start();
 /**
 /*
  Template Name: Home Page afterlogin
  */
get_header(); ?>

<style type="text/css">
input#exampleFormControlTextarea1::placeholder {
    color: #D3D3D3;
}
  .stepthree p{
      margin-bottom: 0px!important;
  }
  .stepthree{
    border: 1px solid burlywood;
    padding-left: 30px;
  } 
  .stepthree p span{
    margin-left: 10%;
  }
  .displ label {
    float: left;
    width: 100%;
    margin: 0px 6px;
    padding: 0px;
    font-size: 19px;
}
.displ {
    display: flex;
    float: left;
    width: 50%;
}
.disnot {
    display: flex;
    float: left;
    width: 50%;
}
.disnot input.form-control {
    width: 16px;
    padding: 0px;
    margin: 0px;
}
.displ input.form-control {
    width: 16px;
    padding: 0px;
    margin: 0px;
}
 .disnot label {
    float: left;
    width: 100%;
    margin: 0px 6px;
    padding: 0px;
    font-size: 19px;
}

</style>
<?php
   if(isset($_SESSION["login"])){
   }else{
      $url = "http://edukeeda.com/signin/";
      wp_redirect( $url );
      exit;
   }
?>
<div class="container">
   <div class="row">
      <div class="col-md-8">
         <div class="after">
            <h1> Complete your profile</h1>
         </div>
        <?php
            global $wpdb;
            $userId = $_SESSION["login"]["id"];
            $getStepOne = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_profileStepOne WHERE user_id = %d",$userId) 
              );
            $getStepTwo = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_profileStepTwo WHERE user_id = %d",$userId) 
              );

            $getUser = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_users WHERE ID = %d",$userId) 
              );
            $getStepSix = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_profileStepSix WHERE user_id = %d",$userId) 
              );
            $getStepSeven = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_profileStepSeven WHERE user_id = %d",$userId) 
              );

            $getProfilePic = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_user_pics WHERE user_id = %d",$userId) 
              );

            $getProfilePrivacy = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_email_privacy WHERE user_id = %d",$userId) 
              );

            $getProfilePrivacy1 = $wpdb->get_results( 
                  $wpdb->prepare( "SELECT * FROM wp_mobile_privacy WHERE user_id = %d",$userId) 
              );
        ?>
        <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profile-code0.php" method="post">
         <div class="col-md-12"> </div>
         
         <div class="col-md-12">
            <div class="classdetails">
               <div class="form-group">
                  <label for="exampleFormControlTextarea1">Profile Headline </label>
                  <input type="text" class="form-control" name="headline" id="exampleFormControlTextarea1" value="<?php if($getStepOne[0]->headline){ echo $getStepOne[0]->headline;}?>"  placeholder="E.g. Director/GOI/IIT Alumni">
               </div>
                      <div class="form-group">
                  <label for="exampleFormControlTextarea1">Profile Summary </label>
                  <input type="text" class="form-control" name="summaryother" id="" value="<?php if($getStepOne[0]->other){ echo $getStepOne[0]->other;}?>">
               </div>
            </div>
         </div>
         <!--<div class="col-md-12">
            <div class="classdetails">
               <div class="form-group">
                  <label for="exampleFormControlTextarea1">Name of Class/Course </label>
                  <textarea class="form-control" name="class" id="exampleFormControlTextarea1" rows="3" required=""><?php //if($getStepOne[0]->class){ echo $getStepOne[0]->class;}?></textarea>
               </div>
            </div>
         </div>-->
         <!--<div class="col-md-12">
            <div class="classdetails">
               <div class="form-group">
                  <label for="exampleFormControlTextarea1">Name Of Competitive Exam</label>
                  <textarea class="form-control" name="exam" id="exampleFormControlTextarea1" rows="3" required=""><?php //if($getStepOne[0]->exam){ echo $getStepOne[0]->exam;}?></textarea>
               </div>
            </div>
         </div>
         <div class="col-md-12">
            <div class="classdetails">
               <div class="form-group">
                  <label for="exampleFormControlTextarea1">Other</label>
                  <textarea class="form-control" name="other" id="exampleFormControlTextarea1" rows="3"><?php //if($getStepOne[0]->other){ echo $getStepOne[0]->other;}?></textarea>
               </div>
            </div>
         </div>-->
         <div class="col-md-12">
             <div class="ss">
               <div class="col-md-12">
                  <center><input type="submit" value="Save" name="submitstepone"></center>
               </div>
            </div>
         </div>
         </form>

         <button class="accordion active">Personal Information</button>
         <div class="panel " style="display: block;">
        <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profilecode1.php" method="post">
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> Name:  </lable>
                  </div>
                  <div class="col-md-7 koll">
                     <input type="text" name="name" id="name" value="<?php if($getUser[0]->display_name){ echo $getUser[0]->display_name;}?>" placeholder="First name" readonly> 
                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> Gender:  </lable>
                  </div>
                  <div class="col-md-7">
                     <select name="gender">
                        <option value="Male" <?php if($getStepTwo[0]->gender == "Male"){echo "selected";}?>>Male</option>
                        <option value="Female" <?php if($getStepTwo[0]->gender == "Female"){echo "selected";}?>>Female</option>
                        <option value="Others" <?php if($getStepTwo[0]->gender == "Others"){echo "selected";}?>>Others</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> Date of Birth:  </lable>
                  </div>
                  <div class="col-md-7 koll">

                     <div class="form-group">
                       <div id="filterDate2">
                        <div class="input-group">
                           <input type="date" name="dob" value="<?php if($getStepTwo[0]->dateofbirth){ echo $getStepTwo[0]->dateofbirth;}?>" class="form-control" placeholder="mm-yyyy" required="required">
                        </div>
                       </div>
                     </div>

                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> Email ID:  </lable>
                  </div>
                  <div class="col-md-7">
                     <input type="text" name="email" id="name" value="<?php if($getUser[0]->user_email){echo $getUser[0]->user_email;}?>" readonly> 
                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> Mobile Number.:  </lable>
                  </div>
                  <div class="col-md-7">
                     <input type="text" name="mobile" value="<?php if($getStepTwo[0]->mobile){ echo $getStepTwo[0]->mobile;}?>" id="name" placeholder="Enter Mobile Number"> 
                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="">
                  <div class="col-md-5">
                     <lable> Country/Region:  </lable>
                  </div>
                  <div class="col-md-7">
                     <select name="country" id="countryselect">
                        <?php
                          $getCountries = $wpdb->get_results("SELECT * FROM countries");
                          foreach($getCountries as $getCountrie) {
                        ?>
                        <option value="<?php echo $getCountrie->id; ?>" <?php if($getStepTwo[0]->country == $getCountrie->id){ echo "selected";}?> > <?php echo $getCountrie->name; ?></option>
                        <?php
                          }
                        ?>
                     </select>
                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> State:  </lable>
                  </div>
                  <div class="col-md-7">
                     <select name="state" id="stateselect">
                        <option value=""> Select </option>
                     </select> 
                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> City:  </lable>
                  </div>
                  <div class="col-md-7">
                     <input type="text" name="city" id="name" value="<?php if($getStepTwo[0]->city){ echo $getStepTwo[0]->city;}?>">
                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> Zip code/Pin Code: </lable>
                  </div>
                  <div class="col-md-7">
                     <input type="text" name="zip" id="name" value="<?php if($getStepTwo[0]->zipcode){ echo $getStepTwo[0]->zipcode;}?>" placeholder="Zip code/Pin Code" required=""> 
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-12">
             <div class="ss">
               <div class="col-md-12">
                  <center><input type="submit" value="Save" name="submitsteptwo"></center>
               </div>
            </div>
         </div>
         </form>

         <button class="accordion active">Details Of Education</button>
         <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <?php
                  $getStepThrees = $wpdb->get_results( 
                      $wpdb->prepare( "SELECT * FROM wp_profileStepThree WHERE user_id = %d ORDER BY id DESC",$userId) 
                  );
                  if($getStepThrees) {
                    foreach($getStepThrees as $getStepThree) {
                      $getEduName = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_schools WHERE id = %d",$getStepThree->school));
                  ?>  
                      <div class="stepthree" style="margin-bottom: 20px;">
                        <div class="row">
                          <div class="col-md-12">
                           <div class="row">
                              <div class="col-md-6">
                                 <p> <b>School/Institution :</b></p>
                              </div>
                              <div class="col-md-6">
                                 <p> <?php echo $getEduName[0]->schoolName; ?> <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profilecode2.php" method="post"> <input type="hidden" name="stepThreeId" value="<?php echo $getStepThree->id;?>"> <input type="submit" name="stepThreeDelete" value="X"style="float:right;margin-right: 10px;padding: 0px 10px;" class="btn btn-warning"></form></p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                  <p> <b>Degree/Course :</b> </p>
                              </div>
                              <div class="col-md-6">
                                 <p> <?php echo $getStepThree->degree;?> </p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <p> <b>Branch/Field Of Study : </b></p>
                              </div>
                              <div class="col-md-6">
                                 <p> <?php echo $getStepThree->branch;?> </p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                 <p> <b>Grade/Percentage :</b></p>
                              </div>
                              <div class="col-md-6">
                                 <p> <?php echo $getStepThree->grade;?> </p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                <p> <b>From :</b> </p>
                              </div>
                              <div class="col-md-6">
                                 <p> <?php echo date("M Y",strtotime($getStepThree->from)); ?> </p>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-6">
                                <p> <b>To :</b> </p>
                              </div>
                              <div class="col-md-6">
                                 <p> <?php echo date("M Y",strtotime($getStepThree->to));?> </p>
                              </div>
                           </div>

                  
                          </div>
                        </div>
                      </div>
                  <?php
                    }
                  }
                ?>
            </div>
         </div>
         <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profilecode2.php" method="post" enctype="multipart/form-data">
         <div class="panel " style="display: block;">
            <div>
              <p class="text-center text-primary">
                <?php
                  if(isset($_GET['addeducation'])) {
                     echo "Education added successfully";
                  }
                ?>
              </p>
            </div>
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> School/Institution:  </lable>
                  </div>
                  <div class="col-md-7 koll">
                     <input type="text" name="school" id="schoolname" value="" placeholder="Enter School/Institution" required="" list="school" onkeyup="myFunctionedu(this.value)"> 
                     <datalist id="school">
                     </datalist>
                  </div>
               </div>
            </div>

            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> Degree/Course  </lable>
                  </div>
                  <div class="col-md-7 koll">
                     <input type="text" name="degree" id="name" value="" placeholder="Enter Degree/Course" required=""> 
                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable>Field Of Study/Major Skills </lable>
                  </div>
                  <div class="col-md-7 koll">
                     <input type="text" name="branch" id="name" value="" placeholder="Enter Field Of Study" required=""> 
                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="ss">
                  <div class="col-md-5">
                     <lable> Grade/Percentage </lable>
                  </div>
                  <div class="col-md-7 koll">
                     <input type="text" name="grade" id="name" value="" placeholder="Enter Grade/Percentage"> 
                  </div>
               </div>
            </div>
            <div class="kol">
               <div class="col-md-5">
                  <p>From</p>
                  <div class="form-group">
                    <div id="filterDate2">
                     <div class="input-group date datetimepicker1" data-date-format="mm-yyyy">
                        <input type="date" name="from" value="" class="form-control" placeholder="mm-yyyy" required="required">
                     </div>
                   </div>
                  </div>
               </div>
               <div class="col-sm-7">
                  <p>To</p>
                  <div class="form-group">
                    <div id="filterDate2">
                     <div class="input-group date datetimepicker1" data-date-format="mm-yyyy">
                        <input type="date" name="to" value="" class="form-control" placeholder="mm-yyyy" required="required">
                     </div>
                    </div>
                  </div>
               </div>
            </div>
            <div class="form-group">
               <label>Description</label>
               <label></label>
               <textarea class="form-control" name="desc" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <div class="koi">
               <div class="col-md-5">
                  <!--<div class="posi">
                     <p>Add Other Education</p>
                  </div>-->
               </div>
            </div>
            <div class="col-md-12">
             <div class="ss">
               <div class="col-md-12">
                  <center><input type="submit" value="Save" name="submitstepthree"></center>
               </div>
            </div>
         </div>
         </div>
         </form>

         <button class="accordion">Add Experience/Position</button>
         <div class="panel" style="display: block;">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <?php
                      $getStepFours = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_profileStepFour WHERE user_id = %d ORDER BY id DESC",$userId));

                      if($getStepFours) {
                        foreach($getStepFours as $getStepFour) {
                          $getCompName = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_companies WHERE id = %d",$getStepFour->company_name));

                          $getCountryName = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM countries WHERE id = %d",$getStepFour->country));
                      ?>  
                          <div class="stepthree" style="margin-bottom: 20px;">
                            <div class="row">
                              <div class="col-md-12">
                                 <div class="row">
                                    <div class="col-md-6">
                                       <p> <b>Title:</b></p>
                                    </div>
                                    <div class="col-md-6">
                                       <p> <?php echo $getStepFour->title;?> <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profilecode3.php" method="post"> <input type="hidden" name="stepFourId" value="<?php echo $getStepFour->id;?>"> <input type="submit" name="stepFourDelete" value="X"style="float:right;margin-right: 10px;padding: 0px 10px;" class="btn btn-warning"></form></p>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <p> <b>Company Name:</b> </p>
                                    </div>
                                    <div class="col-md-6">
                                       <p> <?php echo $getCompName[0]->companyName; ?> </p>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <p> <b>Country/Region: </b></p>
                                    </div>
                                    <div class="col-md-6">
                                       <p> <?php echo $getCountryName[0]->name;?> </p>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <p> <b>Location:</b></p>
                                    </div>
                                    <div class="col-md-6">
                                       <p> <?php echo $getStepFour->location;?> </p>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <p> <b>From</b> </p>
                                    </div>
                                    <div class="col-md-6">
                                       <p> <?php echo date("M Y",strtotime($getStepFour->from));?> </p>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <?php if($getStepFour->to){ ?><p> <b>To :</b> </p><?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                      <?php if($getStepFour->to){?><p> <?php echo date("M Y",strtotime($getStepFour->to));?> </p><?php }?>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-6">
                                       <p> <b>Currently working :</b> </p>
                                    </div>
                                    <div class="col-md-6">
                                      <p> <?php if($getStepFour->current_working == 0){ echo "No";}else{echo "Yes";}?> </p>
                                    </div>
                                 </div>
				<div class="row">
                                    <div class="col-md-6">
                                       <p> <b>Brief about job :</b> </p>
                                    </div>
                                    <div class="col-md-6">
                                      <p> <?php if($getStepFour->desc){ echo $getStepFour->desc;}?> </p>
                                    </div>
                                 </div>
                                  
                              </div>
                            </div>
                          </div>
                      <?php
                        }
                      }
                    ?>
                </div>
             </div>
            <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profilecode3.php" method="post" enctype="multipart/form-data">
               <div class="kol">
               </div>
               <div class="panel positnpan1 sugglist">
                  <div>
                    <p class="text-center text-primary">
                      <?php
                        if(isset($_GET['addexperience'])) {
                           echo "Experience added successfully";
                        }
                      ?>
                    </p>
                  </div>
                  <div class="kol">
                     <div class="col-md-5">
                        <lable>Position/Designation:</lable>
                     </div>
                     <div class="col-md-7">
                        <div class="koll">
                           <input type="text" name="title" id="name" placeholder="Title" required=""> 
                        </div>
                     </div>
                  </div>
                  <div class="kol">
                     <div class="col-md-5">
                        <lable>Company Name:</lable>
                     </div>
                     <div class="col-md-7">
                        <div class="koll">
                           <input type="text" name="company_name" id="companyname" placeholder="Company Name" required="" list="comname" onkeyup="myFunctioncomp(this.value)">
                           <datalist id="comname">
                           </datalist>
                        </div>
                     </div>
                  </div>

                  <div class="kol">
                     <div class="">
                        <div class="col-md-5">
                           <lable> Country/Region:  </lable>
                        </div>
                        <div class="col-md-7">
                           <select name="country">
                              <?php
                                $getCountries = $wpdb->get_results("SELECT * FROM countries");
                                foreach($getCountries as $getCountrie) {
                              ?>
                              <option value="<?php echo $getCountrie->id; ?>" <?php if($getStepTwo[0]->country == $getCountrie->id){ echo "selected";}?> > <?php echo $getCountrie->name; ?></option>
                              <?php
                                }
                              ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="kol">
                     <div class="col-md-5">
                        <lable>City:</lable>
                     </div>
                     <div class="col-md-7">
                        <input type="text" name="location" id="name" placeholder="New Delhi" required="">
                     </div>
                  </div>
                  <div class="kol">
                     <div class="col-md-5">
                        <p>From</p>
                        <div class="form-group">
                           <div class="input-group date" id="datetimepicker1">
                              <input type="date" name="from" class="form-control" required="required">
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-7 datetwo">
                        <p>To</p>
                        <div class="form-group">
                           <div class="input-group date" id="datetimepicker1">
                              <input type="date" name="to" class="form-control">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="work">
                        <lable>Currently working </lable>
                        <lable><input type="checkbox" name="current_working" value="1" id="currentpos">Yes</lable>
                     </div>
                  </div>
                  <div class="form-group">
                     <label> Brief about job </label>
                     <label></label>
<?php
                          $my_content2 =" ";
                          $editor_id2 = 'desc';
                          $option_name2 ='desc';
                          wp_editor($my_content2, $editor_id2, array('wpautop'=> true,'textarea_name' => $option_name2, "media_buttons" => false, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true)); 
                      ?>
                  </div>
                  <div class="form-group">
                        <center><input type="submit" value="Save" name="submitstepfour"></center>
                  </div>
               </div>
            </form>
            
         </div>
         
         <button class="accordion active">Social Involment/Extra Curricular.</button>
         <div class="panel " style="display: block;">
            <div class="form-group">
               <label>Social Involment/Extra Curricular</label>
                <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profile-code.php" method="post">
                  <textarea class="form-control" name="social" id="exampleFormControlTextarea1" rows="3"><?php if($getStepSix[0]->social){echo $getStepSix[0]->social;}?></textarea>
                  <center><input type="submit" value="Save" name="submitstepsix" style="margin-top: 20px;"></center>
                </form>
            </div>
         </div>
         
         <!--<button class="accordion active">Accomplishments</button>
         <div class="panel " style="display: block;">
            <div class="form-group">
               <label>Accomplishments.</label>
               <form action="<?php //echo site_url(); ?>/wp-content/themes/sydney/profile-code.php" method="post">
                  <textarea class="form-control" id="exampleFormControlTextarea1" name="accomplishment" rows="3"><?php //if($getStepSeven[0]->accomplishment){echo $getStepSeven[0]->accomplishment;}?></textarea>
                  <center><input type="submit" value="Save" name="submitstepseven" style="margin-top: 20px;"></center>
               </form>
            </div>
         </div>-->
         
         <button class="accordion active">Language Known</button>
         <div class="panel " style="display: block;">
            <div class="form-group">
               <label>Language Known</label>
               <?php
                      $getStepEight = $wpdb->get_results( 
                          $wpdb->prepare( "SELECT * FROM wp_profileStepEighth WHERE user_id = %d ORDER BY id ASC",$userId) 
                      );
                      if($getStepEight) {

                        foreach($getStepEight as $getStepEighth) {
                          $getLang = $wpdb->get_results( 
                              $wpdb->prepare( "SELECT * FROM languages WHERE id = %d",$getStepEighth->user_lang) 
                          );
                      ?>  
                          <div class="stepthree" style="margin-bottom: 20px;">
                            
                            <div class="row">
                              <div class="col-md-6">
                                  <p> <b>Language Known: </b></p>
                              </div>
                              <div class="col-md-6">
                                <p> <?php echo $getLang[0]->name;?> <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profilecode8.php" method="post"> <input type="hidden" name="stepEightId" value="<?php echo $getStepEighth->id;?>"> <input type="submit" name="stepEightDelete" value="X" style="float:right;margin-right: 10px;padding: 0px 10px;" class="btn btn-warning"></form></p>
                              </div>
                            </div>
                          </div>
                      <?php
                        }
                      }
                    ?>
               <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profilecode8.php" method="post">
                  <select name="lang" class="form-control">
                    <option value=""> Select </option>
                    <?php 
                      $getLangs = $wpdb->get_results("SELECT * FROM languages");

                      foreach($getLangs as $getLang) {
                    ?>
                        <option value="<?php echo $getLang->id; ?>"> <?php echo $getLang->name; ?> </option>
                    <?php
                      }
                    ?>
                  </select>
                  <center><input type="submit" value="Save" name="submitsteplang" style="margin-top: 20px;"></center>
               </form>
            </div>
         </div>
         
         <button class="accordion active">Email id</button>
         <div class="panel " style="display: block;">
            <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profilecode10.php" method="post">
               <p> Email id :- <?php if($getUser[0]->user_email){echo $getUser[0]->user_email;}?></p>
              <div class="form-group">
                <div class="displ">
                  <input type="checkbox" class="form-control" name="emailcheck" <?php if($getProfilePrivacy[0]->id) { if($getProfilePrivacy[0]->status == "0"){} else{ echo "checked";}} else{ echo "checked";}?>>
                  <label>Mention this in my professional Profile</label>
                </div>
                
                <?php if($getProfilePrivacy) { 
                ?>
                    <input type="hidden" name="privacyId" value="<?php echo $getProfilePrivacy[0]->id;?>">
                <?php
                  }
                ?>
                  <center><input type="submit" value="Save" name="submitsteppass" style="margin-top: 20px;"></center>
              </div>
            </form>
         </div>

         <button class="accordion active"> Mobile </button>
         <div class="panel " style="display: block;">
            <form action="<?php echo site_url(); ?>/wp-content/themes/sydney/profilecode11.php" method="post">
               <p> Mobile Number :- <?php if($getStepTwo[0]->mobile){echo $getStepTwo[0]->mobile;}?></p>
              <div class="form-group">
                <div class="displ">
                  <input type="checkbox" class="form-control" name="emailcheck" <?php if($getProfilePrivacy1[0]->id) { if($getProfilePrivacy1[0]->status == "0"){} else{ echo "checked";}}?>>
                  <label>Mention this in my professional Profile</label>
                </div>
                
                <?php if($getProfilePrivacy1) { 
                ?>
                    <input type="hidden" name="privacyId" value="<?php echo $getProfilePrivacy1[0]->id;?>">
                <?php
                  }
                ?>
                  <center><input type="submit" value="Save" name="submitsteppass" style="margin-top: 20px;"></center>
              </div>
            </form>
         </div>

      </div>
      <div class="col-md-4">
         <div class="col-md-12 cat">
            <div class="side uploadsection">
              <?php
                if($getProfilePic){
              ?>
                  <img src="<?php echo $getProfilePic[0]->profilePic;?>" class="img-thumbnail" alt="Cinque Terre">
              <?php
                } else {
              ?>
               <img src="/img/person-dummy.png" class="img-thumbnail" alt="Cinque Terre">
              <?php
                }
              ?>
               <form method="post" action="<?php echo site_url(); ?>/wp-content/themes/sydney/profile-code7.php" enctype="multipart/form-data">
                  <input type="file" name="pic" id="fileCampusimg" accept="image/*"><br>
                <b>( Max size 500 kb)</b>
                  <input type="submit" value="Upload Image" id="profileupload" name="submit" disabled>
               </form>
            </div>
         </div>
      </div>
      <?php while ( have_posts() ) : the_post(); ?>
      <?php get_template_part( 'content', 'page' ); ?>
      <?php
         // If comments are open or we have at least one comment, load up the comment template
         if ( comments_open() || get_comments_number() ) :
           comments_template();
         endif;
         ?>
      <?php endwhile; // end of the loop. ?>
      </main><!-- #main -->
   </div>
   <!-- #primary -->
   <?php get_footer(); ?>
</div>


<script type="text/javascript">
   var uploadField = document.getElementById("fileCampusimg");
   uploadField.onchange = function() {
       if(this.files[0].size > 524288) {
          alert("File size is greater than 500kb");
          this.value = "";
          document.getElementById('profileupload').disabled = true;
       
       } else if(this.files[0].size < 0) {
            alert("File not found");
            this.value = "";
            document.getElementById('profileupload').disabled = true;
       } else{
      document.getElementById('profileupload').disabled = false;
   }
   };
</script>

<style>
@media (max-width: 767px){
 .page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper {
    padding: 0px;
}
}
   .work input[type="checkbox"] {
   margin-left: 46px;
   }
   .work {
   margin-bottom: 20px;
   }
   .brif label {
   float: left;
   color: #1c0658;
   }
   .classdetails label {
   float: left;
   color: #1c0658;
   }
   .ss input[type="checkbox"] {
   margin-left: 40px;
   }
   .side.uploadsection{
   text-align:center;
   }
   .side.uploadsection img.img-thumbnail {
   width: 100px;
   height: 100px;
   border-radius: 100%;
   }
   .side.uploadsection input[type=file] {
   display: inline-block;
   margin: 16px auto;
   }
   .kol {
   float: left;
   width: 100%;
   margin-bottom: 13px;
   }
   .kol input[type="submit"]{
   padding: 6px 29px;
   }
   .cat {
   padding: 0;
   }
   .kol input[type="text"], .kol input[type="date"], .kol select {
   width: 100%;
   padding: 0 !important;
   height: 30px;
   box-shadow: none;
   border-radius: 0px;
   background: transparent;
   margin-bottom: 0;
   padding: 1px 8px !important;
   font-size: 15px;
   }
   .posi p {
   font-size: 20px;
   font-weight: 600;
   color: #1c0658;
   }
   .accordion {
   color: #060658;
   cursor: pointer;
   padding: 18px;
   width: 100%;
   border: none;
   text-align: left;
   outline: none;
   font-size: 15px;
   transition: 0.4s;
   margin-bottom: 17px;
   background-color: #f5f5f5;
   box-shadow: none;
   border-radius: 0px;
   border-bottom: 2px solid #7171d2;
   }
   .mycustpos{
   color: #060658;
   cursor: pointer;
   padding: 0px;
   width:0px;
   border: none;
   font-size: 15px;
   }
   .mycustpos1{
   color: #060658;
   cursor: pointer;
   padding: 0px;
   width:0px;
   border: none;
   font-size: 15px;
   }
   .af select{
   width: 100%;
   border-radius: 0px;
   background: transparent;
   outline: none;
   height: auto;
   }
   .lol1{
   width: 100%;
   border-radius: 0px;
   background: transparent;
   outline: none;
   height: auto;
   }
   .af  input[type="text"]{
   width: 100%;
   padding: 0 !important;
   height: auto;
   box-shadow: none;
   border-radius: 0px;
   background: transparent;
   }
   .form{
   margin-bottom: 30px;
   }
   .lable {
   color: #060658;
   }
   .active, .accordion:hover {
   background-color: #f5f5f5;
   box-shadow: none;
   border-radius: 0px;
   border-bottom: 2px solid #7171d2;
   }
   .panel {
   padding: 0 18px;
   display: none;
   background-color: white;
   overflow: hidden;
   box-shadow: 0 7px 4px 0 rgba(0, 0, 0, 0.07);
   border-radius: 10px;
   }
   .after h1 {
   font-size: 30px;
   }
   .af h5{
   text-transform: capitalize;
   font-size: 15px;
   }
   .af {
   border-radius: 10px;
   background-color: white;
   padding: 10px;
   width: 100%;
   outline: none;
   float: left;
   }
   .aff {
   font-weight: 600;
   background-color: white;
   padding: 10px;
   width: 100%;
   outline: none;
   }
   .accordion:after {
   content: '\002B';
   color: #7171d2;;
   font-weight: bold;
   float: right;
   margin-left: 5px;
   font-size: 40px;
   }
   .active:after {
   content: "\2212";
   }
   .lol .fa {
   margin-right: 0px!important;
   }
   .lol1 .fa {
   margin-right: 0px!important;
   }
   .lol1 input#name {
   width: 100%;
   height: auto;
   }
   .From select {
   height: auto;
   width: 100%;
   }.From select {
   height: auto;
   width: 100%;
   }
   .to select {
   height: auto;
   width: 100%;
   }
   .input-group {
   position: relative;
   display: table;
   border-collapse: separate;
   width: 100%;
   }
   .lol input.form-control {
   height: auto;
   }
   input[type="number"]{
   width:100%;
   }
   .panel.positnpan.sugglist {
   display: block;
   float: left;
   }
   .panel.positnpan1.sugglist1 {
   display: block;
   float: left;
   }
   .panel.positnpan1.sugglist {
   float: left;
   width: 100%;
   display: block;
   }
   .panel.positnpan2.sugglist {
   display: block;
   }
   .pos button.accordion {
   width: 18px;
   background: transparent;
   border: none;
   }
   button.accordion.mycustpos2 {
   width: 18px;
   background: transparent;
   border: none;
   }
   .accordion.mycustpos1 {
   background: transparent;
   width: 16px;
   background: transparent;
   border: none;
   }
   .mycustpos1 {
   color: #060658;
   cursor: pointer;
   padding: 0px;
   width: 17px;
   border: none;
   font-size: 15px;}
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
   var acc = document.getElementsByClassName("accordion");
   var i;
   
   for (i = 0; i < acc.length; i++) {
       acc[i].addEventListener("click", function() {
           this.classList.toggle("active");
           var panel = this.nextElementSibling;
           if (panel.style.display === "block") {
               panel.style.display = "none";
           } else {
               panel.style.display = "block";
           }
       });
   }
</script>
<script>
   // $(function() {
   //     $( "#accordion" ).accordion({
   //       collapsible: true,
   //       active: false
   //     });
   //  });   
</script>

<script type="text/javascript">
   // $(function () {
   //     $('.datetimepicker1').datepicker({
   //        format: "mm-yyyy"
   //     });
   // });
</script>
<script>
   jQuery(".mycustpos").click(function (e) {
       e.stopPropagation();
      jQuery(".positnpan").toggleClass('sugglist');
   });
</script>
<script>
   jQuery(".mycustpos1").click(function (e) {
       e.stopPropagation();
      jQuery(".positnpan1").toggleClass('sugglist');
   });
</script>
<script>
   jQuery(".mycustpos2").click(function (e) {
       e.stopPropagation();
      jQuery(".positnpan2").toggleClass('sugglist');
   });

   function myFunctionedu(value) {
        var sname = value;
        $.ajax({
            type: "POST",
            dataType: "json",
            url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_school_name.php",
            data:{sname:sname},
            success : function(data) {
                document.getElementById('school').innerHTML = data;
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(textStatus, errorThrown);
            }
        });
    };

   function myFunctioncomp(value) {
          var cname = value;
          $.ajax({
              type: "POST",
              dataType: "json",
              url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_comapny_name.php",
              data:{cname:cname},
              success : function(data) {
                document.getElementById('comname').innerHTML = data;
              },
              error: function(jqXHR, textStatus, errorThrown){
                  console.log(textStatus, errorThrown);
              }
          });
      };

   $(document).ready(function(){
      $("#currentpos").click(function(){
         if($(this).is(":checked")) {
            $(".datetwo").css("display","none");
         } else {
            $(".datetwo").css("display","block");
         }
      });
   });
   $("#countryselect").on('change',function(e){
         var cid =  $(this).val();
          $.ajax({
                type: "POST",
                dataType: "json",
                url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_state_name.php",
                data:{cid:cid},
                success : function(data) {           
                    $("#stateselect").html(data);
                    console.log(data);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(textStatus, errorThrown);
                }
            });
   });
</script>

