<?php
session_start();
/*
Template Name: Premium Series template
*/
get_header();
?>
<style type="text/css">
  .col-md-4 .premium-class{border: 1px solid #000;padding: 20px;}
  .col-md-4 .premium-button a{width:100%;font-size: 21px;margin-top: 20px;}
</style>

<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
  <div class="row">
    <div class="col-md-12">
      <h2><center>Premium Tests</center></h2>
    </div>
  </div>
  <div class="row">
    <?php
      global $wpdb;
      $status = 1;
      $results = $wpdb->get_results($wpdb->prepare( "SELECT * FROM wp_premium_exam_type WHERE status=%d", $status));

      foreach($results as $result) {
    ?>
        <div class="col-md-4">
          <div class="premium-class">
            <h5><center> <?php echo $result->exam_title; ?> </center></h5>
            <?php echo $result->exam_desc; ?>
            <h6>Limited time offer - Join for free and make us proud</h6>
          </div>
          <div class="premium-button">
          <?php
            
            if($_SESSION["login"]) {
          ?>
            <center><a href="<?php site_url();?>/jointest?action=<?php echo $result->id;?>" role="button" class="btn btn-warning">Join now</a></center>
          <?php
            } else {
          ?>
              <center><a href="<?php site_url();?>/post-article-form?page=premium" role="button" class="btn btn-warning">Join now</a></center>
          <?php
            }
          ?>
          </div>
        </div>
    <?php
      }
    ?>
  </div>
   
  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>

