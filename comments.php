<?php
session_start();
/**
 * The template for displaying comments.
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package Sydney
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
  return;
}
?>
<style>
    .commenttoggle .btn-lg {
    padding: 10px 16px;
    font-size: 18px;
    line-height: 1.3333333;
    border-radius: 6px;
    float: none!important;
}
   .bypostauthor {
    float: left;
    width: 100%;
}
.alt{
    min-height: 0px;
}
.commenttoggle .btn-warning {
    color: #fff;
    background-color: #52c641;
    border-color: #52c641;
}
.commenttoggle input#submit {
    background-color: #52c641;
}
.commenttoggle input#submit:hover{
  background-color: #fff;
}
</style>
<div id="comments" class="comments-area">

  <?php // You can start editing here -- including this comment! ?>

    <div class="commenttoggle">
      <button type="button" class="btn btn-warning btn-lg" id="cmtToggleButton" style="float: right;"> Show Comments (<?php if(have_comments()){ echo get_comments_number();} ?>)</button>
      <div style="clear:both;"></div>
      
      <?php
        $args = array(
          'comment_notes_after'  => '',
          'title_reply' => __( 'Comments' ),
        );
        comment_form($args);
      ?>
      <?php
        if(!$_SESSION['login']) {
      ?>
          <p class="cmmtcheck">You must <a href="http://edukeeda.com/signin/"> Register </a> or <a href="http://edukeeda.com/signin/">Login</a> to post a comment.</p>
      <?php
        } 
      ?>
    </div>
  <?php if ( have_comments() ) : ?>
    <h4 class="comment-title">
      <?php
        printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'sydney' ),
          number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
      ?>
    </h4>

    <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
    <nav id="comment-nav-above" class="comment-navigation" role="navigation">
      <h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'sydney' ); ?></h1>
      <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'sydney' ) ); ?></div>
      <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'sydney' ) ); ?></div>
    </nav><!-- #comment-nav-above -->
    <?php endif; // check for comment navigation ?>

    <ol class="comments-list">
      <?php
        wp_list_comments( array(
          'style'      => 'ol',
          'short_ping' => true,
          'avatar_size'=> 60,
          'reverse_top_level'=>true,
        ) );
      ?>
    </ol><!-- .comment-list -->

    <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
    <nav id="comment-nav-below" class="comment-navigation" role="navigation">
      <h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'sydney' ); ?></h1>
      <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'sydney' ) ); ?></div>
      <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'sydney' ) ); ?></div>
    </nav><!-- #comment-nav-below -->
    <?php endif; // check for comment navigation ?>

  <?php endif; // have_comments() ?>

  <?php
    // If comments are closed and there are comments, let's leave a little note, shall we?
    if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
  ?>
    <p class="no-comments"><?php _e( 'Comments are closed.', 'sydney' ); ?></p>
  <?php endif; ?>

</div><!-- #comments -->
