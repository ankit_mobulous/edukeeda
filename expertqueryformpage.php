<div class="Postyourpage">
<?php
session_start();
/**
/*
 Template Name: Expert Query Form Page
 */
if(isset($_SESSION["posterror"])) {
    echo "<script>alert('Minimum 450 words required in description');</script>";
    unset($_SESSION["posterror"]);
}
if($_SESSION["login"]) {

} else {
  header('Location: https://edukeeda.com/signin/');
}  
$userId = $_SESSION["login"]["id"];
get_header(); ?>
<script type="text/javascript">
    $(document).ready(function(){
      $("#catone").on("change", function(){
            $(".showone").css("display","none");
            $(".showtwo").css("display","none");
            $(".showthree").css("display","none");
            var cid = $(this).val();
            $.ajax({
                type: "POST",
                dataType: "json",
                url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_expert_cat_list.php",
                data:{cid:cid},
                success : function(data) {
                    $(".showone").css("display","block");
                    $("#cattwo").html(data);
                    console.log(data);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(textStatus, errorThrown);
                }
            });
      });
  });

  $(document).ready(function(){
      $("#cattwo").on("change", function(){
          $(".showthree").css("display","none");
            var cid = $("#catone").val();
            var ccid = $(this).val();

            $.ajax({
                type: "POST",
                url : "<?php echo site_url(); ?>/wp-content/themes/sydney/ajaxcall/get_expert_cat_list.php",
                data:{cid:cid,ccid:ccid},
                success : function(data) {
                  console.log(data.length);
                    if(data.length <=3){
                      $(".showtwo").css("display","none");
                      $(".showthree").css("display","block");
                    } else {
                      $(".showtwo").css("display","block");       
                      $("#catthree").html(data);
                    }
                    //console.log(data);
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(textStatus, errorThrown);
                }
            });
      });
  });
  $(document).ready(function(){
      $("#catthree").on("change", function(){
          $(".showthree").css("display","block"); 
      });
  });
</script>
   <div class="col-md-12 col-sm-12">
      <div class="filteropt">
         <p>Expert Gyan</p>
         <div class="row">
            <div class="col-md-12" style="padding: 45px;">
              <form method="post" action="<?php echo site_url(); ?>/wp-content/themes/sydney/expertquerycode.php" enctype="multipart/form-data" id="expertform">
                  <div class="form-group"> 
                    <label for="title" style="font-size: 21px;">Which category of Expert Gyan Section is best suited for your Post / Article / Blog?</label>
                    <select name="catone" class="form-control" id="catone" required>
                        <option value=""> Select </option>
                        <option value="industry"> Industry/Business </option>
                        <option value="student_stuff"> College Stuff & Career </option>
                        <option value="blogs"> Blogs & Opinion </option>
                        <option value="home_appliances"> Home Appliances </option>
                    </select>
                  </div>
                  <div class="form-group showone" style="display: none">
                    <label for="title">Category</label>
                    <select name="cattwo" class="form-control" id="cattwo" required>
                        <option value=""> Select </option>
                    </select>
                  </div>
                  <div class="form-group showtwo" style="display: none">
                    <label for="title">Sub Category</label>
                    <select name="catthree" class="form-control" id="catthree" required>
                        <option value=""> Select </option>
                    </select>
                  </div>
                  <div class="showthree" style="display: none">
                    <div class="form-group">
                      <label for="title">Title:</label>
                      <input type="text" name="title" class="form-control" id="title" required>
                    </div>
                    <div class="form-group">
                      <label for="title">Short Description (Write best suited 1 to 3 lines to highlight appropriate subject/concept/summary of your post):</label>
                      <?php
                          $my_content1 =" ";
                          $editor_id1 = 'shortDesc';
                          $option_name1 ='shortDesc';
                          wp_editor($my_content1, $editor_id1, array('wpautop'=> true,'textarea_name' => $option_name1, "media_buttons" => true, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true));
                      ?>
                    </div>
                    <div class="form-group">
                      <label for="title">Description:</label> (Write Minimum 450 Words For A Good Post)
                      <?php
                          $my_content2 =" ";
                          $editor_id2 = 'desc';
                          $option_name2 ='desc';
                          wp_editor($my_content2, $editor_id2, array('wpautop'=> true,'textarea_name' => $option_name2, "media_buttons" => true, 'editor_height' => 200, 'editor_class'=>'mytext_class', 'tinymce'=> true,'quicktags'=> true)); 
                      ?>
                    </div>
                    <div class="form-group">
                      <label for="title">Upload Image (Min 200 KB, Max 1 MB):</label>
                      <input type="file" name="fileexpert" class="form-control" id="short" accept="image/jpeg,image/jpg" required="required">
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" name="submitexpert" value="Submit" class="btn btn-primary expertSubmit">
                    </div>
                  </div>
              </form>
            </div>
          </div>
      </div>
   </div>

   <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>
   <?php endwhile; // end of the loop. ?>
   </main><!-- #main -->
</div>
<!-- #primary -->
<?php get_footer(); ?>
</div>
<style>
   .stu {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   margin-bottom: 50px;
   }
   .stu h1 {
   font-size: 27px;
   margin: 15px 30px;
   }
   .optstu {
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   float: left;
   width: 100%;
   margin: 14px 3px;
   }   
   .optstu p {
   margin-bottom: 0px;
   font-size: 13px;
   color: #060658;
   padding: 20px;
   }
   .optstu input[type="checkbox"] {
   margin: 0px 11px;
   }
   .filteropt {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   margin-bottom: 30px;
   margin-top: 50px;
   padding-bottom: 30px;
   }
   .Query {
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   }
   .Query p {
   font-size: 13px;
   margin: 36px 28px;
   float: left;
   }
   .Queryw{
   float: left;
   width: 100%;
   box-shadow: #ddd 0px 0px 18px 0px;
   border-radius: 11px 11px 11px 11px;
   }
   .Queryw p {
   font-size: 24px;
   margin: 36px 28px;
   float: left;
   font-weight: 600;
   color: #060658;
   }
   .sub {
   float: left;
   width: 100%;
   margin: 15px 0px;
   text-align: center;
   }
   .sub button.btn.btn-primary {
   background: #f68e2f;
   }
   .filteropt select {
   background: #fff;
   border: 1px solid #ddd;
   height: 42px;
   line-height: 8px;
   padding: 1px 16px;
   margin: 39px 0px 0 22px;
   }
   .filteropt p{
   float: left;
    padding: 28px 10px 10px 30px;
    color: #0a0a5b;
    font-weight: 600;
    font-size: 24px;
    margin-bottom: 0px;
   }
   p.wantpost {
    margin-bottom: 0px!important;
    padding-bottom: 0px;
    font-weight: 300;
    margin-left: 5%;
  }
  .tab-content {
    border: 1px solid #ddd;
    min-height: 500px;
        padding: 33px;
}
.nav-tabs>li {
    width: 50%;
}
.filteropt select {
    margin: 5px 0px 0 1px!important;
}
</style>
<script type="text/javascript">
   var uploadField = document.getElementById("short");
   uploadField.onchange = function() {
       if(this.files[0].size > 1024000) {
          alert("File size is greater than 1MB");
          this.value = "";
       
       } else if(this.files[0].size < 102400) {
            alert("File size is less than 100kb");
            this.value = "";
       }
   };
</script>
