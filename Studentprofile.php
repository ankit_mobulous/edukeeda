<div class="ajeetbaba">

<?php
/**
/*
 Template Name: Student Profile Login
 */

get_header(); 

//print_r($_SESSION);

?>





 <style>
 header {
    display: none;
}

.dynamicprofile {
    float: left;
    margin: 0 auto;
    width: 100%;
    padding: 10px 27px;
    background: #ffffff;
    color: #5c6b77;
    box-shadow: 0px 0px 4px 0px;
        margin-bottom: 0px;
}
.dynamicprofile img{
	    width: 35px;
    height: 34px;
    float: left;
    margin-right: 10px;
}

.dynamicprofile p.username.info {
    font-size: 18px;
    margin: 0;
    color: #1553de;
}

.header-clone {
    display: none;
}

.settingsopn {
    float: right;
}

.settingsopn ul {list-style-type:none;    padding: 0; margin: 0;}
.settingsopn ul li{display:inline-block;    margin-right: 30px;}
.settingsopn ul li:last-child{ margin-right: 0px;}
.usernamesopn{float:left;     width: 50%;}
.studentprofilesmt {
    float: left;
    width: 100%;
}

.studentprofilesmt .elementor-widget-tabs .elementor-tab-title.elementor-active {
    color: #ffffff;
    background: #555ba2;
    border-radius: 1px 33px 1px 0px;
    box-shadow: none;
    border: 0px solid #fff;
}

.studentprofilesmt .elementor-widget-tabs .elementor-tab-title{
	    color: #a3a9ab;
}

.studentprofilesmt .elementor-tabs {
    box-shadow: #bdbdbd 0px 0px 20px 0px;
}

.studentprofilesmt .elementor-column-gap-default>.elementor-row>.elementor-column>.elementor-element-populated {
    padding: 0 12px;
}

.studentprofilesmt .entry-content {
    margin: 0 auto;
    float: none;
    width: 89%;
}

.advanceftr {
    float: left;
    width: 100%;
}
.advanceftr h5 {
    font-size: 20px;
    padding: 0 23px;
}

.mainhead {
    margin: 0px 0 40px 0;
    text-align: center;
    background: #8787d0;
    padding: 20px 0;
    color: #fff;
}
.mainhead h6 {
    color: #ffffff;
}
 </style>
 <div class="studentprofilesmt">


 <div class="dynamicprofile">
 <div class="entry-content">
 <div class="usernamesopn"><span><img src="../img/competitive.jpg"><p class="username info"><strong>Welcome Mr./Ms. </strong><?php echo !empty($_SESSION['user_data']->display_name)?$_SESSION['user_data']->display_name:"NOT FOUND";?></p></span></div>
 <div class="settingsopn">
 <ul>
 <li><i class="fa fa-home"></i> <a href="<?php echo site_url(); ?>">Home</a> </li>
  <li><i class="fa fa-cog"></i> <a href="#">Manage Settings</a></li>
   <li><i class="fa fa-sign-out"></i><a href="<?php echo site_url(); ?>/logout"> Logout</a></li>
 </ul>
 </div>
 </div>
 </div>
 
  <div class="col-md-12 mainhead">
<h6 class="elementor-heading-title elementor-size-default">Best Quality Practice Exercise & Test Series at your screen</h6>
</div>
 
 <div class="advanceftr">

 </div>

	<div id="primary" class="content-area col-md-12">
		<main id="main" class="post-wrap" role="main">

			<?php while ( have_posts() ) : the_post();
global $post;

			?>

				<?php
				if($post->ID=="921")
				{
						get_template_part( 'content', 'student' );
				}
				else{
							get_template_part( 'content', 'page' );
				}
			 ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
				?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_footer(); ?>
</div>
</div>
