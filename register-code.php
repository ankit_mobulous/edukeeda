  <?php
session_start();
$path = $_SERVER['DOCUMENT_ROOT'];

include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';

global $wpdb;

    if(isset($_POST['submit'])){
        $response = array();
        $first_name = trim($_POST['p_name']);
        $mobile = trim($_POST['p_mobile']);
        $email = trim($_POST['p_email']);
        $password = trim($_POST['p_password']);
        $confirmPassword = trim($_POST['p_c_password']);

        $username = $first_name;

        if($password === $confirmPassword) {

          $results = $wpdb->get_results( 
              $wpdb->prepare( "SELECT * FROM wp_users WHERE user_email = %s", $email) 
          );

          $results1 = $wpdb->get_results( 
              $wpdb->prepare( "SELECT * FROM wp_users WHERE mobile = %d", $mobile) 
          );

          if ($results) {
              $already = "Email id already exists";
              $_SESSION["registerfailed"] = $already;
              header('Location: https://edukeeda.com/signin/');

          } else if($results1){
              $already = "Mobile Number already exists";
              $_SESSION["registerfailed"] = $already;
              header('Location: https://edukeeda.com/signin/');

          } else {

              $user_id = wp_create_user($username, $password, $email);

              if (!is_wp_error($user_id)) {

                $user = get_user_by('id', $user_id);
                
                $user_role = new WP_User( $user->ID );
                $user_role->set_role('author');

                $wpdb->update("wp_users", array('user_status'=>1), array('ID'=>$user_id));
                $wpdb->update("wp_users", array('newuser'=>1), array('ID'=>$user_id));
                $wpdb->query($wpdb->prepare("DELETE FROM wp_verify_email WHERE email = %s", $email));

                $dataName = array("user_id"=>$user_id, "name" => $first_name,"mobile"=>$mobile);
                $wpdb->insert("wp_profileStepTwo", $dataName);

                $success = "You have registered successfully. Now you can login";
                $_SESSION["registersuccess"] = $success;
                unset($_SESSION["registerfailed"]);
                header('Location: https://edukeeda.com/signin/');
              } else {
                $error = "Registration Failed";
                $_SESSION["registerfailed"] = $error;
                header('Location: https://edukeeda.com/signin/');
              }
          }
        } else {
            $notMatch = "Password field and confirm password field does not match";
            $_SESSION["registerfailed"] = $notMatch;
            header('Location: https://edukeeda.com/signin/');
        }
    }

?>
