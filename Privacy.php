<div class="eventnew">
<?php
/**
/*
Template Name:privacypolicy
*/
get_header();
?>
<style>

    h1 {
      font-size: 35px!important;
     

   }
 h1:before {
    content: "";
    width: 120px;
    height: 9px;
    background: #060658;
    position: absolute;
    bottom: 8px;
    left: 0;
}
 h1:after {
    content: "";
    width: 26px;
    height: 9px;
    background: #f68e2f;
    position: absolute;
    bottom: 8px;
    left: 0;
    z-index: 1;
    }
   
		.mainheader {
    width: 100%;
    float: left;
    position: relative;
}
	@media only screen and (max-width: 767px){
		.page .page-wrap .content-wrapper, .single .page-wrap .content-wrapper{
          padding: 0px 30px;      
        }
        .mainheader {
    width: 100%;
    float: left;
    position: relative;
    padding-top: 20px;
}
	}
</style>
       
       
			 <?php while ( have_posts() ) : the_post(); ?>
   <?php get_template_part( 'content', 'page' ); ?>
   <?php endwhile; // end of the loop. ?>
        

<?php get_footer(); ?>
